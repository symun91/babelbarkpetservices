<?php
/**
 * @property appointmentmodel      $appointmentmodel
 * @property petbreedmodel         $petbreedmodel
 * @property servicemodel          $servicemodel
 * @property vendorsmodel          $vendorsmodel
 * @property vendorcategoriesmodel $vendorcategoriesmodel
 * @property customermodel         $customermodel
 * @property petmodel              $petmodel
 * @property appusersmodel         $appusersmodel
 * @property appuservendorsmodel   $appuservendorsmodel
 * @property petprescriptionmodel  $petprescriptionmodel
 * @property petvaccinationmodel   $petvaccinationmodel
 * @property promotionsmodel       $promotionsmodel
 * @property sikkaofficemodel      $sikkaofficemodel
 */
class Sikkacron extends CI_Controller
{
    var $data=array();
    var $file_path;

    function __construct()
    {

        parent::__construct();
        $this->load->model('appointmentmodel');
        $this->load->model('petbreedmodel');
        $this->load->model('servicemodel');
        $this->load->model('vendorsmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('customermodel');
        $this->load->model('petmodel');
        $this->load->model('appusersmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('petprescriptionmodel');
        $this->load->model('petvaccinationmodel');
        $this->load->model('promotionsmodel');
        $this->load->model('sikkaofficemodel');

    }

    function updateAllPracticeToBizbark(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if ($user_agent == 'Bizbark cURL Request') {
            $bizbarksikkavendors  = $this->getAllSikkaVendors();
            foreach ($bizbarksikkavendors as $rows){
                $sikkaofficeid = $rows['sikkaoffice_id'];
                $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($sikkaofficeid);
                $request_key = $this->getrequestid($sikkaofficeid,$secret_key['secret_key']);
                $sikkapracticesurl = "https://api.sikkasoft.com/v2/practices?request_key=".$request_key;
                $detailjson = file_get_contents($sikkapracticesurl);
                $sikkapractices = json_decode($detailjson);
                $sikkapracticeid=$sikkapractices->practice_id;
                $data = array(
                    'sikkapractice_id'=>$sikkapracticeid
                );
                $isUpdated = $this->updateSikkaVendor($data,$sikkaofficeid);
                $sikkapracticeofficeid = $sikkaofficeid.'_'.$sikkapracticeid;
                $this->updatepracticetoBizbark($sikkapracticeofficeid);
                $this->addPrescription($sikkapracticeofficeid);
                $this->addVaccination($sikkapracticeofficeid);

            }
            echo true;
        }else{
            echo false;
        }
    }



    private function getrequestid($sikkaofficeid,$secret_key)
    {
        $sikkarequestkey = "https://api.sikkasoft.com/v2/start?app_id=".APP_ID."&app_key=".APP_KEY."&office_id=".$sikkaofficeid."&secret_key=".$secret_key;
        $detailjson = @file_get_contents($sikkarequestkey);
        if($detailjson === FALSE)
        {
            return NULL;
        }else{
            $requestkey = json_decode($detailjson);
            return $requestkey[0]->request_key;
        }



    }

    private function addGuarantortoDatabase($guarantors,$vendorid)
    {
        $data = array();
        for ($i = 0; $i < count($guarantors); $i++)
        {
            $data = array(
                'vendorid' => $vendorid,
                'firstname' => $guarantors[$i]->firstname,
                'lastname' => $guarantors[$i]->lastname,
                'phoneno' => $guarantors[$i]->workphone,
                'email' => $guarantors[$i]->email,
                'address' => $guarantors[$i]->address_line1,
                'city' => $guarantors[$i]->city,
                'state' => $guarantors[$i]->state,
                'country' => "",
                'zipcode' => $guarantors[$i]->zipcode,
                'sikka_status' => $guarantors[$i]->status,
                'guarantor_id' => $guarantors[$i]->guarantor_id,
                'practice_id' => $guarantors[$i]->practice_id,
                'patient_id'=>$guarantors[$i]->patient_id
            );
            $this->customermodel->updateSikkaCustomer($data);
        }
    }

    private function addVetPatientstoDatabase($vet_patients)
    {
        $data = array();

        $count_repeat = 0;

        for ($i = 0; $i < count($vet_patients); $i++)
        {

            $count_repeat = 0;

            $gender = $vet_patients[$i]->gender;

            if (strpos($gender, 'Female') !== false)
            {
                $gender = 'F';
            }
            else
            {
                $gender = 'M';
            }
            if($vet_patients[$i]->microchip)
            {
                $microchipped = '1';
            }
            else
            {
                $microchipped = '0';
            }

            $breedid = $this->petbreedmodel->getBreedid($vet_patients[$i]->breed);
            $breedid = intval($breedid);
            $petid = $this->genratePrimaryKey('50');
            while($this->petmodel->petIdExists($petid) == TRUE)
            {
                $petid = $this->genratePrimaryKey('60');
                $count_repeat++;
                /*
                                $arr_content = array();

                                $arr_content['percent'] = 70+$count_repeat;
                                $arr_content['message'] = "Duplicate";

                                file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));*/


            }
            $weight = 0;

            if($vet_patients[$i]->weight)
            {
                $weight = intval($vet_patients[$i]->weight);
            }
            $data = array(
                'name' => $vet_patients[$i]->petname,
                'gender' => $gender,
                'birthdate' => $vet_patients[$i]->birthdate,
                'isadopted' => 0,
                'currentweight' => $weight,
                'furcolor' => $vet_patients[$i]->color,
                'microchipped' => $microchipped,
                'microchipid' => $vet_patients[$i]->microchip,
                'primarybreed' => $breedid,
                'veterinarianid' => 1,
                'profileCompletion'=> 70,
                'guarantor_id' => $vet_patients[$i]->guarantor_id,
                'practice_id' => $vet_patients[$i]->practice_id,
                'patient_id'=>$vet_patients[$i]->patient_id
            );

            $response = array();

            $response =  array_merge($response,$data);

            $response['pettid']  = $petid;

            $response['time'] = 'Before';

            //file_put_contents("tmp/" . "tempsikkapet" . ".txt", json_encode($response));


            $this->petmodel->updateSikkaPet($data,$petid);

            $response['time'] = 'After';
            //file_put_contents("tmp/" . "tempsikkapet" . ".txt", json_encode($response));
        }
    }

    private function addPrescriptiontoDatabase($prescriptions)
    {
        $data = array();
        $filteredData = array();
        $count = 0;


        for ($i = 0; $i < count($prescriptions); $i++)
        {

            $itemtype = $prescriptions[$i]->item_type;

            if (strpos($itemtype, 'Meds:') !== false && $this->petmodel->sikkapetexisttoggle($prescriptions[$i]->guarantor_id,$prescriptions[$i]->patient_id) == TRUE)
            {
                $data[$count] = array(
                    'description' => $prescriptions[$i]->description,
                    'item_type' => $prescriptions[$i]->item_type,
                    'direction' => $prescriptions[$i]->direction,
                    'quantity' => $prescriptions[$i]->quantity,
                    'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                    'patient_id' => $prescriptions[$i]->patient_id
                );
                $noOfRows = $this->petprescriptionmodel->updatePrescriptions($data[$count]);
                if($noOfRows<=0){
                    $filteredData[$count] = array(
                        'description' => $prescriptions[$i]->description,
                        'item_type' => $prescriptions[$i]->item_type,
                        'direction' => $prescriptions[$i]->direction,
                        'quantity' => $prescriptions[$i]->quantity,
                        'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                        'patient_id' => $prescriptions[$i]->patient_id
                    );
                }
                $count++;

            }
            else if(strpos($itemtype, 'Item') !== false && $this->petmodel->sikkapetexisttoggle($prescriptions[$i]->guarantor_id,$prescriptions[$i]->patient_id) == TRUE)
            {
                $data[$count] = array(
                    'description' => $prescriptions[$i]->description,
                    'item_type' => $prescriptions[$i]->item_type,
                    'direction' => $prescriptions[$i]->direction,
                    'quantity' => $prescriptions[$i]->quantity,
                    'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                    'patient_id' => $prescriptions[$i]->patient_id
                );
                $noOfRows = $this->petprescriptionmodel->updatePrescriptions($data[$count]);
                if($noOfRows<=0){
                    $filteredData[$count] = array(
                        'description' => $prescriptions[$i]->description,
                        'item_type' => $prescriptions[$i]->item_type,
                        'direction' => $prescriptions[$i]->direction,
                        'quantity' => $prescriptions[$i]->quantity,
                        'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                        'patient_id' => $prescriptions[$i]->patient_id
                    );
                }
                $count++;
            }

        }

        If(!empty($filteredData)){
            $this->petprescriptionmodel->addPrescriptions($filteredData);
        }

    }
    private function addVaccinationstoDatabase($vaccinations,$office_id)
    {
        $data = array();

        $datacc = array();
        $count = 0;

        $saveit = TRUE;

        for ($i = 0; $i < count($vaccinations); $i++)
        {
            $petiddata = $this->petmodel->sikkapetexist($vaccinations[$i]->guarantor_id,$vaccinations[$i]->patient_id);
            $description = $vaccinations[$i]->description;
            if($petiddata['petid'] > 0)
            {
                $data['petid'] = $petiddata['petid'];


                switch ($office_id) {
                    case 'V11128': #Strathem
                        if(strpos($description, 'DAPP 5 Vaccine 1 yr') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'DAPP 5 Vaccine 3 yr') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Bordetella / Para / Adeno Vaccine nasal') !== false)
                        {
                            $data['vaccineid'] = 10; #Adenovirus (canine hepatitis)
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Rabies Vaccine Canine 1 yr (Nobivac)') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Rabies Vaccine Canine 3 yr (Nobivac)') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Canine Influenza Vaccine H3N2/H3N8') !== false)
                        {
                            $data['vaccineid'] = 2; #CIV
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Leptospirosis Vaccine 1 yr') !== false)
                        {
                            $data['vaccineid'] = 3; #Leptospirosis
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm Lyme Ehrlic Anap (In-house)') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm Antigen by ELISA Snap') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm / Tick 4Dx (I72440)') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{

                            $saveit = FALSE;
                        }
                        break;

                    case 'V9333': #Acacia

                        if(strpos($description, 'WP DA2PPV/Corona Adult') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'RABIES VACCINATION 1 YEAR') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Rabies Canine') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Rabies Purevax') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Bordetella Adult') !== false)
                        {
                            $data['vaccineid'] = 1; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Lab-Heartworm Antigen Canine') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'WP Blood Parasite Screen/HWT') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{
                            $saveit = FALSE;
                        }

                        break;
                    case 'V15410': #BabelBark AVImark Test

                        if(strpos($description, 'DA2PP') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Rabies') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Bordatella') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Lyme') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{
                            $saveit = FALSE;
                        }

                        break;
                    default:
                        return;
                }
                $data['duedate'] = $vaccinations[$i]->due_date;

                if(isset($data['petid'])&& isset($data['vaccineid']) && isset($data['duedate'])){
                    $noOfVaccine = $this->petvaccinationmodel->checkVaccineExist($data['petid'],$data['vaccineid'],$data['duedate']);
                    if($saveit==TRUE && $noOfVaccine<=0)
                    {
                        $datacc[$count] = $data;
                        $count++;
                    }

                }

            }

        }
        if(!empty($datacc)){
            $this->petvaccinationmodel->addPetVaccination($datacc);
        }
    }



    function addPrescription($sikkapracticeofficeid)
    {

        //$sikkapracticeofficeid = $this->input->post('practice_id');
        $office_practiceid = explode("_", $sikkapracticeofficeid);

        $result = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);
        $vendorid = $result['vendorid'];
        if(!$vendorid)
        {
            echo json_encode("Vendorid not found");
            return;
        }

        $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);
        $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
        $offset = 0;
        $limit = 500;

        $detailjson = $this->fetchPrescriptions($request_key,$office_practiceid[1],$offset,$limit);

        if($detailjson)
        {

            $detail = json_decode($detailjson);
            $totalCount = intval($detail[0]->total_count);
            $prescriptions = $detail[0]->items;

            $this->addPrescriptiontoDatabase($prescriptions);


            $loopCount = ceil($totalCount/$limit);

            for($i=0;$i<$loopCount;$i++)
            {
                $offset = ($i+1);
                $detailsecondjson = $this->fetchPrescriptions($request_key,$office_practiceid[1],$offset,$limit);
                $detail = json_decode($detailsecondjson);
                if(is_array($detail) && is_array($detail[0]->items))
                {
                    $prescriptions = $detail[0]->items;
                    $this->addPrescriptiontoDatabase($prescriptions);
                }
            }

            $data = array('prescription_added' => 1);

            $result = $this->vendorsmodel->updateVendor($data,$vendorid);

        }
        else{
            echo json_encode("No prescriptions found for this practice!");
        }

    }

    function addVaccination($sikkapracticeofficeid)
    {
        ini_set('max_execution_time', 18000);


        //$sikkapracticeofficeid = $this->input->post('practice_id');
        $office_practiceid = explode("_", $sikkapracticeofficeid);

        $result = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);
        $vendorid = $result['vendorid'];
        if(!$vendorid)
        {
            echo json_encode("Vendor id not found");
            return;
        }

        $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);
        $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
        $offset = 0;
        $limit = 2000;


        $detailjson = $this->fetchVaccinations($request_key,$office_practiceid[1],$offset,$limit);


        $office_id = $office_practiceid[0];


        if($detailjson)
        {

            $detail = json_decode($detailjson);
            $totalCount = intval($detail[0]->total_count);
            $vaccinations = $detail[0]->items;
            $this->addVaccinationstoDatabase($vaccinations,$office_id);

            $loopCount = ceil($totalCount/$limit);

            for($i=0;$i<$loopCount;$i++)
            {
                $offset = ($i+1);

                $percentage = ($i/$loopCount);

                $percentage = ceil($percentage*100);

                $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
                $detailsecondjson = $this->fetchVaccinations($request_key,$office_practiceid[1],$offset,$limit);
                $detail = json_decode($detailsecondjson);
                if(is_array($detail) && is_array($detail[0]->items))
                {
                    $vaccinations = $detail[0]->items;
                    $this->addVaccinationstoDatabase($vaccinations,$office_id);
                }
            }

            $data = array('vaccinationadded' => 1);

            $result = $this->vendorsmodel->updateVendor($data,$vendorid);

        }

    }

    private function fetchGuarantors($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/guarantors?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }
    private function fetchVeterinary_patients($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/patients/veterinary_patients?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }
    private function fetchPrescriptions($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/prescriptions?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);

    }
    private function fetchVaccinations($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/reminders?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);

    }

    function updatepracticetoBizbark($sikkapracticeofficeid)
    {


        ini_set('max_execution_time', 18000);
        //$sikkapracticeofficeid = $this->input->post('practice_id');

        $office_practiceid = explode("_", $sikkapracticeofficeid);

        $vendor_id = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);


        $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);


        $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);


        $sikkapracticesurl = "https://api.sikkasoft.com/v2/practices/".$office_practiceid[1]."?request_key=".$request_key;
        $detailjson = file_get_contents($sikkapracticesurl);

        $practice = json_decode($detailjson);

        $address = $practice->address_line1." ".$practice->address_line2." ".$practice->city." ".$practice->state." ".$practice->country." ".$practice->zipcode;

        $result = $this->geocode($address);

        if($result)
        {
            $latlng = $result[0].",".$result[1];
        }
        else{
            $latlng = "";
        }

        $datacc = array(
            //'password' => md5($this->input->post('password'))
            'firstname' => "Sikka",
            'lastname' => "Vet",
            'comapnyname' => $practice->name,
            'address' => $practice->address_line1." ".$practice->address_line2,
            'city' => $practice->city,
            'state' => $practice->state,
            'country' => $practice->country,
            'zipcode' => $practice->zipcode,
            'contact'=>$practice->phone,
            'website'=>$practice->website,
            'agreedterms' => 1,
            'createdon' =>date('Y-m-d H:i'),
            'addresslatlng'=>$latlng
        );

        if($practice->email)
        {
            $datacc['email'] = $practice->email;
        }

        $result = 	$this->vendorsmodel->updateVendor($datacc,$vendor_id['vendorid']);

        $offset = 0;
        $limit = 5000;

        $detailjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

        $veterinary_patients = array();

        if($detailjson)
        {
            $detail = json_decode($detailjson);
            $totalCount = intval($detail[0]->total_count);

            $veterinary_patients = $detail[0]->items;



            ///////////////////// LOOP //////////////////////

            $loopCount = ceil($totalCount/$limit);


            for($i=0;$i<$loopCount;$i++)
            {
                $offset = ($i+1);
                $detailsecondjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

                $detail = json_decode($detailsecondjson);
                if(is_array($detail) && is_array($detail[0]->items))
                {
                    $veterinary_patients = array_merge($veterinary_patients,$detail[0]->items);

                }

            }


            $finalVeterinary_patients = array();
            $vetPatients_patient_id = array();

            for($i=0;$i<count($veterinary_patients);$i++)
            {
                $specie = $veterinary_patients[$i]->species;

                $specie = strtolower($specie);
                if($specie=='canine')
                {
                    array_push($finalVeterinary_patients, $veterinary_patients[$i]);
                    array_push($vetPatients_patient_id, $veterinary_patients[$i]->patient_id);

                }
            }




            $offset = 0;
            $limit = 5000;

            //$request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);

            $guarantors = array();
            $detailjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);

            if($detailjson)
            {
                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);
                $guarantors = $detail[0]->items;


                ///////////////////// LOOP //////////////////////
                if($totalCount>$limit)
                {
                    $loopCount = ceil($totalCount/$limit);
                    for($i=0;$i<$loopCount;$i++)
                    {
                        $offset = ($i+1);
                        $detailsecondjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);
                        $detail = json_decode($detailsecondjson);
                        if(is_array($detail) && is_array($detail[0]->items))
                        {
                            $guarantors = array_merge($guarantors,$detail[0]->items);
                        }
                    }
                }


                $finalGuarantors = array();
                $finalGuarantorsGuarantorid = array();

                $finalPets = array();

                for($i=0;$i<count($guarantors);$i++)
                {
                    if(in_array($guarantors[$i]->patient_id, $vetPatients_patient_id))
                    {
                        if(!in_array($guarantors[$i]->guarantor_id, $finalGuarantorsGuarantorid))
                        {
                            array_push($finalGuarantors,$guarantors[$i]);
                            array_push($finalGuarantorsGuarantorid,$guarantors[$i]->guarantor_id);
                        }
                    }
                }

                $this->addGuarantortoDatabase($finalGuarantors,$vendor_id['vendorid']);

                $this->addVetPatientstoDatabase($finalVeterinary_patients);

                $customers_of_this_vendor = $this->customermodel->getAllCustomers($vendor_id['vendorid']);

                for($c=0;$c<count($customers_of_this_vendor);$c++)
                {

                    $g_id = $customers_of_this_vendor[$c]['guarantor_id'];

                    $c_id = $customers_of_this_vendor[$c]['customerid'];
                    $pet_ids = $this->petmodel->getCustomerPets($g_id);

                    for ($i=0; $i <count($pet_ids) ; $i++) {

                        $data = array('customerid' => $c_id,
                            'petid' => $pet_ids[$i]['petid']
                        );

                        $this->customermodel->updatepet($data,$c_id,$pet_ids[$i]['petid']);
                    }

                }

            }
        }

    }

    // function to geocode address, it will return false if unable to geocode address
    function geocode($address){

        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                $data_arr = array();

                array_push(
                    $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
                );

                return $data_arr;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    private static function genratePrimaryKey($serverid)
    {
        $primarykey=$serverid.time().mt_rand(1000, 9999);
        if($primarykey>9223372036854775807) //max of 64 bit int
        {
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }

    function getAllSikkaVendors(){
        $sql = "select * from vendor v where v.sikkaoffice_id in (select s.sikkaofficeid from sikkaoffices s)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function updateSikkaVendor($data,$sikkaofficeId){
        $this->db->where('sikkaoffice_id',$sikkaofficeId);
        $this->db->update('vendor',$data);
        return $this->db->affected_rows();
    }

    function updatePrescriptions($data){
        $sql = "select * from petprescriptionssikka p 
               where p.description='{$data['description']}' 
               and p.quantity='{$data['quantity']}' 
               and p.sikka_prescription_id='{$data['sikka_prescription_id']}' 
               and p.patient_id='{$data['patient_id']}'";
        $query = $this->db->query($sql);
        return  $query->num_rows();
    }

}
?>