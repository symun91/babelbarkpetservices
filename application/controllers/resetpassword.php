<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resetpassword extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('appusersmodel'));
	}

	function index()
	{
		$data['msg'] = '';
		$data['token'] = $this->input->get_post('token');
		if(!empty($_POST)){
			
			$newPassword = $this->input->post('pwd_new');
			$cpwd_new = $this->input->post('cpwd_new');
			$token = $this->input->post('token');
			if($newPassword == $cpwd_new){
				$userId = $this->appusersmodel->getAppUserByResetToken($token);
				if(!empty($userId) && !empty($token)){
					$updateArr = array();
					$updateArr['resetpasswordtoken'] = '';
					$updateArr['password'] = md5($newPassword);
					$responce = $this->appusersmodel->updateUser($updateArr,$userId['appuserid']);
					if($responce){
						$data['msg'] = 'Password update succesfully.';
					} else {
						$data['msg'] = 'Something is wrong.';	
					}
				} else {
					$data['msg'] = 'Reset password link is invalid.';	
				}
				$updateArr = array();
			} else {
				$data['msg'] = 'New password and confirm password dose not match.';
			}

		}

		$this->load->view('resetpassword.php',$data);
	}
}

