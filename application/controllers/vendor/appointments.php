<?php

/**
 * @property appointmentmodel $appointmentmodel
 * @property servicemodel  $servicemodel
 * @property customermodel  $customermodel
 * @property appuservendorsmodel  $appuservendorsmodel
 * @property appusersmodel  $appusersmodel
 * @property vendorsmodel  $vendorsmodel
 */
class Appointments extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('appointmentmodel');
        $this->load->model('customermodel');
        $this->load->model('petmodel');
        $this->load->model('servicemodel');
        $this->load->model('petbreedmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('appusersmodel');
        $this->load->model('employeemodel');
        $this->load->model('vendorsmodel');
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $appusers = array();
        $customers = $this->customermodel->getAllCustomers($vendorid);
        for ($i = 0; $i < count($customers); $i++) {
            $customers[$i]['usertype'] = 'customer';
        }
        $babelbarkusers = $this->appuservendorsmodel->getUsersForVendor($vendorid);
        for ($i = 0; $i < count($babelbarkusers); $i++) {
            $appuserid = $babelbarkusers[$i]['appuserid'];
            $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
            $appuser['usertype'] = 'appuser';
            $appusers[$i] = $appuser;
        }
        $commonArray = array_merge($customers, $appusers);
        sort($commonArray);
        $this->customers = $commonArray;
        $this->pets = array();
        $petData = array();
        for ($i = 0; $i < count($this->customers); $i++) {
            if ($this->customers[$i]['usertype'] == 'customer') {
                $customerid = $this->customers[$i]['customerid'];
                $userpets = $this->customermodel->getPets($customerid);
                if ($userpets > 1) {
                    $userpetid = $this->customermodel->getPetIDs($customerid);
                    for ($j = 0; $j < $userpets; $j++) {
                        $petdetail = $this->petmodel->getPetDetails($userpetid[$j]['petid']);
                        $petData[$j] = $petdetail;
                    }
                    $this->pets[$i] = $petData;
                } else {
                    $userpetid = $this->customermodel->getPetID($customerid);
                    $petdetail = $this->petmodel->getPetDetails($userpetid);
                    $this->pets[$i] = $petdetail;
                }
            } else {
                $appuserid = $this->customers[$i]['appuserid'];
                $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
                $appuserpets = $this->appusersmodel->getPets($appuserid);
                if ($appuserpets > 1) {
                    $userpetid = $this->appusersmodel->getPetIDs($appuserid);
                    for ($j = 0; $j < $appuserpets; $j++) {
                        $petdetail = $this->petmodel->getPetDetails($userpetid[$j]['petid']);
                        $petData[$j] = $petdetail;
                    }
                    $this->pets[$i] = $petData;
                } else {
                    $userpetid = $this->appusersmodel->getPetID($appuserid);
                    $petdetail = $this->petmodel->getPetDetails($userpetid);
                    $this->pets[$i] = $petdetail;
                }
            }
        }
        $this->vendorEmployees = $this->employeemodel->getEmployeeList($vendorid); #get employee list using vendor Id
        $this->services = $this->servicemodel->getAllServices1($vendorid);
    }

    function newAppointments() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $newappointments = $this->appointmentmodel->getNewAppointments($vendorid);
        if (count($newappointments) > 0) {
            $data['newappointments'] = $newappointments;
            $this->load->view('vendor/maincontents/appointment_notifications', $data);
        } else
            echo "nodata";
    }

    function updateAppointmentStatusToRead() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $status = $this->input->post('status');
        $this->appointmentmodel->updateReadStatus($vendorid);
        echo 1;
    }

    function listofappointments() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $data = array();
        $resultapp = $this->appointmentmodel->getAllappointmentsofvendor($vendorid);
        for ($i = 0; $i < count($resultapp); $i++) {
            $breedname = "";
            $breedid = $resultapp[$i]['petbreed'];
            if ($breedid != null && $breedid != "") {
                $breedname = $this->petbreedmodel->getBreedTitle($breedid);
            }
            $resultapp[$i]['breedname'] = $breedname;
        }
        $data['appointments'] = $resultapp;
        $data['unscheduledappointments'] = $this->appointmentmodel->getAllUnscheduledAppointments($vendorid);
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/listofappointments', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function declineAppointment($appointmentid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $appointmentDetails = $this->appointmentmodel->getAppointmentInfo($appointmentid);
        if ($appointmentDetails) {
            $subject = "Appointment Request Declined";
            $toemail = $appointmentDetails['appuseremail'];
            // To send HTML mail, the Content-type header must be set.
            $sendmessage = file_get_contents('resources/appointment_decline_notification_template.html');
            $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
            $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
            $sendmessage = str_replace('%fromdate%', date("M d, Y", strtotime($appointmentDetails['boardingfromdate'])), $sendmessage);
            $sendmessage = str_replace('%fromtime%', date("h:i A", strtotime($appointmentDetails['boardingfromtime'])), $sendmessage);
            $sendmessage = str_replace('%totime%', date("h:i A", strtotime($appointmentDetails['boardingtotime'])), $sendmessage);
            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->subject($subject);
            $this->email->message($sendmessage);
            $result = $this->email->send();
            if (!$result) {
                $result = $this->email->print_debugger();
            }
            $this->appointmentmodel->deleteAppointment($appointmentid);
            $msg = "Appointment declined successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/appointments/listofappointments');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function addappointment() {
        $data = array();
        $data['appointmentdetail'] = array(
            'appointmentid' => 0,
            'boardingfromdate' => "",
            'boardingtodate' => "",
            'boardingfromtime' => "",
            'boardingtotime' => "",
            'estimatedduration' => "",
            'durationunit' => "hours",
            'customerid' => "",
            'petid' => "",
            'serviceid' => "",
            'userid' => "",
            'pickdrop' => "",
            'reminderrequired' => "",
            'bookingnote' => "",
            'appuserid' => "",
            'customertype' => "",            
            'status' => 0
        );
        $data['serviceInfo'] = array(
            'duration' => "",
        );
        $data['vendorEmployees'] = $this->vendorEmployees;
        $data['customers'] = $this->customers;
        $data['pets'] = $this->pets;
        $data['services'] = $this->services;
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/addappointment', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function viewDetails($appointmentid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $appointmentstatus = $this->appointmentmodel->appointmentStatus($vendorid, $appointmentid);
        if ($appointmentstatus == TRUE) {
            $data = array();
            $appointmentdetail = $this->appointmentmodel->getAppointmentDetails($appointmentid);
            if ($appointmentdetail['appuserid'] == "" || $appointmentdetail['appuserid'] == null) {
                $appointmentdetail['customertype'] = 'customer';
                $customerId = $appointmentdetail['customerid'];
                $petInfo = $this->customermodel->getPetInfo($customerId);
            } else {
                $appointmentdetail['customertype'] = 'appuser';
                $AppuserId = $appointmentdetail['appuserid'];
                $petInfo = $this->appusersmodel->getPetInfo($AppuserId);
            }
            $serviceId = $appointmentdetail['serviceid'];            
            $data['serviceInfo'] = $this->servicemodel->getServiceDetails($serviceId);
            $data['appointmentdetail'] = $appointmentdetail;
            $data['customers'] = $this->customers;
            $data['pets'] = $this->pets;
            $data['petInfo'] = $petInfo;
            $data['services'] = $this->services;
            $data['vendorEmployees'] = $this->vendorEmployees;
            $this->data['maincontent'] = $this->load->view('vendor/maincontents/addappointment', $data, true);
            $this->load->view('vendor/layout', $this->data);
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function getAllPets() {
        $userType = $_POST['userType'];
        $newCustomer = $_POST['newCustomer'];
        if ($userType == 'customer') {
            $petInfo = $this->customermodel->getPetInfo($newCustomer);
        } else {
            $petInfo = $this->appusersmodel->getPetInfo($newCustomer);
        }
        foreach ($petInfo as $petRow) {
            echo '<option value=' . $petRow['petid'] . '>' . $petRow['name'] . '</option>';
        }
    }

    function updateAppointment() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $appointmentid = $this->input->post('appointmentid');
        $bookingnote = (trim($this->input->post('bookingnote')));
        $boardingfromdate = date('Y-m-d', strtotime(trim($this->input->post('startdate'))));
        $boardingtodate = date('Y-m-d', strtotime(trim($this->input->post('enddate'))));
        $boardingfromtime = $this->input->post('starttime');
        $boardingfromtime = explode(' ', $boardingfromtime);
        $boardingtotime = $this->input->post('endtime');
        $boardingtotime = explode(' ', $boardingtotime);
        if ($boardingfromtime[1] == "AM" && $boardingtotime[1] == "PM") {
            $boardingfromtime1 = $boardingfromtime[0];
            $boardingtotime1 = explode(":", $boardingtotime[0]);
            if ($boardingtotime1[0] != 12) {
                $boardingtotime1[0] = $boardingtotime1[0] + 12;
                $boardingtotime1 = $boardingtotime1[0] . ":" . $boardingtotime1[1];
            } else {
                $boardingtotime1 = $boardingtotime1[0] . ":" . $boardingtotime1[1];
            }
            /* print_r($boardingfromtime1);
              print_r($boardingtotime1); */
        } else if ($boardingfromtime[1] == "PM" && $boardingtotime[1] == "PM") {
            $boardingfromtime1 = explode(":", $boardingfromtime[0]);
            if ($boardingfromtime1[0] != 12) {
                $boardingfromtime1[0] = $boardingfromtime1[0] + 12;
                $boardingfromtime1 = $boardingfromtime1[0] . ":" . $boardingfromtime1[1];
            } else {
                $boardingfromtime1 = $boardingfromtime1[0] . ":" . $boardingfromtime1[1];
            }
            $boardingtotime1 = explode(":", $boardingtotime[0]);
            if ($boardingtotime1[0] != 12) {
                $boardingtotime1[0] = $boardingtotime1[0] + 12;
                $boardingtotime1 = $boardingtotime1[0] . ":" . $boardingtotime1[1];
            } else {
                $boardingtotime1 = $boardingtotime1[0] . ":" . $boardingtotime1[1];
            }
            /* print_r($boardingfromtime1);
              print_r($boardingtotime1); */
        } else if ($boardingfromtime[1] == "PM" && $boardingtotime[1] == "AM") {
            $boardingfromtime1 = explode(":", $boardingfromtime[0]);
            if ($boardingfromtime1[0] != 12) {
                $boardingfromtime1[0] = $boardingfromtime1[0] + 12;
                $boardingfromtime1 = $boardingfromtime1[0] . ":" . $boardingfromtime1[1];
            } else {
                $boardingfromtime1 = $boardingfromtime1[0] . ":" . $boardingfromtime1[1];
            }
            $boardingtotime1 = $boardingtotime[0];
        } else {
            $boardingfromtime1 = $boardingfromtime[0];
            $boardingtotime1 = $boardingtotime[0];
        }
        $estimduration = (trim($this->input->post('estimatedduration')));
        $durationunit = $this->input->post('durationunit');
        $customerid = $this->input->post('selectcustomerid');
        $petid = $this->input->post('petid');
        $serviceid = $this->input->post('service');
        $userid = $this->input->post('userid');
        $pickdrop = $this->input->post('pickdrop');
        $reminderneeded = $this->input->post('reminderneeded');
        $status = $this->input->post('status');
        $customertype = $this->input->post('customertype');
        $date = new DateTime($boardingfromdate . ' ' . $boardingfromtime[0]);
        $Fromtime = $date->format('Y-m-d H:i:s');
        //echo $estimduration;
        if (!$estimduration) {
            $estimduration = 1;
        }
        if (!$durationunit) {
            $durationunit = 'hours';
        }
        if ($durationunit == 'hours') {
            $date->add(new DateInterval("PT{$estimduration}H"));
        } else if ($durationunit == 'days') {
            $date->add(new DateInterval("P{$estimduration}D"));
        } else if ($durationunit == 'minutes') {
            $date->add(new DateInterval("PT{$estimduration}M"));
        }
        $Totime = $date->format('Y-m-d H:i:s');
        $datacc = array(
            'vendorid' => $vendorid,
            'appointmentid' => $appointmentid,
            'boardingfromdate' => $boardingfromdate,
            'boardingtodate' => $boardingtodate,
            'boardingfromtime' => $boardingfromtime1,
            'boardingtotime' => $boardingtotime1,
            'estimatedduration' => $estimduration,
            'durationunit' => $durationunit,
            'petid' => $petid,
            'serviceid' => $serviceid,
            'userid' => $userid,
            'pickdrop' => $pickdrop,
            'reminderrequired' => $reminderneeded,
            'bookingnote' => $bookingnote
        );
        if ($customertype == "customer") {
            $datacc['customerid'] = $customerid;
            $datacc['appuserid'] = null;
        } else {
            $datacc['appuserid'] = $customerid;
            $datacc['customerid'] = null;
        }
        if ($appointmentid == 0)
            $datacc['status'] = 3;
        if ($status == 4) {
            $datacc['status'] = 2;
        }
        $serviceData = $this->servicemodel->getServiceDetails($serviceid);
        $appointmentid = $this->appointmentmodel->updateAppointment($datacc, $appointmentid);
        //send email notification to appuser
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $vendorcompanyname = $admin['adminDetails']['comapnyname'];
        $vendorlocation = $admin['adminDetails']['zipcode'] . '%20' . $admin['adminDetails']['city'] . '%20' . $admin['adminDetails']['state'] . '%20' . $admin['adminDetails']['country'];
        $appuserid = $customerid;

        if ($admin['adminDetails']['category'] == 1) {
            $siteAddress = 'Babelvet';
            $logoName = 'logo_vet.png';
        }else{
            $siteAddress = 'Bizbark';
            $logoName = 'logo.png';
        }

        $appuserdata = $this->appusersmodel->fetchUserDetails($appuserid);
        $toemail = $appuserdata['email'];
        $subject = "Appointment Scheduled";
        // To send HTML mail, the Content-type header must be set.
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From:' . $vendoremail . "\r\n"; // Sender's Email
        $sendmessage = file_get_contents('resources/appointment_notification_template.html');
        $sendmessage = str_replace('%userfname%', $appuserdata['firstname'], $sendmessage);
        $sendmessage = str_replace('%userlname%', $appuserdata['lastname'], $sendmessage);
        $sendmessage = str_replace('%servicename%',$serviceData['name'] , $sendmessage);
        $sendmessage = str_replace('%fromdate%', date("M d, Y", strtotime($boardingfromdate)), $sendmessage);
        $sendmessage = str_replace('%fromtime%', date("h:i A", strtotime($boardingfromtime1)), $sendmessage);
        $sendmessage = str_replace('%totime%', date("h:i A", strtotime($boardingtotime1)), $sendmessage);
        $sendmessage = str_replace('%sitename%', $siteAddress, $sendmessage);
        $sendmessage = str_replace('%logoname%', $logoName, $sendmessage);
        $sendmessage = str_replace('%lfromtime%',$Fromtime,$sendmessage);
        $sendmessage = str_replace('%ltotime%', $Totime, $sendmessage);
        $sendmessage = str_replace('%companyname%', $vendorcompanyname, $sendmessage);
        $sendmessage = str_replace('%vendorlocation%', $vendorlocation, $sendmessage);
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = SITE_MAIL;
        $config['smtp_pass'] = "doogyWOOGY!";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
        $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
        $this->email->to($toemail);
        $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
        //$this->email->cc($vendoremail, $vendorfname);
        $this->email->subject($subject);
        $this->email->message($sendmessage);
        $result = $this->email->send();

        $this->session->set_flashdata('success_message', 'Appointment updated.');
        redirect('vendor/appointments/listofappointments');
    }

    function deleteAppointment($appointmentid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $appointmentstatus = $this->appointmentmodel->appointmentStatus($vendorid, $appointmentid);
        if ($appointmentstatus == TRUE) {
            $this->appointmentmodel->deleteAppointment($appointmentid);
            $msg = "Appointment deleted successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/appointments/listofappointments');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function validateAppointmentRule() {
        $retval = "valid";
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $serviceid = $this->input->post('service');
        $btnValue = $this->input->post('btnValue');
        $appointmentid = $this->input->post('appointmentid');
        $startdate = date('Y-m-d', strtotime(trim($this->input->post('startdate'))));
        $starttime = $this->input->post('starttime');
        $endtime = $this->input->post('endtime');
        $boardingfromtime = date("H:i:s", strtotime($starttime));
        $boardingtotime = date("H:i:s", strtotime($endtime));
        $servicedetail = $this->servicemodel->getServiceDetails($serviceid);
        $serviceavaildays = $servicedetail['availabilitydays'];
        $serviceavailfrmhr = $servicedetail['availabilityfromhour'];
        $serviceavailtohr = $servicedetail['availabilitytohour'];
        $capacity = $servicedetail['capacity'];
        $maximumNotice = $servicedetail['maximum_notice'];
        $weekdays = array('Monday' => 'M', 'Tuesday' => 'T', 'Wednesday' => 'W', 'Thursday' => 'Th', 'Friday' => 'F', 'Saturday' => 'Sa', 'Sunday' => 'Su');
        $selectedDay = date('l', strtotime($startdate));
        $selectedDayVal = $weekdays[$selectedDay];
        $availdaysarr = explode(',', $serviceavaildays);
        $availfrmhrarr = explode(',', $serviceavailfrmhr);
        $availtohrarr = explode(',', $serviceavailtohr);
        $serviceselday_availfromtime = "";
        $serviceselday_availtotime = "";
        //if(in_array($selectedDayVal,$availdaysarr))
        for ($i = 0; $i < count($availdaysarr); $i++) {
            if ($availdaysarr[$i] == $selectedDayVal) {
                $retval = "valid";
                if (count($availfrmhrarr) > $i) {
                    $serviceselday_availfromtime = $availfrmhrarr[$i];
                }
                if (count($availtohrarr) > $i) {
                    $serviceselday_availtotime = $availtohrarr[$i];
                }
                break;
            }
        }
        if ($retval == "valid") {
            if ($serviceselday_availfromtime != "" && $serviceselday_availfromtime != 0) {
                if (strtotime($serviceselday_availfromtime) <= strtotime($starttime)) {
                    if ($serviceselday_availtotime != "" && $serviceselday_availtotime != 0) {
                        if (strtotime($serviceselday_availtotime) >= strtotime($endtime)) {
                            $retval = "valid";
                        } else {
                            $retval = "End Time is outside of service availability.";
                        }
                    } else {
                        $retval = "valid";
                    }
                } else {
                    $retval = "Start Time is outside of service availability.";
                }
            } else {
                $retval = "valid";
            }
        } else {
            $retval = "Entered Start Date is not available for the service.";
        }
        /* if($retval=="valid"){
          if(strtotime($startdate) > strtotime("+$maximumNotice weeks")) {
          $retval = "Selected date must be in between $maximumNotice weeks";
          }
          }
         * 
         */
        if ($retval == "valid") {
            $newBoardingtotime = date("H:i:s", strtotime("-1 minutes", strtotime($boardingtotime)));
            $appointmentInfo = $this->appointmentmodel->getNoOfAppointment($vendorid, $serviceid, $startdate, $boardingfromtime, $newBoardingtotime);
            $count = count($appointmentInfo);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $noOfAppointment = $appointmentInfo[$i]['total_appointment'];
                    $AppBoardingFromDate = $appointmentInfo[$i]['boardingfromdate'];
                    $AppBoardingFromTime = $appointmentInfo[$i]['boardingfromtime'];
                    if($btnValue=='Update'){
                        if($AppBoardingFromDate == $startdate && $AppBoardingFromTime == $boardingfromtime){
                            $noOfAppointment = $noOfAppointment-1; // Removing the updating appointment from the count
                        }else{
                            $noOfAppointment = $noOfAppointment;
                        }
                    }
                    if ($noOfAppointment >= $capacity) {
                        $retval = "The maximum capacity for this service has been reached for this time slot.";
                        break;
                    }
                }
            }
        }
        echo $retval;
    }

    public function check_status() {
        $serviceid = $this->input->post('serviceid');
        $serviceQuery = $this->appointmentmodel->getAssignEmployeeStatus($serviceid);
        if ($serviceQuery):
            echo '1';
        else:
            echo '0';
        endif;
    }
    
    public function getServiceTime() {
        $serviceid = $this->input->post('serviceid');
        $startdayname = $this->input->post('startdayname');
        $serviceInfo = $this->servicemodel->getServiceDetails($serviceid);
        $serviceDays = explode(',', $serviceInfo['availabilitydays']);
        $dayPosition = array_search($startdayname, $serviceDays);
        if ($dayPosition > -1) {
            $serviceFromHour = explode(',', $serviceInfo['availabilityfromhour']);
            $dayStartTime = $serviceFromHour[$dayPosition];

            $serviceToHour = explode(',', $serviceInfo['availabilitytohour']);
            $dayEndTime = $serviceToHour[$dayPosition];
            $array = array(
                'dayStartTime' => date('h:i A', strtotime($dayStartTime)),
                'dayEndTime' => date('h:i A', strtotime($dayEndTime))
            );
            echo json_encode($array);
        } else {
            $array = array(
                'dayStartTime' => date('h:i A'),
                'dayEndTime' => date('h:i A')
            );
            echo json_encode($array);
        }
    }

}
