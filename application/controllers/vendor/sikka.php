<?php
/**
 * @property appusersmodel               $appusersmodel
 * @property appuserdevicesmodel         $appuserdevicesmodel
 * @property petmodel                    $petmodel
 * @property petmedicationmodel          $petmedicationmodel
 * @property petvaccinationmodel         $petvaccinationmodel
 * @property petgoalsmodel               $petgoalsmodel
 * @property petlogmodel                 $petlogmodel
 * @property veterinarianmodel           $veterinarianmodel
 * @property petfeedmodel                $petfeedmodel
 * @property petbreedmodel               $petbreedmodel
 * @property vendorsmodel                $vendorsmodel
 * @property appuservendorsmodel         $appuservendorsmodel
 * @property promotionsmodel             $promotionsmodel
 * @property mailnotifymodel             $mailnotifymodel
 * @property nonregistervendormodule     $nonregistervendormodule
 * @property appointmentmodel            $appointmentmodel
 * @property servicemodel                $servicemodel
 * @property customermodel               $customermodel
 * @property medicalfilesmodel           $medicalfilesmodel
 * @property appuserpromotionmodel       $appuserpromotionmodel
 * @property sikkaofficemodel            $sikkaofficemodel
 * @property vendorcategoriesmodel       $vendorcategoriesmodel
 */
class sikka extends CI_Controller
{
    var $data=array();

    function __construct()
    {
        parent::__construct();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('petbreedmodel');
        $this->load->model('promotionsmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('appusersmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('appuserpromotionmodel');
        $this->load->model('customermodel');
        $this->load->model('sikkaofficemodel');

        $this->locations=array('BabelBark App', 'Social Media','Craigslist', 'Local Classifieds', 'Fliers', 'Website', 'Other');


        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);
        $categorystr=$vendordetails['category'];
        $selcategarr=explode(",", $categorystr);
        $servicetypes=$this->vendorcategoriesmodel->getVendorCategories();
        $this->vendorscateg=array();
        for($i=0;$i<count($servicetypes);$i++)
        {
            $categid=$servicetypes[$i]['categoryid'];
            if(in_array($categid, $selcategarr))
            {
                $this->vendorscateg[]=$servicetypes[$i];
            }

        }
    }

    private static function genratePrimaryKey($serverid)
    {
        $primarykey=$serverid.time().mt_rand(1000, 9999);
        if($primarykey>9223372036854775807) //max of 64 bit int
        {
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }
    function importFromSikka()
    {
        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $email = $admin['adminDetails']['email'];
        $data = array();
        $this->data['maincontent']=$this->load->view('vendor/maincontents/sikka-frontend',$data,true);
        $this->load->view('vendor/layout',$this->data);
    }

    private function getrequestid($sikkaofficeid,$secret_key)
    {
        $sikkarequestkey = "https://api.sikkasoft.com/v2/start?app_id=".APP_ID."&app_key=".APP_KEY."&office_id=".$sikkaofficeid."&secret_key=".$secret_key;
        $detailjson = file_get_contents($sikkarequestkey);
        $requestkey = json_decode($detailjson);
        return $requestkey[0]->request_key;
    }


    function getVendorDetails()
    {
        set_time_limit ( 4000);
        $admin=$this->session->userdata('admin');
        $email = $admin['adminDetails']['email'];
        $vendorid=$admin['adminDetails']['vendorid'];

        $arr_content = array();

        $filename_path = 'tmp/progress'.$vendorid.'.txt';

        $arr_content['percent'] = 0;
        $arr_content['message'] = "Started";


        file_put_contents($filename_path, json_encode($arr_content));


        $office_practiceid = $this->vendorsmodel->getVendorSikkaOfficeId($vendorid);

        if(is_null($office_practiceid['sikkaoffice_id']))
        {
            echo json_encode(0);
            return;
        }

        $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid['sikkaoffice_id']);
        $request_key = $this->getrequestid($office_practiceid['sikkaoffice_id'],$secret_key['secret_key']);

        $arr_content['percent'] = 5;
        $arr_content['message'] = "Fetching Pets";


        file_put_contents($filename_path, json_encode($arr_content));


        $offset = 0;
        $limit = 500;
        $detailjson = $this->fetchVeterinary_patients($request_key,$office_practiceid['sikkapractice_id'],$offset,$limit);
        $veterinary_patients = array();

        if($detailjson)
        {
            $detail = json_decode($detailjson);
            $totalCount = intval($detail[0]->total_count);

            $veterinary_patients = $detail[0]->items;

            $arr_content['percent'] = 10;
            $arr_content['message'] = "Fetching Pets";


            file_put_contents($filename_path, json_encode($arr_content));




            ///////////////////// LOOP SKIPPED//////////////////////
            $loopCount = ceil($totalCount/$limit);

            for($i=0;$i<$loopCount;$i++)
            {
                $offset = ($i+1);
                $detailsecondjson = $this->fetchVeterinary_patients($request_key,$office_practiceid['sikkapractice_id'],$offset,$limit);

                $detail = json_decode($detailsecondjson);
                if(is_array($detail) && is_array($detail[0]->items))
                {
                    $veterinary_patients = array_merge($veterinary_patients,$detail[0]->items);

                }

            }

            $arr_content['percent'] = 20;
            $arr_content['message'] = "Processing Pets";


            file_put_contents($filename_path, json_encode($arr_content));

            $finalVeterinary_patients = array();

            $vetPatients_patient_id = array();


            for($i=0;$i<count($veterinary_patients);$i++)
            {
                $specie = $veterinary_patients[$i]->species;

                $specie = strtolower($specie);
                if($specie=='canine')
                {
                    array_push($finalVeterinary_patients, $veterinary_patients[$i]);
                    array_push($vetPatients_patient_id, $veterinary_patients[$i]->patient_id);
                }
            }



            $offset = 0;
            $limit = 1000;

            $arr_content['percent'] = 30;
            $arr_content['message'] = "Fetching Customers";


            file_put_contents($filename_path, json_encode($arr_content));

            $request_key = $this->getrequestid($office_practiceid['sikkaoffice_id'],$secret_key['secret_key']);

            $guarantors = array();
            $detailjson = $this->fetchGuarantors($request_key,$office_practiceid['sikkapractice_id'],$offset,$limit);
            if($detailjson)
            {
                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);
                $guarantors = $detail[0]->items;

                $arr_content['percent'] = 40;
                $arr_content['message'] = "Fetching Customers";


                file_put_contents($filename_path, json_encode($arr_content));


                //echo json_encode($guarantors[0]);
                //$this->addGuarantortoDatabase($guarantors,$vendorid);
                ///////////////////// LOOP SKIPPED//////////////////////
                if($totalCount>$limit)
                {
                    $loopCount = ceil($totalCount/$limit);
                    for($i=0;$i<$loopCount;$i++)
                    {
                        $offset = ($i+1);
                        $detailsecondjson = $this->fetchGuarantors($request_key,$office_practiceid['sikkapractice_id'],$offset,$limit);
                        $detail = json_decode($detailsecondjson);
                        if(is_array($detail) && is_array($detail[0]->items))
                        {
                            $guarantors = array_merge($guarantors,$detail[0]->items);
                        }
                    }
                }

                $finalGuarantors = array();

                $finalPets = array();

                $arr_content['percent'] = 50;
                $arr_content['message'] = "Processing Customers";


                file_put_contents($filename_path, json_encode($arr_content));

                /*	for($i=0;$i<count($guarantors);$i++)
                    {
                        for($j=0;$j<count($finalVeterinary_patients);$j++)
                        {
                            if($guarantors[$i]->guarantor_id == $finalVeterinary_patients[$j]->guarantor_id)
                            {
                                array_push($finalGuarantors,$guarantors[$i]);
                            }
                        }
                    }*/

                for($i=0;$i<count($guarantors);$i++)
                {
                    if(in_array($guarantors[$i]->patient_id, $vetPatients_patient_id))
                    {
                        array_push($finalGuarantors,$guarantors[$i]);
                    }
                }

                $arr_content['percent'] = 60;
                $arr_content['message'] = "Updating Customers in database";


                file_put_contents($filename_path, json_encode($arr_content));

                $this->addGuarantortoDatabase($finalGuarantors,$vendorid);
                $arr_content['percent'] = 75;
                $arr_content['message'] = "Updating Customers in database";


                file_put_contents($filename_path, json_encode($arr_content));
                $this->addVetPatientstoDatabase($finalVeterinary_patients);

                $arr_content['percent'] = 90;
                $arr_content['message'] = "Customer and Pet relation";


                file_put_contents($filename_path, json_encode($arr_content));

                for($c=0;$c<count($customers_of_this_vendor);$c++)
                {

                    $g_id = $customers_of_this_vendor[$c]['guarantor_id'];
                    $p_id = $customers_of_this_vendor[$c]['patient_id'];
                    $c_id = $customers_of_this_vendor[$c]['customerid'];
                    $pet_id = $this->petmodel->getCustomerPet($g_id,$p_id);

                    $data = array('customerid' => $c_id,
                        'petid' => $pet_id['petid']
                    );

                    $this->customermodel->updatepet($data,$c_id,$pet_id['petid']);

                }

                $arr_content['percent'] = 100;
                $arr_content['message'] = "Updated";


                file_put_contents($filename_path, json_encode($arr_content));

                echo json_encode("Data Updated Successfully.");
            }
            else{
                echo json_encode("Sikka Api fetching problem. No Customers found.");
            }

        }else{
            echo json_encode("Sikka Api fetching problem. No Pets found.");
        }

    }

    function getProgress()
    {
        ini_set('max_execution_time', 2000);

        //$file = str_replace(".", "", $_GET['file']);

        $admin=$this->session->userdata('admin');

        $vendorid=$admin['adminDetails']['vendorid'];

        $arr_content = array();

        $filename_path = 'tmp/progress'.$vendorid.'.txt';
        $isfirsttime = $this->input->post('isfirst');
        $file = $filename_path;
        if($isfirsttime == "true")
        {
            if(file_exists($file))
            {
                unlink($file);
            }
            $arr_content['percent'] = 0;
            $arr_content['message'] = "Started";


            file_put_contents($filename_path, json_encode($arr_content));
            echo json_encode(array("percent" => 0, "message" => "File deleted"));
            return;
        }
        // Make sure the file is exist.
        if (file_exists($file)) {
            // Get the content and echo it.
            $text = file_get_contents($file);
            echo $text;


            // Convert to JSON to read the status.
            $obj = json_decode($text);
            // If the process is finished, delete the file.
            if ($obj->percent == 100) {
                unlink($file);
            }
        }
        else {
            $arr_content = array();

            $arr_content['percent'] = 0;
            $arr_content['message'] = "Started";


            file_put_contents($filename_path, json_encode($arr_content));
            echo json_encode(array("percent" => 0, "message" => "File not created"));
        }
        //echo json_encode($this->session->userdata('progress_practice'));
    }


    private function addGuarantortoDatabase($guarantors,$vendorid)
    {
        $data = array();
        for ($i = 0; $i < count($guarantors); $i++)
        {
            $data = array(
                'vendorid' => $vendorid,
                'firstname' => $guarantors[$i]->firstname,
                'lastname' => $guarantors[$i]->lastname,
                'phoneno' => $guarantors[$i]->workphone,
                'email' => $guarantors[$i]->email,
                'address' => $guarantors[$i]->address_line1,
                'city' => $guarantors[$i]->city,
                'state' => $guarantors[$i]->state,
                'country' => "",
                'zipcode' => $guarantors[$i]->zipcode,
                'sikka_status' => $guarantors[$i]->status,
                'guarantor_id' => $guarantors[$i]->guarantor_id,
                'practice_id' => $guarantors[$i]->practice_id,
                'patient_id'=>$guarantors[$i]->patient_id
            );
            $this->customermodel->updateSikkaCustomer($data);
        }


    }

    private function addVetPatientstoDatabase($vet_patients)
    {

        $data = array();

        for ($i = 0; $i < count($vet_patients); $i++)
        {

            $gender = $vet_patients[$i]->gender;

            if (strpos($gender, 'Female') !== false)
            {
                $gender = 'F';
            }
            else
            {
                $gender = 'M';
            }
            if($vet_patients[$i]->microchip)
            {
                $microchipped = "1";
            }
            else
            {
                $microchipped = "0";
            }

            $breedid = $this->petbreedmodel->getBreedid($vet_patients[$i]->breed);
            $petid = $this->genratePrimaryKey('40');
            if($this->petmodel->petIdExists($petid))
            {
                $petid = $this->genratePrimaryKey('50');
            }
            $data = array(
                'name' => $vet_patients[$i]->petname,
                'gender' => $gender,
                'birthdate' => $vet_patients[$i]->birthdate,
                'isadopted' => 0,
                'currentweight' => $vet_patients[$i]->weight,
                'furcolor' => $vet_patients[$i]->color,
                'microchipped' => $microchipped,
                'microchipid' => $vet_patients[$i]->microchip,
                'primarybreed' => $breedid,
                'veterinarianid' => 1,
                'profileCompletion'=> 70,
                'guarantor_id' => $vet_patients[$i]->guarantor_id,
                'practice_id' => $vet_patients[$i]->practice_id,
                'patient_id'=>$vet_patients[$i]->patient_id
            );
            $this->petmodel->updateSikkaPet($data,$petid);
        }

    }

    private function fetchGuarantors($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/guarantors?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }
    private function fetchVeterinary_patients($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/patients/veterinary_patients?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }

    public function sikkaVaccinationReport(){
        $this->load->library('pdf');
        $pdf = new pdf('p','mm','A4');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Times','',12);
        for($i=1;$i<=40;$i++)
            $pdf->Cell(0,10,base_url().'Printing line number '.$i,0,1);
        $pdf->Output();

    }

}