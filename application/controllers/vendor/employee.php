<?php

class Employee extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();

        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('employeemodel');
        $this->load->model('vendorsmodel');

        $this->roles = array('admin', 'standard');
        $this->defaultemployee = array(
            'userid' => 0,
            'firstname' => "",
            'lastname' => "",
            'email' => "",
            'password' => "",
            'phone' => ""
        );
    }

    function listofemployees() {
        $admin = $this->session->userdata('admin');
        $this->data['title'] = '| Listofemployee';
        $vendorid = $admin['adminDetails']['vendorid'];
        /* pagignation control */
        $config['base_url'] = base_url() . 'vendor/employee/listofemployees/';
        $config['total_rows'] = $this->employeemodel->getEmployeesCount($vendorid);
        $config['per_page'] = PERPAGE;
        $config['uri_segment'] = 4;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $this->pagination->initialize($config);
        /* pagination section ends here */

        $page = $this->uri->segment(4);
        if ($page < 1)
            $page = 0;
        $vendorEmployees = $this->employeemodel->getAllEmployees($config['per_page'], $page, $vendorid);
        $data = array();
        $data['employees'] = $vendorEmployees;
        $str_links = $this->pagination->create_links();
        $data['links'] = explode('&nbsp;', $str_links);
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/listofemployees', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function deleteEmployee($employeeid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $employeestatus = $this->employeemodel->employeestatus($vendorid, $employeeid);
        if ($employeestatus == TRUE) {
            $this->employeemodel->deleteEmployee($employeeid);
            $msg = "Employee deleted successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/employee/listofemployees');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function viewDetails($userid) {
        $data = array();
        $this->data['title'] = '| View Details';
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $employeestatus = $this->employeemodel->employeestatus($vendorid, $userid);
        if ($employeestatus == TRUE) {
            $employee = $this->employeemodel->getEmployeeDetails($userid);
            $data['employeedetail'] = $employee;
            // $data['roles']=$this->roles;

            $this->data['maincontent'] = $this->load->view('vendor/maincontents/addemployee', $data, true);
            $this->load->view('vendor/layout', $this->data);
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function addemployee() {
        $data = array();
        $this->data['title'] = '| Add Employee';
        $data['employeedetail'] = $this->defaultemployee;
        //$data['roles']=$this->roles;

        $this->data['maincontent'] = $this->load->view('vendor/maincontents/addemployee', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function updateEmployee() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $userid = $this->input->post('userid');
        $firstname = (trim($this->input->post('firstname')));
        $lastname = (trim($this->input->post('lastname')));
        $email = (trim($this->input->post('email')));
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');

        $datacc = array(
            'vendorid' => $vendorid,
            'userid' => $userid,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'password' => $password,
            'phone' => $phone
        );
        $serviceid = $this->employeemodel->updateemployee($datacc, $userid);

        $vendordetail = $this->vendorsmodel->getVendorDetails($vendorid);
        $vendorname = $vendordetail['firstname'] . " " . $vendordetail['lastname'];

        $requestHost = $_SERVER['HTTP_HOST'];
        $host = 'Bizbark';

        if (strpos($requestHost, 'babelvet') !== false) {
            $host = 'Babelvet';
        }

        $username = $firstname . " " . $lastname;
        $companyname = $vendordetail['comapnyname'];

        //send reminder mail to vendor
        $sendmessage = file_get_contents('resources/VendorAddedEmployee.html');

        $sendmessage = str_replace('%customername%', $firstname.' '.$lastname, $sendmessage);
        $sendmessage = str_replace('%companyname%', $companyname, $sendmessage);
        $sendmessage = str_replace('%email%', $email, $sendmessage);
        $sendmessage = str_replace('%password%', $password, $sendmessage);
        $sendmessage = str_replace('%host%', $host, $sendmessage);


        $from = SITE_MAIL;
        $subject = 'Welcome to ' . $host;

        ///send_template_mail('',$sendmessage, $subject, $vendoremail, $from);
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = SITE_MAIL;
        $config['smtp_pass'] = "doogyWOOGY!";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from(SITE_MAIL_SUPPORT, $host);
        $this->email->to($email);
        $this->email->reply_to(SITE_MAIL_SUPPORT, $host);
        $this->email->cc(SITE_MAIL, $host);
        $this->email->subject($subject);
        //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');

        $this->email->message($sendmessage);
        
        if ($this->employeemodel->employeeIdExists($userid) == TRUE){
            $result = '';
        }else{
            $result = $this->email->send();
        }
            $this->session->set_flashdata('success_message', 'Employee updated.');
            redirect('vendor/employee/listofemployees');
            echo "Email Vendor Success";
    }
    
    public function checkUniqueEmail(){
        $email = $this->input->post('email');
        $query = $this->employeemodel->checkUniqueEmail($email);
        if($query > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
