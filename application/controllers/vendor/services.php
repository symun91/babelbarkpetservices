<?php

class Services extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('petbreedmodel');
        $this->load->model('admin_model');
        $this->load->model('servicemodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('settingsmodel');


        $this->services = array();
        //$this->servicetypes=array('walker','sitter','grooming','petstore');
        //$this->servicetypes=array('walker' => 'Walk','sitter'=>'Sitting','boarding'=>'Boarding','grooming'=>'Grooming','petstore'=>'Pet Store
        //','other'=>'Other');
        $servicecategories = $this->vendorcategoriesmodel->getVendorCategories();

        $servicecategarr = array();
        for ($i = 0; $i < count($servicecategories); $i++) {
            $servicecategarr[$servicecategories[$i]['title']] = $servicecategories[$i]['title'];
        }
        $this->servicetypes = $servicecategarr;

        $this->paymenttypes = array('Cash', 'Credit/Debit', 'Check', 'Other');

        $this->weekdays = array('Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su');
        $this->defaultservice = array(
            'serviceid' => 0,
            'name' => "",
            'type' => "",
            'price' => "",
            'priceby' => "total",
            'paymenttype' => "",
            'duration' => "",
            'durationunit' => "hours",
            'availabilitydays' => "",
            'available_status' => "",
            'availabilityfromhour' => "",
            'availabilitytohour' => "",
            'reservation' => "",
            'hide_services' => "",
            'restriction' => "",
            'restrictfromage' => "",
            'restricttoage' => "",
            'restrictfromwt' => "",
            'restricttowt' => "",
            'medicalcondnotallowed' => "",
            'notes' => "",
            'minimal_notice' => "",
            'maximum_notice' => "",
            'capacity' => "",
            'assign_emp_status' => "",
            'restrictagelimit' => "inbetween",
            'restrictweightlimit' => "inbetween"
        );

        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $metricunits = json_decode(METRICUNITS, true);
        $settingdetails = $this->settingsmodel->getDetails1($vendorid);
        if ($settingdetails == "")
            $this->wtunit = $metricunits['weight'];
        else {
            $unittype = $settingdetails['unittype'];

            if ($unittype == "Metric") {
                $this->wtunit = $metricunits['weight'];
            } else {
                $britishunits = json_decode(BRITISHUNITS, true);
                $this->wtunit = $britishunits['weight'];
            }
        }
    }

    function listofservices() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $data = array();
        /* pagignation control */
        $config['base_url'] = base_url() . 'vendor/services/listofservices/';
        $config['total_rows'] = $this->servicemodel->getServicesCount($vendorid);
        $config['per_page'] = PERPAGE;
        $config['uri_segment'] = 4;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $this->pagination->initialize($config);
        /* pagination section ends here */

        $page = $this->uri->segment(4);
        if ($page < 1)
            $page = 0;
        $services = $this->servicemodel->getAllServices($config['per_page'], $page, $vendorid);

        $data = array();
        $data['services'] = $services;
        $str_links = $this->pagination->create_links();
        $data['links'] = explode('&nbsp;', $str_links);

        $this->data['maincontent'] = $this->load->view('vendor/maincontents/listofservices', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function deleteServices($serviceid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $servicestatus = $this->servicemodel->serviceStatus($vendorid, $serviceid);
        if ($servicestatus == TRUE) {
            $this->servicemodel->deleteService($serviceid);
            $msg = "Service deleted successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/services/listofservices');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function addservice() {
        $data = array();
        $data['servicedetail'] = $this->defaultservice;
        $data['servicetypes'] = $this->servicetypes;
        $data['paymenttypes'] = $this->paymenttypes;
        $data['weekdays'] = $this->weekdays;
        $data['weightunit'] = $this->wtunit;
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $data['details'] = $this->admin_model->find_data('row', array('vendorid' => $vendorid), VENDOR, 0, 0, 'vendorid');
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/addservice', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function viewDetails($serviceid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $servicestatus = $this->servicemodel->serviceStatus($vendorid, $serviceid);
        if ($servicestatus == TRUE) {
            $data = array();
            $service = $this->servicemodel->getServiceDetails($serviceid);
            $data['servicedetail'] = $service;
            $data['servicetypes'] = $this->servicetypes;
            $data['details'] = $this->admin_model->find_data('row', array('vendorid' => $vendorid), VENDOR, 0, 0, 'vendorid');
            $data['paymenttypes'] = $this->paymenttypes;
            $data['weekdays'] = $this->weekdays;
            $data['weightunit'] = $this->wtunit;
            $this->data['maincontent'] = $this->load->view('vendor/maincontents/addservice', $data, true);
            $this->load->view('vendor/layout', $this->data);
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function updateService() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];

        $this->form_validation->set_rules('servicename', 'Service Name', 'trim|required');
        $this->form_validation->set_rules('selecttype', 'Service Type', 'trim|required');
        //$this->form_validation->set_rules('serviceprice', 'Service Price', 'trim|required');
        //$this->form_validation->set_rules('selectpaymenttype', 'Payement Type', 'trim|required');

        $this->form_validation->set_rules('selectpaymenttype');
        $this->form_validation->set_rules('priceby');
        $this->form_validation->set_rules('duration');
        $this->form_validation->set_rules('durationunit');
        $this->form_validation->set_rules('availabilitydays');
        $this->form_validation->set_rules('calendar_color');
        $this->form_validation->set_rules('fromhour');
        $this->form_validation->set_rules('tohour');
        $this->form_validation->set_rules('reservation');
        $this->form_validation->set_rules('restriction');
        $this->form_validation->set_rules('fromageyrs');
        $this->form_validation->set_rules('fromagemonths');
        $this->form_validation->set_rules('toageyrs');
        $this->form_validation->set_rules('toagemonths');
        $this->form_validation->set_rules('fromwt');
        $this->form_validation->set_rules('towt');
        $this->form_validation->set_rules('medicalcondnotallowed');
        $this->form_validation->set_rules('servicenotes');

        if ($this->form_validation->run() == FALSE) {
            $data = array();
            $data['servicetypes'] = $this->servicetypes;
            $data['paymenttypes'] = $this->paymenttypes;
            $data['weekdays'] = $this->weekdays;
            $data['servicedetail'] = $this->defaultservice;

            $this->data['maincontent'] = $this->load->view('vendor/maincontents/addservice', $data, true);
            $this->load->view('vendor/layout', $this->data);
        } else {
            $serviceid = $this->input->post('serviceid');
            $servicename = (trim($this->input->post('servicename')));
            $servicetype = $this->input->post('selecttype');
            $price = (trim($this->input->post('serviceprice')));
            $priceby = $this->input->post('priceby');
            $paymenttype = $this->input->post('selectpaymenttype');
            $duration = (trim($this->input->post('duration')));
            $durationunit = $this->input->post('durationunit');
            //availability days
            if (is_array($this->input->post('availabilitydays'))) {
                $availabilitydays = implode(",", $this->input->post('availabilitydays'));
            } else {
                $availabilitydays = $this->input->post('availabilitydays');
            }

            $fromhour = (trim($this->input->post('availabityfromhours')));
            $tohour = (trim($this->input->post('availabitytohours')));
            $reservation = $this->input->post('reservation');
            $restriction = $this->input->post('restriction');
            $availableStatus = $this->input->post('available_status');
            $hideServices = $this->input->post('hide_services');
            $assign_emp_status = $this->input->post('assign_emp_status');
            $minimal_notice = (trim($this->input->post('minimal_notice')));
            $maximum_notice = (trim($this->input->post('maximum_notice')));
            $capacity = (trim($this->input->post('capacity')));
            $agelimit = $this->input->post('agelimit');
            $fromageyrs = (trim($this->input->post('fromageyrs')));
            $fromagemonths = (trim($this->input->post('fromagemonths')));

            $toageyrs = (trim($this->input->post('toageyrs')));
            $toagemonths = (trim($this->input->post('toagemonths')));

            $calendar_color = (trim($this->input->post('calendar_color')));
            $fromage = "";
            if ($fromageyrs != "")
                $fromage = $fromageyrs . "years";
            if ($fromagemonths != "")
                $fromage = $fromage . $fromagemonths . "months";

            $toage = "";
            if ($toageyrs != "")
                $toage = $toageyrs . "years";
            if ($toagemonths != "")
                $toage = $toage . $toagemonths . "months";

            $wtlimit = $this->input->post('weightlimit');
            $fromwt = (trim($this->input->post('fromwt')));
            $towt = (trim($this->input->post('towt')));
            $medicalcondnotallowed = (trim($this->input->post('medicalcondnotallowed')));
            $servicenotes = (trim($this->input->post('servicenotes')));

            //add/update service
            $datacc = array(
                'vendorid' => $vendorid,
                'serviceid' => $serviceid,
                'name' => $servicename,
                'type' => $servicetype,
                'price' => $price,
                'priceby' => $priceby,
                'paymenttype' => $paymenttype,
                'duration' => $duration,
                'durationunit' => $durationunit,
                'calendar_color' => $calendar_color,
                'availabilitydays' => $availabilitydays,
                'availabilityfromhour' => $fromhour,
                'availabilitytohour' => $tohour,
                'reservation' => $reservation,
                'restriction' => $restriction,
                'restrictfromage' => $fromage,
                'restricttoage' => $toage,
                'restrictfromwt' => $fromwt,
                'restricttowt' => $towt,
                'medicalcondnotallowed' => $medicalcondnotallowed,
                'notes' => $servicenotes,
                'restrictagelimit' => $agelimit,
                'restrictweightlimit' => $wtlimit,
                'available_status' => $availableStatus,
                'hide_services' => $hideServices,
                'assign_emp_status' => $assign_emp_status,
                'minimal_notice' => $minimal_notice,
                'maximum_notice' => $maximum_notice,
                'capacity' => $capacity
            );

            $serviceid = $this->servicemodel->updateService($datacc, $serviceid);


            $this->session->set_flashdata('success_message', 'Service updated.');
            redirect('vendor/services/listofservices');
        }
    }

}

?>
