<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property admin_model $admin_model
 * @property appointmentmodel $appointmentmodel
 * @property servicemodel $servicemodel
 * @property vendorsmodel $vendorsmodel
 * @property vendorcategoriesmodel $vendorcategoriesmodel
 * @property promotionsmodel $promotionsmodel
 * @property employeemodel $employeemodel
 */
class Home extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();
        $this->load->library('admin_init_elements');

        $this->load->model('admin_model');
        $this->load->model('appointmentmodel');
        $this->load->model('servicemodel');
        $this->load->model('promotionsmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('employeemodel');

        //$this->stateUSArray=array("Alabama","Montana","Alaska","Nebraska","Arizona","Nevada","Arkansas","New Hampshire","California","New Jersey","Colorado","New Mexico","Connecticut","New York","Delaware","North Carolina","Florida","North Dakota","Georgia","Ohio","Hawaii","Oklahoma","Idaho","Oregon","Illinois","Pennsylvania","Indiana","Rhode Island","Iowa","South Carolina","Kansas","South Dakota","Kentucky","Tennessee","Louisiana","Texas","Maine","Utah","Maryland","Vermont","Massachusetts","Virginia","Michigan","Washington","Minnesota","West Virginia","Mississippi","Wisconsin","Missouri","Wyoming");
        //$this->stateCanadaArray = array("Ontario", "Quebec", "Nova Scotia", "New Brunswick", "Manitoba", "British Columbia", "Prince Edward Island", "Saskatchewan", "Alberta", "Newfoundland and Labrador", "Northwest Territories", "Yukon", "Nunavut");
        $this->weekdays = array('Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su');
        $this->stateUSArray = json_decode(USSTATES, true);
        $this->stateCanadaArray = json_decode(CANADASTATES, true);
    }

    function index() {
        if ($this->session->userdata('isadmin')) {
            redirect('admin/home');
        }
        $this->admin_init_elements->init_elements();
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $data = array();
        $data['vendor_data'] = array('name' => $admin['adminDetails']['firstname'] . ' ' . $admin['adminDetails']['lastname'], 'value' => $vendorid);
        $events = $selected_employee = $selected_service = $selected_customer = array();
        if ($this->input->post('vendor_employee')) {
            $selected_employee = $this->input->post('vendor_employee');
        }
        if ($this->input->post('vendor_services')) {
            $selected_service = $this->input->post('vendor_services');
        }
        if ($this->input->post('vendor_customer')) {
            $selected_customer = $this->input->post('vendor_customer');
        }


        $appointments = $this->appointmentmodel->getAllappointments1($vendorid, $selected_employee, $selected_service, $selected_customer);

        $data['selected_employee'] = $selected_employee;
        $data['selected_service'] = $selected_service;
        $data['selected_customer'] = $selected_customer;

        for ($i = 0; $i < count($appointments); $i++) {
            if($appointments[$i]['userid']!=''){
                $employee_info = $this->employeemodel->getEmployeeDetails($appointments[$i]['userid']);
            }
            $events[$i]['title'] = $appointments[$i]['servicename'];
            $events[$i]['color'] = $appointments[$i]['calendar_color'];
            $start = $appointments[$i]['boardingfromdate'];
            if ($appointments[$i]['boardingfromtime'] != "")
                $start = $start . "T" . $appointments[$i]['boardingfromtime'];
            $events[$i]['start'] = $start;

            $end = $appointments[$i]['boardingtodate'];
            if ($appointments[$i]['boardingtotime'] != "")
                $end = $end . "T" . $appointments[$i]['boardingtotime'];
            $events[$i]['end'] = $end;
            $events[$i]['weekdescription'] = "<span>".$appointments[$i]['petname']."</span></br>                                               
                                                <span>".$appointments[$i]['firstname'].' '.$appointments[$i]['lastname']."</span></br>                                              
                                                <span>".$employee_info['firstname'].' '.$employee_info['lastname']."</span>";
            $events[$i]['daydescription'] = "<span>".$appointments[$i]['petname']."</span></br>                                               
                                                <span>".$appointments[$i]['firstname'].' '.$appointments[$i]['lastname']."</span></br>                                              
                                                <span>".$employee_info['firstname'].' '.$employee_info['lastname']."</span>";
            $events[$i]['url'] = base_url() . 'vendor/appointments/viewDetails/' . $appointments[$i]['appointmentid'];
        }


        $currentmonthappointments = $this->appointmentmodel->getcurrentAppointments($vendorid);

        $data['events'] = json_encode($events);

        $data['currentmonthappointments'] = $currentmonthappointments;
        list($data['all_services'], $data['all_employee']) = $this->appointmentmodel->get_vendor_services_and_employees($vendorid);
        $data['upcomingappointments'] = $this->appointmentmodel->getUpcomingAppointments($vendorid);
        $data['services'] = $this->servicemodel->getLatestServices($vendorid);
        $data['promotions'] = $this->promotionsmodel->getAllPromotions1($vendorid);

        $this->load->model('customermodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('appusersmodel');
        $appusers = array();
        $customers = $this->customermodel->getAllCustomers($vendorid);
        for ($i = 0; $i < count($customers); $i++) {
            $customers[$i]['usertype'] = 'customer';
        }

        $babelbarkusers = $this->appuservendorsmodel->getUsersForVendor($vendorid);
        for ($i = 0; $i < count($babelbarkusers); $i++) {
            $appuserid = $babelbarkusers[$i]['appuserid'];
            $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
            $appuser['usertype'] = 'appuser';
            $appusers[$i] = $appuser;
        }
        $commonArray = array_merge($customers, $appusers);
        sort($commonArray);
        $data['all_customers'] = $commonArray;

        $this->data['maincontent'] = $this->load->view('vendor/maincontents/dashboard', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function upload_thumbnail() {
        $this->load->helper('string');
        $w = $this->input->post('thumb_width_');
        $h = $this->input->post('thumb_height_');
        $x1 = $this->input->post('x_axis');
        $y1 = $this->input->post('y_axis');
        $x2 = $this->input->post('x2_axis');
        $y2 = $this->input->post('y2_axis');
        $imgage = $this->input->post('img_');

        $img = awsBucketPath . $imgage;
        $fname = "temp_logoimage_" . time() . ".jpg";
        $uploaddir = 'resources/uploads/temp/';
        $pathTofile = "resources/uploads/temp/$fname";

        list($width, $height, $type, $attr) = getimagesize($img);
        /* echo "image path: ";
          echo $img;
          echo "type: ";
          echo $type; */
        if ($type == 1) {
            $nw = $x2; // Maximum thumbnail width
            $nh = $y2; //Maximum thumbnail height

            $img1 = $img;
            $im = imagecreatefromgif($img1);
            $size = min(imagesx($im), imagesy($im));
            $im2 = imagecrop($im, ['x' => $x1, 'y' => $y1, 'width' => $size, 'height' => $size]);
            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = imagegif($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;
        } else if ($type == 2) {
            error_reporting(E_ALL);

            $nw = $w; // Maximum thumbnail width
            $nh = $h; //Maximum thumbnail height

            $im = imagecreatefromjpeg($img);
            $size = min(imagesx($im), imagesy($im));

            $im2 = imagecrop($im, ['x' => $x1, 'y' => $y1, 'width' => $size, 'height' => $size]);

            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = imagejpeg($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;
        } else if ($type == 3) {
            $nw = $w; // Maximum thumbnail width
            $nh = $h; //Maximum thumbnail height
            $im = imagecreatefromPNG($img);
            $size = min(imagesx($im), imagesy($im));
            $im2 = imagecrop($im, ['x' => $x1, 'y' => $y1, 'width' => $size, 'height' => $size]);
            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = imagepng($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;
        }
    }

    function profile() {
        $this->admin_init_elements->init_elements();
        $data = array();
        $image = '';
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $data['details'] = $this->admin_model->find_data('row', array('vendorid' => $vendorid), VENDOR, 0, 0, 'vendorid');
        $data['servicetypes'] = $this->vendorcategoriesmodel->getVendorCategories();
        $data['weekdays'] = $this->weekdays;
        $data['USstates'] = $this->stateUSArray;
        $data['Canadastates'] = $this->stateCanadaArray;

        if ($this->input->post('formsubmit') == 'yes') {

            $email = $this->input->post('email');
            if ($this->vendorsmodel->isEmailExistsForAUser($email, $vendorid) == 1) {
                $this->session->set_flashdata('error_message', 'Email Id already exists.');
                redirect('vendor/home/profile');
            } else {

                $categories = $this->input->post('selecttype');
                if (is_array($this->input->post('selecttype'))) {
                    $categorystr = implode(",", $this->input->post('selecttype'));
                } else
                    $categorystr = $this->input->post('selecttype');

                $latlng = $this->input->post('lat') . "," . $this->input->post('lng');
                if (is_array($this->input->post('availabilitydays'))) {
                    $availabilitydays = implode(",", $this->input->post('availabilitydays'));
                } else {
                    $availabilitydays = $this->input->post('availabilitydays');
                }


                $fromhour = (trim($this->input->post('availabityfromhours')));
                $tohour = (trim($this->input->post('availabitytohours')));
                $datacc = array(
                    'email' => $this->input->post('email'),
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'comapnyname' => $this->input->post('comapnyname'),
                    'contact' => $this->input->post('phoneno'),
                    'website' => $this->input->post('website'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'country' => $this->input->post('country'),
                    'zipcode' => $this->input->post('zipcode'),
                    'description' => $this->input->post('description'),
                    'category' => $categorystr,
                    'addresslatlng' => $latlng,
                    'operatingdays' => $availabilitydays,
                    'availabilityfromhour' => $fromhour,
                    'availabilitytohour' => $tohour
                );
                $update = $this->vendorsmodel->updateVendor($datacc, $vendorid);
                $uploadmsg = "Profile updated.";

                //upload banner image


                $logoimageupdated = false;


                $logoimage = $this->input->post('logoimage1');
                $remove_logo = intval($this->input->post('remove_logo'));
                if ($remove_logo) {

                    // Removing current image of logged user
                    $current_image = $data['details']['logoimage'];
                    if (!empty($current_image)) {
                        $sourceFolder = 'resources/uploads/images/admin/';
                        aws_delete($sourceFolder, $current_image);
                        $datacc = array();
                        $datacc['logoimage'] = '';
                        $this->vendorsmodel->updateVendor($datacc, $vendorid);
                        $logoimageupdated = true;
                    }
                } else if ($logoimage != '') {
                    $logoimage1 = explode('/temp/', $logoimage);
                    $filename = $logoimage1[1];
                    $imgurl = '';
                    $allowed = array('gif', 'png', 'jpg', 'jpeg');
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (!in_array($ext, $allowed)) {
                        //return false;
                        $uploadmsg = $uploadmsg . "Logo Image Upload failed";
                    } else {
                        /*                         * *********************************** local server upload  **************************** */
//            $fname=$vendorid.".".$ext;
//
//            $location='resources/uploads/images/admin/'.$fname;
//            if(rename($logoimage,$location))
//            {
//                $imgurl='resources/uploads/images/admin/'.$fname;
//                $datacc=array();
//                $datacc['logoimage']=$imgurl;
//                $this->vendorsmodel->updateVendor($datacc,$vendorid);
//                $logoimageupdated=true;
//            }

                        /*                         * *********************************** S3 Integration  **************************** */


                        $sourceFolder = 'resources/uploads/temp/';
                        $sourceFileName = $filename;
                        $targetFolder = 'resources/uploads/images/admin/';
                        $targetFileName = $vendorid . "." . $ext;
                        $fname = $vendorid . "." . $ext;


                        $s3msg = aws_copy($sourceFolder, $sourceFileName, $targetFolder, $targetFileName);
                        if ($s3msg == 'S3 file copy successful') {
                            $imgurl = 'resources/uploads/images/admin/' . $fname;
                            $datacc = array();
                            $datacc['logoimage'] = $imgurl;
                            $this->vendorsmodel->updateVendor($datacc, $vendorid);
                            $logoimageupdated = true;
                            aws_delete($sourceFolder, $sourceFileName);
                        }
                        /*                         * *********************************** End S3 Integration  **************************** */
                    }
                }

                if ($update > 0 || $logoimageupdated) {
                    $this->session->unset_userdata('admin');
                    $updateData = $this->admin_model->find_data('row', array('vendorid' => $vendorid), VENDOR, 0, 0, 'vendorid');

                    $admin = array('isloggedin' => 1, 'adminDetails' => $updateData, 'role' => 'admin');
                    $this->session->set_userdata('admin', $admin);
                    $this->session->set_flashdata('success_message', $uploadmsg);
                    redirect('vendor/home/profile');
                } else {
                    $this->session->set_flashdata('error_message', 'No changes to be saved.Please try again.');
                    redirect('vendor/home/profile');
                }
            }
        }

        $this->data['maincontent'] = $this->load->view('vendor/maincontents/admin-profile', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function storeImageUrl() {
        if (file_exists($_FILES['logoimage']['tmp_name']) || is_uploaded_file($_FILES['logoimage']['tmp_name'])) {

            $imgurl = '';
            $uploadmsg = '';
            $allowed = array('gif', 'png', 'jpg', 'jpeg');
            $filename = $_FILES['logoimage']['name'];
            $filetmpname = $_FILES['logoimage']['tmp_name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if (!in_array($ext, $allowed)) {
                //return false;	
                $uploadmsg = $uploadmsg . "Logo Image Upload failed";
            } else {
                /*                 * ********************************** S3 Integration  **************************** */

                $uploaddir = 'resources/uploads/temp/';
                $fname = "temp_logoimage_" . time() . "." . $ext;
                $s3msg = aws_upload($uploaddir, $fname, $filetmpname);

                if ($s3msg == 'S3 upload successful') {
                    $imgurl = 'resources/uploads/temp/' . $fname;
                    echo $imgurl;
                } else {
                    $uploadmsg = $uploadmsg . "Logo Image Upload failed!";
                }

                /*                 * *********************************** End S3 Integration  **************************** */
            }
        }
    }

    function changePassword() {
        $this->admin_init_elements->init_elements();
        $data = array();
        if ($this->input->post('submit')) {
            if ($this->validate_form_data() == true) {
                $admin = $this->session->userdata('admin');
                $updateData = array('password' => $this->input->post('new_password'), 'updatedon' => date('Y-m-d H:i:s'));
                $update = $this->admin_model->save_data($updateData, $admin['adminDetails']['vendorid'], 'vendorid', VENDOR);

                if ($update > 0) {
                    $this->session->unset_userdata('admin');
                    $adminData = $this->admin_model->find_data('row', array('vendorid' => $admin['adminDetails']['vendorid']), VENDOR, 0, 0, 'vendorid');
                    $admin = array('isloggedin' => 1, 'adminDetails' => $adminData, 'role' => 'admin');
                    $this->session->set_userdata('admin', $admin);
                    $this->session->set_flashdata('success_message', 'Your password is changed successfully.');
                    redirect('vendor/home/changePassword');
                } else {
                    $this->session->set_flashdata('error_message', 'Some error occurred while changing your password.Please try again.');
                    redirect('vendor/home/changePassword');
                }
            }
        }

        $this->data['maincontent'] = $this->load->view('vendor/maincontents/change-password', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function forgetPassword() {
        $this->admin_init_elements->init_loginSignup_head();
        $data = array();
        if ($this->input->post('submit')) {
            if ($this->validate_forget_data() == true) {
                $admin = $this->admin_model->find_data('row', array('email' => $this->input->post('email')), VENDOR, 0, 0, 'vendorid');
            }
        }
        $this->load->view('vendor/maincontents/forget-password', $this->data);
    }

    private function validate_profile_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    private function validate_admin_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|strip_tags|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|strip_tags|required');
        $this->form_validation->set_rules('email', 'Email Id', 'trim|strip_tags|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        //$this->form_validation->set_rules('password','Password','trim|required|min_length[6]');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    private function validate_form_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|callback_is_correct_password');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    function is_correct_password($str) {
        $admin = $this->session->userdata('admin');
        if ($this->input->post('old_password') == $admin['adminDetails']['password']) {
            return true;
        } else {
            $this->form_validation->set_message('is_correct_password', 'Invalid password.');
            return false;
        }
    }

    private function check_access() {
        $admin = $this->session->userdata('admin');
        if ($admin['adminDetails']['type'] != "SA") {
            $this->session->set_flashdata('error_message', 'Sorry you are not authorized to access this section.Please try different section.');
            redirect('admin/home/index');
        }
    }

    function testingServer() {
        echo 'Check git push';
    }

}

?>
