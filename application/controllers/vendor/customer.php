<?php

/**
 * @property appusersmodel               $appusersmodel
 * @property appuserdevicesmodel         $appuserdevicesmodel
 * @property petmodel                    $petmodel
 * @property petmedicationmodel          $petmedicationmodel
 * @property petvaccinationmodel         $petvaccinationmodel
 * @property petgoalsmodel               $petgoalsmodel
 * @property petlogmodel                 $petlogmodel
 * @property veterinarianmodel           $veterinarianmodel
 * @property petfeedmodel                $petfeedmodel
 * @property petbreedmodel               $petbreedmodel
 * @property vendorsmodel                $vendorsmodel
 * @property appuservendorsmodel         $appuservendorsmodel
 * @property promotionsmodel             $promotionsmodel
 * @property mailnotifymodel             $mailnotifymodel
 * @property nonregistervendormodule     $nonregistervendormodule
 * @property appointmentmodel            $appointmentmodel
 * @property servicemodel                $servicemodel
 * @property customermodel               $customermodel
 * @property medicalfilesmodel           $medicalfilesmodel
 * @property settingsmodel               $settingsmodel
 */
class Customer extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper(array('common_helper', 'url'));
        $this->load->model('petbreedmodel');
        $this->load->model('petfeedmodel');
        $this->load->model('customermodel');
        $this->load->model('petmodel');
        $this->load->model('veterinarianmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('appusersmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('petmedicationmodel');
        $this->load->model('invitationtokenmodel');
        $this->load->model('settingsmodel');
        $this->load->model('medicalfilesmodel');
        $this->load->model('petvaccinationmodel');
        $this->defaultpetmedication = array();
        $this->weekdays = array('M', 'T', 'W', 'Th', 'F', 'Sa', 'Su');
        $this->medicines = $this->petmedicationmodel->getAllMedications();
        $this->vaccinations = $this->petvaccinationmodel->getAllVaccinations();
        $this->defaultcustomer = array(
            'customerid' => 0,
            'firstname' => "",
            'lastname' => "",
            'phoneno' => "",
            'email' => "",
            'address' => "",
            'city' => "",
            'state' => "",
            'country' => "",
            'zipcode' => "",
            'notes' => "",
        );
        //$this->regularity=array('Daily', 'Weekly','2 Days', '3 Days','4 Days', '5 Days','6 Days', 'Monthly');
        $this->regularity = array('Day', 'Week', 'Month');
        $this->defaultpet = array(
            'petid' => 0,
            'name' => "",
            'proilefpicture' => "",
            'primarybreed' => "",
            'secondarybreed' => "",
            'primaryfood' => "",
            'secondaryfood' => "",
            'birthdate' => "",
            'microchipped' => 0,
            'microchipid' => "",
            'govtlicensetagno' => "",
            'govtlicensetagissuer' => "",
            'isadopted' => 0,
            'gender' => "",
            'currentweight' => "",
            'targetweight' => "",
            'medicalhistory' => "",
            'amountoffeed' => "",
            'frequencyoffeed' => "",
            'approximateage' => "",
            'birthdayunsure' => 0
        );
        $this->defaultvet = array(
            'veterinarianid' => 0,
            'name' => "",
            'phoneno' => "",
            'emergencyno' => "",
            'contactperson' => "",
            'fromhoursofoperation' => "",
            'tohoursofoperation' => "",
            'availabilitydays' => "",
            'address' => "",
            'city' => "",
            'state' => "",
            'country' => "",
            'zipcode' => ""
        );
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $settingdetails = $this->settingsmodel->getDetails1($vendorid);
        if ($settingdetails == "")
            $this->feedunit = "kilos";
        else
            $this->feedunit = $settingdetails['feedingunit'];
        $this->unittype = !empty($settingdetails['unittype']) ? $settingdetails['unittype'] : '' ;
        $this->stateUSArray = json_decode(USSTATES, true);
        $this->stateCanadaArray = json_decode(CANADASTATES, true);
    }

    private static function genratePrimaryKey($serverid) {
        $primarykey = $serverid . time() . mt_rand(1000, 9999);
        if ($primarykey > 9223372036854775807) { //max of 64 bit int
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }

    private static function genrateSessionToken() {
        $sessiontok = md5(microtime() . rand());
        return $sessiontok;
    }

    function listofgenericcustomer($customerType = 'all') {
        $data = array();
        $data['usertype'] = $this->uri->segment(4);
        $data['customers'] = array();
        $data['genericCustomers'] = array();
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/listofgenericcustomer', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }
    public function listofgenericcustomerajax() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $customers = array();
        $appusers = array();
        $customerType = 'all'; //changed as per requirement
        if ($customerType == 'all' || $customerType == 'generic') {
            $genericcustomers = $this->customermodel->getAllCustomers($vendorid);
            for ($i = 0; $i < count($genericcustomers); $i++) {
                $customerid = $genericcustomers[$i]['customerid'];
                $customerpets = $this->customermodel->getPets($customerid);
                if ($customerpets > 1) {
                    $customerpetids = $this->customermodel->getPetIDs($customerid);
                    $customerpetname = "";
                    $count = 0;
                    $genericcustomers[$i]['usertype'] = 'Generic Customer';
                    for ($j = 0; $j < $customerpets; $j++) {
                        $petdetail = $this->petmodel->getPetDetails($customerpetids[$j]['petid']);
                        $count++;
                        $customerpetname = $customerpetname . "" . $count . " " . $petdetail['name'] . "<br/>";
                    }
                    $genericcustomers[$i]['petname'] = $customerpetname;
                    $customers[$i] = $genericcustomers[$i];
                } else {
                    $customerpetids = $this->customermodel->getPetIDs($customerid);
                    $petdetail = $this->petmodel->getPetDetails($customerpetids[0]['petid']);
                    $genericcustomers[$i]['petname'] = $petdetail['name'];
                    $genericcustomers[$i]['usertype'] = 'Generic Customer';
                    $customers[$i] = $genericcustomers[$i];
                }
            }
        }
        if ($customerType == 'all' || $customerType == 'babelbark') {
            $babelbarkusers = $this->appuservendorsmodel->getUsersForVendor($vendorid);
            if (count($babelbarkusers) > 0) {
                for ($i = 0; $i < count($babelbarkusers); $i++) {
                    $appuserid = $babelbarkusers[$i]['appuserid'];
                    $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
                    $appuserpets = $this->appusersmodel->getPets($appuserid);
                    if ($appuserpets > 1) {
                        $appuserpetid = $this->appusersmodel->getPetIDs($appuserid);
                        $petname = "";
                        $count = 0;
                        for ($j = 0; $j < $appuserpets; $j++) {
                            $petdetails = $this->petmodel->getPetDetails1($appuserpetid[$j]['petid']);
                            $count++;
                            if (ALLOW_VET == 1) {
                                $petname = $petname . '<a href=' . base_url() . 'viewPetProfile/' . $appuserpetid[$j]['petid'] . '/Babelbark target="_blank">' . $count . " " . $petdetails['name'] . '</a><br/>';
                            } else {
                                $petname = $petname . "" . $count . " " . $petdetails['name'] . "<br/>";
                            }
                        }
                        $appuser['usertype'] = 'BabelBark Customer';
                        $appuser['petname'] = $petname;
                        $appusers[$i] = $appuser;
                    } else {
                        $appuserpetid = $this->appusersmodel->getPetIDs($appuserid);
                        $petdetail = $this->petmodel->getPetDetails1($appuserpetid);
                        $appuser['usertype'] = 'BabelBark Customer';
                        if (ALLOW_VET == 1) {
                            $appuser['petname'] = '<a href=' . base_url() . 'viewPetProfile/' . $appuserpetid . '/Babelbark target="_blank">' . $petdetail['name'] . '</a>';
                        } else {
                            $appuser['petname'] = $petdetail['name'];
                        }
                        $appusers[$i] = $appuser;
                    }
                }
            }
        }
        $data = array();
        $commonArray = array_merge($customers, $appusers);
        $data['customers'] = $commonArray;
        $data['genericCustomers'] = json_encode($customers);
        echo json_encode($data);exit;
    }
    function addgenericcustomer() {
        $data = array();
        $data['breeds'] = $this->petbreedmodel->getAllBreeds();
        $data['feedbrands'] = $this->petfeedmodel->getAllFeedBrands();
        $data['custdetail'] = $this->defaultcustomer;
        $data['petdetail'] = $this->defaultpet;
        $data['vetdetail'] = $this->defaultvet;
        $data['weekdays'] = $this->weekdays;
        $data['medicines'] = $this->medicines;
        $data['vaccinations'] = $this->vaccinations;
        $data['regularity'] = $this->regularity;
        $data['petmedication'] = "";
        $data['petvaccination'] = "";
        $data['feedunit'] = $this->feedunit;
        $data['unittype'] = $this->unittype;
        $data['USstates'] = $this->stateUSArray;
        $data['Canadastates'] = $this->stateCanadaArray;
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/addgenericcustomer', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function viewDetails($userid, $usertype) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $user = str_replace("%20", " ", $usertype);
        $customerstatus = $this->customermodel->customerStatus($userid, $vendorid, $user);
        if ($customerstatus == TRUE) {
            $data = array();
            $usertype = str_replace("%20", " ", $usertype);
            $customers = array();
            $appusers = array();
            $nextcustomer = 0;
            $precustomer = 0;
            $nextusertype = "N/A";
            $preusertype = "N/A";
            $customerType = 'all'; //changed as per requirement
            if ($customerType == 'all' || $customerType == 'generic') {
                $genericcustomer = $this->customermodel->getAllCustomers($vendorid);
                for ($i = 0; $i < count($genericcustomer); $i++) {
                    $customer['customerid'] = $genericcustomer[$i]['customerid'];
                    $customer['usertype'] = 'Generic Customer';
                    $customer['updatedon'] = $genericcustomer[$i]['updatedon'];
                    $customers[$i] = $customer;
                }
            }
            if ($customerType == 'all' || $customerType == 'babelbark') {
                $babelbarkusers = $this->appuservendorsmodel->getUsersForVendor($vendorid);
                if (count($babelbarkusers) > 0) {
                    for ($i = 0; $i < count($babelbarkusers); $i++) {
                        $appuser['appuserid'] = $babelbarkusers[$i]['appuserid'];
                        $appuser['usertype'] = 'BabelBark Customer';
                        $appuser['updatedon'] = $babelbarkusers[$i]['updatedon'];
                        $appusers[$i] = $appuser;
                    }
                }
            }
            $commonArray = array_merge($customers, $appusers);

            function cmp($customers, $appusers) {
                return $customers['updatedon'] - $appusers['updatedon'];
            }

            usort($commonArray, 'cmp');
            for ($i = 0; $i < count($commonArray); $i++) {
                if ($commonArray[$i]['usertype'] == 'Generic Customer') {
                    if ($userid == $commonArray[$i]['customerid']) {
                        if (($i + 1) < count($commonArray)) {
                            if ($commonArray[$i + 1]['usertype'] == 'Generic Customer') {
                                $nextcustomer = $commonArray[$i + 1]['customerid'];
                                $nextusertype = $commonArray[$i + 1]['usertype'];
                            } else {
                                $nextcustomer = $commonArray[$i + 1]['appuserid'];
                                $nextusertype = $commonArray[$i + 1]['usertype'];
                            }
                        }
                        if (($i - 1) >= 0) {
                            if ($commonArray[$i - 1]['usertype'] == 'Generic Customer') {
                                $precustomer = $commonArray[$i - 1]['customerid'];
                                $preusertype = $commonArray[$i - 1]['usertype'];
                            } else {
                                $precustomer = $commonArray[$i - 1]['appuserid'];
                                $preusertype = $commonArray[$i - 1]['usertype'];
                            }
                        }
                    }
                } else {
                    if ($userid == $commonArray[$i]['appuserid']) {
                        if (($i + 1) < count($commonArray)) {
                            if ($commonArray[$i + 1]['usertype'] == 'Generic Customer') {
                                $nextcustomer = $commonArray[$i + 1]['customerid'];
                                $nextusertype = $commonArray[$i + 1]['usertype'];
                            } else {
                                $nextcustomer = $commonArray[$i + 1]['appuserid'];
                                $nextusertype = $commonArray[$i + 1]['usertype'];
                            }
                        }
                        if (($i - 1) >= 0) {
                            if ($commonArray[$i - 1]['usertype'] == 'Generic Customer') {
                                $precustomer = $commonArray[$i - 1]['customerid'];
                                $preusertype = $commonArray[$i - 1]['usertype'];
                            } else {
                                $precustomer = $commonArray[$i - 1]['appuserid'];
                                $preusertype = $commonArray[$i - 1]['usertype'];
                            }
                        }
                    }
                }
                if ($nextusertype != "N/A" || $preusertype != "N/A") {
                    break;
                }
            }
            $data['feedunit'] = $this->feedunit;
            $data['unittype'] = $this->unittype;
            $data['USstates'] = $this->stateUSArray;
            $data['Canadastates'] = $this->stateCanadaArray;
            if ($usertype == "Generic Customer") {
                $this->pets = array();
                $medicationarr = array();
                $vaccinationarr = array();
                $customer = $this->customermodel->getCustomerDetails($userid);
                $customerid = $customer['customerid'];
                $customerpets = $this->customermodel->getPets($userid);
                $customervetid = $customer['veterinarianid'];
                $vetdetail = $this->veterinarianmodel->getVetDetails($customervetid);
                if (count($vetdetail) == 0) {
                    $vetdetail = $this->defaultvet;
                }
                if ($customerpets > 1) {
                    $customerpetids = $this->customermodel->getPetIDs($userid);
                    for ($i = 0; $i < $customerpets; $i++) {
                        $petdetail = $this->petmodel->getPetDetails($customerpetids[$i]['petid']);
                        $medicationadded = $this->petmedicationmodel->getPetMedicationForPet1($customerpetids[$i]['petid']);
                        $vaccinationadded = $this->petvaccinationmodel->getPetVaccinationForPet1($customerpetids[$i]['petid']);
                        $this->pets[$i] = $petdetail;
                        $medicationarr[$i] = $medicationadded;
                        $vaccinationarr[$i] = $vaccinationadded;
                    }
                } else {
                    $customerpetids = $this->customermodel->getPetIDs($userid);
                    $petdetail = $this->petmodel->getPetDetails($customerpetids[0]['petid']);
                    $medicationadded = $this->petmedicationmodel->getPetMedicationForPet1($customerpetids[0]['petid']);
                    $vaccinationadded = $this->petvaccinationmodel->getPetVaccinationForPet1($customerpetids[0]['petid']);
                    $this->pets[0] = $petdetail;
                    $medicationarr[0] = $medicationadded;
                    $vaccinationarr[0] = $vaccinationadded;
                }
                $data['custdetail'] = $customer;
                $data['pets'] = $this->pets;
                $data['vetdetail'] = $vetdetail;
                $data['breeds'] = $this->petbreedmodel->getAllBreeds();
                $data['feedbrands'] = $this->petfeedmodel->getAllFeedBrands();
                $data['weekdays'] = $this->weekdays;
                $data['medicines'] = $this->medicines;
                $data['vaccinations'] = $this->vaccinations;
                $data['regularity'] = $this->regularity;
                $data['petmedication'] = $medicationarr;
                $data['petvaccination'] = $vaccinationarr;
                $data['nextcustomer'] = $nextcustomer;
                $data['nextusertype'] = $nextusertype;
                $data['precustomer'] = $precustomer;
                $data['preusertype'] = $preusertype;
                $data['petFileInfo'] = $this->vendorsmodel->getPetFile($customerpetids[0]['petid'], $vendorid);
                $data['vendorFileInfo'] = $this->vendorsmodel->getVendorFile($customerid, $vendorid);
                $this->data['maincontent'] = $this->load->view('vendor/maincontents/viewcustomerdetails', $data, true);
            } else { //app user
                $appuser = $this->appusersmodel->fetchUserDetails1($userid);
                $this->pets = array();
                $this->vets = array();
                $medicalFilesarr = array();
                $medicationarr = array();
                $vaccinationarr = array();
                $appuserpets = $this->appusersmodel->getPets($userid);
                if ($appuserpets > 1) {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    for ($i = 0; $i < $appuserpets; $i++) {
                        $petdetails = $this->petmodel->getPetDetails1($appuserpetid[$i]['petid']);
                        $medicationadded = $this->petmedicationmodel->getPetMedicationForPet1($appuserpetid[$i]['petid']);
                        $vaccinationadded = $this->petvaccinationmodel->getPetVaccineDetails($appuserpetid[$i]['petid']);
                        $uservetids = $petdetails['primaryvet'];
                        if ($uservetids != null) {
                            $vetdetails = $this->vendorsmodel->getVendorDetails($uservetids);
                        } else {
                            $vetdetails['comapnyname'] = " ";
                            $vetdetails['contact'] = 000;
                            $vetdetails['address'] = " ";
                            $vetdetails['city'] = " ";
                            $vetdetails['zipcode'] = " ";
                            $vetdetails['firstname'] = " ";
                            $vetdetails['lastname'] = " ";
                            $vetdetails['state'] = " ";
                            $vetdetails['availabilityfromhour'] = " ";
                            $vetdetails['availabilitytohour'] = " ";
                            $vetdetails['operatingdays'] = " ";
                        }
                        $medicalFiles = $this->medicalfilesmodel->getMedicalReports($userid, $appuserpetid[$i]['petid']);
                        $this->pets[$i] = $petdetails;
                        $this->vets[$i] = $vetdetails;
                        $medicationarr[$i] = $medicationadded;
                        $vaccinationarr[$i] = $vaccinationadded;
                        $medicalFilesarr[$i] = $medicalFiles;
                    }
                } else {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    $petdetail = $this->petmodel->getPetDetails1($appuserpetid);
                    $uservetid = $petdetail['primaryvet'];
                    $medicalFiles = $this->medicalfilesmodel->getMedicalReports($userid, $appuserpetid);
                    if ($uservetid != null) {
                        $vetdetail = $this->vendorsmodel->getVendorDetails($uservetid);
                    } else {
                        $vetdetail['comapnyname'] = " ";
                        $vetdetail['contact'] = 000;
                        $vetdetail['address'] = " ";
                        $vetdetail['city'] = " ";
                        $vetdetail['zipcode'] = " ";
                        $vetdetail['firstname'] = " ";
                        $vetdetail['lastname'] = " ";
                        $vetdetail['state'] = " ";
                        $vetdetail['availabilityfromhour'] = " ";
                        $vetdetail['availabilitytohour'] = " ";
                        $vetdetail['operatingdays'] = " ";
                    }
                    $medicationadded = $this->petmedicationmodel->getPetMedicationForPet1($appuserpetid);
                    $vaccinationadded = $this->petvaccinationmodel->getPetVaccineDetails($appuserpetid);
                    $this->pets[0] = $petdetail;
                    $this->vets[0] = $vetdetail;
                    $medicationarr[0] = $medicationadded;
                    $vaccinationarr[0] = $vaccinationadded;
                    $medicalFilesarr[0] = $medicalFiles;
                }
                $data['pets'] = $this->pets;
                $data['vets'] = $this->vets;
                $data['appuserdetail'] = $appuser;
                $data['medicalfiles'] = $medicalFilesarr;
                $data['petmedication'] = $medicationarr;
                $data['petvaccination'] = $vaccinationarr;
                $data['breeds'] = $this->petbreedmodel->getAllBreeds();
                $data['feedbrands'] = $this->petfeedmodel->getAllFeedBrands();
                $data['weekdays'] = $this->weekdays;
                $data['nextcustomer'] = $nextcustomer;
                $data['nextusertype'] = $nextusertype;
                $data['precustomer'] = $precustomer;
                $data['preusertype'] = $preusertype;
                $data['petFileInfo'] = $this->vendorsmodel->getPetFile($data['pets'][0]['petid'], $vendorid);
                $data['vendorFileInfo'] = $this->vendorsmodel->getVendorFile($userid, $vendorid);
                $this->data['maincontent'] = $this->load->view('vendor/maincontents/babelbarkuserdetails', $data, true);
            }
            $this->load->view('vendor/layout', $this->data);
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function updateappCustomer() {
        $customerid = $this->input->post('customerid');
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $petid = $this->input->post('petid');
        $customerNotes = (trim($this->input->post('user_notes')));
        $cNotes = array(
            'notes' => $customerNotes,
        );
        $updateAppUserNotes = $this->appusersmodel->updateUser($cNotes, $customerid);
        $petNotes = (trim($this->input->post('pet_notes')));
        $pNotes = array(
            'notes' => $petNotes,
        );
        $updatePetNotes = $this->petmodel->updatePet($pNotes, $petid);
        // Multiple customer file add
        if (file_exists($_FILES['vendorFile']['tmp_name'][0])) {
            $count = count($_FILES['vendorFile']['tmp_name']);
            $datacc = array();
            for ($i = 0; $i < $count; $i++) {
                if (file_exists($_FILES['vendorFile']['tmp_name'][$i]) || is_uploaded_file($_FILES['files']['tmp_name'][$i])) {
                    $filename = $_FILES['vendorFile']['name'][$i];
                    $filetmpname = $_FILES['vendorFile']['tmp_name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $fname = time() . '_' . $petid . "." . $ext;
                    $uploaddir = 'resources/uploads/vendorfiles/';
                    $s3msg = aws_upload($uploaddir, $fname, $filetmpname);
                    if ($s3msg == 'S3 upload successful') {
                        $fileurl = 'resources/uploads/vendorfiles/' . $fname;
                        $datacc[$i] = array(
                            'filename' => $filename,
                            'fileurl' => $fileurl,
                            'customerid' => $customerid,
                            'petid' => $petid,
                            'vendorid' => $vendorid,
                            'status' => 1
                        );
                    }
                }
            }

            $this->vendorsmodel->addFileCustomerPetWise($datacc);
        }

        // Multiple pet file add
        if (file_exists($_FILES['petFile']['tmp_name'][0])) {
            $count = count($_FILES['petFile']['tmp_name']);
            $datacc = array();
            for ($i = 0; $i < $count; $i++) {
                if (file_exists($_FILES['petFile']['tmp_name'][$i]) || is_uploaded_file($_FILES['files']['tmp_name'][$i])) {
                    $filename = $_FILES['petFile']['name'][$i];
                    $filetmpname = $_FILES['petFile']['tmp_name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $fname = time() . '_' . $petid . "." . $ext;
                    $uploaddir = 'resources/uploads/vendorfiles/';
                    $s3msg = aws_upload($uploaddir, $fname, $filetmpname);

                    if ($s3msg == 'S3 upload successful') {
                        $fileurl = 'resources/uploads/vendorfiles/' . $fname;
                        $datacc[$i] = array(
                            'filename' => $filename,
                            'fileurl' => $fileurl,
                            'customerid' => $customerid,
                            'petid' => $petid,
                            'vendorid' => $vendorid,
                            'status' => 2
                        );
                    }
                }
            }
            $this->vendorsmodel->addFileCustomerPetWise($datacc);
        }

        $this->session->set_flashdata('success_message', 'Customer updated.');
        redirect('vendor/customer/listofgenericcustomer');
    }

    function updateCustomer() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $customerid = $this->input->post('customerid');
        $petid = $this->input->post('petid');
        $vetid = $this->input->post('vetid');
        $customerfname = (trim($this->input->post('customerfname')));
        $customerlname = (trim($this->input->post('customerlname')));
        $customeremail = (trim($this->input->post('customeremail')));
        $phoneno = (trim($this->input->post('phoneno')));
        $address = (trim($this->input->post('address')));
        $city = (trim($this->input->post('city')));
        $state = (trim($this->input->post('state')));
        $country = (trim($this->input->post('country')));
        $zipcode = (trim($this->input->post('zipcode')));
        $notes = (trim($this->input->post('notes')));
        $petname = (trim($this->input->post('petname')));
        $birthdate = date('Y-m-d', strtotime(trim($this->input->post('birthdate'))));
        $ispetchipid = ($this->input->post('microchipped'));
        //$ispetchipid=0;
        //if((int) $ispetchipid == 1){
        //$ispetchipid=1;
        //}
        $govtlicensetagno = ($this->input->post('govtlicensetagno'));
        $govtlicensetagissuer = ($this->input->post('govtlicensetagissuer'));
        $isadopted = ($this->input->post('isadopted'));
        $isadoptedval = 0;
        if ((int) $isadopted == 1) {
            $isadoptedval = 1;
        }
        $petcurrentweight = ($this->input->post('petcurrentweight'));
        $pettargetweight = ($this->input->post('pettargetweight'));
        $gender = ($this->input->post('genderchk'));
        if ($this->unittype == 'British') {
            $petcurrentweight = $petcurrentweight / 2.20462;
            $pettargetweight = $pettargetweight / 2.20462;
        }
        $medicalhistory = (trim($this->input->post('medicalhistory')));
        $primarybreed = ($this->input->post('primarybreed'));
        if ($primarybreed == "")
            $primarybreed = null;
        $secondarybreed = ($this->input->post('secondarybreed'));
        if ($secondarybreed == "")
            $secondarybreed = null;
        $primaryfood = ($this->input->post('primaryfood'));
        if ($primaryfood == "")
            $primaryfood = null;
        $secondaryfood = ($this->input->post('secondaryfood'));
        if ($secondaryfood == "")
            $secondaryfood = null;
        $feedamount = (trim($this->input->post('feedamount')));
        $feedfrequency = (trim($this->input->post('feedfrequency')));
        $bdayunsure = ($this->input->post('bdayunsure'));
        $bdayunsureval = 0;
        if ((int) $bdayunsure == 1) {
            $bdayunsureval = 1;
        }
        $vetname = (trim($this->input->post('vetname')));
        $vetphone = (trim($this->input->post('vetphone')));
        $vetemergphone = (trim($this->input->post('vetemergphone')));
        $vetcontactPerson = (trim($this->input->post('vetcontactPerson')));
        $vetaddress = (trim($this->input->post('vetaddress')));
        $vetcity = (trim($this->input->post('vetcity')));
        $vetstate = (trim($this->input->post('vetstate')));
        $vetcountry = (trim($this->input->post('vetcountry')));
        $vetzipcode = (trim($this->input->post('vetzipcode')));
        $fromhoursofoperation = ($this->input->post('fromhoursofoperation'));
        $tohoursofoperation = ($this->input->post('tohoursofoperation'));
        if (is_array($this->input->post('availabilitydays'))) {
            $availabilitydays = implode(",", $this->input->post('availabilitydays'));
        } else {
            $availabilitydays = $this->input->post('availabilitydays');
        }
        //add customer
        $datacc = array(
            'vendorid' => $vendorid,
            'firstname' => $customerfname,
            'lastname' => $customerlname,
            'phoneno' => $phoneno,
            'email' => $customeremail,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zipcode' => $zipcode,
            'notes' => $notes
        );
        $customerid = $this->customermodel->updateCustomer($datacc, $customerid);
        //add pet
        $datacc = array();
        $datacc['name'] = $petname;
        $datacc['medicalhistory'] = $medicalhistory;
        $datacc['primarybreed'] = $primarybreed;
        $datacc['secondarybreed'] = $secondarybreed;
        $datacc['primaryfood'] = $primaryfood;
        $datacc['secondaryfood'] = $secondaryfood;
        $datacc['birthdate'] = $birthdate;
        $datacc['isadopted'] = $isadopted;
        $datacc['microchipped'] = $ispetchipid;
        $datacc['govtlicensetagno'] = $govtlicensetagno;
        $datacc['govtlicensetagissuer'] = $govtlicensetagissuer;
        $date = strtotime(''); //strtotime('0000/00/00');
        $datacc['yearlycheckupdate'] = date('Y-m-d', $date);
        $datacc['approximateage'] = "";
        $datacc['birthdateupdatedon'] = date('Y-m-d', $date);
        $datacc['gender'] = $gender;
        $datacc['currentweight'] = $petcurrentweight;
        $datacc['targetweight'] = $pettargetweight;
        $datacc['amountoffeed'] = $feedamount;
        $datacc['frequencyoffeed'] = $feedfrequency;
        $datacc['birthdayunsure'] = $bdayunsure;
        if ($bdayunsureval == 1) {
            $approxageyrs = (trim($this->input->post('approxageyrs')));
            $approxagemonths = (trim($this->input->post('approxagemonths')));
            $approxage = "";
            if ($approxageyrs != "")
                $approxage = $approxageyrs . "years";
            if ($approxagemonths != "")
                $approxage = $approxage . $approxagemonths . "months";
            $datacc['approximateage'] = $approxage;
            if ($petid == 0)
                $datacc['birthdateupdatedon'] = date('Y-m-d');
        }
        if ($ispetchipid == 1) {
            $petchipid = ($this->input->post('microchipid'));
            $datacc['microchipid'] = $petchipid;
        }
        if ($petid == 0)
            $petid = $this->genratePrimaryKey('40');
        $datacc['petid'] = $petid;
        $affrows = $this->petmodel->updatePet1($datacc, $petid);
        if ($affrows > 0) {
            //add customer pet
            $datacc = array();
            $datacc['customerid'] = $customerid;
            $datacc['petid'] = $petid;
            $this->customermodel->updatepet($datacc, $customerid, $petid);
            //upload photo
            $imgurl = '';
            if (file_exists($_FILES['profileimage']['tmp_name']) || is_uploaded_file($_FILES['profileimage']['tmp_name'])) {
                $allowed = array('gif', 'png', 'jpg', 'jpeg');
                $filename = $_FILES['profileimage']['name'];
                $filetmpname = $_FILES['profileimage']['tmp_name'];
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    //return false;
                } else {
                    $uploaddir = 'resources/uploads/petimage/';
                    $fname = $petid . "." . $ext;
                    $s3msg = aws_upload($uploaddir, $fname, $filetmpname);
                    if ($s3msg == 'S3 upload successful') {
                        $imgurl = 'resources/uploads/petimage/' . $fname;
                    }
                    $datacc = array();
                    $datacc['proilefpicture'] = $imgurl;
                    $this->petmodel->updatePet($datacc, $petid);
                }
            }
            // Multiple pet file add
            if (file_exists($_FILES['petFile']['tmp_name'][0])) {
                $count = count($_FILES['petFile']['tmp_name']);
                $datacc = array();
                for ($i = 0; $i < $count; $i++) {
                    if (file_exists($_FILES['petFile']['tmp_name'][$i]) || is_uploaded_file($_FILES['files']['tmp_name'][$i])) {
                        $filename = $_FILES['petFile']['name'][$i];
                        $filetmpname = $_FILES['petFile']['tmp_name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        $fname = time() . '_' . $petid . "." . $ext;
                        $uploaddir = 'resources/uploads/vendorfiles/';
                        $s3msg = aws_upload($uploaddir, $fname, $filetmpname);

                        if ($s3msg == 'S3 upload successful') {
                            $fileurl = 'resources/uploads/vendorfiles/' . $fname;
                            $datacc[$i] = array(
                                'filename' => $filename,
                                'fileurl' => $fileurl,
                                'customerid' => $customerid,
                                'petid' => $petid,
                                'vendorid' => $vendorid,
                                'status' => 2
                            );
                        }
                    }
                }
                $this->vendorsmodel->addFileCustomerPetWise($datacc);
            }
            // Multiple vendor file add
            if (file_exists($_FILES['vendorFile']['tmp_name'][0])) {
                $count = count($_FILES['vendorFile']['tmp_name']);
                $datacc = array();
                for ($i = 0; $i < $count; $i++) {
                    if (file_exists($_FILES['vendorFile']['tmp_name'][$i]) || is_uploaded_file($_FILES['files']['tmp_name'][$i])) {
                        $filename = $_FILES['vendorFile']['name'][$i];
                        $filetmpname = $_FILES['vendorFile']['tmp_name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        $fname = time() . '_' . $petid . "." . $ext;
                        $uploaddir = 'resources/uploads/vendorfiles/';
                        $s3msg = aws_upload($uploaddir, $fname, $filetmpname);

                        if ($s3msg == 'S3 upload successful') {
                            $fileurl = 'resources/uploads/vendorfiles/' . $fname;
                            $datacc[$i] = array(
                                'filename' => $filename,
                                'fileurl' => $fileurl,
                                'customerid' => $customerid,
                                'petid' => $petid,
                                'vendorid' => $vendorid,
                                'status' => 1
                            );
                        }
                    }
                }
                $this->vendorsmodel->addFileCustomerPetWise($datacc);
            }


            //add medications
            if ($this->input->post('medicinesadded') != "") {
                //delete old medications
                $this->petmedicationmodel->deleteByPetID($petid);
                $medicationjson = $this->input->post('medicinesadded');
                $medobj = json_decode($medicationjson, true);
                foreach ($medobj['medication'] as $item) {
                    $datacc = array();
                    $datacc['petid'] = $petid;
                    $datacc['medicineid'] = $item['medicineid'];
                    $datacc['frequency'] = $item['frequency'];
                    $datacc['dosage'] = $item['dosage'];
                    $datacc['medicationtime'] = "8:00";
                    $datacc['medicationtimings'] = $item['medicationtimings'];
                    $datacc['frequencytimes'] = $item['frequencytimes'];
                    $this->petmedicationmodel->addMedication($datacc);
                }
            }
            // add vaccination
            if ($this->input->post('vaccinesadded') != "") {
                //delete old medications
                $this->petvaccinationmodel->deleteByPetID($petid);
                $vaccinationjson = $this->input->post('vaccinesadded');
                $vacobj = json_decode($vaccinationjson, true);
                foreach ($vacobj['vaccination'] as $item) {
                    $datacc = array();
                    $datacc['petid'] = $petid;
                    $datacc['vaccineid'] = $item['vaccineid'];
                    $datacc['info'] = "-";
                    $duedate = date('Y-m-d', strtotime($item['duedate']));
                    $datacc['duedate'] = $duedate;
                    $this->petvaccinationmodel->addVaccination($datacc);
                }
            }
        }
        //add vet
        if ($vetname != "" || $vetphone != "" || $vetaddress != "") {
            $datacc = array(
                'name' => $vetname,
                'phoneno' => $vetphone,
                'emergencyno' => $vetemergphone,
                'contactperson' => $vetcontactPerson,
                'fromhoursofoperation' => $fromhoursofoperation,
                'tohoursofoperation' => $tohoursofoperation,
                'availabilitydays' => $availabilitydays,
                'address' => $vetaddress,
                'city' => $vetcity,
                'state' => $vetstate,
                'country' => $vetcountry,
                'zipcode' => $vetzipcode
            );
            $vetid = $this->veterinarianmodel->updateVet($datacc, $vetid);
            //update customer for vetid
            $datacc = array();
            $datacc['veterinarianid'] = $vetid;
            $this->customermodel->updateCustomer($datacc, $customerid);
        }
        $this->session->set_flashdata('success_message', 'Customer updated.');
        redirect('vendor/customer/listofgenericcustomer');
    }

    function deleteCustomer($customerid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $user = "Generic Customer";
        $customerstatus = $this->customermodel->customerStatus($customerid, $vendorid, $user);
        if ($customerstatus == TRUE) {
            $customerpets = $this->customermodel->getPets($customerid);
            if ($customerpets > 1) {
                $customerpetid = $this->customermodel->getPetIDs($customerid);
                //delete customerpets
                for ($j = 0; $j < $customerpets; $j++) {
                    ///delete petmedication
                    $this->customermodel->deleteCustomerPetMedications($customerpetid[$j]['petid']);
                    $this->customermodel->deleteCustomerPetVaccinations($customerpetid[$j]['petid']);
                    //delete pet
                    $this->customermodel->deletePet($customerpetid[$j]['petid']);
                }
            } else {
                $customerpetid = $this->customermodel->getPetID($customerid);
                $this->customermodel->deleteCustomerPetMedications($customerpetid);
                $this->customermodel->deleteCustomerPetVaccinations($customerpetid);
                //delete pet
                $this->customermodel->deletePet($customerpetid);
            }
            //delete customer
            $this->customermodel->deleteCustomerPets($customerid);
            $this->customermodel->deleteCustomer($customerid);
            $query = "SET foreign_key_checks = 1";
            $this->db->query($query);
            $msg = "Customer deleted successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/customer/listofgenericcustomer');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function deleteAppuser($appuserid) {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $user = "BabelBark Customer";
        $customerstatus = $this->customermodel->customerStatus($appuserid, $vendorid, $user);
        if ($customerstatus == TRUE) {
            $this->appusersmodel->deleteAppUser($appuserid);
            $msg = "BabelBark User deleted successfully";
            $this->session->set_flashdata('success_message', $msg);
            redirect('vendor/customer/listofgenericcustomer');
        } else {
            $data = array();
            $this->load->view('vendor/error-403', $this->data);
        }
    }

    function downloadCustomers($vendorid) {
        ///echo $vendorid;
        $customers = array();
        $appusers = array();
        $customers_new = array();
        $customerType = 'all'; //changed as per requirement
        if ($customerType == 'all' || $customerType == 'generic') {
            $customers = $this->customermodel->getAllCustomers($vendorid);
            for ($i = 0; $i < count($customers); $i++) {
                $customerid = $customers[$i]['customerid'];
                $customerpets = $this->customermodel->getPets($customerid);
                if ($customerpets > 1) {
                    $customerpetids = $this->customermodel->getPetIDs($customerid);
                    $customerpetname = "";
                    $customerpetBD = "";
                    $count = 0;
                    for ($j = 0; $j < $customerpets; $j++) {
                        $petdetail = $this->petmodel->getPetDetails($customerpetids[$j]['petid']);
                        $count++;
                        $customerpetname = $customerpetname . "" . $count . "-" . $petdetail['name'] . "  ";
                        $customerpetBD = $customerpetBD . "" . $count . "-" . $petdetail['birthdate'] . "  ";
                    }
                } else {
                    $customerpetids = $this->customermodel->getPetIDs($customerid);
                    $petdetail = $this->petmodel->getPetDetails($customerpetids[0]['petid']);
                    $customerpetname = $petdetail['name'];
                    $customerpetBD = $petdetail['birthdate'];
                }
                /* $customerpetid=$this->customermodel->getPetID($customerid);
                  $petdetail=$this->petmodel->getPetDetails($customerpetid); */
                $customers_new[$i]['Id'] = $customerid;
                $customers_new[$i]['FirstName'] = $customers[$i]['firstname'];
                $customers_new[$i]['LastName'] = $customers[$i]['lastname'];
                $customers_new[$i]['Email'] = $customers[$i]['email'];
                $customers_new[$i]['Contact'] = $customers[$i]['phoneno'];
                $customers_new[$i]['Address'] = $customers[$i]['address'];
                $customers_new[$i]['City'] = $customers[$i]['city'];
                $customers_new[$i]['State'] = $customers[$i]['state'];
                $customers_new[$i]['Country'] = $customers[$i]['country'];
                $customers_new[$i]['Zipcode'] = $customers[$i]['zipcode'];
                //$customers_new[$i]['UpdatedOn']=$customers[$i]['updatedon'];
                //$customers_new[$i]['Notes']=$customers[$i]['notes'];
                //$customers_new[$i]['PetID']=$customerpetid;
                $customers_new[$i]['DogName'] = $customerpetname;
                $customers_new[$i]['DogBirthDay'] = $customerpetBD;
                $customers_new[$i]['Type'] = "Generic Customer";
            }
        }
        if ($customerType == 'all' || $customerType == 'babelbark') {
            $babelbarkusers = $this->appuservendorsmodel->getUsersForVendor($vendorid);
            if (count($babelbarkusers) > 0) {
                for ($i = 0; $i < count($babelbarkusers); $i++) {
                    $appuserid = $babelbarkusers[$i]['appuserid'];
                    $appuser1 = $this->appusersmodel->fetchUserDetails2($appuserid);
                    $appuserpets = $this->appusersmodel->getPets($appuserid);
                    $petname = "";
                    $petDB = "";
                    $count = 0;
                    if ($appuserpets > 1) {
                        $appuserpetid = $this->appusersmodel->getPetIDs($appuserid);
                        for ($i = 0; $i < $appuserpets; $i++) {
                            $petdetail = $this->petmodel->getPetDetails($appuserpetid[$i]['petid']);
                            $count++;
                            $petname = $petname . "" . $count . "-" . $petdetail['name'] . "  ";
                            $petDB = $petDB . "" . $count . "-" . $petdetail['birthdate'] . "  ";
                        }
                    } else {
                        $appuserpetid = $this->appusersmodel->getPetIDs($appuserid);
                        $petdetail = $this->petmodel->getPetDetails($appuserpetid);
                        $petname = $petdetail['name'];
                        $petDB = $petdetail['birthdate'];
                    }
                    /* $appuserpetid=$this->appusersmodel->getPetID($appuserid);
                      $petdetail=$this->petmodel->getPetDetails($appuserpetid); */
                    $appuser['Id'] = $appuserid;
                    $appuser['FirstName'] = $appuser1['firstname'];
                    $appuser['LastName'] = $appuser1['lastname'];
                    $appuser['Email'] = $appuser1['email'];
                    $appuser['Contact'] = $appuser1['phonenumber'];
                    $appuser['Address'] = $appuser1['address'];
                    $appuser['City'] = $appuser1['city'];
                    $appuser['State'] = $appuser1['state'];
                    $appuser['Country'] = $appuser1['country'];
                    $appuser['Zipcode'] = $appuser1['zip'];
                    //$appuser['UpdatedOn']=$appuser1['updatedon'];
                    //$appuser['Notes']="";
                    //$appuser['PetID']=$appuserpetid;
                    $appuser['PetName'] = $petname;
                    $appuser['DogBirthDay'] = $petDB;
                    $appuser['Type'] = 'BabelBark User';
                    $appusers[$i] = $appuser;
                }
            }
        }
        $commonArray = array_merge($customers_new, $appusers);
        $fileName = 'BizBarkCustomers-' . $vendorid . "-" . date('d-m-Y') . '.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");
        $fh = @fopen('php://output', 'w');
        $headerDisplayed = false;
        foreach ($commonArray as $data) {
            // Add a header row if it hasn't been added yet
            if (!$headerDisplayed) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }
            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;
    }

    function checkInvitedUser() {
        $useremail = trim($this->input->post('email'));
        $userid = $this->appusersmodel->fetchUserID($useremail);
        if ($userid == 0) {
            echo 'newuser';  //user doesnt exist
        } else {
            echo 'existinguser';
        }
    }

    function inviteUserPriviewEmail() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $sendmessage = file_get_contents('resources/invitemail_template.html');
        $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
        $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
        echo $sendmessage;
    }

    function inviteUser() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $toemail = trim($this->input->post('email'));
        $subject = "Invitation to BabelBark";
        // To send HTML mail, the Content-type header must be set.
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: BabelBark ' . SITE_MAIL_BABEL . "\r\n"; // Sender's Email
        $headers .= 'cc: BabelBark ' . SITE_MAIL_SUPPORT_BABEL . "\r\n";
        $sendmessage = file_get_contents('resources/invitemail_template.html');
        $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
        $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
        //mail($toemail, $subject, $sendmessage, $headers);
        //send_template_mail('',$sendmessage, $subject, $toemail, SITE_MAIL_BABEL);
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = SITE_MAIL_BABEL;
        $config['smtp_pass'] = "doogyWOOGY!";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
        $this->email->from(SITE_MAIL_BABEL, 'BabelBark');
        $this->email->to($toemail);
        $this->email->cc($vendoremail);
        $this->email->reply_to(SITE_MAIL_SUPPORT_BABEL, 'BabelBark');
        $this->email->subject($subject);
        //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
        $this->email->message($sendmessage);
        $result = $this->email->send();
        echo '1';
    }

    function profileImportPriviewEmail() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $companyname = $admin['adminDetails']['comapnyname'];
        $vendorlogoimage = awsBucketPath.$admin['adminDetails']['logoimage'];

        $toemail = trim($this->input->post('email'));
        $userid = $this->appusersmodel->fetchUserID($toemail);
        if ($userid == 0) {
            echo '2';  //user doesnt exist
        } else {
            $userdata = $this->appusersmodel->fetchUserDetails($userid);
            $appuserpets = $this->appusersmodel->getPets($userid);
            $petname = "";
            $count = 0;

            $dom = new DOMDocument('1.0');
            $ul = $dom->createElement('ul');
            $dom->appendChild($ul);

            if ($appuserpets > 1) {
                $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                for ($i = 0; $i < $appuserpets; $i++) {
                    $petdetail = $this->petmodel->getPetDetails($appuserpetid[$i]['petid']);
                    $count++;
                    $comma = "";
                    if($i<($appuserpets-1)){
                        $comma = ", ";
                    }
                    $petname = $petname.$petdetail['name'].$comma;
                    $li = $dom->createElement('li');
                    $e = $dom->createElement('a',$petdetail['name']);
                    $a = $li->appendChild($e);
                    $url = '#';
                    $a->setAttribute('href',$url);
                    $ul->appendChild($li);
                }
            } else {
                $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                $petdetail = $this->petmodel->getPetDetails($appuserpetid);
                $petname = $petdetail['name'];
                $li = $dom->createElement('li');
                $e = $dom->createElement('a',$petdetail['name']);
                $a = $li->appendChild($e);
                $url = '#';
                $a->setAttribute('href',$url);
                $ul->appendChild($li);
            }
            $dom->appendChild($ul);
            $pets = $dom->savehtml();

            $sendmessage = file_get_contents('resources/invitemail_profimport_template.html');
            $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
            $sendmessage = str_replace('%company%', $companyname, $sendmessage);
            $sendmessage = str_replace('%petname%', $petname, $sendmessage);
            $sendmessage = str_replace('%pets%', $pets, $sendmessage);
            $sendmessage = str_replace('%vendorlogoimage%', $vendorlogoimage, $sendmessage);
            echo $sendmessage;
        }
    }

    function inviteUserForProfileImport() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $vendoremail = $admin['adminDetails']['email'];
        $vendorfname = $admin['adminDetails']['firstname'];
        $vendorlname = $admin['adminDetails']['lastname'];
        $companyname = $admin['adminDetails']['comapnyname'];
        $vendorlogoimage = awsBucketPath.$admin['adminDetails']['logoimage'];

        $toemail = trim($this->input->post('email'));
        $userid = $this->appusersmodel->fetchUserID($toemail);
        if ($userid == 0) {
            echo '2';  //user doesnt exist
        } else {
            $invitationexist = $this->invitationtokenmodel->isInvitationExist($userid, $vendorid);
            if ($invitationexist) {

                //store it in DB
                $datacc = array();
                $sessiontoken = $this->genrateSessionToken();
                $datacc['appuserid'] = $userid;
                $datacc['token'] = $sessiontoken;
                $datacc['fromvendorid'] = $vendorid;
                $this->invitationtokenmodel->addInvitation($datacc);
                //$url = base_url() . 'acceptinvitation?token=' . $sessiontoken;

                $userdata = $this->appusersmodel->fetchUserDetails($userid);
                $appuserpets = $this->appusersmodel->getPets($userid);
                $petname = "";
                $count = 0;
                $dom = new DOMDocument('1.0');
                $ul = $dom->createElement('ul');
                $dom->appendChild($ul);

                if ($appuserpets > 1) {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    for ($i = 0; $i < $appuserpets; $i++) {
                        $petdetail = $this->petmodel->getPetDetails($appuserpetid[$i]['petid']);
                        $count++;
                        $comma = "";
                        if($i<($appuserpets-1)){
                            $comma = ", ";
                        }
                        $petname = $petname.$petdetail['name'].$comma;
                        $li = $dom->createElement('li');
                        $e = $dom->createElement('a',$petdetail['name']);
                        $a = $li->appendChild($e);
                        $url = base_url() . 'acceptinvitation?token=' . $sessiontoken;
                        $a->setAttribute('href',$url);
                        $ul->appendChild($li);
                    }
                } else {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    $petdetail = $this->petmodel->getPetDetails($appuserpetid);
                    $petname = $petdetail['name'];
                    $li = $dom->createElement('li');
                    $e = $dom->createElement('a',$petdetail['name']);
                    $a = $li->appendChild($e);
                    $url = base_url() . 'acceptinvitation?token=' . $sessiontoken;
                    $a->setAttribute('href',$url);
                    $ul->appendChild($li);
                }
                $dom->appendChild($ul);
                $pets = $dom->savehtml();

                $subject = "BabelBark request for profile import";
                // To send HTML mail, the Content-type header must be set.
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From:' . $vendoremail . "\r\n"; // Sender's Email
                $sendmessage = file_get_contents('resources/invitemail_profimport_template.html');
                $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
                $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
                $sendmessage = str_replace('%company%', $companyname, $sendmessage);
                $sendmessage = str_replace('%petname%', $petname, $sendmessage);
                $sendmessage = str_replace('%pets%', $pets, $sendmessage);
                $sendmessage = str_replace('%vendorlogoimage%', $vendorlogoimage, $sendmessage);

                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL_BABEL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_BABEL, 'BabelBark');
                $this->email->to($toemail);
                $this->email->reply_to(SITE_MAIL_SUPPORT_BABEL, 'BabelBark');
                $this->email->subject($subject);
                $this->email->message($sendmessage);
                $result = $this->email->send();

                $vendorSubject = "BabelBark request for invitation";
                // To send HTML mail, the Content-type header must be set.
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From:' . $vendoremail . "\r\n"; // Sender's Email
                $vendormessage = file_get_contents('resources/vendor_invitation.html');
                $vendormessage = str_replace('%toemail%', $toemail, $vendormessage);
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL_BABEL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_BABEL, 'BabelBark');
                $this->email->to($vendoremail);
                $this->email->reply_to(SITE_MAIL_SUPPORT_BABEL, 'BabelBark');
                $this->email->subject($vendorSubject);
                $this->email->message($vendormessage);

                $result = $this->email->send();
                echo '1';
            } else {
                //store it in DB
                $datacc = array();
                $sessiontoken = $this->genrateSessionToken();
                $datacc['appuserid'] = $userid;
                $datacc['token'] = $sessiontoken;
                $datacc['fromvendorid'] = $vendorid;
                $this->invitationtokenmodel->addInvitation($datacc);

                $userdata = $this->appusersmodel->fetchUserDetails($userid);
                $appuserpets = $this->appusersmodel->getPets($userid);
                $petname = "";
                $count = 0;
                $dom = new DOMDocument('1.0');
                $ul = $dom->createElement('ul');
                $dom->appendChild($ul);
                if ($appuserpets > 1) {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    for ($i = 0; $i < $appuserpets; $i++) {
                        $petdetail = $this->petmodel->getPetDetails($appuserpetid[$i]['petid']);
                        $count++;
                        $comma = "";
                        if($i<($appuserpets-1)){
                            $comma = ", ";
                        }
                        $petname = $petname.$petdetail['name'].$comma;
                        $li = $dom->createElement('li');
                        $e = $dom->createElement('a',$petdetail['name']);
                        $a = $li->appendChild($e);
                        $url = base_url() . 'acceptinvitation?token=' . $sessiontoken;
                        $a->setAttribute('href',$url);
                        $ul->appendChild($li);
                    }
                } else {
                    $appuserpetid = $this->appusersmodel->getPetIDs($userid);
                    $petdetail = $this->petmodel->getPetDetails($appuserpetid);
                    $petname = $petdetail['name'];
                    $li = $dom->createElement('li');
                    $e = $dom->createElement('a',$petdetail['name']);
                    $a = $li->appendChild($e);
                    $url = base_url() . 'acceptinvitation?token=' . $sessiontoken;
                    $a->setAttribute('href',$url);
                    $ul->appendChild($li);
                }
                $dom->appendChild($ul);
                $pets = $dom->savehtml();


                $subject = "BabelBark request for profile import";
                // To send HTML mail, the Content-type header must be set.
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From:' . $vendoremail . "\r\n"; // Sender's Email
                $sendmessage = file_get_contents('resources/invitemail_profimport_template.html');
                $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
                $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
                $sendmessage = str_replace('%company%', $companyname, $sendmessage);
                $sendmessage = str_replace('%petname%', $petname, $sendmessage);
                $sendmessage = str_replace('%pets%', $pets, $sendmessage);
                $sendmessage = str_replace('%vendorlogoimage%', $vendorlogoimage, $sendmessage);

                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL_BABEL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_BABEL, 'BabelBark');
                $this->email->to($toemail);
                //$this->email->cc($vendoremail);
                $this->email->reply_to(SITE_MAIL_SUPPORT_BABEL, 'BabelBark');
                $this->email->subject($subject);
                //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
                $this->email->message($sendmessage);
                $result = $this->email->send();
                $vendorSubject = "BabelBark request for invitation";
                // To send HTML mail, the Content-type header must be set.
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From:' . $vendoremail . "\r\n"; // Sender's Email
                $vendormessage = file_get_contents('resources/vendor_invitation.html');
                $vendormessage = str_replace('%toemail%', $toemail, $vendormessage);
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL_BABEL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_BABEL, 'BabelBark');
                $this->email->to($vendoremail);
                $this->email->reply_to(SITE_MAIL_SUPPORT_BABEL, 'BabelBark');
                $this->email->subject($vendorSubject);
                $this->email->message($vendormessage);
                $result = $this->email->send();
                echo '1';
            }
        }
    }

    function uploadCSV() {
        $data = array();
        $this->data['maincontent'] = $this->load->view('vendor/maincontents/upload-customer-CSV', $data, true);
        $this->load->view('vendor/layout', $this->data);
    }

    function importCSV() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        if (file_exists($_FILES['csvfile']['tmp_name']) || is_uploaded_file($_FILES['csvfile']['tmp_name'])) {
            $allowed = array('csv');
            $filename = $_FILES['csvfile']['name'];
            $filetmpname = $_FILES['csvfile']['tmp_name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                $this->session->set_flashdata('error_message', 'Please upload CSV file.');
            } else {
                /*                 * *********************************** S3 Integration  **************************** */
                /* $num=0;
                  $uploaddir = 'resources/uploads/customercsv/';
                  $s3msg = aws_upload($uploaddir,$filename,$filetmpname);
                  $uploadfile = $uploaddir ."/". $filename;
                  if($s3msg=='S3 upload successful'){ */
                /*                 * *********************************** End S3 Integration  **************************** */
                if (!file_exists('resources/uploads/customercsv/')) {
                    mkdir('resources/uploads/customercsv/');
                }
                $uploaddir = realpath('./resources/uploads/customercsv/');
                $uploadfile = $uploaddir . "/" . $filename;
                $num = 0;
                if (move_uploaded_file($_FILES['csvfile']['tmp_name'], $uploadfile)) {
                    if (($handle = fopen($uploadfile, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                            $num = count($data);
                        }
                        fclose($handle);
                        if ($num == 11) {
                            $customerdata = array();
                            $file_handle = fopen($uploadfile, 'r');
                            while (($result = fgetcsv($file_handle)) !== false) {
                                //if (array(null) !== $result) { // ignore blank lines
                                if (count(array_filter($result)) != 0) {
                                    $customerdata[] = $result;
                                }
                            }
                            /* while (!feof($file_handle) ) {
                              $customerdata[] = fgetcsv($file_handle, 1024);
                              } */
                            fclose($file_handle);
                            //print_r($customerdata);
                            // echo "count".count($customerdata);
                            $uploadsuccess = true;
                            $emptyfields = true;
                            $emailvalidation = true;
                            $phonenovalidation = true;
                            $countrysuccess = true;
                            $statesuccess = true;
                            $zipcodevalidation = true;
                            for ($i = 1; $i < count($customerdata); $i++) { //skipping header
                                $customerfirstname = $customerdata[$i][0];
                                $customerlastname = $customerdata[$i][1];
                                $petname = $customerdata[$i][2];
                                $customeremail = $customerdata[$i][3];
                                $phone = $customerdata[$i][4];
                                $address = $customerdata[$i][5];
                                $city = $customerdata[$i][6];
                                $state = $customerdata[$i][7];
                                $country = $customerdata[$i][8];
                                $zipcode = $customerdata[$i][9];
                                $notes = $customerdata[$i][10];
                                $linenumber = $i;
                                if ($customerfirstname == "" || $customerfirstname == " " || $customerlastname == "" || $customerlastname == " " || $petname == "" || $petname == " ") {
                                    $uploadsuccess = false;
                                    $emptyfields = false;
                                    break;
                                }
                                /* if (!filter_var($customeremail, FILTER_VALIDATE_EMAIL)) {
                                  $uploadsuccess=false;
                                  $emailvalidation=false;
                                  break;
                                  } */
                                if ($phone != "" && $phone != " ") {
                                    if (!preg_match('/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/', $phone)) {
                                        $uploadsuccess = false;
                                        $phonenovalidation = false;
                                        break;
                                    }
                                }
                                if ($country != "United States" && $country != "Canada") {
                                    $uploadsuccess = false;
                                    $countrysuccess = false;
                                    break;
                                }
                                if ($country == "United States") {
                                    $count = 0;
                                    $USstatesdecode = json_decode(USSTATES);
                                    for ($j = 0; $j < count($USstatesdecode); $j++) {
                                        if ($state == $USstatesdecode[$j]) {
                                            $count++;
                                        }
                                    }
                                    if ($count != 1) {
                                        $uploadsuccess = false;
                                        $statesuccess = false;
                                        break;
                                    }
                                    if ($zipcode != "" && $zipcode != " ") {
                                        if (strlen($zipcode) != 5 || !preg_match('/^\d+$/', $zipcode)) {
                                            $uploadsuccess = false;
                                            $zipcodevalidation = false;
                                            break;
                                        }
                                    }
                                } else if ($country == "Canada") {
                                    $count = 0;
                                    $CANADAstatesdecode = json_decode(CANADASTATES);
                                    for ($j = 0; $j < count($CANADAstatesdecode); $j++) {
                                        if ($state == $CANADAstatesdecode [$j]) {
                                            $count++;
                                        }
                                    }
                                    if ($count != 1) {
                                        $uploadsuccess = false;
                                        $statesuccess = false;
                                        break;
                                    }
                                    if ($zipcode != "" && $zipcode != " ") {
                                        if (!preg_match('/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/', $zipcode)) {
                                            $uploadsuccess = false;
                                            $zipcodevalidation = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if ($uploadsuccess) {
                                for ($i = 1; $i < count($customerdata); $i++) { //skipping header
                                    $customerfirstname = $customerdata[$i][0];
                                    $customerlastname = $customerdata[$i][1];
                                    $petname = $customerdata[$i][2];
                                    $customeremail = $customerdata[$i][3];
                                    $phone = $customerdata[$i][4];
                                    $address = $customerdata[$i][5];
                                    $city = $customerdata[$i][6];
                                    $state = $customerdata[$i][7];
                                    $country = $customerdata[$i][8];
                                    $zipcode = $customerdata[$i][9];
                                    $notes = $customerdata[$i][10];
                                    //add customer
                                    $datacc = array(
                                        'vendorid' => $vendorid,
                                        'firstname' => $customerfirstname,
                                        'lastname' => $customerlastname,
                                        'phoneno' => $phone,
                                        'email' => $customeremail,
                                        'address' => $address,
                                        'city' => $city,
                                        'state' => $state,
                                        'country' => $country,
                                        'zipcode' => $zipcode,
                                        'notes' => $notes
                                    );
                                    $customerid = 0;
                                    $customerid = $this->customermodel->updateCustomer($datacc, $customerid);
                                    //add pet
                                    $petid = $this->genratePrimaryKey('40');
                                    while ($this->petmodel->petIdExists($petid) == TRUE) {
                                        $petid = $this->genratePrimaryKey('50');
                                    }
                                    $datacc = array(
                                        'petid' => $petid,
                                        'name' => $petname
                                    );
                                    $affrows = $this->petmodel->updatePet1($datacc, $petid);
                                    if ($affrows > 0) {
                                        //add customer pet
                                        $datacc = array();
                                        $datacc['customerid'] = $customerid;
                                        $datacc['petid'] = $petid;
                                        $this->customermodel->updatepet($datacc, $customerid, $petid);
                                    }
                                }
                                $this->session->set_flashdata('success_message', 'CSV uploaded successfully!');
                                unlink($uploadfile);
                            } else {
                                if (!$emptyfields) {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': At least one row is missing information for the required fields of First Name, Last Name, or Pet Name. Please complete these fields and try again! ');
                                } else if (!$statesuccess) {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': State values must be full state name. Country Value is required if State value is included!');
                                } else if (!$emailvalidation) {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': Email ' . $customeremail . ' is not valid. Please Enter correct your email. And try Again!');
                                } else if (!$zipcodevalidation) {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': Zipcode ' . $zipcode . ' is not valid according your state. Please Enter correct zipcode. And try Again!');
                                } else if (!$phonenovalidation) {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': Phone Number ' . $phone . ' is not in valid formate. Please Enter your phone number in valid formate. And try Again!');
                                } else {
                                    $this->session->set_flashdata('error_message', 'Line # ' . $linenumber . ': Country values must be either United States or Canada!');
                                }
                            }
                        } else {
                            $this->session->set_flashdata('error_message', 'Please upload CSV file with correct number of fields (delete extra columns if any)');
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error_message', 'Please upload CSV file.');
        }
        redirect('vendor/customer/uploadCSV');
    }

    function getRegionalVets() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $searchedvets = array();
        $mapapikey = "AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis";
        $vendoraddress = $admin['adminDetails']['address'] + "," + $admin['adminDetails']['city'] + "," + $admin['adminDetails']['state'] + "," + $admin['adminDetails']['country'] + "," + $admin['adminDetails']['zipcode'];
        $vendoraddress = str_replace(" ", "+", $vendoraddress);
        $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$vendoraddress&sensor=false");
        $json = json_decode($json);
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        $latlng = $lat . "," . $long;
        $vetdetailurl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" . $admin['adminDetails']['addresslatlng'] . "&keyword=veterinarian&rankBy=distance&sensor=false&key=" . $mapapikey;
        $vetdetailjson = file_get_contents($vetdetailurl);
        $nearbyvetsdata = json_decode(str_replace("&quot;", "\"", htmlentities($vetdetailjson)), true);
        $serchdetailarr = array();
        $serchdetailarr = $nearbyvetsdata['results'];
        for ($i = 0; $i < count($serchdetailarr); $i++) {
            $placeid = $serchdetailarr[$i]['place_id'];
            $placedetailurl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $placeid . "&key=" . $mapapikey;
            $detailjson = file_get_contents($placedetailurl);
            $placedata = json_decode(str_replace("&quot;", "\"", htmlentities($detailjson)), true);
            $formatted_address = $placedata['result']['formatted_address'];
            $searchedvets[$i]['id'] = "";
            $searchedvets[$i]['name'] = $placedata['result']['name'];
            $searchedvets[$i]['address'] = $formatted_address;
            $addresscomp = $placedata['result']['address_components'];
            $city = "";
            $address = "";
            $state = "";
            $zip = "";
            for ($j = 0; $j < count($addresscomp); $j++) {
                if ($addresscomp[$j]['types'][0] == "route") {
                    $address = $addresscomp[$j]['long_name'];
                }
                if ($addresscomp[$j]['types'][0] == "locality") {
                    $city = $addresscomp[$j]['long_name'];
                }
                if ($addresscomp[$j]['types'][0] == "administrative_area_level_1") {
                    $state = $addresscomp[$j]['long_name'];
                }
                if ($addresscomp[$j]['types'][0] == "postal_code") {
                    $zip = $addresscomp[$j]['long_name'];
                }
            }
            $searchedvets[$i]['streetaddress'] = $address;
            $searchedvets[$i]['city'] = $city;
            $searchedvets[$i]['state'] = $state;
            $searchedvets[$i]['zip'] = $zip;
            if (isset($placedata['result']['international_phone_number']))
                $searchedvets[$i]['phoneno'] = $placedata['result']['international_phone_number'];
            else
                $searchedvets[$i]['phoneno'] = "";
            if (isset($placedata['result']['website']))
                $searchedvets[$i]['website'] = $placedata['result']['website'];
            else
                $searchedvets[$i]['website'] = "N/A";
            $opennow = "false";
            if (isset($serchdetailarr[$i]['opening_hours'])) {
                $searchedvets[$i]['openinghours'] = $serchdetailarr[$i]['opening_hours'];
                $openinghours = $serchdetailarr[$i]['opening_hours'];
                $opennow = $openinghours['open_now'];
            } else
                $searchedvets[$i]['openinghours'] = "";
            $searchedvets[$i]['opennow'] = $opennow;
            if (isset($serchdetailarr[$i]['rating']))
                $searchedvets[$i]['rating'] = $serchdetailarr[$i]['rating'];
            else
                $searchedvets[$i]['rating'] = "N/A";
        }
        $data['vetlist'] = $searchedvets;
        $this->load->view('vendor/maincontents/vetslist', $data);
    }

    function getPetFile() {
        $vendorId = $_POST['vendorId'];
        $petId = $_POST['petId'];
        $petFileInfo = $this->vendorsmodel->getPetFile($petId, $vendorId);
        foreach ($petFileInfo as $petRows){
            echo '<tr id="petRow'.$petRows['id'].'">
                                                                <td>'.$petRows['filename'].'</td>
                                                                <td>'.$petRows['updatedon'].'</td>
                                                                <td><a href='.base_url().'view_vendor_file/'.$petRows['id'].' target="_blank">View</td>
                                                                <td>
                                                                    <a style="cursor: pointer" onclick="javascript:deletePetFile('.$petRows['id'].','.$petRows['vendorid'].','.$petRows['petid'].')">
                                                                        <span class="label label-danger">Delete</span>
                                                                    </a>
                                                                </td>
                                                            </tr>';
        }

    }

    function deletePetFile() {
        $petFileId = $_POST['petFileId'];
        $vendorId = $_POST['vendorId'];
        $petId = $_POST['petId'];
        $isDeleted = $this->vendorsmodel->deleteFile($petFileId);
        $noOfFile = $this->vendorsmodel->getPetFile($petId, $vendorId);
        if ($isDeleted > 0) {
            $arr = array(
                'msg' => 'File deleted successfully',
                'noOfFile' => count($noOfFile)
            );
            echo json_encode($arr);
        } else {
            echo 'File delete failed';
        }
    }

    function deleteVendorFile() {
        $vendorFileId = $_POST['vendorFileId'];
        $vendorId = $_POST['vendorId'];
        $petId = $_POST['petId'];
        $isDeleted = $this->vendorsmodel->deleteFile($vendorFileId);
        $noOfFile = $this->vendorsmodel->getVendorFile($petId, $vendorId);
        if ($isDeleted > 0) {
            $arr = array(
                'msg' => 'File deleted successfully',
                'noOfFile' => count($noOfFile)
            );
            echo json_encode($arr);
        } else {
            echo 'File delete failed';
        }
    }

    function uploadPetFile() {
        $admin = $this->session->userdata('admin');
        $vendorid = $admin['adminDetails']['vendorid'];
        $customerid = $this->input->post('customerid');
        $petid = $this->input->post('petid');
        if (file_exists($_FILES['petFile']['tmp_name']) || is_uploaded_file($_FILES['petFile']['tmp_name'])) {
            $filename = $_FILES['petFile']['name'];
            $filetmpname = $_FILES['petFile']['tmp_name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $uploaddir = 'resources/uploads/vendorfiles/';
            $fname = time() . '_' . $petid . "." . $ext;
            $s3msg = aws_upload($uploaddir, $fname, $filetmpname);
            if ($s3msg == 'S3 upload successful') {
                $fileurl = 'resources/uploads/vendorfiles/' . $fname;
                $datacc = array();
                $datacc['filename'] = $filename;
                $datacc['fileurl'] = $fileurl;
                $datacc['customerid'] = $customerid;
                $datacc['petid'] = $petid;
                $datacc['vendorid'] = $vendorid;
                $this->vendorsmodel->addFileCustomerPetWise($datacc);
                $this->session->set_flashdata('success_message', 'Customer updated.');
            } else {
                $this->session->set_flashdata('error_message', 'Customer updated failed.');
            }
            redirect('vendor/customer/listofgenericcustomer');
        }
    }

    public function ajax_update_customer($step = 0) {

        $step = intval($step);
        $update_data = array();

        $response = '';
        $customerID = intval($this->input->post('customerid'));

        switch ($step) {
            case 1:
                $update_data = array(
                    'firstname' => $this->input->post('fname'),
                    'lastname' => $this->input->post('lname'),
                    'phoneno' => $this->input->post('phoneno'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'country' => $this->input->post('country'),
                    'zipcode' => $this->input->post('zipcode'),
                    'notes' => $this->input->post('notes')
                );

                $cID = $this->customermodel->updateCustomer($update_data, $customerID, TRUE);
                if ($cID == $customerID) {
                    $response = 'success';
                }

                break;
            case 2:

                break;

            default:
                break;
        }

        echo $response;
        exit;
    }

}
