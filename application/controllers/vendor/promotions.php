<?php
/**
 * @property appusersmodel               $appusersmodel
 * @property appuserdevicesmodel         $appuserdevicesmodel
 * @property petmodel                    $petmodel
 * @property petmedicationmodel          $petmedicationmodel
 * @property petvaccinationmodel         $petvaccinationmodel
 * @property petgoalsmodel               $petgoalsmodel
 * @property petlogmodel                 $petlogmodel
 * @property veterinarianmodel           $veterinarianmodel
 * @property petfeedmodel                $petfeedmodel
 * @property petbreedmodel               $petbreedmodel
 * @property vendorsmodel                $vendorsmodel
 * @property appuservendorsmodel         $appuservendorsmodel
 * @property promotionsmodel             $promotionsmodel
 * @property mailnotifymodel             $mailnotifymodel
 * @property nonregistervendormodule     $nonregistervendormodule
 * @property appointmentmodel            $appointmentmodel
 * @property servicemodel                $servicemodel
 * @property customermodel               $customermodel
 * @property medicalfilesmodel           $medicalfilesmodel
 * @property sikkapetimportrequestsmodel $sikkapetimportrequestsmodel
 */
class Promotions extends CI_Controller
{
    var $data=array();
    function __construct()
    {
        parent::__construct();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('image_lib');
        $this->load->model('petbreedmodel');
        $this->load->model('promotionsmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('appusersmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('appuserpromotionmodel');
        $this->load->model('settingsmodel');

        $this->locations=array('BabelBark App', 'Social Media','Craigslist', 'Local Classifieds', 'Fliers', 'Website', 'Other');


        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $settingdetails = $this->settingsmodel->getDetails1($vendorid);
        if ($settingdetails == "")
            $this->feedunit = "kilos";
        else
        $this->feedunit = $settingdetails['feedingunit'];
        $this->unittype=$settingdetails['unittype'];
        $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);
        $categorystr=$vendordetails['category'];
        $selcategarr=explode(",", $categorystr);
        $servicetypes=$this->vendorcategoriesmodel->getVendorCategories();
        $this->vendorscateg=array();
        for($i=0;$i<count($servicetypes);$i++)
        {
            $categid=$servicetypes[$i]['categoryid'];
            if(in_array($categid, $selcategarr))
            {
                $this->vendorscateg[]=$servicetypes[$i];
            }

        }
    }

    private static function genratePrimaryKey($serverid)
    {
        $primarykey=$serverid.time().mt_rand(1000, 9999);
        if($primarykey>9223372036854775807) //max of 64 bit int
        {
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }
    function listing()
    {
        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];

        /*  //pagignation control
            $config['base_url'] = base_url().'vendor/promotions/listing/';
            $config['total_rows'] = $this->promotionsmodel->getPromotionsCount($vendorid);
            $config['per_page'] = PERPAGE;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['prev_link'] = 'Previous';
            $config['next_link'] = 'Next';
            $this->pagination->initialize($config);
            // pagination section ends here

            $page = $this->uri->segment(4);
            if($page < 1)
                $page = 0;*/


        $data = array();
        $data['promotions'] = $this->promotionsmodel->getAllPromotions($vendorid);
        //$str_links = $this->pagination->create_links();
        //$data['links'] = explode('&nbsp;',$str_links);


        $appusers = array();
        $babelbarkusers=$this->appuservendorsmodel->getUsersForVendor($vendorid);
        for($i=0;$i<count($babelbarkusers);$i++)
        {
            $appuserid=$babelbarkusers[$i]['appuserid'];
            $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
            $appusers[$i] = $appuser;
        }
        $data['appusers'] =$appusers;
        $data['promocodes'] =$this->promotionsmodel->getAllPromoCodes($vendorid);

        $this->data['maincontent']=$this->load->view('vendor/maincontents/promotions-listing',$data,true);
        $this->load->view('vendor/layout',$this->data);
    }

    function storeImageUrl()
    {
        if (file_exists($_FILES['bannerimage']['tmp_name']) || is_uploaded_file($_FILES['bannerimage']['tmp_name'])) {

            $imgurl = '';
            $allowed =  array('gif','png' ,'jpg' ,'jpeg','JPG','PNG');
            $filename = $_FILES['bannerimage']['name'];
            $filetmpname = $_FILES['bannerimage']['tmp_name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                //return false;
                $uploadmsg=$uploadmsg."Banner Image Upload failed";
            }
            else {
                /************************************* local server upload  *****************************/
//                if (!file_exists('resources/uploads/temp/')) {
//                    mkdir('resources/uploads/temp/');
//                }
//                $uploaddir = realpath('./resources/uploads/temp/');
//
//
//                $fname="temp_banner".".".$ext;
//                $uploadfile = $uploaddir ."/". $fname;
//                if (move_uploaded_file($_FILES['bannerimage']['tmp_name'], $uploadfile)) {
//                    $imgurl='resources/uploads/temp/'.$fname;
//                    echo $imgurl;
//                }
//                else {
//                    $uploadmsg=$uploadmsg."Banner Image Upload failed!";
//                    echo $uploadmsg;
//                }
                /************************************* S3 Integration  *****************************/

                $uploaddir = 'resources/uploads/temp/';
                $fname="temp_banner".".".$ext;
                $s3msg = aws_upload($uploaddir,$fname,$filetmpname);

                if($s3msg=='S3 upload successful'){
                    $imgurl='resources/uploads/temp/'.$fname;
                    echo $imgurl;
                }else {
                    $uploadmsg=$uploadmsg."Banner Image Upload failed!";
                    echo $uploadmsg;
                }

                /************************************* End S3 Integration  *****************************/

            }

        }
    }


    function addPromotions()
    {
        $data=array();

        $data['breeds']=$this->petbreedmodel->getAllBreeds();
        $data['locations']=$this->locations;
        $data['unittype']=$this->unittype;

        $data['servicetypes']=$this->vendorscateg;

        $data['detail'] =array(
            'promotionid' =>0,
            'name' =>"",
            'description' => "",
            'beginingdate' => "",
            'expirationdate' => "",
            'agerangefrom' =>"",
            'agerangeto' =>"",
            'targetbreeds' => "",
            'targetweightsfrom' => "",
            'targetweightsto' => "",
            'isadopted' => 0,
            'targetactivityscoresmin' => "",
            'targetactivityscoresmax' => "",
            'advertlocations' =>"",
            'capacity' =>"",
            'redeemedcount' => 0,
            'promotionalurl' => "",
            'bannerimage' => "",
            'promotiongraphic' => "",
            'promocode' => "",
            'targetsubscribedcustomers' => "",
            'category' => "",
            'prmourltype' => "",
            'webpageheader' => "",
            'webpageimage' => "",
            'webpagedesc' => ""

        );
        $this->data['maincontent']=$this->load->view('vendor/maincontents/promotions-form',$data,true);

        $this->load->view('vendor/layout',$this->data);
    }

    function viewDetails($promotionid)
    {
        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $promotionstatus = $this->promotionsmodel->promotionStatus($vendorid,$promotionid);
        if($promotionstatus == TRUE)
        {
            $data=array();
            $data['detail'] = $this->promotionsmodel->getPromotionDetails($promotionid);
            $data['breeds']=$this->petbreedmodel->getAllBreeds();
            $data['locations']=$this->locations;
            $data['servicetypes']=$this->vendorscateg;
            $data['unittype']=$this->unittype;

            $this->data['maincontent']=$this->load->view('vendor/maincontents/promotions-form',$data,true);
            $this->load->view('vendor/layout',$this->data);
        }
        else
        {
            $data=array();
            $this->load->view('vendor/error-403',$this->data);
        }

    }

    function upload_thumbnail()
    {
        $this->load->helper('string');
        $w=$this->input->post('thumb_width_');
        $h=$this->input->post('thumb_height_');
        $x1=$this->input->post('x_axis');
        $y1=$this->input->post('y_axis');
        $x2 = $this->input->post('x2_axis');
        $y2 = $this->input->post('y2_axis');
        $img =	$this->input->post('img_');

        $img = awsBucketPath.$img;
        $fname = "temp_banner_" . time() . ".jpg";
        $uploaddir = 'resources/uploads/temp/';
        $pathTofile = "resources/uploads/temp/$fname";

        list($width, $height, $type, $attr)=getimagesize($img);

        if($type==1)
        {
            $nw = $w;// Maximum thumbnail width
            $nh = $w/6;//Maximum thumbnail height

            $img1=$img;
            $im = imagecreatefromgif($img1);
            $size = min(imagesx($im), imagesy($im));
            $im2 = imagecrop( $im, ['x' => $x1, 'y' => $y1, 'width' => $nw, 'height' => $nh]);
            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = 	imagegif($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;

        }
        else if($type==2)
        {
            error_reporting(E_ALL);

            $nw = $w;// Maximum thumbnail width
            $nh = $w/6;//Maximum thumbnail height
            $im = imagecreatefromjpeg($img);
            $size = min(imagesx($im), imagesy($im));

            $im2 = imagecrop( $im, ['x' => $x1, 'y' => $y1, 'width' => $nw, 'height' => $nh]);

            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = 	imagejpeg($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;
        }
        else if($type == 3)
        {
            $nw = $w;// Maximum thumbnail width
            $nh = $w/6;//Maximum thumbnail height
            $im = imagecreatefromPNG($img);
            $size = min(imagesx($im), imagesy($im));
            $im2 = imagecrop( $im, ['x' => $x1, 'y' => $y1, 'width' => $nw, 'height' => $nh]);
            if ($im2 !== FALSE) {
                $imgcropped = "resources/uploads/temp/$fname";
                $newcroppedimage = 	imagepng($im2, $imgcropped);
            }
            $s3msg = aws_upload($uploaddir, $fname, $pathTofile);
            echo $imgcropped;
        }



    }

    function updatePromotions()
    {
        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $promotionid=$this->input->post('promotionid');

        $promotionname= (trim($this->input->post('promotionname')));
        $promotiondesc= (trim($this->input->post('promotiondesc')));

        $categories= $this->input->post('selecttype');
        if (is_array($this->input->post('selecttype'))) {
            $categorystr= implode (",", $this->input->post('selecttype'));
        }
        else
            $categorystr=$this->input->post('selecttype');

        $beginingdate = date('Y-m-d', strtotime(trim($this->input->post('startdate'))));
        $expirationdate = date('Y-m-d', strtotime(trim($this->input->post('expirydate'))));

        $agerangefrom= (trim($this->input->post('agerangefrom')));
        $agerangeto= (trim($this->input->post('agerangeto')));

        $weightfrom= (trim($this->input->post('weightfrom')));
        $weightto= (trim($this->input->post('weightto')));
        if($this->unittype == 'British'){
            $weightfrom = $weightfrom/2.20462;
            $weightto = $weightto/2.20462;
        } 
        $isadopted= (trim($this->input->post('isadopted')));

        $breeds= $this->input->post('breeds');
        if (is_array($this->input->post('breeds'))) {
            $breedstr= implode (",", $this->input->post('breeds'));
        }
        else
            $breedstr=	$this->input->post('breeds');



        $advertloc= (trim($this->input->post('advertloc')));
        $actscoresmin= (trim($this->input->post('actscoresmin')));
        $actscoresmax= (trim($this->input->post('actscoresmax')));
        $capacity= (trim($this->input->post('capacity')));
        //$redeemedcnt= (trim($this->input->post('redeemedcnt')));
        $promotionalurl= (trim($this->input->post('promotionalurl')));
        $targetsubscribedcustomers= $this->input->post('targetsubscribedcustomers');

        $urltype= ($this->input->post('urlchk'));

        $webpageheader= $this->input->post('webpageheader');
        $webpagedesc= $this->input->post('webpagedesc');

        $uploadmsg="Promotion updated.";

        $datacc =array(
            'vendorid'=>$vendorid,
            'name' => $promotionname,
            'description' => $promotiondesc,
            'beginingdate' => $beginingdate,
            'expirationdate' => $expirationdate,
            'agerangefrom' =>$agerangefrom,
            'agerangeto' =>$agerangeto,
            'targetbreeds' => $breedstr,
            'targetweightsfrom' =>$weightfrom,
            'targetweightsto' => $weightto,
            'isadopted' => $isadopted ,
            'targetactivityscoresmin' => $actscoresmin,
            'targetactivityscoresmax' =>$actscoresmax,
            'advertlocations' => $advertloc,
            'capacity' => $capacity,
            'promotionalurl' => $promotionalurl,
            'targetsubscribedcustomers' => $targetsubscribedcustomers,
            'category' => $categorystr,
            'prmourltype' => $urltype,
            'webpageheader' => $webpageheader,
            'webpagedesc' => $webpagedesc
        );

        if($promotionid==0) //for new addition
        {

            $promocode = $this->genratePrimaryKey('30');
            $datacc['promocode']=$promocode;
        }

        $date_now = date("Y-m-d"); // this format is string comparable

        if ($expirationdate >= $date_now) {
            $datacc['isactive']=1;
        }else{
            $datacc['isactive']=0;
        }

        $promotionid=$this->promotionsmodel->updatePromotions($datacc,$promotionid);

        if($urltype=="internal")
        {
            $promotionalurl=base_url()."promotiondetail?promotionid=".$promotionid;
            $datacc=array();
            $datacc['promotionalurl']=$promotionalurl;
            $this->promotionsmodel->updatePromotions($datacc,$promotionid);
        }

        //upload promotion webpage image

        if (file_exists($_FILES['webpageimage']['tmp_name']) || is_uploaded_file($_FILES['webpageimage']['tmp_name'])) {

            $imgurl = '';
            $allowed =  array('gif','png' ,'jpg' ,'jpeg');
            $filetmpname = $_FILES['webpageimage']['tmp_name'];
            $filename = $_FILES['webpageimage']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if(!in_array($ext,$allowed) ) {
                //return false;
                $uploadmsg=$uploadmsg."Webpage Image Upload failed";
            }
            else {
                /************************************* Local server upload  *****************************/
//                if (!file_exists('resources/uploads/promotions/')) {
//                    mkdir('resources/uploads/promotions/');
//                }
//                $uploaddir = realpath('./resources/uploads/promotions/');
//
//
//                $fname=$promotionid."_webpage".".".$ext;
//                $uploadfile = $uploaddir ."/". $fname;
//                if (move_uploaded_file($_FILES['webpageimage']['tmp_name'], $uploadfile)) {
//                    $imgurl='resources/uploads/promotions/'.$fname;
//                    $datacc=array();
//                    $datacc['webpageimage']=$imgurl;
//                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
//                }
//                else {
//                    $uploadmsg=$uploadmsg."Webpage Image Upload failed!";
//                }
                /************************************* S3 Integration  *****************************/

                $uploaddir = 'resources/uploads/promotions/';
                $fname=$promotionid."_webpage".".".$ext;
                $s3msg = aws_upload($uploaddir,$fname,$filetmpname);

                if($s3msg=='S3 upload successful'){
                    $imgurl='resources/uploads/promotions/'.$fname;
                    $datacc=array();
                    $datacc['webpageimage']=$imgurl;
                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
                }else {
                    $uploadmsg=$uploadmsg."Webpage Image Upload failed!";
                }

                /************************************* End S3 Integration  *****************************/

            }

        }
        //upload banner image
        $bannerimage=$this->input->post('bannerimage1');
        if($bannerimage != ''){
            $bannerimage1=explode('/temp/', $bannerimage);
            $filename=$bannerimage1[1];
            $imgurl = '';
            $allowed =  array('gif','png' ,'jpg' ,'jpeg');
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                //return false;
                $uploadmsg=$uploadmsg."App Banner Image Upload failed!";
            }
            else
            {
                /************************************* local server upload *****************************/
//                $fname=$promotionid."_banner".".".$ext;
//
//                $location='resources/uploads/promotions/'.$fname;
//                if(rename($bannerimage,$location))
//                {
//                    $imgurl='resources/uploads/promotions/'.$fname;
//                    $datacc=array();
//                    $datacc['bannerimage']=$imgurl;
//                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
//                }
                /************************************* S3 Integration  *****************************/

                $sourceFolder = 'resources/uploads/temp/';
                $sourceFileName = $filename;
                $targetFolder = 'resources/uploads/promotions/';
                $targetFileName = $promotionid."_banner".".".$ext;
                $fname =$promotionid."_banner".".".$ext;

                $s3msg = aws_copy($sourceFolder,$sourceFileName,$targetFolder,$targetFileName);
                if($s3msg=='S3 file copy successful'){
                    $imgurl='resources/uploads/promotions/'.$fname;
                    $datacc=array();
                    $datacc['bannerimage']=$imgurl;
                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
                    aws_delete($sourceFolder,$sourceFileName);
                }
                /************************************* End S3 Integration  *****************************/
            }
        }

        //upload graphic image

        if (file_exists($_FILES['graphicimage']['tmp_name']) || is_uploaded_file($_FILES['graphicimage']['tmp_name'])) {

            $imgurl = '';
            $allowed =  array('gif','png' ,'jpg' ,'jpeg');
            $filename = $_FILES['graphicimage']['name'];
            $filetmpname = $_FILES['graphicimage']['tmp_name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if(!in_array($ext,$allowed) ) {
                //return false;
                $uploadmsg=$uploadmsg."App Full Graphice Upload failed!";
            }
            else {
                /************************************* local server upload  *****************************/
//                if (!file_exists('resources/uploads/promotions/')) {
//                    mkdir('resources/uploads/promotions/');
//                }
//                $uploaddir = realpath('./resources/uploads/promotions/');
//
//
//                $fname=$promotionid."_graphic".".".$ext;
//                $uploadfile = $uploaddir ."/". $fname;
//                if (move_uploaded_file($_FILES['graphicimage']['tmp_name'], $uploadfile)) {
//                    $imgurl='resources/uploads/promotions/'.$fname;
//                    $datacc=array();
//                    $datacc['promotiongraphic']=$imgurl;
//                    $datacc['promotionalurl']=base_url().$imgurl;
//                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
//                }
//                else {
//                    $uploadmsg=$uploadmsg."App Full Graphice Upload failed!";
//                }

                /************************************* S3 Integration  *****************************/

                $uploaddir = 'resources/uploads/promotions/';
                $fname=$promotionid."_graphic".".".$ext;
                $s3msg = aws_upload($uploaddir,$fname,$filetmpname);

                if($s3msg=='S3 upload successful'){
                    $imgurl='resources/uploads/promotions/'.$fname;
                    $datacc=array();
                    $datacc['promotiongraphic']=$imgurl;
                    $datacc['promotionalurl']=base_url().$imgurl;
                    $this->promotionsmodel->updatePromotions($datacc,$promotionid);
                }else {
                    $uploadmsg=$uploadmsg."App Full Graphice Upload failed!";
                }
                /************************************* End S3 Integration  *****************************/

            }

        }

        $this->session->set_flashdata('success_message', $uploadmsg);

        redirect('vendor/promotions/listing');

    }


    function changeStatus($promotionid,$isactive=0)
    {
        $status='';
        $datacc=array(
            'isactive'=>$isactive
        );
        $checkExpireDate = $this->promotionsmodel->checkExpirationDate($promotionid);
        if($checkExpireDate>0){
            $update= $this->promotionsmodel->updatePromotions($datacc,$promotionid);
        }


        if($update>0)
        {
            if($isactive==0){$status='Inactivated';}else{$status='Activated';}
            $msg="Prmotion is ".$status.' successfully';
            $this->session->set_flashdata('success_message',$msg);
            redirect('vendor/promotions/listing');

        }else
        {
            if($isactive==0){$status='Inactivating';}else{$status='Activating';}
            $msg="Some error occurred while ".$status.' the promotion.Please try again.';
            if($checkExpireDate<=0){
                $msg="Promotion expiration date is not valid !";
            }
            $this->session->set_flashdata('error_message',$msg);
            redirect('vendor/promotions/listing/');
        }
        exit();

    }

    function deletePromotion($promotionid)
    {
        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];
        $promotionstatus = $this->promotionsmodel->promotionStatus($vendorid,$promotionid);
        if($promotionstatus == TRUE)
        {
            $this->promotionsmodel->deletePromotionRedeemed($promotionid);
            $this->promotionsmodel->deletePromotion($promotionid);
            $msg="Promotion deleted successfully";
            $this->session->set_flashdata('success_message',$msg);
            redirect('vendor/promotions/listing');
        }
        else
        {
            $data=array();
            $this->load->view('vendor/error-403',$this->data);
        }
    }


    function fetchPromotionInfo()
    {
        $promotionid= $this->input->post('promotionid');
        $appuserid= $this->input->post('appuserid');
        $promocapacity=$this->promotionsmodel->getPromoCapacity($promotionid);
        if( $this->promotionsmodel->isPromoRedeemed($appuserid,$promotionid))
        {
            echo "promotionRedeemedAlready";
        }
        else if($promocapacity!=0) //0== infinte redemtions can be done
        {
            if(($promocapacity-$this->promotionsmodel->getRedeemedCount($promotionid))==0){
                echo "promotionCapacityExceeded";
            }
        }
        else {
            $data['userdetail'] = $this->appusersmodel->fetchUserDetails1($appuserid);
            $data['promotiondetail'] = $this->promotionsmodel->getPromotionDetails($promotionid);
            $this->load->view('vendor/maincontents/promotioninfo',$data);
        }
    }

    function confirmPromocode()
    {
        $promotionid= $this->input->post('promotionid');
        $appuserid= $this->input->post('appuserid');
        $promocode= $this->input->post('promocode');
        $datacc=array();
        $datacc['promotionid']=$promotionid;
        $datacc['appuserid']=$appuserid;
        $datacc['promocode']=$promocode;
        $this->promotionsmodel->updatePromotionRedeemed($datacc);

        $this->promotionsmodel->incrmentRedeemcnt($promotionid);
        echo '1';
    }
    function promotionstatistics($promotionid)
    {
        $data=array();
        $detail = $this->promotionsmodel->getPromotionDetails($promotionid);
        $beginingdate= new DateTime($detail['beginingdate']);
        $expirationdate= new DateTime($detail['expirationdate']);

        $daterange = new DatePeriod($beginingdate, new DateInterval('P1D'), $expirationdate);

        $capacity=$detail['capacity'];
        $numberarr=range(0, $capacity);
        //print_r($daterange);
        $daysarray=array();
        $resultarry=array();
        $redeemcntdata=array();

        foreach($daterange as $date){
            $daysarray[]=$date->format("Y-m-d");
        }
        $daysarray[]=$detail['expirationdate'];


        $redeemdetail = $this->promotionsmodel->getRedeemDetails($promotionid);
        for($i=0;$i<count($daysarray);$i++)
        {
            $day=$daysarray[$i];
            $redeemcnt=0;
            for($j=0;$j<count($redeemdetail);$j++)
            {
                $updatedon=$redeemdetail[$j]['updatedon'];
                $updateddate = date('Y-m-d', strtotime($updatedon));

                if(strtotime($updateddate)==strtotime($day))
                {
                    $redeemcnt=$redeemcnt+1;
                }
            }
            $resultarry[$day]=$redeemcnt;
            $redeemcntdata[]=$redeemcnt;
        }


        $data['capacity'] =$capacity;
        $data['promocode'] =$detail['promocode'];
        $data['daysarray'] = json_encode($daysarray);
        $data['resultarry'] = $resultarry;
        $data['redeemcntdata'] = json_encode($redeemcntdata);
        $this->data['maincontent']=$this->load->view('vendor/maincontents/promotionstatistics',$data,true);

        $this->load->view('vendor/layout',$this->data);
    }

    function redeemrequests()
    {

        $admin=$this->session->userdata('admin');
        $vendorid=$admin['adminDetails']['vendorid'];

        $vendorpromotions=$this->promotionsmodel->getAllPromotions1($vendorid);
        $waitingrquests=$this->appuserpromotionmodel->getWaitingRequests();

        $vendorpromoarr= array();
        for($i=0;$i<count($vendorpromotions);$i++)
        {
            $vendorpromoarr[]=$vendorpromotions[$i]['promotionid'];
        }
        $validwaitingreq=array();
        for($i=0;$i<count($waitingrquests);$i++)
        {
            $waitingpromoid=$waitingrquests[$i]['promotionid'];
            if(in_array($waitingpromoid, $vendorpromoarr))
            {

                $detail=$this->promotionsmodel->getPromotionDetails($waitingpromoid);
                $appuserid=$waitingrquests[$i]['appuserid'];
                $userdetails=$this->appusersmodel->fetchUserDetails1($appuserid);

                $waitingrquests[$i]['username']=$userdetails['firstname']." ".$userdetails['lastname'];
                $waitingrquests[$i]['promoname']=$detail['name'];
                $validwaitingreq[]=$waitingrquests[$i];
            }

        }

        $data['redeemrequests'] =$validwaitingreq;
        $this->data['maincontent']=$this->load->view('vendor/maincontents/promotionsredeem_requests',$data,true);

        $this->load->view('vendor/layout',$this->data);
    }

    function redeemPromotion($requestid,$promotionid)
    {
        $promocapacity=$this->promotionsmodel->getPromoCapacity($promotionid);

        $validcapacity=false;
        if($promocapacity!=0) //0== infinte redemtions can be done
        {
            if(($promocapacity-$this->promotionsmodel->getRedeemedCount($promotionid))==0){
                $validcapacity=false;

            }
            else {
                $validcapacity=true;
            }
        }
        else {
            $validcapacity=true;
        }
        if($validcapacity)
        {
            $this->appuserpromotionmodel->redeemPromotion($requestid);
            $this->promotionsmodel->incrmentRedeemcnt($promotionid);
            $this->session->set_flashdata('success_message', "Request redeemed successfully!");
            redirect('vendor/promotions/redeemrequests');
        }
        else
        {
            $this->session->set_flashdata('error_message', "Promocode has exceeded its capcity!");
            redirect('vendor/promotions/redeemrequests');
        }

    }

}