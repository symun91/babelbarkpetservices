<?php

class Settings extends CI_Controller
{
	var $data=array();
	function __construct()
	{
		parent::__construct();
	
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		
		$this->load->helper(array('common_helper','url'));
		$this->load->model('settingsmodel');
	}
	
	function index()
	{
		$admin=$this->session->userdata('admin');
	    $vendorid=$admin['adminDetails']['vendorid'];
	    
	   
	     $data['metricunits']= json_decode (METRICUNITS);
	     $data['britishunits']= json_decode (BRITISHUNITS);
	   	 $data['feedunits']= json_decode (FEEDUNITS);
	   	 
	   	 $settingdetails=$this->settingsmodel->getDetails1($vendorid);
	   	 if($settingdetails=="")
	   	 {
	   	 	$settingdetails=array();
	   	 	$settingdetails['unittype']='Metric';
	   	 	$settingdetails['feedingunit']='kilos';
	   	 }
	   	 $data['settingdetails']=$settingdetails;
	   	
	    if($this->input->post('formsubmit')=='yes')
		{
			$datacc=array(
					 	'unittype' => $this->input->post('unitchk'),
		               'feedingunit' => $this->input->post('selectfeedunit'),
						'vendorid' => $vendorid
			);
			$this->settingsmodel->updateSettings($datacc,$vendorid);
			
			$this->session->set_flashdata('success_message',"Settings updated successfully!");
			redirect('vendor/settings');
		}
		$this->data['maincontent']=$this->load->view('vendor/maincontents/settings-form',$data,true);
		$this->load->view('vendor/layout',$this->data);
	}
}
?>