<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @property admin_model $admin_model
 * @property employeemodel $employeemodel
 * @property servicemodel $servicemodel
 * @property vendorsmodel $vendorsmodel
 * @property vendorcategoriesmodel $vendorcategoriesmodel
 * @property settingsmodel $settingsmodel
 */

class Administrator extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('employeemodel');
        $this->load->model('servicemodel');
        $this->load->model('vendorsmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('settingsmodel');
        $this->load->helper(array('common_helper'));
        $this->load->library('session');

        $this->weekdays = array('M', 'T', 'W', 'Th', 'F', 'Sa', 'Su');
        $this->stateUSArray = json_decode(USSTATES, true);
        $this->stateCanadaArray = json_decode(CANADASTATES, true);
    }

    private static function genratePrimaryKey($serverid) {
        $primarykey = $serverid . time() . mt_rand(1000, 9999);
        if ($primarykey > 9223372036854775807) { //max of 64 bit int
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }

    private static function generateLoginToken(){
        $loginToken = time() . mt_rand(1000, 9999);
        return $loginToken;
    }
    function login() {
        // This is for babelvet login //
        if($_GET){
            if(isset($_GET['login_token'])) {
                $login_token = $_GET['login_token'];
            }
            if(isset($_GET['id'])) {
                $vendorId = $_GET['id'];
            }
            if(isset($_GET['appointment_no'])) {
                $appointment_no = $_GET['appointment_no'];
            }
        }

        if(!empty($login_token)){
            $checkLoginToken = $this->vendorsmodel->checkLoginToken($login_token);
            if($checkLoginToken){
                $adminData = $this->admin_model->find_data('row', array('email' => $checkLoginToken['email'], 'password like binary' => $checkLoginToken['password']), VENDOR, 0, 0, 'vendorid');
                $this->session->set_flashdata('success_msg', 'Login Successful.');
                $admin = array('isloggedin' => 1, 'adminDetails' => $adminData, 'role' => 'admin');
                $this->session->set_userdata('admin', $admin);
                $this->session->set_userdata('isadmin', FALSE);
                redirect('vendor/home/index');
            }
        }

        if(!empty($vendorId)&&!empty($appointment_no)){
            $checkVendorExist = $this->vendorsmodel->getVendorDetails($vendorId);
            if($checkVendorExist){
                $adminData = $this->admin_model->find_data('row', array('email' => $checkVendorExist['email'], 'password like binary' => $checkVendorExist['password']), VENDOR, 0, 0, 'vendorid');
                $admin = array('isloggedin' => 1, 'adminDetails' => $adminData, 'role' => 'admin');
                $this->session->set_userdata('admin', $admin);
                $this->session->set_userdata('isadmin', FALSE);
                redirect("vendor/appointments/viewDetails/$appointment_no");
            }
        }
        // End of babelvet login //
        $data = array();
        $this->load->library('admin_init_elements');
        $data['head'] = $this->admin_init_elements->init_loginSignup_head();

        if ($this->input->post('submit')) {
            if ($this->validate_form_data() == true) {
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $adminData = $this->admin_model->find_data('row', array('email' => $email, 'password like binary' => $password), VENDOR, 0, 0, 'vendorid');

                $loginToken = $this->generateLoginToken();
                $vendorId   =$adminData['vendorid'];
                $tokenData = array(
                    'login_token' => $loginToken
                );
                $isAdded = $this->vendorsmodel->saveLoginToken($tokenData,$vendorId);
                if (!empty($adminData)) {
                    if ($adminData['category'] == 1) {
                        if($isAdded){
                            redirect(BABELVET_ADD.'/vendor/login?login_token='.$loginToken);
                        }else{
                            redirect(BABELVET_ADD.'/vendor/login');
                        }
                    } else {
                        $this->session->set_flashdata('success_msg', 'Login Successful.');
                        $admin = array('isloggedin' => 1, 'adminDetails' => $adminData, 'role' => 'admin');
                        $this->session->set_userdata('admin', $admin);
                        $this->session->set_userdata('isadmin', FALSE);
                        redirect(BIZBARK_ADD.'/vendor/login?login_token='.$loginToken);
                    }

                } else {
                    //check whether  employee
                    $empData = $this->employeemodel->employeeLogin($email, $password);
                    if (!empty($empData)) {
                        $this->session->set_flashdata('success_msg', 'Login Successful.');
                        $admin = array('isloggedin' => 1, 'adminDetails' => $empData, 'role' => 'employee');
                        $this->session->set_userdata('admin', $admin);
                        redirect('vendor/home/index');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Invalid login. Please try again.');
                        redirect('vendor/login');
                    }
                }
            }
        }
        $this->load->view('vendor/maincontents/login', $this->data);
    }

    function register() {
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_loginSignup_head();
        $data = array();
        $this->data['weekdays'] = $this->weekdays;
        $this->data['USstates'] = $this->stateUSArray;
        $this->data['Canadastates'] = $this->stateCanadaArray;
        $this->data['servicetypes'] = $this->vendorcategoriesmodel->getSelectedVendorCategories();
        $this->data['formvalidationerror'] = "no";
        $this->data['country'] = "";
        $this->data['state'] = "";

        if ($this->input->post('submit')) {
            if ($this->validate_register_data() == true) {
                $categories = $this->input->post('selecttype');
                if (is_array($this->input->post('selecttype'))) {
                    $categorystr = implode(",", $this->input->post('selecttype'));
                } else
                    $categorystr = $this->input->post('selecttype');

                $latlng = $this->input->post('lat') . "," . $this->input->post('lng');

                $vendorid = $this->genratePrimaryKey('40');
                $datacc = array(
                    'vendorid' => $vendorid,
                    'email' => $this->input->post('email'),
                    //'password' => md5($this->input->post('password')),
                    'password' => $this->input->post('password'),
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'comapnyname' => $this->input->post('comapnyname'),
                    'category' => $categorystr,
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'country' => $this->input->post('country'),
                    'zipcode' => $this->input->post('zipcode'),
                    'agreedterms' => $this->input->post('agreedterms'),
                    'createdon' => date('Y-m-d H:i'),
                    'addresslatlng' => $latlng,
                    'operatingdays' => "M,T,W,Th,F,Sa,Su",
                    'availabilityfromhour' => "9:00,9:00,9:00,9:00,9:00,9:00,9:00",
                    'availabilitytohour' => "5:00,5:00,5:00,5:00,5:00,5:00,5:00",
                    'Hearaboutus' => implode(' ', $this->input->post('parentcheckbox'))
                );
                $this->vendorsmodel->registerVendor($datacc);

                //set default units entry
                $datacc = array(
                    'vendorid' => $vendorid,
                    'unittype' => 'Metric',
                    'feedingunit' => 'kilos',
                );
                $this->settingsmodel->inserDefaultEntry($datacc);
                //send mail

                $from = SITE_MAIL;
                $toemail = $this->input->post('email');
                $subject = 'Welcome to Bizbark!';


                $requestHost = $_SERVER['HTTP_HOST'];
                $host = 'Bizbark';

                if (strpos($requestHost, 'babelvet') !== false) {
                    $host = 'Babelvet';
                }

                //send reminder mail to vendor
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->to($toemail);
                $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->cc(SITE_MAIL, 'BizBark');
                $this->email->subject($subject);

                $sendmessage = file_get_contents('resources/registration_email_template.html');
                $sendmessage = str_replace('%host%', $host, $sendmessage);

                $this->email->message($sendmessage);
               

                $result = $this->email->send();

                if (!$result) {

                } else {
                    $this->session->set_flashdata('success_message', 'You have registered successfully!');
                }
                redirect('vendor/administrator/login');
            } else {
                $this->data['formvalidationerror'] = "yes";
                $this->data['country'] = $this->input->post('country');
                $this->data['state'] = $this->input->post('state');
                $this->load->view('vendor/maincontents/register', $this->data);
            }
        } else {
            $this->load->view('vendor/maincontents/register', $this->data);
        }
    }

    function forgetPassword() {
        $data = array();
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_loginSignup_head();
        //$this->admin_init_elements->init_header();
        //$this->admin_init_elements->init_footer();
        
        if ($this->input->post('email')) {
            if ($this->validate_forgetPassword_data() == true) {
                $random_password = rand_string(6);
                $admin = $this->admin_model->find_data('row', array('email' => $this->input->post('email')), VENDOR, 0, 0, 'vendorid');
                if (!empty($admin)) {
                    $updateData = array(
                        //'password'=>md5($random_password),
                        'password' => $random_password,
                        'updatedon' => date('Y-m-d H:i:s')
                    );
                    $update = $this->admin_model->save_data($updateData, $admin['vendorid'], 'vendorid', VENDOR);
                } else {
                    $admin = $this->admin_model->find_data('row', array('email' => $this->input->post('email')), VENDORUSER, 0, 0, 'vendorid');
                    $updateData = array(
                        //'password'=>md5($random_password),
                        'password' => $random_password,
                        'updatedon' => date('Y-m-d H:i:s')
                    );
                    $update = $this->admin_model->save_data($updateData, $admin['userid'], 'userid', VENDORUSER);
                }
                if ($update > 0) {

                    $to = $admin['email'];
                    $from = SITE_MAIL;
                    $subject = 'New password for Bizbark vendor login';

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From:' . $from . "\r\n"; // Sender's Email
                    $sendmessage = file_get_contents('resources/forgetpassword_email_template.html');
                    $sendmessage = str_replace('%newpassword%', $random_password, $sendmessage);

                    $this->load->library('email');
                    $config['protocol'] = "smtp";
                    $config['smtp_host'] = "ssl://smtp.gmail.com";
                    $config['smtp_port'] = "465";
                    $config['smtp_user'] = SITE_MAIL;
                    $config['smtp_pass'] = "doogyWOOGY!";
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "html";
                    $config['newline'] = "\r\n";

                    $this->email->initialize($config);
                    $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
                    $this->email->to($to);
                    $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
                    $this->email->cc(SITE_MAIL, 'BizBark');
                    $this->email->subject($subject);

                    $this->email->message($sendmessage);

                    $result = $this->email->send();
                    if (!$result) {
                        $this->session->set_flashdata('error_msg', 'Your password was reset successfully. But some error occurred while sending your password in mail.');
                    } else {
                        $this->session->set_flashdata('success_msg', 'Your password was reset successfully and has been sent to your mail!');
                    }

                    redirect('vendor/forgetPassword');
                } else {
                    $this->session->set_flashdata('error_msg', 'Some error occurred while reseting your password.Please try again.');
                    redirect('vendor/forgetPassword');
                }
            }
        }

        $this->load->view('vendor/maincontents/forget-password', $this->data);
    }

    function lock($controller = 'home', $method = 'index') {
        $this->data['admin'] = $this->session->userdata('admin');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_head();
        $admin['lock'] = array(
            'islock' => 1,
            'controller' => $controller,
            'method' => $method
        );
        $this->session->set_userdata($admin);
        $this->load->view('vendor/maincontents/lock-screen', $this->data);
    }

    function unlock() {
        $admin = $this->session->userdata('admin');
        //print_r($admin);
        $lock = $this->session->userdata('lock');
        $password = md5($this->input->post('password'));
        if ($password != '') {
            if ($password == $admin['adminDetails']['password']) {
                $this->session->unset_userdata('lock');
                redirect('vendor/' . $lock['controller'] . '/' . $lock['method']);
            } else {
                $this->session->set_flashdata('error_message', 'Invalid Password.Please try again.');
                redirect('vendor/lock/' . $lock['controller'] . '/' . $lock['method']);
            }
        }
        redirect('vendor/lock/' . $lock['controller'] . '/' . $lock['method']);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('vendor/administrator/login');
    }

    function validate_unlock_data() {
        return true;
    }

    private function validate_register_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|strip_tags|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|strip_tags|required');
        $this->form_validation->set_rules('comapnyname', 'Company Name', 'trim|strip_tags|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|strip_tags|required|valid_email|callback_check_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('selecttype', 'Category', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        if ($this->input->post('country') == "United States")
            $this->form_validation->set_rules('zipcode', 'Zipcode', 'required|exact_length[5]|integer');
        else
            $this->form_validation->set_rules('zipcode', 'Zipcode', 'required|callback_check_zipcode');

        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');

        $this->form_validation->set_rules('agreedterms', 'Checkbox', 'callback_accept_terms');
        $this->form_validation->set_rules('agreement', 'Checkbox', 'callback_accept_terms1');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    function check_email($email) {
        if ($this->vendorsmodel->isEmailExists($email)) {
            $this->form_validation->set_message('check_email', 'This email is already registered with us, please try again.');
            return false;
        } else
            return true;
    }

    function check_zipcode($zipcode) {
        //if(preg_match("/^([a-ceghj-npr-tv-z]){1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}$/i",$zipcode))
        if (preg_match('/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/', $zipcode))
            return true;
        else {
            $this->form_validation->set_message('check_zipcode', 'Please enter valid Zipcode for Canada.');
            return false;
        }
    }

    function accept_terms($agreedterms) {
        //  if ($this->input->post('agreedterms')) return true;
        if ($agreedterms == '1')
            return true;
        $this->form_validation->set_message('accept_terms', 'Please read and accept our Terms and Conditions.');
        return false;
    }

    function accept_terms1($agreement) {
        //  if ($this->input->post('agreedterms')) return true;
        if ($agreement == '1')
            return true;
        $this->form_validation->set_message('accept_terms1', 'Please read and accept our Vendor Affiliate Agreement.');
        return false;
    }

    function validate_forgetPassword_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('email', 'Email Id', 'trim|required|is_valid_email|callback_is_email_active');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    function is_email_active() {
        $email = $this->admin_model->find_data('row', array('email' => $this->input->post('email')), VENDOR, 0, 0, 'vendorid');
        if(empty($email)){
          $email = $this->admin_model->find_data('row', array('email' => $this->input->post('email')), VENDORUSER, 0, 0, 'vendorid');  
        }
        if (!empty($email)) {
            return true;
        } else {
            $this->form_validation->set_message('is_email_active', 'This email ID is not registered with us, please try again or create an account!');
            return false;
        }
    }

    function validate_form_data() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
        $this->form_validation->set_rules('email', 'Email Id', 'trim|required|is_valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

}

?>