<?php
/**
 * @property medicalfilesmodel   $medicalfilesmodel
 */
class UploadMedicalFile extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model(array('appusersmodel','medicalfilesmodel','petmodel'));

    }

    function index()
    {
        $data['msg'] = '';
        if(!empty($_GET)){

            $token = $this->input->get('token');
            $recorddetails=$this->medicalfilesmodel->verifyToken($token);
            if($recorddetails!=="")
            {
                $data['recorddetails'] = $recorddetails;
                $data['petdetails']=$this->petmodel->getPetDetails($recorddetails['petid']);
                $data['appuserdetails']=$this->appusersmodel->fetchUserDetails1($recorddetails['appuserid']);
                $data['token']=$token;
            }
            else {
                $data['msg'] = 'Invalid Token';
            }

        }
        else
        {
            $data['msg'] = ' Token is missing';
        }
        $this->load->view('uploadMedicalFile.php',$data);
    }

    function uploadFile()
    {
        $token = $this->input->post('get_token');
        $recordid = $this->input->post('recordid');
        $count = count($_FILES['files']['name']);

        $allowed = array('docx','pdf','txt','png', 'jpg', 'jpeg');
        $msg[] =array();
        for($i=0; $i<$count;$i++){
            $filename    = $_FILES['files']['name'][$i];
            $filesize = $_FILES['files']['size'][$i];
            $maxsize = 12582912;
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if (!in_array($ext, $allowed)) {
                $msg[] = "The file type is not allowed. Please try another type.";
            }

            if(($filesize >= $maxsize) || ($filesize == 0)) {
                $msg[] = 'File too large. File must be less than 12 megabytes.';
            }
        }

        if(count($msg)>1){
            foreach($msg as $error) {
                $this->session->set_flashdata('error_message', $error);
            }

            redirect('uploadMedicalFile?token='.$token);
        }

        for($i=0; $i<$count;$i++) {
            if (file_exists($_FILES['files']['tmp_name'][$i]) || is_uploaded_file($_FILES['files']['tmp_name'][$i])) {
                $filename    = $_FILES['files']['name'][$i];
                $filetmpname = $_FILES['files']['tmp_name'][$i];

                $uploaddir = 'resources/uploads/vetmedicalfiles/';
                $s3msg = aws_upload($uploaddir,$filename,$filetmpname);

                if($s3msg=='S3 upload successful') {
                    //update record
                    $datacc = array();
                    $datacc['filename'] = $filename;
                    $fileurl = 'resources/uploads/vetmedicalfiles/' . $filename;
                    $datacc['fileurl'] = $fileurl;
                    $datacc['token'] = "";
                    $datacc['updatedon'] = date('Y-m-d H:i:s');
                    if($i==0){
                        $this->medicalfilesmodel->updateMedicalFiles($recordid, $datacc);
                    }else{
                        $recordInfo = $this->medicalfilesmodel->getReportInfo($recordid);
                        $datacc['petid'] = $recordInfo['petid'];
                        $datacc['appuserid'] = $recordInfo['appuserid'];
                        $datacc['vetemail'] = $recordInfo['vetemail'];
                        $this->medicalfilesmodel->addMultipleMedicalFiles($datacc);
                    }
                }
                /************************************* End S3 Integration  *****************************/

            }
        }
        if($s3msg=='S3 upload successful'){
            $data['msg'] = 'File uploaded successfully!';
            $this->load->view('uploadMedicalFile.php',$data);
        }
        else {
            $this->session->set_flashdata('error_message', 'Please upload  file.');
            redirect('uploadMedicalFile?token='.$token);

        }

    }

}
?>