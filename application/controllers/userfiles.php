<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property medicalfilesmodel            $medicalfilesmodel
 * @property reportmodel                  $reportmodel
 * @property appusersmodel                $appusersmodel
 * @property vendorsmodel                 $vendorsmodel
 */
class Userfiles extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/medicalfilesmodel');
        $this->load->model('appusersmodel');
        $this->load->model('reportmodel');
        $this->load->model('vendorsmodel');
    }

    public function view_image($url){
        $actual_url = str_replace('-','/',$url);
        $this->_displayImage($actual_url);
    }

    public function view_medical_file($medicalfileid,$userType,$token=null){
        ob_clean();
        if($userType=='BabelBark'){
            $fileType = 2; // 2 = Medical files
            $fileInfo = $this->medicalfilesmodel->getMedicalInfo($medicalfileid);
            $filename = $fileInfo[0]['filename'];
            $fileurl  = $fileInfo[0]['fileurl'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $modifiedPath = urlencode($fileurl);
            $data['msg'] = "";
            $this->_displayFile($modifiedPath,$filename,$ext);
        }elseif($userType=='Sikka'){
            $fileInfo = $this->reportmodel->getSikkaReport($medicalfileid);
            $filename = $fileInfo[0]['filename'];
            $fileurl  = $fileInfo[0]['fileurl'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $modifiedPath = urlencode($fileurl);
            $data['msg'] = "";
            $this->_displayFile($modifiedPath,$filename,$ext);
        }else{
            $data['msg'] = 'Invalid Request';
            $this->load->view('vendor/filenotfound',$data);
        }
    }

    public function _displayImage($url) {
        $path = awsBucketPath.$url;
        $data = aws_image($path);
        if($data==null){
            return null;
        }else{
            header("Content-Type: image/jpeg");
            echo $data;
        }

    }


    public function _displayFile($url, $filename,$ext){
        if($ext=='jpg'||$ext=='jpeg'||$ext=='png'){
            $contentType = "image/$ext";
        }elseif($ext=='pdf'){
            $contentType = "application/$ext";
        }elseif($ext=='docx'||$ext=='doc'){
            $contentType = "application/$ext";
        }

        $path = awsBucketPath.$url;
        $data = aws_image($path);
        if($data==FALSE){
            $data['msg'] = 'File not found!';
            $this->load->view('vendor/filenotfound',$data);
        }else{
            header('Cache-Control: public');
            header('Content-Description: File Transfer');
            header("Content-Type: $contentType");
            header("Content-Disposition: inline; filename=" . $filename);
            echo $data;
        }
    }

    public function _downLoadFile($url, $filename,$ext){
        if($ext=='jpg'||$ext=='jpeg'||$ext=='png'){
            $contentType = "image/$ext";
        }elseif($ext=='pdf'){
            $contentType = "application/$ext";
        }elseif($ext=='docx'||$ext=='doc'){
            $contentType = "application/$ext";
        }

        $path = awsBucketPath.$url;
        $data = aws_image($path);
        if($data==FALSE){
            return FALSE;
        }else{
            header('Cache-Control: public');
            header('Content-Description: File Transfer');
            header("Content-Type: $contentType");
            header("Content-Disposition: inline; filename=" . $filename);
            return $data;
        }
    }
    function view_vendor_file($vendorFileId){
        $fileInfo = $this->vendorsmodel->getVendorFileById($vendorFileId);
        $fileUrl = $fileInfo['fileurl'];
        $filename = $fileInfo['filename'];
        if($fileUrl){
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $modifiedPath = urlencode($fileUrl);
            $data['msg'] = "";
            $this->_displayFile($modifiedPath,$filename,$ext);
        }else{
            $data['msg'] = 'Invalid Request';
            $this->load->view('vendor/filenotfound',$data);
        }
    }

}

