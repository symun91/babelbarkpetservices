<?php
/**
 * @property appusersmodel $appusersmodel
 * @property sikkapetimportrequestsmodel  $sikkapetimportrequestsmodel
 * @property petmodel $petmodel
 * @property petvaccinationmodel $petvaccinationmodel
 */
class ImportSikkaPet extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('appusersmodel','sikkapetimportrequestsmodel','petmodel','petvaccinationmodel'));
	
	}
	
	function index()
	{
		$data['msg'] = '';
		if(!empty($_GET)){
			
			$token = $this->input->get('token');
			$id = $this->input->get('id');
			$sikkapetid =$this->input->get('sikkapetid');

			$recorddetails = $this->sikkapetimportrequestsmodel->verifyToken($id,$token);
			if($recorddetails!=="") 
			{

				if($sikkapetid)
				{

					$resultvaccine = $this->petvaccinationmodel->getPetVaccineDetails($sikkapetid);


					for($i=0;$i<count($resultvaccine);$i++)
					{
						$vaccinedata = array();
						$vaccinedata['petid'] = $recorddetails['apppetid'];
						$vaccinedata['vaccineid'] = $resultvaccine[$i]['id'];
						$vaccinedata['duedate'] = $resultvaccine[$i]['duedate'];
						$checkVaccineExist = $this->petvaccinationmodel->checkVaccineExist($recorddetails['apppetid'],$resultvaccine[$i]['id'],$resultvaccine[$i]['duedate']);
						if($checkVaccineExist==0){
                            $this->petvaccinationmodel->addVaccination($vaccinedata);
                        }

					}
					$newtoken  = $this->genrateSessionToken();

					$importdata = array();
					$importdata['token'] = $newtoken;
					$importdata['sikkapetid'] = $sikkapetid;
					$this->sikkapetimportrequestsmodel->updatepetimportrequest($id,$importdata);
					$data['msg'] = 'Medical Reports Imported Successfully.';

				}else{
					$data['msg'] = 'Invalid Information';
				}


			}
			else {
				$data['msg'] = 'Invalid Token';
			}
			  
		}
		else
		{
			$data['msg'] = ' Token is missing';
		}
		  $this->load->view('importsikkapetview.php',$data);
	}

	 private static function genrateSessionToken()
        {
		$sessiontok=md5(microtime().rand());
		return $sessiontok;
        }
	 
}
?>