<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Post-Receive Hooks API
 *
 * @package     CodeIgniter Git Deploy
 * @author      Vince Kronlein
 * @copyright   Copyright (c) 2017
 */
class Deploy extends CI_Controller
{
    protected $directory;
    
    protected $parent;
    
    protected $repository;
    
    protected $branch;

    /**
     * Instantiate the Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // Is the config file in the environment folder?
        if (defined('ENVIRONMENT') && file_exists(APPPATH . 'config/' . ENVIRONMENT . '/github.php')) {
            $file = APPPATH . 'config/' . ENVIRONMENT . '/github.php';
        } else {
            if (! file_exists(APPPATH . 'config/github.php')) {
                show_error('The configuration file github.php does not exist.');
            } else {
                $file = APPPATH . 'config/github.php';
            }
        }
        
        include($file);
        
        $paths = $this->seek();

        $this->parent     = $paths['parent'];
        $this->directory  = $paths['directory'];
        
        $this->repository = strtolower($config['github']['repository']);
        $this->branch     = strtolower($config['github']['branch']);
    }

    /**
     * Receive the payload request fron GitHub
     * 
     * @return string
     */
    public function receive()
    {
        $payload = $this->input->get_post('payload');
        
        $message = 'BEGIN GIT PULL ' . date('M d, Y g:i:s', time()) . " \n\n";

        if ($payload) {
            $payload = json_decode($payload);

            /*
            |--------------------------------------------------------------------------
            | Inspect the Repository
            |--------------------------------------------------------------------------
            */
            if ($this->repository == strtolower($payload->repository->name)) {

                /*
                |--------------------------------------------------------------------------
                | Inspect the Branch
                |--------------------------------------------------------------------------
                */
                $head = 'refs/heads/' . $this->branch;

                if ($payload->ref == $head) {

                    /*
                    |--------------------------------------------------------------------------
                    | Change Working Directory
                    |--------------------------------------------------------------------------
                    */
                    chdir ($this->parent . DIRECTORY_SEPARATOR . $this->directory);

                    /*
                    |--------------------------------------------------------------------------
                    | Run the Git Pull Command
                    |--------------------------------------------------------------------------
                    */
                    exec("/usr/bin/git pull origin " . $this->branch . " 2>&1", $result);

                    foreach ($result as $key => $value) {
                        $message .= $value . "\n";
                    }

                } else {
                    $message .= "Wrong Branch, pull failed. \n";
                }

            } else {
                $message .= "Wrong Repository, pull failed. \n";
            }
            
        } else {
            $message .= "Empty payload, pull failed. \n";
        }

        /*
        |--------------------------------------------------------------------------
        | Return Our Message to the Git Response
        |--------------------------------------------------------------------------
        */
       
        echo $message;
    }

    /**
     * Find the correct directory for running the pull command.
     * 
     * @return array
     */
    private function seek()
    {
        $directories = [];
        $dirs        = explode('/', dirname(__FILE__));

        // remove empty first item
        array_shift($dirs);

        // remove the top/home directory as we won't be able to read it anyway
        $count = (count($dirs) - 1);

        for ($i = 0; $i < $count; $i++) {
            $directories[] = '/' . implode('/', $dirs);
            array_pop($dirs);
        }

        $paths = [];

        foreach ($directories as $directory) {
            if (in_array('.git', scandir($directory))) {
                $paths['parent']    = dirname($directory);
                $paths['directory'] = basename($directory);
            }
        }

        return $paths;
    }
}
