<?php
/**
 * @property appointmentmodel      $appointmentmodel
 * @property petbreedmodel         $petbreedmodel
 * @property servicemodel          $servicemodel
 * @property vendorsmodel          $vendorsmodel
 * @property vendorcategoriesmodel $vendorcategoriesmodel
 * @property customermodel         $customermodel
 * @property petmodel              $petmodel
 * @property appusersmodel         $appusersmodel
 * @property appuservendorsmodel   $appuservendorsmodel
 * @property petprescriptionmodel  $petprescriptionmodel
 * @property petvaccinationmodel   $petvaccinationmodel
 * @property promotionsmodel       $promotionsmodel
 * @property sikkaofficemodel      $sikkaofficemodel
 */
class Sikka extends CI_Controller
{
    var $data=array();
    var $file_path;
    //var $customers;
    //var	$appuser;

    function __construct()
    {

        parent::__construct();
        $this->load->library('super_admin_init_elements');
        $this->super_admin_init_elements->init_elements();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('appointmentmodel');
        $this->load->model('petbreedmodel');
        $this->load->model('servicemodel');
        $this->load->model('vendorsmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('customermodel');
        $this->load->model('petmodel');
        $this->load->model('appusersmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('petprescriptionmodel');
        $this->load->model('petvaccinationmodel');
        $this->load->model('promotionsmodel');
        $this->load->model('sikkaofficemodel');



        /*if (!file_exists('resources/downloads/csv/')) {
                           mkdir('resources/downloads/csv/');
                           }
       $this->file_path =  realpath('./resources/downloads/csv/');*/



    }

    function fetchStoreSikkaOffices()
    {
        if($this->session->userdata('isadmin'))
        {
            $sikkaofficeid = $this->input->post('sikkaofficeid');

            $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($sikkaofficeid);


            $request_key = $this->getrequestid($sikkaofficeid,$secret_key['secret_key']);

            if(is_null($request_key))
            {
                echo json_encode("Cannot connect with SIKKA server, please try again");
                return;
            }

            $sikkapracticesurl = "https://api.sikkasoft.com/v2/practices?request_key=".$request_key;
            $detailjson = file_get_contents($sikkapracticesurl);

            $bizbarksikkavendors  = $this->vendorsmodel->getsikkavendor($sikkaofficeid);

            $sikkapractices = json_decode($detailjson);

            if($bizbarksikkavendors)
            {
//                if($bizbarksikkavendors['sikkapractice_id'] == $sikkapractices->practice_id)
//                {
                    $sikkapractices->shouldUpdate = 1;
                    $sikkapractices->prescription_added = $bizbarksikkavendors['prescription_added'];
                    $sikkapractices->vaccinationadded = $bizbarksikkavendors['vaccinationadded'];
//                }
//                else{
//                    $sikkapractices->shouldUpdate = 0;
//                    $sikkapractices->prescription_added = 0;
//                    $sikkapractices->vaccinationadded = 0;
//                }
            }else{
                $sikkapractices->shouldUpdate = 0;
                $sikkapractices->prescription_added = 0;
                $sikkapractices->vaccinationadded = 0;
            }

            $sikkapractices->office_id = $sikkaofficeid;
            $sikkapractices->email = $bizbarksikkavendors['email'];


            if($detailjson)
            {
                echo json_encode($sikkapractices);
            }
            else{
                echo json_encode("No data found");
            }
        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }

    private function getrequestid($sikkaofficeid,$secret_key)
    {
        $sikkarequestkey = "https://api.sikkasoft.com/v2/start?app_id=".APP_ID."&app_key=".APP_KEY."&office_id=".$sikkaofficeid."&secret_key=".$secret_key;
        $detailjson = @file_get_contents($sikkarequestkey);
        if($detailjson === FALSE)
        {
            return NULL;
        }else{
            $requestkey = json_decode($detailjson);
            return $requestkey[0]->request_key;
        }



    }

    private function addGuarantortoDatabase($guarantors,$vendorid)
    {
        $data = array();
        for ($i = 0; $i < count($guarantors); $i++)
        {
            $data = array(
                'vendorid' => $vendorid,
                'firstname' => $guarantors[$i]->firstname,
                'lastname' => $guarantors[$i]->lastname,
                'phoneno' => $guarantors[$i]->workphone,
                'email' => $guarantors[$i]->email,
                'address' => $guarantors[$i]->address_line1,
                'city' => $guarantors[$i]->city,
                'state' => $guarantors[$i]->state,
                'country' => "",
                'zipcode' => $guarantors[$i]->zipcode,
                'sikka_status' => $guarantors[$i]->status,
                'guarantor_id' => $guarantors[$i]->guarantor_id,
                'practice_id' => $guarantors[$i]->practice_id,
                'patient_id'=>$guarantors[$i]->patient_id
            );
            $this->customermodel->updateSikkaCustomer($data);
        }
    }

    private function addVetPatientstoDatabase($vet_patients)
    {
        $data = array();

        $count_repeat = 0;

        for ($i = 0; $i < count($vet_patients); $i++)
        {

            $count_repeat = 0;

            $gender = $vet_patients[$i]->gender;

            if (strpos($gender, 'Female') !== false)
            {
                $gender = 'F';
            }
            else
            {
                $gender = 'M';
            }
            if($vet_patients[$i]->microchip)
            {
                $microchipped = '1';
            }
            else
            {
                $microchipped = '0';
            }

            $breedid = $this->petbreedmodel->getBreedid($vet_patients[$i]->breed);
            $breedid = intval($breedid);
            $petid = $this->genratePrimaryKey('50');
            while($this->petmodel->petIdExists($petid) == TRUE)
            {
                $petid = $this->genratePrimaryKey('60');
                $count_repeat++;

                $arr_content = array();

                $arr_content['percent'] = 70+$count_repeat;
                $arr_content['message'] = "Duplicate";

                file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


            }
            $weight = 0;

            if($vet_patients[$i]->weight)
            {
                $weight = intval($vet_patients[$i]->weight);
            }
            $data = array(
                'name' => $vet_patients[$i]->petname,
                'gender' => $gender,
                'birthdate' => $vet_patients[$i]->birthdate,
                'isadopted' => 0,
                'currentweight' => $weight,
                'furcolor' => $vet_patients[$i]->color,
                'microchipped' => $microchipped,
                'microchipid' => $vet_patients[$i]->microchip,
                'primarybreed' => $breedid,
                'veterinarianid' => 1,
                'profileCompletion'=> 70,
                'guarantor_id' => $vet_patients[$i]->guarantor_id,
                'practice_id' => $vet_patients[$i]->practice_id,
                'patient_id'=>$vet_patients[$i]->patient_id
            );

            $response = array();

            $response =  array_merge($response,$data);

            $response['pettid']  = $petid;

            $response['time'] = 'Before';

            file_put_contents("tmp/" . "tempsikkapet" . ".txt", json_encode($response));


            $this->petmodel->updateSikkaPet($data,$petid);

            $response['time'] = 'After';
            file_put_contents("tmp/" . "tempsikkapet" . ".txt", json_encode($response));
        }
    }

    private function addPrescriptiontoDatabase($prescriptions)
    {
        $data = array();
        $count = 0;

        for ($i = 0; $i < count($prescriptions); $i++)
        {
            $itemtype = $prescriptions[$i]->item_type;

            if (strpos($itemtype, 'Meds:') !== false && $this->petmodel->sikkapetexisttoggle($prescriptions[$i]->guarantor_id,$prescriptions[$i]->patient_id) == TRUE)
            {
                $data[$count] = array(
                    'description' => $prescriptions[$i]->description,
                    'item_type' => $prescriptions[$i]->item_type,
                    'direction' => $prescriptions[$i]->direction,
                    'quantity' => $prescriptions[$i]->quantity,
                    'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                    'patient_id' => $prescriptions[$i]->patient_id
                );
                $count++;
                //$this->petprescriptionmodel->addPrescriptions($data);
            }
            else if(strpos($itemtype, 'Item') !== false && $this->petmodel->sikkapetexisttoggle($prescriptions[$i]->guarantor_id,$prescriptions[$i]->patient_id) == TRUE)
            {
                $data[$count] = array(
                    'description' => $prescriptions[$i]->description,
                    'item_type' => $prescriptions[$i]->item_type,
                    'direction' => $prescriptions[$i]->direction,
                    'quantity' => $prescriptions[$i]->quantity,
                    'sikka_prescription_id' => $prescriptions[$i]->prescription_id,
                    'patient_id' => $prescriptions[$i]->patient_id
                );
                $count++;
            }

        }

        /// echo json_encode($data);
        $this->petprescriptionmodel->addPrescriptions($data);

    }
    private function addVaccinationstoDatabase($vaccinations,$office_id)
    {
        $data = array();

        $datacc = array();
        $count = 0;

        $saveit = TRUE;

        for ($i = 0; $i < count($vaccinations); $i++)
        {
            $petiddata = $this->petmodel->sikkapetexist($vaccinations[$i]->guarantor_id,$vaccinations[$i]->patient_id);
            $description = $vaccinations[$i]->description;
            if($petiddata['petid'] > 0)
            {
                $data['petid'] = $petiddata['petid'];


                switch ($office_id) {
                    case 'V11128': #Strathem
                        if(strpos($description, 'DAPP 5 Vaccine 1 yr') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'DAPP 5 Vaccine 3 yr') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Bordetella / Para / Adeno Vaccine nasal') !== false)
                        {
                            $data['vaccineid'] = 10; #Adenovirus (canine hepatitis)
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Rabies Vaccine Canine 1 yr (Nobivac)') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Rabies Vaccine Canine 3 yr (Nobivac)') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Canine Influenza Vaccine H3N2/H3N8') !== false)
                        {
                            $data['vaccineid'] = 2; #CIV
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Leptospirosis Vaccine 1 yr') !== false)
                        {
                            $data['vaccineid'] = 3; #Leptospirosis
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm Lyme Ehrlic Anap (In-house)') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm Antigen by ELISA Snap') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Heartworm / Tick 4Dx (I72440)') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{

                            $saveit = FALSE;
                        }
                        break;

                    case 'V9333': #Acacia

                        if(strpos($description, 'WP DA2PPV/Corona Adult') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'RABIES VACCINATION 1 YEAR') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Rabies Canine') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Rabies Purevax') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'WP Bordetella Adult') !== false)
                        {
                            $data['vaccineid'] = 1; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Lab-Heartworm Antigen Canine') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'WP Blood Parasite Screen/HWT') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{
                            $saveit = FALSE;
                        }

                        break;
                    case 'V15410': #BabelBark AVImark Test

                        if(strpos($description, 'DA2PP') !== false)
                        {
                            $data['vaccineid'] = 8; #pervovirus
                            $saveit = TRUE;
                        }
                        else if(strpos($description, 'Rabies') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Bordatella') !== false)
                        {
                            $data['vaccineid'] = 11; #Rabies
                            $saveit = TRUE;

                        }
                        else if(strpos($description, 'Lyme') !== false)
                        {
                            $data['vaccineid'] = 6; #Lyme
                            $saveit = TRUE;
                        }
                        else{
                            $saveit = FALSE;
                        }

                        break;
                    default:
                        return;
                }


                $data['duedate'] = $vaccinations[$i]->due_date;
                if($saveit==TRUE)
                {
                    $datacc[$count] = $data;
                    $count++;
                }

            }

        }
        $this->petvaccinationmodel->addPetVaccination($datacc);
    }



    function addPrescription()
    {
        ini_set('max_execution_time', 6000);
        if($this->session->userdata('isadmin'))
        {
            $sikkapracticeofficeid = $this->input->post('practice_id');
            $office_practiceid = explode("_", $sikkapracticeofficeid);

            $result = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);
            $vendorid = $result['vendorid'];
            if(!$vendorid)
            {
                echo json_encode("error");
                return;
            }

            $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);
            $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
            $offset = 0;
            $limit = 500;

            $detailjson = $this->fetchPrescriptions($request_key,$office_practiceid[1],$offset,$limit);



            if($detailjson)
            {

                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);
                $prescriptions = $detail[0]->items;

                $this->addPrescriptiontoDatabase($prescriptions);


                $loopCount = ceil($totalCount/$limit);

                for($i=0;$i<$loopCount;$i++)
                {
                    $offset = ($i+1);
                    $detailsecondjson = $this->fetchPrescriptions($request_key,$office_practiceid[1],$offset,$limit);
                    $detail = json_decode($detailsecondjson);
                    if(is_array($detail) && is_array($detail[0]->items))
                    {
                        $prescriptions = $detail[0]->items;
                        $this->addPrescriptiontoDatabase($prescriptions);
                    }
                }

                $data = array('prescription_added' => 1);


                $result = $this->vendorsmodel->updateVendor($data,$vendorid);

                if($result>0)
                {
                    echo json_encode("success");
                }else{
                    echo "Something went wrong!!";
                }
            }
            else{
                echo json_encode("No prescriptions found for this practice!");
            }
        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }

    function addVaccination()
    {
        ini_set('max_execution_time', 6000);

        $arr_content = array();

        $arr_content['percent'] = 0;
        $arr_content['message'] = "Started";
        file_put_contents('tmp/temp.txt', json_encode($arr_content));



        if($this->session->userdata('isadmin'))
        {
            $sikkapracticeofficeid = $this->input->post('practice_id');
            $office_practiceid = explode("_", $sikkapracticeofficeid);

            $result = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);
            $vendorid = $result['vendorid'];
            if(!$vendorid)
            {
                echo json_encode("error");
                return;
            }

            $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);
            $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
            $offset = 0;
            $limit = 2000;


            $detailjson = $this->fetchVaccinations($request_key,$office_practiceid[1],$offset,$limit);

            $arr_content['percent'] = 1;
            $arr_content['message'] = "";
            file_put_contents('tmp/temp.txt', json_encode($arr_content));

            $office_id = $office_practiceid[0];


            if($detailjson)
            {

                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);
                $vaccinations = $detail[0]->items;

                $this->addVaccinationstoDatabase($vaccinations,$office_id);


                $loopCount = ceil($totalCount/$limit);

                for($i=0;$i<$loopCount;$i++)
                {
                    $offset = ($i+1);

                    $percentage = ($i/$loopCount);


                    $percentage = ceil($percentage*100);

                    $arr_content['percent'] = $percentage;
                    $arr_content['message'] = "";
                    file_put_contents('tmp/temp.txt', json_encode($arr_content));



                    $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);
                    $detailsecondjson = $this->fetchVaccinations($request_key,$office_practiceid[1],$offset,$limit);
                    $detail = json_decode($detailsecondjson);
                    if(is_array($detail) && is_array($detail[0]->items))
                    {
                        $vaccinations = $detail[0]->items;
                        $this->addVaccinationstoDatabase($vaccinations,$office_id);
                    }
                }

                $data = array('vaccinationadded' => 1);


                $result = $this->vendorsmodel->updateVendor($data,$vendorid);

                if($result>0)
                {
                    $arr_content['percent'] = 100;
                    $arr_content['message'] = "";
                    file_put_contents('tmp/temp.txt', json_encode($arr_content));
                    echo json_encode("success");
                }
                else
                {
                    echo json_encode("Something went wrong!!");
                }

            }
            else
            {
                echo json_encode("No vaccinations found for this practice!");
            }
        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }

    function addtoBizBark()
    {

        ini_set('max_execution_time', 6000);

        $arr_content = array();

        $arr_content['percent'] = 0;
        $arr_content['message'] = "Started";


        file_put_contents('tmp/temp.txt', json_encode($arr_content));






        if($this->session->userdata('isadmin'))
        {

            $sikkapracticeofficeid = $this->input->post('practice_id');

            $vendoremail = $this->input->post('email');

            $office_practiceid = explode("_", $sikkapracticeofficeid);

            $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);




            $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);




            $sikkapracticesurl = "https://api.sikkasoft.com/v2/practices/".$office_practiceid[1]."?request_key=".$request_key;
            $detailjson = file_get_contents($sikkapracticesurl);


            ////// Mapping Practice Sikka to Vendor BizBark

            $practice = json_decode($detailjson);

            $address = $practice->address_line1." ".$practice->address_line2." ".$practice->city." ".$practice->state." ".$practice->country." ".$practice->zipcode;


            $result = $this->geocode($address);

            if($result)
            {
                $latlng = $result[0].",".$result[1];
            }
            else{
                $latlng = "";
            }

            if(!$practice->email)
            {
                $email  = $vendoremail;
            }
            else
            {
                $email = $practice->email;
            }

            $name = $practice->name;

            $password = $this->genratePrimaryKey('20');
            $vendorid = $this->genratePrimaryKey('40');
            $datacc = array(
                'vendorid' => $vendorid ,
                'email' => $email,
                //'password' => md5($this->input->post('password')),
                'password' =>$password,
                'firstname' => "Sikka",
                'lastname' => "Vet",
                'comapnyname' => $name,
                'category' => "1",
                'address' => $practice->address_line1." ".$practice->address_line2,
                'city' => $practice->city,
                'state' => $practice->state,
                'country' => $practice->country,
                'zipcode' => $practice->zipcode,
                'contact'=>$practice->phone,
                'website'=>$practice->website,
                'agreedterms' => 1,
                'createdon' =>date('Y-m-d H:i'),
                'addresslatlng'=>$latlng,
                'operatingdays' => "M,T,W,Th,F,Sa,Su",
                'availabilityfromhour' => "9:00,9:00,9:00,9:00,9:00,9:00,9:00",
                'availabilitytohour' => "5:00,5:00,5:00,5:00,5:00,5:00,5:00",
                'sikkapractice_id'=>$office_practiceid[1],
                'sikkaoffice_id'=>$office_practiceid[0],
                'prescription_added'=>0,
                'vaccinationadded'=>0
            );
            $result = 	$this->vendorsmodel->registerSikkaPracticeAsVendor($datacc);


            $arr_content['percent'] = 5;
            $arr_content['message'] = "Vendor Created";


            file_put_contents("tmp/temp.txt", json_encode($arr_content));



            $subject = "BizBark account creation";
            $toemail=$email;
            // To send HTML mail, the Content-type header must be set.

            $sendmessage  = file_get_contents('resources/sikkaAccountCreation.html');
            //$sendmessage = str_replace('%username%', $username, $sendmessage);
            $sendmessage = str_replace('%name%', $name, $sendmessage);
            $sendmessage = str_replace('%email%', $email, $sendmessage);
            $sendmessage = str_replace('%password%', $password, $sendmessage);

            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
            $this->email->subject($subject);
            //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');

            $this->email->message($sendmessage);


            $result =$this->email->send();


            $arr_content['percent'] = 10;
            $arr_content['message'] = "Email Sent";


            file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));



            $offset = 0;
            $limit = 5000;

            $detailjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

            $arr_content['percent'] = 20;
            $arr_content['message'] = "Fetching Veterinary Patients";


            file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


            $veterinary_patients = array();

            if($detailjson)
            {
                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);

                $veterinary_patients = $detail[0]->items;

                ///////////////////// LOOP //////////////////////

                $loopCount = ceil($totalCount/$limit);

                for($i=0;$i<$loopCount;$i++)
                {
                    $offset = ($i+1);
                    $detailsecondjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

                    $detail = json_decode($detailsecondjson);
                    if(is_array($detail) && is_array($detail[0]->items))
                    {
                        $veterinary_patients = array_merge($veterinary_patients,$detail[0]->items);

                    }

                }

                $arr_content['percent'] = 30;
                $arr_content['message'] = "Seperating Veterinary Patients type Canine";


                file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));

                $finalVeterinary_patients = array();

                $vetPatients_patient_id = array();

                for($i=0;$i<count($veterinary_patients);$i++)
                {
                    $specie = $veterinary_patients[$i]->species;

                    $specie = strtolower($specie);
                    if($specie=='canine')
                    {
                        array_push($finalVeterinary_patients, $veterinary_patients[$i]);
                        array_push($vetPatients_patient_id, $veterinary_patients[$i]->patient_id);

                    }
                }



                $arr_content['percent'] = 40;
                $arr_content['message'] = "Fetching Guarantors";


                file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                $offset = 0;
                $limit = 5000;

                $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);

                $guarantors = array();


                $detailjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);


                if($detailjson)
                {
                    $detail = json_decode($detailjson);
                    $totalCount = intval($detail[0]->total_count);
                    $guarantors = $detail[0]->items;


                    ///////////////////// LOOP //////////////////////
                    if($totalCount>$limit)
                    {
                        $loopCount = ceil($totalCount/$limit);
                        for($i=0;$i<$loopCount;$i++)
                        {
                            $offset = ($i+1);
                            $detailsecondjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);
                            $detail = json_decode($detailsecondjson);
                            if(is_array($detail) && is_array($detail[0]->items))
                            {
                                $guarantors = array_merge($guarantors,$detail[0]->items);
                            }
                        }
                    }
                    $arr_content['percent'] = 50;
                    $arr_content['message'] = "Matching Guarantors & Veterinary Patients";


                    file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                    $finalGuarantors = array();
                    $finalGuarantorsGuarantorid = array();

                    $finalPets = array();

                    for($i=0;$i<count($guarantors);$i++)
                    {
                        if(in_array($guarantors[$i]->patient_id, $vetPatients_patient_id))
                        {
                            if(!in_array($guarantors[$i]->guarantor_id, $finalGuarantorsGuarantorid))
                            {
                                array_push($finalGuarantors,$guarantors[$i]);
                                array_push($finalGuarantorsGuarantorid,$guarantors[$i]->guarantor_id);
                            }
                        }
                    }

                    $arr_content['percent'] = 60;
                    $arr_content['message'] = "Adding Guarantors into database Count: ". count($finalGuarantors);


                    file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                    $this->addGuarantortoDatabase($finalGuarantors,$vendorid);
                    $arr_content['percent'] = 70;
                    $arr_content['message'] = "Adding Patients into database Count: ". count($finalVeterinary_patients);


                    file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                    $this->addVetPatientstoDatabase($finalVeterinary_patients);

                    $arr_content['percent'] = 80;
                    $arr_content['message'] = "Adding Guarantors & Patients relation Customer pets";


                    file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                    $customers_of_this_vendor = $this->customermodel->getAllCustomers($vendorid);

                    $arr_content['percent'] = 90;
                    $arr_content['message'] = "Adding Guarantors & Patients relation Customer pets";


                    file_put_contents("tmp/" . "temp" . ".txt", json_encode($arr_content));


                    for($c=0;$c<count($customers_of_this_vendor);$c++)
                    {

                        $g_id = $customers_of_this_vendor[$c]['guarantor_id'];

                        $c_id = $customers_of_this_vendor[$c]['customerid'];
                        $pet_ids = $this->petmodel->getCustomerPets($g_id);

                        for ($i=0; $i <count($pet_ids) ; $i++) {

                            $data = array('customerid' => $c_id,
                                'petid' => $pet_ids[$i]['petid']
                            );

                            $this->customermodel->updatepet($data,$c_id,$pet_ids[$i]['petid']);
                        }
                    }

                    $arr_content['percent'] = 100;
                    $arr_content['message'] = "Success";


                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));

                }

                echo json_encode(count($finalGuarantors));
            }
            else{
                echo json_encode("Something went wrong");
            }

        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }


    function getProgress()
    {

        //$file = str_replace(".", "", $_GET['file']);
        $isfirsttime = $this->input->post('isfirst');
        $file = "tmp/" . "temp" . ".txt";
        if($isfirsttime == "true")
        {
            if(file_exists($file))
            {
                unlink($file);
            }

            $arr_content['percent'] = 0;
            $arr_content['message'] = "Started";


            file_put_contents('tmp/temp.txt', json_encode($arr_content));
            echo json_encode(array("percent" => 0, "message" => "File deleted"));
            return;
        }
        // Make sure the file is exist.
        if (file_exists($file)) {
            // Get the content and echo it.
            $text = file_get_contents($file);
            echo $text;


            // Convert to JSON to read the status.
            $obj = json_decode($text);
            // If the process is finished, delete the file.
            if ($obj->percent == 100) {
                unlink($file);
            }
        }
        else {
            $arr_content = array();

            $arr_content['percent'] = 0;
            $arr_content['message'] = "Started";


            file_put_contents('tmp/temp.txt', json_encode($arr_content));
            echo json_encode(array("percent" => 0, "message" => "File not created"));
        }
        //echo json_encode($this->session->userdata('progress_practice'));
    }

    private function fetchGuarantors($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/guarantors?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }
    private function fetchVeterinary_patients($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/patients/veterinary_patients?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);
    }
    private function fetchPrescriptions($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/prescriptions?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);

    }
    private function fetchVaccinations($request_key,$practice_id,$offset,$limit)
    {
        $sikkapracticesurl = "https://api.sikkasoft.com/v2/reminders?request_key=".$request_key."&practice_id=".$practice_id."&limit=".$limit."&offset=".$offset;
        return file_get_contents($sikkapracticesurl);

    }

    function updatepracticetoBizbark()
    {

        $arr_content = array();

        $arr_content['percent'] = 1;
        $arr_content['message'] = "Started";


        file_put_contents('tmp/temp.txt', json_encode($arr_content));

        ini_set('max_execution_time', 6000);

        if($this->session->userdata('isadmin'))
        {
            $sikkapracticeofficeid = $this->input->post('practice_id');

            $office_practiceid = explode("_", $sikkapracticeofficeid);

            $vendor_id = $this->vendorsmodel->getVendorId($office_practiceid[0],$office_practiceid[1]);


            $secret_key = $this->sikkaofficemodel->getSecretKeyOfOffice($office_practiceid[0]);


            $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);

            $arr_content['percent'] = 5;
            $arr_content['message'] = "Fetching Sikka Practices";
            file_put_contents('tmp/temp.txt', json_encode($arr_content));

            $sikkapracticesurl = "https://api.sikkasoft.com/v2/practices/".$office_practiceid[1]."?request_key=".$request_key;
            $detailjson = file_get_contents($sikkapracticesurl);

            $practice = json_decode($detailjson);

            $address = $practice->address_line1." ".$practice->address_line2." ".$practice->city." ".$practice->state." ".$practice->country." ".$practice->zipcode;

            $result = $this->geocode($address);

            if($result)
            {
                $latlng = $result[0].",".$result[1];
            }
            else{
                $latlng = "";
            }

            $datacc = array(
                //'password' => md5($this->input->post('password'))
                'firstname' => "Sikka",
                'lastname' => "Vet",
                'comapnyname' => $practice->name,
                'address' => $practice->address_line1." ".$practice->address_line2,
                'city' => $practice->city,
                'state' => $practice->state,
                'country' => $practice->country,
                'zipcode' => $practice->zipcode,
                'contact'=>$practice->phone,
                'website'=>$practice->website,
                'agreedterms' => 1,
                'createdon' =>date('Y-m-d H:i'),
                'addresslatlng'=>$latlng
            );

            if($practice->email)
            {
                $datacc['email'] = $practice->email;
            }

            $result = 	$this->vendorsmodel->updateVendor($datacc,$vendor_id['vendorid']);

            $arr_content['percent'] = 10;
            $arr_content['message'] = "Vendor updated";


            file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));


            $offset = 0;
            $limit = 5000;

            $detailjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

            $arr_content['percent'] = 20;
            $arr_content['message'] = "fetching Veterinary Patients";


            file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));

            $veterinary_patients = array();

            if($detailjson)
            {
                $detail = json_decode($detailjson);
                $totalCount = intval($detail[0]->total_count);

                $veterinary_patients = $detail[0]->items;




                ///////////////////// LOOP //////////////////////
                $loopCount = ceil($totalCount/$limit);

                for($i=0;$i<$loopCount;$i++)
                {
                    $offset = ($i+1);
                    $detailsecondjson = $this->fetchVeterinary_patients($request_key,$office_practiceid[1],$offset,$limit);

                    $detail = json_decode($detailsecondjson);
                    if(is_array($detail) && is_array($detail[0]->items))
                    {
                        $veterinary_patients = array_merge($veterinary_patients,$detail[0]->items);

                    }

                }

                $finalVeterinary_patients = array();
                $vetPatients_patient_id = array();

                for($i=0;$i<count($veterinary_patients);$i++)
                {
                    $specie = $veterinary_patients[$i]->species;

                    $specie = strtolower($specie);
                    if($specie=='canine')
                    {
                        array_push($finalVeterinary_patients, $veterinary_patients[$i]);
                        array_push($vetPatients_patient_id, $veterinary_patients[$i]->patient_id);

                    }
                }






                $offset = 0;
                $limit = 5000;

                $request_key = $this->getrequestid($office_practiceid[0],$secret_key['secret_key']);

                $guarantors = array();
                $detailjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);

                $arr_content['percent'] = 40;
                $arr_content['message'] = "Fetching Guarantors";


                file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));
                if($detailjson)
                {
                    $detail = json_decode($detailjson);
                    $totalCount = intval($detail[0]->total_count);
                    $guarantors = $detail[0]->items;

                    ///////////////////// LOOP //////////////////////
                    if($totalCount>$limit)
                    {
                        $loopCount = ceil($totalCount/$limit);
                        for($i=0;$i<$loopCount;$i++)
                        {
                            $offset = ($i+1);
                            $detailsecondjson = $this->fetchGuarantors($request_key,$office_practiceid[1],$offset,$limit);
                            $detail = json_decode($detailsecondjson);
                            if(is_array($detail) && is_array($detail[0]->items))
                            {
                                $guarantors = array_merge($guarantors,$detail[0]->items);
                            }
                        }
                    }
                    $arr_content['percent'] = 50;
                    $arr_content['message'] = "Matching Guarantors";


                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));
                    $finalGuarantors = array();
                    $finalGuarantorsGuarantorid = array();

                    $finalPets = array();

                    for($i=0;$i<count($guarantors);$i++)
                    {
                        if(in_array($guarantors[$i]->patient_id, $vetPatients_patient_id))
                        {
                            if(!in_array($guarantors[$i]->guarantor_id, $finalGuarantorsGuarantorid))
                            {
                                array_push($finalGuarantors,$guarantors[$i]);
                                array_push($finalGuarantorsGuarantorid,$guarantors[$i]->guarantor_id);
                            }
                        }
                    }
                    $arr_content['percent'] = 60;
                    $arr_content['message'] = "Saving Guarantors to Database";


                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));
                    $this->addGuarantortoDatabase($finalGuarantors,$vendor_id['vendorid']);

                    $arr_content['percent'] = 70;
                    $arr_content['message'] = "Saving Patients to Database";


                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));

                    $this->addVetPatientstoDatabase($finalVeterinary_patients);

                    $arr_content['percent'] = 80;
                    $arr_content['message'] = "Making Customer Pets relationship";
                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));

                    $customers_of_this_vendor = $this->customermodel->getAllCustomers($vendor_id['vendorid']);

                    $arr_content['percent'] = 90;
                    $arr_content['message'] = "Making Customer Pets relationship and Saving it.";
                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));

                    for($c=0;$c<count($customers_of_this_vendor);$c++)
                    {

                        $g_id = $customers_of_this_vendor[$c]['guarantor_id'];

                        $c_id = $customers_of_this_vendor[$c]['customerid'];
                        $pet_ids = $this->petmodel->getCustomerPets($g_id);

                        for ($i=0; $i <count($pet_ids) ; $i++) {

                            $data = array('customerid' => $c_id,
                                'petid' => $pet_ids[$i]['petid']
                            );

                            $this->customermodel->updatepet($data,$c_id,$pet_ids[$i]['petid']);
                        }



                    }

                    $arr_content['percent'] = 100;
                    $arr_content['message'] = "Success";


                    file_put_contents("tmp/" ."temp" . ".txt", json_encode($arr_content));
                }


                echo json_encode("success");
            }
            else{
                echo json_encode("Something went wrong");
            }
        }
        else{
            redirect('vendor/administrator/logout');
        }
    }

    // function to geocode address, it will return false if unable to geocode address
    function geocode($address){

        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                $data_arr = array();

                array_push(
                    $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
                );

                return $data_arr;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    private static function genratePrimaryKey($serverid)
    {
        $primarykey=$serverid.time().mt_rand(1000, 9999);
        if($primarykey>9223372036854775807) //max of 64 bit int
        {
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }


}
?>