<?php
class Logout extends CI_Controller
{
	
	function __construct()
	{
		
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('admin_model');
	}
	
	function index()
	{
		$this->session->sess_destroy();
		redirect('admin/login');
	}
}
?>
