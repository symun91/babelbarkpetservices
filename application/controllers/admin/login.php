<?php
class Login extends CI_Controller
{
	
	function __construct()
	{
		
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('admin_model');
	}
	
	function index()
	{
		$data=array();
		$this->load->library('super_admin_init_elements');
		$data['head'] = $this->super_admin_init_elements->init_head();
		
		
		
		if($this->input->post('submit'))
		{
			if($this->validate_form_data()==true)
			{
				$username=$this->input->post('username');
				$password=$this->input->post('password');
				$adminData=$this->admin_model->find_data('row',array('username'=>$username,'password'=>$password),'admin',0,0,'adminid');
				if(!empty($adminData))
				{
					
					$this->session->set_flashdata('success_msg','Login Successful.');

					$admin=array('isloggedin'=>1,'adminDetails'=>$adminData,'role'=>'superadmin');
					$this->session->set_userdata('admin',$admin);
					$this->session->set_userdata('isadmin',TRUE);
					redirect('admin/home/index');
				}else
				{
					
					
					$this->session->set_flashdata('error_msg','Invalid login.Please try again.');
					redirect('admin/login');
				
				}
				
			}
		}
		
	$this->load->view('vendor/maincontents/admin-login',$this->data);
	}
	
function validate_form_data()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">', '</div>');
		$this->form_validation->set_rules('username','UserName','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run()==true)
		{
			return true;
		}else
		{
			return false;
		}
	}
}

?>
