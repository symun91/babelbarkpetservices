<?php

/**
 * @property  appointmentmodel          $appointmentmodel
 * @property  petbreedmodel             $petbreedmodel
 * @property  servicemodel              $servicemodel
 * @property  vendorsmodel              $vendorsmodel
 * @property  vendorcategoriesmodel     $vendorcategoriesmodel
 * @property  customermodel             $customermodel
 * @property  petmodel                  $petmodel
 * @property  appusersmodel             $appusersmodel
 * @property  appuservendorsmodel       $appuservendorsmodel
 * @property  promotionsmodel           $promotionsmodel
 * @property  sikkaofficemodel          $sikkaofficemodel
 * @property  admin_model               $admin_model
 */

class Home extends CI_Controller
{
    var $data=array();
    var $file_path;
    //var $customers;
    //var	$appuser;
    function __construct()
    {

        parent::__construct();
        $this->load->library('super_admin_init_elements');
        $this->super_admin_init_elements->init_elements();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('appointmentmodel');
        $this->load->model('petbreedmodel');
        $this->load->model('servicemodel');
        $this->load->model('vendorsmodel');
        $this->load->model('vendorcategoriesmodel');
        $this->load->model('customermodel');
        $this->load->model('petmodel');
        $this->load->model('appusersmodel');
        $this->load->model('appuservendorsmodel');
        $this->load->model('customermodel');
        $this->load->model('promotionsmodel');
        $this->load->model('sikkaofficemodel');
        $this->load->model('admin_model');
    }

    function search()
    {
        $this->load->model('vendorsmodel');
        $keyword = $this->input->post('search');

        if($this->session->userdata('isadmin'))
        {
            $data=array();
            $vendors =$this->vendorsmodel->getVendorSearch($keyword);
            for($i=0;$i<count($vendors);$i++)
            {
                $categories=$vendors[$i]['category'];
                $categoryarr=explode(",", $categories);
                $categorynamestr="";
                for($j=0;$j<count($categoryarr);$j++)
                {
                    $categoryid=$categoryarr[$j];
                    $categoryname=$this->vendorcategoriesmodel->getCategoryName($categoryid);
                    if($categorynamestr=="")
                        $categorynamestr=$categoryname;
                    else
                        $categorynamestr=$categorynamestr.",".$categoryname;
                }
                $vendors[$i]['category']=$categorynamestr;
                $vendors[$i]['noofappointments'] =$this->appointmentmodel->getAppointmentsCount($vendors[$i]['vendorid']);
            }
            //$data['vendors'] =$vendors;

            if(isset($keyword) && !empty($keyword))
            {
                $data['vendors'] = $vendors;
                $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-home',$data,true);
                $this->load->view('vendor/layout',$this->data);
            }
            else
            {
                redirect('admin/home/index');
            }
        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }

    function index($perpage=5,$selsort='createdon')
    {
        if($this->session->userdata('isadmin'))
        {
            $data=array();
            $vendors =$this->vendorsmodel->getAllVendors1();
            for($i=0;$i<count($vendors);$i++)
            {
                $categories=$vendors[$i]['category'];
                $categoryarr=explode(",", $categories);
                $categorynamestr="";
                for($j=0;$j<count($categoryarr);$j++)
                {
                    $categoryid=$categoryarr[$j];
                    $categoryname=$this->vendorcategoriesmodel->getCategoryName($categoryid);
                    if($categorynamestr=="")
                        $categorynamestr=$categoryname;
                    else
                        $categorynamestr=$categorynamestr.",".$categoryname;
                }
                $vendors[$i]['category']=$categorynamestr;
                $vendors[$i]['noofappointments'] =$this->appointmentmodel->getAppointmentsCount($vendors[$i]['vendorid']);
            }
            $data['vendors'] =$vendors;


            $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-home',$data,true);
            $this->load->view('vendor/layout',$this->data);
        }else{
            redirect('vendor/administrator/logout');
        }

    }

    function listofgenericcustomers($customerType = 'all')
    {
        $config['base_url'] = base_url().'admin/home/listofgenericcustomers/';
        $config['total_rows'] = $this->customermodel->getAllCustomersCount();
        $config['per_page'] = 25;
        $config['uri_segment']= 4;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $this->pagination->initialize($config);

        $customers = array();
        $page = $this->uri->segment(4);
        if($page < 1)
            $page = 0;
        $customers=$this->customermodel->getAllCustomersForAdmin1($config['per_page'],$page);
        for($i=0;$i<count($customers);$i++)
        {
            $customerid=$customers[$i]['customerid'];
            $customerpets=$this->customermodel->getPets($customerid);

            if($customerpets>1)
            {
                $customerpetids=$this->customermodel->getPetIDs($customerid);
                $customerpetname="";
                $customerpetBD="";
                $count=0;
                for($j=0;$j<$customerpets;$j++)
                {
                    $petdetail=$this->petmodel->getPetDetails($customerpetids[$j]['petid']);
                    $count++;
                    $customerpetname=$customerpetname."".$count." ".$petdetail['name']."<br/>";
                    $customerpetBD=$customerpetBD."".$count." ".$petdetail['birthdate']."<br/>";
                }
                $customers[$i]['petname']=$customerpetname;
                $customers[$i]['petDB']=$customerpetBD;
                $customers[$i]['usertype'] = 'Generic Customer';
            }
            else
            {
                $customerpetids=$this->customermodel->getPetIDs($customerid);
                $petdetail=$this->petmodel->getPetDetails($customerpetids[0]['petid']);
                $customerpetname=$petdetail['name'];
                $customerpetBD=$petdetail['birthdate'];
                $customers[$i]['petname']=$customerpetname;
                $customers[$i]['petDB']=$customerpetBD;
                $customers[$i]['usertype'] = 'Generic Customer';
            }
            /*
            $customerpetid=$this->customermodel->getPetID($customerid);
            $petdetail=$this->petmodel->getPetDetails($customerpetid);
            $customerpetname=$petdetail['name'];
            $customerpetBD=$petdetail['birthdate'];
            $customers[$i]['petname']=$customerpetname;
            $customers[$i]['petDB']=$customerpetBD;
            $customers[$i]['usertype'] = 'Generic Customer';*/
        }

        $data = array();
        $data['customers'] = $customers;
        $str_links = $this->pagination->create_links();
        $data['links'] = explode('&nbsp;',$str_links);
        $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-genericcustomers',$data,true);
        $this->load->view('vendor/layout',$this->data);
    }


    function listofappusers()
    {
        $config['base_url'] = base_url().'admin/home/listofappusers/';
        $config['total_rows'] = $this->appusersmodel->getAllAppusersCount();
        $config['per_page'] = 10;
        $config['uri_segment']= 4;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $this->pagination->initialize($config);

        $appusers = array();

        $page = $this->uri->segment(4);
        if($page < 1)
            $page = 0;
        $babelbarkusers=$this->appusersmodel->getAllUsersForAdmin1($config['per_page'],$page);
        if(count($babelbarkusers)>0)
        {
            for($i=0;$i<count($babelbarkusers);$i++)
            {
                $appuserid=$babelbarkusers[$i]['appuserid'];
                $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);
                $appuserpets=$this->appusersmodel->getPets($appuserid);
                $petname="";
                $petDB="";
                $count=0;
                if($appuserpets>1)
                {
                    $appuserpetid=$this->appusersmodel->getPetIDs($appuserid);
                    for($j=0;$j<$appuserpets;$j++)
                    {
                        $petdetail=$this->petmodel->getPetDetails($appuserpetid[$j]['petid']);
                        $count++;
                        $petname = $petname."".$count." ".$petdetail['name']."<br/>";
                        $petDB = $petDB."".$count." ".$petdetail['birthdate']."<br/>";
                    }
                    $appuser['usertype'] = 'BabelBark Customer';
                    $appuser['petname'] = $petname;
                    $appuser['petDB']=$petDB;
                    $appusers[$i] = $appuser;
                }
                else
                {
                    $appuserpetid=$this->appusersmodel->getPetID($appuserid);
                    $petdetail=$this->petmodel->getPetDetails($appuserpetid);
                    $petname=$petdetail['name'];
                    $petDB=$petdetail['birthdate'];
                    $appuser['usertype'] = 'BabelBark Customer';
                    $appuser['petname'] = $petname;
                    $appuser['petDB']=$petDB;
                    $appusers[$i] = $appuser;
                }
                /*$appuserpetid=$this->appusersmodel->getPetID($appuserid);
                $petdetail=$this->petmodel->getPetDetails($appuserpetid);
                $appuser['usertype'] = 'BabelBark Customer';
                $appuser['petname'] = $petdetail['name'];
                $appuser['petDB']=$petdetail['birthdate'];
                $appusers[$i] = $appuser;*/
            }
        }

        $data = array();
        $data['customers'] = $appusers;
        $str_links = $this->pagination->create_links();
        $data['links'] = explode('&nbsp;',$str_links);
        $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-appusers',$data,true);
        $this->load->view('vendor/layout',$this->data);
    }

    function customers($vendorid)
    {

        if($this->session->userdata('isadmin'))
        {
            $customers = array();
            $appusers = array();
            $customerType='all'; //changed as per requirement
            if($customerType == 'all' || $customerType == 'generic'){

                $customers=$this->customermodel->getAllCustomers($vendorid);

                for($i=0;$i<count($customers);$i++)
                {
                    $customerid=$customers[$i]['customerid'];
                    $customerpetid=$this->customermodel->getPetID($customerid);
                    $petdetail=$this->petmodel->getPetDetails($customerpetid);
                    $customerpetname=$petdetail['name'];
                    $customers[$i]['petname']=$customerpetname;
                    $customers[$i]['usertype'] = 'Generic Customer';
                }
            }

            if($customerType == 'all' || $customerType == 'babelbark'){
                $babelbarkusers=$this->appuservendorsmodel->getUsersForVendor($vendorid);

                if(count($babelbarkusers)>0)
                {
                    for($i=0;$i<count($babelbarkusers);$i++)
                    {
                        $appuserid=$babelbarkusers[$i]['appuserid'];
                        $appuser = $this->appusersmodel->fetchUserDetails1($appuserid);

                        $appuserpetid=$this->appusersmodel->getPetID($appuserid);
                        $petdetail=$this->petmodel->getPetDetails($appuserpetid);

                        $appuser['usertype'] = 'BabelBark Customer';
                        $appuser['petname'] = $petdetail['name'];
                        $appusers[$i] = $appuser;
                    }
                }
            }


            $data = array();

            $commonArray = array_merge($customers,$appusers);
            function cmp($customers, $appusers) {

                return $customers['updatedon'] - $appusers['updatedon'];
            }
            usort($commonArray,  'cmp');


            $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);
            //echo '<pre>'; print_r($commonArray); die;
            $data['usertype'] = $this->uri->segment(4);
            $data['customers'] = $commonArray;
            $data['vendorid']=$vendorid;
            $data['vendorname']=$vendordetails['firstname']." ".$vendordetails['lastname'];

            $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-customers',$data,true);
            $this->load->view('vendor/layout',$this->data);
        }else{
            redirect('vendor/administrator/logout');
        }

    }

    function services($vendorid)
    {

        /* pagignation control */
        $config['base_url'] = base_url().'admin/home/services/'.$vendorid;
        $config['total_rows'] = $this->servicemodel->getServicesCount($vendorid);
        $config['per_page'] = PERPAGE;
        $config['uri_segment'] =5;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);
        /* pagination section ends here */

        $page = $this->uri->segment(5);
        if($page < 1)
            $page = 0;
        $services=$this->servicemodel->getAllServices($config['per_page'], $page,$vendorid);

        $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);

        $data = array();
        $data['services'] = $services;
        $data['pagination'] =  $this->pagination->create_links();
        $data['vendorid']=$vendorid;
        $data['vendorname']=$vendordetails['firstname']." ".$vendordetails['lastname'];
        $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-services',$data,true);
        $this->load->view('vendor/layout',$this->data);


    }

    function appointments($vendorid)
    {


        /* pagignation control */
        /*$config['base_url'] = base_url().'admin/home/appointments/';
        $config['total_rows'] = $this->appointmentmodel->getAppointmentsCount($vendorid);
        $config['per_page'] = PERPAGE;
            $config['uri_segment'] =5;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);
        /* pagination section ends here */

        /*$page = $this->uri->segment(5);
        if($page < 1)
            $page = 0;*/
        $appointments=$this->appointmentmodel->getAllappointmentsofvendor($vendorid);
        //print_r(count($appointments));
        $data = array();

        for($i=0;$i<count($appointments);$i++)
        {
            $breedname="";
            $breedid=$appointments[$i]['petbreed'];
            if($breedid!=null && $breedid!="")
            {
                $breedname=$this->petbreedmodel->getBreedTitle($breedid);
            }
            $appointments[$i]['breedname']=$breedname;
        }
        $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);

        $data['appointments'] = $appointments;
        $data['pagination'] =  $this->pagination->create_links();
        $data['vendorid']=$vendorid;
        $data['vendorname']=$vendordetails['firstname']." ".$vendordetails['lastname'];
        $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-appointments',$data,true);
        $this->load->view('vendor/layout',$this->data);


    }

    function downloadVendors()
    {
        $vendors = array();
        $vendors = $this->vendorsmodel->getAllVendorsForReport();

        for($i=0;$i<count($vendors);$i++)
        {
            $vendorid=$vendors[$i]['VendorID'];
            $countCustomers = $this->customermodel->getCustomersCount($vendorid);
            $countBableBarkCustomers = $this->appuservendorsmodel->getUsersForVendorCount($vendorid);
            //$vendors[$i]['NumberOfCustomers']  = $this->customermodel->getCustomersCount($vendorid);
            $vendors[$i]['NumberOfCustomers'] = $countCustomers+$countBableBarkCustomers;
            $vendors[$i]['NumberOfAppointments'] = $this->appointmentmodel->getAppointmentsCount($vendorid);
            $vendors[$i]['NumberOfServices'] = $this->servicemodel->getServicesCount($vendorid);
            $vendors[$i]['NumberOfPromotions'] = $this->promotionsmodel->getPromotionsCount($vendorid);

            $categories=$vendors[$i]['Category'];
            $categoryarr=explode(",", $categories);
            $categorynamestr="";
            for($j=0;$j<count($categoryarr);$j++)
            {
                $categoryid=$categoryarr[$j];
                $categoryname=$this->vendorcategoriesmodel->getCategoryName($categoryid);
                if($categorynamestr=="")
                    $categorynamestr=$categoryname;
                else
                    $categorynamestr=$categorynamestr.",".$categoryname;
            }
            $vendors[$i]['Category']=$categorynamestr;
        }

        $fileName = 'BizBarkVendors-'.date('d-m-Y').'.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        foreach ( $vendors as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;

    }

    function downloadGenericcustomers()
    {
        $customers = array();
        $customers=$this->customermodel->getAllCustomersForAdmin();
        for($i=0;$i<count($customers);$i++)
        {
            $customerid=$customers[$i]['customerid'];
            $customerpets=$this->customermodel->getPets($customerid);

            if($customerpets>1)
            {
                $customerpetids=$this->customermodel->getPetIDs($customerid);
                $customerpetname="";
                $customerpetBD="";
                $count=0;
                for($j=0;$j<$customerpets;$j++)
                {
                    $petdetail=$this->petmodel->getPetDetails($customerpetids[$j]['petid']);
                    $count++;
                    $customerpetname=$customerpetname."".$count."-".$petdetail['name']."  ";
                    $customerpetBD=$customerpetBD."".$count."-".$petdetail['birthdate']."  ";
                }
                $customers[$i]['petname']=$customerpetname;
                $customers[$i]['petDB']=$customerpetBD;
                $customers[$i]['usertype'] = 'Generic Customer';
            }
            else
            {
                $customerpetids=$this->customermodel->getPetIDs($customerid);
                $petdetail=$this->petmodel->getPetDetails($customerpetids[0]['petid']);
                $customerpetname=$petdetail['name'];
                $customerpetBD=$petdetail['birthdate'];
                $customers[$i]['petname']=$customerpetname;
                $customers[$i]['petDB']=$customerpetBD;
                $customers[$i]['usertype'] = 'Generic Customer';
            }
            /*$customerpetid=$this->customermodel->getPetID($customerid);
            $petdetail=$this->petmodel->getPetDetails($customerpetid);
            $customerpetname=$petdetail['name'];
            $customerpetBD=$petdetail['birthdate'];
            $customers[$i]['petname']=$customerpetname;
            $customers[$i]['petDB']=$customerpetBD;
            $customers[$i]['usertype'] = 'Generic Customer';*/
        }
        $fileName = 'BizBarkGenericCustomers-'.date('d-m-Y').'.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        foreach ( $customers as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;
    }

    function downloadBabelBarkcustomers()
    {
        $appusers = array();
        $babelbarkusers=$this->appusersmodel->getAllUsersForAdmin();
        if(count($babelbarkusers)>0)
        {
            for($i=0;$i<count($babelbarkusers);$i++)
            {
                $appuserid=$babelbarkusers[$i]['appuserid'];
                $appuser1 = $this->appusersmodel->fetchUserDetails2($appuserid);
                $appuserpets=$this->appusersmodel->getPets($appuserid);
                $petname="";
                $petDB="";
                $count=0;
                if($appuserpets>1)
                {
                    $appuserpetid=$this->appusersmodel->getPetIDs($appuserid);
                    for($j=0;$j<$appuserpets;$j++)
                    {
                        $petdetail=$this->petmodel->getPetDetails($appuserpetid[$j]['petid']);
                        $count++;
                        $petname = $petname."".$count."-".$petdetail['name']."\n";
                        $petDB = $petDB."".$count."-".$petdetail['birthdate']."\n";
                    }
                }
                else
                {
                    $appuserpetid=$this->appusersmodel->getPetID($appuserid);
                    $petdetail=$this->petmodel->getPetDetails($appuserpetid);
                    $petname=$petdetail['name'];
                    $petDB=$petdetail['birthdate'];
                }
                $appuser['ID']=$appuserid;
                $appuser['FirstName']=$appuser1['firstname'];
                $appuser['LastName']=$appuser1['lastname'];
                $appuser['Email']=$appuser1['email'];
                $appuser['Contact']=$appuser1['phonenumber'];
                $appuser['Address']=$appuser1['address'];
                $appuser['City']=$appuser1['city'];
                $appuser['State']=$appuser1['state'];
                $appuser['Country']=$appuser1['country'];
                $appuser['Zipcode']=$appuser1['zip'];
                /*$appuser['UpdatedOn']=$appuser1['updatedon'];
                $appuser['Notes']="";
                $appuser['PetID']=$appuserpetid;*/
                $appuser['PetName'] = $petname;
                $appuser['DogBirthDay']=$petDB;
                $appuser['Type'] = 'BabelBark User';
                $appusers[$i] = $appuser;
            }
        }
        $fileName = 'BabelBarkCustomers-'.date('d-m-Y').'.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        foreach ( $appusers as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;
    }

    function downloadCustomers($vendorid)
    {
        $customers = array();
        $appusers = array();
        $customers_new=array();


        $customerType='all'; //changed as per requirement
        if($customerType == 'all' || $customerType == 'generic'){

            $customers=$this->customermodel->getAllCustomers($vendorid);

            for($i=0;$i<count($customers);$i++)
            {
                $customerid=$customers[$i]['customerid'];
                $customerpets=$this->customermodel->getPets($customerid);
                if($customerpets>1)
                {
                    $customerpetids=$this->customermodel->getPetIDs($customerid);
                    $customerpetname="";
                    $customerpetBD="";
                    $count=0;
                    for($j=0;$j<$customerpets;$j++)
                    {
                        $petdetail=$this->petmodel->getPetDetails($customerpetids[$j]['petid']);
                        $count++;
                        $customerpetname=$customerpetname."".$count."->".$petdetail['name']."  ";
                        $customerpetBD=$customerpetBD."".$count."->".$petdetail['birthdate']."  ";
                    }
                }
                else
                {
                    $customerpetids=$this->customermodel->getPetIDs($customerid);
                    $petdetail=$this->petmodel->getPetDetails($customerpetids[0]['petid']);
                    $customerpetname=$petdetail['name'];
                    $customerpetBD=$petdetail['birthdate'];
                }
                /*$customerpetid=$this->customermodel->getPetID($customerid);
                $petdetail=$this->petmodel->getPetDetails($customerpetid);
                $customerpetname=$petdetail['name'];*/


                $customers_new[$i]['ID']=$customerid;
                $customers_new[$i]['FirstName']=$customers[$i]['firstname'];
                $customers_new[$i]['LastName']=$customers[$i]['lastname'];
                $customers_new[$i]['Email']=$customers[$i]['email'];
                $customers_new[$i]['Contact']=$customers[$i]['phoneno'];
                $customers_new[$i]['Address']=$customers[$i]['address'];
                $customers_new[$i]['City']=$customers[$i]['city'];
                $customers_new[$i]['State']=$customers[$i]['state'];
                $customers_new[$i]['Country']=$customers[$i]['country'];
                $customers_new[$i]['Zipcode']=$customers[$i]['zipcode'];
                $customers_new[$i]['UpdatedOn']=$customers[$i]['updatedon'];
                $customers_new[$i]['Notes']=$customers[$i]['notes'];
                $customers_new[$i]['PetID']=$customerpetid;
                $customers_new[$i]['PetName']=$customerpetname;
                $customers_new[$i]['Type']="Generic Customer";


            }
        }

        if($customerType == 'all' || $customerType == 'babelbark'){
            $babelbarkusers=$this->appuservendorsmodel->getUsersForVendor($vendorid);

            if(count($babelbarkusers)>0)
            {
                for($i=0;$i<count($babelbarkusers);$i++)
                {
                    $appuserid=$babelbarkusers[$i]['appuserid'];
                    $appuser1 = $this->appusersmodel->fetchUserDetails2($appuserid);
                    $appuserpets=$this->appusersmodel->getPets($appuserid);
                    $petname="";
                    $petDB="";
                    $count=0;
                    if($appuserpets>1)
                    {
                        $appuserpetid=$this->appusersmodel->getPetIDs($appuserid);
                        for($j=0;$j<$appuserpets;$j++)
                        {
                            $petdetail=$this->petmodel->getPetDetails($appuserpetid[$j]['petid']);
                            $count++;
                            $petname = $petname."".$count."->".$petdetail['name']."  ";
                            $petDB = $petDB."".$count."->".$petdetail['birthdate']."  ";
                        }
                    }
                    else
                    {
                        $appuserpetid=$this->appusersmodel->getPetIDs($appuserid);
                        $petdetail=$this->petmodel->getPetDetails($appuserpetid);
                        $petname=$petdetail['name'];
                        $petDB=$petdetail['birthdate'];
                    }

                    /*$appuserpetid=$this->appusersmodel->getPetID($appuserid);
                    $petdetail=$this->petmodel->getPetDetails($appuserpetid);*/



                    $appuser['ID']=$appuserid;
                    $appuser['FirstName']=$appuser1['firstname'];
                    $appuser['LastName']=$appuser1['lastname'];
                    $appuser['Email']=$appuser1['email'];
                    $appuser['Contact']=$appuser1['phonenumber'];
                    $appuser['Address']=$appuser1['address'];
                    $appuser['City']=$appuser1['city'];
                    $appuser['State']=$appuser1['state'];
                    $appuser['Country']=$appuser1['country'];
                    $appuser['Zipcode']=$appuser1['zip'];
                    /*$appuser['UpdatedOn']=$appuser1['updatedon'];
                    $appuser['Notes']="";
                    $appuser['PetID']=$appuserpetid;*/
                    $appuser['PetName'] = $petname;
                    $appuser['DogBirthDay']=$petDB;
                    $appuser['Type'] = 'BabelBark User';



                    $appusers[$i] = $appuser;
                }
            }
        }



        $commonArray = array_merge($customers_new,$appusers);

        function cmp($customers_new, $appusers) {

            return $customers_new['UpdatedOn'] - $appusers['UpdatedOn'];
        }
        usort($commonArray,  'cmp');

        $fileName = 'BizBarkCustomers-'.$vendorid."-".date('d-m-Y').'.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        foreach ( $commonArray as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;

    }

    function downloadServices($vendorid)
    {

        $servies_final_arr=array();
        $fileName = 'BizBarkServices-'.$vendorid."-".date('d-m-Y').'.csv';

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        $services = $this->servicemodel->getAllServices1($vendorid);

        for($i=0;$i<count($services);$i++)
        {
            $availabilitydays= $services[$i]['availabilitydays'];
            $avafromhour=$services[$i]['availabilityfromhour'];
            $avatohour=$services[$i]['availabilitytohour'];
            $checkeddays_arr=array();
            $avafromhour_arr=array();
            $avatohour_arr=array();
            $availdays_dispstr="";
            if($availabilitydays!="" && $availabilitydays!="0")
            {
                $checkeddays_arr=explode(",", $availabilitydays);
                $avafromhour_arr=explode(",", $avafromhour);
                $avatohour_arr=explode(",", $avatohour);
                for($j=0;$j<count($checkeddays_arr);$j++)
                {
                    //$availdays_dispstr=$availdays_dispstr."".$checkeddays_arr[$j].": ".$avafromhour_arr[$j]."-".$avatohour_arr[$j];
                    $availdays_dispstr=$availdays_dispstr."<br>".$checkeddays_arr[$i];
                    if(count($avafromhour_arr)>$i)
                        $availdays_dispstr=$availdays_dispstr.": ".$avafromhour_arr[$i]."-".$avatohour_arr[$i];
                }
            }

            if($services[$i]['reservation']==1)
                $dispres='Yes';
            else
                $dispres='No';

            $disprestrictions="";
            if($services[$i]['restriction']==1){
                if($services[$i]['restrictfromage']!="" && $services[$i]['restricttoage']!="")
                    $agerestrinctions=$services[$i]['restrictfromage']."-".$services[$i]['restricttoage'];
                else
                    $agerestrinctions="None";
                if($services[$i]['restrictfromwt']!="" && $services[$i]['restricttowt']!="")
                    $wtrestrinctions=$services[$i]['restrictfromwt']."-".$services[$i]['restricttowt'];
                else
                    $wtrestrinctions="None";

                $disprestrictions="Age Restrictions: ". $agerestrinctions."Weight Restrictions: ".$wtrestrinctions." Medical Conditions Allowed: ". $services[$i]['medicalcondnotallowed']."<br/>";
            }

            $servies_final_arr[$i]['ID']=$services[$i]['serviceid'];
            $servies_final_arr[$i]['Name']=$services[$i]['name'];
            $servies_final_arr[$i]['Type']=$services[$i]['type'];
            $servies_final_arr[$i]['Price']=$services[$i]['price']."$ (".$services[$i]['priceby'].")";
            $servies_final_arr[$i]['Duration']=$services[$i]['duration']." ".$services[$i]['durationunit'];
            $servies_final_arr[$i]['AvailabileDays']=$availdays_dispstr;
            $servies_final_arr[$i]['Reservation']=$dispres;
            $servies_final_arr[$i]['Restriction']=$disprestrictions;
            $servies_final_arr[$i]['Notes']=$services[$i]['notes'];
            $servies_final_arr[$i]['UpdatedOn']=$services[$i]['updatedon'];
        }

        foreach ( $servies_final_arr as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;


    }

    function downloadAppointments($vendorid)
    {

        $appointments_final_arr=array();
        $fileName = 'BizBarkAppointments-'.$vendorid."-".date('d-m-Y').'.csv';

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );

        $headerDisplayed = false;

        $appointments = $this->appointmentmodel->getAllappointments1($vendorid);

        for($i=0;$i<count($appointments);$i++)
        {
            $breedname="";
            $breedid=$appointments[$i]['petbreed'];
            if($breedid!=null && $breedid!="")
            {
                $breedname=$this->petbreedmodel->getBreedTitle($breedid);
            }
            $appointments[$i]['breedname']=$breedname;

            $appointments_final_arr[$i]['ID']=$appointments[$i]['appointmentid'];
            $appointments_final_arr[$i]['Service Name']=$appointments[$i]['servicename'];
            $appointments_final_arr[$i]['Customer Name']=$appointments[$i]['firstname']." ".$appointments[$i]['lastname'];
            $appointments_final_arr[$i]['Pet Name']=$appointments[$i]['petname'];
            $appointments_final_arr[$i]['Pet Breed']=$appointments[$i]['breedname'];
            $appointments_final_arr[$i]['bookingnote']=$appointments[$i]['bookingnote'];
            $appointments_final_arr[$i]['estimatedduration']=$appointments[$i]['estimatedduration'];
            $appointments_final_arr[$i]['UpdatedOn']=$appointments[$i]['updatedon'];
        }

        foreach ( $appointments_final_arr as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;

    }

    function delete($vendorid)
    {
        $this->vendorsmodel->deleteVendor($vendorid);

        $msg="Vendor deleted successfully";
        $this->session->set_flashdata('success_message',$msg);
        redirect('admin/home/index');
    }

    function navigateToSikka()
    {
        if($this->session->userdata('isadmin'))
        {
            $data = array();
            $this->fetchsikkaOffices();

            $result = $this->sikkaofficemodel->getsikkaoffices();



            $data['offices'] = $result;
            $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-sikka',$data,true);
            $this->load->view('vendor/layout',$this->data);
        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }

    function fetchsikkaOffices()
    {
        if($this->session->userdata('isadmin'))
        {
            $sikkaauthorizedofficesurl = "https://api.sikkasoft.com/auth/v2/authorized_practices?app_id=".APP_ID."&app_key=".APP_KEY;
            $detailjson = @file_get_contents($sikkaauthorizedofficesurl);
            if($detailjson === FALSE)
            {
                return;
            }
            $detail = json_decode($detailjson);



            for($j=0;$j<count($detail[0]->items);$j++)
            {
                $datacc =array(
                    'sikkaofficeic'=>$detail[0]->items[$j]->office_id,
                    'sikkaofficename' => $detail[0]->items[$j]->practice_name,
                    'address' => $detail[0]->items[$j]->address,
                    'city' => $detail[0]->items[$j]->city,
                    'state' => $detail[0]->items[$j]->state,
                    'zip' => $detail[0]->items[$j]->zip,
                    'secret_key' =>$detail[0]->items[$j]->secret_key
                );

                $this->sikkaofficemodel->updateSikkaOffices($datacc);
            }

        }
        else
        {
            redirect('vendor/administrator/logout');
        }
    }
    private function validate_form_data()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger form-error-msg">','</div>');
        $this->form_validation->set_rules('old_password','Old Password','trim|required|callback_is_correct_password');
        $this->form_validation->set_rules('new_password','New Password','trim|required|min_length[6]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');
        if($this->form_validation->run()==true)
        {
            return true;
        }else
        {
            return false;
        }
    }
    function adminChangePassword(){
        $this->super_admin_init_elements->init_elements();
        $data=array();
        if($this->input->post('submit'))
        {
            if($this->validate_form_data()==true)
            {
                $admin=$this->session->userdata('admin');
                $updateData=array('password'=>$this->input->post('new_password'),'updatedon'=>date('Y-m-d H:i:s'));
                $update=$this->admin_model->save_data($updateData,$admin['adminDetails']['adminid'],'adminid','admin');

                if($update>0)
                {
                    $this->session->unset_userdata('admin');
                    $adminData=$this->admin_model->find_data('row',array('adminid'=>$admin['adminDetails']['adminid']),'admin',0,0,'adminid');
                    $admin=array('isloggedin'=>1,'adminDetails'=>$adminData,'role'=>'superadmin');
                    $this->session->set_userdata('admin',$admin);
                    $this->session->set_userdata('isadmin',TRUE);
                    $this->session->set_flashdata('success_message','Your password is changed successfully.');
                    redirect('admin/home/adminChangePassword');
                }else
                {
                    $this->session->set_flashdata('error_message','Some error occurred while changing your password.Please try again.');
                    redirect('admin/home/adminChangePassword');
                }
            }
        }

        $this->data['maincontent']=$this->load->view('vendor/maincontents/admin-change-password',$data,true);
        $this->load->view('vendor/layout',$this->data);
    }
}
?>