<?php //cron will run every day
/**
 * @property petmodel $petmodel
 */
class Petbirthday_cron extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('petmodel');
	}
	
	function updateApproximateAge()
	{
		$pets=$this->petmodel->getPetswithUnsureBirthday();
		for($i=0;$i<count($pets);$i++)
		{
			$birthdayupdatedon=$pets[$i]['birthdateupdatedon'];
			$currentdate=date("Y-m-d");
			$d1 = new DateTime($birthdayupdatedon);
			$d2 = new DateTime($currentdate);
			
			$diff = $d2->diff($d1);
			
			$diffinyears= $diff->y;
			if ($diffinyears==1)
			{
				//update the approximate age
				$approxage=$pets[$i]['approximateage'];
			 				  $approxageyears="";
				        	  $approxagemonths="";
				         if($approxage!="" && $approxage!="0")
				         {
				         	$approxtemp=explode("years", $approxage);
				         	$approxageyears=$approxtemp[0];
				         	
				         	if($approxtemp[1]!="")
				         	{
				         		$approxtemp1=explode("months", $approxtemp[1]);
				         		$approxagemonths=$approxtemp1[0];
				         	}
				         	
				         	if($approxageyears!="")
				         		$approxageyears=$approxageyears+1;
				         	else 
				         		$approxageyears=1;
				         		
				         	$newapproxage="";
				         if($approxageyears!="")
							$newapproxage=$approxageyears."years";
						if($approxagemonths!="")
							$newapproxage=$newapproxage.$approxagemonths."months";
				         		
				         	$this->petmodel->increaseApproxAge($pets[$i]['petid'],$newapproxage);
				         }
				         
				         
				
			}
		}
		echo "success";
		
	}
	
}


?>