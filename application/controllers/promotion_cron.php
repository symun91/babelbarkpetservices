<?php

/**
 * @property promotionsmodel $promotionsmodel
 */
class Promotion_cron extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('promotionsmodel');
    }

    function updateAllExpirePromotions(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if ($user_agent == 'Bizbark cURL Request') {
            $this->promotionsmodel->updateAllExpirePromotions();
            echo True;
        }else{
            echo false;
        }
    }
}
