<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activateappuseraccount extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('appusersmodel'));
		
	}

	function index()
	{
		$data['msg'] = '';
		
		if(!empty($_GET)){
			
			
			$token = $this->input->get('token');
			$userdetails=$this->appusersmodel->getUserByActivateToken($token);
		    $appuserid=$userdetails['appuserid'];
		  
		       
		      $datacc = array(
		               'isverified' => 1
	    			
		      );
			 $this->appusersmodel->updateUser($datacc,$appuserid);
			
		    $this->load->view('activateappuseraccount.php',$data);
		}

		
	}
}

