<?php
/**
 * @property petmodel                    $petmodel
 * @property reportmodel                 $reportmodel
 * @property appusersmodel               $appusersmodel
 * @property vendorsmodel                $vendorsmodel
 * @property customermodel               $customermodel
 * @property petvaccinationmodel         $petvaccinationmodel
 * @property petprescriptionmodel        $petprescriptionmodel
 * @property petmedicationmodel          $petmedicationmodel
 */

class Reports extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('common_helper', 'url'));
        $this->load->library('MyPdf');
        $this->load->model('appusersmodel');
        $this->load->model('petmodel');
        $this->load->model('reportmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('customermodel');
        $this->load->model('petvaccinationmodel');
        $this->load->model('petprescriptionmodel');
        $this->load->model('petmedicationmodel');

    }
    public function checkForUpdateInfo(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if ($user_agent == 'Bizbark cURL Request') {
            $sikkaAppUserList = $this->appusersmodel->sikkaAppUserLIst();
            $currentDate = date('M d, Y');
            foreach ($sikkaAppUserList as $sikkaRows) {
                $vendorId = $sikkaRows['vendorid'];
                $appUserId = $sikkaRows['appuserid'];
                $petId = $sikkaRows['petid'];
                $patient_id = $sikkaRows['patient_id'];
                $isReportCreated = $this->reportmodel->checkReportCreated($vendorId, $appUserId, $petId);
                if ($isReportCreated > 0) {
                    $checkForMedicationInfo = $this->petmedicationmodel->getLatestMedInfo($petId);
                    $checkForSikkaMedicationInfo = $this->petprescriptionmodel->getLatestSikkaMedInfo($patient_id);
                    $checkForVacInfo = $this->petvaccinationmodel->getLatestVacInfo($petId);
                    $totalLatestInfo = $checkForMedicationInfo + $checkForVacInfo +$checkForSikkaMedicationInfo;
                    if ($totalLatestInfo > 0) {
                        $this->sikkaVaccinationReport($vendorId, $appUserId,$petId,$currentDate,$patient_id);
                    }
                } else {
                    $this->sikkaVaccinationReport($vendorId, $appUserId,$petId,$currentDate,$patient_id);
                }
            }
            echo true;
        }else{
            echo false;
        }
    }
    public function sikkaVaccinationReport($vendorId,$userId,$petId,$currentDate,$patient_id = NULL){
        $vendorInfo = $this->vendorsmodel->getVendorDetails($vendorId);
        $clientInfo = $this->appusersmodel->fetchUserDetails2($userId);
        $data['clientInfo'] = $clientInfo;
        $petInfo = $this->petmodel->getPetInfo($petId);
        if(empty($vendorInfo)){
            return true;
        }elseif(empty($clientInfo)){
            return true;
        }elseif(empty($petInfo)){
            return true;
        }else{
            $data['vendorInfo'] = $vendorInfo;
            if ($vendorInfo['logoimage'] == '') {
                $data['vet_logo'] = "assets/images/noImageAvailable.jpg";
            } else {
                $img_path = awsBucketPath . $vendorInfo['logoimage'];
                $file_headers = @get_headers($img_path);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $data['vet_logo'] = "assets/images/noImageAvailable.jpg";
                } else {
                    $data['vet_logo'] = awsBucketPath . $vendorInfo['logoimage'];
                }
            }
            $data['company'] = $vendorInfo['comapnyname'];
            $data['patientInfo'] = $this->petmodel->getPetInfo($petId);
            $data['vaccinationInfo'] = $this->petvaccinationmodel->getPetVaccineDetails($petId);
            $data['medicationInfo'] = $this->petmedicationmodel->getPetMedicationForPet($petId);
            $data['sikkaMedicationInfo'] = $this->petprescriptionmodel->getSikkaMedicationInfo($patient_id);
            $data['generateDate'] = $currentDate;
            $fileName = time() . '_' . $petId . '.pdf';
            $fileTmpPath = "resources/downloads/temp/$fileName";
            $data['fileTmpPath'] = $fileTmpPath;
            $this->load->view('vendor/maincontents/sikkavaccinationreport', $data);
            $uploaddir = 'resources/uploads/vaccinationreport/';
            $s3msg = aws_upload($uploaddir, $fileName, $fileTmpPath);
            if ($s3msg == 'S3 upload successful') {
                $reportInfo = array(
                    'vendorid' => $vendorId,
                    'appuserid' => $userId,
                    'petid' => $petId,
                    'filename' => $fileName,
                    'fileurl' => $uploaddir . $fileName,
                    'generatedate' => date('Y-m-d')
                );
                $this->reportmodel->deleteReportLog($vendorId, $userId, $petId);
                $this->reportmodel->addReport($reportInfo);
            }
        }
    }
    function deleteFile()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if ($user_agent == 'Bizbark cURL Request') {
            $dir = $_SERVER["DOCUMENT_ROOT"] . '/resources/downloads/temp';
            $files = scandir($dir);
            for ($i = 0; $i < count($files); $i++) {
                $path = "$dir/$files[$i]";
                if (file_exists($path)) {
                    if ($files[$i] != "." && $files[$i] != "..") {
                        unlink($path);
                    }
                }
            }
            echo true;
        }else{
            echo false;
        }

    }
}