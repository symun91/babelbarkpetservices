<?php
/**
 * @property petmodel                    $petmodel
 * @property appusersmodel               $appusersmodel
 */
class Petprofile extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper(array('common_helper','url'));
        $this->load->model('petmodel');
        $this->load->model('appusersmodel');

    }


    function index(){
        redirect('vendor/login');
    }

    function viewPetProfile($petId,$userType,$token = null)
    {

        $sessiondata = $this->session->userdata('admin');
        $category = $sessiondata['adminDetails']['category'];
        $vendorId = $sessiondata['adminDetails']['vendorid'];
        $data['petClientInfo'] = $this->petmodel->getAppUserInfo($petId);

        if ($userType == 'Guest') {
            $fileType = 1; // 1 = pet profile
            $verifyToken = $this->appusersmodel->verifyToken($petId,$token,$fileType);
            if($verifyToken==1) {
                $data['msg'] = "";
                $this->_displayPetProfile($petId,$data);
            }else{
                $data['msg'] = 'Invalid Request';
                $this->load->view('vendor/filenotfound',$data);
            }
        }elseif($userType == 'Babelbark') {
            if($category==''){
                $data['msg'] = 'Invalid Request';
            }elseif($vendorId==''){
                $data['msg'] = 'Invalid Request';
            }else{
                $data['msg'] = "";
                $this->_displayPetProfile($petId,$data);
            }

        }else{
            $data['msg'] = 'Invalid Request';
            $this->load->view('vendor/filenotfound',$data);
        }
    }

    function _displayPetProfile($petId,$data){

        $data['petInfo'] = $this->petmodel->getPetInfo($petId);

        /************** Pet weight graph ***********/

        $data['petWeightGraph'] = $this->petmodel->getPetWeightGraph($petId);
        $petWeightGraph = $data['petWeightGraph'];
        $monthNames = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $monthArr = array();
        $weightArr = array();
        foreach ($monthNames as $val) {
            $weight = 0;
            foreach ($petWeightGraph as $weightRows) {
                if ($weightRows['month'] == $val) {
                    $weight = $weightRows['measuredwt'];
                }
            }
            $monthArr[] = $val;
            $weightArr[] = $weight;
        }
        $data['monthValuesForWeight'] = "'" . implode("','", $monthArr) . "'";
        $data['weightValuesForWeight'] = implode(",", $weightArr);

        /************** Activity Graph ***********/

        $data['petActivityLog'] = $this->petmodel->getPetActivitylog($petId);
        $petActivityLog = $data['petActivityLog'];
        $activityMonthArr = array();
        $activityPointArr = array();
        foreach ($monthNames as $val) {
            $activityPoint = 0;
            foreach ($petActivityLog as $activityRows) {
                if ($activityRows['month'] == $val) {
                    $activityPoint = ceil($activityRows['petPoint'] / 30);
                }
            }
            $activityMonthArr[] = $val;
            $activityPointArr[] = $activityPoint;
        }
        $data['activityMonth'] = "'" . implode("','", $activityMonthArr) . "'";
        $data['activityPoint'] = implode(",", $activityPointArr);

        /************** Dosage Frequency ***********/

        $data['medication'] = $this->petmodel->medicationInfo($petId);
        $medication = $data['medication'];
        $startTime = '';
        $medicationPercent = array();
        $TakenDosagePercent = 0;
        foreach ($medication as $medRows) {
            $petMedicationId = $medRows['petmedicationid'];
            $medicationTime = explode(",", $medRows['medicationtimings']);
            $startTime = $medicationTime[0];
            $frequencyTime = $medRows['frequencytimes'];

            $now = time(); // or your date as well
            $your_date = strtotime($startTime);
            $dateDiff = abs($now - $your_date);
            $noOfDays = intval($dateDiff / (60 * 60 * 24));
            $TotalNoOfDosageUnit = $noOfDays * $frequencyTime;
            $TakenNoOfDosageUnit = $this->petmodel->getMedicationLog($petMedicationId, $petId);
            if ($TotalNoOfDosageUnit > 0 && $TakenNoOfDosageUnit > 0) {
                $TakenDosagePercent = round(($TakenNoOfDosageUnit / $TotalNoOfDosageUnit) * 100, 1);
            } else {
                $TakenDosagePercent = 0;
            }
            $medicationPercent[] = $TakenDosagePercent;
        }
        $data['medicationPercent'] = $medicationPercent;
        $this->data['maincontent'] = $this->load->view('petprofile', $data, true);
        $this->load->view('vendor/petprofile_layout', $this->data);
    }

}
?>