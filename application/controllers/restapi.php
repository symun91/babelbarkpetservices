<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
/**
 * @property appusersmodel               $appusersmodel
 * @property appuserdevicesmodel         $appuserdevicesmodel
 * @property petmodel                    $petmodel
 * @property petmedicationmodel          $petmedicationmodel
 * @property petvaccinationmodel         $petvaccinationmodel
 * @property petgoalsmodel               $petgoalsmodel
 * @property petlogmodel                 $petlogmodel
 * @property veterinarianmodel           $veterinarianmodel
 * @property petfeedmodel                $petfeedmodel
 * @property petbreedmodel               $petbreedmodel
 * @property vendorsmodel                $vendorsmodel
 * @property appuservendorsmodel         $appuservendorsmodel
 * @property promotionsmodel             $promotionsmodel
 * @property mailnotifymodel             $mailnotifymodel
 * @property nonregistervendormodule     $nonregistervendormodule
 * @property appointmentmodel            $appointmentmodel
 * @property servicemodel                $servicemodel
 * @property customermodel               $customermodel
 * @property medicalfilesmodel           $medicalfilesmodel
 * @property sikkapetimportrequestsmodel $sikkapetimportrequestsmodel
 */
class restapi extends REST_Controller
{

    function __construct()
    {
        //ini_set('display_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        // Construct our parent class
        parent::__construct();
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
        date_default_timezone_set('UTC');

        $this->load->helper(array('common_helper','url'));
        $this->load->library('encrypt');
        //$this->load->model(array('appusersmodel','appuserdevicesmodel','petmodel','petmedicationmodel','petvaccinationmodel','petgoalsmodel','petlogmodel','veterinarianmodel','petfeedmodel','petbreedmodel','vendorsmodel','appuservendorsmodel','promotionsmodel','mailnotifymodel','nonregistervendormodule','appointmentmodel','servicemodel','customermodel','medicalfilesmodel','sikkapetimportrequestsmodel'));
        $this->load->model(
            array(
                'api/appusersmodel',
                'api/appuserdevicesmodel',
                'api/petmodel',
                'api/petmedicationmodel',
                'api/petvaccinationmodel',
                'api/petgoalsmodel',
                'api/petlogmodel',
                'api/veterinarianmodel',
                'api/petfeedmodel',
                'api/petbreedmodel',
                'api/vendorsmodel',
                'api/appuservendorsmodel',
                'api/promotionsmodel',
                'api/mailnotifymodel',
                'api/nonregistervendormodule',
                'api/appointmentmodel',
                'api/servicemodel',
                'api/customermodel',
                'api/medicalfilesmodel',
                'api/sikkapetimportrequestsmodel')
        );
    }

    //booking : mode= serving/waiting/finished
    //            status= completed/accepted/rejected/cancelled

    private static function genratePrimaryKey($serverid)
    {
        $primarykey=$serverid.time().mt_rand(1000, 9999);
        if($primarykey>9223372036854775807) //max of 64 bit int
        {
            genratePrimaryKey($serverid);
        }
        return $primarykey;
    }


    private static function genrateSessionToken()
    {
        $sessiontok=md5(microtime().rand());
        return $sessiontok;
    }

    function loginUser_POST()
    {
        $email =  ($this->input->post('email')) ? $this->input->post('email') : 0;
        $password =  ($this->input->post('password')) ? $this->input->post('password') : 0;
        $devicename = $this->input->post('devicename');
        $devicetoken= $this->input->post('devicetoken');
        $deviceuuid= $this->input->post('deviceuuid');
        $isiphone= $this->input->post('isiphone');

        $userid = $this->appusersmodel->fetchUserID($email);
        if($userid == 0)
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error' ,'statusdesc' => INVALIDLOGINMSG), 200);
            return;
        }

        if($this->appuserdevicesmodel->is_UserInDeviceListExists($userid) == 1)
        {
            $deviceid = 0;
            $devicecc = array(
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,
                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->updateDeviceWithUserid($devicecc , $userid);

            $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);

        }
        else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
        {
            $deviceid = 0;
            $devicecc = array(
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,
                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
        }
        else {

            $deviceid = $this->genratePrimaryKey('20');
            $devicecc = array(
                'deviceid' => $deviceid,
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,

                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->registerDevice($devicecc);

        }
        $userid = $this->appusersmodel->userLogin($email,md5($password));
        if($userid != 0)
        {
            $datacc=array();
            $datacc['sessiontoken']=$this->genrateSessionToken();
            $datacc['lastloggedon']=date('Y-m-d H:i');
            $this->appusersmodel->updateUser($datacc,$userid);

            $retarr=array();
            $retarr=$this->appusersmodel->fetchUserDetails($userid);
            $petid=$this->appusersmodel->getPetID($userid);
            if($petid==0)
            {

                $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => 'Wrong Signup details, Kindly contact support@babelbark.com'), 200);
            }else{
                $petdata=$this->petmodel->getPetDetails($petid);
                if($petdata!=0)
                {
                    if(count($petdata)>0)
                    {
                        $petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
                        $petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
                        $petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
                        $petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
                        $petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
                        $petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
                        $retarr['petdetails']= $petdata;
                    }
                }
                else
                {
                    $petdata=array();
                    $retarr['petdetails']=$petdata;
                }

                $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => LOGINSUCCMSG,'data' =>$retarr), 200);
            }

        }
        else {

            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => INVALIDLOGINMSG), 200);
        }
    }

    function loginUserWithFB_POST()
    {

        $facebookid=$this->input->post('facebookid');
        $username=$this->input->post('username');
        $devicename = $this->input->post('devicename');
        $devicetoken= $this->input->post('devicetoken');
        $deviceuuid= $this->input->post('deviceuuid');
        $isiphone= $this->input->post('isiphone');

        $userid = $this->appusersmodel->fetchUserIDByFBId($facebookid);

        if($userid == 0)
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error' ,'statusdesc' => INVALIDFACEBOOKLOGINMSG), 200);
            return;
        }

        if($this->appuserdevicesmodel->is_UserInDeviceListExists($userid) == 1)
        {
            $deviceid = 0;
            $devicecc = array(
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,
                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->updateDeviceWithUserid($devicecc , $userid);

            $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);

        }
        else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
        {
            $deviceid = 0;
            $devicecc = array(
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,
                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
        }
        else {

            $deviceid = $this->genratePrimaryKey('20');
            $devicecc = array(
                'deviceid' => $deviceid,
                'userid' => $userid,
                'devicename' => $devicename,
                'devicetoken' => $devicetoken,
                'deviceuuid'  => $deviceuuid ,

                'isiphone'  => $isiphone
            );
            $id = $this->appuserdevicesmodel->registerDevice($devicecc);

        }
        $userid = $this->appusersmodel->userFBLogin(md5($facebookid),$facebookid);
        if($userid == 0)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => INVALIDFACEBOOKLOGINMSG), 200);
        }
        else if ($userid == -1)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => ERRORLOGINMSG), 200);
        }
        else {
            $datacc=array();
            $datacc['sessiontoken']=$this->genrateSessionToken();
            $datacc['lastloggedon']=date('Y-m-d H:i');
            $this->appusersmodel->updateUser($datacc,$userid);

            $retarr=array();
            $retarr=$this->appusersmodel->fetchUserDetails($userid);
            $petid=$this->appusersmodel->getPetID($userid);

            $petdata=$this->petmodel->getPetDetails($petid);
            if($petdata!=0)
            {
                if(count($petdata)>0)
                {
                    $petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
                    $petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
                    $petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
                    $petdata['primarybreed']=$this->petbreedmodel->getBreedDetails($petdata['primarybreed']);
                    $petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
                    $petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
                    $retarr['petdetails']= $petdata;
                }
            }
            else
            {
                $petdata=array();
                $retarr['petdetails']=$petdata;
            }

            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => LOGINSUCCMSG,'data' =>$retarr), 200);
        }
    }



    function signUp_post()
    {
        $bool = TRUE;
        if($this->input->post('email')) {
            $email = $this->input->post('email');
        }
        else {
            $bool = FALSE;
        }
        if($this->input->post('password')) {
            $password = $this->input->post('password');
        }
        else {
            $bool = FALSE;
        }
        if($this->input->post('devicename')) {
            $devicename = $this->input->post('devicename');
        }
        else {
            $bool =FALSE;
        }

        if($this->input->post('devicetoken')) {
            $devicetoken= $this->input->post('devicetoken');
        }
        else {
            $bool =FALSE;
        }
        if($this->input->post('deviceuuid')) {
            $deviceuuid= $this->input->post('deviceuuid');
        }
        else {
            $bool =FALSE;
        }
        if($this->input->post('isiphone') || $this->input->post('isiphone')==0) {
            $isiphone= $this->input->post('isiphone');
        }
        else {
            $bool =FALSE;
        }
        if($this->input->post('username')) {
            $username= $this->input->post('username');
        }
        else {
            $bool =FALSE;
        }



        $facebookid =  ($this->input->post('facebookid')) ? $this->input->post('facebookid') : "";

        $isEmailExist = $this->appusersmodel->isEmailExists($email);

        if($isEmailExist>0){
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => EMAILEXISTMSG), 200);
            //return false;
        }



        $userid = $this->genratePrimaryKey('40');

        if(!$bool)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
            return false;
        }

        if(!empty($facebookid))
        {
            if($this->appusersmodel->isFacebookidExist($facebookid) == 1)
            {
                $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
                return;
            }
        }

        $activationtoken=$this->genrateSessionToken();
        $datacc = array(
            'appuserid' => $userid ,
            'email' => $email,
            'password' => md5($password),
            'username' => $username,
            'sessiontoken'=> $this->genrateSessionToken(),
            'lastloggedon'=>date('Y-m-d H:i'),
            'fbid' => $facebookid,
            'isverified' => 0,
            'activationtoken'=>$activationtoken
        );

        $id = $this->appusersmodel->registerUser($datacc);

        if($id > 0)
        {
            //add default vendor
            /*($datacc=array();
			   $datacc['appuserid']=$userid;
	 		 $datacc['vendorids']='123656767'; //for testing
			$this->appuservendorsmodel-> updateVendors($datacc,$userid);*/

            if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
            {
                $deviceid = 0;
                $devicecc = array(
                    'userid' => $userid,
                    'devicename' => $devicename,
                    'devicetoken' => $devicetoken,
                    'deviceuuid'  => $deviceuuid ,
                    'isiphone'  => $isiphone
                );
                $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
                $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);
            }
            else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
            {
                $deviceid = 0;
                $devicecc = array(
                    'userid' => $userid,
                    'devicename' => $devicename,
                    'devicetoken' => $devicetoken,
                    'deviceuuid'  => $deviceuuid ,
                    'isiphone'  => $isiphone
                );
                $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
            }
            else {
                $deviceid = $this->genratePrimaryKey('20');
                $devicecc = array(
                    'deviceid' => $deviceid,
                    'userid' => $userid,
                    'devicename' => $devicename,
                    'devicetoken' => $devicetoken,
                    'deviceuuid'  => $deviceuuid ,
                    'isiphone'  => $isiphone
                );
                $id = $this->appuserdevicesmodel->registerDevice($devicecc);
            }

            //send registration mail to activate account
            $subject = "BabelBark Account Verification";
            $toemail=$email;
            // To send HTML mail, the Content-type header must be set.
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From:' . SITE_MAIL. "\r\n"; // Sender's Email
            $sendmessage  = file_get_contents('resources/appuser_activate_template.html');
            //$sendmessage = str_replace('%username%', $username, $sendmessage);

            $url = base_url() . 'activateappuseraccount?token='.$activationtoken;
            $sendmessage = str_replace('%activateurl%', $url, $sendmessage);

            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->subject($subject);
            //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');

            $this->email->message($sendmessage);


            $result =$this->email->send();

            //mail($toemail, $subject, $sendmessage, $headers);
            //	send_template_mail('',$sendmessage, $subject, $toemail, SITE_MAIL);

            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => REGSUCCMSG,'data' =>$this->appusersmodel->fetchUserDetails($userid)), 200);

        }
        else {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }
    }
    /*
         * profile setup app USER
         * Params- firstname,lastname,email,password,birthdate,gender,address,city,state,zip,devicename,devicetoken,deviceuuid,isiphone
         */
    function setUserProfile_post()	 {
        $bool = TRUE;
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        if($this->input->post('firstname')) {
            $firstname= $this->input->post('firstname');
        }
        else {
            $firstname = "";
        }
        if($this->input->post('lastname')) {
            $lastname= $this->input->post('lastname');
        }
        else {
            $lastname = "";
        }
        if($this->input->post('email')) {
            $email = $this->input->post('email');
        }
        else {
            $bool = FALSE;
        }

        if($this->input->post('birthdate')) {
            $birthdate = $this->input->post('birthdate');
        }
        else{
            $birthdate = "";
        }

        if($this->input->post('gender')) {
            $gender = $this->input->post('gender');
        }
        else{
            $gender = "";
        }


        if($this->input->post('address')) {
            $address = $this->input->post('address');
        }
        else{
            $address = "";
        }
        if($this->input->post('country')) {
            $country = $this->input->post('country');
        }
        else{
            $country = "";
        }
        if($this->input->post('city')) {
            $city = $this->input->post('city');
        }
        else{
            $city = "";
        }
        if($this->input->post('state')) {
            $state = $this->input->post('state');
        }
        else{
            $state = "";
        }
        if($this->input->post('zip')) {
            $zip = $this->input->post('zip');
        }
        else{
            $zip = "";
        }
        if($this->input->post('phonenumber')) {  // added by debartha
            $phonenumber = $this->input->post('phonenumber');
        }
        else{
            $phonenumber = "";
        }


        if(!$bool)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
            return false;
        }


        $datacc = array(
            'firstname' => $firstname,
            'lastname'=>$lastname,
            'email' => $email,
            // 'password' => md5($password),
            'birthday' => $birthdate,
            'gender' => $gender,
            'appuserid' => $userid ,
            'address' => $address,
            'city' => $city,
            'state' =>$state,
            'phonenumber' => $phonenumber,
            'zip' => $zip,
            'country' => $country,
//            'sessiontoken'=> $this->genrateSessionToken(),
            'lastloggedon'=>date('Y-m-d H:i')

        );

        $this->appusersmodel->updateUser($datacc,$userid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEPROFSUCCMSG,'data' => $this->appusersmodel->fetchUserDetails($userid)),200);

    }
    /*
	 * UPDATE PROFILE
	 * params- firstname,lastname,email,birthdate,gender,address,city,state,zip
	 */
    function updateUserProfile_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $datacc = array();
        if($this->post('email')) {
            $email = $this->post('email');
            if($this->appusersmodel->isEmailExistsForAUser($email,$userid) == 1)
            {
                $this->response(array('statuscode' => '400','statusmsg' => 'success','statusdesc' => EMAILEXISTMSG),200);
                return false;;
            }
            $datacc['email'] = $email;
        }

        if($this->post('firstname')) {
            $firstname = $this->post('firstname');
            $datacc['firstname'] = $firstname;
        }

        if($this->post('lastname')) {
            $lastname = $this->post('lastname');
            $datacc['lastname'] = $lastname;
        }
        /*

	        if($this->post('password')) {
			       $password = $this->post('password');
			      $datacc['password'] = md5($password);
			 }*/

        if($this->post('birthdate')) {
            $birthdate= $this->post('birthdate');
            $datacc['birthday'] = $birthdate;
        }
        if($this->input->post('gender')) {
            $language = $this->input->post('gender');
            $datacc['gender'] = $gender;
        }
        if($this->input->post('address')) {
            $address = $this->input->post('address');
            $datacc['address'] = $address;
        }

        if($this->input->post('city')) {
            $city = $this->input->post('city');
            $datacc['city'] = $city;
        }
        if($this->input->post('state')) {
            $state = $this->input->post('state');
            $datacc['state'] = $state;
        }

        if($this->input->post('country')) {
            $country = $this->input->post('country');
            $datacc['country'] = $country;
        }

        if($this->input->post('zip')) {
            $zip = $this->input->post('zip');
            $datacc['zip'] = $zip;
        }
        if($this->input->post('phonenumber')) {
            $phonenumber = $this->input->post('phonenumber');
            $datacc['phonenumber'] = $phonenumber;
        }

        if(count($datacc) > 0)
            $this->appusersmodel->updateUser($datacc,$userid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEPROFSUCCMSG,'data' => $this->appusersmodel->fetchUserDetails($userid)),200);

    }

    function getpetProfiePercentage_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $result = $this->petmodel->getpetprofilepercentage($petid);

        if(is_null($result['currentweight']))
        {
            $result['currentweight'] = 0;
        }

        if(is_null($result['targetweight']))
        {
            $result['targetweight'] = 0;
        }

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);

    }

    function getmicrochipdetails_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $result = $this->petmodel->getmicrochipiddetails($petid);

        if(is_null($result['microchipid']))
        {
            $result['microchipid'] = "";
        }

        if(is_null($result['govtlicensetagno']))
        {
            $result['govtlicensetagno'] = "";
        }

        if(is_null($result['govtlicensetagissuer']))
        {
            $result['govtlicensetagissuer'] = "";
        }

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);

    }


    function getUserProfile_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->appusersmodel->fetchUserDetails($userid)), 200);
    }

    function getAllUserPets_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0)
        {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }
        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $result = $this->appusersmodel->getAllUserPets($userid);

        if($result !== 0)
        {
            $petdata = array();
            for ($i=0; $i < count($result) ; $i++)
            {
                $petid = $result[$i]['petid'];
                $petdetails = $this->petmodel->getPetDetails($petid);
                $petdata[$i] = $petdetails;
            }
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data'=>$petdata), 200);
            return true;
        }
        else
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' => $result), 200);
            return true;
        }

        $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => "INVALID USER ID"),200);
        return false;
    }

    function setPetProfile_post()
    {

        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $datacc = array();
        $bool = TRUE;

        if($this->post('petname')) {
            $petname = $this->post('petname');
            $datacc['name'] = $petname;
        }
        else {
            $bool=false;
        }
        if($this->post('primarybreed')) {
            $primarybreed = $this->post('primarybreed');
            $datacc['primarybreed'] = $primarybreed;
        }
        else {
            $bool=false;
        }

        if($this->post('secondarybreed')) {
            $secondarybreed = $this->post('secondarybreed');
            $datacc['secondarybreed'] = $secondarybreed;
        }
        if($this->post('primaryfood')) {
            $primaryfood = $this->post('primaryfood');
            $datacc['primaryfood'] = $primaryfood;
        }

        if($this->post('secondaryfood')) {
            $secondaryfood = $this->post('secondaryfood');
            $datacc['secondaryfood'] = $secondaryfood;
        }
        if($this->post('birthdate')) {
            $birthdate = $this->post('birthdate');
            $datacc['birthdate'] = $birthdate;
        }
        else {
            $bool=false;
        }

        if($this->input->post('isadopted') || $this->input->post('isadopted')==0){
            $isadopted = $this->post('isadopted');
            $datacc['isadopted'] = $isadopted;
        }
        if($this->input->post('approximateage') ){
            $approximateage = $this->post('approximateage');
            $datacc['approximateage'] = $approximateage;
        }
        if($this->post('gender')) {
            $gender = $this->post('gender');
            $datacc['gender'] = $gender;
        }
        else {
            $bool=false;
        }

        if($this->post('currentweight')) {
            $currentweight = $this->post('currentweight');
            $datacc['currentweight'] = $currentweight;
        }
        else {
            $bool=false;
        }
        if($this->post('targetweight')) {
            $targetweight = $this->post('targetweight');
            $datacc['targetweight'] = $targetweight;
        }
        if($this->input->post('ismicrochipped') || $this->input->post('ismicrochipped')==0){
            $microchipped = $this->post('ismicrochipped');
            $datacc['microchipped'] = $microchipped;
        }
        if($this->post('microchipid')) {
            $microchipid = $this->post('microchipid');
            $datacc['microchipid'] = $microchipid;
        }
        if($this->post('govtlicensetagno')) {
            $govtlicensetagno = $this->post('govtlicensetagno');
            $datacc['govtlicensetagno'] = $govtlicensetagno;
        }
        if($this->post('amountoffeed')) {
            $amountoffeed = $this->post('amountoffeed');
            $datacc['amountoffeed'] = $amountoffeed;
        }
        if($this->post('unitoffeed')) {
            $unitoffeed = $this->post('unitoffeed');
            $datacc['unitoffeed'] = $unitoffeed;
        }
        if($this->post('frequencyoffeed')) {
            $frequencyoffeed = $this->post('frequencyoffeed');
            $datacc['frequencyoffeed'] = $frequencyoffeed;
        }
        if($this->post('govtlicensetagissuer')) {
            $govtlicensetagissuer = $this->post('govtlicensetagissuer');
            $datacc['govtlicensetagissuer'] = $govtlicensetagissuer;
        }

        if($this->post('veterinarianid')) {
            $veterinarianid = $this->post('veterinarianid');
            $datacc['veterinarianid'] = $veterinarianid;
        }else {
            $bool=false;
        }
        if($this->post('yearlycheckupdate')) {
            $yearlycheckupdate = $this->post('yearlycheckupdate');
            $datacc['yearlycheckupdate'] = $yearlycheckupdate;
        }
        $isprescribed = $this->post('isprescribed');
        $datacc['isprescribed'] = $isprescribed;
        if($this->post('misfitdevid')) {
            $misfitdevid = $this->post('misfitdevid');
            $datacc['misfitdevid'] = $misfitdevid;
        }
        if($this->post('profileCompletion'))
        {
            $profileCompletion = $this->post('profileCompletion');
            $datacc['profileCompletion'] = $profileCompletion;
        }

        if(!$bool)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
            return false;
        }

        $petid = $this->genratePrimaryKey('40');

        $datacc['petid']=$petid;
        $id = $this->petmodel->registerPet($datacc);

        if($id > 0) {
            //add user pet
            $datacc = array();
            $datacc['appuserid'] = $userid;
            $datacc['petid'] = $petid;
            $this->appusersmodel->addpet($datacc);
            //profile pic
            $imgurl = '';
            if (isset($_FILES['profileimage'])) {
                $allowed = array('gif', 'png', 'jpg', 'jpeg');
                $filename = $_FILES['profileimage']['name'];
                $filetmpname = $_FILES['profileimage']['tmp_name'];
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {

                    $this->response(array('statuscode' => '400', 'statusmsg' => 'error', 'statusdesc' => INVALIDFILEEXTMSG), 200);
                    return false;
                }
                if (is_uploaded_file($_FILES['profileimage']['tmp_name'])) {

                    /************************************* local server upload *****************************/

//                    if (!file_exists('api/resources/uploads/petimage/')) {
//                        mkdir('api/resources/uploads/petimage/');
//                    }
//                    $uploaddir = realpath('./api/resources/uploads/petimage/');
//
//                    $ext = end(explode('.', $_FILES['profileimage']['name']));
//                    $fname = $petid . '_' . time() . "." . $ext;
//                    $uploadfile = $uploaddir . "/" . $fname;
//                    if (move_uploaded_file($_FILES['profileimage']['tmp_name'], $uploadfile)) {
//                        $imgurl = 'resources/uploads/petimage/' . $fname;
//                    }
                    /************************************* S3 Integration  *****************************/

                    $uploaddir = 'resources/uploads/petimage/';
                    $ext = end(explode('.', $_FILES['profileimage']['name']));
                    $fname = $petid . '_' . time() . "." . $ext;
                    $s3msg = aws_upload($uploaddir,$fname,$filetmpname);

                    if($s3msg=='S3 upload successful'){
                        $imgurl = 'resources/uploads/petimage/' . $fname;
                    }

                    /************************************* End S3 Integration  *****************************/
                }
                $datacc = array();
                $datacc['proilefpicture'] = $imgurl;
                $this->petmodel->updatePet($datacc, $petid);
            }


            //add medication
            $medications = $this->post('medications');
            if (isset($medications)) {
                $medicationjson = $this->post('medications');
                if (isset($medicationjson)) {
                    $medobj = json_decode($medicationjson, true);
                    if (!empty($medobj)) {
                        foreach ($medobj['medication'] as $item) {
                            $datacc = array();
                            $datacc['petid'] = $petid;
                            $datacc['medicineid'] = $item['id'];
                            $datacc['frequency'] = $item['frequency'];
                            $datacc['dosage'] = $item['dosage'];

                            $medicationtime = "08:00";
                            $datacc['medicationtime'] = $medicationtime;
                            if (array_key_exists('timings', $item)) {
                                $datacc['medicationtimings'] = $item['timings'];
                            }
                            if (array_key_exists('unit', $item)) {
                                $datacc['dosageunit'] = $item['unit'];
                            }

                            if (array_key_exists('remindersTime', $item)) {
                                $reminder = $item['remindersTime'];

                                // $medicationtime =   $reminder[0]['time'];
                                for ($i = 0; $i < count($reminder); $i++) {
                                    if ($i == 0) {
                                        $medicationtime = $reminder[$i]['time'];
                                    } else {
                                        $medicationtime = $medicationtime . "," . $reminder[$i]['time'];
                                    }
                                }

                                $datacc['medicationtimings'] = $medicationtime;

                            }

                            if (array_key_exists('timesOfFrequency', $item)) {
                                $datacc['frequencytimes'] = $item['timesOfFrequency'];
                            }


                            $this->petmedicationmodel->addMedication($datacc);
                        }
                    }
                }
            }

            //add vaccination
            if($this->post('vaccinations')) {
                $vaccjson=$this->post('vaccinations');
                $vacobj = json_decode($vaccjson, true);
                foreach($vacobj['vaccination'] as $item)
                {
                    $datacc=array();
                    $datacc['petid']=$petid;
                    $datacc['vaccineid']=$item['id'];
                    $datacc['duedate']=$item['duedate'];
                    $this->petvaccinationmodel->addVaccination($datacc);
                }

            }
            $petdata=$this->petmodel->getPetDetails($petid);
            if(count($petdata)>0)
            {
                $petdata['vaccination']=$this->petmodel->getPetVaccineDetails($petid);
                $petdata['medication']=$this->petmodel->getPetMedicationDetails($petid);
                $petdata['feedtitile']=$this->petfeedmodel->getFeedTitle($petdata['primaryfood']);
                $petdata['breedtitile']=$this-> petbreedmodel-> getBreedTitle($petdata['primarybreed']);

                $petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
                $petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
                $petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
                $petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
            }

            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data' =>$petdata),200);
        }
        else{
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }
    }
    function getuseremail_post()
    {

        $userid = $this->post('userid');
        $useremail = $this->appusersmodel->getuserEmail($userid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data'=>$useremail['email']),200);
    }

    function updatePetVeterinarian_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid = $this->post('petid');
        $data = array();
        if($this->post('primaryvet'))
        {
            $primaryvet = $this->post('primaryvet');
            $data['primaryvet'] = $primaryvet;

        }
        if($this->post('emergencyvet'))
        {
            $emergencyvet = $this->post('emergencyvet');
            $data['emergencyvet'] = $emergencyvet;
        }

        $result = $this->appusersmodel->updateUser($data,$userid);

        if($result > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }
        else
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }
    }

    function updatePetMisfitdevice_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid = $this->post('petid');
        $data = array();
        if($this->post('misfitdeviceid'))
        {
            $misfitdeviceid = $this->post('misfitdeviceid');
            $data['misfitdeviceid'] = $misfitdeviceid;

        }

        $result = $this->petmodel->updatePet($data,$petid);

        if($result > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }
        else
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => UPDATEDATASUCCMSG), 200);
        }

    }


    function updateCurrentPetID_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid = $this->post('petid');
        $data = array();
        $data['currentpetid'] = $petid;

        $result = $this->appusersmodel->updateUser($data,$userid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);

    }


    function updatePetLostFound_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid = $this->post('petid');
        $data = array();
        if($this->post('lostfoundvendor'))
        {
            $lostfoundvendor = $this->post('lostfoundvendor');
            $data['lostfoundvendor'] = $lostfoundvendor;
        }

        $result = $this->appusersmodel->updateUser($data,$userid);

        if($result > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }
        else
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }

    }


    function fetchPetVeterinarian_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }



        $result = $this->appusersmodel->getUserVet($userid);
        $data = array();

        if($result!=0)
        {
            if($result['primaryvet'])
            {
                $vetresult = $this->vendorsmodel->getVendorDetailsVet($result['primaryvet']);
                if($vetresult==0)
                {
                    $data['primary'] = $this->nonregistervendormodule->getNonregiVendorDetails($result['primaryvet']);

                    if($data['primary'] == 0)
                    {
                        $data['noprimary'] = NULL;
                    }else{
                        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis";

                        $placeid = $data['primary']['placeid'];

                        $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
                        $detailjson = file_get_contents($placedetailurl);
                        $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

                        if( isset( $placedata['result']['opening_hours'] ))
                        {
                            if($placedata['result']['opening_hours']['open_now'])
                            {
                                $data['primary']['openinghours'] = "Open Today";
                            }else{
                                $data['primary']['openinghours'] = "Closed";
                            }
                        }
                        else
                            $data['primary']['openinghours']="";
                        $data['primary']['isregistered'] = 0;
                    }
                }
                else{
                    $data['primary'] = $vetresult;
                    $data['primary']['isregistered'] = 1;
                }

            }
            else{
                $data['noprimary'] = NULL;
                //$data['primary']['isregistered'] = 1;
            }

            if($result['emergencyvet'])
            {
                $vetresult= $this->vendorsmodel->getVendorDetailsVet($result['emergencyvet']);
                if($vetresult==0)
                {
                    $data['emergency'] = $this->nonregistervendormodule->getNonregiVendorDetails($result['emergencyvet']);

                    if($data['emergency'] == 0)
                    {
                        $data['noemergency'] = NULL;
                    }else{
                        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis";

                        $placeid = $data['emergency']['placeid'];

                        $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
                        $detailjson = file_get_contents($placedetailurl);
                        $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

                        if( isset( $placedata['result']['opening_hours'] ))
                        {
                            if($placedata['result']['opening_hours']['open_now'])
                            {
                                $data['emergency']['openinghours'] = "Open Today";
                            }else{
                                $data['emergency']['openinghours'] = "Closed";
                            }
                        }
                        else
                            $data['emergency']['openinghours']="";
                        $data['emergency']['isregistered'] = 0;
                    }
                }
                else{
                    $data['emergency'] = $vetresult;
                    $data['emergency']['isregistered'] = 1;
                }
            }
            else{
                $data['noemergency'] = NULL;
                //$data['primary']['isregistered'] = 1;
            }
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => FETCHDATASUCCMSG,'data'=>$data),200);
        }
        else {

            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR), 200);

        }
    }

    function fetchpetlostfound_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }



        $result = $this->appusersmodel->getUserLostfound($userid);
        $data = array();

        if($result!=0)
        {

            if($result['lostfoundvendor'])
            {
                $vetresult = $this->vendorsmodel->getVendorDetailsVet($result['lostfoundvendor']);
                if($vetresult==0)
                {
                    $data['lostfoundvendor'] = $this->nonregistervendormodule->getNonregiVendorDetails($result['lostfoundvendor']);

                    if($data['lostfoundvendor']==0)
                    {
                        $data['nolostnfound'] = NULL;
                    }else{
                        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis";

                        $placeid = $data['lostfoundvendor']['placeid'];

                        $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
                        $detailjson = file_get_contents($placedetailurl);
                        $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

                        if( isset( $placedata['result']['opening_hours'] ))
                        {
                            if($placedata['result']['opening_hours']['open_now'])
                            {
                                $data['lostfoundvendor']['openinghours'] = "Open Today";
                            }else{
                                $data['lostfoundvendor']['openinghours'] = "Closed";
                            }
                        }
                        else
                            $data['lostfoundvendor']['openinghours']="";


                        $data['lostfoundvendor']['isregistered'] = 0;
                    }


                }
                else{
                    $data['lostfoundvendor'] = $vetresult;
                    $data['lostfoundvendor']['isregistered'] = 1;
                }
            }else{
                $data['nolostnfound'] = NULL;
            }
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => FETCHDATASUCCMSG,'data'=>$data),200);
        }
        else{
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR), 200);
        }
    }

    function requestMedicalReport_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid= $this->post('petid');
        $vetemail = $this->post('vetemail');
        $vetname = $this->post('vetname');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $token = $this->genrateSessionToken();

        $data['petid'] = $petid;
        $data['appuserid'] = $userid;
        $data['vetemail']= $vetemail;
        $data['token']=$token;
        $data['filename']="";
        $data['fileurl']="";

        $id = $this->medicalfilesmodel->generateMedicalRow($data);
        $useremail = $this->appusersmodel->getuserEmail($userid);
        $dogname = $this->petmodel->getPetName($petid);


        if($id>0)
        {

            //$sent = $this->mailnotifymodel->sendmedicalrequest($vetemail,$useremail['email'],$token);
            $subject = "Need a copy of my pet's medical records.";
            $toemail=$vetemail;


            // To send HTML mail, the Content-type header must be set.
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
            $headers .= "From: BizBark <" . SITE_MAIL_SUPPORT_BIZBARK. ">\r\n";
            $headers .= "Reply-To: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .= "Return-Path: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .=  "CC: BabelBark User <" . $useremail['email'] .">\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
            // Sender's Email
            $sendmessage  = file_get_contents('resources/requestMedicalReport_template.html');
            //$sendmessage = str_replace('%username%', $username, $sendmessage);
            $userNAME = $useremail['firstname'].' '.$useremail['lastname'];
            $url = '';

            $url = base_url().'/uploadMedicalFile?token='.$token;

            $sendmessage = str_replace('%uploadlink%', $url, $sendmessage);
            $sendmessage = str_replace('%dogname%', $dogname['name'], $sendmessage);
            $sendmessage = str_replace('%customername%',$userNAME, $sendmessage);
            $sendmessage = str_replace('%vendorname%', $vetname, $sendmessage);


            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->cc($useremail['email']);
            $this->email->subject($subject);

            $this->email->message($sendmessage);


            $result =$this->email->send();
            if(!$result)
            {
                $result = $this->email->print_debugger();
            }

            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data'=>$result),200);
        }else{
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }


    }

    function shareMedicalFile_POST(){
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $medicalfileid= $this->post('medicalfileid');
        $filetype = $this->post('filetype'); //1 = medical file 2 = sikka medical file
        $petid= $this->post('petid');
        $receiveremail = $this->post('receiveremail');
        $receivername = $this->post('receivername');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        if($medicalfileid!='')
        {
            $useremail = $this->appusersmodel->getuserEmail($userid);
            $availableDays = 7;
            $startdate = date('Y-m-d');
            $expiredated = date('Y-m-d', strtotime($startdate. " + $availableDays days"));
            $data['receiveremail'] = $receiveremail;
            $data['receivername'] = $receivername;
            $data['startdate']= $startdate;
            $data['expiredated'] = $expiredated;

            /***************** Medication File Share ***********************/
            if($filetype==1){
                $MedicalInfo= $this->medicalfilesmodel->getMedicalInfo($medicalfileid);
            }else{
                $MedicalInfo= $this->medicalfilesmodel->getSikkaMedicalInfo($medicalfileid);
            }


            $MedicalFileUrl= $MedicalInfo[0]['fileurl'];
            $MedicalFileName= $MedicalInfo[0]['filename'];
            $ext = explode(".",$MedicalFileName);
            $saveNewMedName = time().'_'.$medicalfileid.'_'.$petid.'.'.$ext[1];

            /***************** Pet Profile Share ***********************/
            $token = $this->genrateSessionToken();
            $data['token'] = $token;
            $filetype = 1;
            $data['recordid'] = $petid;
            $data['filetype'] = $filetype;
            $isPetProfileUpdated = $this->appusersmodel->UpdateShareRecords($petid,$filetype);
            $isPetProfileAdded = $this->appusersmodel->addToShareRecords($data);
            $petInfo = $this->petmodel->getPetName($petid);
            $PetProfileUrl = BABELVET_ADD.'/viewPetProfile/'.$petid.'/Guest/'.$token;

            $subject = "Pet Profile : ".$petInfo['name'];
            $toemail=$receiveremail;

            // To send HTML mail, the Content-type header must be set.
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
            $headers .= "From: BizBark <" . SITE_MAIL_SUPPORT_BIZBARK. ">\r\n";
            $headers .= "Reply-To: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .= "Return-Path: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .=  "CC: BabelBark User <" . $useremail['email'] .">\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
            // Sender's Email
            $sendmessage  = file_get_contents('resources/shareMedicalFile_template.html');
            $userNAME = $useremail['firstname'].' '.$useremail['lastname'];
            $sendmessage = str_replace('%petname%', $petInfo['name'], $sendmessage);
            $sendmessage = str_replace('%uploadProfilelink%', $PetProfileUrl, $sendmessage);
            $sendmessage = str_replace('%customername%',$userNAME, $sendmessage);
            $sendmessage = str_replace('%receivername%', $receivername, $sendmessage);

            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->cc($useremail['email']);
            $this->email->subject($subject);
            $this->email->message($sendmessage);
            $UploadPath=$_SERVER["DOCUMENT_ROOT"]."/resources/downloads/temp/".$saveNewMedName;
            $msg= aws_download($MedicalFileUrl,$UploadPath);
            if($msg=='File downloaded Successfully'){
                $this->email->attach($UploadPath,'attachment',$MedicalFileName);
            }
            $result =$this->email->send();
            if(!$result)
            {
                $result = $this->email->print_debugger();
            }
            unlink($UploadPath);
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data'=>$result),200);
        }else{
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }
    }

    function sharePetProfile_POST(){
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid= $this->post('petid');
        $receiveremail = $this->post('receiveremail');
        $receivername = $this->post('receivername');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $token = $this->genrateSessionToken();
        $url = BABELVET_ADD.'/viewPetProfile/'.$petid.'/Guest/'.$token;

        $availableDays = 7;
        $startdate = date('Y-m-d');
        $expiredated = date('Y-m-d', strtotime($startdate. " + $availableDays days"));
        $data['recordid'] = $petid;
        $data['receiveremail'] = $receiveremail;
        $data['receivername'] = $receivername;
        $data['token'] = $token;
        $data['startdate']= $startdate;
        $data['expiredated'] = $expiredated;
        $filetype = 1;
        $data['filetype'] = $filetype;
        $isUpdated = $this->appusersmodel->UpdateShareRecords($petid,$filetype);
        $isAdded = $this->appusersmodel->addToShareRecords($data);
        $petInfo = $this->petmodel->getPetName($petid);
        $useremail = $this->appusersmodel->getuserEmail($userid);

        if($isAdded)
        {

            $subject = "Pet Profile: ".$petInfo['name'];
            $toemail = $receiveremail;

            // To send HTML mail, the Content-type header must be set.
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
            $headers .= "From: BizBark <" . SITE_MAIL_SUPPORT_BIZBARK. ">\r\n";
            $headers .= "Reply-To: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .= "Return-Path: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .=  "CC: BabelBark User <" . $useremail['email'] .">\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
            // Sender's Email
            $sendmessage  = file_get_contents('resources/sharePetProfile_template.html');
            $userNAME = $useremail['firstname'].' '.$useremail['lastname'];
            $sendmessage = str_replace('%uploadlink%', $url, $sendmessage);
            $sendmessage = str_replace('%customername%',$userNAME, $sendmessage);
            $sendmessage = str_replace('%receivername%', $receivername, $sendmessage);
            $sendmessage = str_replace('%petname%', $petInfo['name'], $sendmessage);

            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->cc($useremail['email']);
            $this->email->subject($subject);
            $this->email->message($sendmessage);

            $result =$this->email->send();

            if(!$result)
            {
                $result = $this->email->print_debugger();
            }

            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data'=>$result),200);
        }else{
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }
    }

    function getMedicalReports_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petId=$this->post('petid');
        $result = $this->medicalfilesmodel->getMedicalReports($userid,$petId);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => FETCHDATASUCCMSG,'data' => $result),200);
    }

    function getPetMedicationlog_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        $data['fullTime'] =  $this->petlogmodel->getMedicationLog($petid);
        $data['month']  = $this->petlogmodel->getMedicationLogOfthisMonth($petid);
        $data['yesterday'] = $this->petlogmodel->getYesterdayMedication($petid);
        $data['fullTimeInactive'] =  $this->petlogmodel->getMedicationLogInactive($petid);
        $data['monthInactive']  = $this->petlogmodel->getMedicationLogOfthisMonthInactive($petid);
        $data['yesterdayInactive'] = $this->petlogmodel->getYesterdayMedicationInactive($petid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => FETCHDATASUCCMSG,'data' => $data),200);

    }


    function updatePetProfile_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');
        $datacc=array();

        $datacc['petid']=$petid;
        if($this->post('petname')) {
            $petname = $this->post('petname');
            $datacc['name'] = $petname;
        }
        if($this->post('primarybreed')) {
            $primarybreed = $this->post('primarybreed');
            $datacc['primarybreed'] = $primarybreed;
        }
        if($this->post('secondarybreed')) {
            $secondarybreed = $this->post('secondarybreed');
            $datacc['secondarybreed'] = $secondarybreed;
        }
        if($this->post('primaryfood')) {
            $primaryfood = $this->post('primaryfood');
            $datacc['primaryfood'] = $primaryfood;
        }
        if($this->post('secondaryfood')) {
            $secondaryfood = $this->post('secondaryfood');
            $datacc['secondaryfood'] = $secondaryfood;
        }
        if($this->post('birthdate')) {
            $birthdate = $this->post('birthdate');
            $datacc['birthdate'] = $birthdate;
        }
        if($this->input->post('isadopted') || $this->input->post('isadopted')==0){
            $isadopted = $this->post('isadopted');
            $datacc['isadopted'] = $isadopted;
        }
        if($this->input->post('approximateage') ){
            $approximateage = $this->post('approximateage');
            $datacc['approximateage'] = $approximateage;
        }
        if($this->post('gender')) {
            $gender = $this->post('gender');
            $datacc['gender'] = $gender;
        }
        if($this->post('currentweight')) {
            $currentweight = $this->post('currentweight');
            $datacc['currentweight'] = $currentweight;
        }
        if($this->post('targetweight')) {
            $targetweight = $this->post('targetweight');
            $datacc['targetweight'] = $targetweight;
        }
        if($this->input->post('ismicrochipped') || $this->input->post('ismicrochipped')==0){
            $microchipped = $this->post('ismicrochipped');
            $datacc['microchipped'] = $microchipped;
        }
        if($this->post('microchipid')) {
            $microchipid = $this->post('microchipid');
            $datacc['microchipid'] = $microchipid;
        }
        if($this->post('amountoffeed')) {
            $amountoffeed = $this->post('amountoffeed');
            $datacc['amountoffeed'] = $amountoffeed;
        }
        if($this->post('unitoffeed')) {
            $unitoffeed = $this->post('unitoffeed');
            $datacc['unitoffeed'] = $unitoffeed;
        }
        if($this->post('frequencyoffeed')) {
            $frequencyoffeed = $this->post('frequencyoffeed');
            $datacc['frequencyoffeed'] = $frequencyoffeed;
        }

        if($this->post('govtlicensetagno')) {
            $govtlicensetagno = $this->post('govtlicensetagno');
            $datacc['govtlicensetagno'] = $govtlicensetagno;
        }
        if($this->post('govtlicensetagissuer')) {
            $govtlicensetagissuer = $this->post('govtlicensetagissuer');
            $datacc['govtlicensetagissuer'] = $govtlicensetagissuer;
        }
        if($this->post('veterinarianid')) {
            $veterinarianid = $this->post('veterinarianid');
            $datacc['veterinarianid'] = $veterinarianid;
        }
        if($this->post('yearlycheckupdate')) {
            $yearlycheckupdate = $this->post('yearlycheckupdate');
            $datacc['yearlycheckupdate'] = $yearlycheckupdate;
        }
        //if($this->post('isprescribed')) {
        $isprescribed = $this->post('isprescribed');
        $datacc['isprescribed'] = $isprescribed;
        //}
        if($this->post('misfitdevid')) {
            $misfitdevid = $this->post('misfitdevid');
            $datacc['misfitdevid'] = $misfitdevid;
        }
        if($this->post('profileCompletion')){
            $percentageProfileCompletion  = $this->post('profileCompletion');
            $datacc['profileCompletion'] = $percentageProfileCompletion;
        }



        $id =  $this->petmodel->updatePet($datacc,$petid);

        //if($id > 0)
        //{


        //profile pic
        $imgurl = '';

        if(isset($_FILES['profileimage'])) {
            $allowed =  array('gif','png' ,'jpg' ,'jpeg');
            $ext =  end(explode('.', $_FILES['profileimage']['name']));
            $filetmpname = $_FILES['profileimage']['tmp_name'];
            if(!in_array($ext,$allowed) ) {

                $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => INVALIDFILEEXTMSG),200);
                return false;;
            }
            if(is_uploaded_file($_FILES['profileimage']['tmp_name']))
            {
                /************************************* local server upload  *****************************/
//                if (!file_exists('api/resources/uploads/petimage/')) {
//                    mkdir('api/resources/uploads/petimage/');
//                }
//                $uploaddir = realpath('./api/resources/uploads/petimage/');
//
//
//                $fname=$petid.'_'.time().".".$ext;
//                $uploadfile = $uploaddir ."/". $fname;
//                if (move_uploaded_file($_FILES['profileimage']['tmp_name'], $uploadfile)) {
//
//                    $imgurl='resources/uploads/petimage/'.$fname;
//                    $datacc = array();
//                    $datacc['proilefpicture']=$imgurl;
//                    $this->petmodel->updatePet($datacc,$petid);
//                }

                /************************************* S3 Integration  *****************************/

                $uploaddir = 'resources/uploads/petimage/';
                $ext = end(explode('.', $_FILES['profileimage']['name']));
                $fname = $petid . '_' . time() . "." . $ext;
                $s3msg = aws_upload($uploaddir,$fname,$filetmpname);

                if($s3msg=='S3 upload successful'){
                    $imgurl = 'resources/uploads/petimage/' . $fname;
                    $datacc = array();
                    $datacc['proilefpicture']=$imgurl;
                    $this->petmodel->updatePet($datacc,$petid);
                }

                /************************************* End S3 Integration  *****************************/
            }

        }


        //update medication
        $medications = $this->post('medications');
        $updatedOn = $this->post('updatedon');
        if($this->post('medications')) {
            if (isset($medications)) {
                //delete old medications
                //$this->petmedicationmodel->deleteByPetID($petid);
                $dataStatus = array(
                    'status'=>0,
                    'updatedon'=>$updatedOn
                );
                $this->petmedicationmodel->updateByPetID($petid,$dataStatus);
                $medicationjson=$this->post('medications');
                if (isset($medicationjson)) {
                    $medobj = json_decode($medicationjson, true);
                    if (!empty($medobj)) {
                        foreach ($medobj['medication'] as $item) {
                            $datacc = array();
                            $datacc['petid'] = $petid;
                            $datacc['medicineid'] = $item['id'];
                            $datacc['frequency'] = $item['frequency'];
                            $datacc['dosage'] = $item['dosage'];
                            $medicationtime = "08:00";
                            $datacc['medicationtime'] = $medicationtime;
                            $datacc['updatedon'] = $updatedOn;
                            if (array_key_exists('timings', $item)) {
                                $datacc['medicationtimings'] = $item['timings'];
                            }

                            if (array_key_exists('unit', $item) && $item['unit']) {
                                $datacc['dosageunit'] = $item['unit'];
                            }

                            if (array_key_exists('remindersTime', $item)) {
                                $reminder = $item['remindersTime'];

                                // $medicationtime =   $reminder[0]['time'];
                                for ($i = 0; $i < count($reminder); $i++) {
                                    if ($i == 0) {
                                        $medicationtime = $reminder[$i]['time'];
                                    } else {
                                        $medicationtime = $medicationtime . "," . $reminder[$i]['time'];
                                    }
                                }

                                $datacc['medicationtimings'] = $medicationtime;

                            }

                            if (array_key_exists('timesOfFrequency', $item)) {
                                $datacc['frequencytimes'] = $item['timesOfFrequency'];
                            }

                            if(array_key_exists('petmedicationid',$item)){
                                $petmedicationid = $item['petmedicationid'];
                                $datacc['status'] = 1;
                                $this->petmedicationmodel->updatePetMedication($petmedicationid,$datacc);
                            }else{
                                $this->petmedicationmodel->addMedication($datacc);
                            }

                        }
                    }
                }
            }
        }

        //update vaccination
        if($this->post('vaccinations')) {
            //delete old vaccinations
            $this->petvaccinationmodel->deleteByPetID($petid);
            $vaccjson=$this->post('vaccinations');
            $vacobj = json_decode($vaccjson, true);
            foreach($vacobj['vaccination'] as $item)
            {
                $datacc=array();
                $datacc['petid']=$petid;
                $datacc['vaccineid']=$item['id'];
                $datacc['duedate']=$item['duedate'];
                $this->petvaccinationmodel->addVaccination($datacc);
            }

        }

        $petDetails = $petdata=$this->petmodel->getPetDetails($petid);

        $data['proilefpicture'] = $petDetails['proilefpicture'];
        $data['name'] = $petDetails['name'];

        $data['primaryfeed']=$this->petfeedmodel->getFeedDetails($petDetails['primaryfood']);
        $data['primarybreed']=$this->petbreedmodel-> getBreedDetails($petDetails['primarybreed']);
        $data['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petDetails['secondaryfood']);
        $data['secondarybreed']=$this->petbreedmodel-> getBreedDetails($petDetails['secondarybreed']);

        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data' => $data),200);
        //}

        //else {
        //$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        //}
    }

    private function _display($array, $key) {
        $result = '';
        foreach ($array as $i => $value) {
            if (is_array($value)) {
                display($value);
            } else {
                if ($i == $key) {
                    $result = $value;
                    break;
                }
            }
        }
    }

    function fetchPetFeedUnit_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        $result = $this->petmodel->getPetFeedUnit($petid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);


    }

    function getPetProfile_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');
        $result=array();
        $petdata=$this->petmodel->getPetDetails($petid);
        if(count($petdata)>0)
        {
            $petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
            $petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
            $petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
            $petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
            $petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
            $petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
        }
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$petdata), 200);
    }
    function getPetFeedBrands_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        $petdata=$this->petmodel->getPetFeedBrands($petid);
        if(count($petdata)>0)
        {
            $petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
            $petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
        }
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$petdata), 200);
    }


    ////////////// CHECK FOR ACTIVITY SPELLINGS. :)
    function setPetActivityGoals_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $points= $this->post('points');

        $datacc=array();
        $datacc['petid']=$petid;
        $datacc['points']=$points;
        $affrows=$this->petgoalsmodel->addActivityGoals($datacc,$petid);
        if($affrows > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }

        else {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }

    }


    function setPetWeightGoals_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $weight= $this->post('weight');

        $datacc=array();
        $datacc['petid']=$petid;
        $datacc['weight']=$weight;
        $affrows=$this->petgoalsmodel->addWeightGoals($datacc,$petid);
        if($affrows > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }

        else {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }

    }

    function setPetMedicalGoals_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $dosagehours= $this->post('dosagehours');

        $datacc=array();
        $datacc['petid']=$petid;
        $datacc['dosagehours']=$dosagehours;
        $affrows=$this->petgoalsmodel->addMedicalGoals($datacc,$petid);
        if($affrows > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }

        else {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }

    }



    function updatePetJournal_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        if($this->post('medicineid')) {
            $bool=true;
            $datacc=array();
            $datacc['petid']=$petid;
            $datacc['petmedicationid'] = $this->post('medicineid');
            if($this->input->post('dosage'))
            {
                $datacc['dosage']= $this->input->post('dosage');
            }
            else {
                $bool =FALSE;
            }
            if($this->input->post('unit'))
            {
                $datacc['dosageunit']= $this->input->post('unit');
            }
            else{
                $datacc['dosageunit']  = "";
            }
            if($this->input->post('medicationdatetime')) {
                $datacc['medicationdatetime']= $this->input->post('medicationdatetime');
            }
            else {
                $bool =FALSE;
            }
            if(!$bool)
            {
                $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
                return false;
            }

            $this->petlogmodel->addMedicationLog($datacc);
        }

        if($this->post('feedbrandid')) {
            $bool=true;
            $datacc=array();
            $datacc['petid']=$petid;
            $datacc['feedbrandid'] = $this->post('feedbrandid');
            if($this->input->post('feedamount')) {
                $datacc['amount']= $this->input->post('feedamount');
            }
            else {
                $bool =FALSE;
            }
            if($this->input->post('feeddatetime')) {
                $datacc['feeddatetime']= $this->input->post('feeddatetime');
            }
            else {
                $bool =FALSE;
            }
            if($this->input->post('unitoffeed')) {
                $datacc['unitoffeed']= $this->input->post('unitoffeed');
            }

            if(!$bool)
            {
                $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
                return false;
            }

            $this->petlogmodel->addFeedingLog($datacc);
        }

        if($this->post('measuredweight')) {
            $bool=true;
            $datacc=array();
            $datacc['petid']=$petid;
            $datacc['measuredwt'] = $this->post('measuredweight');
            if($this->input->post('weightdatetime')) {
                $datacc['wtdatetime']= $this->input->post('weightdatetime');
            }
            else {
                $bool =FALSE;
            }

            if(!$bool)
            {
                $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
                return false;
            }

            $this->petlogmodel->addWeightLog($datacc);

            $datacc = array();
            $datacc['currentweight'] = $this->post('measuredweight');
            $this->petmodel->updatePet($datacc,$petid);
        }

        if($this->post('yearlycheckupdate')) {
            $yearlycheckupdate = $this->post('yearlycheckupdate');
            $datacc=array();
            $datacc['yearlycheckupdate'] = $yearlycheckupdate;
            $this->petmodel->updatePet($datacc,$petid);

        }
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);

    }

    function addWeightChange_post()
    {

        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');
        $bool=true;
        if($this->post('measuredweight')) {
            $datacc['measuredwt'] = $this->post('measuredweight');
        }
        else {
            $bool =FALSE;
        }

        if($this->input->post('weightdatetime')) {
            $datacc['wtdatetime']= $this->input->post('weightdatetime');
        }
        else {
            $bool =FALSE;
        }

        if(!$bool)
        {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
            return false;
        }

        $datacc['petid']=$petid;

        $affrows=$this->petlogmodel->addWeightLog($datacc);
        $dataWeight = array();
        $dataWeight['currentweight'] = $this->post('measuredweight');
        $affrows = $this->petmodel->updatePet($dataWeight,$petid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
    }

    function getPetOwners_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->appusersmodel->getPetOwners($petid)), 200);
    }

    function getPetnDevices_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $result = $this->appusersmodel->getAllUserPets($userid);

        $devicedata = array();

        if($result!=0)
        {
            for($i=0;$i<count($result);$i++)
            {
                $petdetails = $this->petmodel->getPetDeviceID($result[$i]['petid']);

                $devicedata[$i]['name'] = $petdetails['name'];
                $devicedata[$i]['deviceid'] = $petdetails['misfitdeviceid'];
            }
        }
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$devicedata), 200);
    }

    function getAllVeterinarians_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->veterinarianmodel->getAllVeterinarians()), 200);
    }

    function getAllMedicines_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $AllMedications = $this->petmedicationmodel->getAllMedications();
        //print_r($AllMedications);
        $result=array();
        if(!empty($AllMedications)){
            $cnt=0;
            foreach ($AllMedications as $key => $value) {
                /*$medicinetradenames = $this->petmedicationmodel->medicinetradenames($value['medicineid']);
					$AllMedications[$key]['tradename'] = $medicinetradenames['tradename'];	//fullname
					$AllMedications[$key]['fullname'] = $value['name'].' ('.$medicinetradenames['tradename'].')';*/
                $medicineid=$value['medicineid'];
                $medicinetradenames=$this->petmedicationmodel->getmedicineAlltradenames($medicineid);

                for($i=0;$i<count($medicinetradenames);$i++)
                {
                    $result[$cnt]['medicineid']=$medicineid;
                    $result[$cnt]['name']=$value['name'];
                    $result[$cnt]['url']=$value['url'];
                    $result[$cnt]['tradename']=$medicinetradenames[$i]['tradename'];
                    $result[$cnt]['fullname']=$value['name'].' ('.$medicinetradenames[$i]['tradename'].')';
                    $cnt++;
                }

            }
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' => $result), 200);
    }

    function getMedicineDeatails_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $medicineid = $this->post('medicineid');

        $this->response(array('statuscode' => '200','statusmsg' => 'error' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petmedicationmodel->fetchMedicineDetails($medicineid)), 200);
    }

    function getAllVaccines_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petvaccinationmodel->getAllVaccinations()), 200);
    }


    function getVaccineDetails_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $vaccineid = $this->post('vaccineid');

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petvaccinationmodel->fetchVaccineDetails($vaccineid)), 200);
    }


    function getAllFeedBrands_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'error' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petfeedmodel->getAllFeedBrands()), 200);
    }

    function getAllBreeds_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petbreedmodel->getAllBreeds()), 200);
    }


    function getVendorsByCategory_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);
        if($selectedvendors!="")
        {
            $arrVendors = explode(',', $selectedvendors);

            $categories=array('walker','sitter','grooming','petstore','veterinarian','lostfoundservices');
            $result=array();
            for($i=0;$i<count($categories);$i++)
            {
                $categoryname=$categories[$i];
                $categoryvendors=$this->vendorsmodel->getVendorsByCategory($categoryname);

                for($j=0;$j<count($categoryvendors);$j++)
                {
                    $vendorid=$categoryvendors[$j]['vendorid'];
                    if (in_array($vendorid, $arrVendors))
                        $categoryvendors[$j]['isSelectedByUser']=1;
                    else
                        $categoryvendors[$j]['isSelectedByUser']=0;
                }
                $result[$categoryname]=$categoryvendors;
            }
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);
        }

        else {

            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR), 200);

        }
    }

    function setProfileSharePermissionsForVendor_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $vendorid = $this->post("vendorid");
        $isAuthorized = $this->post('isauthorized');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0)
        {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request

        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        //////////////////////////////////////////////////////////////
        ///////////////////////Share Profile ////////////////////////

        $appuserdata=$this->appusersmodel->fetchUserDetails($userid);

        $vendordata = $this->vendorsmodel->getVendorDetails($vendorid);

        $toemail=$vendordata['email'];
        $subject = "BabelBark User Profile Shared";
        // To send HTML mail, the Content-type header must be set.

        $sendmessage  = file_get_contents('resources/shareuserProfile_vendor.html');
        $sendmessage = str_replace('%userfname%', $appuserdata['firstname'], $sendmessage);
        $sendmessage = str_replace('%userlname%',$appuserdata['lastname'] , $sendmessage);
        $sendmessage  = str_replace('%vendorfname%',$vendordata['firstname'] , $sendmessage);
        $sendmessage  = str_replace('%vendorlname%',$vendordata['lastname'] , $sendmessage);


        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = SITE_MAIL;
        $config['smtp_pass'] = "doogyWOOGY!";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
        $this->email->to($toemail);
        $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
        $this->email->cc($appuserdata['email'],$appuserdata['firstname']);
        $this->email->subject($subject);

        $this->email->message($sendmessage);
        $result =$this->email->send();



        $this->appuservendorsmodel-> setAuthorizedVendorForUser($userid, $vendorid, $isAuthorized);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
    }

    function deleteSelectedVendor_Post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0)
        {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request

        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $vendorid = $this->post('vendorid');
        $this->appuservendorsmodel->deleteVendor($userid,$vendorid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
    }
    function updateSelectedVendors_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0)
        {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request

        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $vendorids = $this->post('vendorid');
        $vendoridsArr = explode(',', $vendorids);

        $newVendor = $vendorids;

        if(count($vendoridsArr)!=0)
        {
            $newVendor = $vendoridsArr[count($vendoridsArr)-1];
        }
        $datacc=array();
        $datacc['appuserid']=$userid;
        $datacc['vendorid']=$newVendor;
        $datacc['isauthorized'] = 0;
        $isExist = $this->appuservendorsmodel->checkVendorExist($userid,$newVendor);
        if($isExist>0){
            $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => VENDOREXIST),200);
        }else{
            $this->appuservendorsmodel-> updateVendors($datacc,$userid);
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }

    }
    function getSelectedVendorswithName_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $selectedvendors= $this->appuservendorsmodel->getVendorsForUserwithNames($userid);
        $result = "";

        /*$arrVendors = explode(',', $selectedvendors);



		$result=array();
		for($i=0;$i<count($arrVendors);$i++)
		{

			$result[$i]=$this->vendorsmodel->getVendorDetails($arrVendors[$i]);
		}*/
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$selectedvendors), 200);

    }
    function getSelectedVendorServices_post()
    {
        $vendorid = $this->post('vendorid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $selectedservices= $this->servicemodel->getAllServices(100,0,$vendorid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$selectedservices), 200);

    }

    function getSelectedVendors_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $selectedvendors= $this->appuservendorsmodel->getVendorsForUser($userid);
        $result = "";


        $i = 0;
        foreach ($selectedvendors as $row)
        {
            if($i == 0)
            {
                $result = $row['vendorid'];

            }else{
                $result =$result.",".$row['vendorid'];
            }
            $i = $i+1;

            //use the database table fields name in the place of field_name property
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);
    }

    function checkAppointmentCapacity_post(){
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $vendorid         = $this->post('vendorid');
        $serviceid        = $this->post('serviceid');
        $boardingfromdate = $this->post('boardingfromdate');
        $boardingfromtime = $this->post('boardingfromtime');
        $boardingtotime   = $this->post('boardingtotime');
        $capacity    = $this->post('capacity');
        $newBoardingtotime = date("H:i:s",strtotime("-1 minutes",strtotime($boardingtotime)));
        $appointmentInfo  = $this->appointmentmodel->getNoOfAppointment($vendorid,$serviceid,$boardingfromdate,$boardingfromtime,$newBoardingtotime);
        if(count($appointmentInfo)>0){
            for($i=0;$i<count($appointmentInfo);$i++){
                $noOfAppointment = $appointmentInfo[$i]['total_appointment'];
                if($noOfAppointment>=$capacity){
                    $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => "The maximum capacity for this service has been reached for this time slot."),200);
                    break;
                }
            }
        }
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => CAPACITYMSG,'data' => 'true'),200);
    }

    function saveAppointmentOfVendor_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');


        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid =$this->post('petid');

        $vendorid = $this->post('vendorid');

        $data = array();
        $data['petid'] = $petid;
        $data['vendorid'] = $vendorid;
        $data['bookingnote'] = $this->post('bookingnote');
        $data['estimatedduration'] = $this->post('estimatedduration');
        $data['serviceid'] = $this->post('serviceid');
        $data['boardingfromdate'] = $this->post('boardingfromdate');
        $data['boardingfromtime'] = $this->post('boardingfromtime');
        $data['boardingtodate'] = $this->post('boardingtodate');
        $data['boardingtotime'] = $this->post('boardingtotime');
        $data['appuserid'] = $userid;
        $data['pickdrop']  = '0';
        $data['reminderrequired'] = '1';
        $data['status'] = 1;

        ////////////////////////////////////////////////////////////////
        /////////////////////Email to Vendor /////////////////////////
        $appuserdata=$this->appusersmodel->fetchUserDetails($userid);

        $vendordata = $this->vendorsmodel->getVendorDetails($vendorid);

        $serviceData = $this->servicemodel->getServiceDetails($data['serviceid']);

        $appointmentId  = $this->appointmentmodel->addAppointment($data);

        if ($vendordata['category'] == 1) {
            $base_url = BABELVET_ADD;
        }else{
            $base_url = BIZBARK_ADD;
        }

        $toemail=$vendordata['email'];
        $subject = "Appointment Scheduled";
        // To send HTML mail, the Content-type header must be set.

        $sendmessage  = file_get_contents('resources/api_appointment_notification_template.html');
        $sendmessage = str_replace('%userfname%', $appuserdata['firstname'], $sendmessage);
        $sendmessage = str_replace('%userlname%',$appuserdata['lastname'] , $sendmessage);
        $sendmessage = str_replace('%servicename%',$serviceData['name'] , $sendmessage);
        $sendmessage = str_replace('%fromdate%',date("M d, Y", strtotime($this->post('boardingfromdate'))), $sendmessage);
        $sendmessage = str_replace('%fromtime%', date("h:i A", strtotime($this->post('boardingfromtime'))), $sendmessage);
        $sendmessage = str_replace('%totime%', date("h:i A", strtotime($this->post('boardingtotime'))), $sendmessage);
        $sendmessage = str_replace('%uploadlink%',"$base_url/vendor/login?id=$vendorid&appointment_no=$appointmentId" , $sendmessage);

        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = SITE_MAIL;
        $config['smtp_pass'] = "doogyWOOGY!";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
        $this->email->to($toemail);
        $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
        //$this->email->cc($appuserdata['email'],$appuserdata['firstname']);
        $this->email->subject($subject);
        $this->email->message($sendmessage);

        $result =$this->email->send();




        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);
    }




    function getPetMedication_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petmedicationmodel->getPetMedicationForPet($petid)), 200);

    }
    function getMultiplePetMedication_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $petid= $this->post('petid');

        $result = $this->appusersmodel->getAllUserPets($userid);
        $medications = array();

        if($petid)
        {
            if($result !== 0)
            {
                $petdata = array();
                for ($i=0; $i < count($result) ; $i++)
                {
                    $petid = $result[$i]['petid'];
                    $medicines  = $this->petmedicationmodel->getPetMedicationForPet($petid);
                    $petdetails = $this->petmodel->getPetDetails($petid);
                    $petname = $petdetails['name'];
                    $medications[$i]['petname'] = $petname;
                    $medications[$i]['medications'] = $medicines;
                }
            }
            else{
                $medicines  = $this->petmedicationmodel->getPetMedicationForPet($petid);
                $petdetails = $this->petmodel->getPetDetails($petid);
                $petname = $petdetails['name'];
                $medications[0]['petname'] = $petname;
                $medications[0]['medications'] = $medicines;
            }
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' => $medications), 200);
        }
        else{

            $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => "PET ID IS NULL"),400);
            return false;

        }

    }

    function gettempCategory_Post()
    {

        $selectedvendors=$this->appuservendorsmodel->getVendorsForUser('4014607249413705');

        $arrVendors = array();
        if($selectedvendors!="")
        {
            foreach ($selectedvendors as $row)
            {
                array_push($arrVendors, $row['vendorid']);
                //use the database table fields name in the place of field_name property
            }
        }
        $result = [];
        for($i=0;$i<count($arrVendors);$i++)
        {
            $vendorid=$arrVendors[$i];

            $category =  $this->vendorsmodel->getVendorCategories($vendorid);
            $arrCategory = explode(',', $category['category']);
            $arrcategories = $arrCategory;
            $result = array_merge($result,$arrCategory);


        }
        $result = array_unique($result);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);
    }
    function getVendorCategories_post()
    {
        $category =  $this->vendorsmodel->getVendorCategories('8888888888888888');
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$category['category']), 200);
    }

    function getPromotions_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $userdetails=$this->appusersmodel->fetchUserDetails($userid);
        $promotionsresult=array();
        $promotionsresult['isverified']=$userdetails['isverified'];

        $selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);

        $arrVendors = array();
        if($selectedvendors!="")
        {
            foreach ($selectedvendors as $row)
            {
                array_push($arrVendors, $row['vendorid']);
                //use the database table fields name in the place of field_name property
            }

            $petid=$this->appusersmodel->getPetID($userid);
            $petdata=$this->petmodel->getPetDetails($petid);
            $petage=$petdata['approximateage'];
            $petweight=$petdata['currentweight'];
            $petbreed=$petdata['primarybreed'];
            $isadopted = $petdata['isadopted'];
            $petacivityscore=$this->petlogmodel->getPetActivityPoints($petid);

            $promotionscnt=0;
            $ids = join("','",$arrVendors);
            $otherVendorPromotions = $this->promotionsmodel->getAllPromotionsForOtherVendors($ids,$isadopted);
            $arrcategories;
            $result = [];
            for($i=0; $i<count($arrVendors); $i++)
            {
                $vendorid=$arrVendors[$i];

                $category =  $this->vendorsmodel->getVendorCategories($vendorid);
                if(isset($category['category']))
                {
                    $arrCategory = explode(',', $category['category']);
                    $result = array_merge($result,$arrCategory);
                }
            }
            $result = array_unique($result);

            $data = [];
            for($j=0;$j<count($otherVendorPromotions);$j++)
            {
                $isOtherCategory = false;
                foreach ($result as $category)
                {
                    if (preg_match('/\b' . $category . '\b/', $otherVendorPromotions[$j]['category'])) {
                        $isOtherCategory = true;
                    }

                }
                if($isOtherCategory)
                {
                    array_push($data, $j);
                }
            }

            foreach($data as $index)
            {
                unset($otherVendorPromotions[$index]);
            }

            $blabla = [];
            for($i=0;$i<count($arrVendors);$i++)
            {
                $vendorid=$arrVendors[$i];
                $promotions=$this->promotionsmodel->getAllPromotionsForApp($vendorid,$isadopted);
                //array_push($blabla, $promotions);

                for($j=0;$j<count($promotions);$j++)
                {
                    $valid=true;

                    $capacity = $promotions[$j]['capacity'];
                    $redeemedcount = $promotions[$j]['redeemedcount'];
                    if($capacity>0)
                    {
                        if($redeemedcount >= $capacity)
                        {
                            $valid = false;
                        }
                    }

                    if($valid)
                    {
                        $promotionsresult[]=$promotions[$j];
                        $promotionscnt++;
                    }

                }
            }
            $promotions = $otherVendorPromotions;
            for($j=0;$j<count($promotions);$j++)
            {
                $valid=true;

                $capacity = $promotions[$j]['capacity'];
                $redeemedcount = $promotions[$j]['redeemedcount'];
                if($capacity>0)
                {
                    if($redeemedcount >= $capacity)
                    {
                        $valid = false;
                    }
                }

                if($valid)
                {
                    if(isset($promotions[$j]))
                    {
                        $promotionsresult[]=$promotions[$j];
                        $promotionscnt++;
                    }

                }

            }
            if($promotionscnt>0)
                $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$promotionsresult), 200);
            else
                $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$promotionsresult), 200);

        }
        else {

            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR,'data' =>$promotionsresult), 200);

        }

    }

    function updateAcitivityScore_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid = $this->post('petid');
        $actualpoints = $this->post('points');
        $syncdate= $this->post('syncdate');
        $syncdon = $this->post('syncdon');
        $totalsteps= $this->post('totalsteps');
        $variance= $this->post('variance');
        $datacc=array();
        $datacc['petid']=$petid;
        $datacc['actualpoints']=$actualpoints;
        $datacc['syncdate']=$syncdate;
        $datacc['syncdon']=$syncdon;
        $datacc['totalsteps']=$totalsteps;
        $datacc['variance']=$variance;
        $this->petlogmodel->addActivityLog($datacc);
        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);

    }

    function updateAcitivityScores_POST()
    {

        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        if($this->post('activity_log')) {
            $activity_log=$this->post('activity_log');
            $activity_log = json_decode($activity_log, true);
            foreach($activity_log['activity_log'] as $item)
            {
                $datacc=array();
                $datacc['petid']=$item['petid'];
                $datacc['actualpoints']=$item['points'];
                $datacc['syncdate']=$item['syncdate'];
                $datacc['syncdon']=$item['syncdon'];
                $datacc['totalsteps']=$item['totalsteps'];
                $datacc['variance']=$item['variance'];
                $this->petlogmodel->addActivityLog($datacc);

            }
        }

        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);

    }

    function updateMedicationStatistics_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $code= $this->post('code');
        $month= $this->post('month');
        $percentage= $this->post('percentage');
        $week= $this->post('week');

        $datacc=array();
        $datacc['petid']=$petid;
        $datacc['appuserid']=$userid;
        $datacc['code']=$code;
        $datacc['month']=$month;
        $datacc['percentage']=$percentage;
        $datacc['week']=$week;
        $affrows=$this->petmedicationmodel->updateMedicationStatistics($datacc);
        if($affrows > 0)
        {
            $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
        }

        else {
            $this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
        }

    }

    function syncPetLogs_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $petid= $this->post('petid');
        $resultarr=array();
        $resultarr['medicationstatistics']=$this->petmedicationmodel->getMedicationStatistics($petid);
        $resultarr['activitylog']=$this->petlogmodel->getActivityLog($petid);
        $resultarr['weightlog']=$this->petlogmodel->getWeightLog($petid);
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
    }

    function getVendorBySearch_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $categoryname= $this->post('category');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $searchName = $this->input->post('searchname');

        $categoryID = "7";
        if($categoryname=="Lost & Found Service")
        {
            $categoryname="lostfoundservices";

        }
        if($categoryname == "lostfoundservices")
        {
            $categoryID="7";
        }
        else if($categoryname=="walker")
        {
            $categoryID="6";
        }
        else if($categoryname=="sitter")
        {
            $categoryID="3";
        }
        else if($categoryname=="grooming")
        {
            $categoryID="4";
        }
        else if($categoryname=="petstore")
        {
            $categoryID="2";
        }
        else if($categoryname=="veterinarian")
        {
            $categoryID="1";
        }
        else if($categoryname=="trainer")
        {
            $categoryID = '5';
        }

        $registeredvendorstemp=$this->vendorsmodel->getVendorByNameAndId($categoryID,$searchName);
        $registeredvendors=array();
        for($i=0;$i<count($registeredvendorstemp);$i++)
        {
            //	$formatted_address=$registeredvendorstemp[$i]['address'].",".$registeredvendorstemp[$i]['city'].",".$registeredvendorstemp[$i]['state'].",".$registeredvendorstemp[$i]['country'].",".$registeredvendorstemp[$i]['zipcode'];
            $formatted_address=$registeredvendorstemp[$i]['address'];
            if($formatted_address!="")
                $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['city'];
            if($formatted_address!="")
                $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['state'];
            if($formatted_address!="")
                $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['country'];
            if($formatted_address!="")
                $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['zipcode'];

            if($formatted_address=="")
                $formatted_address="-";

            $registeredvendors[$i]['id']=$registeredvendorstemp[$i]['vendorid'];
            $registeredvendors[$i]['companyname']=$registeredvendorstemp[$i]['comapnyname'];	// zaim
            $registeredvendors[$i]['name']=$registeredvendorstemp[$i]['firstname']." ".$registeredvendorstemp[$i]['lastname'];
            $registeredvendors[$i]['address']=$formatted_address;
            $registeredvendors[$i]['phoneno']=$registeredvendorstemp[$i]['contact'];
            $registeredvendors[$i]['types']=array($registeredvendorstemp[$i]['category']);
            $registeredvendors[$i]['email']=$registeredvendorstemp[$i]['email'];
            $registeredvendors[$i]['url']="";
            $registeredvendors[$i]['website']=$registeredvendorstemp[$i]['website'];
            $registeredvendors[$i]['openinghours']="";
            $registeredvendors[$i]['rating']="";
            $registeredvendors[$i]['isregistered']=1;
        }


        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis"; //map api key


        $latlng = $latitude.",".$longitude;

        $srchtype="petstore";

        $keyword="pet_store";
        if($categoryname=="walker")
        {
            $keyword="pet walker";
        }
        else if($categoryname=="sitter")
        {
            $keyword="pet sitting";
        }
        else if($categoryname=="grooming")
        {
            $keyword="pet grooming";
        }
        else if($categoryname=="petstore")
        {
            $keyword="pet_store";
        }
        else if($categoryname=="veterinarian")
        {
            $keyword="veterinarian";
        }
        else if($categoryname=="lostfoundservices" || $categoryname=="Lost & Found Service")
        {
            $keyword="pet lost and found services";
        }
        else if($categoryname=="trainer")
        {
            $keyword = "dog trainer";
        }

        $placedetailurl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".urlencode($searchName)."&key=".$mapapikey;
        //$placedetailurl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$latlng."&keyword=".urlencode($keyword)."&name=".$searchName."&radius=50000&sensor=false&key=".$mapapikey;

        $detailjson = file_get_contents($placedetailurl);
        $nearbyvendorsdata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);


        $serchdetailarr=array();
        $serchdetailarr=$nearbyvendorsdata['results'];

        $searchedvendors=array();
        for($i=0;$i<count($serchdetailarr);$i++)
        {
            $placeid=$serchdetailarr[$i]['place_id'];
            $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
            $detailjson = file_get_contents($placedetailurl);
            $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

            $formatted_address=$placedata['result']['formatted_address'];
            $searchedvendors[$i]['id']="";
            $searchedvendors[$i]['name']=$placedata['result']['name'];
            $searchedvendors[$i]['address']=$formatted_address;
            if( isset( $placedata['result']['international_phone_number'] ))
                $searchedvendors[$i]['phoneno']=$placedata['result']['international_phone_number'];
            else
                $searchedvendors[$i]['phoneno']="";

            $searchedvendors[$i]['types']=$placedata['result']['types'];
            $searchedvendors[$i]['email']="";
            if( isset( $placedata['result']['url'] ))
                $searchedvendors[$i]['url']=$placedata['result']['url'];
            else
                $searchedvendors[$i]['url']="";

            if( isset( $placedata['result']['website'] ))
                $searchedvendors[$i]['website']=$placedata['result']['website'];
            else
                $searchedvendors[$i]['website']="";

            if( isset( $placedata['result']['opening_hours'] ))
            {
                if($placedata['result']['opening_hours']['open_now'])
                {
                    $searchedvendors[$i]['openinghours'] = "Open Today";
                }else{
                    $searchedvendors[$i]['openinghours'] = "Closed";
                }
            }
            else
                $searchedvendors[$i]['openinghours']="";

            if( isset($placedata['result']['rating']))
                $searchedvendors[$i]['rating']=$placedata['result']['rating']."";
            else
                $searchedvendors[$i]['rating']="";

            $searchedvendors[$i]['isregistered']=0;
            $searchedvendors[$i]['placeid'] = $placeid;
        }
        //$resultarr=array_merge($registeredvendors,$searchedvendors);
        if($categoryname=="lostfoundservices" || $categoryname=="Lost & Found Service")
        {
            $resultarr=$registeredvendors;
        }else{
            $resultarr=array_merge($registeredvendors,$searchedvendors);
        }

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);


    }




    function getNearestVendors_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $categoryname= $this->post('category');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');


        $categoryID = '7';
        if($categoryname=="Lost & Found Service")
        {
            $categoryname="lostfoundservices";

        }
        if($categoryname == "lostfoundservices")
        {
            $categoryID='7';
        }
        else if($categoryname=="walker")
        {
            $categoryID="6";
        }
        else if($categoryname=="sitter")
        {
            $categoryID="3";
        }
        else if($categoryname=="grooming")
        {
            $categoryID="4";
        }
        else if($categoryname=="petstore")
        {
            $categoryID="2";
        }
        else if($categoryname=="veterinarian")
        {
            $categoryID="1";
        }
        else if($categoryname=="trainer")
        {
            $categoryID = '5';
        }

        $registeredvendorstemp=$this->vendorsmodel->getVendorById($categoryID);


        $registeredvendors=array();
        for($i=0;$i<count($registeredvendorstemp);$i++)
        {


            $distance = 100000;
            if($categoryname != 'lostfoundservices')
            {
                $arrlatlng = array();
                if( $registeredvendorstemp[$i]['addresslatlng']!="")
                {
                    $arrlatlng = explode(',', $registeredvendorstemp[$i]['addresslatlng']);
                }

                if(is_array($arrlatlng))
                {
                    if(isset($arrlatlng[0]))
                    {

                        $distance = $this->vincentyGreatCircleDistance($latitude, $longitude, $arrlatlng[0], $arrlatlng[1],6371000);
                    }
                }
            }
            else
            {
                $distance = 10;
            }




            if($distance < 50000)
            {
                $formatted_address=$registeredvendorstemp[$i]['address'];
                if($formatted_address!="")
                    $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['city'];
                if($formatted_address!="")
                    $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['state'];
                if($formatted_address!="")
                    $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['country'];
                if($formatted_address!="")
                    $formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['zipcode'];

                if($formatted_address=="")
                    $formatted_address="-";

                if($formatted_address!="-" || $categoryname == 'lostfoundservices')
                {

                    $registeredvendors[$i]['id']=$registeredvendorstemp[$i]['vendorid'];
                    $registeredvendors[$i]['companyname']=$registeredvendorstemp[$i]['comapnyname'];	// zaim
                    $registeredvendors[$i]['name']=$registeredvendorstemp[$i]['firstname']." ".$registeredvendorstemp[$i]['lastname'];

                    $registeredvendors[$i]['address']=$formatted_address;
                    $registeredvendors[$i]['phoneno']=$registeredvendorstemp[$i]['contact'];
                    $registeredvendors[$i]['types']=array($registeredvendorstemp[$i]['category']);
                    $registeredvendors[$i]['email']=$registeredvendorstemp[$i]['email'];
                    $registeredvendors[$i]['url']="";
                    $registeredvendors[$i]['website']=$registeredvendorstemp[$i]['website'];
                    $registeredvendors[$i]['openinghours']="";
                    $registeredvendors[$i]['rating']="";
                    $registeredvendors[$i]['isregistered']=1;

                }
                //$registeredvendors[$i]['distance'] = $distance;

            }


            //	$formatted_address=$registeredvendorstemp[$i]['address'].",".$registeredvendorstemp[$i]['city'].",".$registeredvendorstemp[$i]['state'].",".$registeredvendorstemp[$i]['country'].",".$registeredvendorstemp[$i]['zipcode'];

        }


        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis"; //map api key


        $latlng = $latitude.",".$longitude;

        $srchtype="petstore";

        $keyword="pet_store";
        if($categoryname=="walker")
        {
            $keyword="pet walker";
        }
        else if($categoryname=="sitter")
        {
            $keyword="pet sitting";
        }
        else if($categoryname=="grooming")
        {
            $keyword="pet grooming";
        }
        else if($categoryname=="petstore")
        {
            $keyword="pet_store";
        }
        else if($categoryname=="veterinarian")
        {
            $keyword="veterinarian";
        }
        else if($categoryname=="lostfoundservices" || $categoryname=="Lost & Found Service")
        {
            $keyword="pet lost and found services";
        }
        else if($categoryname=="trainer")
        {
            $keyword = "dog trainer";
        }
        $placedetailurl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$latlng."&keyword=".urlencode($keyword)."&rankby=distance&sensor=false&key=".$mapapikey;

        $detailjson = file_get_contents($placedetailurl);
        $nearbyvendorsdata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);


        $serchdetailarr=array();
        $serchdetailarr=$nearbyvendorsdata['results'];

        $searchedvendors=array();
        for($i=0;$i<count($serchdetailarr);$i++)
        {
            $placeid=$serchdetailarr[$i]['place_id'];
            $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
            $detailjson = file_get_contents($placedetailurl);
            $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

            $formatted_address=$placedata['result']['formatted_address'];
            $searchedvendors[$i]['id']="";
            $searchedvendors[$i]['name']=$placedata['result']['name'];
            $searchedvendors[$i]['address']=$formatted_address;
            if( isset( $placedata['result']['international_phone_number'] ))
                $searchedvendors[$i]['phoneno']=$placedata['result']['international_phone_number'];
            else
                $searchedvendors[$i]['phoneno']="";

            $searchedvendors[$i]['types']=$placedata['result']['types'];
            $searchedvendors[$i]['email']="";
            if( isset( $placedata['result']['url'] ))
                $searchedvendors[$i]['url']=$placedata['result']['url'];
            else
                $searchedvendors[$i]['url']="";

            if( isset( $placedata['result']['website'] ))
                $searchedvendors[$i]['website']=$placedata['result']['website'];
            else
                $searchedvendors[$i]['website']="";

            if( isset( $placedata['result']['opening_hours'] ))
            {
                if($placedata['result']['opening_hours']['open_now'])
                {
                    $searchedvendors[$i]['openinghours'] = "Open Today";
                }else{
                    $searchedvendors[$i]['openinghours'] = "Closed";
                }
            }
            else
                $searchedvendors[$i]['openinghours']="";

            if( isset($placedata['result']['rating']))
                $searchedvendors[$i]['rating']=$placedata['result']['rating']."";
            else
                $searchedvendors[$i]['rating']="";

            $searchedvendors[$i]['isregistered']=0;
            $searchedvendors[$i]['placeid'] = $placeid;
        }
//        $resultarr=array_merge($registeredvendors,$searchedvendors);
        if($categoryname=="lostfoundservices" || $categoryname=="Lost & Found Service")
        {
            $resultarr=$registeredvendors;
        }else{
            $resultarr=array_merge($registeredvendors,$searchedvendors);
        }


        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);

    }

    private static function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians

        $latFrom = deg2rad(floatval($latitudeFrom));
        $lonFrom = deg2rad(floatval($longitudeFrom));
        $latTo = deg2rad(floatval($latitudeTo));
        $lonTo = deg2rad(floatval($longitudeTo));

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
    function getSomething_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid = $this->post('petid');
        /*if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
		return false;
      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}*/


        ///fetching Appointments

        $retarr = $this->appointmentmodel->getAllAppointments_User($petid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$retarr), 200);


    }
    function getAllCurrentSchedules_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid = $this->post('petid');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        ///fetching Appointments

        $retarr = $this->appointmentmodel->getAllAppointments_User($petid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$retarr), 200);


    }

    function getUpComingAppointments_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid = $this->post('petid');
        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $retarr = $this->appointmentmodel->getUpcomingAppointments($petid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$retarr), 200);
    }

    function getRatingOfPlace_Post()
    {

        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis";
        $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJQYmn-vMDGTkRLnweR0AEIbM&key=".$mapapikey;
        $data = [];
        $detailjson = file_get_contents($placedetailurl);
        $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

        if( isset( $placedata['result']['opening_hours'] ))
            $data[0]=$placedata['result']['opening_hours']['open_now'];
        else
            $data[0]="";

        if( isset( $placedata['result']['rating']))
        {
            $data[1]=$placedata['result']['rating'];
        }
        else
            $data[1]="";
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$data), 200);
    }



    function getAllSelectedVendors_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $categories=array('veterinarian','petstore','sitter','grooming','trainer','walker','lostfoundservices','shelters');
        // $categoriesNumbers=array('6','3','4','2','1','7','5','8');
        $selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);

        $result = "";

        $arrVendors = array();
        foreach ($selectedvendors as $row)
        {
            array_push($arrVendors, $row['vendorid']);

            //use the database table fields name in the place of field_name property
        }

        // while ($row = mysql_fetch_array($selectedvendors, MYSQL_ASSOC))
        // {
        // 	$id = explode(',', $row["vendorid"]);
        // 	array_push($arrVendors, $id[0]);
        // }


        // query
        $vendoridsauthorized = array();
        $query = $this->appuservendorsmodel->getAuthorizedVendorsForUser($userid);
        // while ($row = mysql_fetch_array($query, MYSQL_ASSOC))
        foreach ($query as $row)
        {
            array_push($vendoridsauthorized, $row["vendorid"]);
            // printf("ID: %s  Name: %s", $row["id"], $row["name"]);
        }

        $petid = $this->appusersmodel->getPetID($userid);
        $vendorLogoImagePath = base_url().'view_image/';

        for($i=0;$i<count($categories);$i++)
        {
            // $categoryname=$categories[$i];
            // $categoriesNumbers =
            $categoryvendors=$this->vendorsmodel->getVendorById($i+1);
            $regivendroeresult=array();
            $cnt=0;
            for($j=0;$j<count($categoryvendors);$j++)
            {
                $vendorid=$categoryvendors[$j]['vendorid'];
                if (in_array($vendorid, $arrVendors))
                {
                    $regivendroeresult[$cnt]['id']=$categoryvendors[$j]['vendorid'];
                    $regivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
                    $regivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
                    $regivendroeresult[$cnt]['email']=$categoryvendors[$j]['email'];
                    $formatted_address=$categoryvendors[$j]['address']." ".$categoryvendors[$j]['city'].", ".$categoryvendors[$j]['state'].", ".$categoryvendors[$j]['zipcode'].", ".$categoryvendors[$j]['country'];
                    $regivendroeresult[$cnt]['address']=$formatted_address;
                    $regivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['contact'];
                    $regivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
                    $regivendroeresult[$cnt]['url']="";
                    $regivendroeresult[$cnt]['isregistered']=1;
                    $regivendroeresult[$cnt]['logoimage']=$vendorLogoImagePath.str_replace('/','-',$categoryvendors[$j]['logoimage']);
                    $regivendroeresult[$cnt]['companyname']=$categoryvendors[$j]['comapnyname'];
                    $regivendroeresult[$cnt]['addresslatlng']=$categoryvendors[$j]['addresslatlng'];


                    if(is_null($categoryvendors[$j]['sikkaoffice_id']) || is_null($categoryvendors[$j]['sikkapractice_id']))
                    {
                        $regivendroeresult[$cnt]['issikkavendor'] = 0;
                    }
                    else
                    {
                        $regivendroeresult[$cnt]['issikkavendor'] = 1;
                    }

                    if (in_array($vendorid, $vendoridsauthorized))
                    {
                        $regivendroeresult[$cnt]['isauthorized']=1;
                    }

                    $lastsync = $this->sikkapetimportrequestsmodel->getupdatedOn($categoryvendors[$j]['vendorid'],$petid);
                    $regivendroeresult[$cnt]['lastsync']=$lastsync;

                    $cnt++;
                }


            }
            $result[$categories[$i]]=$regivendroeresult;
        }

        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis"; //map api key
        $result1=array();
        $arrVendors =array();
        $selectednonregivendors=$this->nonregistervendormodule->getNonregiVendorsForUser($userid);


        for($i=0;$i<count($selectednonregivendors);$i++)
        {
            $arrVendors[$i]=$selectednonregivendors[$i]['nonregisteredvendorid'];
        }

        //$arrVendors = explode(',', $selectednonregivendors);

        for($i=0;$i<count($categories);$i++)
        {
            $categoryname=$categories[$i];
            $categoryvendors=$this->nonregistervendormodule->getNonregiVendorsByCategory($categoryname);

            $nonregivendroeresult=array();
            $cnt=0;
            for($j=0;$j<count($categoryvendors);$j++)
            {
                $vendorid=$categoryvendors[$j]['nonregisteredvendorid'];

                if (in_array($vendorid, $arrVendors))
                {
                    $nonregivendroeresult[$cnt]['id']=$categoryvendors[$j]['nonregisteredvendorid'];
                    $nonregivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
                    $nonregivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
                    $nonregivendroeresult[$cnt]['email']="";
                    $formatted_address=$categoryvendors[$j]['streetaddress'];
                    $nonregivendroeresult[$cnt]['address']=$formatted_address;
                    $nonregivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['phone'];
                    $nonregivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
                    $nonregivendroeresult[$cnt]['url']=$categoryvendors[$j]['url'];
                    $nonregivendroeresult[$cnt]['isregistered']=0;
                    $nonregivendroeresult[$cnt]['logoimage']="";
                    $nonregivendroeresult[$cnt]['isauthorized']=0;
                    ////////////////// Getting data from placeid
                    $placeid = $categoryvendors[$j]['placeid'];

                    $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
                    $detailjson = file_get_contents($placedetailurl);
                    $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

                    if( isset( $placedata['result']['opening_hours'] ))
                    {
                        if($placedata['result']['opening_hours']['open_now'])
                        {
                            $nonregivendroeresult[$cnt]['openinghours'] = "Open Today";
                        }else{
                            $nonregivendroeresult[$cnt]['openinghours'] = "Closed";
                        }
                    }
                    else
                        $nonregivendroeresult[$cnt]['openinghours']="";

                    if( isset( $placedata['result']['rating']))
                        $nonregivendroeresult[$cnt]['rating']=$placedata['result']['rating']."";
                    else
                        $nonregivendroeresult[$cnt]['rating']="";

                    $cnt++;
                }


            }
            $result1[$categoryname]=$nonregivendroeresult;
        }

        $resultarr=array_merge_recursive($result, $result1);
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
    }


    function sikkaAppUserDataImport_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $petid = $this->post('petid');
        $vendorid = $this->post('vendorid');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }


        $resultdata = $this->appusersmodel->getuserEmail($userid);

        $useremail = $resultdata['email'];

        $petname = $this->petmodel->getPetName($petid);


        $customerid = $this->customermodel->getCustomerid_Email($useremail,$vendorid);

        $vendorInfo = $this->vendorsmodel->getVendorDetails($vendorid);
        if($customerid>0)
        {

            $listofsikkapets = $this->customermodel->getCustomerSikkaPets($customerid,$vendorInfo['sikkapractice_id']);

            $firstname = $resultdata['firstname'];

            $dom = new DOMDocument('1.0');

            $ul = $dom->createElement('ul');

            $dom->appendChild($ul);

            $token = $this->genrateSessionToken();

            $data['apppetid'] = $petid;
            $data['token']= $token;
            $data['sikkapetid']="";

            /////Store vendor id
            $data['vendorid'] = $vendorid;
            $id = $this->sikkapetimportrequestsmodel->insertSikkaImportRequest($data); /// id is inserted row ID

            for($i=0;$i<count($listofsikkapets);$i++)
            {
                $li = $dom->createElement('li');
                $e = $dom->createElement('a',$listofsikkapets[$i]['name']);
                $a = $li->appendChild($e);
                $url = BABELVET_ADD . '/importSikkaPet?token='.$token.'&sikkapetid='.$listofsikkapets[$i]['petid'].'&id='.$id;
                $a->setAttribute('href',$url);
                $ul->appendChild($li);
            }
            $dom->appendChild($ul);
            $pets = $dom->savehtml();

            $subject = "Medical Records Requested";
            $toemail = $useremail;

            // To send HTML mail, the Content-type header must be set.
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
            $headers .= "From: BizBark <" . SITE_MAIL_SUPPORT_BIZBARK. ">\r\n";
            $headers .= "Reply-To: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .= "Return-Path: BizBark <".SITE_MAIL_SUPPORT_BIZBARK.">\r\n";
            $headers .=  "CC: BabelBark User <" .$useremail.">\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
            // Sender's Email
            $sendmessage  = file_get_contents('resources/requestmedicalrecords.html');
            $sendmessage = str_replace('%firstname%', $firstname, $sendmessage);
            $sendmessage = str_replace('%petname%', $petname['name'], $sendmessage);
            $sendmessage = str_replace('%pets%', $pets, $sendmessage);

            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = SITE_MAIL;
            $config['smtp_pass'] = "doogyWOOGY!";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->to($toemail);
            $this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
            $this->email->cc($useremail);
            $this->email->subject($subject);
            $this->email->message($sendmessage);

            $result =$this->email->send();

            if(!$result)
            {
                $result = $this->email->print_debugger();
            }

            $senddata = array();
            $senddata['useremail'] = $useremail;
            $senddata['isemailsent'] = 1;

            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$senddata), 200);
        }
        else
        {
            $senddata = array();
            $senddata['useremail'] = $useremail;
            $senddata['isemailsent'] = 0;
            $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$senddata), 200);
        }


    }

    function getAllSelectedVets_post()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }
        $babelbarkCategory = 1;
        if($this->post('category'))
        {
            $specificCategory = $this->post('category');
            if($specificCategory != 'veterinarian')
            {
                $babelbarkCategory = 7;
            }

            $categories=array($specificCategory);
        }
        else{
            $categories = array('veterinarian');
        }

        // $categoriesNumbers=array('6','3','4','2','1','7','5');
        $selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);

        $result = "";

        $arrVendors = array();
        foreach ($selectedvendors as $row)
        {
            array_push($arrVendors, $row['vendorid']);

            //use the database table fields name in the place of field_name property
        }

        // query
        $vendoridsauthorized = array();
        $query = $this->appuservendorsmodel->getAuthorizedVendorsForUser($userid);
        // while ($row = mysql_fetch_array($query, MYSQL_ASSOC))
        foreach ($query as $row)
        {
            array_push($vendoridsauthorized, $row["vendorid"]);
            // printf("ID: %s  Name: %s", $row["id"], $row["name"]);
        }
        $filePath = base_url().'view_image/';
        for($i=0;$i<count($categories);$i++)
        {
            // $categoryname=$categories[$i];
            // $categoriesNumbers =
            $categoryvendors=$this->vendorsmodel->getVendorById($babelbarkCategory);
            $regivendroeresult=array();
            $cnt=0;
            for($j=0;$j<count($categoryvendors);$j++)
            {
                $vendorid=$categoryvendors[$j]['vendorid'];
                if (in_array($vendorid, $arrVendors))
                {
                    $regivendroeresult[$cnt]['id']=$categoryvendors[$j]['vendorid'];
                    $regivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
                    $regivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
                    $regivendroeresult[$cnt]['email']=$categoryvendors[$j]['email'];
                    $formatted_address=$categoryvendors[$j]['address'].",".$categoryvendors[$j]['city'].",".$categoryvendors[$j]['state'].",".$categoryvendors[$j]['country'].",".$categoryvendors[$j]['zipcode'];
                    $regivendroeresult[$cnt]['address']=$formatted_address;
                    $regivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['contact'];
                    $regivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
                    $regivendroeresult[$cnt]['url']="";
                    $regivendroeresult[$cnt]['isregistered']=1;
                    $regivendroeresult[$cnt]['logoimage']=$filePath.str_replace('/','-',$categoryvendors[$j]['logoimage']);
                    $regivendroeresult[$cnt]['companyname']=$categoryvendors[$j]['comapnyname'];


                    if (in_array($vendorid, $vendoridsauthorized))
                    {
                        $regivendroeresult[$cnt]['isauthorized']=1;
                    }

                    $cnt++;
                }


            }
            $result[$categories[$i]]=$regivendroeresult;
        }

        $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis"; //map api key
        $result1=array();
        $arrVendors =array();
        $selectednonregivendors=$this->nonregistervendormodule->getNonregiVendorsForUser($userid);
        for($i=0;$i<count($selectednonregivendors);$i++)
        {
            $arrVendors[$i]=$selectednonregivendors[$i]['nonregisteredvendorid'];
        }

        for($i=0;$i<count($categories);$i++)
        {
            $categoryname=$categories[$i];
            $categoryvendors=$this->nonregistervendormodule->getNonregiVendorsByCategory($categoryname);

            $nonregivendroeresult=array();
            $cnt=0;
            for($j=0;$j<count($categoryvendors);$j++)
            {
                $vendorid=$categoryvendors[$j]['nonregisteredvendorid'];
                if (in_array($vendorid, $arrVendors))
                {
                    $nonregivendroeresult[$cnt]['id']=$categoryvendors[$j]['nonregisteredvendorid'];
                    $nonregivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
                    $nonregivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
                    $nonregivendroeresult[$cnt]['email']="";
                    $formatted_address=$categoryvendors[$j]['streetaddress'].",".$categoryvendors[$j]['city'].",".$categoryvendors[$j]['state'].",".$categoryvendors[$j]['zip'];
                    $nonregivendroeresult[$cnt]['address']=$formatted_address;
                    $nonregivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['phone'];
                    $nonregivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
                    $nonregivendroeresult[$cnt]['url']=$categoryvendors[$j]['url'];
                    $nonregivendroeresult[$cnt]['isregistered']=0;
                    $nonregivendroeresult[$cnt]['logoimage']="";
                    $nonregivendroeresult[$cnt]['isauthorized']=0;
                    ////////////////// Getting data from placeid
                    $placeid = $categoryvendors[$j]['placeid'];

                    $placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
                    $detailjson = file_get_contents($placedetailurl);
                    $placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);

                    if( isset( $placedata['result']['opening_hours'] ))
                    {
                        if($placedata['result']['opening_hours']['open_now'])
                        {
                            $nonregivendroeresult[$cnt]['openinghours'] = "Open Today";
                        }else{
                            $nonregivendroeresult[$cnt]['openinghours'] = "Closed";
                        }
                    }
                    else
                        $nonregivendroeresult[$cnt]['openinghours']="";

                    if( isset( $placedata['result']['rating']))
                        $nonregivendroeresult[$cnt]['rating']=$placedata['result']['rating']."";
                    else
                        $nonregivendroeresult[$cnt]['rating']="";

                    $cnt++;
                }


            }
            $result1[$categoryname]=$nonregivendroeresult;
        }

        $resultarr=array_merge_recursive($result, $result1);
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
    }


    function test_get()
    {

        $medicationjson='{
    "medication": [
      {"id": "1", "frequency": "2"},
{"id": "2", "frequency": "1"},
{"id": "3", "frequency": "4"}
]

}';
        $medobj = json_decode($medicationjson, true);
        foreach($medobj['medication'] as $item)
        {
            echo $item['id'].":".$item['frequency'];

        }
    }

    function userRequestAuth($userid,$sessiontoken)
    {
        $retarr=array();
        //authenticate the session token
        $authresult=  $this->appusersmodel->requestAuth($userid,$sessiontoken);
        if($authresult=="expired")
        {
            $retarr['error'] =SESSIONEXPRDMSG;
        }
        else
        {
            $retarr=$authresult;
        }
        return $retarr;
    }

    function forgotPassword_POST(){
        $email = $this->post('email');

        if($email == ''){
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
            return false;
        }

        $emailExistCheck = $this->appusersmodel->isEmailExists($email);

        if($emailExistCheck){
            $appUserId = $this->appusersmodel->fetchUserID($email);

            //Generate reset password token and save to appuser table
            $resetpasswordtoken = $this->genrateSessionToken();
            $updateData['resetpasswordtoken'] = $resetpasswordtoken;
            if($this->appusersmodel->updateUser($updateData,$appUserId)){
                //send reset password link via email
                $this->mailnotifymodel->sendResetPasswordLink($appUserId,$resetpasswordtoken);
                $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => 'success'), 200);

            } else {
                $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => GENERALERRMSG), 200);
                return false;
            }
        } else {
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => EMAILNOTEXIST), 200);
            return false;
        }

    }
    function invitevendor_POST()
    {
        $email = $this->post('email');
        $appUserId = $this->post('userid');
        if($email == ''){
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
            return false;
        }

        if($appUserId == ''){
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
            return false;
        }

        $this->mailnotifymodel->inviteuser($appUserId,$email);
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => EMAILSENT), 200);

    }

    function inviteuser_POST(){
        $email = $this->post('email');
        $appUserId = $this->post('userid');
        if($email == ''){
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
            return false;
        }

        if($appUserId == ''){
            $this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
            return false;
        }

        $this->mailnotifymodel->inviteuser($appUserId,$email);
        $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => EMAILSENT), 200);

    }

    function addrNonRegisterVendor_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }

        $firstname 	= $this->post('firstname');
        $lastname = $this->post('lastname');
        $distance = $this->post('distance');
        $phone = $this->post('phone');
        $streetaddress = $this->post('streetaddress');
        $city = $this->post('city');
        $state = $this->post('state');
        $zip = $this->post('zip');
        $url = $this->post('url');
        $category = $this->post('category');
        $placeid = $this->post('placeid');

        $nonregisteredvendorid = $this->genratePrimaryKey('40');

        $insertArray = array(
            'nonregisteredvendorid' => $nonregisteredvendorid,
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'distance'  => $distance,
            'phone'		=> $phone,
            'streetaddress' => $streetaddress,
            'city'		=> $city,
            'state'		=> $state,
            'zip'		=> $zip,
            'url'		=> $url,
            'category'	=> $category,
            'placeid' => $placeid
        );
        $isExist = $this->appuservendorsmodel->checkVendorExist($userid,$nonregisteredvendorid);
        if($isExist>0){
            $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => VENDOREXIST),200);
        }else{
            if($this->nonregistervendormodule->insertNonRegisterVendor($insertArray))
            {
                $appUserInsertArray = array(
                    'appuserid' => $userid,
                    'nonregisteredvendorid' => $nonregisteredvendorid,
                );
                if($this->nonregistervendormodule->insertAppuserNonRegVendors($appUserInsertArray)){
                    $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => VENDORREGSUCCMSG,'data' =>$appUserInsertArray), 200);
                    return false;
                }
            } else {
                $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => GENERALERRMSG), 200);
                return false;
            }
        }

    }


    function removeNonRegisterVendor_POST()
    {
        $userid = $this->post('userid');
        $deviceid = $this->post('deviceid');
        $sessiontoken=$this->post('sessiontoken');
        $nonregisteredvendorid=$this->post('vendorid');

        if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
            $this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
            return false;
        }

        $reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
        if(array_key_exists("error", $reqauthresult))
        {
            $this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
            return false;
        }



        $this->nonregistervendormodule->removeNonregivendorsForUser($userid,$nonregisteredvendorid);

        $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
    }

}





?>
