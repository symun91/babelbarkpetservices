<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @property invitationtokenmodel  $invitationtokenmodel Description
 */

class Acceptinvitation extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('appusersmodel','invitationtokenmodel','appuservendorsmodel','vendorsmodel'));
		
	}

	function index()
	{
		
		
		if(!empty($_GET)){
			
			
			$token = $this->input->get('token');
			$invitataiontoken=$this->invitationtokenmodel->getInvitationToken($token);
			if($invitataiontoken == 1)
			{
				$invitataiondetails=$this->invitationtokenmodel->getInvitationByToken($token);
		    	$appuserid=$invitataiondetails['appuserid'];
		    	$fromvendorid=$invitataiondetails['fromvendorid'];
		    //add vendor as selected
		   /* $existingvendroids=$this->appuservendorsmodel->getVendorsForUser($appuserid);
		    if($existingvendroids=="")
		    	$vendorids=$fromvendorid;
		    else 
		       $vendorids=$existingvendroids.",".$fromvendorid;*/
		       
		     	$datacc=array();
			   		//$datacc['appuserid']=$appuserid;
	 		 		//$datacc['vendorid']=$fromvendorid;
	 		  		$datacc['isauthorized'] = 1;

				$updated = $this->appuservendorsmodel->authorizedVendor($fromvendorid,$appuserid,$datacc);

	 	 		$count=$this->appuservendorsmodel->isAppuseridAndVendoridExists($appuserid,$fromvendorid);
	 	 		$datacc['appuserid']=$appuserid;
	 		 	$datacc['vendorid']=$fromvendorid;
				if($count == 0)
				{
					$this->appuservendorsmodel->insertInAppUserVendors($datacc);
				}
			//delete token entry
				$this->invitationtokenmodel->deleteByToken($token);
			
				$vendor=$this->vendorsmodel->getVendorDetails($fromvendorid);
				$vendorname=$vendor['firstname']." ".$vendor['lastname'];
				$vendoremail=$vendor['email'];
			
		   		$appuser=$this->appusersmodel->fetchUserDetails2($appuserid);
		   		$username=$appuser['firstname']." ".$appuser['lastname'];
		   		$from=SITE_MAIL;
				$subject='BabelBark customer shares profile';
				
					//send reminder mail to vendor
					$sendmessage  = file_get_contents('resources/BabelBarkUserShareProfile.html'); 
					
					$sendmessage = str_replace('%vendorname%', $vendorname, $sendmessage); 
					$sendmessage = str_replace('%customername%', $username, $sendmessage); 
					 
					///send_template_mail('',$sendmessage, $subject, $vendoremail, $from);
					$this->load->library('email');
    				$config['protocol'] = "smtp";
    				$config['smtp_host'] = "ssl://smtp.gmail.com";
    				$config['smtp_port'] = "465";
    				$config['smtp_user'] = SITE_MAIL; 
    				$config['smtp_pass'] = "doogyWOOGY!";
  					$config['charset'] = "utf-8";
				    $config['mailtype'] = "html";
				    $config['newline'] = "\r\n";

				    $this->email->initialize($config);
				    $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
				    $this->email->to($vendoremail);
				    $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
				    $this->email->cc(SITE_MAIL,'BizBark');
				    $this->email->subject($subject);
				    $this->email->message($sendmessage);
				    $result =$this->email->send();
					
					if(!$result)
					 {
					 	$result = $this->email->print_debugger();
					 	echo $result;
					 }else{
					 	echo " ";
					 }
					 	$data['msg'] = "You have successfully authorized ". $vendorname." to view your BabelBark profile information!";
		   				$this->load->view('acceptinvitation.php',$data);
			}
			else
			{
                            $invitataiondetails = $this->invitationtokenmodel->getInvitationByTokenByStatus($token);
                            $fromvendorid = $invitataiondetails['fromvendorid'];
                            $vendor = $this->vendorsmodel->getVendorDetails($fromvendorid);
                            $vendorname = $vendor['firstname'] . " " . $vendor['lastname'];
                            $data['msg'] = "You have already authorized " . $vendorname . " to view your BabelBark profile information!";
                            $this->load->view('acceptinvitation.php', $data);
                        }
			
		}	
	}
}

