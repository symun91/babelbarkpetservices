<?php
class Promotiondetail extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('appusersmodel','promotionsmodel','appuserpromotionmodel'));
		
	}
	
	function index()
	{
		$data['msg'] = '';
		if(!empty($_GET)){
			$appuserid = $this->input->get('appuserid');
			$promotionid = $this->input->get('promotionid');
			if($appuserid!="" && $promotionid!="")
			{
				
				
				$data['detail'] = $this->promotionsmodel->getPromotionDetails($promotionid);
				$data['promotionid']=$promotionid;
				$data['appuserid']=$appuserid;
			
			}
			else
			{
				$data['msg'] = 'Request parameters are missing';
			}
			
		}
		else
		{
			$data['msg'] = 'Request parameters are missing';
		}
		  $this->load->view('promotionDetail.php',$data);
	}
	
	function getpromotion()
	{
		$appuserid = $this->input->post('appuserid');
		$promotionid = $this->input->post('promotionid');
		if(!$this->appuserpromotionmodel->promotionRedeemed($promotionid,$appuserid)) //not already redeemed
		{
			$datacc =array(
			'appuserid'=>$appuserid,
			'promotionid' => $promotionid,
			'status'=> 'waiting',
			'createdon' => date('Y-m-d H:i')
			);
	  	 $this->appuserpromotionmodel->updatePromotions($datacc,$promotionid,$appuserid);
	  	 $data['msg'] = 'Promotion redeem request sent successfully!';
		}
	   else
	   {
	   	$data['msg'] = 'Promotion is already redeemed to you';
	   }
	 $this->load->view('promotionDetail.php',$data);
	}
}
?>