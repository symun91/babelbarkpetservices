<?php //cron will run every hour
/**
 * @property appointmentmodel $appointmentmodel
 */
class Appointment_reminder_cron extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('common_helper','url'));
        $this->load->model('appointmentmodel');
        $this->load->model('vendorsmodel');
        $this->load->model('customermodel');
        $this->load->model('appusersmodel');

    }

    function sendReminderMail()
    {
        $appointments=$this->appointmentmodel->getAllUpcomingAppointments3();
        for($i=0;$i<count($appointments);$i++)
        {
            $startdatetime=$appointments[$i]['boardingfromdate']."T".$appointments[$i]['boardingfromtime'];

            $startdatetime1 = new DateTime($startdatetime);

            $currentsqldate=$appointments[$i]['curdate'];
            $hourdiff = round(( strtotime($startdatetime)-strtotime($currentsqldate))/3600, 1);


            if(($hourdiff>=48&&$hourdiff<49) || ($hourdiff>=6&&$hourdiff<7))
            {

                $servicename=$appointments[$i]['servicename'];
                $petname=$appointments[$i]['petname'];
                $notes=$appointments[$i]['bookingnote'];
                $pickdrop=$appointments[$i]['pickdrop'];
                $pickdropstr="No";
                if($pickdrop==1)
                    $pickdropstr=="Yes";

                $boardingfromdate=$appointments[$i]['boardingfromdate'];
                $boardingfromtime=$appointments[$i]['boardingfromtime'];
                $boardingtodate=$appointments[$i]['boardingtodate'];
                $boardingtotime=$appointments[$i]['boardingtotime'];



                //appointment_reminedr_email_template
                $vendorid=$appointments[$i]['vendorid'];
                $vendordetails=$this->vendorsmodel->getVendorDetails($vendorid);
                $vendoremail=$vendordetails['email'];
                $vendorname=$vendordetails['firstname']." ".$vendordetails['lastname'];
                $vendorphone=$vendordetails['contact'];
                $vendorwebsite=$vendordetails['website'];
                $vendorlogo=$vendordetails['logoimage'];
                $vendorcompanyname = $vendordetails['comapnyname'];
                $vendorlocation = $vendordetails['zipcode'] . '%20' . $vendordetails['city'] . '%20' . $vendordetails['state'] . '%20' . $vendordetails['country'];

                $useremail="";
                $username="";

                $customerid=$appointments[$i]['customerid'];
                $appuserid=$appointments[$i]['appuserid'];
                if($customerid!=null || $customerid!="" || $customerid!=0 )
                {
                    $customerdetails= $this->customermodel->getCustomerDetails($customerid);
                    $useremail=$customerdetails['email'];
                    $username=$customerdetails['firstname']." ".$customerdetails['lastname'];
                }
                else if($appuserid!=null || $appuserid!="" || $appuserid!=0 )
                {
                    $appuserdetails = $this->appusersmodel->fetchUserDetails1($appuserid);
                    $useremail=$appuserdetails['email'];
                    $username=$appuserdetails['firstname']." ".$appuserdetails['lastname'];
                }
                $date = new DateTime($boardingfromdate . ' ' . $boardingfromtime);
                $Fromtime = $date->format('Y-m-d H:i:s');
                $date = new DateTime($boardingtodate . ' ' . $boardingtotime);
                $Totime = $date->format('Y-m-d H:i:s');

                if ($vendordetails['category'] == 1) {
                    $siteAddress = 'Babelvet';
                    $logoName = 'logo_vet.png';
                }else{
                    $siteAddress = 'Bizbark';
                    $logoName = 'logo.png';
                }




                $from=SITE_MAIL;
                $subject='Bizbark appointment reminder';

                //send reminder mail to vendor
                $sendmessage  = file_get_contents('resources/appointment_reminder_vendor_template.html');

                $sendmessage = str_replace('%customername%', $username, $sendmessage);
                $sendmessage = str_replace('%petname%', $petname, $sendmessage);
                $sendmessage = str_replace('%servicename%', $servicename, $sendmessage);
                $sendmessage = str_replace('%pickupdropoff%', $pickdropstr, $sendmessage);
                $sendmessage = str_replace('%notes%', $notes, $sendmessage);
                $sendmessage = str_replace('%fromdate%', $boardingfromdate, $sendmessage);
                $sendmessage = str_replace('%todate%', $boardingtodate, $sendmessage);
                $sendmessage = str_replace('%fromtime%', $boardingfromtime, $sendmessage);
                $sendmessage = str_replace('%totime%', $boardingtotime, $sendmessage);
                $sendmessage = str_replace('%lfromtime%',$Fromtime,$sendmessage);
                $sendmessage = str_replace('%ltotime%', $Totime, $sendmessage);
                $sendmessage = str_replace('%companyname%', $vendorcompanyname, $sendmessage);
                $sendmessage = str_replace('%vendorlocation%', $vendorlocation, $sendmessage);
                $sendmessage = str_replace('%sitename%', $siteAddress, $sendmessage);
                $sendmessage = str_replace('%logoname%', $logoName, $sendmessage);

                ///send_template_mail('',$sendmessage, $subject, $vendoremail, $from);
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->to($vendoremail);
                $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->cc(SITE_MAIL,'BizBark');
                $this->email->subject($subject);
                //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');

                $this->email->message($sendmessage);
                $result = $this->email->send($sendmessage);

                if(!$result)
                {
                    $result = $this->email->print_debugger();
                    echo $result;
                }else{
                    echo "Email Vendor Success";
                }

                //send reminder mail to appuser/customer
                $subject='BabelBark appointment reminder';
                $sendmessage  = file_get_contents('resources/appointment_reminder_user_template.html');

                $sendmessage = str_replace('%vendorname%', $vendorname, $sendmessage);
                $sendmessage = str_replace('%petname%', $petname, $sendmessage);
                $sendmessage = str_replace('%servicename%', $servicename, $sendmessage);
                $sendmessage = str_replace('%pickupdropoff%', $pickdropstr, $sendmessage);
                $sendmessage = str_replace('%notes%', $notes, $sendmessage);
                $sendmessage = str_replace('%fromdate%', $boardingfromdate, $sendmessage);
                $sendmessage = str_replace('%todate%', $boardingtodate, $sendmessage);
                $sendmessage = str_replace('%fromtime%', $boardingfromtime, $sendmessage);
                $sendmessage = str_replace('%totime%', $boardingtotime, $sendmessage);
                $sendmessage = str_replace('%vendoremail%', $vendoremail, $sendmessage);
                $sendmessage = str_replace('%vendorwebsite%', $vendorwebsite, $sendmessage);
                $sendmessage = str_replace('%vendorphone%', $vendorphone, $sendmessage);
                $sendmessage = str_replace('%lfromtime%',$Fromtime,$sendmessage);
                $sendmessage = str_replace('%ltotime%', $Totime, $sendmessage);
                $sendmessage = str_replace('%companyname%', $vendorcompanyname, $sendmessage);
                $sendmessage = str_replace('%vendorlocation%', $vendorlocation, $sendmessage);
                $sendmessage = str_replace('%sitename%', $siteAddress, $sendmessage);
                $sendmessage = str_replace('%logoname%', $logoName, $sendmessage);

                //send_template_mail('',$sendmessage, $subject, $useremail, $from);
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = SITE_MAIL;
                $config['smtp_pass'] = "doogyWOOGY!";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);
                $this->email->from(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->to($useremail);
                $this->email->reply_to(SITE_MAIL_SUPPORT, 'BizBark');
                $this->email->cc(SITE_MAIL,'BizBark');
                $this->email->subject($subject);
                //$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');

                $this->email->message($sendmessage);

                $result =$this->email->send();
                if(!$result)
                {
                    $result = $this->email->print_debugger();
                    echo $result;
                }else{
                    echo "Email User Success";
                }

            }


        }

    }
}
?>