<?php
require_once 'vendor/autoload.php';
include_once 'config_s3.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

if (! function_exists('aws_upload')) 
{
    /**
     * Attempt to upload a file to S3
     * 
     * @param  string $folder
     * @param  string $name
     * @param  string $tmp
     * @return string
     */
    function aws_upload($folder, $name, $tmp)
    {
        $s3file = '';
        $client = S3Client::factory([
            'region'      => awsRegion, //'eu-central-1',
            'version'     => 'latest',
            'credentials' => [
                'key'    => awsAccessKey,
                'secret' => awsSecretKey,
            ]
        ]);

        try {
            $client->putObject([
                'Bucket'       => awsBucket,
                'Key'          => $folder . $name,
                'SourceFile'   => $tmp,
                'ACL'          => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY'
            ]);

            $s3file  = 'https://' . awsBucket . '.s3.amazonaws.com/' . $folder . $name;
            $message = "S3 upload successful";

        } catch (S3Exception $e) {
            $message = "S3 upload failed";
        }

        return $message;
    }
}

if (! function_exists('aws_copy'))
{
    /**
     * Attempt to copy a file from one bucket to another.
     * 
     * @param  string $sourceFolder
     * @param  string $sourceFileName
     * @param  string $targetFolder
     * @param  string $targetFileName
     * @return string
     */
    function aws_copy($sourceFolder, $sourceFileName, $targetFolder, $targetFileName)
    {
        $s3file        = '';
        $sourceBucket  = awsBucket;
        $sourceKeyname = $sourceFolder . $sourceFileName;
        $targetBucket  = awsBucket;
        $targetKeyname = $targetFolder.$targetFileName;

        $client = S3Client::factory([
            'region'      => awsRegion,
            'version'     => 'latest',
            'credentials' => [
                'key'    => awsAccessKey,
                'secret' => awsSecretKey,
            ]
        ]);

        try {
            $client->copyObject([
                'Bucket'     => $targetBucket,
                'Key'        => $targetKeyname,
                'ACL'        => 'authenticated-read',
                'CopySource' => "{$sourceBucket}/{$sourceKeyname}"
            ]);

            $s3file  = 'https://' . awsBucket . '.s3.amazonaws.com/' . $targetKeyname;
            $message = "S3 file copy successful";

        } catch (S3Exception $e) {
            $message = "S3 file copy failed";
        }

        return $message;
    }
}

if (! function_exists('aws_delete'))
{
    /**
     * Attempt to delete an image from S3.
     * 
     * @param  string $sourceFolder
     * @param  string $sourceFileName
     * @return string
     */
    function aws_delete($sourceFolder, $sourceFileName)
    {
        $client = S3Client::factory([
            'region'      => awsRegion,
            'version'     => 'latest',
            'credentials' => [
                'key'    => awsAccessKey,
                'secret' => awsSecretKey,
            ]
        ]);

        try {
            $client->deleteObject([
                'Bucket' => awsBucket,
                'Key'    => $sourceFolder.$sourceFileName
            ]);

            $message = "File has been deleted Successfully";

        } catch (S3Exception $e) {
            $message = "File delete failed";
        }

        return $message;
    }
}

if (! function_exists('image_validation'))
{
    /**
     * Validate that the passed in extension type is an image.
     * 
     * @param  string $ext
     * @return bool
     */
    function image_validation($ext)
    {
        $valid_formats = ["jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP"];

        return in_array($ext, $valid_formats);
    }
}

if (! function_exists('getExtension'))
{
    /**
     * Get the extension from the passed in filename.
     * 
     * @param  string $str
     * @return string
     */
    function getExtension($filename) {
        $i = strrpos($filename, ".");

        if (! $i) {
            return "";
        }

        $l   = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);

        return $ext;
    }
}  

if (! function_exists('aws_image'))
{
    /**
     * Get an image from S3.
     * 
     * @param  string $img_path
     * @return image
     */
    function aws_image($img_path){
        $client = S3Client::factory([
            'region'      => awsRegion,
            'version'     => 'latest',
            'credentials' => [
                'key'    => awsAccessKey,
                'secret' => awsSecretKey,
            ]
        ]);

        $client->registerStreamWrapper();

        $img_path = urldecode($img_path);

        $file_headers = @get_headers($img_path);

        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            $data  = file_get_contents($img_path);
            return $data;
        }
    }
}
    
if (! function_exists('aws_download'))
{
    /**
     * Attempt to download an image from S3.
     * 
     * @param  string $filepath
     * @param  string $uploadPath
     * @return string
     */
    function aws_download($filepath, $uploadPath)
    {
        $client = S3Client::factory([
            'region'      => awsRegion,
            'version'     => 'latest',
            'credentials' => [
                'key'    => awsAccessKey,
                'secret' => awsSecretKey,
            ]
        ]);

        // Save object to a file.
        try {
            $result = $client->getObject([
                'Bucket' => awsBucket,
                'Key'    => $filepath,
                'SaveAs' => $uploadPath
            ]);

            $message = "File downloaded Successfully";

        } catch (S3Exception $e) {
            // Catch an S3 specific exception.
            $message = $e->getMessage();
        }

        return $message;
    }
}
