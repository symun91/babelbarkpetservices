<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* Messages and constants 
 * Added by Bhairavi
 * */

define ("METRICUNITS", json_encode(array('weight'=>'kg','distance'=>'km'))); 
define ("BRITISHUNITS", json_encode(array('weight'=>'lb','distance'=>'miles'))); 
define ("FEEDUNITS", json_encode(array('cups','kilos')));

$usstatesarry=array("Alabama","Washington DC","Montana","Alaska","Nebraska","Arizona","Nevada","Arkansas","New Hampshire","California","New Jersey","Colorado","New Mexico","Connecticut","New York","Delaware","North Carolina","Florida","North Dakota","Georgia","Ohio","Hawaii","Oklahoma","Idaho","Oregon","Illinois","Pennsylvania","Indiana","Rhode Island","Iowa","South Carolina","Kansas","South Dakota","Kentucky","Tennessee","Louisiana","Texas","Maine","Utah","Maryland","Vermont","Massachusetts","Virginia","Michigan","Washington","Minnesota","West Virginia","Mississippi","Wisconsin","Missouri","Wyoming","Puerto Rico");
sort($usstatesarry);
$canadastatesarr=array("Ontario", "Quebec", "Nova Scotia", "New Brunswick", "Manitoba", "British Columbia", "Prince Edward Island", "Saskatchewan", "Alberta", "Newfoundland and Labrador", "Northwest Territories", "Yukon", "Nunavut");
sort($canadastatesarr);
define ("USSTATES", json_encode($usstatesarry));
define ("CANADASTATES", json_encode($canadastatesarr));
//define ("USSTATES", json_encode(array("Alabama","Montana","Alaska","Nebraska","Arizona","Nevada","Arkansas","New Hampshire","California","New Jersey","Colorado","New Mexico","Connecticut","New York","Delaware","North Carolina","Florida","North Dakota","Georgia","Ohio","Hawaii","Oklahoma","Idaho","Oregon","Illinois","Pennsylvania","Indiana","Rhode Island","Iowa","South Carolina","Kansas","South Dakota","Kentucky","Tennessee","Louisiana","Texas","Maine","Utah","Maryland","Vermont","Massachusetts","Virginia","Michigan","Washington","Minnesota","West Virginia","Mississippi","Wisconsin","Missouri","Wyoming")));

//define ("CANADASTATES", json_encode(array("Ontario", "Quebec", "Nova Scotia", "New Brunswick", "Manitoba", "British Columbia", "Prince Edward Island", "Saskatchewan", "Alberta", "Newfoundland and Labrador", "Northwest Territories", "Yukon", "Nunavut")));

define ('PERPAGE','5');
//Messages
define('NODATAMSG', 'nodatafound');
define('GENERALERRMSG', 'Something went wrong.');
define('INVALIDLOGINMSG', 'Invalid email or password.');
define('MANDATORYFIELDsERRMSG', 'Few mandatory fields are missing.');
define('EMAILEXISTMSG', 'Email already exists.');
define('USERNAMEEXISTMSG', 'Username already exists.');
define('INVALIDFILEEXTMSG', 'File format is not supported.');
define('DEVICEWIPEDOUTMSG','Your device is wiped out');
define('SESSIONEXPRDMSG','Session Expired.');
define('NOSELECTEDVENDOR','No vendor is selected! ');

define('LOGINSUCCMSG', 'User login successfull.');
define('REGSUCCMSG', 'User registered successfully.');
define('UPDATEPROFSUCCMSG','User profile updated successfully.');
define('UPDATEDATASUCCMSG','Data updated successfully.');
define('FETCHDATASUCCMSG','Data fetched successfully.');
define('EMAILSENT','Invite email sent successfully.');
define('EMAILNOTEXIST','Email ID does not exist.');
define('VENDORREGSUCCMSG','Registered successfully.');
define('FULLCAPACITYMSG','Appoinment is already full for the time slot.');
define('CAPACITYMSG',"Appointment capacity is available in this time slot");
define('VENDOREXIST','Vendor already added.');


/* End of Messages and constants */

/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('ADMINISTRATOR','admin');
define('SITE_MAIL','admin@bizbark.com');
define('SITE_MAIL_SUPPORT','support@bizbark.com');
define('SITE_MAIL_BABEL','admin@babelbark.com');
define('SITE_MAIL_SUPPORT_BABEL','support@babelbark.com');
define('VENDOR','vendor');
define('VENDORUSER','vendorusers');
$http_https = isset($_SERVER['HTTPS']) ? "https://" : "http://";
$base_url   = $http_https . $_SERVER['HTTP_HOST'] . '/';
define('PETIMAGEURL' , $base_url);
define('BABELVET_TITLE', 'BabelVet');
define('BIZBARK_TITLE', 'BizBark');

if (ENVIRONMENT=='development'){
    define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'babelvet.dev'));
    define('BABELVET_ADD', $http_https.'babelvet.dev');
    define('BIZBARK_ADD', $http_https.'bizbark.dev');
} elseif(ENVIRONMENT=='testing'){
    define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'test.babelvet.com'));
    define('BABELVET_ADD', $http_https.'test.babelvet.com');
    define('BIZBARK_ADD', $http_https.'test.bizbark.com');
} elseif(ENVIRONMENT=='staging'){
    define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'vet.19peaches.com'));
    define('BABELVET_ADD', $http_https.'vet.19peaches.com');
    define('BIZBARK_ADD', $http_https.'bark.19peaches.com');
} elseif(ENVIRONMENT=='production'){
    if($_SERVER['HTTP_HOST'] == 'babelvet.com'){
        define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'babelvet.com'));
        define('BABELVET_ADD','http://babelvet.com');
        define('BIZBARK_ADD', $http_https.'bizbark.com');
    }
    if($_SERVER['HTTP_HOST'] == 'bizbark.com'){
        define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'babelvet.com'));
        define('BABELVET_ADD','http://babelvet.com');
        define('BIZBARK_ADD', $http_https.'bizbark.com');
    }
    if($_SERVER['HTTP_HOST'] == 'portal.babelvet.com'){
        define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'portal.babelvet.com'));
        define('BABELVET_ADD','http://portal.babelvet.com');
        define('BIZBARK_ADD', $http_https.'portal.bizbark.com');
    }
    if($_SERVER['HTTP_HOST'] == 'portal.bizbark.com'){
        define('ALLOW_VET', ($_SERVER['HTTP_HOST'] == 'portal.babelvet.com'));
        define('BABELVET_ADD','http://portal.babelvet.com');
        define('BIZBARK_ADD', $http_https.'portal.bizbark.com');
    }

}



define('APP_ID', 'e1488e12aed1e07f93e779abbb9d4b49');
define('APP_KEY', '69e750e3dacd9ee68a45fe1e5bbd0801');
define('OFFICE_ID', 'V1002');
define('SECRET_KEY', '34040470V936B6BFGOQ');
