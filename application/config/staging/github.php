<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * @package     CodeIgniter Git Deploy
 * @author      Vince Kronlein
 * @copyright   Copyright (c) 2017
 */

$config['github'] = [
	'repository' => 'backend',
	'branch'     => 'develop'
];
