<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once 'fpdf/fpdf.php';

class MyPdf extends FPDF{
    function __construct($orientation='p',$unit='mm',$format='A4')
    {
        parent::__construct();
        $CI = get_instance();
    }
        function Header()
    {
        // Logo
        $this->Image($GLOBALS['vet_logo'], 10, 10, 30, 10);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(32);
        // Title
        $this->SetFont('Arial', 'B', 15);
        $this->Cell(30, 10, $GLOBALS['company'], 0, 0, 'L');
        // Logo
        $this->Image('assets/images/pet_profile_logo.png', 250, 7, 30, 10);
        // Line break
        $this->Ln(20);
    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-20);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}

