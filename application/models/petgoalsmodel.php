<?php
class Petgoalsmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function addActivityGoals($datacc,$petid)
	{
		 
	   if( $this->petidExistsActivity($petid) == TRUE ) { //update
			$this->db->where('petid', $petid);
			$this->db->update('petactivitygoals', $datacc);
		}
		else // insert
		{
			  $this->db->insert('petactivitygoals', $datacc);
		  
		 }
	
	return $this->db->affected_rows();
	}
	
function petidExistsActivity($petid){
		
		$this->db->select('petid');
		$this->db->where('petid', $petid);
		$result =  $this->db->get('petactivitygoals');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	
function addMedicalGoals($datacc,$petid)
	{
		 
	   if( $this->petidExistsMedical($petid) == TRUE ) { //update
			$this->db->where('petid', $petid);
			$this->db->update('petmedicalgoals', $datacc);
		}
		else // insert
		{
			  $this->db->insert('petmedicalgoals', $datacc);
		  
		 }
	
	return $this->db->affected_rows();
	}
	
function petidExistsMedical($petid){
		
		$this->db->select('petid');
		$this->db->where('petid', $petid);
		$result =  $this->db->get('petmedicalgoals');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	

	
function addWeightGoals($datacc,$petid)
	{
		 
	   if( $this->petidExistsWeight($petid) == TRUE ) { //update
			$this->db->where('petid', $petid);
			$this->db->update('petweightgoals', $datacc);
		}
		else // insert
		{
			  $this->db->insert('petweightgoals', $datacc);
		  
		 }
	
	return $this->db->affected_rows();
	}
function petidExistsWeight($petid){
		
		$this->db->select('petid');
		$this->db->where('petid', $petid);
		$result =  $this->db->get('petweightgoals');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
}
?>