<?php
class Classpush extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}

    function fetchDeviceInfo($userid)
    {
    	$this->db->select('devicetoken,isiphone');
		$this->db->where('userid',$userid);
		$this->db->where('isactive',1);
		$query = $this->db->get('appuserdevices');
		return $query->row();
    }


//    pushtype=2; //booking request send
//	  pushtype=3; //booking request cancelled
//	  pushtype=4; //booking request accepted by driver
//	  pushtype=5; //driver has came

    function sendPushChatNotification($userid,$pushmessage,$bookingid,$usertype,$pushtype)
    {
    	 
				
			$deviceinfo = $this->classpush->fetchDeviceInfo($userid);
			$device_token = $deviceinfo->devicetoken;
			$isandroid =  $deviceinfo->isandroid;
			
			$query = "SELECT badgecount FROM appuserdevices WHERE devicetoken = '{$device_token}'";	
			$query = $this->db->query($query);
			$row = $query->row_array();
			$updatequery = "update userdevices set badgecount=badgecount+1 WHERE devicetoken ='{$device_token}'";	
			$updatequery = $this->db->query($updatequery);
			$device = $device_token;
			
			$datacc=array();
			if($usertype=="user")				
				$datacc['touserid']=$userid;	
			else 
				$datacc['todriverid']=$userid;
			$datacc['message']=$pushmessage	;
			$datacc['usertype']=$usertype;	
			$datacc['status']="unread";
			$this->db->insert('messages', $datacc);
			
			if($isandroid == '1') //android
			{
				$regId = $device_token;
				include_once '/var/www/MomoTaxi/resources/GCM.php';
				$gcm = new GCM();
				$pushid= 1;
				$registatoin_ids = array($regId);
				
			
			   $message = array("message" => $pushmessage,"id"=>$pushid, 'bookingid' => $bookingid, 'type' => $pushtype);
				
				$result = $gcm->send_notification($registatoin_ids, $message);
				
				$return_array['success']="true";
				$return_array['a_message']="android notification sent";
			}
			else {  //iphone
		
			   
			   $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'bookingid' => $bookingid, 'type' => $pushtype);
			
			$payload = json_encode($payload);
			$options = array('ssl' => array(
			  'local_cert' => '/var/www/MomoTaxi/pushapns/apns-prod-cert.pem',
			  'passphrase' => 'teks123'
			));
			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, $options);
			$apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);

			$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
			fwrite($apns, $apnsMessage);   
			fclose($apns);
			}
		
    }
    
    function sendTaxiRequestPushToDrivers($userid,$bookingid)
    {
    	//fetch all drivers with same package and within pickup radius
    	$booking=array();
    	$drivers=array();
    	$this->db->select('*');	
		$this->db->where('bookingid',$bookingid);
		$query = $this->db->get('bookinghistory');
		if($query->num_rows() == 1) 
		$booking= $query->row_array();
		 
		  
		  $pickuploclat=$booking['pickuploclat'];
		  $pickuploclong=$booking['pickuploclong'];
		  $packageid=$booking['packageid'];
		  
		  //fetch all drivers with same package
		 $this->db->select('*');	
		 $this->db->where('workingpackageid','enabled');
	    $this->db->where('status','enabled');
	   	$this->db->where('sevicestatus','on');
		$query = $this->db->get('driver');		
		$drivers= $query->result_array();		
		
		$result=array();
		for($i=0;$i<count($drivers);$i++)
		{
			$pickupradius=$drivers[$i]['pickupradius'];
			$drivercurlat=$drivers[$i]['currentloclat'];
		 	$drivercurlong=$drivers[$i]['currentloclong'];
		 	if($this->distance($pickuploclat, $pickuploclong, $drivercurlat, $drivercurlong)<=$pickupradius)
		 	{
		 		$result[]=$drivers[$i];
		 	}
		}
		$pushmessage="User has requested for taxi!";
		for($i=0;$i<count($result);$i++)
		{
			$touserid=$result[$i]['drvierid'];
			$this->sendPushChatNotification($touserid, $pushmessage, $bookingid, "driver","2");
		}
    }
    
    function sendCancelRequestPushToDriver($userid,$bookingid)
    {
    		$booking=array();
    		$this->db->select('*');	
			$this->db->where('bookingid',$bookingid);
			$query = $this->db->get('bookinghistory');
			if($query->num_rows() == 1) 
			$booking= $query->row_array();
			
			$status=$booking['status'];
			if($status=="accepted")
			{
				$pushmessage="User has cancelled the booking!";
				$driverid=$booking['driverid'];
				$this->sendPushChatNotification($driverid, $pushmessage, $bookingid, "driver","3");
			}
    }
    
    
 function sendBookingAcceptedPushToUser($bookingid)
    {
    		$booking=array();
    		$this->db->select('*');	
			$this->db->where('bookingid',$bookingid);
			$query = $this->db->get('bookinghistory');
			if($query->num_rows() == 1) 
			$booking= $query->row_array();
			
			$status=$booking['status'];
			if($status=="accepted")
			{
				$pushmessage="Driver has accepted the booking!";
				$userid=$booking['userid'];
				$this->sendPushChatNotification($userid, $pushmessage, $bookingid, "user","4");
			}
    }
    
    
 function sendDriverHaveCamePushToUser($bookingid)
    {
    		$booking=array();
    		$this->db->select('*');	
			$this->db->where('bookingid',$bookingid);
			$query = $this->db->get('bookinghistory');
			if($query->num_rows() == 1) 
			$booking= $query->row_array();
		
			
				$pushmessage="Driver has came!";
				$userid=$booking['userid'];
				$this->sendPushChatNotification($userid, $pushmessage, $bookingid, "user","5");
			
    }
function distance($lat1, $lon1, $lat2, $lon2) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  //if ($unit == "K") {
      return ($miles * 1.609344);//km
  //} 
		}
}