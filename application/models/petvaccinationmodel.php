<?php
class Petvaccinationmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function addVaccination($datacc)
	{
		     $this->db->insert('petvaccination', $datacc);
		     return $this->db->affected_rows();
	}
	
	

function deleteByPetID($petid)
	{
		 $query = "delete from petvaccination where petid = '{$petid}'";
		$this->db->query($query);
	}
	
	function getAllVaccinations(){
		 $this->db->select('*');
		 $this->db->order_by("name","asc");		   
		$query = $this->db->get('vaccine');		
		return $query->result_array();		
	}
	
function fetchVaccineDetails($vaccineid)
   {
	    $this->db->select('*');	
		$this->db->where('vaccineid',$vaccineid);
		$query = $this->db->get('vaccine');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}
   }
   
   function getPetVaccineDetails($petid)
	{
		$this->db->select('petvaccination.id as petvaccinationid,petvaccination.vaccineid as id,petvaccination.info,petvaccination.duedate,vaccine.name');
		 $this->db->join('vaccine' , 'petvaccination.vaccineid = vaccine.vaccineid');
		$this->db->where('petid',$petid);
		$query = $this->db->get('petvaccination');
		 return $query->result_array();
		
	}

	function getPetVaccinationForPet1($petid)
	{
		$this->db->select('petvaccination.id,petvaccination.vaccineid ,petvaccination.duedate,vaccine.name');
		 $this->db->join('vaccine' , 'petvaccination.vaccineid = vaccine.vaccineid');
		$this->db->where('petvaccination.petid',$petid);
		$query = $this->db->get('petvaccination');
		 return $query->result_array();

	}

	function addPetVaccination($data) {
    	$this->db->insert_batch('petvaccination', $data);
	}

	function getLatestVacInfo($petId) {
        $sql = "select * from petvaccination where petid = '{$petId}' and DATE(updatedon) = CURDATE()";
        $query = $this->db->query($sql);
        return $query->num_rows();
	}

	function checkVaccineExist($petId,$vaccineId,$dueDate){
	    $sql = "select * from petvaccination p where p.petid='{$petId}' and p.vaccineid = '{$vaccineId}' and p.duedate = '{$dueDate}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

}
?>