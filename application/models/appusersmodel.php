<?php

class Appusersmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }
    function requestAuth($userid,$sessiontoken)
    {
        $cd=date('Y-m-d H:i');
        $this->db->select('*');
        $this->db->where('TIMESTAMPDIFF(HOUR,lastloggedon,\''.$cd.'\')<=',12);
        $this->db->where('appuserid', $userid);
        $this->db->where('sessiontoken', $sessiontoken);
        $result =  $this->db->get('appusers');
        return ($result->num_rows() == 1) ? $result->row_array() : "expired";

    }

    function getAllUsersForAdmin()
    {
        $this->db->select('*');
        $query = $this->db->get('appusers');
        return $query->result_array();
    }

    function getAllUsersForAdmin1($num, $offset)
    {
        $this->db->select('*');
        $this->db->limit($num, $offset);
        $query = $this->db->get('appusers');
        return $query->result_array();
    }

    function getAllAppusersCount()
    {
        $this->db->select('*');
        $query = $this->db->get('appusers');
        return $query->num_rows();
    }

    function fetchUserID($email)
    {
        $this->db->select('appuserid');
        $this->db->where('email',$email);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            return $query->row()->appuserid;
        }
        else {
            return 0;
        }
    }
    function fetchUserIDByFBId($facebookid)
    {
        $this->db->select('appuserid');
        $this->db->where('fbid',$facebookid);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            return $query->row()->appuserid;
        }
        else {
            return 0;
        }
    }
    function userLogin($email,$password)
    {
        $this->db->select('appusers.appuserid');
        $this->db->join('appuserdevices' , 'appuserdevices.userid = appusers.appuserid');
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            //return $query->row_array();
            return $query->row()->appuserid;
        }
        else {
            return 0;
        }
    }

    function userFBLogin($username,$facebookid)
    {
        $this->db->select('appusers.appuserid');
        $this->db->join('appuserdevices' , 'appuserdevices.userid = appusers.appuserid');
        $this->db->where('fbid',$facebookid);
        $this->db->where('username',$username);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            //return $query->row_array();
            return $query->row()->appuserid;
        }
        else {
            return 0;
        }
    }

    function registerUser($datacc)
    {
        $this->db->insert('appusers', $datacc);
        return $this->db->affected_rows();
    }


    function updateUser($datacc,$userid)
    {
        $this->db->where('appuserid',$userid);
        $this->db->update('appusers', $datacc);
        return $this->db->affected_rows();
    }

    function addpet($datacc)
    {
        $this->db->insert('appuserpets', $datacc);
        return $this->db->affected_rows();
    }

    function updatepetUser($datacc,$petid)
    {
        $this->db->where('appuserid',$userid);
        $this->db->update('appuserpets', $datacc);
        return $this->db->affected_rows();
    }
    function getPetID($userid)
    {
        $this->db->select('petid');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appuserpets');
        if($query->num_rows==1)
        {
            return $query->row()->petid;
        }
        else
        {
            return 0;
        }
    }
    function getAllUserPets($userid)
    {
        $this->db->select('petid');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appuserpets');

        if($query->num_rows() == 0)
        {
            return 0;
        }
        else
        {
            return $query->result_array();
        }
    }
    function getPetIDs($userid)
    {
        $this->db->select('petid');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appuserpets');
        if($query->num_rows>1)
        {
            return $query->result_array();
        }
        else
        {
            //return 0;
            return $query->row()->petid;
        }
    }

    function getPets($userid)
    {
        $this->db->select('petid');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appuserpets');
        if($query->num_rows > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }

    function deleteAppUserPets($appuserid)
    {
        $query = "delete from appuserpets where appuserid = '{$appuserid}' ";
        $this->db->query($query);
    }

    function deleteAppUserPetMedications($petid)
    {
        $query = "delete from petmedication where petid = '{$petid}' ";
        $this->db->query($query);
    }

    function deleteAppUserPetVaccinations($petid)
    {
        $query = "delete from petvaccination where petid = '{$petid}' ";
        $this->db->query($query);
    }

    function deletePet($petid)
    {
        $query = "delete from pet where petid = '{$petid}' ";
        $this->db->query($query);
    }

    function deleteAppUser($appuserid)
    {
        $query = "delete from appuservendors where appuserid = '{$appuserid}' ";
        $this->db->query($query);
    }

    function isEmailExists($email)
    {
        $this->db->select('appuserid');
        $this->db->where('email',$email);
        $query = $this->db->get('appusers');
        return ($query->num_rows() == 1) ? 1 : 0;
    }
    function isEmailExistsForAUser($email,$userid)
    {
        $this->db->select('appuserid');
        $this->db->where('email',$email);
        $this->db->where('appuserid !=',$userid);
        $query = $this->db->get('appusers');
        return ($query->num_rows() == 1) ? 1 : 0;
    }


    function fetchUserDetails($userid)
    {
        $this->db->select('appuserid,firstname,lastname,email,birthday,gender,address,city,state,zip,sessiontoken,phonenumber,country,isverified');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }
    function fetchUserDetails1($userid)
    {
        $this->db->select('appuserid,firstname,lastname,email,birthday,gender,address,city,state,zip,notes,sessiontoken,phonenumber,updatedon');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }

    function fetchUserDetails2($userid)
    {
        $this->db->select('*');
        $this->db->where('appuserid',$userid);
        $query = $this->db->get('appusers');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }

    function getPetOwners($petid)
    {
        $this->db->select('appusers.*');
        $this->db->join('appusers' , 'appuserpets.appuserid = appusers.appuserid');
        $this->db->where('appuserpets.petid','$petid');
        $query = $this->db->get('appuserpets');
        return $query->result_array();
    }

    function getAppUserByResetToken($token){
        $this->db->select('appuserid');
        $this->db->where('resetpasswordtoken',$token);
        $query = $this->db->get('appusers');
        if($query->num_rows() > 0) {
            return $query->row_array();
        }
        else {
            return array();
        }
    }

    function getUserByActivateToken($token)
    {
        $this->db->select('*');
        $this->db->where('activationtoken',$token);
        $query = $this->db->get('appusers');
        return $query->row_array();
    }
    function getPetInfo($appUserId){
        $sql = "select p.petid,p.name from appuserpets as au
                    left join pet as p on p.petid = au.petid
					where au.appuserid = '{$appUserId}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function verifyToken($recordId,$token,$fileType){
        $sql = "SELECT * FROM sharerecords 
                WHERE recordid ='{$recordId}' 
                and token ='{$token}'
                and status = 1 
                and filetype = '{$fileType}'
                AND CURDATE() between startdate and expiredated";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function sikkaAppUserLIst(){
        $sql = "select ap.appuserid,s.apppetid petid,pt.name,s.sikkapetid,p.patient_id,p.name,s.vendorid,s.updatedon 
                from sikkapetimportrequests s 
                left join appuserpets ap on ap.petid = s.apppetid
                left join pet p on p.petid = s.sikkapetid
                left join pet pt on pt.petid = s.apppetid
                where s.sikkapetid !='' 
                and ap.appuserid !=''
                group by s.apppetid";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
?>