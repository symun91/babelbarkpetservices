<?php
class Vendorsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function getVendorsByCategory($categoryname){
        $this->db->select('*');
        $this->db->where('category',$categoryname);
        $query = $this->db->get('vendor');
        return $query->result_array();
    }

    function getVendorSearch($keyword)
    {
        $this->db->select('*');
        $this->db->from('vendor');
        $this->db->like('email',$keyword);
        $this->db->or_like('firstname',$keyword);
        $this->db->or_like('lastname',$keyword);
        $this->db->or_like('website',$keyword);
        $this->db->or_like('address',$keyword);
        $this->db->or_like('state',$keyword);
        $this->db->or_like('country',$keyword);
        $this->db->or_like('contact',$keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return $query->result_array();
        }
    }

    function getVendorDetails($vendorid)
    {
        $this->db->select('*');
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }


    function registerVendor($datacc)
    {
        $this->db->insert('vendor', $datacc);
        return $this->db->affected_rows();

    }
    function registerSikkaPracticeAsVendor($datacc)
    {
        $this->db->insert('vendor',$datacc);
        return $this->db->insert_id();
    }

    function updateVendor($datacc,$vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $this->db->update('vendor', $datacc);
        return $this->db->affected_rows();
    }

    function isEmailExists($email)
    {
        $this->db->select('vendorid');
        $this->db->where('email',$email);
        $query = $this->db->get('vendor');
        return ($query->num_rows() == 1) ? true : false;
    }

    function isEmailExistsForAUser($email,$vendorid)
    {
        $this->db->select('vendorid');
        $this->db->where('email',$email);
        $this->db->where('vendorid !=',$vendorid);
        $query = $this->db->get('vendor');
        return ($query->num_rows() == 1) ? 1 : 0;
    }
    function getVendrorsCount()
    {
        $query = $this->db->get('vendor');
        return $query->num_rows();
    }
    function getAllVendors($num, $offset)
    {
        $this->db->limit($num, $offset);
        $this->db->order_by("createdon", "desc");
        $query = $this->db->get('vendor');
        return $query->result_array();
    }
    function getAllVendors1()
    {
        $this->db->order_by("createdon", "desc");
        $query = $this->db->get('vendor');
        return $query->result_array();
    }

    function getAllVendorsForReport()
    {
        $sql = "SELECT vendorid as VendorID,email as Email,firstname as FirstName,lastname as LastName,comapnyname as CompanyName,contact as Contact,address as Address,city as City,state as State,country as Country,termsandconditions,category as Category,description as Description, createdon as DateCreated FROM vendor order by createdon desc";
        //return $this->db->query($sql);
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    function deleteAppUsers($vendorid)
    {
        $query = "delete from appuservendors where vendorid = '{$vendorid}' ";
        $this->db->query($query);
        return true;
    }
    function deleteSetting($vendorid)
    {
        //delete settings
        $query = "delete from vendorsettings where vendorid = '{$vendorid}' ";
        $this->db->query($query);
        return true;
    }
    function deleteServices($vendorid)
    {
        //delete services
        $query = "delete from service where vendorid = '{$vendorid}' ";
        $this->db->query($query);
        return true;
    }
    function deleteEmployees($vendorid)
    {
        //delete employees
        $query = "delete from vendorusers where vendorid = '{$vendorid}' ";
        $this->db->query($query);
        return true;
    }
    function deleteCustomers($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('customer');
        $customersCount =  $query->num_rows();

        for($i=0; $i<$customersCount; $i++)
        {
            $query = "select customerid from customer where vendorid = '{$vendorid}' ";
            $customerid = $this->db->query($query);
            $customerid = $customerid->row()->customerid;

            $query = "select veterinarianid from customer where vendorid = '{$vendorid}' ";
            $veterinarianid = $this->db->query($query);
            $veterinarianid = $veterinarianid->row()->veterinarianid;
            //print_r($veterinarianid);

            $this->db->select('petid');
            $this->db->where('customerid',$customerid);
            $query = $this->db->get('customerpets');
            if($query->num_rows() == 1) {
                $customerpet = 1;
            }
            else {
                $customerpet = $query->num_rows();
            }

            $this->db->select('petid');
            $this->db->where('customerid',$customerid);
            $query = $this->db->get('customerpets');
            if($query->num_rows() == 1) {
                $customerpetid = $query->row()->petid;
            }
            else {
                $customerpetid = $query->result_array();
            }

            if($customerpet>1)
            {
                //delete petmedication
                for($j=0;$j<$customerpet;$j++)
                {
                    $query = "delete from petmedication where petid = '{$customerpetid[$j]['petid']}' ";
                    $this->db->query($query);

                    $query = "delete from petvaccination where petid = '{$customerpetid[$j]['petid']}' ";
                    $this->db->query($query);
                    //delete pet
                    $query = "delete from pet where petid = '{$customerpetid[$j]['petid']}' ";
                    $this->db->query($query);
                }
            }
            else
            {
                $query = "delete from petmedication where petid = '{$customerpetid}' ";
                $this->db->query($query);

                $query = "delete from petvaccination where petid = '{$customerpetid}' ";
                $this->db->query($query);
                //delete pet
                $query = "delete from pet where petid = '{$customerpetid}' ";
                $this->db->query($query);
            }
            //delete customerpets
            $query = "delete from customerpets where customerid = '{$customerid}' ";
            $this->db->query($query);
            //foreign key constraints uncheck
            $query = "SET foreign_key_checks = 0";
            $this->db->query($query);

            //delete customers
            $query = "delete from customer where customerid ='{$customerid}' ";
            $this->db->query($query);

            //delete veterinarians
            $query = "delete from veterinarian where veterinarianid = '{$veterinarianid}' ";
            $this->db->query($query);

        }
        $query = "SET foreign_key_checks = 1";
        $this->db->query($query);
        return true;
    }
    function deleteVendor($vendorid)
    {
        $this->db->trans_begin();
        //delete promotions
        $query = "delete from promotions where vendorid = '{$vendorid}' ";
        $this->db->query($query);

        //delete appoinments
        $query1 = "delete from appointment where vendorid = '{$vendorid}' ";
        $this->db->query($query1);

        $this->deleteAppUsers($vendorid);

        $this->deleteSetting($vendorid);

        $this->deleteServices($vendorid);

        $this->deleteEmployees($vendorid);

        $this->deleteCustomers($vendorid);

        $query = "delete from vendor where vendorid = '{$vendorid}' ";
        $this->db->query($query);

        $this->db->trans_complete();

        if($this->db->trans_status() == FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }


    }
    function getsikkavendor($officeid)
    {
        $this->db->select('*');
        $this->db->where('sikkaoffice_id',$officeid);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }

    function getVendorId($officeid,$practice_id)
    {
        $this->db->select('vendorid');
        $this->db->where('sikkaoffice_id',$officeid);
        $this->db->where('sikkapractice_id',$practice_id);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }

    function getVendorSikkaOfficeId($vendorid)
    {
        $this->db->select('sikkaoffice_id,sikkapractice_id');
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }
    function saveLoginToken($loginToken,$vendorid){
        $this->db->where('vendorid',$vendorid);
        $result = $this->db->update('vendor',$loginToken);
        return $result;
    }

    function checkLoginToken($login_token){
        $this->db->select('*');
        $this->db->where('login_token',$login_token);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }
    function addFileCustomerPetWise($data){
        $this->db->insert_batch('vendorfiles',$data);
    }

    function getPetFile($petId,$vendorId){
        $sql = "select v.id,v.vendorid,v.petid,v.filename,v.fileurl,DATE_FORMAT(v.updatedon,'%M %d, %Y') as updatedon  FROM vendorfiles v WHERE v.vendorid = '{$vendorId}' AND v.petid = '{$petId}' and v.status = 2";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function getVendorFile($customerId,$vendorId){
        $sql = "select v.id,v.vendorid,v.petid,v.filename,v.fileurl,DATE_FORMAT(v.updatedon,'%M %d, %Y') as updatedon  FROM vendorfiles v WHERE v.vendorid = '{$vendorId}' and v.customerid='{$customerId}' and v.status = 1";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function deleteFile($id){
        $this->db->where('id',$id);
        $this->db->delete('vendorfiles');
        return $this->db->affected_rows();
    }

    function getVendorFileById($Id){
        $this->db->select('*');
        $this->db->where('id',$Id);
        $query = $this->db->get('vendorfiles');
        return $query->row_array();
    }

}

?>