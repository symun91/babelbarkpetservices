<?php

class Employeemodel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function employeeLogin($email, $password) {

        $this->db->select('*');

        $this->db->where('email', $email);
        $this->db->where('password like binary', $password);
        $query = $this->db->get('vendorusers');
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return '';
        }
    }

    function getEmployeesCount($vendorid) {
        $this->db->where('vendorid', $vendorid);
        $query = $this->db->get('vendorusers');
        return $query->num_rows();
    }
    
    function checkUniqueEmail($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('vendorusers');
        return $query->num_rows();
    }

    function employeestatus($vendorid, $userid) {
        $this->db->where('vendorid', $vendorid);
        $this->db->where('userid', $userid);
        $query = $this->db->get('vendorusers');
        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getEmployeeList($vendorid) {
        $this->db->where('vendorid', $vendorid);
        $query = $this->db->get('vendorusers');
        return $query->result_array();
    }

    function getAllEmployees($num, $offset, $vendorid) {
        $this->db->where('vendorid', $vendorid);
        $this->db->limit($num, $offset);
        $this->db->order_by('userid', 'DESC');
        $query = $this->db->get('vendorusers');
        return $query->result_array();
    }

    function updateemployee($datacc, $userid) {
        if ($this->employeeIdExists($userid) == TRUE) { //update
            $this->db->where('userid', $userid);
            $this->db->update('vendorusers', $datacc);
            return $userid;
        } else {
            $this->db->insert('vendorusers', $datacc);
            return $this->db->insert_id();
        }
    }

    function employeeIdExists($userid) {

        $this->db->select('userid');
        $this->db->where('userid', $userid);
        $result = $this->db->get('vendorusers');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function getEmployeeDetails($userid) {
        $this->db->select('*');
        $this->db->where('userid', $userid);
        $query = $this->db->get('vendorusers');
        return $query->row_array();
    }

    function deleteEmployee($userid) {
        $query = "delete from vendorusers where userid = '{$userid}' ";
        $this->db->query($query);
    }

}

?>
