<?php
class Appuserpromotionmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function updatePromotions($datacc,$promotionid,$appuserid)
	{
		  if ( $this->promotionIdExists($promotionid,$appuserid) == TRUE ) { //update
		  	  $this->db->where('appuserid',$appuserid);
		     $this->db->where('promotionid',$promotionid);
		     $this->db->update('appuserpromotions', $datacc);
		     return $promotionid;
		  }
		  else {
		  	  $this->db->insert('appuserpromotions', $datacc);
		     return $this->db->insert_id();
		  }
	}
function  promotionIdExists($promotionid,$appuserid){
		
		$this->db->select('promotionid');
		  $this->db->where('appuserid',$appuserid);
		  $this->db->where('promotionid',$promotionid);
		$result =  $this->db->get('appuserpromotions');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
function  promotionRedeemed($promotionid,$appuserid){
		
		$this->db->select('promotionid');
		  $this->db->where('appuserid',$appuserid);
		  $this->db->where('promotionid',$promotionid);
		  $this->db->where('status',"redeemed");
		$result =  $this->db->get('appuserpromotions');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function getWaitingRequests()
	{
		
		$this->db->where('status',"waiting");
		$query = $this->db->get('appuserpromotions');
		return $query->result_array();
	}
	
	function redeemPromotion($requestid)
	{
		$query = "update appuserpromotions set status='redeemed' where id = '{$requestid}' ";
		$query = $this->db->query($query);
	}
}
?>