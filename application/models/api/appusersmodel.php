<?php

class Appusersmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
		function requestAuth($userid,$sessiontoken)
		{
			$cd=date('Y-m-d H:i');
			$this->db->select('*');
			//$this->db->where('TIMESTAMPDIFF(HOUR,lastloggedon,\''.$cd.'\')<=',12);
			$this->db->where('appuserid', $userid);
			$this->db->where('sessiontoken', $sessiontoken);	
			$result =  $this->db->get('appusers');
			return ($result->num_rows() == 1) ? $result->row_array() : "expired";
	
		}
	function fetchUserID($email)
   {
   	    $this->db->select('appuserid');
		$this->db->where('email',$email);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       return $query->row()->appuserid;
		}
		else {
			   return 0;
		}
   }
function fetchUserIDByFBId($facebookid)
   {
   	    $this->db->select('appuserid');
		$this->db->where('fbid',$facebookid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       return $query->row()->appuserid;
		}
		else {
			   return 0;
		}
   }
    function userLogin($email,$password)
   {
   	 $this->db->select('appusers.appuserid');
   	 $this->db->join('appuserdevices' , 'appuserdevices.userid = appusers.appuserid');
		$this->db->where('email',$email);
		$this->db->where('password',$password);		
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       //return $query->row_array();
		    return $query->row()->appuserid;
		}
		else {
			   return 0;
		}
   }

   function getUserCity($userid)
   {
 		$this->db->select('city');
 		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       //return $query->row_array();
		    return $query->row()->city;
		}
		else {
			   return 0;
		}
   }
   
   function userFBLogin($facebookidencrypted,$facebookid)
   {
    	$this->db->select('appuserid');
		$this->db->where('fbid',$facebookid);
		$this->db->where('password',$facebookidencrypted);		
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       //return $query->row_array();
		    return $query->row()->appuserid;
		}
		else if($query->num_rows() > 1)
		{
			return -1;
		}
		else {
			   return 0;
		}
   }
   
	function registerUser($datacc)
	{
		     $this->db->insert('appusers', $datacc);
		     return $this->db->affected_rows();
	}
	
	
	function updateUser($datacc,$userid)
	{
		     $this->db->where('appuserid',$userid);
		     $this->db->update('appusers', $datacc);
		     return $this->db->affected_rows();
	}
	
	function addpet($datacc)
	{
		$this->db->insert('appuserpets', $datacc);
		$id =  $this->db->affected_rows();
		$this->updatecurrentpetid($datacc['appuserid'],$datacc['petid']);
		return $id;
	}

	function updatepetUser($datacc,$petid)
	{
		$this->db->where('appuserid',$userid);
		$this->db->update('appuserpets', $datacc);
		     return $this->db->affected_rows();
	}
	function updatecurrentpetid($userid,$petid)
	{
		$this->db->where('appuserid',$userid);
		$this->db->update('appusers',array('currentpetid' => $petid));
		 return $this->db->affected_rows();
	}
	function getPetID($userid)
	{
	 	$this->db->select('currentpetid');
		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) 
		{
			$petid = $query->row()->currentpetid;
		     if(empty($petid))
		     {
		     	$this->db->select('petid');
				$this->db->where('appuserid',$userid);
				$query = $this->db->get('appuserpets');
				if($query->num_rows() == 1)
				{
					$id = $this->updatecurrentpetid($userid,$query->row()->petid);
					return $query->row()->petid;
				}
				else
				{
					foreach($query->result() as $row)
						{
							$petid = $row->petid;
						    break; // No more iterating
						}
					$id = $this->updatecurrentpetid($userid,$petid);
					return $petid;
				}
		     }
		     else
		     {
		     	return $petid;
		     }
		}
		else 
		{
			   return 0;
		}
	}

	function getAllUserPets($userid)
	{
		$this->db->select('petid');
		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appuserpets');

		if($query->num_rows() == 0)
		{
			return 0;
		}
		else
		{
			return $query->result_array();
		}
	}

	 function isFacebookidExist($facebookid)
	  {
	   	    $this->db->select('appuserid');
			$this->db->where('fbid',$facebookid);
			$query = $this->db->get('appusers');
			return ($query->num_rows() == 1) ? 1 : 0;
	  }

	 function isEmailExists($email)
	  {
	   	    $this->db->select('appuserid');
			$this->db->where('email',$email);
			$query = $this->db->get('appusers');
            return ($query->num_rows() == 1) ? 1 : 0;
			//return $query->num_rows();
	  }
	function isEmailExistsForAUser($email,$userid)
	{
		$this->db->select('appuserid');
		$this->db->where('email',$email);
		$this->db->where('appuserid !=',$userid);
		$query = $this->db->get('appusers');
	    return ($query->num_rows() == 1) ? 1 : 0;
	}
	function getuserEmail($userid)
	{
		$this->db->select('email,firstname,lastname');
		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}
	}
	  
	   
	   function fetchUserDetails($userid)
   {
	    $this->db->select('appuserid,firstname,lastname,email,birthday,gender,address,city,state,zip,sessiontoken,phonenumber,country,isverified');	
		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}
   }
 function fetchUserDetails1($userid)
   {
	    $this->db->select('appuserid,firstname,lastname,email,birthday,gender,address,city,state,zip,sessiontoken,phonenumber,updatedon');	
		$this->db->where('appuserid',$userid);
		$query = $this->db->get('appusers');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}
   }
   
   function getPetOwners($petid)
   {
   	 $this->db->select('appusers.*');	
	   	 $this->db->join('appusers' , 'appuserpets.appuserid = appusers.appuserid');
	     $this->db->where('appuserpets.petid','$petid');
		$query = $this->db->get('appuserpets');		
		return $query->result_array();		
   }
   
   function getAppUserByResetToken($token){
   		$this->db->select('appuserid');	
		$this->db->where('resetpasswordtoken',$token);
		$query = $this->db->get('appusers');
		if($query->num_rows() > 0) {
			return $query->row_array();
		}
		else {
			return array();
		}
   }
   
function getUserByActivateToken($token)
	{
		 $this->db->select('*');	
		$this->db->where('activationtoken',$token);
		$query = $this->db->get('appusers');
		return $query->row_array();
	}

	function getUserVet($userid)
	{
		$this->db->select('primaryvet,emergencyvet');
		$this->db->from('appusers');
		$this->db->where('appuserid',$userid);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}


	}

	function getUserLostfound($userid)
	{
		$this->db->select('lostfoundvendor');
		$this->db->from('appusers');
		$this->db->where('appuserid',$userid);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}


	}
	function addToShareRecords($data){
        $result = $this->db->insert('sharerecords',$data);
        return $result;
    }
    function UpdateShareRecords($recordid,$filetype){
	    $data = array(
	        'status'=>0
        );
       $this->db->where('recordid',$recordid);
       $this->db->where('filetype',$filetype);
       $this->db->update('sharerecords',$data);
       return $this->db->affected_rows();
    }
   
}
	?>