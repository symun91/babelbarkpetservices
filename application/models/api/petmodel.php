<?php
class Petmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function registerPet($datacc)
    {
        $this->db->insert('pet', $datacc);
        return $this->db->affected_rows();
    }


    function updatePet($datacc,$petid)
    {
        $this->db->where('petid',$petid);
        $this->db->update('pet', $datacc);
        return $this->db->affected_rows();
    }

    function getPetDetails($petid)
    {
        $filePath = base_url().'view_image/';
        $sql = "select IFNULL(a.currentpetid,0) as currentpetid, p.petid, p.name , p.primarybreed, p.secondarybreed, p.primaryfood, p.secondaryfood, p.birthdate, p.gender, p.isadopted, 
                p.currentweight, p.targetweight, concat('{$filePath}',REPLACE(p.proilefpicture,'/','-')) as proilefpicture, p.furcolor, p.eyecolor, p.medicalhistory , p.amountoffeed, p.frequencyoffeed, p.insurancepolicyprovider, p.policyno, 
                p.pethubtagno, p.microchipped, p.microchipid, p.govtlicensetagno, p.govtlicensetagissuer, p.veterinarianid, p.misfitdevid, p.yearlycheckupdate, p.approximateage, p.birthdateupdatedon,
                p.birthdayunsure, p.isprescribed, p.profileCompletion, p.primaryvet, p.emergencyvet, p.lostfoundvendor, p.unitoffeed, p.guarantor_id, p.practice_id, p.patient_id, p.misfitdeviceid,          
                v.name as veterinarianname
                from pet AS p 
                Left join veterinarian AS v on v.veterinarianid = p.veterinarianid
                Left join appusers as a on a.currentpetid = p.petid
                where p.petid = '{$petid}'";
        $query = $this->db->query($sql);
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }

    function getPetVet($petid)
    {
        $this->db->select('primaryvet,emergencyvet');
        $this->db->from('pet');
        $this->db->where('petid',$petid);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }


    }
    function getPetLostfound($petid)
    {
        $this->db->select('lostfoundvendor');
        $this->db->from('pet');
        $this->db->where('petid',$petid);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }


    }

    function getPetDeviceID($petid)
    {
        $this->db->select('misfitdeviceid,name');
        $this->db->from('pet');
        $this->db->where('petid',$petid);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }




    function getPetVaccineDetails($petid)
    {
        $this->db->select('petvaccination.id as petvaccinationid,petvaccination.vaccineid,petvaccination.info,petvaccination.duedate');
        $this->db->where('petid',$petid);
        $query = $this->db->get('petvaccination');
        return $query->result_array();

    }

    function getPetMedicationDetails($petid)
    {
        $this->db->select('petmedication.petmedicationid,petmedication.medicineid,petmedication.frequency');
        $this->db->where('petid',$petid);
        $query = $this->db->get('petmedication');
        return $query->result_array();

    }

    function updatePet1($datacc,$petid)
    {

        if ( $this->petIdExists($petid) == TRUE ) { //update
            $this->db->where('petid',$petid);
            $this->db->update('pet', $datacc);
            return 1;
        }
        else // insert
        {
            $this->db->insert('pet', $datacc);
            return $this->db->affected_rows();
        }


    }


    function getPetFeedUnit($petid)
    {
        $this->db->select('unitoffeed');
        $this->db->where('petid',$petid);
        $result = $this->db->get('pet');
        return $result->row_array();
    }


    function  petIdExists($petid){

        $this->db->select('petid');
        $this->db->where('petid',$petid);
        $result =  $this->db->get('pet');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }
    function getPetName($petid)
    {
        $this->db->select('name');
        $this->db->where('petid',$petid);
        $query = $this->db->get('pet');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }
    function getpetprofilepercentage($petid)
    {
        $sql = "select p.measuredwt as currentweight,pt.targetweight,pt.profileCompletion from petweightlog p 
                left join pet pt on pt.petid = p.petid
                where p.petid = '{$petid}' and p.wtdatetime <= CURDATE() 
                order by p.wtdatetime desc 
                limit 1";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getmicrochipiddetails($petid)
    {
        $this->db->select('microchipid,govtlicensetagno,govtlicensetagissuer');
        $this->db->where('petid',$petid);
        $query = $this->db->get('pet');
        return $query->row_array();
    }

    function getPetFeedBrands($petid)
    {
        $this->db->select('primaryfood,secondaryfood');
        $this->db->where('petid',$petid);
        $query = $this->db->get('pet');
        return $query->row_array();
    }
}
?>