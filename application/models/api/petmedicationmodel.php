<?php
class Petmedicationmodel extends CI_Model {

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}

function addMedication($datacc)
	{
		     $this->db->insert('petmedication', $datacc);
            return $this->db->affected_rows();
	}

function deleteByPetID($petid)
	{
        $query = "delete from petmedication where petid = '{$petid}'";
		$this->db->query($query);
	}
	function updateByPetID($petid,$dataStatus){
	    $this->db->where('petid',$petid);
	    $this->db->where('status',1);
	    $this->db->update('petmedication',$dataStatus);
    }
    function updatePetMedication($petmedicationid,$datacc){
        $this->db->where('petmedicationid',$petmedicationid);
        $this->db->update('petmedication',$datacc);
    }

	function getPetMedicationForPet($petid)
	{
		$this->db->select('petmedication.petmedicationid,petmedication.medicineid as id,petmedication.frequency,medicines.name,medicines.url, petmedication.dosage,petmedication.medicationtime as time,petmedication.frequencytimes as timesOfFrequency,petmedication.medicationtimings as timings,petmedication.dosageunit as unit,petmedication.`status`,petmedication.updatedon');
        $this->db->join('medicines' , 'petmedication.medicineid = medicines.medicineid');
		$this->db->where('petmedication.petid',$petid);
		$query = $this->db->get('petmedication');
        return $query->result_array();

	}
	
	function getPetMedicationForPet1($petid)
	{
		$this->db->select('petmedication.petmedicationid,petmedication.medicineid ,petmedication.frequency,medicines.name, petmedication.dosage,petmedication.medicationtime');
		 $this->db->join('medicines' , 'petmedication.medicineid = medicines.medicineid');
		$this->db->where('petmedication.petid',$petid);
		$query = $this->db->get('petmedication');
		 return $query->result_array();

	}

function getAllMedications(){
		 $this->db->select('*');
		$query = $this->db->get('medicines');
		return $query->result_array();
	}



	   function fetchMedicineDetails($medicineid)
   {
	    $this->db->select('*');
		$this->db->where('medicineid',$medicineid);
		$query = $this->db->get('medicines');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return 0;
		}
   }
   
   function updateMedicationStatistics($datacc)
   {
   	  $this->db->insert('medicationstatistics', $datacc);
		     return $this->db->affected_rows();
   }
   
   function getMedicationStatistics($petid)
   {
   	$this->db->select('*');
		$this->db->where('petid',$petid);
		$query = $this->db->get('medicationstatistics');
		 return $query->result_array();
   }

   function medicinetradenames($medicineid){

   	    $this->db->select(' GROUP_CONCAT(tradename) AS tradename',false);
		$this->db->where('medicineid',$medicineid);
		$query = $this->db->get('medicinetradenames');
		 return $query->row_array();
   }
   
 function getmedicineAlltradenames($medicineid){

   	    $this->db->select('*');
		$this->db->where('medicineid',$medicineid);
		$query = $this->db->get('medicinetradenames');
		 return $query->result_array();
   }

}
?>