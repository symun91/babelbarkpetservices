<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class restapi extends REST_Controller
{

function __construct()
    {
    	 ini_set('display_errors', 1);
        // Construct our parent class
        parent::__construct();
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
         date_default_timezone_set('UTC');
		
		$this->load->helper(array('common_helper','url'));
		$this->load->library('encrypt');
		$this->load->model(array('appusersmodel','appuserdevicesmodel','petmodel','petmedicationmodel','petvaccinationmodel','petgoalsmodel','petlogmodel','veterinarianmodel','petfeedmodel','petbreedmodel','vendorsmodel','appuservendorsmodel','promotionsmodel','mailnotifymodel','nonregistervendormodule'));
    }

    //booking : mode= serving/waiting/finished
    //            status= completed/accepted/rejected/cancelled

 private static function genratePrimaryKey($serverid)
        {
		$primarykey=$serverid.time().mt_rand(1000, 9999);
			if($primarykey>9223372036854775807) //max of 64 bit int
			{
				genratePrimaryKey($serverid);
			}
		return $primarykey;
        }


 private static function genrateSessionToken()
        {
		$sessiontok=md5(microtime().rand());
		return $sessiontok;
        }

        /*
	 *  USER APIS
	 */
        /*
         * Login USER
         * Params- email,password,devicename,devicetoken,deviceuuid,isiphone
         */

       
   function loginUser_POST()
   {
	 	$email =  ($this->input->post('email')) ? $this->input->post('email') : 0;
		$password =  ($this->input->post('password')) ? $this->input->post('password') : 0;
        $devicename = $this->input->post('devicename');
        $devicetoken= $this->input->post('devicetoken');
        $deviceuuid= $this->input->post('deviceuuid');
        $isiphone= $this->input->post('isiphone');

        $userid = $this->appusersmodel->fetchUserID($email);
			if($this->appuserdevicesmodel->is_UserInDeviceListExists($userid) == 1)
					{
                         $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithUserid($devicecc , $userid);

						  $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);

					}
					else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
					{
                         $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
					}
					else {

                        $deviceid = $this->genratePrimaryKey('20');
						$devicecc = array(
			                           'deviceid' => $deviceid,
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,

			                           'isiphone'  => $isiphone
                                 );
						 $id = $this->appuserdevicesmodel->registerDevice($devicecc);

					}
		$userid = $this->appusersmodel->userLogin($email,md5($password));
        if($userid != 0)
		{
			$datacc=array();
        	$datacc['sessiontoken']=$this->genrateSessionToken();
        	$datacc['lastloggedon']=date('Y-m-d H:i');
        	$this->appusersmodel->updateUser($datacc,$userid);

        	$retarr=array();
        	$retarr=$this->appusersmodel->fetchUserDetails($userid);
        	$petid=$this->appusersmodel->getPetID($userid);
 $petdata=$this->petmodel->getPetDetails($petid);
if($petdata!=0)
{
	  if(count($petdata)>0)
		{
			$petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
			$petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
			$petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
			$petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
			$petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
			$petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
 $retarr['petdetails']= $petdata;
			}
                 }
else
{
$petdata=array();
$retarr['petdetails']=$petdata;
}

			$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => LOGINSUCCMSG,'data' =>$retarr), 200);
		}
		else {

			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => INVALIDLOGINMSG), 200);
		}
    }
    
    function loginUserWithFB_POST()
    {
    	
    	 $facebookid=$this->input->post('facebookid');
    	 $username=$this->input->post('username');
       	 $devicename = $this->input->post('devicename');
        $devicetoken= $this->input->post('devicetoken');
        $deviceuuid= $this->input->post('deviceuuid');
        $isiphone= $this->input->post('isiphone');

        $userid = $this->appusersmodel->fetchUserIDByFBId($facebookid);
        
			if($this->appuserdevicesmodel->is_UserInDeviceListExists($userid) == 1)
					{
                         $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithUserid($devicecc , $userid);

						  $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);

					}
					else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
					{
                         $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
					}
					else {

                        $deviceid = $this->genratePrimaryKey('20');
						$devicecc = array(
			                           'deviceid' => $deviceid,
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,

			                           'isiphone'  => $isiphone
                                 );
						 $id = $this->appuserdevicesmodel->registerDevice($devicecc);

					}
		 $userid = $this->appusersmodel->userFBLogin($username,$facebookid);
        if($userid != 0)
		{
			$datacc=array();
        	$datacc['sessiontoken']=$this->genrateSessionToken();
        	$datacc['lastloggedon']=date('Y-m-d H:i');
        	$this->appusersmodel->updateUser($datacc,$userid);

        	$retarr=array();
        	$retarr=$this->appusersmodel->fetchUserDetails($userid);
        	$petid=$this->appusersmodel->getPetID($userid);
 $petdata=$this->petmodel->getPetDetails($petid);
if($petdata!=0)
{
	  if(count($petdata)>0)
		{
			$petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
			$petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
			$petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
			$petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
			$petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
			$petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
 $retarr['petdetails']= $petdata;
			}
                 }
else
{
$petdata=array();
$retarr['petdetails']=$petdata;
}

			$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => LOGINSUCCMSG,'data' =>$retarr), 200);
		}
		else {

			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => INVALIDLOGINMSG), 200);
		}
    }

    function signUp_post()
    {
    		 $bool = TRUE;
    	 if($this->input->post('email')) {
		     $email = $this->input->post('email');
		 }
		 else {
			  $bool = FALSE;
		 }
		 if($this->input->post('password')) {
		     $password = $this->input->post('password');
		 }
		 else {
			  $bool = FALSE;
		 }
		 	if($this->input->post('devicename')) {
		     $devicename = $this->input->post('devicename');
		}
		else {
			$bool =FALSE;
		}

		if($this->input->post('devicetoken')) {
		     $devicetoken= $this->input->post('devicetoken');
		}
		else {
			$bool =FALSE;
		}
		if($this->input->post('deviceuuid')) {
		     $deviceuuid= $this->input->post('deviceuuid');
		}
		else {
			$bool =FALSE;
		}
		if($this->input->post('isiphone') || $this->input->post('isiphone')==0) {
		     $isiphone= $this->input->post('isiphone');
		}
		else {
			$bool =FALSE;
		}
                if($this->input->post('username')) {
		     $username= $this->input->post('username');
		}
		else {
			$bool =FALSE;
		}

		
	
		 $facebookid =  ($this->input->post('facebookid')) ? $this->input->post('facebookid') : "";
		 
		 
		$userid = $this->genratePrimaryKey('40');

		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}

	    if($this->appusersmodel->isEmailExists($email) == 1) {
	    	$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => EMAILEXISTMSG), 200);
			return false;

	    }

$activationtoken=$this->genrateSessionToken();
	    $datacc = array(
	    'appuserid' => $userid ,
		               'email' => $email,
		               'password' => md5($password),
                               'username' => $username,
	    				'sessiontoken'=> $this->genrateSessionToken(),
        			'lastloggedon'=>date('Y-m-d H:i'),
	    			'fbid' => $facebookid,
	      'isverified' => 0,
	   'activationtoken'=>$activationtoken
		);
      
		$id = $this->appusersmodel->registerUser($datacc);

		if($id > 0)
		{
			//add default vendor
			 /*($datacc=array();
			   $datacc['appuserid']=$userid;
	 		 $datacc['vendorids']='123656767'; //for testing
			$this->appuservendorsmodel-> updateVendors($datacc,$userid);*/
			
		   if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
					{
						 $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
						  $this->appuserdevicesmodel->deleteByDeviceID($deviceuuid,$userid);
					}
					else if($this->appuserdevicesmodel->is_DeviceExists($deviceuuid) == 1)
					{
                         $deviceid = 0;
			             $devicecc = array(
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						  $id = $this->appuserdevicesmodel->updateDeviceWithDeviceUUID($devicecc , $deviceuuid);
					}
				   else {
						$deviceid = $this->genratePrimaryKey('20');
						$devicecc = array(
			                           'deviceid' => $deviceid,
			                           'userid' => $userid,
			                           'devicename' => $devicename,
			                           'devicetoken' => $devicetoken,
			                           'deviceuuid'  => $deviceuuid ,
			                           'isiphone'  => $isiphone
                                 );
						 $id = $this->appuserdevicesmodel->registerDevice($devicecc);
					}

				//send registration mail to activate account
				$subject = "BabelBark Account Verification";
				$toemail=$email;
			// To send HTML mail, the Content-type header must be set.
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From:' . SITE_MAIL. "\r\n"; // Sender's Email
				$sendmessage  = file_get_contents('resources/appuser_activate_template.html'); 
		 		//$sendmessage = str_replace('%username%', $username, $sendmessage); 
			
				$url = base_url().'activateappuseraccount?token='.$activationtoken;
				$sendmessage = str_replace('%activateurl%', $url, $sendmessage); 
				//mail($toemail, $subject, $sendmessage, $headers);
				send_template_mail('',$sendmessage, $subject, $toemail, SITE_MAIL);
				
			 $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => REGSUCCMSG,'data' =>$this->appusersmodel->fetchUserDetails($userid)), 200);
		}
		else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
		}
    }
		/*
         * profile setup app USER
         * Params- firstname,lastname,email,password,birthdate,gender,address,city,state,zip,devicename,devicetoken,deviceuuid,isiphone
         */
     function setUserProfile_post()	 {
	 	 $bool = TRUE;
     $userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}
	     if($this->input->post('firstname')) {
	         $firstname= $this->input->post('firstname');
		 }
		 else {
			 $firstname = "";
		 }
        if($this->input->post('lastname')) {
	         $lastname= $this->input->post('lastname');
		 }
		 else {
			 $lastname = "";
		 }
		 if($this->input->post('email')) {
		     $email = $this->input->post('email');
		 }
		 else {
			  $bool = FALSE;
		 }

		 if($this->input->post('birthdate')) {
		     $birthdate = $this->input->post('birthdate');
		 }
		 else{   // added by debartha
                     $birthdate = "";
                 }

		if($this->input->post('gender')) {
		     $gender = $this->input->post('gender');
		}
                else{   // added by debartha
                     $gender = "";
                }


     if($this->input->post('address')) {
		     $address = $this->input->post('address');
		}
     else{   // added by debartha
                     $address = "";
                }
     if($this->input->post('country')) {
		     $country = $this->input->post('country');
		}
     else{   // added by debartha
             $country = "";
     }
     if($this->input->post('city')) {
		     $city = $this->input->post('city');
		}
     else{   // added by debartha
                     $city = "";
                }
     if($this->input->post('state')) {
		     $state = $this->input->post('state');
		}
     else{   // added by debartha
                     $state = "";
                }
     if($this->input->post('zip')) {
		     $zip = $this->input->post('zip');
		}
     else{   // added by debartha
                     $zip = "";
                }
     if($this->input->post('phonenumber')) {  // added by debartha
		     $phonenumber = $this->input->post('phonenumber');
		}
     else{
                     $phonenumber = "";
                }


		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}

	   /*  if($this->appusersmodel->isEmailExists($email) == 1) {
	    	$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => EMAILEXISTMSG), 200);
			return false;

	    } */  // Commented by Debartha


	    $datacc = array(
		               'firstname' => $firstname,
	    				'lastname'=>$lastname,
		               'email' => $email,
		              // 'password' => md5($password),
		               'birthday' => $birthdate,
	    				'gender' => $gender,
		               'appuserid' => $userid ,
		               'address' => $address,
	    			   'city' => $city,
	                   	 'state' =>$state,
                               'phonenumber' => $phonenumber,    // added by debartha
	      				'zip' => $zip,
	      				'country' => $country,
	    				'sessiontoken'=> $this->genrateSessionToken(),
        			'lastloggedon'=>date('Y-m-d H:i')

		);

		 $this->appusersmodel->updateUser($datacc,$userid);
			$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEPROFSUCCMSG,'data' => $this->appusersmodel->fetchUserDetails($userid)),200);

	 }
	/*
	 * UPDATE PROFILE
	 * params- firstname,lastname,email,birthdate,gender,address,city,state,zip
	 */
	function updateUserProfile_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}

		    $datacc = array();
	        if($this->post('email')) {
			      $email = $this->post('email');
						if($this->appusersmodel->isEmailExistsForAUser($email,$userid) == 1)
			{
						$this->response(array('statuscode' => '400','statusmsg' => 'success','statusdesc' => EMAILEXISTMSG),200);
						return false;;
			}
			      $datacc['email'] = $email;
			 }

		 if($this->post('firstname')) {
			       $firstname = $this->post('firstname');
			      $datacc['firstname'] = $firstname;
			 }

			 if($this->post('lastname')) {
			       $lastname = $this->post('lastname');
			      $datacc['lastname'] = $lastname;
			 }
			/*

	        if($this->post('password')) {
			       $password = $this->post('password');
			      $datacc['password'] = md5($password);
			 }*/

			 if($this->post('birthdate')) {
			       $birthdate= $this->post('birthdate');
			      $datacc['birthday'] = $birthdate;
			 }
			if($this->input->post('gender')) {
				     $language = $this->input->post('gender');
				      $datacc['gender'] = $gender;
				}
	if($this->input->post('address')) {
		     $address = $this->input->post('address');
		      $datacc['address'] = $address;
		}

     if($this->input->post('city')) {
		     $city = $this->input->post('city');
		     $datacc['city'] = $city;
		}
     if($this->input->post('state')) {
		     $state = $this->input->post('state');
		     $datacc['state'] = $state;
		}
		
	if($this->input->post('country')) {
		     $country = $this->input->post('country');
		     $datacc['country'] = $country;
		}	
		
     if($this->input->post('zip')) {
		     $zip = $this->input->post('zip');
		     $datacc['zip'] = $zip;
		}
if($this->input->post('phonenumber')) {
		     $phonenumber = $this->input->post('phonenumber');
 $datacc['phonenumber'] = $phonenumber;
		}

			if(count($datacc) > 0)
			 $this->appusersmodel->updateUser($datacc,$userid);
			$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEPROFSUCCMSG,'data' => $this->appusersmodel->fetchUserDetails($userid)),200);

	}

function getUserProfile_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}

	  $this->response(array('statuscode' => '200','statusmsg' => 'error' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->appusersmodel->fetchUserDetails($userid)), 200);
	}

	function setPetProfile_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}

	$datacc = array();
		 $bool = TRUE;
	if($this->post('petname')) {
			  $petname = $this->post('petname');
			  $datacc['name'] = $petname;
	}
	else {
		$bool=false;
	}
	if($this->post('primarybreed')) {
			  $primarybreed = $this->post('primarybreed');
			  $datacc['primarybreed'] = $primarybreed;
	}
	else {
		$bool=false;
	}
	if($this->post('secondarybreed')) {
			  $secondarybreed = $this->post('secondarybreed');
			  $datacc['secondarybreed'] = $secondarybreed;
	}
	if($this->post('primaryfood')) {
			  $primaryfood = $this->post('primaryfood');
			  $datacc['primaryfood'] = $primaryfood;
	}
	else {
		$bool=false;
	}
	if($this->post('secondaryfood')) {
			  $secondaryfood = $this->post('secondaryfood');
			  $datacc['secondaryfood'] = $secondaryfood;
	}
	if($this->post('birthdate')) {
			  $birthdate = $this->post('birthdate');
			  $datacc['birthdate'] = $birthdate;
	}
	else {
		$bool=false;
	}
	if($this->input->post('isadopted') || $this->input->post('isadopted')==0){
			  $isadopted = $this->post('isadopted');
			  $datacc['isadopted'] = $isadopted;
	}
if($this->input->post('approximateage') ){
			  $approximateage = $this->post('approximateage');
			  $datacc['approximateage'] = $approximateage;
	}
	if($this->post('gender')) {
			  $gender = $this->post('gender');
			  $datacc['gender'] = $gender;
	}
	else {
		$bool=false;
	}
	if($this->post('currentweight')) {
			  $currentweight = $this->post('currentweight');
			  $datacc['currentweight'] = $currentweight;
	}
	else {
		$bool=false;
	}
	if($this->post('targetweight')) {
			  $targetweight = $this->post('targetweight');
			  $datacc['targetweight'] = $targetweight;
	}
	if($this->input->post('ismicrochipped') || $this->input->post('ismicrochipped')==0){
			  $microchipped = $this->post('ismicrochipped');
			  $datacc['microchipped'] = $microchipped;
	}
	if($this->post('microchipid')) {
			  $microchipid = $this->post('microchipid');
			  $datacc['microchipid'] = $microchipid;
	}
	if($this->post('govtlicensetagno')) {
			  $govtlicensetagno = $this->post('govtlicensetagno');
			  $datacc['govtlicensetagno'] = $govtlicensetagno;
	}
	if($this->post('govtlicensetagissuer')) {
			  $govtlicensetagissuer = $this->post('govtlicensetagissuer');
			  $datacc['govtlicensetagissuer'] = $govtlicensetagissuer;
	}
	if($this->post('veterinarianid')) {
			  $veterinarianid = $this->post('veterinarianid');
			  $datacc['veterinarianid'] = $veterinarianid;
	}else {
		$bool=false;
	}
	if($this->post('yearlycheckupdate')) {
			  $yearlycheckupdate = $this->post('yearlycheckupdate');
			  $datacc['yearlycheckupdate'] = $yearlycheckupdate;
	}
	if($this->post('misfitdevid')) {
			  $misfitdevid = $this->post('misfitdevid');
			  $datacc['misfitdevid'] = $misfitdevid;
	}

	if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}
	$petid = $this->genratePrimaryKey('40');

	 $datacc['petid']=$petid;
	$id = $this->petmodel->registerPet($datacc);

		if($id > 0)
		{
			//add user pet
			 $datacc=array();
			  $datacc['appuserid']=$userid;
			 $datacc['petid']=$petid;
			 $this->appusersmodel->addpet($datacc);

	//profile pic
	  $imgurl = '';
		 if(isset($_FILES['profileimage'])) {
		 $allowed =  array('gif','png' ,'jpg' ,'jpeg');
		 $filename = $_FILES['profileimage']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {

				    $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => INVALIDFILEEXTMSG),200);
					return false;;
				}
					 if(is_uploaded_file($_FILES['profileimage']['tmp_name']))
						{
                          //  $uploaddir = '/www/readysettext/resources/uploads/campaignimage';
                          if (!file_exists('resources/uploads/petimage/')) {
    						mkdir('resources/uploads/petimage/');
							}
							$uploaddir = realpath('./resources/uploads/petimage/');

							$ext =  end(explode('.', $_FILES['profileimage']['name']));
			                $fname=$petid.'_'.time().".".$ext;
							$uploadfile = $uploaddir ."/". $fname;
							if (move_uploaded_file($_FILES['profileimage']['tmp_name'], $uploadfile)) {
								$imgurl='resources/uploads/petimage/'.$fname;
							}
						 }
						 	$datacc=array();
			       			$datacc['proilefpicture']=$imgurl;
						 $this->petmodel->updatePet($datacc,$petid);
		       }


	//add medication
		       if($this->post('medications')) {
		       	$medicationjson=$this->post('medications');
		       	$medobj = json_decode($medicationjson, true);
		       	foreach($medobj['medication'] as $item)
		       	{
			       	$datacc=array();
			       	$datacc['petid']=$petid;
			       	$datacc['medicineid']=$item['id'];
			       	$datacc['frequency']=$item['frequency'];
                    $datacc['dosage']=$item['dosage'];
                    $datacc['medicationtime']=$item['time'];
                    

			       	$this->petmedicationmodel->addMedication($datacc);
		       	}

		       }

	//add vaccination
		  if($this->post('vaccinations')) {
		       	$vaccjson=$this->post('vaccinations');
		       	$vacobj = json_decode($vaccjson, true);
		       	foreach($vacobj['vaccination'] as $item)
		       	{
			       	$datacc=array();
			       	$datacc['petid']=$petid;
			       	$datacc['vaccineid']=$item['id'];
			       	$datacc['duedate']=$item['duedate'];
			       	$this->petvaccinationmodel->addVaccination($datacc);
		       	}

		       }
		       $petdata=$this->petmodel->getPetDetails($petid);
	  if(count($petdata)>0)
		{
			$petdata['vaccination']=$this->petmodel->getPetVaccineDetails($petid);
			$petdata['medication']=$this->petmodel->getPetMedicationDetails($petid);
			$petdata['feedtitile']=$this->petfeedmodel->getFeedTitle($petdata['primaryfood']);
			$petdata['breedtitile']=$this-> petbreedmodel-> getBreedTitle($petdata['primarybreed']);
		
			$petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
			$petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
			$petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
			$petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
			}

	 $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data' =>$petdata),200);
		}

		else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
		}
	}

	function updatePetProfile_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}

	  $petid= $this->post('petid');
	 $datacc=array();

	 $datacc['petid']=$petid;
	if($this->post('petname')) {
			  $petname = $this->post('petname');
			  $datacc['name'] = $petname;
	}
	if($this->post('primarybreed')) {
			  $primarybreed = $this->post('primarybreed');
			  $datacc['primarybreed'] = $primarybreed;
	}
	if($this->post('secondarybreed')) {
			  $secondarybreed = $this->post('secondarybreed');
			  $datacc['secondarybreed'] = $secondarybreed;
	}
	if($this->post('primaryfood')) {
			  $primaryfood = $this->post('primaryfood');
			  $datacc['primaryfood'] = $primaryfood;
	}
	if($this->post('secondaryfood')) {
			  $secondaryfood = $this->post('secondaryfood');
			  $datacc['secondaryfood'] = $secondaryfood;
	}
	if($this->post('birthdate')) {
			  $birthdate = $this->post('birthdate');
			  $datacc['birthdate'] = $birthdate;
	}
	if($this->input->post('isadopted') || $this->input->post('isadopted')==0){
			  $isadopted = $this->post('isadopted');
			  $datacc['isadopted'] = $isadopted;
	}
if($this->input->post('approximateage') ){
			  $approximateage = $this->post('approximateage');
			  $datacc['approximateage'] = $approximateage;
	}
	if($this->post('gender')) {
			  $gender = $this->post('gender');
			  $datacc['gender'] = $gender;
	}
	if($this->post('currentweight')) {
			  $currentweight = $this->post('currentweight');
			  $datacc['currentweight'] = $currentweight;
	}
	if($this->post('targetweight')) {
			  $targetweight = $this->post('targetweight');
			  $datacc['targetweight'] = $targetweight;
	}
	if($this->input->post('ismicrochipped') || $this->input->post('ismicrochipped')==0){
			  $microchipped = $this->post('ismicrochipped');
			  $datacc['microchipped'] = $microchipped;
	}
	if($this->post('microchipid')) {
			  $microchipid = $this->post('microchipid');
			  $datacc['microchipid'] = $microchipid;
	}
	if($this->post('govtlicensetagno')) {
			  $govtlicensetagno = $this->post('govtlicensetagno');
			  $datacc['govtlicensetagno'] = $govtlicensetagno;
	}
	if($this->post('govtlicensetagissuer')) {
			  $govtlicensetagissuer = $this->post('govtlicensetagissuer');
			  $datacc['govtlicensetagissuer'] = $govtlicensetagissuer;
	}
	if($this->post('veterinarianid')) {
			  $veterinarianid = $this->post('veterinarianid');
			  $datacc['veterinarianid'] = $veterinarianid;
	}
	if($this->post('yearlycheckupdate')) {
			  $yearlycheckupdate = $this->post('yearlycheckupdate');
			  $datacc['yearlycheckupdate'] = $yearlycheckupdate;
	}
	if($this->post('misfitdevid')) {
			  $misfitdevid = $this->post('misfitdevid');
			  $datacc['misfitdevid'] = $misfitdevid;
	}



	$id =  $this->petmodel->updatePet($datacc,$petid);

		//if($id > 0)
		//{


	//profile pic
	  $imgurl = '';

		 if(isset($_FILES['profileimage'])) {
		 $allowed =  array('gif','png' ,'jpg' ,'jpeg');
		 $ext =  end(explode('.', $_FILES['profileimage']['name']));
				//$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {

				    $this->response(array('statuscode' => '400','statusmsg' => 'error','statusdesc' => INVALIDFILEEXTMSG),200);
					return false;;
				}
					 if(is_uploaded_file($_FILES['profileimage']['tmp_name']))
						{
                          //  $uploaddir = '/www/readysettext/resources/uploads/campaignimage';
                          if (!file_exists('resources/uploads/petimage/')) {
    						mkdir('resources/uploads/petimage/');
							}
							$uploaddir = realpath('./resources/uploads/petimage/');


			                $fname=$petid.'_'.time().".".$ext;
							$uploadfile = $uploaddir ."/". $fname;
							if (move_uploaded_file($_FILES['profileimage']['tmp_name'], $uploadfile)) {
								
								$imgurl='resources/uploads/petimage/'.$fname;
								$datacc = array();
								$datacc['proilefpicture']=$imgurl;
						 		$this->petmodel->updatePet($datacc,$petid);
							}
						 }

		       }


	//update medication

		       if($this->post('medications')) {
		       	 //delete old medications
		       	  	$this->petmedicationmodel->deleteByPetID($petid);
		       	$medicationjson=$this->post('medications');
		       	$medobj = json_decode($medicationjson, true);
		       	foreach($medobj['medication'] as $item)
		       	{
			       	$datacc=array();
			       	$datacc['petid']=$petid;
			       	$datacc['medicineid']=$item['id'];
			       	$datacc['frequency']=$item['frequency'];
                    $datacc['dosage']=$item['dosage'];
                     $medicationtime = "08:00";
                    $datacc['medicationtime'] = $medicationtime;
                    $datacc['medicationtimings'] = $medicationtime;
                    if(array_key_exists('remindersTime', $item))
                    {
                    $reminder = $item['remindersTime'];
                   
                  // $medicationtime =   $reminder[0]['time'];
                    for($i=0;$i<count($reminder);$i++)
                    {
                    	if($i==0)
                    	{
                    		$medicationtime = $reminder[$i]['time'];
                    	}else{
                    		$medicationtime =$medicationtime.",".$reminder[$i]['time'];
                    	}
                    }

                      $datacc['medicationtimings'] = $medicationtime;
                   
                    }

					 if(array_key_exists('timesOfFrequency', $item))
                    {
                     $datacc['frequencytimes']=$item['timesOfFrequency'];
                     }
                    
                 	
                 	
			       	$this->petmedicationmodel->addMedication($datacc);
		       	}

		       }

	//update vaccination
		  if($this->post('vaccinations')) {
		  	 //delete old vaccinations
		       	  	$this->petvaccinationmodel->deleteByPetID($petid);
		       	$vaccjson=$this->post('vaccinations');
		       	$vacobj = json_decode($vaccjson, true);
		       	foreach($vacobj['vaccination'] as $item)
		       	{
			       	$datacc=array();
			       	$datacc['petid']=$petid;
			       	$datacc['vaccineid']=$item['id'];
			       	$datacc['duedate']=$item['duedate'];
			       	$this->petvaccinationmodel->addVaccination($datacc);
		       	}

		       }

     $petDetails = $petdata=$this->petmodel->getPetDetails($petid);

     $data['proilefpicture'] = $petDetails['proilefpicture'];
     $data['name'] = $petDetails['name'];

	 $this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG,'data' => $data),200);
		//}

		//else {
			//$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
		//}
	}

	function getPetProfile_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
	    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
			return false;
	      }

	$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
	if(array_key_exists("error", $reqauthresult))
	{
		$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
		 return false;
	}

	  $petid= $this->post('petid');
	  $result=array();
	  $petdata=$this->petmodel->getPetDetails($petid);
	  if(count($petdata)>0)
		{
			$petdata['vaccination']=$this->petvaccinationmodel->getPetVaccineDetails($petid);
			$petdata['medication']=$this->petmedicationmodel->getPetMedicationForPet($petid);
			$petdata['primaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['primaryfood']);
			$petdata['primarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['primarybreed']);
			$petdata['secondaryfeed']=$this->petfeedmodel->getFeedDetails($petdata['secondaryfood']);
			$petdata['secondarybreed']=$this-> petbreedmodel-> getBreedDetails($petdata['secondarybreed']);
			}
	  $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$petdata), 200);
	}

	function setPetActivityGoals_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $petid= $this->post('petid');
	  $points= $this->post('points');

	  $datacc=array();
	  $datacc['petid']=$petid;
	  $datacc['points']=$points;
	  $affrows=$this->petgoalsmodel->addActivityGoals($datacc,$petid);
	  if($affrows > 0)
		{
	   	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		}

		else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
		}

	}


function setPetWeightGoals_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $petid= $this->post('petid');
	  $weight= $this->post('weight');

	  $datacc=array();
	  $datacc['petid']=$petid;
	  $datacc['weight']=$weight;
	  $affrows=$this->petgoalsmodel->addWeightGoals($datacc,$petid);
	  if($affrows > 0)
		{
	   	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		}

	  else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
	   }

	}

function setPetMedicalGoals_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $petid= $this->post('petid');
	  $dosagehours= $this->post('dosagehours');

	  $datacc=array();
	  $datacc['petid']=$petid;
	  $datacc['dosagehours']=$dosagehours;
	  $affrows=$this->petgoalsmodel->addMedicalGoals($datacc,$petid);
	  if($affrows > 0)
		{
	   	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		}

	  else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
	   }

	}


	function updatePetJournal_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}

		$petid= $this->post('petid');

	if($this->post('medicineid')) {
		$bool=true;
			 $datacc=array();
	 		 $datacc['petid']=$petid;
			  $datacc['petmedicationid'] = $this->post('medicineid');
			 if($this->input->post('dosage')) {
				     $datacc['dosage']= $this->input->post('dosage');
				}
				else {
					$bool =FALSE;
				}
	 		 if($this->input->post('medicationdatetime')) {
				      $datacc['medicationdatetime']= $this->input->post('medicationdatetime');
				}
				else {
					$bool =FALSE;
				}
		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}

		$this->petlogmodel->addMedicationLog($datacc);
	}

	if($this->post('feedbrandid')) {
		$bool=true;
			 $datacc=array();
	 		 $datacc['petid']=$petid;
			  $datacc['feedbrandid'] = $this->post('feedbrandid');
			 if($this->input->post('feedamount')) {
				     $datacc['amount']= $this->input->post('feedamount');
				}
				else {
					$bool =FALSE;
				}
	 		 if($this->input->post('feeddatetime')) {
				     $datacc['feeddatetime']= $this->input->post('feeddatetime');
				}
				else {
					$bool =FALSE;
				}
		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}

		$this->petlogmodel->addFeedingLog($datacc);
	}

	if($this->post('measuredweight')) {
		$bool=true;
			 $datacc=array();
	 		 $datacc['petid']=$petid;
			  $datacc['measuredwt'] = $this->post('measuredweight');
			 if($this->input->post('weightdatetime')) {
				     $datacc['wtdatetime']= $this->input->post('weightdatetime');
				}
				else {
					$bool =FALSE;
				}

		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}

		$this->petlogmodel->addWeightLog($datacc);
	}

	if($this->post('yearlycheckupdate')) {
			  $yearlycheckupdate = $this->post('yearlycheckupdate');
			   $datacc=array();
			  $datacc['yearlycheckupdate'] = $yearlycheckupdate;
			  $this->petmodel->updatePet($datacc,$petid);

	}
		$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);

	}

	function addWeightChange_post()
	{

		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}

		$petid= $this->post('petid');
		$bool=true;
	if($this->post('measuredweight')) {
			  $datacc['measuredwt'] = $this->post('measuredweight');
	}
	else {
					$bool =FALSE;
		}

			 if($this->input->post('weightdatetime')) {
				     $datacc['wtdatetime']= $this->input->post('weightdatetime');
				}
				else {
					$bool =FALSE;
				}

		if(!$bool)
		{
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
			return false;
		}
	    $datacc=array();
	 		 $datacc['petid']=$petid;

		$affrows=$this->petlogmodel->addWeightLog($datacc);
		 if($affrows > 0)
		{
	   	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		}
	  else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
	   }
	}

	function getPetOwners_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}

		$petid= $this->post('petid');

		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->appusersmodel->getPetOwners($petid)), 200);
	}

	function getAllVeterinarians_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}


		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->veterinarianmodel->getAllVeterinarians()), 200);
	}

function getAllMedicines_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$AllMedications = $this->petmedicationmodel->getAllMedications();
		//print_r($AllMedications);
  $result=array();
		if(!empty($AllMedications)){
			$cnt=0;
			foreach ($AllMedications as $key => $value) {
					/*$medicinetradenames = $this->petmedicationmodel->medicinetradenames($value['medicineid']);
					$AllMedications[$key]['tradename'] = $medicinetradenames['tradename'];	//fullname
					$AllMedications[$key]['fullname'] = $value['name'].' ('.$medicinetradenames['tradename'].')';*/
				$medicineid=$value['medicineid'];
				$medicinetradenames=$this->petmedicationmodel->getmedicineAlltradenames($medicineid);
				
				for($i=0;$i<count($medicinetradenames);$i++)
				{
					$result[$cnt]['medicineid']=$medicineid;
					$result[$cnt]['name']=$value['name'];
					$result[$cnt]['url']=$value['url'];
					$result[$cnt]['tradename']=$medicinetradenames[$i]['tradename'];
					$result[$cnt]['fullname']=$value['name'].' ('.$medicinetradenames[$i]['tradename'].')';
					$cnt++;
				}

				}	
		}
		

		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' => $result), 200);
	}

	function getMedicineDeatails_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$medicineid = $this->post('medicineid');

		$this->response(array('statuscode' => '200','statusmsg' => 'error' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petmedicationmodel->fetchMedicineDetails($medicineid)), 200);
	}

function getAllVaccines_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}


		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petvaccinationmodel->getAllVaccinations()), 200);
	}


function getVaccineDetails_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$vaccineid = $this->post('vaccineid');

		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petvaccinationmodel->fetchVaccineDetails($vaccineid)), 200);
	}


function getAllFeedBrands_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}


		$this->response(array('statuscode' => '200','statusmsg' => 'error' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petfeedmodel->getAllFeedBrands()), 200);
	}

function getAllBreeds_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}


		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petbreedmodel->getAllBreeds()), 200);
	}


	function getVendorsByCategory_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

	if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}

		$selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);
		if($selectedvendors!="")
		{
		$arrVendors = explode(',', $selectedvendors);

		$categories=array('walker','sitter','grooming','petstore','veterinarian','lostfoundservices');
		$result=array();
		for($i=0;$i<count($categories);$i++)
		{
			$categoryname=$categories[$i];
			$categoryvendors=$this->vendorsmodel->getVendorsByCategory($categoryname);

                        for($j=0;$j<count($categoryvendors);$j++)
		{
                     $vendorid=$categoryvendors[$j]['vendorid'];
                           if (in_array($vendorid, $arrVendors))
                                $categoryvendors[$j]['isSelectedByUser']=1;
                           else
                               $categoryvendors[$j]['isSelectedByUser']=0;
                                  }
                             $result[$categoryname]=$categoryvendors;
		}
		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$result), 200);
		}
		
		else {
			
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR), 200);
		
		}
		}
	function updateSelectedVendors_POST()
	{

	$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
			$vendorids = $this->post('vendorids');
			  $datacc=array();
			   $datacc['appuserid']=$userid;
	 		 $datacc['vendorids']=$vendorids;
			$this->appuservendorsmodel-> updateVendors($datacc,$userid);
				$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
	}
	function getSelectedVendors_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);
		// $arrVendors = explode(',', $selectedvendors);
		//
		//
		// $result=array();
		// for($i=0;$i<count($arrVendors);$i++)
		// {
		//
		// 	$result[$i]=$this->vendorsmodel->getVendorDetails($arrVendors[$i]);
		// }
		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$selectedvendors), 200);
	}



	function getPetMedication_POST()
	{
	  $userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$petid= $this->post('petid');
		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$this->petmedicationmodel->getPetMedicationForPet($petid)), 200);

	}
	
	function getPromotions_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$userdetails=$this->appusersmodel->fetchUserDetails($userid);
		$promotionsresult=array();
		$promotionsresult['isverified']=$userdetails['isverified'];
		
		$selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);
		if($selectedvendors!="")
		{
		$arrVendors = explode(',', $selectedvendors);
		
		$petid=$this->appusersmodel->getPetID($userid);
		$petdata=$this->petmodel->getPetDetails($petid);
		$petage=$petdata['approximateage'];
		$petweight=$petdata['currentweight'];
		$petbreed=$petdata['primarybreed'];
		$petacivityscore=$this->petlogmodel->getPetActivityPoints($petid);
		
		
		
		
		
		$promotionscnt=0;
		
		for($i=0;$i<count($arrVendors);$i++)
		{
			$vendorid=$arrVendors[$i];
			$promotions=$this->promotionsmodel->getAllPromotionsForApp($vendorid);
			for($j=0;$j<count($promotions);$j++)
			{
				$valid=true;
				
				$targetsubscribedcustomers=$promotions[$j]['targetsubscribedcustomers'];
				
				if($targetsubscribedcustomers=='1') //target all
				{
					$valid=true;
				}
				else {
				$promotionagefrom=$promotions[$j]['agerangefrom'];
				$promotionageto=$promotions[$j]['agerangeto'];
				if($promotionagefrom!="" && $promotionagefrom!="0")
				{
					if($petage<$promotionagefrom)
					   $valid=false;
				}
				if($promotionageto!="" && $promotionageto!="0")
				{
					if($petage>$promotionageto)
					   $valid=false;
				}
				
				$promotionwtfrom=$promotions[$j]['targetweightsfrom'];
				$promotionwtto=$promotions[$j]['targetweightsto'];
				if($promotionwtfrom!="" && $promotionwtfrom!="0")
				{
					if($petweight<$promotionagefrom)
					   $valid=false;
				}
				if($promotionageto!="" && $promotionageto!="0")
				{
					if($petweight>$promotionageto)
					   $valid=false;
				}
				
				$prmotionbreeds=$promotions[$j]['targetbreeds'];
				if($prmotionbreeds!="" && $prmotionbreeds!=0)
				{
					$arrPromBreeds = explode(',', $prmotionbreeds);
					if(!in_array($petbreed, $arrPromBreeds))
						$valid=false;
				}
				
				
				$promotionactscorefrom=$promotions[$j]['targetactivityscoresmin'];
				$promotionactscoreto=$promotions[$j]['targetactivityscoresmax'];
				if($promotionactscorefrom!="" && $promotionactscorefrom!="0")
				{
					if($petacivityscore<$promotionactscorefrom)
					   $valid=false;
				}
				if($promotionactscoreto!="" && $promotionactscoreto!="0")
				{
					if($petacivityscore>$promotionactscoreto)
					   $valid=false;
				}
			}
				
				if($valid)
				{
				  $promotionsresult[]=$promotions[$j];
				  $promotionscnt++;
				}
				
			}
		}
		if($promotionscnt>0)
			$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$promotionsresult), 200);
		else 
			$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$promotionsresult), 200);
		
		}
	else {
			
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => NOSELECTEDVENDOR,'data' =>$promotionsresult), 200);
		
		}
		
	}
	
	function updateAcitivityScore_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		$petid = $this->post('petid');
		$actualpoints = $this->post('points');
		$syncdate= $this->post('syncdate');
		$syncdon = $this->post('syncdon');
		$totalsteps= $this->post('totalsteps');
		$variance= $this->post('variance');
		 $datacc=array();
	   $datacc['petid']=$petid;
	   $datacc['actualpoints']=$actualpoints;
	   $datacc['syncdate']=$syncdate;
	   $datacc['syncdon']=$syncdon;
	   $datacc['totalsteps']=$totalsteps;
	   $datacc['variance']=$variance;
	   $this->petlogmodel->addActivityLog($datacc);
	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		
	}
	
	
function updateMedicationStatistics_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $petid= $this->post('petid');
	  $code= $this->post('code');
	  $month= $this->post('month');
	  $percentage= $this->post('percentage');
	  $week= $this->post('week');

	  $datacc=array();
	  $datacc['petid']=$petid;
	  $datacc['appuserid']=$userid;
	  $datacc['code']=$code;
	  $datacc['month']=$month;
	  $datacc['percentage']=$percentage;
	  $datacc['week']=$week;
	  $affrows=$this->petmedicationmodel->updateMedicationStatistics($datacc);
	  if($affrows > 0)
		{
	   	$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
		}

		else {
			$this->response(array('statuscode' => '400','statusmsg' => 'error' ,'statusdesc' => GENERALERRMSG), 200);
		}

	}
	
	function syncPetLogs_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $petid= $this->post('petid');
	  $resultarr=array();
	   $resultarr['medicationstatistics']=$this->petmedicationmodel->getMedicationStatistics($petid);
	   $resultarr['activitylog']=$this->petlogmodel->getActivityLog($petid);
	   $resultarr['weightlog']=$this->petlogmodel->getWeightLog($petid);
	   $this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
	}
	
	
	function getNearestVendors_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  $categoryname= $this->post('category');
	    $latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
	  
		if($categoryname=="Lost & Found Service")
		{
			//$categoryname="lostfoundservices";
			$categoryname=7;			
		}
		if($categoryname == "lostfoundservices")
		{
			$categoryname=7;
		}
		
	  $registeredvendorstemp=$this->vendorsmodel->getVendorById($categoryname);
	  $registeredvendors=array();
	  for($i=0;$i<count($registeredvendorstemp);$i++)
	  {
	  //	$formatted_address=$registeredvendorstemp[$i]['address'].",".$registeredvendorstemp[$i]['city'].",".$registeredvendorstemp[$i]['state'].",".$registeredvendorstemp[$i]['country'].",".$registeredvendorstemp[$i]['zipcode'];
	  	$formatted_address=$registeredvendorstemp[$i]['address'];
	  	if($formatted_address!="")
	  			$formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['city'];
	  	if($formatted_address!="")
	  		$formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['state'];
	  	if($formatted_address!="")
	  		$formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['country'];	
	  	if($formatted_address!="")
	  		$formatted_address=$formatted_address.",".$registeredvendorstemp[$i]['zipcode'];
	  		
	  	if($formatted_address=="")
	  		$formatted_address="-";
	  	
	  	$registeredvendors[$i]['id']=$registeredvendorstemp[$i]['vendorid'];
	  	$registeredvendors[$i]['name']=$registeredvendorstemp[$i]['firstname']." ".$registeredvendorstemp[$i]['lastname'];
	  	$registeredvendors[$i]['address']=$formatted_address;
	  	$registeredvendors[$i]['phoneno']=$registeredvendorstemp[$i]['contact'];
	  	$registeredvendors[$i]['types']=array($registeredvendorstemp[$i]['category']);
	    $registeredvendors[$i]['email']=$registeredvendorstemp[$i]['email'];
	  	$registeredvendors[$i]['url']="";
	  	$registeredvendors[$i]['website']=$registeredvendorstemp[$i]['website'];
	 	$registeredvendors[$i]['openinghours']="";
	    $registeredvendors[$i]['rating']="";
	  	$registeredvendors[$i]['isregistered']=1;
	  }
	  
	  
	  $mapapikey="AIzaSyDX-EBXYQghOXyVOPbC-G7ChnQrYUTzXis"; //map api key
	  
	
		$latlng = $latitude.",".$longitude;

		$srchtype="petstore";
		
		$keyword="pet_store";
		if($categoryname=="walker")
		{
			$keyword="pet walker";
		}
		else if($categoryname=="sitter")
		{
			$keyword="pet sitting";
		}
	else if($categoryname=="grooming")
		{
			$keyword="pet grooming";
		}
	else if($categoryname=="petstore")
		{
			$keyword="pet_store";
		}
	else if($categoryname=="veterinarian")
		{
			$keyword="veterinary";
		}
	else if($categoryname=="lostfoundservices" || $categoryname=="Lost & Found Service")
		{
			$keyword="pet lost and found services";
		}
		$placedetailurl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$latlng."&radius=16000&keyword=".urlencode($keyword)."&rankBy=distance&sensor=false&key=".$mapapikey;
	
	  	$detailjson = file_get_contents($placedetailurl);
		$nearbyvendorsdata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);
		
		
		$serchdetailarr=array();
		$serchdetailarr=$nearbyvendorsdata['results'];
		
		 $searchedvendors=array();
		for($i=0;$i<count($serchdetailarr);$i++)
		{
			$placeid=$serchdetailarr[$i]['place_id'];
			$placedetailurl ="https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".$mapapikey;
		 	$detailjson = file_get_contents($placedetailurl);
			$placedata=json_decode(str_replace("&quot;","\"",htmlentities($detailjson)),true);
			
			$formatted_address=$placedata['result']['formatted_address'];
			$searchedvendors[$i]['id']="";
			$searchedvendors[$i]['name']=$placedata['result']['name'];
		  	$searchedvendors[$i]['address']=$formatted_address;
		  	if( isset( $placedata['result']['international_phone_number'] ))
		  		$searchedvendors[$i]['phoneno']=$placedata['result']['international_phone_number'];
		  	else
		  		$searchedvendors[$i]['phoneno']="";
		  		
		  	$searchedvendors[$i]['types']=$placedata['result']['types'];
		    $searchedvendors[$i]['email']="";
		    if( isset( $placedata['result']['url'] ))
		  		$searchedvendors[$i]['url']=$placedata['result']['url'];
		  	else
		  		$searchedvendors[$i]['url']="";
		  		
		  	 if( isset( $placedata['result']['website'] ))
		  	 	$searchedvendors[$i]['website']=$placedata['result']['website'];
		  	 else
		  		$searchedvendors[$i]['website']="";
		  		
		  	if( isset( $placedata['result']['openinghours'] ))
		 		$searchedvendors[$i]['openinghours']=$placedata['result']['opening_hours'];
		 	 else
		  		$searchedvendors[$i]['openinghours']="";
		  		if(isset( $serchdetailarr[$i]['opening_hours']))
		  			$searchedvendors[$i]['openinghours']=$serchdetailarr[$i]['opening_hours'];
		  			 else
		  		$searchedvendors[$i]['openinghours']="";
		  		
		  	if( isset( $placedata['result']['user_ratings_total'] ))
		   	 $searchedvendors[$i]['rating']=$placedata['result']['user_ratings_total'];
		     else
		  		$searchedvendors[$i]['rating']="";
		  	$searchedvendors[$i]['isregistered']=0;
		}
		
		$resultarr=array_merge($registeredvendors,$searchedvendors);
		
		$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
		
	}
	function getAllSelectedVendors_post()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		
		 $categories=array('walker','sitter','grooming','petstore','veterinarian','lostfoundservices');
		$selectedvendors=$this->appuservendorsmodel->getVendorsForUser($userid);
		 $arrVendors = explode(',', $selectedvendors);
		
		
		for($i=0;$i<count($categories);$i++)
		{
		 	$categoryname=$categories[$i];
			$categoryvendors=$this->vendorsmodel->getVendorsByCategory($categoryname);
					$regivendroeresult=array();
					$cnt=0;
                        for($j=0;$j<count($categoryvendors);$j++)
						{
                     		$vendorid=$categoryvendors[$j]['vendorid'];
                           if (in_array($vendorid, $arrVendors))
                           {
                            	$regivendroeresult[$cnt]['id']=$categoryvendors[$j]['vendorid'];
							 	$regivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
							 	$regivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
							 	$regivendroeresult[$cnt]['email']=$categoryvendors[$j]['email'];
							 	$formatted_address=$categoryvendors[$j]['address'].",".$categoryvendors[$j]['city'].",".$categoryvendors[$j]['state'].",".$categoryvendors[$j]['country'].",".$categoryvendors[$j]['zipcode'];
							 	$regivendroeresult[$cnt]['address']=$formatted_address;
							 	$regivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['contact'];
						  		$regivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
						   		$regivendroeresult[$cnt]['url']="";
						   		$regivendroeresult[$cnt]['isregistered']=1;
						   		$regivendroeresult[$cnt]['logoimage']=$categoryvendors[$j]['logoimage'];
						   		
						   		$cnt++;
                           }
                           
                           
                           }
                             $result[$categoryname]=$regivendroeresult;
		}
		//print_r($result);
		/*$regivendroeresult=array();
		 for($i=0;$i<count($arrVendors);$i++)
		 {
		
		 	$detail=$this->vendorsmodel->getVendorDetails($arrVendors[$i]);
		 	$regivendroeresult[$i]['id']=$detail['vendorid'];
		 	$regivendroeresult[$i]['firstname']=$detail['firstname'];
		 	$regivendroeresult[$i]['lastname']=$detail['lastname'];
		 	$regivendroeresult[$i]['email']=$detail['email'];
		 	$formatted_address=$detail['address'].",".$detail['city'].",".$detail['state'].",".$detail['country'].",".$detail['zipcode'];
		 	$regivendroeresult[$i]['address']=$formatted_address;
		 	$regivendroeresult[$i]['phoneno']=$detail['contact'];
	  		$regivendroeresult[$i]['type']=$detail['category'];
	   		$regivendroeresult[$i]['url']="";
	   		$regivendroeresult[$i]['isregistered']=1;
	  		
		 }*/
		 $result1=array();
		 $arrVendors =array();
		 $selectednonregivendors=$this->nonregistervendormodule->getNonregiVendorsForUser($userid);
		 for($i=0;$i<count($selectednonregivendors);$i++)
		 {
		 	$arrVendors[$i]=$selectednonregivendors[$i]['nonregisteredvendorid'];
		 }
		
	 //$arrVendors = explode(',', $selectednonregivendors);
		
		for($i=0;$i<count($categories);$i++)
		{
		 	$categoryname=$categories[$i];
			$categoryvendors=$this->nonregistervendormodule->getNonregiVendorsByCategory($categoryname);
			
					$nonregivendroeresult=array();
					$cnt=0;
                        for($j=0;$j<count($categoryvendors);$j++)
						{
                     		$vendorid=$categoryvendors[$j]['nonregisteredvendorid'];
                           if (in_array($vendorid, $arrVendors))
                           {
                            	$nonregivendroeresult[$cnt]['id']=$categoryvendors[$j]['nonregisteredvendorid'];
							 	$nonregivendroeresult[$cnt]['firstname']=$categoryvendors[$j]['firstname'];
							 	$nonregivendroeresult[$cnt]['lastname']=$categoryvendors[$j]['lastname'];
							 	$nonregivendroeresult[$cnt]['email']="";
							 	$formatted_address=$categoryvendors[$j]['streetaddress'].",".$categoryvendors[$j]['city'].",".$categoryvendors[$j]['state'].",".$categoryvendors[$j]['zip'];
							 	$nonregivendroeresult[$cnt]['address']=$formatted_address;
							 	$nonregivendroeresult[$cnt]['phoneno']=$categoryvendors[$j]['phone'];
						  		$nonregivendroeresult[$cnt]['type']=$categoryvendors[$j]['category'];
						   		$nonregivendroeresult[$cnt]['url']=$categoryvendors[$j]['url'];
						   		$nonregivendroeresult[$cnt]['isregistered']=0;
						   		$nonregivendroeresult[$cnt]['logoimage']="";
						   		
						   		$cnt++;
                           }
                           
                           
                           }
                             $result1[$categoryname]=$nonregivendroeresult;
		}
		 
		 /*$nonregivendroeresult=array();
		 for($i=0;$i<count($selectednonregivendors);$i++)
		 {
		
		 	$detail=$this->nonregistervendormodule->getNonregiVendorDetails($selectednonregivendors[$i]['nonregisteredvendorid']);
		 	$nonregivendroeresult[$i]['id']=$detail['nonregisteredvendorid'];
		 	$nonregivendroeresult[$i]['firstname']=$detail['firstname'];
		 	$nonregivendroeresult[$i]['lastname']=$detail['lastname'];
		 	$nonregivendroeresult[$i]['email']="";
		 	$formatted_address=$detail['streetaddress'].",".$detail['city'].",".$detail['state'].",".$detail['zip'];
		 	$nonregivendroeresult[$i]['address']=$formatted_address;
		 	$nonregivendroeresult[$i]['phoneno']=$detail['phone'];
	  		$nonregivendroeresult[$i]['type']="pet_store";
	   		$nonregivendroeresult[$i]['url']=$detail['url'];
	   		$nonregivendroeresult[$i]['isregistered']=0;
	   		$nonregivendroeresult[$i]['logoimage']="";
		 }
		  $result1["petstore"]=$nonregivendroeresult;*/
		  
	$resultarr=array_merge_recursive($result, $result1);
	$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => FETCHDATASUCCMSG,'data' =>$resultarr), 200);
	}
	
	
	function test_get()
	{

		       	$medicationjson='{
    "medication": [
      {"id": "1", "frequency": "2"},
{"id": "2", "frequency": "1"},
{"id": "3", "frequency": "4"}
]

}';
		       	$medobj = json_decode($medicationjson, true);
		       	foreach($medobj['medication'] as $item)
		       	{
		       		echo $item['id'].":".$item['frequency'];
		       	}
	}
	/*
	 *
	 */


function userRequestAuth($userid,$sessiontoken)
	{
		$retarr=array();
		//authenticate the session token
		$authresult=  $this->appusersmodel->requestAuth($userid,$sessiontoken);
		 if($authresult=="expired")
		{
			 $retarr['error'] =SESSIONEXPRDMSG;
		}
		else
		{
			$retarr=$authresult;
		}
                   $retarr=array(); //for demo
		return $retarr;
	}

function forgotPassword_POST(){
	$email = $this->post('email');

	if($email == ''){
		$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => MANDATORYFIELDsERRMSG), 200);
		return false;
	}

	$emailExistCheck = $this->appusersmodel->isEmailExists($email);

	if($emailExistCheck){
		$appUserId = $this->appusersmodel->fetchUserID($email);
		
		//Generate reset password token and save to appuser table
		$resetpasswordtoken = $this->genrateSessionToken();
		$updateData['resetpasswordtoken'] = $resetpasswordtoken;
		if($this->appusersmodel->updateUser($updateData,$appUserId)){
			//send reset password link via email
			$this->mailnotifymodel->sendResetPasswordLink($appUserId,$resetpasswordtoken);
			$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => 'success'), 200);

		} else {
			$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => GENERALERRMSG), 200);
			return false;
		}
	} else {
		$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => EMAILNOTEXIST), 200);
		return false;
	}

}

function inviteuser_POST(){
	$email = $this->post('email');
    $appUserId = $this->post('userid');
	if($email == ''){
		$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
		return false;
	}
	
	if($appUserId == ''){
		$this->response(array('statuscode' => '400','statusmsg' => 'success' ,'statusdesc' => REQUIREDFIELD), 200);
		return false;
	}

    $this->mailnotifymodel->inviteuser($appUserId,$email);
	$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => EMAILSENT), 200);

}

function addrNonRegisterVendor_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
	  
		$firstname 	= $this->post('firstname');
		$lastname = $this->post('lastname');
		$distance = $this->post('distance');
		$phone = $this->post('phone');
		$streetaddress = $this->post('streetaddress');
		$city = $this->post('city');
		$state = $this->post('state');
		$zip = $this->post('zip');
		$url = $this->post('url');
		$category = $this->post('category');

		$nonregisteredvendorid = $this->genratePrimaryKey('40');

		$insertArray = array(
				'nonregisteredvendorid' => $nonregisteredvendorid,
				'firstname' => $firstname,
				'lastname'  => $lastname,
				'distance'  => $distance,
				'phone'		=> $phone,
				'streetaddress' => $streetaddress,
				'city'		=> $city,
				'state'		=> $state,
				'zip'		=> $zip,
				'url'		=> $url,
				'category'		=> $category,
				
			);
		if($this->nonregistervendormodule->insertNonRegisterVendor($insertArray)){

			$appUserInsertArray = array(
				'appuserid' => $userid,
				'nonregisteredvendorid' => $nonregisteredvendorid,
				);
			if($this->nonregistervendormodule->insertAppuserNonRegVendors($appUserInsertArray)){
				$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => VENDORREGSUCCMSG), 200);
				return false;	
			} 
		} else {
			$this->response(array('statuscode' => '200','statusmsg' => 'success' ,'statusdesc' => GENERALERRMSG), 200);
			return false;
		}


	   
	}

	
function removeNonRegisterVendor_POST()
	{
		$userid = $this->post('userid');
		$deviceid = $this->post('deviceid');
		$sessiontoken=$this->post('sessiontoken');
		$nonregisteredvendorid=$this->post('vendorid');

		if($this->appuserdevicesmodel->deviceValidation($userid , $deviceid) == 0) {
		    	$this->response(array('statuscode' => '409','statusmsg' => 'error','statusdesc' => DEVICEWIPEDOUTMSG),200);
				return false;
		      }

		$reqauthresult=$this->userRequestAuth($userid,$sessiontoken); //authenticate request
		if(array_key_exists("error", $reqauthresult))
		{
			$this->response(array('statuscode' => '401','statusmsg' => 'error','statusdesc' => SESSIONEXPRDMSG),200);
			 return false;
		}
		
		$this->nonregistervendormodule->removeNonregivendorsForUser($userid,$nonregisteredvendorid);
		$this->response(array('statuscode' => '200','statusmsg' => 'success','statusdesc' => UPDATEDATASUCCMSG),200);
	}

}
?>
