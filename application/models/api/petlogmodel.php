<?php
class Petlogmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function addMedicationLog($datacc)
    {
        $this->db->insert('petmedicationlog', $datacc);
        return $this->db->affected_rows();
    }

    function addFeedingLog($datacc)
    {
        $this->db->insert('petfeedinglog', $datacc);
        return $this->db->affected_rows();
    }

    function addWeightLog($datacc)
    {
        $this->db->insert('petweightlog', $datacc);
        return $this->db->affected_rows();
    }
    function addActivityLog($datacc)
    {
        $this->db->insert('petactivitylog', $datacc);
        return $this->db->affected_rows();
    }
    function getPetActivityPoints($petid)
    {
        $this->db->select_sum('actualpoints');
        $this->db->where('petid',$petid);
        $query = $this->db->get('petactivitylog');

        return $query->row()->actualpoints;

    }

    function getActivityLog($petid)
    {
        $this->db->select('*');
        $this->db->where('petid',$petid);
        $query = $this->db->get('petactivitylog');
        return $query->result_array();
    }
    function getWeightLog($petid)
    {
        $this->db->select('*');
        $this->db->where('petid',$petid);
        $query = $this->db->get('petweightlog');
        return $query->result_array();
    }


    function getMedicationLog($petid)
    {
        $tomorrow = date('Y-m-d H:i:s', strtotime('tomorrow'));
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 1) 
                AND `petid` =  '{$petid}'
                AND `medicationdatetime` < '{$tomorrow}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getMedicationLogOfthisMonth($petid)
    {
        $date = date('Y-m');
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 1) 
                AND `petid` =  '{$petid}'
                AND  `medicationdatetime`  LIKE '%$date%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getYesterdayMedication($petid)
    {
        $yesterday = date('Y-m-d H:i:s', strtotime('-1 day',strtotime('yesterday')));
        $tomorrow = date('Y-m-d H:i:s', strtotime('today'));
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 1) 
                AND `petid` =  '{$petid}'
                AND `medicationdatetime` > '{$yesterday}'
                AND `medicationdatetime` < '{$tomorrow}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getMedicationLogInactive($petid)
    {
        $tomorrow = date('Y-m-d H:i:s', strtotime('tomorrow'));
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 0) 
                AND `petid` =  '{$petid}'
                AND `medicationdatetime` < '{$tomorrow}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getMedicationLogOfthisMonthInactive($petid)
    {
        $date = date('Y-m');
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 0) 
                AND `petid` =  '{$petid}'
                AND  `medicationdatetime`  LIKE '%$date%'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getYesterdayMedicationInactive($petid)
    {
        $yesterday = date('Y-m-d H:i:s', strtotime('-1 day',strtotime('yesterday')));
        $tomorrow = date('Y-m-d H:i:s', strtotime('today'));
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` IN (select DISTINCT(petmedicationid) from petmedication where petid = '{$petid}' and status = 0) 
                AND `petid` =  '{$petid}'
                AND `medicationdatetime` > '{$yesterday}'
                AND `medicationdatetime` < '{$tomorrow}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

}
?>