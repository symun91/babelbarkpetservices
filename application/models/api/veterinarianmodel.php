<?php
class Veterinarianmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
	
	function getAllVeterinarians(){
		 $this->db->select('*');		   
		$query = $this->db->get('veterinarian');		
		return $query->result_array();		
	}
	
	function addVeterinarian($datacc)
	{
		  $this->db->insert('veterinarian', $datacc);
		   return $this->db->insert_id();
	}
	
	
	function getVetDetails($veterinarianid)
	{
	  $this->db->select('*');	
		$this->db->where('veterinarianid',$veterinarianid);
		$query = $this->db->get('veterinarian');
		return $query->row_array();
	}
	
function updateVet($datacc,$veterinarianid)
	{
		  if ( $this->vetIdExists($veterinarianid) == TRUE ) { //update
		    	$this->db->where('veterinarianid',$veterinarianid);
		     $this->db->update('veterinarian', $datacc);
		     return $veterinarianid;
		  }
		  else {
		  	  $this->db->insert('veterinarian', $datacc);
		     return $this->db->insert_id();
		  }
	}
function  vetIdExists($veterinarianid){
		
		$this->db->select('veterinarianid');
		$this->db->where('veterinarianid',$veterinarianid);
		$result =  $this->db->get('veterinarian');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
}

?>