<?php
class Sikkapetimportrequestsmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}

	function insertSikkaImportRequest($data)
	{
		$this->db->insert('sikkapetimportrequests',$data);
		return $this->db->insert_id();
	}

	function getupdatedOn($vendorid,$petid)
	{
		$this->db->select('updatedon');
		$this->db->where('vendorid',$vendorid);
		$this->db->where('apppetid',$petid);
		$this->db->limit(1);
		$query = $this->db->get('sikkapetimportrequests');
		if($query->num_rows() == 1)
		{
			return $query->row()->updatedon;
		}
		else{
			return "";
		}



	}
}