<?php
class Petbreedmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function getAllBreeds(){
		 $this->db->select('*');		   
		$query = $this->db->get('petbreed');		
		return $query->result_array();		
	}
	
	function getBreedTitle($breedid)
	{
	 $this->db->select('title');
		$this->db->where('breedid',$breedid);
		$query = $this->db->get('petbreed');
		if($query->num_rows() == 1) {
		       return $query->row()-> title;
		}
		else
		return "";
	}
	function getBreedDetails($breedid)
	{
	 $this->db->select('*');
			$this->db->where('breedid',$breedid);
		$query = $this->db->get('petbreed');
		 return $query->row_array();
		
	}
}
	
	?>