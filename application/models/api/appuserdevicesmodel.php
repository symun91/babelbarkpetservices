<?php
class Appuserdevicesmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
public function is_UserInDeviceListExists($userid)
	{
		$query = "SELECT * FROM appuserdevices where userid = {$userid}";
		$query = $this->db->query($query);
		return ($query->num_rows() > 0) ? 1 : 0;
	
	}
 function updateDeviceWithUserid($datacc , $userid)
	{
		    $this->db->where('userid' , $userid);
			 $this->db->update('appuserdevices', $datacc);
		     return $this->db->affected_rows();
	}
	
	function deleteByDeviceID($deviceuuid,$userid)
	{
		 $query = "delete from appuserdevices where deviceuuid = '{$deviceuuid}' and userid != {$userid}";
		$this->db->query($query);
	}
	
	function updateDeviceWithDeviceUUID($datacc , $deviceuuid)
	{
		     $this->db->where('deviceuuid' , $deviceuuid);
		     $this->db->update('appuserdevices', $datacc);
		     return $this->db->affected_rows();
	}
	
	function registerDevice($datacc)
	{
		     $this->db->insert('appuserdevices', $datacc);
		     return $this->db->affected_rows();
	}
	
 function is_DeviceExists($deviceuuid)
	{
		$query = "SELECT * FROM appuserdevices where deviceuuid = '{$deviceuuid}'";
		$query = $this->db->query($query);
		return ($query->num_rows() > 0) ? 1 : 0;
	
	}
	
	public function deviceValidation($userid , $deviceuuid)
	{
		$query = "SELECT * FROM appuserdevices where userid = {$userid} and deviceuuid = '{$deviceuuid}'";
		$query = $this->db->query($query);
		return ($query->num_rows() > 0) ? 1 : 0;

	}
}
?>