<?php

class Mailnotifymodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
			$this->load->helper(array('common_helper','url'));
	}

	function sendResetPasswordLink($userId,$token){

		$userDetails = $this->appusersmodel->fetchUserDetails($userId);
		

		$username = $userDetails['firstname'];
		$emailId = $userDetails['email'];
		$url = base_url() . 'resetpassword?token='.$token;

		$subject = 'BabelBark Password';
		$msg = '
		<span style="font-size:16px;color:#6B6B6D;font-family:Comic Sans,sans-serif;">
		Hello <b>'.$username.'</b>,<br><br>
		To set or reset your password. <a href="'.$url.'">Tap Here!</a><br><br>
		If you did not request a password reset, please ignore this email.<br>
		If you have any questions or concerns please contact us at <a href="mailto:info@babelbark.com">info@babelbark.com</a> <br><br>
		
		Thank you!<br>
		BabelBark <br>
		617.957.7383					
		</span>';
		$this->load->library('email');
				$config['protocol'] = "smtp";
				$config['smtp_host'] = "ssl://smtp.gmail.com";
				$config['smtp_port'] = "465";
				$config['smtp_user'] = SITE_MAIL; 
				$config['smtp_pass'] = "doogyWOOGY!";
				$config['charset'] = "utf-8";
				$config['mailtype'] = "html";
				$config['newline'] = "\r\n";

				$this->email->initialize($config);
				$this->email->from(SITE_MAIL_SUPPORT, 'BabelBark');
				$this->email->to($emailId);
				$this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
				$this->email->subject($subject);

				//$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
				
				$this->email->message($msg);

				$result =$this->email->send();
	}
	function invitevendor($userId, $email)
	{
		$userDetails = $this->appusersmodel->fetchUserDetails($userId);
		
		$firstname = $userDetails['firstname'];
		
		$lastname = $userDetails['lastname'];

		$email_user = $userDetails['email'];
		
		$subject = 'BabelBark Invitaion';
		$msg = '
		<span style="font-size:16px;color:#6B6B6D;font->
family:Comic Sans,sans-serif;">
		Hello!<br		 <b>'.$firstname.' '.$lastname.'</b> has invited you to join BabelBark!<br><br>
		
		BabelBark is a mobile app and software platform that connects pet parents, pet vendors and veterinarians to help dogs have the happiest and healthiest life possible - visit our site at <a href="https://babelbark.com/">https://babelbark.com/</a> to learn more!
  <br><br>
		An all-in-one platform for all your pet needs, join now! <br><br>
		
		<br>
		<img src="http://bizbark.com/assets/images/logobabelbark.png" width="100"><br>
		617.957.7383<br>
		<a mailto:"info@babelbark.com">info@babelbark.com</a>					
		</span>				
		';
		

		$this->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = SITE_MAIL; 
		$config['smtp_pass'] = "doogyWOOGY!";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);
		$this->email->from(SITE_MAIL, 'BabelBark');
		$this->email->to($email);
		$this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
		$this->email->subject($subject);
		$this->email->cc($email_user);
		
		$this->email->message($msg);


		 $result =$this->email->send();



	}

   function inviteuser($userId, $email){

		$userDetails = $this->appusersmodel->fetchUserDetails($userId);
		
		$firstname = $userDetails['firstname'];
		
		$lastname = $userDetails['lastname'];

		$email_user = $userDetails['email'];
		
		$subject = 'BabelBark Invitaion';
		$msg = '
		<span style="font-size:16px;color:#6B6B6D;font->
family:Comic Sans,sans-serif;">
		Hello!<br		 <b>'.$firstname.' '.$lastname.'</b> has invited you to join BabelBark!<br><br>
		
		BabelBark is a mobile app and software platform that connects pet parents, pet vendors and veterinarians to help dogs have the happiest and healthiest life possible - visit our site at <a href="https://babelbark.com/">https://babelbark.com/</a> to learn more!
  <br><br>
		An all-in-one platform for all your pet needs, join now! <br><br>
		
		<br>
		<img src="'.base_url().'assets/images/logobabelbark.png" width="100"><br>
		617.957.7383<br>
		<a mailto:"info@babelbark.com">info@babelbark.com</a>					
		</span>				
		';
		

		$this->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = SITE_MAIL; 
		$config['smtp_pass'] = "doogyWOOGY!";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);
		$this->email->from(SITE_MAIL, 'BabelBark');
		$this->email->to($email);
		$this->email->reply_to(SITE_MAIL_SUPPORT, 'BabelBark');
		$this->email->subject($subject);
		$this->email->cc($email_user);
		$this->email->message($msg);
		 $result =$this->email->send();
	}
	function sendmedicalrequest($toemail,$ccemail,$token)
	{
		$subject = 'Need a copy of my pet\'s medical records.';
		$msg = '<p>Hello!</p>
			<p>I\'m maintaining my pet\'s record on BabelBark app. Kindly upload my pet\'s medical reports on <a href="http://bizbark.com/uploadMedicalFile?token='+$token+'">THIS LINK</a>.</p>
<p>In case you face any problems then please email me.</p>
 <br> <br>
Have a Great Day!
  <br> <br>
		 <img src="http://bizbark.com/assets/images/logo.png" width="150"/>
			 <br>
			617.957.7383
			 <br>
			info@babelbark.com';
		$this->email->from('admin@babelbark.com', 'BabelBark');
		$this->email->to($toemail);
		$this->email->set_mailtype('html');
		$this->email->cc($ccemail);
		$this->email->subject($subject);
		//$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
		$this->email->message($msg);
		return $this->email->send();


	}

}