<?php
class Invitationtokenmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
		
	}
	
 function addInvitation($datacc)
	{
		     $this->db->insert('invitationtoken', $datacc);
		     return $this->db->insert_id();
	}
	
	function isInvitationExist($userid,$vendorid)
	{
		$this->db->select('id');
		$this->db->where('appuserid',$userid);
		$this->db->where('fromvendorid',$vendorid);
		$result =  $this->db->get('invitationtoken');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function getInvitationByToken($token)
	{
		 $this->db->select('*');	
		$this->db->where('token',$token);
		$query = $this->db->get('invitationtoken');
		return $query->row_array();
	}
	
	function deleteByToken($token)
	{
		 $query = "delete from invitationtoken where token = '{$token}'";
		$this->db->query($query);
		
	}
}
?>