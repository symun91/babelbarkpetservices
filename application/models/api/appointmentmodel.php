<?php
class Appointmentmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function getAppointmentsCount($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('appointment');
        return $query->num_rows();
    }

    function getAllappointments($num, $offset,$vendorid)
    {
        $this->db->select('appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,pet.primarybreed as petbreed');
        $this->db->join('customer' , 'appointment.customerid = customer.customerid');
        $this->db->join('service' , 'appointment.serviceid = service.serviceid');
        $this->db->join('pet' , 'appointment.petid = pet.petid');
        $this->db->where('appointment.vendorid',$vendorid);
        $this->db->limit($num, $offset);
        $query = $this->db->get('appointment');
        return $query->result_array();
    }
    function getAllappointments1($vendorid)
    {
        $this->db->select('appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,pet.primarybreed as petbreed');
        $this->db->join('customer' , 'appointment.customerid = customer.customerid');
        $this->db->join('service' , 'appointment.serviceid = service.serviceid');
        $this->db->join('pet' , 'appointment.petid = pet.petid');
        $this->db->where('appointment.vendorid',$vendorid);

        $query = $this->db->get('appointment');
        return $query->result_array();
    }

    function getcurrentAppointments($vendorid)
    {
        $query ="select appointment.*,service.name as servicename  from appointment,service where appointment.serviceid = service.serviceid  and (MONTH(CURDATE())=MONTH(appointment.boardingfromdate) OR MONTH(CURDATE())=MONTH(appointment.boardingtodate)) and appointment.boardingtodate >= CURDATE() and appointment.vendorid={$vendorid}";
        $query = $this->db->query($query);
        return $query->result_array();
    }


    function getAppointmentDetails($appointmentid)
    {
        $this->db->select("*");

        $this->db->where('appointmentid',$appointmentid);
        $query = $this->db->get('appointment');
        return $query->row_array();

    }

    function updateAppointment($datacc,$appointmentid)
    {
        if ( $this->appointmentIdExists($appointmentid) == TRUE ) { //update
            $this->db->where('appointmentid',$appointmentid);
            $this->db->update('appointment', $datacc);
            return $appointmentid;
        }
        else {
            $this->db->insert('appointment', $datacc);
            return $this->db->insert_id();
        }
    }
    function  appointmentIdExists($appointmentid){

        $this->db->select('appointmentid');
        $this->db->where('appointmentid',$appointmentid);
        $result =  $this->db->get('appointment');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }
    function getAllAppointments_User($petid)
    {
        $this->db->select('appointment.*,vendor.comapnyname as companyname,service.name as servicename');
        $this->db->join('vendor' , 'appointment.vendorid = vendor.vendorid');
        $this->db->join('service' , 'appointment.serviceid = service.serviceid');
        $this->db->where('appointment.petid',$petid);
        $this->db->where('appointment.boardingfromdate >=','CURDATE()',FALSE);
        $this->db->order_by('appointment.boardingfromdate','asc');
        $this->db->order_by('appointment.boardingfromtime','asc');
        $this->db->limit(10, 0);
        $query = $this->db->get('appointment');
        return $query->result_array();
    }

    function addAppointment($data)
    {
         $this->db->insert('appointment',$data);
         return $this->db->insert_id();
    }
    function getUpcomingAppointments($petid)
    {
        $this->db->select('*');
        $this->db->where('appointment.petid',$petid);
        $this->db->where('appointment.boardingfromdate >=','CURDATE()',FALSE);
        $this->db->order_by('appointment.boardingfromdate','asc');
        $this->db->order_by('appointment.boardingfromtime','asc');
        $this->db->limit(1, 0);
        $query = $this->db->get('appointment');
        return $query->result_array();
    }

    public function getNoOfAppointment($vendorid,$serviceid,$boardingfromdate,$boardingfromtime,$boardingtotime){
        $sql = "SELECT COUNT(*) AS total_appointment, boardingfromtime, boardingfromdate
                FROM appointment
                WHERE vendorid = '{$vendorid}' 
                AND serviceid = '{$serviceid}'
                AND boardingfromtime BETWEEN '{$boardingfromtime}' AND '{$boardingtotime}' 
                AND boardingfromdate = '{$boardingfromdate}'   
                AND (status = 3 or status = 2 or status = 4)   
                GROUP BY boardingfromtime";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}


?>