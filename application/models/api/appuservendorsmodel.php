<?php
class Appuservendorsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }
    function updateVendors($datacc,$appuserid)
    {
        $this->db->insert('appuservendors', $datacc);
        return $this->db->affected_rows();
    }

    function  appuserExists($appuserid){

        $this->db->select('appuserid');
        $this->db->where('appuserid',$appuserid);
        $result =  $this->db->get('appuservendors');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function getAuthorizedVendorsForUser($appuserid)
    {
        $this->db->select('vendorid');
        $this->db->where('appuserid',$appuserid);
        $this->db->where('isauthorized', 1);
        $query = $this->db->get('appuservendors');

        return $query->result_array();
    }

    function setAuthorizedVendorForUser($appuserid, $vendorid, $isauthorized)
    {
        $array = array('appuserid' => $appuserid, 'vendorid' => $vendorid);

        $this->db->where($array);
        $data = array();
        $data['isauthorized'] = $isauthorized;
        $this->db->update('appuservendors', $data);
    }

    function getVendorsForUser($appuserid)
    {   $query = "SELECT vendorid FROM appuservendors where appuserid = ".$appuserid;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getVendorsForUserwithNames($appuserid)
    {

        $this->db->select('appuservendors.vendorid as id,vendor.comapnyname as companyname,vendor.firstname,vendor.lastname');
        $this->db->join('vendor' , 'appuservendors.vendorid = vendor.vendorid');
        $this->db->where('appuservendors.appuserid',$appuserid);
        $query = $this->db->get('appuservendors');
        return $query->result_array();

    }
    function getUsersForVendor($vendorid)
    {
        $query = "SELECT appuserid FROM appuservendors where FIND_IN_SET('$vendorid',vendorid) > 0";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    function deleteVendor($userid,$vendorid)
    {
        $this->db->where('appuserid', $userid);
        $this->db->where('vendorid', $vendorid);
        $this->db->delete('appuservendors');
    }

    function checkVendorExist($userid,$vendorid){
        $this->db->select('*');
        $this->db->where('appuserid', $userid);
        $this->db->where('vendorid', $vendorid);
        $result = $this->db->get('appuservendors');
        return $result->num_rows();
    }

}
