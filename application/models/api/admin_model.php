<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function find_data($return_type='array',$conditions='',$table='',$limit=0,$offset=0,$orderby='userid',$order='DESC')
	{
			$result = '';
			$this->db->select('*');
			if($conditions != '')
			{
				$this->db->where($conditions);
			}
			if($limit != 0)
			{
				$this->db->limit($limit,$offset);
			}
			$this->db->from($table);
		    $this->db->order_by($orderby,$order);
			$query = $this->db->get();
			switch ($return_type) 
			{
				case 'array':
				case '':
					if($query->num_rows() > 0){$result = $query->result_array();}
					break;
					
				case 'row':
					if($query->num_rows() > 0){$result = $query->row_array();}
					break;
					
				case 'list':
					
					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $row)
						{
							$list_arr[$row->userid] = $row->firstname.' '.$row->lastname;
						}
						$result = $list_arr;
					}
					break;
					
				case 'count':
					$result = $query->num_rows();
					break;
			}
			//echo $this->db->last_query();die;
			return $result;
	 }
    
	
	 function save_data($postData=array(),$id=0,$feild='',$table='')
	 {
	 	if($id==0)
	 	{
	 		$query=$this->db->insert($table,$postData);
	 		if($this->db->affected_rows() > 0)
	 		{
	 			return $this->db->insert_id();
	 		}else
	 		{
	 			return false;
	 		}
	 	}else
	 	{
	 		$this->db->where($feild,$id);
	 		$query=$this->db->update($table,$postData);
	 		if($this->db->affected_rows() > 0)
	 		{
	 			return $this->db->affected_rows();
	 		}else
	 		{
	 			return false;
	 		}
	 	}
	 }
	 
	 function delete_data($id=0,$idfeild='',$table='')
	 {
	 	$this->db->where($idfeild,$id);
	 	$query=$this->db->delete($table);
	 	return $this->db->affected_rows();
	 }
}
?>