<?php
class Customermodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
function addCustomer($datacc)
	{
		     $this->db->insert('customer', $datacc);
		     return $this->db->insert_id();
	}
	
function updateCustomer($datacc,$customerid)
	{
		  if ( $this->customerIdExists($customerid) == TRUE ) { //update
		     $this->db->where('customerid',$customerid);
		     $this->db->update('customer', $datacc);
		     return $customerid;
		  }
		  else {
		  	  $this->db->insert('customer', $datacc);
		     return $this->db->insert_id();
		  }
	}
function  customerIdExists($customerid){
		
		$this->db->select('customerid');
		  $this->db->where('customerid',$customerid);
		$result =  $this->db->get('customer');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}

	function  getCustomerid_Email($email,$vendorid)
	{
		$this->db->select('customerid');
		$this->db->where('email',$email);
		$this->db->where('vendorid',$vendorid);
		$result =  $this->db->get('customer');
		return ($result->num_rows() == 1) ? $result->row()->customerid : 0;
	}
	function  getCustomerid($petid){
		
		$this->db->select('*');
		  $this->db->where('petid',$petid);
		$result =  $this->db->get('customerpets');
		return  $result->row_array();
	}
function addpet($datacc)
	{
		$this->db->insert('customerpets', $datacc);
		     return $this->db->affected_rows();
	} 
	
	function updatepet($datacc,$customerid,$petid)
	{
	  if ( $this->customerPetExists($customerid,$petid) == TRUE ) { //do nothing
		    
		     return 1;
		  }
		  else {
		  	  $this->db->insert('customerpets', $datacc);
		     return $this->db->insert_id();
		  }
	} 
	
function  customerPetExists($customerid,$petid){
		
		$this->db->select('customerid');
		  $this->db->where('customerid',$customerid);
		   $this->db->where('petid',$petid);
		$result =  $this->db->get('customerpets');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function getCustomersCount($vendorid)
	{
		  $this->db->where('vendorid',$vendorid);
		$query = $this->db->get('customer');
		return $query->num_rows();
	}
	
	function getAllCustomers($vendorid)
	{
		$this->db->select('*');
		$this->db->where('vendorid',$vendorid);
		$query = $this->db->get('customer');
		return $query->result_array();
	}
	
	function getPetID($customerid)
	{
		
	  $this->db->select('petid');
		$this->db->where('customerid',$customerid);
		$query = $this->db->get('customerpets');
		if($query->num_rows() == 1) {
		       return $query->row()->petid;
		}
		else {
			   return 0;
		}
	}

	function getCustomerPets($customerid)
	{
		 $this->db->select('pet.*');
	   	 $this->db->join('pet' , 'customerpets.petid = pet.petid');
	     $this->db->where('customerpets.customerid',$customerid);
		 $query = $this->db->get('customerpets');
		 return $query->result_array();
	}

    function getCustomerSikkaPets($customerid,$practice_id)
    {
        $this->db->select('pet.*');
        $this->db->join('pet' , 'customerpets.petid = pet.petid');
        $this->db->where('customerpets.customerid',$customerid);
        $this->db->where('pet.practice_id',$practice_id);
        $query = $this->db->get('customerpets');
        return $query->result_array();
    }

function getCustomerDetails($customerid)
	{
	 $this->db->select('*');	
		$this->db->where('customerid',$customerid);
		$query = $this->db->get('customer');
		return $query->row_array();
	
	}
}
?>