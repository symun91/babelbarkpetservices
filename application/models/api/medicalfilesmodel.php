<?php

class Medicalfilesmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }
    function generateMedicalRow($data)
    {
        $this->db->insert('medicalfiles',$data);
        return $this->db->affected_rows();
    }
    function getMedicalReports($userid,$petId)
    {
        $sqlForPetInfo = "select * from pet where petid = '{$petId}'";
        $queryForPetInfo = $this->db->query($sqlForPetInfo);
        $petName = $queryForPetInfo->row()->name;
        $baseurl =BABELVET_ADD.'/view_medical_file/';        
        $sql = "select id as medicalfileid,filename,concat(concat('{$baseurl}',id),'/BabelBark') as fileurl,1 as filetype from medicalfiles 
                where appuserid = '{$userid}' and petid = '{$petId}' and LENGTH(filename) > 0
                union all
                select reportid as medicalfileid,concat('$petName - Medical Report ',DATE_FORMAT(generatedate, '%M %d, %Y')) as filename,concat(concat('{$baseurl}',reportid),'/Sikka') as fileurl,2 as filetype from reports 
                where appuserid = '{$userid}' and petid = '{$petId}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function  getMedicalFileUrl($medicalfileid){
        $url = awsBucketPath;
        $sql = "select concat('{$url}',fileurl) as fileurl from medicalfiles where id = '{$medicalfileid}'";
        $query = $this->db->query($sql);
        return $query->row()->fileurl;
    }

    function  getMedicalInfo($medicalfileid){
        $sql = "select filename,fileurl from medicalfiles where id = '{$medicalfileid}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getSikkaMedicalInfo($medicalfileid){
        $sql = "select filename,fileurl from reports where reportid = '{$medicalfileid}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
?>