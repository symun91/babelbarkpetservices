<?php
class Promotionsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function updatePromotions($datacc,$promotionid)
    {
        if ( $this->promotionIdExists($promotionid) == TRUE ) { //update
            $this->db->where('promotionid',$promotionid);
            $this->db->update('promotions', $datacc);
            return $promotionid;
        }
        else {
            $this->db->insert('promotions', $datacc);
            return $this->db->insert_id();
        }
    }
    function  promotionIdExists($promotionid){

        $this->db->select('promotionid');
        $this->db->where('promotionid',$promotionid);
        $result =  $this->db->get('promotions');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function addPromotions($datacc)
    {
        $this->db->insert('promotions', $datacc);
        return $this->db->affected_rows();
    }

    function getPromotionsCount($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('promotions');
        return $query->num_rows();
    }

    function getAllPromotions($num, $offset,$vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $this->db->limit($num, $offset);
        $query = $this->db->get('promotions');
        return $query->result_array();
    }

    function getAllPromotions1($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $this->db->where('isactive',1);
        $query = $this->db->get('promotions');
        return $query->result_array();
    }

    function getAllPromotionsForApp($vendorid,$isadopted)
    {
        $filePath = base_url().'view_image/';
        $sql="select promotionid, vendorid, name,  description, beginingdate, expirationdate, agerangefrom, agerangeto, targetbreeds, targetweightsfrom, targetweightsto, targetactivityscoresmin, 
                targetactivityscoresmax, advertlocations,capacity, redeemedcount,isactive, concat('{$filePath}',REPLACE(bannerimage,'/','-')) as bannerimage,promotionalurl, promotiongraphic, promocode, targetsubscribedcustomers, updatedon, category, prmourltype,
                webpageheader, concat('{$filePath}',REPLACE(webpageimage,'/','-')) as webpageimage, webpagedesc,isadopted  
                from promotions where vendorid= '{$vendorid}' and isactive=1 and isadopted= '{$isadopted}' and expirationdate >= CURDATE()";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function getAllPromotionsForOtherVendors($vendorids,$isadopted)
    {
        $filePath = base_url().'view_image/';
        $query="select promotionid, vendorid, name,  description, beginingdate, expirationdate, agerangefrom, agerangeto, targetbreeds, targetweightsfrom, targetweightsto, targetactivityscoresmin, 
                targetactivityscoresmax, advertlocations,capacity, redeemedcount,isactive, concat('{$filePath}',REPLACE(bannerimage,'/','-')) as bannerimage,promotionalurl, promotiongraphic, promocode, targetsubscribedcustomers, updatedon, category, prmourltype,
                webpageheader, concat('{$filePath}',REPLACE(webpageimage,'/','-')) as webpageimage, webpagedesc,isadopted 
                from promotions where vendorid NOT IN  ('$vendorids') and isactive=1 and isadopted= '{$isadopted}' and expirationdate >= CURDATE()";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getPromotionDetails($promotionid)
    {
        $this->db->select('promotions.*,petbreed.title');
        $this->db->join('petbreed' , 'promotions.targetbreeds = petbreed.breedid','left');
        $this->db->where('promotions.promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row_array();

    }

    function getAllPromoCodes($vendorid)
    {
        $this->db->where('vendorid',$vendorid);

        $query = $this->db->get('promotions');
        return $query->result_array();

    }

    function isPromoRedeemed($apuserid,$promotionid)
    {
        $this->db->select('id');
        $this->db->where('promotionid',$promotionid);
        $this->db->where('appuserid',$apuserid);
        $result =  $this->db->get('promotionredeemed');
        return ($result->num_rows() == 1) ? TRUE : FALSE;

    }

    function getPromoCapacity($promotionid)
    {
        $this->db->select('capacity');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row()->capacity;
    }

    function getRedeemedCount($promotionid)
    {
        $this->db->select('redeemedcount');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row()->redeemedcount;
    }
    function updatePromotionRedeemed($datacc)
    {
        $this->db->insert('promotionredeemed', $datacc);
        return $this->db->insert_id();
    }

    function incrmentRedeemcnt($promotionid)
    {

        $query = "update promotions set redeemedcount=redeemedcount+1 where promotionid = '{$promotionid}' ";
        $query = $this->db->query($query);
    }

    function deletePromotion($promotionid)
    {
        $query = "delete from promotions where promotionid = '{$promotionid}' ";
        $this->db->query($query);
    }

    function deletePromotionRedeemed($promotionid)
    {
        $query = "delete from promotionredeemed where promotionid = '{$promotionid}' ";
        $this->db->query($query);
    }

    function getRedeemDetails($promotionid)
    {
        $this->db->select('*');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotionredeemed');
        return $query->result_array();
    }
}