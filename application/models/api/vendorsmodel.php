<?php
class Vendorsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function getVendorsByCategory($categoryname){
        $this->db->select('*');
        $this->db->where('category',$categoryname);
        $query = $this->db->get('vendor');
        return $query->result_array();
    }

    function getVendorById($id)
    {
        $query = "SELECT * FROM vendor where FIND_IN_SET('$id',category) > 0";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getVendorByNameAndId($id,$name)
    {
        $query = "SELECT * FROM vendor where FIND_IN_SET('$id',category) > 0 AND comapnyname LIKE '%$name%'";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    function getVendorCategories($vendorid)
    {
        $this->db->select('category');
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('vendor');
        return $query->row_array();

    }
    function getVendorDetails($vendorid)
    {
        $this->db->select('*');
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('vendor');
        return $query->row_array();
    }

    function getVendorDetailsVet($vendorid)
    {
        $filePath = base_url().'view_image/';
        $sql = "select email,comapnyname as companyname, addresslatlng, address,city,country,zipcode,contact,vendorid as id,CONCAT('{$filePath}',REPLACE(logoimage,'/','-')) as logoimage 
                from vendor where vendorid = '{$vendorid}'";
        $query = $this->db->query($sql);
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }


    function registerVendor($datacc)
    {
        $this->db->insert('vendor', $datacc);
        return $this->db->affected_rows();

    }

    function updateVendor($datacc,$vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $this->db->update('vendor', $datacc);
        return $this->db->affected_rows();
    }

    function isEmailExists($email)
    {
        $this->db->select('vendorid');
        $this->db->where('email',$email);
        $query = $this->db->get('vendor');
        return ($query->num_rows() == 1) ? true : false;
    }

    function isEmailExistsForAUser($email,$vendorid)
    {
        $this->db->select('vendorid');
        $this->db->where('email',$email);
        $this->db->where('vendorid !=',$vendorid);
        $query = $this->db->get('vendor');
        return ($query->num_rows() == 1) ? 1 : 0;
    }
}

?>