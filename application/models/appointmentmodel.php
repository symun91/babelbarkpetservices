<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');
/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Config $config
 * @property CI_Loader $load
 * @property CI_Session $session
 */
class Appointmentmodel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function getAppointmentsCount($vendorid) {
        $sql = "(select a.*,c.firstname,c.lastname,s.name as servicename,p.name as petname,
		p.primarybreed as petbreed from appointment as a,customer as c,service as s,pet as p 
		where a.vendorid='{$vendorid}' 
		and a.customerid = c.customerid 
		and a.serviceid = s.serviceid 
		and a.petid = p.petid)
		UNION 
		(select a.*,ap.firstname,ap.lastname,s.name as servicename,p.name as petname,
		p.primarybreed as petbreed from appointment as a,appusers as ap,service as s,pet as p
		where a.vendorid='{$vendorid}' 
		and a.appuserid = ap.appuserid 
		and a.serviceid = s.serviceid 
		and a.petid = p.petid)";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function deleteAppointment($appointmentid) {
        $query = "delete from appointment where appointmentid = '{$appointmentid}' ";
        $this->db->query($query);
    }

    function appointmentStatus($vendorid, $appointmentid) {
        $this->db->where('appointmentid', $appointmentid);
        $this->db->where('vendorid', $vendorid);
        $query = $this->db->get('appointment');
        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getAssignEmployeeStatus($id) {
        $this->db->where('serviceid', $id);
        $query = $this->db->get('service');
        $result = $query->row()->assign_emp_status;
        if ($result == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getAllappointments($num, $offset, $vendorid) {
        $this->db->select('appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,pet.primarybreed as petbreed');
        $this->db->join('customer', 'appointment.customerid = customer.customerid');
        $this->db->join('service', 'appointment.serviceid = service.serviceid');
        $this->db->join('pet', 'appointment.petid = pet.petid');

        $this->db->where('appointment.vendorid', $vendorid);
        $this->db->limit($num, $offset);

        $query = $this->db->get('appointment');
        return $query->result_array();
    }

    /* function getAllappointments1($vendorid)
      {
      $this->db->select('appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,pet.primarybreed as petbreed');
      $this->db->join('customer' , 'appointment.customerid = customer.customerid');
      $this->db->join('service' , 'appointment.serviceid = service.serviceid');
      $this->db->join('pet' , 'appointment.petid = pet.petid');

      $this->db->where('appointment.vendorid',$vendorid);
      //$this->db->limit($num, $offset);

      $query = $this->db->get('appointment');
      return $query->result_array();
      } */

    function getAllappointments2($num, $offset, $vendorid) {


        $sql = ' (select appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,customer,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.customerid = customer.customerid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid)
		UNION 
		(select appointment.*,appusers.firstname,appusers.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,appusers,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.appuserid = appusers.appuserid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid) limit ' . $offset . ',' . $num;

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function getAllappointmentsofvendor($vendorid) {
        $sql = "(select appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,customer,service,pet where appointment.vendorid='{$vendorid}' and 
		appointment.customerid = customer.customerid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid and
		appointment.status !=4 and
		appointment.status !=1)
		UNION 
		(select appointment.*,appusers.firstname,appusers.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,appusers,service,pet where appointment.vendorid='{$vendorid}' and 
		appointment.appuserid = appusers.appuserid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid and
		appointment.status !=4 and
		appointment.status !=1)";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function getAllUnscheduledAppointments($vendorid) {
        $sql = "select a.appointmentid,a.boardingfromdate,a.boardingfromtime,a.boardingtodate,a.boardingtotime,a.estimatedduration,a.durationunit,s.name as serviceName,au.appuserid,
                    au.firstname as appuserFirstName,au.lastname as appuserLastName,au.email as appUserEmail,au.phonenumber as appUserPhone,p.name as petname                   
                    from appointment as a
                    left join appusers as au on au.appuserid = a.appuserid                 
                    left join service as s on s.serviceid = a.serviceid
                    left join pet as p on p.petid = a.petid
                    where a.vendorid='{$vendorid}' 
                    and (a.status = 4 or a.status = 1)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getAllappointmentsofvendor1($num, $offset, $vendorid) {
        $sql = ' (select appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,customer,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.customerid = customer.customerid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid)
		UNION 
		(select appointment.*,appusers.firstname,appusers.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,appusers,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.appuserid = appusers.appuserid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid)  limit ' . $offset . ',' . $num;

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function getAllappointments1($vendorid, $employee_id, $service_id, $customer_id) {
        /* $this->db->select('appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,pet.primarybreed as petbreed');
          $this->db->join('customer' , 'appointment.customerid = customer.customerid');
          $this->db->join('service' , 'appointment.serviceid = service.serviceid');
          $this->db->join('pet' , 'appointment.petid = pet.petid');
          $this->db->where('appointment.vendorid',$vendorid); */
        $employee_filter = $service_filter = $customer_filter = '';
        error_reporting(1);
        if (!empty($employee_id)) {
            $employee_filter = ' and appointment.userid in(' . implode(',', $employee_id) . ')';
        }
        if (!empty($service_id)) {
            $service_filter = ' and appointment.serviceid = service.serviceid and appointment.serviceid in(' . implode(',', $service_id) . ')';
        } else {
            $service_filter = ' and appointment.serviceid = service.serviceid';
        }
        if(!empty($customer_id)) {
            $customer_filter = ' and (appointment.customerid in(' . implode(',', $customer_id) . ') or appointment.appuserid in(\'' . implode(',', $customer_id) . '\'))';
        }

        $sql = ' (select appointment.*,customer.firstname,customer.lastname,service.calendar_color,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,customer,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.customerid = customer.customerid ' . $service_filter . $employee_filter . $customer_filter . ' and
		appointment.petid = pet.petid)
		UNION 
		(select appointment.*,appusers.firstname,appusers.lastname,service.calendar_color,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed from appointment,appusers,service,pet where appointment.vendorid=' . $vendorid . ' and 
		appointment.appuserid = appusers.appuserid ' . $service_filter . $employee_filter . $customer_filter . ' and
		appointment.petid = pet.petid) ';

        $query = $this->db->query($sql);
//        echo '<pre>'.$this->db->last_query();
//        exit;
        return $query->result_array();
    }

    function getcurrentAppointments($vendorid) {
        $query = "select appointment.*,service.name as servicename  from appointment,service where appointment.serviceid = service.serviceid  and (MONTH(CURDATE())=MONTH(appointment.boardingfromdate) OR MONTH(CURDATE())=MONTH(appointment.boardingtodate)) and appointment.boardingfromdate >= CURDATE() and appointment.vendorid={$vendorid} order by boardingfromdate,boardingfromtime";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getUpcomingAppointments($vendorid) {
        $query = "select appointment.*,service.name as servicename,customer.firstname,customer.lastname from appointment,service,customer where appointment.serviceid = service.serviceid and appointment.customerid = customer.customerid and appointment.boardingfromdate >= CURDATE() and appointment.vendorid={$vendorid}
			UNION
			select appointment.*,service.name as servicename,appusers.firstname,appusers.lastname  from appointment,service,appusers where appointment.serviceid = service.serviceid and appointment.appuserid = appusers.appuserid and appointment.boardingfromdate >= CURDATE() and appointment.vendorid={$vendorid}
			order by boardingfromdate, boardingfromtime";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getAppointmentDetails($appointmentid) {
        $this->db->select("*");
        $this->db->where('appointmentid', $appointmentid);
        $query = $this->db->get('appointment');
        return $query->row_array();
    }
    function getAppointmentInfo($appointmentid){
        $sql = "select app.*,aps.email as appuseremail,v.email as vendoremail from appointment app 
                left join appusers aps on aps.appuserid = app.appuserid
                left join vendor v on v.vendorid = app.vendorid
                where app.appointmentid = '{$appointmentid}'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function updateAppointment($datacc, $appointmentid) {
        if ($this->appointmentIdExists($appointmentid) == TRUE) { //update
            $this->db->where('appointmentid', $appointmentid);
            $this->db->update('appointment', $datacc);
            return $appointmentid;
        } else {
            $this->db->insert('appointment', $datacc);
            return $this->db->insert_id();
        }
    }

    function appointmentIdExists($appointmentid) {

        $this->db->select('appointmentid');
        $this->db->where('appointmentid', $appointmentid);
        $result = $this->db->get('appointment');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }


    function getNewAppointments($vendorid){
        $query ="select appointment.*,service.name as servicename,appusers.firstname,appusers.lastname,appusers.email,appusers.phonenumber, pet.name as petname  
                  from appointment,service,appusers,pet where appointment.serviceid = service.serviceid 
                  and appointment.appuserid = appusers.appuserid 
                  and appointment.petid =pet.petid  
                  and status=1 
                  and appointment.vendorid={$vendorid}";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function updateReadStatus($vendorid) {

        $query = "update appointment set status=4 where status=1 and vendorid = $vendorid ";
        $query = $this->db->query($query);
    }

    function getAllUpcomingAppointments3() {

        $sql = ' (select appointment.*,customer.firstname,customer.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed, now() as curdate from appointment,customer,service,pet where  appointment.boardingfromdate >= CURDATE() and
		appointment.customerid = customer.customerid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid)
		UNION 
		(select appointment.*,appusers.firstname,appusers.lastname,service.name as servicename,pet.name as petname,
		pet.primarybreed as petbreed , now() as curdate from appointment,appusers,service,pet where appointment.boardingfromdate >= CURDATE() and
		appointment.appuserid = appusers.appuserid and
		appointment.serviceid = service.serviceid and
		appointment.petid = pet.petid)';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_vendor_services_and_employees($vendorid) {
        $services = $this->db->select('serviceid, name')->where('vendorid', $vendorid)->get('service')->result();
        $employee = $this->db->select('userid, firstname, lastname')->where('vendorid', $vendorid)->get('vendorusers')->result();
        return array($services, $employee);
    }
    public function getNoOfAppointment($vendorid,$serviceid,$boardingfromdate,$boardingfromtime,$boardingtotime){
        $sql = "SELECT COUNT(*) AS total_appointment, boardingfromtime, boardingfromdate
                FROM appointment
                WHERE vendorid = '{$vendorid}' 
                AND serviceid = '{$serviceid}'
                AND boardingfromtime BETWEEN '{$boardingfromtime}' AND '{$boardingtotime}' 
                AND boardingfromdate = '{$boardingfromdate}'
                AND (status = 3 or status = 2)                
                GROUP BY boardingfromtime";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}