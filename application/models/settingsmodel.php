<?php
class Settingsmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
	function getDetails($vendorid,$measurename)
	{
		 $this->db->select('*');
		$this->db->where('vendorid',$vendorid);
		 $this->db->where('measurename',$measurename);
		$query = $this->db->get('vendorsettings');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return "";
		}
	}
	
function getDetails1($vendorid)
	{
		 $this->db->select('*');
		$this->db->where('vendorid',$vendorid);
		$query = $this->db->get('vendorsettings');
		if($query->num_rows() == 1) {
		       return $query->row_array();
		}
		else {
			   return "";
		}
	}
	
	function inserDefaultEntry($datacc)
	{
		  $this->db->insert('vendorsettings', $datacc);
		   return $this->db->insert_id();
	}
	
function updateSettings($datacc,$vendorid)
	{
		  if ( $this->settingExists($vendorid) == TRUE ) { //update
		     $this->db->where('vendorid',$vendorid);
		   	 $this->db->update('vendorsettings', $datacc);
		     return $vendorid;
		  }
		  else {
		  	  $this->db->insert('vendorsettings', $datacc);
		     return $this->db->insert_id();
		  }
	}
	
function settingExists($vendorid){
		
		$this->db->where('vendorid',$vendorid);
		$result =  $this->db->get('vendorsettings');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
}
?>