<?php
class Petprescriptionmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}

	function addPrescription($datacc)
	{
		     $this->db->insert('petprescriptionssikka', $datacc);
		     return $this->db->insert_id();
	}
	function addPrescriptions($data) {
    $this->db->insert_batch('petprescriptionssikka', $data);
	}
	function getSikkaMedicationInfo($patient_id){
	    $sql = "select * from petprescriptionssikka where patient_id = '{$patient_id}'";
	    $query = $this->db->query($sql);
        return $query->result_array();
    }
    function getLatestSikkaMedInfo($patient_id){
        $sql = "select * from petprescriptionssikka where patient_id = '{$patient_id}' and DATE(updatedon) = CURDATE()";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

}
?>