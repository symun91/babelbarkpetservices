<?php
class Petmedicationmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function addMedication($datacc)
    {
        $this->db->insert('petmedication', $datacc);
        return $this->db->affected_rows();
    }

    function deleteByPetID($petid)
    {
        $query = "delete from petmedication where petid = '{$petid}'";
        $this->db->query($query);
    }

    function getPetMedicationForPet($petid)
    {
        $this->db->select('petmedication.petmedicationid,petmedication.medicineid as id,petmedication.frequency,medicines.name, petmedication.dosage
        ,petmedication.frequencytimes,petmedication.dosageunit');
        $this->db->join('medicines' , 'petmedication.medicineid = medicines.medicineid');
        $this->db->where('petmedication.status',1);
        $this->db->where('petmedication.petid',$petid);
        $query = $this->db->get('petmedication');
        return $query->result_array();

    }

    function getPetMedicationForPet1($petid)
    {
        $this->db->select('petmedication.petmedicationid,petmedication.medicineid ,petmedication.frequency,medicines.name,medicines.url, 
        petmedication.dosage,petmedication.medicationtimings,petmedication.frequencytimes');
        $this->db->join('medicines' , 'petmedication.medicineid = medicines.medicineid');
        $this->db->where('petmedication.petid',$petid);
        $query = $this->db->get('petmedication');
        return $query->result_array();

    }

    function getAllMedications(){
        $this->db->select('*');
        $this->db->order_by("name","asc");
        $query = $this->db->get('medicines');
        return $query->result_array();
    }



    function fetchMedicineDetails($medicineid)
    {
        $this->db->select('*');
        $this->db->where('medicineid',$medicineid);
        $query = $this->db->get('medicines');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else {
            return 0;
        }
    }

    function updateMedicationStatistics($datacc)
    {
        $this->db->insert('medicationstatistics', $datacc);
        return $this->db->affected_rows();
    }

    function getMedicationStatistics($petid)
    {
        $this->db->select('*');
        $this->db->where('petid',$petid);
        $query = $this->db->get('medicationstatistics');
        return $query->result_array();
    }

    function medicinetradenames($medicineid){

        $this->db->select(' GROUP_CONCAT(tradename) AS tradename',false);
        $this->db->where('medicineid',$medicineid);
        $query = $this->db->get('medicinetradenames');
        return $query->row_array();
    }

    function getmedicineAlltradenames($medicineid){

        $this->db->select('*');
        $this->db->where('medicineid',$medicineid);
        $query = $this->db->get('medicinetradenames');
        return $query->result_array();
    }
    function getLatestMedInfo($petId){
        $sql = "select * from petmedication 
                where petid = '{$petId}'
                and status = 1 
                and DATE(updatedon) = CURDATE()";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
}
?>