<?php
class Appuservendorsmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
function updateVendors($datacc,$appuserid)
	{
		  
		if ( $this->appuserExists($appuserid) == TRUE ) { //update
			 $this->db->where('appuserid',$appuserid);
		    $this->db->update('appuservendors', $datacc);
		}
		else // insert
		{
		    $this->db->insert('appuservendors', $datacc);
		}
	
	return $this->db->affected_rows();
	}
function authorizedVendor($vendorid,$appuserid,$data)
{

	if ( $this->appuserExists($appuserid) == TRUE ) { //update
			 $this->db->where('appuserid',$appuserid);
			  $this->db->where('vendorid',$vendorid);
		    $this->db->update('appuservendors', $data);
		    return $this->db->affected_rows();
		}
		else{
			return 0;
		}
	
	
}
function  appuserExists($appuserid){
		
		$this->db->select('appuserid');
		 $this->db->where('appuserid',$appuserid);
		$result =  $this->db->get('appusers');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}

function getVendorsForUser($appuserid)
{
 $this->db->select('vendorids');
		$this->db->where('appuserid',$appuserid);
		$query = $this->db->get('appuservendors');
		if($query->num_rows() == 1) {
		       return $query->row()->vendorids;
		}
else {
			   return "";
		}
}
 	function getUsersForVendor($vendorid)
	{
		$query = "SELECT appuserid , updatedon FROM appuservendors where FIND_IN_SET('$vendorid',vendorid) > 0";
		$query = $this->db->query($query);
		return $query->result_array();	
	}
	
	function insertInAppUserVendors($datacc)
	{
		 $this->db->insert('appuservendors', $datacc);
		 return $this->db->affected_rows();
	}
	
	function isAppuseridAndVendoridExists($appuserid,$fromvendorid)
	{
	   	    $this->db->select('appuserid');
			$this->db->where('appuserid',$appuserid);
			$this->db->where('vendorid',$fromvendorid);
			$query = $this->db->get('appuservendors');
			return ($query->num_rows() == 1) ? 1 : 0;
	}
	function getUsersForVendorCount($vendorid)
	{
		$query = "SELECT appuserid FROM appuservendors where FIND_IN_SET('$vendorid',vendorid) > 0 and isauthorized=1";
		$query = $this->db->query($query);
		return $query->num_rows();
	}
}
