<?php
class Customermodel extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    function addCustomer($datacc)
    {
        $this->db->insert('customer', $datacc);
        return $this->db->insert_id();
    }
    function updateCustomer($datacc, $customerid, $onlyUpdate = FALSE) {
        if ($this->customerIdExists($customerid) == TRUE) { //update
            $this->db->where('customerid', $customerid);
            $this->db->update('customer', $datacc);
            return $customerid;
        } else {
            if (!$onlyUpdate) {
                $this->db->insert('customer', $datacc);
                return $this->db->insert_id();
            } else {
                return 0;
            }
        }
    }
    function customerStatus($userid,$vendorid,$user)
    {
        if ($user == "Generic Customer")
        {
            $this->db->where('customerid',$userid);
            $this->db->where('vendorid',$vendorid);
            $query = $this->db->get('customer');
            if($query->num_rows() == 1) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        else
        {
            $this->db->where('appuserid',$userid);
            $this->db->where('vendorid',$vendorid);
            $query = $this->db->get('appuservendors');
            if($query->num_rows() == 1) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
    }
    function updateSikkaCustomer($datacc)
    {
        $vendorid = $datacc['vendorid'];
        $guarantorid = $datacc['guarantor_id'];
        $patient_id = $datacc['patient_id'];
        $customerid = $this->sikkacustomerexist($vendorid,$guarantorid,$patient_id);
        if($customerid['customerid']>0)
        {
            $this->db->where('customerid',$customerid['customerid']);
            $this->db->update('customer', $datacc);
            return $customerid;
        }else if($customerid['customerid'] == -1)
        {
            ////skip record
        }
        else {
            $this->db->insert('customer', $datacc);
            return $this->db->insert_id();
        }
    }
    function sikkacustomerexist($vendorid,$guarantorid,$patient_id)
    {
        $this->db->select('customerid');
        $this->db->where('vendorid',$vendorid);
        $this->db->where('guarantor_id',$guarantorid);
        $result =  $this->db->get('customer');
        if($result->num_rows() == 1)
        {
            return $result->row_array();
        }
        else if($result->num_rows > 1)
        {
            return array('customerid'=> -1);
        }
        else{
            return array('customerid' => 0);
        }
    }
    function  customerIdExists($customerid){
        $this->db->select('customerid');
        $this->db->where('customerid',$customerid);
        $result =  $this->db->get('customer');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }
    function addpet($datacc)
    {
        $this->db->insert('customerpets', $datacc);
        return $this->db->affected_rows();
    }
    function updatepet($datacc,$customerid,$petid)
    {
        if ( $this->customerPetExists($customerid,$petid) == TRUE ) { //do nothing
            return 1;
        }
        else {
            $this->db->insert('customerpets', $datacc);
            return $this->db->insert_id();
        }
    }
    function  customerPetExists($customerid,$petid){
        $this->db->select('customerid');
        $this->db->where('customerid',$customerid);
        $this->db->where('petid',$petid);
        $result =  $this->db->get('customerpets');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }
    function getCustomersCount($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('customer');
        return $query->num_rows();
    }
    function getAllCustomers($vendorid)
    {
        $this->db->select('*');
        $this->db->where('vendorid',$vendorid);
        $this->db->order_by("updatedon", "DESC");
        $query = $this->db->get('customer');
        return $query->result_array();
    }
    function getAllCustomersForAdmin()
    {
        $this->db->select('*');
        $query = $this->db->get('customer');
        return $query->result_array();
    }
    function getAllCustomersForAdmin1($num,$offset)
    {
        $this->db->select('*');
        $this->db->limit($num, $offset);
        $query = $this->db->get('customer');
        return $query->result_array();
    }
    function getAllCustomersCount()
    {
        $this->db->select('*');
        $query = $this->db->get('customer');
        return $query->num_rows();
    }
    function getPets($customerid)
    {
        $this->db->select('petid');
        $this->db->where('customerid',$customerid);
        $query = $this->db->get('customerpets');
        if($query->num_rows() > 0) {
            return $query->num_rows();
        }
        else {
            return 0;
        }
    }
    function getPetID($customerid)
    {
        $this->db->select('petid');
        $this->db->where('customerid',$customerid);
        $query = $this->db->get('customerpets');
        if($query->num_rows() == 1) {
            return $query->row()->petid;
        }
        else {
            return 0;
        }
    }
    function getPetIDs($customerid)
    {
        $this->db->select('petid');
        $this->db->where('customerid',$customerid);
        $query = $this->db->get('customerpets');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }
        else {
            return 0;
        }
    }
    function getCustomerDetails($customerid)
    {
        $this->db->select('*');
        $this->db->where('customerid',$customerid);
        $query = $this->db->get('customer');
        return $query->row_array();
    }
    function deleteCustomerPets($customerid)
    {
        $query = "SET foreign_key_checks = 0";
        $this->db->query($query);
        $query = "delete from customerpets where customerid = '{$customerid}' ";
        $this->db->query($query);
    }
    function deleteCustomerPetMedications($petid)
    {
        $query = "delete from petmedication where petid = '{$petid}' ";
        $this->db->query($query);
    }
    function deleteCustomerPetVaccinations($petid)
    {
        $query = "delete from petvaccination where petid = '{$petid}' ";
        $this->db->query($query);
    }
    function deletePet($petid)
    {
        $query = "delete from pet where petid = '{$petid}' ";
        $this->db->query($query);
    }
    function deleteCustomer($customerid)
    {
        $query = "delete from customer where customerid = '{$customerid}' ";
        $this->db->query($query);
    }
    function insertGuarantors($data) {
        $this->db->insert_batch('customer', $data);
    }
    function getPetInfo($customerId){
        $sql = "select p.petid,p.name from customerpets as cu
                    left join pet as p on p.petid = cu.petid
					where cu.customerid = '{$customerId}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function getCustomerInfo($userId){
        $sql = "select * from customer where email = (select email from appusers where appuserid = '{$userId}')";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}