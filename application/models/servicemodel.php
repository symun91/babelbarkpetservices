<?php

class Servicemodel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function updateService($datacc, $serviceid) {
        if ($this->serviceIdExists($serviceid) == TRUE) { //update
            $this->db->where('serviceid', $serviceid);
            $this->db->update('service', $datacc);
            return $serviceid;
        } else {
            $this->db->insert('service', $datacc);
            return $this->db->insert_id();
        }
    }

    function serviceStatus($vendorid, $serviceid) {
        $this->db->where('vendorid', $vendorid);
        $this->db->where('serviceid', $serviceid);
        $result = $this->db->get('service');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function serviceIdExists($serviceid) {

        $this->db->select('serviceid');
        $this->db->where('serviceid', $serviceid);
        $result = $this->db->get('service');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function getServicesCount($vendorid) {
        $this->db->where('vendorid', $vendorid);
        $query = $this->db->get('service');
        return $query->num_rows();
    }

    function getAllServices($num, $offset, $vendorid) {
        $this->db->where('vendorid', $vendorid);
        #$this->db->limit($num, $offset);
        $this->db->order_by('updatedon', 'DESC');
        $query = $this->db->get('service');
        return $query->result_array();
    }

    function getServiceDetails($serviceid) {
        $this->db->select('*');
        $this->db->where('serviceid', $serviceid);
        $query = $this->db->get('service');
        return $query->row_array();
    }

    function getAllServices1($vendorid) {
        $this->db->where('vendorid', $vendorid);
        $query = $this->db->get('service');
        return $query->result_array();
    }

    function getLatestServices($vendorid) {
        $this->db->where('vendorid', $vendorid);
        $this->db->order_by("updatedon", "desc");
        $this->db->limit(3);
        $query = $this->db->get('service');
        return $query->result_array();
    }

    function deleteService($serviceid) {
        $query = "delete from service where serviceid = '{$serviceid}' ";
        $this->db->query($query);
    }

}

?>
