<?php
class Vendorcategoriesmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
	function getVendorCategories()
	{
        $this->db->select('*');
        $this->db->where('categoryid !=','7');
		$query = $this->db->get('vendorcategories');		
		return $query->result_array();		
		
	}
	function getSelectedVendorCategories()
	{
        $this->db->select('*');
        $this->db->where('categoryid !=','7');
		$query = $this->db->get('vendorcategories');
		return $query->result_array();

	}
	
	function getCategoryName($categid)
	{
	    $this->db->select('title');
		$this->db->where('categoryid',$categid);
		$query = $this->db->get('vendorcategories');
		if($query->num_rows() == 1) {
		       return $query->row()->title;
		}
		else {
			   return "";
		}
	}
}
?>