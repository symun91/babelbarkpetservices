<?php
class Sikkaofficemodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function updateSikkaOffices($data)
	{
		 $sql = 'INSERT INTO sikkaoffices (sikkaofficeid, sikkaofficename, address, city, state, zip, secret_key) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE sikkaofficename=VALUES(sikkaofficename), address=VALUES(address), city=VALUES(city), secret_key=VALUES(secret_key)';
         $query = $this->db->query($sql, $data);
	}

	function getsikkaoffices()
	{
		$this->db->select('*');
		$result =  $this->db->get('sikkaoffices');
		return $result->result_array();
	}

	function getSecretKeyOfOffice($office_id)
	{
		$this->db->select('secret_key');
		$this->db->where('sikkaofficeid',$office_id);
		$result =  $this->db->get('sikkaoffices');
		return $result->row_array();
	}

	
}
	
	?>