<?php
class Reportmodel extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }
    function addReport($reportInfo){
        $this->db->insert('reports',$reportInfo);
        return $this->db->affected_rows();
    }
    function deleteReportLog($vendorId,$appuserId,$petId){
        $this->db->where('vendorid',$vendorId);
        $this->db->where('appuserid',$appuserId);
        $this->db->where('petid',$petId);
        $this->db->delete('reports');
        return $this->db->affected_rows();
    }
    function checkReportCreated($vendorId,$appuserId,$petId){
        $sql = "select * from reports where vendorid = '{$vendorId}' and appuserid = '{$appuserId}' and petid = '{$petId}' and status =1";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function getSikkaReport($reportId){
        $sql = "select * from reports where reportid = '{$reportId}'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}