<?php
class Petfeedmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function getAllFeedBrands(){
		 $this->db->select('*');		   
		$query = $this->db->get('feedbrand');		
		return $query->result_array();		
	}
	
	function getFeedTitle($feedid)
	{
	 $this->db->select('title');
		$this->db->where('feedbrandid',$feedid);
		$query = $this->db->get('feedbrand');
		if($query->num_rows() == 1) {
		       return $query->row()-> title;
		}
		else
		return "";
	}
	
	function getFeedDetails($feedid)
	{
	 $this->db->select('*');
		$this->db->where('feedbrandid',$feedid);
		$query = $this->db->get('feedbrand');
		 return $query->row_array();
		
	}
	
}
	function getFeedLink($feedid)
	{
		$this->db->select('link');
		$this->db->where('feedbrandid',$feedid);
		$query = $this->db->get('link');
		if($query->num_rows() == 1)
		{
			return $query->row()-> link;
		}
		else
			return "";
	}
	
	?>