<?php
class Nonregistervendormodule extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	
function insertNonRegisterVendor($datacc){
		  $this->db->insert('nonregisteredvendor', $datacc);
		  return $this->db->affected_rows();		
}

function insertAppuserNonRegVendors($datacc){
		  $this->db->insert('appusernonregvendors', $datacc);
		  return $this->db->affected_rows();		
}


function getNonregiVendorsByCategory($categoryname){
		 $this->db->select('*');		 
		// $this->db->where_in('category',$categoryname);  
		$where = "FIND_IN_SET('".$categoryname."', category)";  
		$this->db->where( $where );
		$query = $this->db->get('nonregisteredvendor');		
		return $query->result_array();		
	}
function getNonregiVendorDetails($vendorid)
{
 $this->db->select('*');		 
		 $this->db->where('nonregisteredvendorid',$vendorid);  
		$query = $this->db->get('nonregisteredvendor');		
		return $query->row_array();		
}

function getNonregiVendorsForUser($appuserid)
{
 $this->db->select('nonregisteredvendorid');
		$this->db->where('appuserid',$appuserid);
		$query = $this->db->get('appusernonregvendors');
		return $query->result_array();		
}

function removeNonregivendorsForUser($appuserid,$nonregisteredvendorid)
{
	 $query = "delete from appusernonregvendors where appuserid = '{$appuserid}' and nonregisteredvendorid ='{$nonregisteredvendorid}'";
		$this->db->query($query);
}

}
	
	?>