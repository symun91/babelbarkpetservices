<?php
class Medicalfilesmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function verifyToken($token)
    {
        $this->db->select('*');
        $this->db->where('token',$token);
        $query = $this->db->get('medicalfiles');
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
        else
            return "";
    }

    function updateMedicalFiles($recordid,$datacc)
    {
            $this->db->where('id',$recordid);
            $this->db->update('medicalfiles', $datacc);

    }
    function getMedicalReports($userid,$petid)
    {
        $this->db->select('filename,fileurl','petid');
        $this->db->where('appuserid',$userid);
        $this->db->where('petid',$petid);
        $where = "LENGTH(filename) > 0";
        $this->db->where($where);
        $query = $this->db->get('medicalfiles');
        return $query->result_array();
    }
    function addMultipleMedicalFiles($datacc){
        $this->db->insert('medicalfiles',$datacc);
    }
    function getReportInfo($recordId){
        $this->db->select('petid,appuserid,vetemail');
        $this->db->where('id',$recordId);
        $query = $this->db->get('medicalfiles');
        return $query->row_array();
    }

}