<?php
class Sikkapetimportrequestsmodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();

	}
	function verifyToken($id,$token)
	{
		 $this->db->select('*');	
		 $this->db->where('id',$id);
		$this->db->where('token',$token);
		$query = $this->db->get('sikkapetimportrequests');
		if($query->num_rows() == 1) {
			return $query->row_array();
		}
		else
		return "";
	}
	
	function updatepetimportrequest($recordid,$datacc)
	{
		 	$this->db->where('id',$recordid);
		     $this->db->update('sikkapetimportrequests', $datacc);
		     return $recordid;
	}
}