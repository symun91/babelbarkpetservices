<?php

class Mailnotifymodel extends CI_Model {
		
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
			$this->load->helper(array('common_helper','url'));
	}

	function sendResetPasswordLink($userId,$token){

		$userDetails = $this->appusersmodel->fetchUserDetails($userId);
		

		$username = $userDetails['firstname'];
		$emailId = $userDetails['email'];
		$url = base_url().'resetpassword?token='.$token;
		
		


		$subject = 'BabelBark Password';
		$msg = '
		<span style="font-size:16px;color:#6B6B6D;font-family:Comic Sans,sans-serif;">
		Hello <b>'.$username.'</b>,<br><br>
		To set or reset your password. <a href="'.$url.'">Tap Here!</a><br><br>
		If you did not request a password reset, please ignore this email.<br>
		If you have any questions or concerns please contact us at <a href="mailto:info@babelbark.com">info@babelbark.com</a>  <br><br>
		
		
		Thank you!<br>
		BabelBark <br>
		617.957.7383					
		</span>				
		';
		
		
		/*$this->email->from('admin@babelbark.com', 'BabelBark');
		$this->email->to($emailId);
		$this->email->set_mailtype("html");
		//$this->email->cc('prasenjit.aluni@teks.co.in');
		$this->email->subject($subject);
		//$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
		
		$this->email->message($msg);
		$this->email->send();*/
		
		send_template_mail('',$msg, $subject, $emailId, "admin@bizbark.com");
	}

   function inviteuser($userId, $email){

		$userDetails = $this->appusersmodel->fetchUserDetails($userId);
		

		$firstname = $userDetails['firstname'];
		
		$lastname = $userDetails['lastname'];
		

		$subject = 'BabelBark Invitaion';
		$msg = '
		<span style="font-size:16px;color:#6B6B6D;font-family:Comic Sans,sans-serif;">
		Hello!<br>
		 <b>'.$firstname.' '.$lastname.'</b> has invited you to join BabelBark!<br><br>
		
		BabelBark is a mobile app and software platform that connects pet parents, pet vendors and veterinarians to help dogs have the happiest and healthiest life possible - visit our site at <a href="https://babelbark.com/">https://babelbark.com/</a> to learn more!
  <br><br>
		An all-in-one platform for all your pet needs, join now! <br><br>
		
		<br>
		<img src="http://bizbark.com/assets/images/logobabelbark.png" width="100"><br>
		617.957.7383<br>
		<a mailto:"info@babelbark.com">info@babelbark.com</a>					
		</span>				
		';
		
		
		$this->email->from('admin@babelbark.com', 'BabelBark');
		$this->email->to($email);
		$this->email->set_mailtype("html");
		//$this->email->cc('prasenjit.aluni@teks.co.in');
		$this->email->subject($subject);
		//$this->email->message('To reset new password of Flashagram account '.$new_pwd.'.');
		$this->email->message($msg);
		$this->email->send();
	}

}