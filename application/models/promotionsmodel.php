<?php
class Promotionsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();

    }

    function updatePromotions($datacc,$promotionid)
    {
        if ( $this->promotionIdExists($promotionid) == TRUE ) { //update
            $this->db->where('promotionid',$promotionid);
            $this->db->update('promotions', $datacc);
            return $promotionid;
        }
        else {
            $this->db->insert('promotions', $datacc);
            return $this->db->insert_id();
        }
    }
    function promotionStatus($vendorid,$promotionid)
    {
        $this->db->where('promotionid',$promotionid);
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('promotions');
        if($query->num_rows() == 1)
        {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function  promotionIdExists($promotionid){

        $this->db->select('promotionid');
        $this->db->where('promotionid',$promotionid);
        $result =  $this->db->get('promotions');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function addPromotions($datacc)
    {
        $this->db->insert('promotions', $datacc);
        return $this->db->affected_rows();
    }

    function getPromotionsCount($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $query = $this->db->get('promotions');
        return $query->num_rows();
    }

    function getAllPromotions($vendorid)
    {
        $this->db->where('vendorid',$vendorid);
        $this->db->order_by('updatedon', 'DESC');
        $query = $this->db->get('promotions');
        return $query->result_array();
    }

    function getAllPromotions1($vendorid)
    {
        $limit = 3;
        $this->db->where('vendorid',$vendorid);
        $this->db->where('isactive',1);
        $this->db->order_by("expirationdate", "desc");
        $this->db->limit($limit);
        $query = $this->db->get('promotions');
        return $query->result_array();
    }

    function getAllPromotionsForApp($vendorid)
    {

        $query="select * from promotions where vendorid={$vendorid} and isactive=1 and expirationdate>=CURDATE()";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getPromotionDetails($promotionid)
    {
        $this->db->select('promotions.*,petbreed.title');
        $this->db->join('petbreed' , 'promotions.targetbreeds = petbreed.breedid','left');
        $this->db->where('promotions.promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row_array();

    }

    function getAllPromoCodes($vendorid)
    {
        $this->db->where('vendorid',$vendorid);

        $query = $this->db->get('promotions');
        return $query->result_array();

    }

    function isPromoRedeemed($apuserid,$promotionid)
    {
        $this->db->select('id');
        $this->db->where('promotionid',$promotionid);
        $this->db->where('appuserid',$apuserid);
        $result =  $this->db->get('promotionredeemed');
        return ($result->num_rows() == 1) ? TRUE : FALSE;

    }

    function getPromoCapacity($promotionid)
    {
        $this->db->select('capacity');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row()->capacity;
    }

    function getRedeemedCount($promotionid)
    {
        $this->db->select('redeemedcount');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotions');
        return $query->row()->redeemedcount;
    }
    function updatePromotionRedeemed($datacc)
    {
        $this->db->insert('promotionredeemed', $datacc);
        return $this->db->insert_id();
    }

    function incrmentRedeemcnt($promotionid)
    {

        $query = "update promotions set redeemedcount=redeemedcount+1 where promotionid = '{$promotionid}' ";
        $query = $this->db->query($query);
    }

    function deletePromotion($promotionid)
    {
        $query = "delete from promotions where promotionid = '{$promotionid}' ";
        $this->db->query($query);
    }

    function deletePromotionRedeemed($promotionid)
    {
        $query = "delete from promotionredeemed where promotionid = '{$promotionid}' ";
        $this->db->query($query);
    }

    function getRedeemDetails($promotionid)
    {
        $this->db->select('*');
        $this->db->where('promotionid',$promotionid);
        $query = $this->db->get('promotionredeemed');
        return $query->result_array();
    }
    function updateAllExpirePromotions(){
        $sql = "update promotions set isactive=0 where isactive=1 and expirationdate<= CURDATE()";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();

    }

    function checkExpirationDate($promotionId){
        $sql = "SELECT * FROM promotions p WHERE p.promotionid= {$promotionId} and p.expirationdate >= CURDATE()";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
}