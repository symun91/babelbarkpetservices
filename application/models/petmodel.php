<?php

class Petmodel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function registerPet($datacc) {
        $this->db->insert('pet', $datacc);
        return $this->db->affected_rows();
    }

    function updatePet($datacc, $petid) {
        $this->db->where('petid', $petid);
        $this->db->update('pet', $datacc);
        return $this->db->affected_rows();
    }

    function getPetDetails($petid) {
        $this->db->select('p.*,v.name AS veterinarianname, CASE WHEN c.customerid IS NULL THEN ap.appuserid ELSE c.customerid END AS customer_id', FALSE);
        $this->db->from('pet AS p');
        $this->db->join('veterinarian AS v', 'v.veterinarianid = p.veterinarianid', 'LEFT');
        $this->db->join('customerpets as c', 'c.petid = p.petid', 'LEFT');
        $this->db->join('appuserpets as ap', 'ap.petid = p.petid', 'LEFT');
        $this->db->where('p.petid', $petid);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return 0;
        }
    }

    function getPetDetails1($petid) {
        $this->db->select('p.*,v.name AS veterinarianname, CASE WHEN c.customerid IS NULL THEN ap.appuserid ELSE c.customerid END AS customer_id', FALSE);
        $this->db->from('pet AS p');
        $this->db->join('veterinarian AS v', 'v.veterinarianid = p.veterinarianid', 'LEFT');
        $this->db->join('customerpets as c', 'c.petid = p.petid', 'LEFT');
        $this->db->join('appuserpets as ap', 'ap.petid = p.petid', 'LEFT');
        $this->db->where('p.petid', $petid);
        $query = $this->db->get();
        return $query->row_array();
    }

    function getPetVaccineDetails($petid) {
        $this->db->select('petvaccination.id as petvaccinationid,petvaccination.vaccineid,petvaccination.info,petvaccination.duedate');
        $this->db->where('petid', $petid);
        $query = $this->db->get('petvaccination');
        return $query->result_array();
    }

    function getPetMedicationDetails($petid) {
        $this->db->select('petmedication.petmedicationid,petmedication.medicineid,petmedication.frequency');
        $this->db->where('petid', $petid);
        $query = $this->db->get('petmedication');
        return $query->result_array();
    }

    function updatePet1($datacc, $petid) {

        if ($this->petIdExists($petid) == TRUE) { //update
            $this->db->where('petid', $petid);
            $this->db->update('pet', $datacc);
            return 1;
        } else { // insert
            $this->db->insert('pet', $datacc);
            return $this->db->affected_rows();
        }
    }

    function petIdExists($petid) {

        $this->db->select('petid');
        $this->db->where('petid', $petid);
        $result = $this->db->get('pet');
        return ($result->num_rows() == 1) ? TRUE : FALSE;
    }

    function getPetswithUnsureBirthday() {
        $this->db->where('birthdayunsure', 1);
        $query = $this->db->get('pet');
        return $query->result_array();
    }

    function increaseApproxAge($petid, $approxage) {
        $query = "update pet set approximateage='$approxage' where petid = $petid ";
        $query = $this->db->query($query);
    }

    function insertPets($data) {
        $this->db->insert_batch('pet', $data);
    }

    function updateSikkaPet($datacc, $petid) {

        $guarantorid = $datacc['guarantor_id'];
        $patient_id = $datacc['patient_id'];
        $petiddata = $this->sikkapetexist($guarantorid, $patient_id);
        if ($petiddata['petid'] > 0) {
            file_put_contents("tmp/" . "tempupdate" . ".txt", json_encode($petiddata));
            $this->db->where('petid', $petiddata['petid']);
            $this->db->update('pet', $datacc);
            return $petid;
        } else if ($petiddata['petid'] == -1) {
            ////skip record
            return;
        } else {

            file_put_contents("tmp/" . "tempupdate_insert" . ".txt", json_encode($patient_id));
            $datacc['petid'] = $petid;
            $this->db->insert('pet', $datacc);
            return $this->db->insert_id();
        }
    }

    function sikkapetexist($guarantorid, $patient_id) {
        $this->db->select('petid');
        $this->db->where('guarantor_id', $guarantorid);
        $this->db->where('patient_id', $patient_id);
        $result = $this->db->get('pet');
        if ($result->num_rows() == 1) {
            return $result->row_array();
        } else if ($result->num_rows() > 1) {
            return array('petid' => -1);
        } else {
            return array('petid' => 0);
        }
    }

    function sikkapetexisttoggle($guarantorid, $patient_id) {
        $this->db->select('petid');
        $this->db->where('guarantor_id', $guarantorid);
        $this->db->where('patient_id', $patient_id);
        $result = $this->db->get('pet');
        if ($result->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getCustomerPet($guarantor_id, $patient_id) {
        $this->db->select('petid');
        $this->db->where('guarantor_id', $guarantor_id);
        $this->db->where('patient_id', $patient_id);
        $result = $this->db->get('pet');
        if ($result->num_rows() == 1) {
            return $result->row_array();
        } else {
            return array('petid' => 0);
        }
    }

    function getCustomerPets($guarantor_id) {
        $this->db->select('petid');
        $this->db->where('guarantor_id', $guarantor_id);
        $result = $this->db->get('pet');
        return $result->result_array();
    }

    function getPetInfo($petid) {
        $sql = "select p.name,p.gender,DATE_FORMAT(p.birthdate, '%M %d, %Y') as birthdate,p.microchipid,p.proilefpicture,p.currentweight,fpb.title as primaryfood,fpb.link as primarylink,fsb.title as secondaryfood,fsb.link as secondarylink, 
                p.amountoffeed,p.frequencyoffeed,ppb.title as primarybreed,psb.title as secondarybreed 
				from pet as p
				left join feedbrand as fpb on p.primaryfood = fpb.feedbrandid
				left join feedbrand as fsb on p.secondaryfood = fsb.feedbrandid
                left join petbreed as ppb on p.primarybreed = ppb.breedid
                left join petbreed as psb on p.secondarybreed = psb.breedid
                where p.petid = '{$petid}'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function getPetWeightGraph($petid) {
        $sql = "select distinct(DATE_FORMAT(wtdatetime,'%m-%Y')) as monthYear,MONTHNAME(wtdatetime) as month,measuredwt 
                from petweightlog 
                where petid = '{$petid}' 
                and wtdatetime !=''
                and (wtdatetime BETWEEN (DATE_SUB(curdate(), INTERVAL 12 MONTH)) AND curdate())
                group by monthYear
                order by DATE_FORMAT(wtdatetime,'%Y') asc,
                DATE_FORMAT(wtdatetime,'%m') asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getAppUserInfo($petid) {
        $sql = "select concat(firstname,' ',lastname) as name,address,phonenumber,email from appusers where appuserid = (select appuserid from appuserpets where petid = '{$petid}')";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function getCustomerInfo($petid) {
        $sql = "select concat(firstname,' ',lastname) as name,address,phoneno as phonenumber,email from customer where customerid = (select customerid from customerpets where petid = '{$petid}')";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function getPetActivitylog($petId) {
        $sql = "select MONTHNAME(syncdate) as month,sum(actualpoints) as petPoint 
                from petactivitylog 
                where petid = '{$petId}'
                and syncdate !=''
                and (syncdate BETWEEN (DATE_SUB(curdate(), INTERVAL 12 MONTH)) AND curdate())
			    group by month
				order by DATE_FORMAT(syncdate,'%m') asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function medicationInfo($petId) {
        $sql = "select pm.*,md.name from petmedication as pm
                left join medicines as md on md.medicineid = pm.medicineid 
                where pm.petid = '{$petId}' 
                and pm.status = 1";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getMedicationLog($petMedication, $petId) {
        $tomorrow = date('Y-m-d H:i:s', strtotime('tomorrow'));
        $sql = "SELECT *
                FROM (`petmedicationlog`)
                WHERE `petmedicationid` = '{$petMedication}'
                AND `petid` =  '{$petId}'
                AND `medicationdatetime` < '{$tomorrow}'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

}
