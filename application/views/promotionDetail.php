<!DOCTYPE html>
<?php date_default_timezone_set('UTC');?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BabelBark | Promotion Detail</title>
    <link rel="icon" type="image/ico" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap_backup.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/AdminLTE.min.css">
    <!-- iCheck -->
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <?php if($msg==""){?>
  <header class="sb-page-header">
	<div class="container">
		<h1><?php echo $detail['webpageheader'];?></h1>
		
	</div>
</header>

<!-- Page Content -->
<div class="container">
	<div class="row">
	<img src="<?php echo base_url().$detail['webpageimage'];?>?<?=Date('U')?>" />
	<br><br>
		<div class="col-lg-7 col-md-6 preview-page-window">
		 <h4><?php echo $detail['webpagedesc'];?></h4>
		</div>
	</div>
</div>
  
      <br><br>
        
          <form name="promoform" id="promoform" action="<?php echo base_url();?>promotiondetail/getpromotion" method="POST"  enctype="multipart/form-data">
        	<input type="hidden" name="promotionid" id="promotionid" value="<?php echo $promotionid;?>"/>
             <input type="hidden" name="appuserid" id="appuserid" value="<?php echo $appuserid;?>"/>
         <div class="container text-center">
     	 		<input type="submit" class="btn btn-warning" name="update" id="update" value="I want this">
     	 		
     	 		</div>
		 </form>
			
			<?php }
			else{
			?>
			  <center>  <h1>   <?php  echo $msg;?></h1></center>
			<?php }?>
      
    <script src="<?php echo base_url(); ?>assest/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assest/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
