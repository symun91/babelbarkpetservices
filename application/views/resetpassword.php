<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BabelBark | Reset Password</title>
    <link rel="icon" type="image/ico" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap_backup.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/AdminLTE.min.css">
    <!-- iCheck -->
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="layout-boxed">
    <div class="login-box" style="width:400px;">
      <div class="login-logo" style="font-size:25px;">
      <img src="<?php echo base_url(); ?>assets/images/logobabelbark.png" width="250"><br>
        
      </div>
      
      <div class="login-box-body">
      	<center><h2>Reset Password</h2></center>
        <p class="login-box-msg"><?php echo $msg;?></p>
        <form action="" method="post">
          <input type="hidden" name="token" value="<?php echo $token; ?>" />
          <div class="form-group has-feedback">
            <input type="password" name="pwd_new" class="form-control" placeholder="Password" value="" required="required">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="cpwd_new" class="form-control" placeholder="Confirm Password" required="required">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div>
          </div>
        </form>

        
        
      </div>
    </div>
    <script src="<?php echo base_url(); ?>assest/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assest/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
