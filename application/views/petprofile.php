<!-- Main content -->
<section class="content" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="panel">
                <div class="panel-body">
                    <div class="box box-information box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">BASIC INFORMATION</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body notify-body-class"  ><!-- style="min-height: 763px"	 -->
                            <?php
                            if($petInfo->gender =='M'){
                                $gender = 'Male';
                            }elseif(($petInfo->gender =='F')){
                                $gender = 'Female';
                            }else{
                                $gender = 'N/A';
                            }
                            if($petInfo->proilefpicture ==''){
                                $petImgUrl = awsBucketPath.'resources/uploads/petimage/no_pet_image.png';
                            }else{
                                $petImgUrl = awsBucketPath.$petInfo->proilefpicture;
                            }
                            ?>
                            <div class="col-lg-4 col-md-4">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="<?php echo $petImgUrl;?>" alt="User profile picture">
                                    <h3 class="profile-username text-center"><?php echo strtoupper($petInfo->name);?></h3>
                                    <p class="text-muted text-center info-text-color">Patient</p>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 border-right-new">
                                <div class="box">
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table>
                                            <tr>
                                                <th colspan="2"><label class="heading-text-color">Patient Details</th>
                                            </tr>
                                            <?php
                                            echo '<tr>
                                                    <td><label>Name</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->name.'</label></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Gender</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$gender.'</label></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Birthday</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->birthdate.'</label></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Microchip ID</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->microchipid.'</label></td>
                                                </tr>                                                
                                                <tr>
                                                    <td><label>Primary Breed</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->primarybreed.'</label></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Secondary Breed</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->secondarybreed.'</label></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Weight</label></td>
                                                    <td>&nbsp;</td>
                                                    <td><label class="info-text-color">'.$petInfo->currentweight.'</label></td>
                                                </tr>';
                                            ?>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="box">
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table>
                                            <?php
                                            echo '<tr>
                                            <th colspan="3"><label class="heading-text-color">Client</label></th>
                                        </tr>
                                        <tr>
                                            <td><label>Name</label></td>
                                             <td>&nbsp;</td>
                                            <td><label class="info-text-color">'.$petClientInfo->name.'</label></td>
                                        </tr>
                                        <tr>
                                            <td><label>Address</label></td>
                                             <td>&nbsp;</td>
                                            <td><label class="info-text-color">'.$petClientInfo->address.'</label></td>
                                        </tr>
                                        <tr>
                                            <td><label>Phone</label></td>
                                             <td>&nbsp;</td>
                                            <td><label class="info-text-color">'.$petClientInfo->phonenumber.'</label></td>
                                        </tr>
                                        <tr>
                                            <td><label>E-mail</label></td>
                                             <td>&nbsp;</td>
                                            <td><label class="info-text-color">'.$petClientInfo->email.'</label></td>
                                        </tr>';
                                            ?>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <!-- /.box -->
            <div class="panel">
                <div class="panel-body">
                    <!-- /.box -->
                    <div class="box box-information box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title text-uppercase">NUTRITION</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body notify-body-class"  ><!-- style="min-height: 763px"	 -->
                            <div class="col-lg-4">
                                <label class="heading-text-color">Weight</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-scale"></i></span>
                                    <label class="form-control" readonly="">N/A
                                        <?php
                                        //                                    if($petInfo->frequencyoffeed==''){
                                        //                                        echo 'N/A';
                                        //                                    }else{
                                        //                                        echo $petInfo->frequencyoffeed;
                                        //                                    }
                                        ?>
                                    </label>
                                </div>
                                </br>
                                <label class="heading-text-color">Feed Brand</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-vimeo-square"></i></span>
                                    <label class="form-control" readonly="">
                                        <a href="<?php echo $petInfo->primarylink;?>" target="_blank" style="cursor: pointer">
                                            <?php
                                            if($petInfo->primaryfood==''){
                                                echo 'N/A';
                                            }else{
                                                echo $petInfo->primaryfood;
                                            }
                                            ?>
                                        </a>
                                    </label>
                                </div>
                                &nbsp;
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-vimeo-square"></i></span>
                                    <label class="form-control" readonly="">
                                        <a href="<?php echo $petInfo->secondarylink;?>" target="_blank" style="cursor: pointer">
                                            <?php
                                            if($petInfo->secondaryfood==''){
                                                echo 'N/A';
                                            }else{
                                                echo $petInfo->secondaryfood;
                                            }
                                            ?>
                                        </a>
                                    </label>
                                </div>
                                </br>
                                <label class="heading-text-color">Feed Amount</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-vimeo-square"></i></span>
                                    <label class="form-control" readonly="" >
                                        <?php
                                        if($petInfo->amountoffeed==''){
                                            echo 'N/A';
                                        }else{
                                            echo $petInfo->amountoffeed;
                                        }
                                        ?>
                                    </label>
                                </div>
                                </br>
                                <label class="heading-text-color">Feeding(s) Per Day</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-vimeo-square"></i></span>
                                    <label class="form-control" readonly="" >
                                        <?php
                                        if($petInfo->frequencyoffeed==''){
                                            echo 'N/A';
                                        }else{
                                            echo $petInfo->frequencyoffeed;
                                        }
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div id="nutritionChart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <!-- /.box -->
            <div class="panel">
                <div class="panel-body">
                    <!-- /.box -->
                    <div class="box box-information box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">FITNESS DATA</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body notify-body-class"  ><!-- style="min-height: 763px"	 -->

                            <div class="col-lg-2">

                            </div>
                            <div class="col-lg-8">
                                <div class="row" style="text-align: center">
                                    <h4>Fitness Trend Graph
                                        <a href="#" data-toggle="tooltip" data-placement="bottom"
                                           title="Activity Points are a measurement of your overall activity calculated by the MisFit Device and from your GPS activity. These can be used to measure a pet's relative activity levels on a day to day basis." >(?)</a>
                                    </h4>
                                </div>
                                <div class="row">
                                    <div id="fitnessChart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <!-- /.box -->
            <div class="panel">
                <div class="panel-body">
                    <!-- /.box -->
                    <div class="box box-information box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">DOSAGE FREQUENCY</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body notify-body-class"  ><!-- style="min-height: 763px"	 -->
                            <div class="container horizontal-group">
                                <div class="row text-center">
                                    <?php
                                    if(count($medication)>0){
                                        for ($i=0;$i<count($medication);$i++){
                                            if ($i % 2 == 0) {
                                                $knobColor = '#ff851b';
                                            }elseif ($i % 3 == 0) {
                                                $knobColor = '#D81B60';
                                            }else{
                                                $knobColor = '#dd4b39';
                                            };

                                            ?>
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="text-align: center">
                                                <input class="dial" data-readOnly=true type="text" data-bgColor="#e0e0e0" rel="50" value="<?php echo $medicationPercent[$i];?>" data-fgColor="<?php echo $knobColor;?>">
                                                <div class="form-group has-success">
                                                    <label class="control-label" for="inputSuccess"><?php echo $medication[$i]['name'];?></label></br>
                                                    <label class="control-label" for="inputSuccess"><?php echo $medication[$i]['dosage'].' '.$medication[$i]['dosageunit'].' '.$medication[$i]['frequencytimes'].' times per day';?></label>
                                                </div>
                                            </div>
                                        <?php }}else{?>
                                        <div>
                                            <h4>No Medication Found</h4>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display:none ;">
            <!-- /.box -->
            <div class="panel">
                <div class="panel-body">
                    <!-- /.box -->
                    <div class="box box-information box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">JOURNAL</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body notify-body-class"  ><!-- style="min-height: 763px"	 -->
                            <div class="container">
                                <div id="timeline"><div class="row timeline-movement timeline-movement-top">
                                        <div class="timeline-badge timeline-future-movement">
                                            <a href="#">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                        </div>
                                        <div class="timeline-badge timeline-filter-movement">
                                            <a href="#">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="row timeline-movement">

                                        <div class="timeline-badge">
                                            <span class="timeline-balloon-date-day">18</span>
                                            <span class="timeline-balloon-date-month">APR</span>
                                        </div>


                                        <div class="col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <div class="timeline-panel credits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-11">
                                                    <div class="timeline-panel debits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--due -->

                                    <div class="row timeline-movement">


                                        <div class="timeline-badge">
                                            <span class="timeline-balloon-date-day">13</span>
                                            <span class="timeline-balloon-date-month">APR</span>
                                        </div>

                                        <div class="col-sm-offset-6 col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-11">
                                                    <div class="timeline-panel debits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <div class="timeline-panel credits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="row timeline-movement">


                                        <div class="timeline-badge">
                                            <span class="timeline-balloon-date-day">10</span>
                                            <span class="timeline-balloon-date-month">APR</span>
                                        </div>

                                        <div class="col-sm-offset-6 col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-11">
                                                    <div class="timeline-panel debits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6  timeline-item">
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <div class="timeline-panel credits">
                                                        <ul class="timeline-panel-ul">
                                                            <li><span class="importo">Mussum ipsum cacilds</span></li>
                                                            <li><span class="causale">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </li>
                                                            <li><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11/09/2014</small></p> </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</section>

<script>

    $(function($) {

        $(".dial").knob({
            change : function (value) {
                //console.log("change : " + value);
            },
            release : function (value) {
                //console.log(this.$.attr('value'));
                console.log("release : " + value);
            },
            cancel : function () {
                console.log("cancel : ", this);
            },
            format : function (value) {
                return value + '%';
            },
            draw : function () {

                // "tron" case
                if(this.$.data('skin') == 'tron') {

                    this.cursorExt = 0.3;

                    var a = this.arc(this.cv)  // Arc
                        , pa                   // Previous arc
                        , r = 1;

                    this.g.lineWidth = this.lineWidth;

                    if (this.o.displayPrevious) {
                        pa = this.arc(this.v);
                        this.g.beginPath();
                        this.g.strokeStyle = this.pColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                        this.g.stroke();
                    }

                    this.g.beginPath();
                    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                    this.g.stroke();

                    this.g.lineWidth = 2;
                    this.g.beginPath();
                    this.g.strokeStyle = this.o.fgColor;
                    this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                    this.g.stroke();

                    return false;
                }
            }
        });

        // Example of infinite knob, iPod click wheel
        var v, up=0,down=0,i=0
            ,$idir = $("div.idir")
            ,$ival = $("div.ival")
            ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
            ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
        $("input.infinite").knob(
            {
                min : 0
                , max : 20
                , stopper : false
                , change : function () {
                if(v > this.cv){
                    if(up){
                        decr();
                        up=0;
                    }else{up=1;down=0;}
                } else {
                    if(v < this.cv){
                        if(down){
                            incr();
                            down=0;
                        }else{down=1;up=0;}
                    }
                }
                v = this.cv;
            }
            });

//        $(".dial").each(function() {
//            var dis = this;
//            if ($(dis).val() == 0) {
//                $({
//                    value : 0
//                }).animate({
//                    value : parseInt($(dis).attr("rel"))
//                }, {
//                    duration : 2000,
//                    easing : 'swing',
//                    step : function() {
//                        $(dis).val(Math.ceil(this.value)).trigger('change');
//                        $(dis).val($(dis).val() + '%');
//                    }
//                })
//            }
//        });

        Highcharts.chart('nutritionChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Pet Weight'
            },
            xAxis: {
                categories: [<?php echo $monthValuesForWeight;?>]
            },
            yAxis: {
                title: {
                    text: 'Monthly Average Weight'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Month',
                marker: {
                    symbol: 'square'
                },
                data: [<?php echo $weightValuesForWeight;?>]

            }]
        });
        Highcharts.chart('fitnessChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [<?php echo $activityMonth;?>]
            },
            yAxis: {
                title: {
                    text: 'Daily average activity points'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Month',
                marker: {
                    symbol: 'square'
                },
                data: [<?php echo $activityPoint;?>]

            }]
        });
        $('[data-toggle="tooltip"]').tooltip()
    });

</script>
</body>
</html>

