<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BabelBark | Upload Medical File</title>
    <link rel="icon" type="image/ico" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap_backup.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/AdminLTE.min.css">
    <!-- iCheck -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body  class="layout-boxed">
<div class="login-box" style="width:800px;">
    <div class="login-logo" style="font-size:25px;">
        <img src="<?php echo base_url(); ?>assets/images/logobabelbark.png" width="250"><br>

    </div>
    <section class="content">
        <div class="box-body">
            <?php if($this->session->flashdata('error_message')){?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                </div>
            <?php }?>
            <?php if($this->session->flashdata('success_message')){?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                </div>
            <?php }?>
            <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
        </div>
        <?php //echo '<pre>'; print_r($row); echo '</pre>';?>

        <style>
            .collapsed-box {
                border:2px solid #F9CC85;
            }
        </style>
        <?php if($msg==""){?>
        <form name="medform" id="medform" action="<?php echo base_url();?>uploadMedicalFile/uploadFile" method="POST"  enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning" >
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" name="get_token" id="get_token" value="<?php echo $token;?>"/>
                                <input type="hidden" name="recordid" id="recordid" value="<?php echo $recorddetails['id'];?>"/>

                                <label>Appuser:</label>
                                <input id="appusername" class="form-control" type="text"  value="<?php echo $appuserdetails['firstname'].' '.$appuserdetails['lastname'];?> " disabled>
                            </div>
                            <div class="form-group">
                                <label>Pet:</label>
                                <input id="petname" class="form-control" type="text"  value="<?php echo $petdetails['name'];?> " disabled>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-xs-6">
                                        <!--                                        <input type="file" class="form-control" id="medicalfile" name="medicalfile" />-->
                                        <table class="table">
                                            <thead>
                                            <th><label>Select File<font color="red">*</font></label></th>
                                            <th><td><a onclick="addMoreRowsToAttachEdit(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                            </thead>
                                            <tbody id="addedRowsToAttachEdit">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="box-footer">
                                <input type="submit" class="btn btn-warning" name="update" id="update" value="Upload">
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div>
            </div><!-- /.col -->
</div>
</form>
<?php }
else{
    ?>
    <center>  <h1>   <?php  echo $msg;?></h1></center>
<?php }?>
</section><!-- /.content -->
</div>
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script>
    var rowCount=1 ;
    var files  ;
    function addMoreRowsToAttachEdit(frm){
        files = files + rowCount;
        var medicalfile = $('#medicalfile').val();
        var recRow = '<tr id="rowCount'+rowCount+'">' +
            "<td><input type='file' multiple='' name='files[]' id='"+files+"'/>"+"</td>" +
            '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td>'+
            '</tr>';
        jQuery('#addedRowsToAttachEdit').append(recRow);
        rowCount ++;
    }

    function removeRow(removeNum) {
        jQuery('#rowCount'+removeNum).remove();
    }
</script>
</body>
</html>
