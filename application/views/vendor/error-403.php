<!DOCTYPE html>
<html>
  <head><?php echo $head;?></head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <?php echo $header;?>
      <?php echo $sideMenu;?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>403 Error Page</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">403 error</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="error-page">
            <h2 class="headline text-red"> 403</h2>
            <div class="error-content"><br />
              <h3><i class="fa fa-warning text-red"></i> Access Denied.</h3>
              
              <p>
                We're sorry, but you are not authorized to view this page (Error: 403), you may <?php echo anchor("vendor/home",'Return to dashboard');?> or try using different Link.
              </p>
              
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php echo $footer;?>
      <?php echo $rightMenu;?>
  </body>
</html>
