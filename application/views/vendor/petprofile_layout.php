<?php
$admin=$this->session->userdata('admin');
$userrole=$admin['role'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        <?php
        if(ALLOW_VET == 1){
            echo BABELVET_TITLE;
        }else{
            echo BIZBARK_TITLE;
        }
        ?>
        <?php echo '| Pet Profile';?>
    </title>
    <link rel="icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--  <link href="<?php echo base_url();?>assets/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />-->


    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.css">
    <!-- Bootstrap DateTime Picker -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap time Picker -->
    <link href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap date Picker -->
    <link href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="<?php echo base_url();?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url();?>assets/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />


    <!-------------- Custom CSS for Veterinarian and vendors ------------------------------>

    <?php
    if(ALLOW_VET=='1'){?>
        <link href="<?php echo base_url();?>assets/css/skins/skin-blue.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/VetLTE.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/vet.css" rel="stylesheet" type="text/css" />
    <?php }else{?>
        <link href="<?php echo base_url();?>assets/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/admin.css" rel="stylesheet" type="text/css" />
    <?php } ?>



    <!-------------- End of Custom CSS for Veterinarian and vendors ------------------------------>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo link_tag(array('rel'=>"stylesheet",'href'=>"assets/css/jquery.fancybox.css",'type'=>'text/css'));?>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->

    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />


    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!--<script src="<?php echo base_url();?>assets/plugins/jQueryUI/jQuery-ui.min.js" type="text/javascript"></script>-->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-------------------- Chosen select -------------------------->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/chosen-select/docsupport/prism.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/chosen-select/chosen.css">

    <script src="<?php echo base_url();?>assets/chosen-select/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/chosen-select/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

    <!-------------------- Chosen select -------------------------->


    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- DATA TABES SCRIPT -->
    <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- Bootstrap DateTime Picker JS -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <!-- bootstrap date picker -->
    <script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1
    <script src="<?php //echo base_url();?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>-->
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/js/demo.js" type="text/javascript"></script>
    <!-- page script -->
    <!-- Fancybox script -->
    <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>

    <script src="<?php echo base_url();?>assets/js/Chart.js" type="text/javascript"></script>

    <!-- Include the jQuery Mobile library
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>-->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-list-filter.src.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/dual-list-box.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-dialog.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custome.css">
    <script src="<?php echo base_url();?>assets/plugins/jQuery-Knob-master/dist/jquery.knob.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jQuery-Knob-master/js/jquery.knob.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>



    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable({
                "iDisplayLength": 30
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true,
                "iDisplayLength": 20
            });

        });

        $(document).ready(function(){
            $(".cnf-del").on('click',function(){
                var cnf=confirm('Are You Sure ?');
                if(cnf==true)
                {
                    return true;
                }else
                {
                    return false;
                }
            });

            //Timepicker
            $(".timepicker").timepicker({
                minuteStep: 1,
                template: 'dropdown',
                appendWidgetTo: '.bootstrap-timepicker',
                showSeconds: true,
                showMeridian: false,
                defaultTime: false,
                maxHours:'24'
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#datetimepicker').datetimepicker({
                language: 'en',
                pick12HourFormat: true
            });
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function() {//add various fancybox.ajax in class to activate fancy box
            $(".various").fancybox({
                maxWidth	: 800,
                /*maxHeight	: 600,*/
                fitToView	: true,
                width		: 'auto',
                height		: 'auto',
                autoSize	: true,
                closeClick	: false,
                openEffect	: 'none',
                closeEffect	: 'none',
                afterClose:function () {
                    //window.location.reload();
                    $.fancybox.close();
                }
            });


        });
    </script>

    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>

    <style>
        .profile-user-img {
            border: 3px solid #d2d6de;
            margin: 0 auto;
            padding: 3px;
            width: 150px;
            height: 150px;
        }
        .border-right-new {
            border-right: 1px solid #3c8dbc;
        }
        .skin-blue .main-header .navbar {
            background-color: lightgrey;
        }
        label.heading-text-color {
            color: #3c8dbc;
            font-size: 1.3em;
            text-decoration: underline;
        }
        label.info-text-color {
            color: #3c8dbc;
        }

        /* The heart of the matter */
        .horizontal-group > .row {
            overflow-x: auto;
            white-space: nowrap;
        }
        .horizontal-group > .row > .col-xs-4 {
            display: inline-block;
            float: none;
        }
        .horizontal-group > .row > .col-lg-4 {
            display: inline-block;
            float: none;
        }
        .horizontal-group > .row > .col-md-4 {
            display: inline-block;
            float: none;
        }

    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>vendor/home" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="<?php echo base_url();?>assets/images/pet_profile_logo.png" style="width: 25px; height: 25px;" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="<?php echo base_url();?>assets/images/pet_profile_logo.png" style="width: 70%; height: 50%; padding-right: 1px;" /></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu open">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <?php if($admin['adminDetails']['logoimage']=="") {?>
                            <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="user-image" alt="Admin" />
                        <?php } else { ?>
                            <img src="<?php echo awsBucketPath.$admin['adminDetails']['logoimage'];?>" class="user-image" alt="Admin" style="background-color: white;" />
                        <?php } ?>
                        <span class="hidden-xs"> &nbsp;&nbsp;&nbsp;<?php echo ucfirst($admin['adminDetails']['firstname']);?></span>
                    </a>
                </li>
                <?php $controller=$this->uri->segment(2,'home');$method=$this->uri->segment(3,'index');?>
            </ul>
        </div>
    </nav>
</header>
<div>
    <?php echo $maincontent;?>
</div>
