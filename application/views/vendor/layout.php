 <?php $admin=$this->session->userdata('admin');
 $userrole=$admin['role'];
 if(isset($title)){
     $this->session->set_userdata('title2', $title);
 }
 ?>
<!DOCTYPE html>
<html>
  <head><?php echo $head;?></head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
     <?php echo $header;?>
     <?php echo $sideMenu;?>
   <!-- <?php if($userrole!="superadmin"){?> -->
      
  <!--  <?php }?> -->
      <!-- Content Wrapper. Contains page content -->
       <?php echo $maincontent;?>
     <!-- /.content-wrapper --> 
     <?php echo $footer;?>
     <?php echo $rightMenu;?>
     </div>
  </body>
</html>
