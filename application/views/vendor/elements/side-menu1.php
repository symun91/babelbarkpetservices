<?php 

$admin=$this->session->userdata('admin');
	$userrole=$admin['role'];
$segment_1=$this->uri->segment(2);
$segment_2=$this->uri->segment(3);


$customers="";
$appusers="";
$sikkacustomers = "";

$vendors='active';

if($segment_1=='home'){$vendors='active';
  if($segment_2=='listofappusers'){$appusers="active";$vendors='inactive';}
  if($segment_2=='listofgenericcustomers'){$customers="active";$vendors='inactive';}
  if($segment_2=='navigateToSikka'){$sikkacustomers="active";$customers='inactive';$vendors='inactie';}
}
?>

<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel user-panel-custom">
            <div class="pull-left image">
             
                    <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="img-circle" alt="Admin Image" style="height:52px; width:100px;"/>
                   
            </div>
            
            <div class="pull-left info">
               <p><?php echo ucfirst($admin['adminDetails']['username']);?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           
            <li class="treeview <?php echo $vendors;?>">
              <a href="<?php echo base_url();?>admin/home/index"">
                 <i class="fa fa-user "></i> <span>Vendors</span> <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li class="treeview <?php echo $appusers;?>">
              <a href="<?php echo base_url();?>admin/home/listofappusers">
                <i class="fa fa-mobile fa-lg"></i> <span>BabelBark Customers</span> <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li class="treeview <?php echo $customers;?>">
              <a href="<?php echo base_url();?>admin/home/listofgenericcustomers">
                <i class="fa fa-users "></i> <span>Generic Customers</span> <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
             <li class="treeview <?php echo $sikkacustomers;?>">
            <a href="<?php echo base_url();?>admin/home/navigateToSikka">
              <i class="fa fa-users "></i> <span>Sikka</span> <i class="fa fa-angle-right pull-right"></i>
            </a>
          </li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
