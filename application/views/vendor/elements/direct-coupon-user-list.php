<h4><label>Total Users Found : <?php echo $totalUsers;?></label></h4>
<table id="example3" class="table table-bordered table-striped">
		        			<thead>
				               <tr>
				                 <th><input type="checkbox" id="checkAll">&nbsp;&nbsp;&nbsp; <span id="checkAllTH">Select All</span></th>
				                 <th>User Name</th>
				                 <th>Nickname</th>
				                 <th>Email</th>
				                 <th>Gender</th>	
				                 <th>Login Type</th>
				                 <th>City</th>
				                 <th>State</th>
				                 <th>Country</th>					                 
				                 <th>Details</th>
				                </tr>
				                </thead>
				                <tbody>
				                <?php if(!empty($userList)){$i=1;?>
				                
				                   <?php foreach($userList as $key=>$row){?>
				                  
				                    	<tr>
				                    		<td><input type="checkbox" value="<?php echo $row['userid'];?>" name="userList[]" class="checkToAll" id="checkbox<?php echo $row['userid'];?>"></td>
				                    		<td><?php echo ucfirst($row['firstname'].' '.$row['lastname']);?></td>
				                    		<td><?php echo ucfirst($row['nickname']);?></td>
				                    		<td><?php echo $row['email'];?></td>
				                    		<td><?php echo $row['gender'];?></td>
				                    		<td><?php echo $row['logintype'];?></td>
				                    		<td><?php echo $row['cityname'];?></td>
				                    		<td><?php echo $row['statename'];?></td>
				                    		<td><?php echo $row['countryname'];?></td>
				                    		<td><?php echo anchor('admin/users/details/'.$row['userid'],'<span class="label label-primary">View Details</span>','title="Click to view User Details"');?></td>
				                    	</tr>
				                    <?php $i++;}?>
				                 <?php }else{?>
				                    <tr><td colspan="10" class="alert-danger">No Record To Display.Please select different filter options.</td></tr>
				                 <?php }?>
				                 </tbody>
				              </table>