 <footer class="main-footer">
  <div class="pull-right hidden-xs">
   <i class="fa fa-desktop"></i>
  </div>
  <strong>Copyright &copy; <?php echo date('Y');?>-<?php echo date('Y')+1;?> <a href="<?php echo base_url();?>vendor/home" class="text-red">BabelBark</a>.</strong> All rights reserved.
</footer>
