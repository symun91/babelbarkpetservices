<?php
$admin=$this->session->userdata('admin');
$userrole=$admin['role'];
$segment_1=$this->uri->segment(2);
$segment_2=$this->uri->segment(3);
$dashboard='';$location='';$state='';$city='';$preference='';$report='';$country='';$users="";$msg="";$promos='';
$coupons="";$addCoupon="";$addDirectCoupon=''; $promotions= '';$promotionsAdd = '';$promotionsList = '';
$home="";$adminList="";$addAdmin="";$profile="";$changePassword="";$preferenceList="";$preferenceReport="";
$customers="";$appointments="";$services="";$employees="";$settings="";

if($segment_1=='promotions'){$promotions='active';
    if($segment_2=='listing'){$promotionsList="active";}
    if($segment_2=='addPromotions'){$promotionsAdd="active";}
}

if($segment_1=='customer') $customers='active';
if($segment_1=='appointments') $appointments='active';
if($segment_1=='services') $services='active';
if($segment_1=='employee') $employees='active';
if($segment_1=='settings') $settings='active';

if($segment_1=='home'){$dashboard='active';
    if($segment_2=='' || $segment_2=='index'){$home="active";}
    if($segment_2=='adminList'){$adminList="active";}
    if($segment_2=='addAdmin'){$addAdmin="active";}
    if($segment_2=='profile'){$profile="active";}
    if($segment_2=='changePassword'){$changePassword="active";}
}

$vendorid = $admin['adminDetails']['vendorid'];
$sql = "select count(*) as noofappointment
                  from appointment,service,appusers,pet where appointment.serviceid = service.serviceid 
                  and appointment.appuserid = appusers.appuserid 
                  and appointment.petid =pet.petid  
                  and (status= 1 or status = 4)
                  and appointment.vendorid='{$vendorid}'";
$query = $this->db->query($sql);
$noOfAppointment = $query->row()->noofappointment;
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel user-panel-custom">
            <div class="pull-left image">

                <?php if($userrole=="employee"){?>
                    <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="img-circle" alt="Admin Image" style="height:52px; width:100px;"/>
                <?php } else if($admin['adminDetails']['logoimage']=="") {?>
                    <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="img-circle" alt="Admin Image" style="height:52px; width:100px;"/>
                <?php } else { ?>
                    <img src="<?php echo awsBucketPath.$admin['adminDetails']['logoimage'];?>" class="img-circle" alt="Admin Image" style="background-color: white;height:52px; width:100px;"/>
                <?php } ?>
            </div>

            <div class="pull-left info">
                <p><?php echo ucfirst($admin['adminDetails']['firstname'])?></p>
<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <!-- search form
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." />
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview <?php echo $dashboard;?>">
                <a href="<?php echo base_url();?>vendor/home">
                    <i class="fa fa-home"></i> <span>Dashboard</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
<!--                <ul class="treeview-menu">
                    <li class="<?php echo $home;?>"><a href="<?php echo base_url();?>vendor/home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                </ul>-->
            </li>
            <?php if(ALLOW_VET=='1'){?>
                <li class="treeview">
                    <a href="<?php echo base_url();?>vendor/sikka/importFromSikka"">
                    <i class="fa fa-server "></i> <span>Connect with Sikka</span> <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>
            <?php } ?>
            <li class="treeview <?php echo $promotions;?>">
                <a href="<?php echo base_url();?>vendor/promotions/listing"">
                <i class="fa fa-server <?php echo $promotionsList;?>"></i> <span>Promotions</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php echo $customers;?>">
                <a href="<?php echo base_url();?>vendor/customer/listofgenericcustomer">
                    <i class="fa fa-users "></i> <span>Customers</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php echo $appointments;?>">
                <a href="<?php echo base_url();?>vendor/appointments/listofappointments">
                    <i class="fa fa-calendar"></i> <span>Appointments</span> <i class="fa fa-angle-right pull-right"></i>
                    <?php
                    if($noOfAppointment>0){
                        echo '</i><span class="badge badge-notify">'.$noOfAppointment.'</span>';
                    }
                    ?>
                </a>
            </li>
            <li class="treeview <?php echo $services;?>">
                <a href="<?php echo base_url();?>vendor/services/listofservices"><i class="fa fa-wrench"></i> <span>Services</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php echo $employees;?>">
                <a href="<?php echo base_url();?>vendor/employee/listofemployees">
                    <i class="fa fa-users "></i> <span>Employees</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>

            <li class="treeview <?php echo $settings;?>">
                <a href="<?php echo base_url();?>vendor/settings"><i class="fa fa-cog"></i> <span>Settings</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
