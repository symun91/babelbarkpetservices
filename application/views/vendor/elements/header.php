<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>

<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>vendor/home" class="logo">
        <?php if (ALLOW_VET == '1') { ?>
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/images/BabelVet_logo.png" style="width: 25px; height: 25px;" /></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/BabelVet_logo.png" style="width: 70%; height: 50%; padding-right: 1px;" /></span>
        <?php } else { ?>
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/images/BizBark_Logo.png" style="width: 25px; height: 25px;" /></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/BizBark_Logo.png" style="width: 70%; height: 50%; padding-right: 1px;" /></span>
        <?php } ?>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if (isset($admin['adminDetails']['logoimage']) && $admin['adminDetails']['logoimage'] == "") { ?>
                            <img src="<?php echo base_url() . 'assets/images/userimg.png'; ?>" class="user-image" alt="Admin" />
                        <?php } else { ?>
                            <img src="<?php echo isset($admin['adminDetails']['logoimage']) ? awsBucketPath . $admin['adminDetails']['logoimage'] : ''; ?>" class="user-image" alt="Admin" style="background-color: white;" />
                        <?php } ?>
                        <span class="hidden-xs"> &nbsp;&nbsp;&nbsp;<?php echo ucfirst($admin['adminDetails']['firstname']); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header dm-wdt">
                            <?php if (isset($admin['adminDetails']['logoimage']) && $admin['adminDetails']['logoimage'] == "") { ?>
                                <img src="<?php echo base_url() . 'assets/images/userimg.png'; ?>" class="img-circle" alt="Admin Image" />
                            <?php } else { ?>
                                <img src="<?php echo isset($admin['adminDetails']['logoimage']) ? awsBucketPath . $admin['adminDetails']['logoimage'] : ''; ?>" class="img-circle" alt="Admin Image"  style="background-color: white;"/>
                            <?php } ?>
                            <p>
                                <?php echo ucfirst($admin['adminDetails']['firstname']); ?> - Administrator
                                <small><?php echo change_date_format(date('Y-m-d')); ?></small>
                            </p>
                        </li>
                        <li class="user-footer dm-wdt">
                            <div class="btn-group" role="group" aria-label="...">
                                <?php if ($userrole == "admin") { ?>
                                    <?php echo anchor('vendor/home/profile', 'Profile', 'class="btn btn-default btn-flat"'); ?>
                                    <?php echo anchor('vendor/home/changePassword', 'Change Password', 'class="btn btn-default btn-flat"'); ?>
                                <?php } ?>
                                <?php echo anchor('vendor/administrator/logout', 'Sign out', 'class="btn btn-default btn-flat"'); ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php $controller = $this->uri->segment(2, 'home');
                $method = $this->uri->segment(3, 'index'); ?>
                <!-- Lock Unlock Screen -->
                <li>
<?php //echo anchor('admin/lock/'.$controller.'/'.$method,'<i class="fa fa-lock fa-lg"></i>','title="Lock Screen"'); ?>
                </li>
                <!-- Control Sidebar Toggle Button 
                <li>
                  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears fa-lg"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>