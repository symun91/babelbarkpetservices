 <?php $admin=$this->session->userdata('admin');
 $userrole=$admin['role'];
 date_default_timezone_set('UTC');
 ?>
 <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url();?>vendor/home" class="logo">
            <?php
            if(ALLOW_VET=='1'){?>
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><img src="<?php echo base_url();?>assets/images/BabelVet_logo.png" style="width: 25px; height: 25px;" /></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="<?php echo base_url();?>assets/images/BabelVet_logo.png" style="width: 70%; height: 50%; padding-right: 1px;" /></span>
            <?php }else{?>
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><img src="<?php echo base_url();?>assets/images/BizBark_Logo.png" style="width: 25px; height: 25px;" /></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="<?php echo base_url();?>assets/images/BizBark_Logo.png" style="width: 70%; height: 50%; padding-right: 1px;" /></span>
            <?php } ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="user-image" alt="Admin" />
                  <span class="hidden-xs"> &nbsp;&nbsp;&nbsp;<?php echo ucfirst($admin['adminDetails']['username']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header dm-wdt">
                    <img src="<?php echo base_url().'assets/images/userimg.png';?>" class="img-circle" alt="Admin Image" />
                  
                    <p>
                      <?php echo ucfirst($admin['adminDetails']['username']);?> - Administrator
                      <small><?php echo change_date_format(date('Y-m-d'));?></small>
                    </p>
                  </li>
                  <li class="user-footer dm-wdt">
                  	<div class="btn-group" role="group" aria-label="...">
					  <?php echo anchor('admin/home/adminChangePassword','Change Password','class="btn btn-default btn-flat"');?>
					  <?php echo anchor('admin/logout','Sign out','class="btn btn-default btn-flat"');?>
					</div>
                  </li>
                </ul>
              </li>
              <?php $controller=$this->uri->segment(2,'home');$method=$this->uri->segment(3,'index');?>
              <!-- Lock Unlock Screen -->
              <li>
                <?php //echo anchor('admin/lock/'.$controller.'/'.$method,'<i class="fa fa-lock fa-lg"></i>','title="Lock Screen"');?>
              </li>
              <!-- Control Sidebar Toggle Button 
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears fa-lg"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>

        <div id="spinner" 
  style="display:none;position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url();?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>
