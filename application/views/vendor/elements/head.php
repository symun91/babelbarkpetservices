<meta charset="UTF-8">
<title>
    <?php
    if(ALLOW_VET == 1){
        echo BABELVET_TITLE;
    }else{
        echo BIZBARK_TITLE;
    }
    ?>
    <?php if($this->uri->segment(2)!=''){echo '| '.ucfirst($this->uri->segment(2));}?>
    <?php
    $getUriData2 = $this->uri->segment(2);
    $getUriData3 = $this->uri->segment(3);
    if($getUriData3 == 'addemployee'){
        echo '| Add Employee';
    }elseif(($getUriData2 == 'employee') && ($getUriData3 == 'viewDetails')){
        echo '| View Details';
    }else{
    ?>
    <?php if($this->uri->segment(3)!='' && $this->uri->segment(3)!='index'){echo '| '.ucfirst($this->uri->segment(3));}?>
    <?php }?>
    
</title>
<link rel="icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!--  <link href="<?php echo base_url();?>assets/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />-->


<!-- Bootstrap 3.3.4 -->
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/wizards.css" rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->

<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.css">
<!-- Bootstrap DateTime Picker -->
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap time Picker -->
<link href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />

<!-- Bootstrap date Picker -->
<link href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<!-- Select2 -->
<link href="<?php echo base_url();?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="<?php echo base_url();?>assets/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />


<!-------------- Custom CSS for Veterinarian and vendors ------------------------------>

<?php
if(ALLOW_VET=='1'){?>
    <link href="<?php echo base_url();?>assets/css/skins/skin-blue.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/VetLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/vet.css" rel="stylesheet" type="text/css" />
<?php }else{?>
    <link href="<?php echo base_url();?>assets/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/admin.css" rel="stylesheet" type="text/css" />
<?php } ?>



<!-------------- End of Custom CSS for Veterinarian and vendors ------------------------------>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php echo link_tag(array('rel'=>"stylesheet",'href'=>"assets/css/jquery.fancybox.css",'type'=>'text/css'));?>

<!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->

<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />


<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url();?>assets/plugins/jQueryUI/jQuery-ui.min.js" type="text/javascript"></script>-->
<!-- Bootstrap 3.3.2 JS -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-------------------- Chosen select -------------------------->

<link rel="stylesheet" href="<?php echo base_url();?>assets/chosen-select/docsupport/prism.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/chosen-select/chosen.css">

<script src="<?php echo base_url();?>assets/chosen-select/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/chosen-select/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

<!-------------------- Chosen select -------------------------->


<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<!-- Select2 -->
<script src="<?php echo base_url();?>assets/plugins/select2/select2.full.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- Bootstrap DateTime Picker JS -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- bootstrap date picker -->
<script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<!-- iCheck 1.0.1
    <script src="<?php //echo base_url();?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>-->
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/js/app.min.js" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/js/demo.js" type="text/javascript"></script>
<!-- page script -->
<!-- Fancybox script -->
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>

<script src="<?php echo base_url();?>assets/js/Chart.js" type="text/javascript"></script>

<!-- Include the jQuery Mobile library
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>-->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-list-filter.src.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/dual-list-box.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-dialog.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/counter.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/custom.js" type="text/javascript"></script>



<script type="text/javascript">
    $(function () {
        $("#example1").DataTable({
            "iDisplayLength": 30
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": true,
            "iDisplayLength": 20
        });

    });

    $(document).ready(function(){
        $(".cnf-del").on('click',function(){
            var cnf=confirm('Are You Sure ?');
            if(cnf==true)
            {
                return true;
            }else
            {
                return false;
            }
        });

        //Timepicker
        $(".timepicker").timepicker({
            minuteStep: 1,
            template: 'dropdown',
            appendWidgetTo: '.bootstrap-timepicker',
            showSeconds: true,
            showMeridian: false,
            defaultTime: false,
            maxHours:'24'
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#datetimepicker').datetimepicker({
            language: 'en',
            pick12HourFormat: true
        });
    });
</script>

<script type="text/javascript">

    $(document).ready(function() {//add various fancybox.ajax in class to activate fancy box
        $(".various").fancybox({
            maxWidth	: 800,
            /*maxHeight	: 600,*/
            fitToView	: true,
            width		: 'auto',
            height		: 'auto',
            autoSize	: true,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none',
            afterClose:function () {
                //window.location.reload();
                $.fancybox.close();
            }
        });


    });
</script>

<script type="text/javascript">
    var baseurl = "<?php echo base_url(); ?>";
</script>



      
