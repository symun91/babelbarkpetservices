<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<style>
    .box{
        border-top: 0 !important;
    }
</style>
<div class="content-wrapper">       
    <section class="content-header">
        <h1>
            <?php
            if ($servicedetail['serviceid'] == 0)
                echo "Add Service";
            else
                echo "Service Detail";
            ?>
        </h1>
        <?php if ($servicedetail['serviceid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Service</a></li>
                <li class="active">Add Service</a></li>
            </ol>
        <?php } ?>
    </section>
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <?php if (validation_errors() != "") { ?>
                <div class="alert alert-danger" id="validation">
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <label id="validerrorform"><?php echo validation_errors(); ?></label>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <form role="form" action="<?php echo base_url(); ?>vendor/services/updateService" method="POST" name="addserviceform" id="addserviceform" enctype="multipart/form-data">
            <input type="hidden" name="serviceid" id="serviceid" value="<?php echo $servicedetail['serviceid']; ?>"/>
            <div class="box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="stepwizard col-md-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                    <p>Service Information</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                    <p>Availability</p>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 start ##########################################-->
                        <div class="row setup-content" id="step-1">
                            <div class="col-xs-12 col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Service Name<font color="red">*</font></label>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <input type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Service Name" id="servicename" name="servicename" value="<?php echo set_value('servicename', $servicedetail['name']); ?>"/>
                                                    </div>
                                                </div> 
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Service Type<font color="red">*</font></label>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <select class="form-control" name="selecttype" id="selecttype">
                                                            <option value="0">Select Service Type</option>
                                                            <?php
                                                            foreach ($servicetypes as $key => $value) {
                                                                // for($i=0;$i<count($servicetypes);$i++){
                                                                ?>
                                                                <?php if (strcmp($servicedetail['type'], $value) == 0) {
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" selected <?php echo set_select('slecttype') ?> ><?php echo $value; ?></option>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" <?php echo set_select('selecttype') ?> ><?php echo $value; ?></option>
                                                                <?php } ?>

                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Service Price<font color="red">*</font></label>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <input type="text" class="form-control allownumericwithdecimal" placeholder="Service Price" id="serviceprice" name="serviceprice" value="<?php echo set_value('"serviceprice"', $servicedetail['price']); ?>" maxlength="10"/>
                                                    </div>
                                                </div>                                                
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <label for=""> </label>&nbsp;&nbsp;
                                            Total $ <input type="radio" name="priceby" value="total"  <?php echo set_value('priceby', $servicedetail['priceby']) == "total" ? "checked" : ""; ?>/>
                                            &nbsp;&nbsp;$ By Hour  <input type="radio" name="priceby" value="byhour" <?php echo set_value('priceby', $servicedetail['priceby']) == "byhour" ? "checked" : ""; ?> />
                                            &nbsp;&nbsp;$ By Day  <input type="radio" name="priceby" value="days" <?php echo set_value('priceby', $servicedetail['priceby']) == "days" ? "checked" : ""; ?> />
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label>Payment Type</label>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <select class="form-control" name="selectpaymenttype" id="selectpaymenttype">
                                                            <option value="">Select Payment Type</option>
                                                            <?php for ($i = 0; $i < count($paymenttypes); $i++) { ?>
                                                                <option value="<?php echo $paymenttypes[$i]; ?>" <?php echo set_select('selectpaymenttype', $paymenttypes[$i], ( $servicedetail['paymenttype'] == $paymenttypes[$i] ? TRUE : FALSE)); ?>><?php echo $paymenttypes[$i]; ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Duration </label>&nbsp;(Please set service duration to 0 if customers are allowed to determine their own duration for this service.)
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <input type="text" class="form-control allownumericwithoutdecimal" placeholder="Duration" name="duration"  id="duration" value="<?php echo set_value('duration', $servicedetail['duration']); ?>"/>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <label for=""> </label>&nbsp;&nbsp;
                                            Days  <input type="radio" name="durationunit" value="days" <?php echo set_value('durationunit', $servicedetail['durationunit']) == "days" ? "checked" : ""; ?> />
                                            &nbsp;&nbsp;Hours <input type="radio" name="durationunit" value="hours"  <?php echo set_value('durationunit', $servicedetail['durationunit']) == "hours" ? "checked" : ""; ?>/>
                                            &nbsp;&nbsp;Minutes  <input type="radio" name="durationunit" value="minutes" <?php echo set_value('durationunit', $servicedetail['durationunit']) == "minutes" ? "checked" : ""; ?> />
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Capacity</label>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <input type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Maximum number of concurrent appointments" id="capacity" name="capacity" value="<?php echo set_value('capacity', $servicedetail['capacity']); ?>"/>
                                                    </div>  
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <label>Assign Employee?&nbsp;&nbsp;</label>
                                            Yes  <input type="radio" name="assign_emp_status" value="1" <?php echo set_value('assign_emp_status', $servicedetail['assign_emp_status']) == "1" ? "checked" : ""; ?> />
                                            &nbsp;&nbsp;No <input type="radio" name="assign_emp_status" value="0"  <?php echo set_value('assign_emp_status', $servicedetail['assign_emp_status']) == "0" ? "checked" : ""; ?>/>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label>Schedule Options </label>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <label for=""> </label>&nbsp;&nbsp;
                                                        Available appointment request  <input type="radio" name="available_status" value="1" <?php echo set_value('available_status', $servicedetail['available_status']) == "1" ? "checked" : ""; ?> />
                                                        &nbsp;&nbsp;Unavailable appointment request <input type="radio" name="available_status" value="0"  <?php echo set_value('available_status', $servicedetail['available_status']) == "0" ? "checked" : ""; ?>/>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-2"><label>Minimal Notice</label></div>
                                                    <div class="col-xs-2">
                                                        <input type="text" class="form-control allownumericwithdecimal" id="minimal_notice" name="minimal_notice" value="<?php echo set_value('"minimal_notice"', $servicedetail['minimal_notice']); ?>" maxlength="10"/> 
                                                    </div>
                                                    <div class="col-xs-1">(hours)</div>
                                                    <div class="col-xs-2"><label>Maximum  Notice</label></div>
                                                    <div class="col-xs-2">
                                                        <input type="text" class="form-control allownumericwithdecimal" id="maximum_notice" name="maximum_notice" value="<?php echo set_value('"maximum_notice"', $servicedetail['maximum_notice']); ?>" maxlength="10"/> 
                                                    </div> 
                                                    <div class="col-xs-1">(weeks)</div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 end ##########################################-->
                        <!--############################# STEP 2 start ##########################################-->
                        <div class="row setup-content" id="step-2">
                            <div class="col-xs-6 col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Availability</label>
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        Days
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <?php
                                                        $checkeddays_arr = array();
                                                        if ($servicedetail['availabilitydays'] != "") {
                                                            $checkeddays_arr = explode(",", $servicedetail['availabilitydays']);
                                                        }
                                                        for ($i = 0; $i < count($weekdays); $i++) {
                                                            ?>
                                                            <?php echo $weekdays[$i]; ?> &nbsp;<input type="checkbox"  name="availabilitydays[]" value="<?php echo $weekdays[$i]; ?>"  <?php echo (in_array($weekdays[$i], $checkeddays_arr) ? 'checked' : null); ?> onchange="dayselected(<?php echo $i; ?>);"/>
                                                            &nbsp;&nbsp; &nbsp;&nbsp;
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        Hours
                                                    </div>
                                                    <br/> 
                                                    <input type="hidden" id="availabityfromhours" name="availabityfromhours"/>
                                                    <input type="hidden" id="availabitytohours" name="availabitytohours"/>
                                                    <div class="availabilitydetail"></div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Reservation</label>
                                                <input type="checkbox" name="reservation" value="1" <?php echo ($servicedetail['reservation'] == '1' ? 'checked' : null); ?>/>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Restrictions</label>
                                                <input type="checkbox" id="restriction" name="restriction" value="0" <?php echo ($servicedetail['restriction'] == '1' ? 'checked' : null); ?>/>
                                                <div class="restrictdetail">
                                                    <div class="row">
                                                        <div class="col-xs-2">
                                                            Age Allowed
                                                        </div>
                                                        <div class="col-xs-1"> &nbsp;</div>
                                                        <div class="col-xs-3">
                                                            <label>From </label>
                                                        </div>
                                                        <div class="col-xs-1">&nbsp;</div>
                                                        <div class="col-xs-3">
                                                            <label>To </label>
                                                        </div>
                                                        <div class="col-xs-1"> &nbsp;</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-1">&nbsp;</div>
                                                        <div class="col-xs-1"> 
                                                            <select class="form-control" style="width:120px;" name="agelimit" id="agelimit">
                                                                <option value="inbetween" <?php echo set_select('agelimit', 'inbetween', ( $servicedetail['restrictagelimit'] == 'inbetween' ? TRUE : FALSE)); ?>>In between</option>
                                                                <option value="atleast" <?php echo set_select('agelimit', 'atleast', ( $servicedetail['restrictagelimit'] == 'atleast' ? TRUE : FALSE)); ?>>At least</option>
                                                                <option value="atmost" <?php echo set_select('agelimit', 'atmost', ( $servicedetail['restrictagelimit'] == 'atmost' ? TRUE : FALSE)); ?>>At most</option>
                                                            </select> 
                                                        </div>
                                                        <?php
                                                        $fromage = $servicedetail['restrictfromage'];
                                                        $fromageyears = "";
                                                        $fromagemonths = "";
                                                        if ($fromage != "" && $fromage != "0") {
                                                            $fromtemp = explode("years", $fromage);
                                                            $fromageyears = $fromtemp[0];
                                                            if (count($fromtemp) > 1 && $fromtemp[1] != "") {
                                                                $fromtemp1 = explode("months", $fromtemp[1]);
                                                                $fromagemonths = $fromtemp1[0];
                                                            }
                                                        }
                                                        $toage = $servicedetail['restricttoage'];
                                                        $toageyears = "";
                                                        $toagemonths = "";
                                                        if ($toage != "" && $toage != "0") {
                                                            $totemp = explode("years", $toage);
                                                            $toageyears = $totemp[0];
                                                            if (count($totemp) > 1 && $totemp[1] != "") {
                                                                $totemp1 = explode("months", $totemp[1]);
                                                                $toagemonths = $totemp1[0];
                                                            }
                                                        }
                                                        ?>
                                                        <div class="col-xs-1">&nbsp;</div>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control allownumericwithoutdecimal" name="fromageyrs" id="fromageyrs" value="<?php echo $fromageyears; ?>">
                                                        </div>
                                                        <div class="col-xs-1"> Years</div>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control allownumericwithoutdecimal" name="fromagemonths" id="fromagemonths" value="<?php echo $fromagemonths; ?>">
                                                        </div>
                                                        <div class="col-xs-1"> Months</div>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control allownumericwithoutdecimal" name="toageyrs" id="toageyrs" value="<?php echo $toageyears; ?>">
                                                        </div>
                                                        <div class="col-xs-1"> Years</div>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control allownumericwithoutdecimal" name="toagemonths"  id="toagemonths" value="<?php echo $toagemonths; ?>">
                                                        </div>
                                                        <div class="col-xs-1"> Months</div>
                                                    </div>
                                                    <br>  
                                                    <div class="row">
                                                        <div class="col-xs-2">
                                                            Weight Allowed
                                                        </div>
                                                        <div class="col-xs-1"> &nbsp;</div>
                                                        <div class="col-xs-3">
                                                            <label>From </label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label>To </label>
                                                        </div>
                                                        <div class="col-xs-1"> &nbsp;</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-1">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-xs-2"> 
                                                            <select class="form-control" style="width:120px;" name="weightlimit" id="weightlimit">
                                                                <option value="inbetween" <?php echo set_select('weightlimit', 'inbetween', ( $servicedetail['restrictweightlimit'] == 'inbetween' ? TRUE : FALSE)); ?>>In between</option>
                                                                <option value="atleast" <?php echo set_select('weightlimit', 'atleast', ( $servicedetail['restrictweightlimit'] == 'atleast' ? TRUE : FALSE)); ?>>At least</option>
                                                                <option value="atmost" <?php echo set_select('weightlimit', 'atmost', ( $servicedetail['restrictweightlimit'] == 'atmost' ? TRUE : FALSE)); ?>>At most</option>
                                                            </select> 
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="text" class="form-control allownumericwithdecimal" name="fromwt" id="fromwt" value="<?php echo set_value('fromwt', $servicedetail['restrictfromwt']); ?>"> 
                                                        </div>
                                                        <div class="col-xs-1"><?php echo $weightunit; ?></div>
                                                        <div class="col-xs-2">
                                                            <input type="text" class="form-control allownumericwithdecimal" name="towt" id="towt" value="<?php echo set_value('towt', $servicedetail['restricttowt']); ?>"> 
                                                        </div>
                                                        <div class="col-xs-1"><?php echo $weightunit; ?></div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-xs-2">
                                                            Medical Conditions Not allowed
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <textarea rows="4" cols="90" name="medicalcondnotallowed countable" data-length="1500"><?php echo $servicedetail['medicalcondnotallowed']; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label class="control-label col-xs-2 text-right">
                                                    Calender Color:
                                                    <span class="help-block" id="color_calender">&nbsp;</span>
                                                </label>
                                                <div class="col-xs-2">
                                                    <?php $calender_color = isset($servicedetail['calendar_color']) ? $servicedetail['calendar_color'] : '#5367ce'; ?>
                                                    <input id="calendar_color" name="calendar_color" type="text" class="form-control" value="<?php echo set_value('calendar_color', $calender_color); ?>">

                                                    <script>
                                                        $(function () {
                                                            $('#color_calender').css({'background': '<?php echo $calender_color; ?>'});
                                                            $('#calendar_color').colorpicker().on('changeColor', function (e) {
                                                                $('#color_calender')[0].style.backgroundColor = e.color.toString('rgba');
                                                            });
                                                        });
                                                    </script>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Notes</label>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <textarea rows="4" class="form-control" name="servicenotes"><?php echo $servicedetail['notes']; ?></textarea>
                                                    </div>
                                                    <div class="col-xs-6"></div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                        <?php
                                        if ($userrole == "admin") {
                                            if ($servicedetail['serviceid'] == 0) {
                                                ?>
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Save">
                                            <?php } else { ?>
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Update">
                                                <?php
                                            }
                                        }
                                        #echo anchor('vendor/services/listofservices', 'Back', 'class="btn  btn-primary"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 2 end ##########################################-->
                    </div>
                </div> 
            </div>
        </form>
    </section>
</div>
<?php include_once 'service_js.php'; ?>