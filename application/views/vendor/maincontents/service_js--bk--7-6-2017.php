
<!-- timepicker style -->
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
<!-- timepicker js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js" type="text/javascript"></script>
<script type="text/javascript">

    $(window).load(function () {
        $("#validation").hide();
        var check = "<?php echo $servicedetail['restriction']; ?>";
        if (check == 1) {
            $('.restrictdetail').show();
        } else
        {
            $('.restrictdetail').hide();
        }
    });

    var daycount = 0;
    var startindex = 0;
    var applyall = 0;
    var defaultfromhoursstr = "";
    var defaulttohoursstr = "";
    $(document).ready(function () {


        setTimePicker = function (index)
        {

            $("#from_" + index).timepicker({showMeridian: false});
            $("#to_" + index).timepicker({showMeridian: false});

            /*$("#from_"+index).timepicker({ 'timeFormat': 'H:i' });
             $("#to_"+index).timepicker({ 'timeFormat': 'H:i' });*/

            $("#from_" + index).timepicker('setTime', new Date());
            $("#to_" + index).timepicker('setTime', new Date());

            $("#from_" + index).bind('changeTime.timepicker', onStartTimeChange(index));
            $("#from_" + index).bind('change', onStartTimeChange(index));

            if (applyall == 1)
            {
                $("#from_" + index).timepicker('setTime', defaultfromhoursstr);
                $("#to_" + index).timepicker('setTime', defaulttohoursstr);
            }
        }

        function onStartTimeChange(index)
        {
            var starttime = $("#from_" + index).val();
            var estduration = $("#duration").val();
            var durationunit = $('input[name=durationunit]:checked').val();

            if (starttime != "")
            {
                // Duration is in Hours
                if ((estduration != "") && (durationunit == "hours"))
                {
                    var textArr = starttime.split(":");
                    var textArr1 = textArr[1].split("");
                    var origDate = new Date(1, 1, 1, textArr[0], textArr1[0] + "" + textArr1[1], 0, 0);

                    var timeformate = textArr1[2] + "" + textArr1[3];
                    if (origDate.getHours() < 12)
                    {
                        origDate.setHours(origDate.getHours() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {
                            if (timeformate == "pm")
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                                    $("#to_" + index).val(endtime);
                                } else
                                {
                                    if (origDate.getHours() == 12)
                                    {
                                        var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                                        $("#to_" + index).val(endtime);
                                    } else
                                    {
                                        var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                                        $("#to_" + index).val(endtime);
                                    }
                                }
                            } else
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                                    $("#to_" + index).val(endtime);
                                } else
                                {
                                    if (origDate.getHours() == 12)
                                    {
                                        var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                                        $("#to_" + index).val(endtime);
                                    } else
                                    {
                                        var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                                        $("#to_" + index).val(endtime);
                                    }
                                }
                            }
                        } else
                        {
                            if (origDate.getHours() == 0)
                            {
                                var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#to_" + index).val(endtime);
                            } else
                            {
                                var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#to_" + index).val(endtime);
                            }
                        }
                    } else
                    {
                        origDate.setHours(origDate.getHours() + parseInt(estduration));
                        if (origDate.getHours() == 0)
                        {
                            var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#to_" + index).val(endtime);
                        } else
                        {
                            if (origDate.getHours() < 12)
                            {
                                var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#to_" + index).val(endtime);
                            } else
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                    $("#to_" + index).val(endtime);
                                } else
                                {
                                    var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                    $("#to_" + index).val(endtime);
                                }
                            }
                        }
                    }
                }
                // Duration is in Minutes
                else if ((estduration != "") && (durationunit == "minutes"))
                {
                    var textArr = starttime.split(":");
                    var textArr1 = textArr[1].split("");
                    var origDate = new Date(1, 1, 1, textArr[0], textArr1[0] + "" + textArr1[1], 0, 0);

                    var timeformate = textArr1[2] + "" + textArr1[3];
                    // 11:59 AM to 12:00 PM
                    if (origDate.getHours() == 11 && timeformate == "am")
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {
                            var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                            var hours = endTime.split(":");
                            if (hours[0] == 00)
                            {
                                endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                            }
                            $("#to_" + index).val(endTime);
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#to_" + index).val(endTime);
                        }

                    }
                    // 11:59 PM to 12:00 AM
                    else if (origDate.getHours() == 11 && timeformate == "pm")
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {
                            var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                            var hours = endTime.split(":");

                            if (hours[0] == 00)
                            {
                                endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                            }
                            $("#to_" + index).val(endTime);
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#to_" + index).val(endTime);
                        }
                    } else
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 13)
                        {
                            if (timeformate == "am")
                            {
                                var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "pm";
                                $("#to_" + index).val(endTime);
                            } else
                            {
                                var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + "am";
                                $("#to_" + index).val(endTime);
                            }
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#to_" + index).val(endTime);
                        }

                    }
                }
            }
        }

        Date.prototype.toInputFormat = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();
            return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
        };

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }



        var weekdayarr = ['M', 'T', 'W', 'Th', 'F', 'Sa', 'Su'];
        var userrole = "<?php echo $userrole; ?>";
        var avadays = "<?php echo $servicedetail['availabilitydays']; ?>";
        var avafromhour = "<?php echo $servicedetail['availabilityfromhour']; ?>";
        var avatohour = "<?php echo $servicedetail['availabilitytohour']; ?>";

        if (avadays != "" && avadays != 0)
        {
            var avadaysarr = avadays.split(',');
            var avafromhourarr = avafromhour.split(',');
            var avatohourarr = avatohour.split(',');

            for (var i = 0; i < avadaysarr.length; i++)
            {

                var index = weekdayarr.indexOf(avadaysarr[i]);
                dayselected(index);
                $("#from_" + index).val(avafromhourarr[i]);
                $("#to_" + index).val(avatohourarr[i]);

            }
        }

        if (userrole == "employee")
            $(".box-body :input").attr("disabled", "disabled");

<?php if ($servicedetail['serviceid'] == 0) { ?>
            $('.restrictdetail').hide();
<?php } else { ?>
            $('.restrictdetail').show();
<?php } ?>


<?php if ($servicedetail['restrictagelimit'] != "") { ?>

            var sellimit = "<?php echo $servicedetail['restrictagelimit'] ?>";
            displayAgeInputs(sellimit);
<?php } ?>

<?php if ($servicedetail['restrictweightlimit'] != "") { ?>

            var selwtlimit = "<?php echo $servicedetail['restrictweightlimit'] ?>";
            displayWeightInputs(selwtlimit);
<?php } ?>


        $('#restriction').change(function () {
            if ($(this).is(":checked")) {
                $('.restrictdetail').show();
                $('#restriction').val(1);
            } else
            {
                $('.restrictdetail').hide();
                $('#restriction').val(0);
            }

        });

        $("#agelimit").on('change', function () {

            var sellimit = this.value;
            displayAgeInputs(sellimit);

        });

        $("#weightlimit").on('change', function () {

            var sellimit = this.value;
            displayWeightInputs(sellimit);

        });

        $(".allownumericwithoutdecimal").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });

        $(".allownumericwithdecimal").on("keyup blur", function (e) {
            var val = $(this).val();
            if (isNaN(val)) {
                val = val.replace(/[^0-9\.]/g, '');
                if (val.split('.').length > 2)
                    val = val.replace(/\.+$/, "");
            }
            $(this).val(val);
        });

        $(".allowcharacterwithouthtmltags").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/(<([^>]+)>)/ig, ""));
            /*if((e.which > 64 && e.which < 91 ) || (e.which > 96 && e.which < 123)){
             event.preventDefault();
             }*/
        });

        var weekdayarr = ['M', 'T', 'W', 'Th', 'F', 'Sa', 'Su'];
        $("#update").click(function () {
            var fromhoursstr = "";
            var tohoursstr = "";
            $.each($("input[name='availabilitydays[]']:checked"), function () {
                //values.push($(this).val());
                var index = weekdayarr.indexOf($(this).val());
                if (fromhoursstr == "")
                    fromhoursstr = $("#from_" + index).val();
                else
                    fromhoursstr = fromhoursstr + "," + $("#from_" + index).val();

                if (tohoursstr == "")
                    tohoursstr = $("#to_" + index).val();
                else
                    tohoursstr = tohoursstr + "," + $("#to_" + index).val();
            });
            $("#availabityfromhours").val(fromhoursstr);
            $("#availabitytohours").val(tohoursstr);

            var servicename = $("#servicename").val();
            var servicetype = $("#selecttype").val();
            var serviceprice = $("#serviceprice").val();
            var fromageyrs = $("#fromageyrs").val();
            var fromagemonths = $("#fromagemonths").val();
            var toageyrs = $("#toageyrs").val();
            var toagemonths = $("#toagemonths").val();
            var fromwt = $("#fromwt").val();
            var towt = $("#towt").val();
            var errormsg = "";
            var hasError = false;

            if ($("#restriction").val() == 1)
            {
                if ($('#agelimit').val() == 'inbetween') {
                        if (($("#fromageyrs").val() == "")) {
                            var hasError = true;
                            errormsg = "From age years is required!";
                        } else if ($("#toageyrs").val() == "") {
                            var hasError = true;
                            errormsg = "To age years is required!";
                        } else if ((fromageyrs.trim().length != 0) &&(fromageyrs > toageyrs)) {
                            errormsg='Age From value must be less than Age To value!';
                            hasError = true;
                        } else if ((fromagemonths > 12) && (toagemonths > 12)){
                            errormsg = 'Month Not Larger Than 12!';
                            hasError = true;
                        }
                    }

                    if ($('#weightlimit').val() == 'inbetween') {
                        if (($("#fromwt").val() == "")) {
                            var hasError = true;
                            errormsg = "From weight is required!";
                        } else if ($("#towt").val() == "") {
                            var hasError = true;
                            errormsg = "To weight is required!";
                        } else if ((fromwt.trim().length != 0) &&(fromwt > towt)){
                            errormsg='Weight From value must be less than Weight To value!';
                            hasError = true;
                        } 
                    }

            } else if (servicename.trim().length == 0)
            {
                errormsg = "Service Name is required!";
                $("#servicename").css('border-color', '#dd4b39');
                hasError = true;
            } else if (servicetype.trim() == 0)
            {
                errormsg = "Service Type is required!";
                $("#selecttype").css('border-color', '#dd4b39');
                hasError = true;
            } else if (serviceprice.trim().length == 0)
            {
                errormsg = "Service Price is required!";
                $("#serviceprice").css('border-color', '#dd4b39');
                hasError = true;
            }

            if (servicename.trim().length != 0)
            {
                $("#servicename").css('border-color', '#d2d6de');
            }
            if (servicetype.trim() != 0)
            {
                $("#selecttype").css('border-color', '#d2d6de');
            }
            if (serviceprice.trim().length != 0)
            {
                $("#serviceprice").css('border-color', '#d2d6de');
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }

            if (hasError == false)
            {
                $("#validerrorform").html('');
                $("#addserviceform").submit();
            } else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                ;
                return false;
            }
        });

        $(document).on('click', '#applyall', function () {

            if ($(this).is(":checked")) {
                applyall = 1;
                defaultfromhoursstr = $("#from_" + startindex).val();
                defaulttohoursstr = $("#to_" + startindex).val();

                $.each($("input[name='availabilitydays[]']:checked"), function () {
                    //values.push($(this).val());
                    var index = weekdayarr.indexOf($(this).val());
                    $("#from_" + index).val(defaultfromhoursstr);
                    $("#to_" + index).val(defaulttohoursstr);
                });
            }
        });
    });


    function displayAgeInputs(sellimit)
    {
        if (sellimit == "atleast")
        {
            $("#fromageyrs").removeAttr('disabled');
            $("#fromagemonths").removeAttr('disabled');
            $("#toageyrs").val('');
            $("#toagemonths").val('');
            $("#toageyrs").attr("disabled", "disabled");
            $("#toagemonths").attr("disabled", "disabled");
        } else if (sellimit == "atmost")
        {
            $("#toageyrs").removeAttr('disabled');
            $("#toagemonths").removeAttr('disabled');
            $("#fromageyrs").val('');
            $("#fromagemonths").val('');
            $("#fromageyrs").attr("disabled", "disabled");
            $("#fromagemonths").attr("disabled", "disabled");
        } else
        {
            $("#fromageyrs").removeAttr('disabled');
            $("#fromagemonths").removeAttr('disabled');
            $("#toageyrs").removeAttr('disabled');
            $("#toagemonths").removeAttr('disabled');
        }
    }

    function displayWeightInputs(sellimit)
    {
        if (sellimit == "atleast")
        {
            $("#fromwt").removeAttr('disabled');
            $("#towt").val('');
            $("#towt").attr("disabled", "disabled");

        } else if (sellimit == "atmost")
        {
            $("#towt").removeAttr('disabled');
            $("#fromwt").val('');
            $("#fromwt").attr("disabled", "disabled");

        } else
        {
            $("#fromwt").removeAttr('disabled');
            $("#towt").removeAttr('disabled');

        }
    }

    function dayselected(index)
    {
        var weekarr = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        var day = weekarr[index];

        if ($("#" + day).length)         // use this if you are using id to check
        {
            // it exists
            $("#" + day).remove('');
            daycount--;
        } else {

            var dayhtml = '<div class="row" id="' + day + '">';
            var dayhtml5 = ' <div class="col-xs-2">&nbsp;</div>';
            var dayhtml1 = ' <div class="col-xs-1">' + day + '</div>';
            var dayhtml2 = '<div class="col-xs-2"> <div class="input-group "><input id="from_' + index + '" name="from_' + index + '" type="text" class="form-control  time"  /></div></div>';
            var dayhtml3 = '<div class="col-xs-2"> <div class="input-group"><input id="to_' + index + '" name="to_' + index + '" type="text" class="form-control  time" /></div></div>';
            var dayhtml6 = "";
            if (daycount == 0)
            {
                dayhtml6 = '<div class="col-xs-1"> <input type="checkbox" id="applyall" name="applyall" value="1" disabled/> &nbsp;Apply to all </div>';
                startindex = index;

            }
            if (daycount == 1)
            {
                $("#applyall").prop("disabled", false);
            }

            var dayhtml4 = '</div>';
            var finalhtml = dayhtml + dayhtml5 + dayhtml1 + dayhtml2 + dayhtml3 + dayhtml6 + dayhtml4;


            $(".availabilitydetail").append(finalhtml);
            daycount++;
            setTimePicker(index);


        }
    }


</script>
<script>
    //form validation step by step   

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var weekdayarr = ['M', 'T', 'W', 'Th', 'F', 'Sa', 'Su'];
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;
        // Start of Appointment add/update form validation //
        var fromhoursstr = "";
        var tohoursstr = "";
        $.each($("input[name='availabilitydays[]']:checked"), function () {
            //values.push($(this).val());
            var index = weekdayarr.indexOf($(this).val());
            if (fromhoursstr == "")
                fromhoursstr = $("#from_" + index).val();
            else
                fromhoursstr = fromhoursstr + "," + $("#from_" + index).val();
            if (tohoursstr == "")
                tohoursstr = $("#to_" + index).val();
            else
                tohoursstr = tohoursstr + "," + $("#to_" + index).val();
        });
        $("#availabityfromhours").val(fromhoursstr);
        $("#availabitytohours").val(tohoursstr);
        var servicename = $("#servicename").val();
        var servicetype = $("#selecttype").val();
        var serviceprice = $("#serviceprice").val();
        
        var fromageyrs = $("#fromageyrs").val();
        var fromagemonths = $("#fromagemonths").val();
        var toageyrs = $("#toageyrs").val();
        var toagemonths = $("#toagemonths").val();
        var fromwt = $("#fromwt").val();
        var towt = $("#towt").val();
        //alert(fromageyrs + fromagemonths + toageyrs + toagemonths + fromwt + towt);
        var errormsg = "";
        var hasError = false;
        var step = $(this).attr('value');
        var validText = /(<([^>]+)>)/ig;

        switch (step) {
            case "step-1":
                if (servicename.trim().length == 0)
                {
                    errormsg = "Service Name is required!";
                    $("#servicename").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                    hasError = true;
                } else if (servicetype.trim() == 0)
                {
                    errormsg = "Service Type is required!";
                    $("#selecttype").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (serviceprice.trim().length == 0)
                {
                    errormsg = "Service Price is required!";
                    $("#serviceprice").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }

                if (servicename.trim().length != 0)
                {
                    $("#servicename").css('border-color', '#d2d6de');
                }
                if (servicetype.trim() != 0)
                {
                    $("#selecttype").css('border-color', '#d2d6de');
                }
                if (serviceprice.trim().length != 0)
                {
                    $("#serviceprice").css('border-color', '#d2d6de');
                }
                break;

            case "step-2":
                
                if ($("#restriction").val() == 1)
                {
                    if ($('#agelimit').val() == 'inbetween') {
                        if (($("#fromageyrs").val() == "")) {
                            var hasError = true;
                            errormsg = "From age years is required!";
                        } else if ($("#toageyrs").val() == "") {
                            var hasError = true;
                            errormsg = "To age years is required!";
                        } else if ((fromageyrs.trim().length != 0) &&(fromageyrs > toageyrs)) {
                            errormsg='Age From value must be less than Age To value!';
                            hasError = true;
                        } else if ((fromagemonths > 12) && (toagemonths > 12)){
                            errormsg = 'Month Not Larger Than 12!';
                            hasError = true;
                        }
                    }

                    if ($('#weightlimit').val() == 'inbetween') {
                        if (($("#fromwt").val() == "")) {
                            var hasError = true;
                            errormsg = "From weight is required!";
                        } else if ($("#towt").val() == "") {
                            var hasError = true;
                            errormsg = "To weight is required!";
                        } else if ((fromwt.trim().length != 0) &&(fromwt > towt)){
                            errormsg='Weight From value must be less than Weight To value!';
                            hasError = true;
                        } 
                    }

                }
                break;

        }

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
            $("#validerrorform").html(errormsg);
        } else {
            $('#validation').hide();
            $("#validerrorform").html('');
        }

        // End of Appointment add/update form validation //

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alert(curInputs[i].val());
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');

</script>