<!-- Content Wrapper. Contains page content -->

<div id="spinner"
     style="display:none;position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url();?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Import from Sikka

        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Sikka</a>
            </li>
            <li class="active"><a>import</a></li>
        </ol>
    </section>
    <br><br>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class=" col-xs-12 ">
                <div class="form-group">
                    <div class="col-sm-5">
                        <center>
                            <label for="">
                                <h4>Update your customers and their pets from Sikka</h4>
                            </label>
                        </center>
                    </div>
                    <div class="col-sm-1">
                        <span id="validerror1" class="text-red"></span>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-warning" name='submit1' data-toggle="modal" data-target="#confirm-update" >Update Customers</button>
                    </div>
                </div>
            </div>
    </section>
</div>

<div class="modal fade" id="confirm-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Update from Sikka
            </div>
            <div class="modal-body">
                <p>Are you sure you want to update your data from Sikka? It will take some time.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning btn-ok" onclick="javascript:fetchdata();">Update</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="confirm-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Sikka Response
            </div>
            <div class="modal-body">
                <p class="sikka-response"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Sikka Migration Progress
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div id="progress-percent" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        0%
                    </div>
                </div>
                <p class="text-center" id="progress-message"></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var breakit;
    var last_progress = 0;

    function fetchdata()
    {

        breakit = false;

        $('#confirm-update').modal('hide');
        $('#confirm-progress').modal({backdrop: 'static', keyboard: false});
        $('#confirm-progress').modal('show');
        var url_ = "<?php echo base_url();?>vendor/sikka/getVendorDetails";
        $.ajax({
            type: "post",
            url:url_,
            cache: false,
            success: function(json){
                try
                {
                    $('#confirm-progress').modal('hide');
                    breakit = true;
                    var obj = jQuery.parseJSON(json);
                    if(obj == 0)
                    {
                        obj = 'You are not a SIKKA customer, Kindly Contact  support@bizbark.com  for more details. Thankyou.';
                        $('.sikka-response').text(obj);
                        $('#confirm-details').modal('show');
                    }
                    else{
                        $('.sikka-response').text(obj);
                        $('#confirm-details').modal('show');
                    }
                }
                catch(e)
                {
                    $('#confirm-progress').modal('hide');
                    breakit = true;
                    var response = "Something went wrong kindly try again.";
                    $('.sikka-response').text(response);
                    $('#confirm-details').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                $('#confirm-progress').modal('hide');
                breakit = true;
                var response = "Server Error";
                $('.sikka-response').text(response);
                $('#confirm-details').modal('show');
            }
        });

        getProgress(true);
    }

    function getProgress(isfirsttime){

        var  url_ =  baseurl+'vendor/sikka/getProgress';
        $.ajax({
            type: "post",
            url:url_,
            data:{isfirst:isfirsttime},
            success: function(json){
                try{

                    error = json;

                    var obj = jQuery.parseJSON(json);

                    if(obj.percent != null && obj.percent>last_progress)
                    {
                        last_progress = obj.percent;
                        console.log(obj);

                        $('#progress-percent').html(obj.percent+'%');
                        $('#progress-percent').attr('aria-valuenow', obj.percent).css('width',obj.percent+'%');
                        $('#progress-message').text(obj.message);

                        if(obj.percent==100)
                        {
                            breakit = true;
                            $('#confirm-progress').modal('hide');
                        }


                    }
                    if(breakit == false)
                    {
                        getProgress(false);
                    }


                }
                catch(e) {


                    var response = "Something went wrong kindly try again.";
                    console.log(error);
                    getProgress(false);
                    // $('.sikka-response').text(response);
                    //  $('#confirm-details').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log("Get Progress Error");
                console.log(xhr);
                getProgress(false);
            }
        });
    }



</script>


