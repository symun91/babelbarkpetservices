<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<style>
    .box{
        border-top: 0 !important;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($custdetail['customerid'] == 0)
                echo "Add Generic Customer";
            else
                echo "Customer Detail";
            ?>
        </h1>
        <?php if ($custdetail['customerid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customers</a></li>
                <li class="active">Add Customers</a></li>
            </ol>
        <?php } ?>
    </section>
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <form name="addcustomerform" id="addcustomerform"  action="<?php echo base_url(); ?>vendor/customer/updateCustomer" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="customerid" id="customerid" value="<?php echo $custdetail['customerid']; ?>"/>
            <input type="hidden" name="petid" id="petid" value="<?php echo $petdetail['petid']; ?>"/>
            <input type="hidden" name="vetid" id="vetid" value="<?php echo $vetdetail['veterinarianid']; ?>"/>
            <input type="hidden" name="medicinesadded" id="medicinesadded" value="<?php echo $petmedication; ?>"/>
            <input type="hidden" name="vaccinesadded" id="vaccinesadded" value="<?php echo $petvaccination; ?>"/>
            <div class="box">
                <center><h1></h1></center>
                <div class="box-tools ">
                    <div class="row">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-3 pull-right">
                            <div class="form-group">
                                <button class="btn bg-orange" type="button" id="sendInvite" >Invite Customer to Babelbark</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="stepwizard col-md-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn bg-orange btn-circle">1</a>
                                    <p>Customer Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                    <p>Pet Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                    <p>Veterinarian Details</p>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 start ##########################################-->
                        <div class="row setup-content" id="step-1">
                            <div class="col-lg-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Customer First Name<font color="red">*</font></label>
                                                    <input id="customerfname" name="customerfname" class="form-control" type="text" placeholder="Customer First  Name" value="<?php echo set_value('customerfname', $custdetail['firstname']); ?>" maxlength="200">
                                                </div>  
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Customer Last Name<font color="red">*</font></label>
                                                    <input id="customerlname" name="customerlname" class="form-control" type="text" placeholder="Customer Last  Name" value="<?php echo set_value('customerlname', $custdetail['lastname']); ?>" maxlength="200">
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="targetpopulation">Email</label>
                                                    <input id="customeremail" class="form-control" type="email" placeholder="Email" name="customeremail" maxlength="200" value="<?php echo set_value('customeremail', $custdetail['email']); ?>">
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Phone Number</label>
                                                    <?php
                                                    if ($custdetail['phoneno'] == 0) {
                                                        $custdetail['phoneno'] = "";
                                                    }
                                                    ?>
                                                    <input class="form-control " placeholder="Phone Number" id="phoneno" name="phoneno" type="text" value="<?php echo set_value('phoneno', $custdetail['phoneno']); ?>">
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Address</label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="country" id="country">
                                                                <option value="">Select Country</option>
                                                                <option value="Canada" <?php echo set_select('country', 'Canada', ( $custdetail['country'] == 'Canada' ? TRUE : FALSE)); ?>>Canada</option>
                                                                <option value="United States" <?php echo set_select('country', 'United States', ( $custdetail['country'] == 'United States' || $custdetail['country'] == 'USA' ? TRUE : FALSE)); ?>>United States</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input id="city" class="form-control" type="text" placeholder="City" name="city" maxlength="45" value="<?php echo set_value('city', $custdetail['city']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">	                                               
                                                        <div class="col-sm-6">
                                                            <input id="address" class="form-control" type="text" placeholder="Address" name="address" maxlength="45" value="<?php echo set_value('address', $custdetail['address']); ?>">
                                                        </div>                                                       
                                                        <div class="col-sm-6"> 
                                                            <input id="zipcode" class="form-control" type="text" placeholder="Zipcode" name="zipcode" maxlength="15" value="<?php echo set_value('zipcode', $custdetail['zipcode']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">	
                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="state" id="state"></select>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Notes</label>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control countable" placeholder="Notes" name="notes" data-length="1500"><?php echo $custdetail['notes']; ?></textarea>
                                                        </div>
                                                        <div class="col-xs-6"></div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <div class="box-body">
                                    <p style="border: 1px solid #C2C2C2"></p>
                                    <div class="col-lg-5 col-xs-5">
                                        <table class="table" id="fileUploadTable">
                                            <thead style="background-color:#EEEEEE;">
                                            <th><label>Select file<font color="red">*</font></label></th>
                                            <th><label>Upload Date</label></th>
                                            <th><td style="display: none"><a onclick="addMoreRowsToAttach(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                            </thead>
                                            <tbody id="addedRowsToAttach">

                                            </tbody>
                                        </table>
                                        <div class="alert alert-success">
                                            <strong>Please save this customer's details before uploading a file!</strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="col-lg-6 col-xs-6"></div>
                                        <div class="col-lg-6 col-xs-6">
                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 end ##########################################-->
                        <!--############################# STEP 2 start ##########################################-->
                        <div class="row setup-content" id="step-2">
                            <div class="col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Pet Name<font color="red">*</font></label>
                                                    <input id="petname" name="petname" class="form-control" type="text" placeholder="Pet name" maxlength="100" value="<?php echo set_value('petname', $petdetail['name']); ?>">
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Pet Photo</label>
                                                    <input class="form-control" type="file" id="profileimage" name="profileimage" onchange="PreviewImage();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <a href="#" class="thumbnail" style="width: 142px; height: 145px;">
                                                            <img src="<?php echo empty_image('', ''); ?>" id="preview" style="width: 140px; height: 135px;"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                                if ($custdetail['customerid'] != 0) {
                                                    if ($petdetail['proilefpicture'] != null && $petdetail['proilefpicture'] != "") {
                                                        ?>
                                                        <div class="col-lg-3">
                                                            <div class="input-group">
                                                                <label for="">Current Photo</label> 
                                                                <a href="#" class="thumbnail" style="width: 142px; height: 145px;">
                                                                    <img src="<?php echo base_url() . $petdetail['proilefpicture']; ?>?<?= Date('U') ?>" style="width: 140px; height: 135px;"/>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Is Pet Microchipped</label>
                                            <input type="checkbox" id="microchipped" name="microchipped" value="1" <?php echo ($petdetail['microchipped'] == '1' ? 'checked' : null); ?>/>
                                        </div>
                                        <div class="chipid">
                                            <div class="form-group">
                                                <label>Pet Microchip ID</label>
                                                <input type="text" class="form-control" placeholder="Pet Microchip ID" name="microchipid" value="<?php echo set_value('microchipid', $petdetail['microchipid']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Number</label>
                                            <input id="licensetag" type="text" class="form-control" placeholder="Pet Govt License Tag Number" name="govtlicensetagno" value="<?php echo set_value('govtlicensetagno', $petdetail['govtlicensetagno']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Current Weight (<?php if($unittype == 'Metric'){echo 'kgs';}else{echo 'lbs';} ?>)</label>
                                            <input type="text" class="form-control allownumericwithdecimal" placeholder="Pet Current Weight" name="petcurrentweight" value="<?php echo set_value('petcurrentweight', $petdetail['currentweight']); ?>">	 
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Target Weight (<?php if($unittype == 'Metric'){echo 'kgs';}else{echo 'lbs';} ?>)</label>
                                            <input type="text" class="form-control allownumericwithdecimal" placeholder="Pet Target Weight" name="pettargetweight" value="<?php echo set_value('pettargetweight', $petdetail['targetweight']); ?>">	 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Issue</label>
                                            <input  id="licensetagissue" type="text" class="form-control" placeholder="Pet Govt License Tag Issue" name="govtlicensetagissuer" value="<?php echo set_value('govtlicensetagissuer', $petdetail['govtlicensetagissuer']); ?>">
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Is Your Pet Adopted</label>
                                            <input type="checkbox" name="isadopted" value="1" <?php echo ($petdetail['isadopted'] == '1' ? 'checked' : null); ?>/>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="">Pet BirthDay</label>
                                            <?php
                                            $sdate = $petdetail['birthdate'];
                                            if($sdate == ''){
                                                $birthdate = "";
                                            }else{
                                                $birthdate = date('F d, Y', strtotime($sdate));
                                            }?>
                                            <input class="form-control datepicker" placeholder="Pet Birth / Adoption Date" name="birthdate" id="birthdate" data-date-format="MM dd, yyyy" value="<?php echo set_value('birthdate', $birthdate); ?>">
                                        </div>
                                        Unsure of Pet's Birthdate
                                        <input type="checkbox" name="bdayunsure" id="bdayunsure" value="1" <?php echo ($petdetail['birthdayunsure'] == '1' ? 'checked' : null); ?>/>
                                        <div class="form-group">
                                            <div class="adoptiondetail">
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-xs-2">
                                                        <?php
                                                        $approxage = $petdetail['approximateage'];
                                                        $approxageyears = "";
                                                        $approxagemonths = "";
                                                        if ($approxage != "" && $approxage != "0") {
                                                            $approxtemp = explode("years", $approxage);
                                                            $approxageyears = $approxtemp[0];
                                                            if ($approxtemp[1] != "") {
                                                                $approxtemp1 = explode("months", $approxtemp[1]);
                                                                $approxagemonths = $approxtemp1[0];
                                                            }
                                                        }
                                                        ?>
                                                        Approximate Age
                                                    </div>
                                                    <div class="col-xs-1"> Years</div>
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control allownumericwithoutdecimal" name="approxageyrs" value="<?php echo $approxageyears; ?>">
                                                    </div>
                                                    <div class="col-xs-1"> Months</div>
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control allownumericwithoutdecimal" name="approxagemonths" value="<?php echo $approxagemonths; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <label for="">Calculated Pet Age</label> <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
                                            <input type="text" class="form-control allownumericwithoutdecimal" name="petage" id="petage" value="" readonly>
                                        </div>
                                        <form id="ageunit" name="ageunit">
                                            Years  <input type="radio" name="petageunit"  value="years"/>
                                            &nbsp;&nbsp;Weeks <input type="radio"  name="petageunit"  value="weeks"/>
                                            &nbsp;&nbsp;Days  <input type="radio"  name="petageunit"  value="days" />
                                        </form>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <label for="">Gender </label>
                                            M <input type="radio" name="genderchk" value="M" <?php echo set_value('genderchk', $petdetail['gender']) == "M" ? "checked" : ""; ?> />
                                            F <input type="radio" name="genderchk" value="F" <?php echo set_value('genderchk', $petdetail['gender']) == "F" ? "checked" : ""; ?> />
                                        </div> 
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="row">	
                                                            <label >Pet Medication : <a href="#"  data-toggle="modal" data-target="#medication"><u>Add Pet Medication</u></a></label>
                                                            <span id="medicationaddedlabel"></span>
                                                            <div class="box-body table-responsive" style="padding: 10px 15px;">
                                                                <table id="medicinetable1" class="table table-bordered table-striped ">
                                                                
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;"></div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Pet Medication History</label>
                                            <textarea id="medicalhistory" class="form-control countable"  placeholder="Pet Medication History" name="medicalhistory" data-length="1500"><?php echo set_value('medicalhistory', $petdetail['medicalhistory']); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-xs-12">
                                        <div class="form-group">	
                                            <label >Pet Vaccination : <a href="#"  data-toggle="modal" data-target="#vaccination"><u>Add Pet Vaccination</u></a></label>
                                            <span id="vaccinationaddedlabel">   </span>
                                            <div class="box-body table-responsive">
                                                <table id="vaccinationtable1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <!-- <tr>
                                                                <th>Vaccination</th>
                                                                <th>Due Date</th>
                                                                <th>Delete</th>
                                                    </tr> -->
                                                    </thead></tbody> 
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 ">
                                        <div class="form-group">
                                            <label for="targetpopulation">Pet Breed</label>
                                            <div class="row">	
                                                <div class="col-sm-6">
                                                    <select name="primarybreed" class="form-control">
                                                        <option value="" >Select Primary Breed</option>
                                                        <?php for ($i = 0; $i < count($breeds); $i++) { ?>
                                                            <option value="<?php echo $breeds[$i]['breedid']; ?>" <?php echo set_select('primarybreed', $breeds[$i]['breedid'], ( $petdetail['primarybreed'] == $breeds[$i]['breedid'] ? TRUE : FALSE)); ?>><?php echo $breeds[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">  
                                                    <select name="secondarybreed" class="form-control">
                                                        <option value="" >Select Secondary Breed</option>
                                                        <?php for ($i = 0; $i < count($breeds); $i++) { ?>
                                                            <option value="<?php echo $breeds[$i]['breedid']; ?>" <?php echo set_select('secondarybreed', $breeds[$i]['breedid'], ( $petdetail['secondarybreed'] == $breeds[$i]['breedid'] ? TRUE : FALSE)); ?>><?php echo $breeds[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-xs-12 ">
                                        <div class="form-group">
                                            <label for="targetpopulation">Pet Dietary Notes</label>
                                            <div class="row">	
                                                <div class="col-sm-6">
                                                    <select name="primaryfood" class="form-control">
                                                        <option value="" >Select Primary Food</option>
                                                        <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                            <option value="<?php echo $feedbrands[$i]['feedbrandid']; ?>" <?php echo set_select('primaryfood', $feedbrands[$i]['feedbrandid'], ( $petdetail['primaryfood'] == $feedbrands[$i]['feedbrandid'] ? TRUE : FALSE)); ?>><?php echo $feedbrands[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                        <?php
                                                        if ($petdetail['primaryfood'] == $feedbrands[$i]['feedbrandid']) {
                                                            ?>
                                                            <a href="<?php echo $feedbrands[$i]['link']; ?>" target="_blank"><u> Review food brand nutritional quality </u></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-6"> 
                                                    <select name="secondaryfood" class="form-control">
                                                        <option value="" >Select Secondary Food</option>
                                                        <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                            <option value="<?php echo $feedbrands[$i]['feedbrandid']; ?>" <?php echo set_select('secondaryfood', $feedbrands[$i]['feedbrandid'], ( $petdetail['secondaryfood'] == $feedbrands[$i]['feedbrandid'] ? TRUE : FALSE)); ?>><?php echo $feedbrands[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                        <?php
                                                        if ($petdetail['secondaryfood'] == $feedbrands[$i]['feedbrandid']) {
                                                            ?>
                                                            <a href="<?php echo $feedbrands[$i]['link']; ?>" target="_blank"><u> Review food brand nutritional quality </u></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">	
                                                <div class="col-sm-6"> <div class="input-group"><input id="feedamount" class="form-control allownumericwithoutdecimal" type="number" placeholder="Amount of feed" name="feedamount" value="<?php echo set_value('feedamount', $petdetail['amountoffeed']); ?>"><div class="input-group-addon">(Kilos)</div></div></div>
                                                <div class="col-sm-6"> <div class="input-group"><input id="feedfrequency" class="form-control allownumericwithoutdecimal" type="number" placeholder="Frequency of feed" name="feedfrequency" value="<?php echo set_value('feedfrequency', $petdetail['frequencyoffeed']); ?>"><div class="input-group-addon">(per day)</div></div></div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12">
                                    <div class="box-body">
                                        <p style="border: 1px solid #C2C2C2"></p>
                                        <div class="col-lg-5 col-xs-5">
                                            <table class="table" id="fileUploadTable">
                                                <thead style="background-color:#EEEEEE;">
                                                <th><label>Select file<font color="red">*</font></label></th>
                                                <th><label>Upload Date</label></th>
                                                <th><td style="display: none"><a onclick="addMoreRowsToAttachEdit(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                                </thead>
                                                <tbody id="addedRowsToAttachEdit">

                                                </tbody>
                                            </table>
                                            <div class="alert alert-success">
                                                <strong>Please save this pet's details before uploading a file!</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn pull-right" type="button" value="step-2">Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 2 end ##########################################-->
                        <!--############################# STEP 3 start ##########################################-->
                        <div class="row setup-content" id="step-3">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Veterinarian Details</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-5">
                                                                <a href="javascript:showLocalVets();" ><u>Search for local veterinarians</u></a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <h5 id='loading' >loading, Please wait..</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Veterinarian Name</label>
                                                        <input id="vetname" class="form-control allowcharacterwithouthtmltags"  placeholder="Veterinarian Name" name="vetname" maxlength="200"  value="<?php echo set_value('vetname', $vetdetail['name']); ?>">
                                                        <label class="control-label" for="inputError"></label>	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Phone Number</label>
                                                        <input id="vetphone" class="form-control allownumericwithoutdecimal" type="text" placeholder="Phone Number" name="vetphone" value="<?php echo set_value('vetphone', $vetdetail['phoneno']); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Emergency Number</label>
                                                        <input id="vetemergphone" class="form-control allownumericwithoutdecimal" type="text" placeholder="Emergency Phone Number" name="vetemergphone" value="<?php echo set_value('vetemergphone', $vetdetail['emergencyno']); ?>">	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Point of Contact</label>
                                                        <input id="vetcontactPerson" class="form-control" type="text" placeholder="Name of person to contact" name="vetcontactPerson" maxlength="200" value="<?php echo set_value('vetcontactPerson', $vetdetail['contactperson']); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Address</label>
                                                        <input id="vetaddress" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="Address" name="vetaddress" maxlength="45" value="<?php echo set_value('vetaddress', $vetdetail['address']); ?>">
                                                        <div class="row" style="margin-top: 10px;">	
                                                            <div class="col-sm-6"> <input id="vetcity" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="City" name="vetcity" maxlength="45" value="<?php echo set_value('vetcity', $vetdetail['city']); ?>"></div>
                                                            <div class="col-sm-6"> <input id="vetstate" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="State" name="vetstate" maxlength="45" value="<?php echo set_value('vetstate', $vetdetail['state']); ?>"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;">	
                                                            <div class="col-sm-6"> <input id="vetzipcode" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="Zipcode" name="vetzipcode" maxlength="15" value="<?php echo set_value('vetzipcode', $vetdetail['zipcode']); ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Operating Hours</label>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-xs-1"> From </div>
                                                            <div class="col-xs-5">
                                                                <input id="fromhoursofoperation" type="text allowcharacterwithouthtmltags" name="fromhoursofoperation" value="<?php echo set_value('fromhoursofoperation', $vetdetail['fromhoursofoperation']); ?>" class="form-control input-small">
                                                            </div>
                                                            <div class="col-xs-1"> To </div>
                                                            <div class="col-xs-5">
                                                                <input id="tohoursofoperation" type="text allowcharacterwithouthtmltags" name="tohoursofoperation" value="<?php echo set_value('tohoursofoperation', $vetdetail['tohoursofoperation']); ?>" class="form-control input-small">
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-xs-12">
                                                                <?php
                                                                $checkeddays_arr = array();
                                                                if ($vetdetail['availabilitydays'] != "") {
                                                                    $checkeddays_arr = explode(",", $vetdetail['availabilitydays']);
                                                                }
                                                                for ($i = 0; $i < count($weekdays); $i++) {
                                                                    ?>
                                                                    <?php echo $weekdays[$i]; ?> &nbsp;<input type="checkbox" name="availabilitydays[]" value="<?php echo $weekdays[$i]; ?>"  <?php echo (in_array($weekdays[$i], $checkeddays_arr) ? 'checked' : null); ?>/>
                                                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                        <?php if ($custdetail['customerid'] == 0) { ?>
                                            <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Save">
                                        <?php } else { ?>
                                            <div style="margin-bottom:15px;">
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Update">
                                            </div>
                                            <div class="col-lg-12 col-xs-12" >
                                                <div class="row">
                                                    <div style="float:right;">
                                                        <?php
                                                        if (intval($precustomer) > 0 && $preusertype != "N/A")
                                                            echo anchor('vendor/customer/viewDetails/' . $precustomer . '/' . rawurlencode($preusertype), 'Previous', 'class="btn btn-primary"');
                                                        ?>
                                                        <?php
                                                        if (intval($nextcustomer) > 0 && $nextusertype != "N/A")
                                                            echo anchor('vendor/customer/viewDetails/' . $nextcustomer . '/' . rawurlencode($nextusertype), 'Next', 'class="btn btn-primary"');
                                                    }
                                                    #echo anchor('vendor/customer/listofgenericcustomer', 'Back', 'class="btn  btn-primary"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                         
                        <!--############################# STEP 3 end ##########################################-->
                    </div>
                </div>
            </div>
        </form>  		
    </section>
</div>
<div id="medication" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Medication Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Pet Medication</h4>
            </div>
            <section class="content">
                <div class="row">
                    <div class=" col-xs-12 ">
                        <form role="form">
                            <div class="form-group">
                                <input class="form-control" id="searchinput" type="text" placeholder="Search..." />
                            </div>
                            <div id="srchmedicine"  class="list-group" >
                                <?php for ($i = 0; $i < count($medicines); $i++) { ?>
                                    <div class="list-group-item" >
                                        <a href="javascript:getMedicine(<?php echo $medicines[$i]['medicineid']; ?>,'<?php echo $medicines[$i]['name']; ?>','<?php echo $medicines[$i]['url']; ?>')"><span id="<?php echo $medicines[$i]['medicineid']; ?>"><?php echo $medicines[$i]['name']; ?></span></a>
                                        <a href="<?php echo $medicines[$i]['url']; ?>" target="_blank" style="float:right">  <img src="<?php echo base_url(); ?>assets/images/webicon.png" width="25"></a>
                                    </div>
                                <?php } ?>

                            </div> 
                        </form>
                        <div class="form-group">

                            <input type="hidden" id="medicine"/>				
                            <input type="hidden" id="medicinename"/>
                            <input type="hidden" id="medicinelink"/>				
                            <div class="row">
                                <div class="col-sm-3">
                                    <input  name="dosage" id="dosage" class="form-control allownumericwithoutdecimal" type="text" placeholder="Dose" maxlength="3" ></div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="frequencytimes" id="frequencytimes"  onchange="javscript:addReminders();">
                                        <option value=""> Times</option>
                                        <?php for ($i = 1; $i < 11; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>

                                    </select>

                                </div>
                                <div class="col-sm-1">
                                    Per
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="frequency" id="frequency" onchange="javscript:addReminders();">
                                        <option value=""> Regularity</option>
                                        <?php for ($i = 0; $i < count($regularity); $i++) { ?>
                                            <option value="<?php echo $regularity[$i]; ?>"><?php echo $regularity[$i]; ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <br/>



                            <div id="reminderbox" class="box-body table-responsive" style="padding: 10px 15px;">
                                <table id="remindertable" class="table table-bordered table-striped">


                                </table>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-sm-3" > 
                                    <button class="btn btn-warning" name='add'  onclick="javascript:addMedication();">Add</button>
                                    <button class="btn btn-warning" name='done'  onclick="javascript:addMedicationDone();">Done</button></div>

                                <div class="col-sm-12"> <label class="text-red" id="validerror"></label></div>
                            </div>					

                        </div> 
                    </div>

                    <div class="box-body table-responsive" style="padding: 10px 15px;">
                        <table id="medicinetable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Medication</th>
                                    <th>Frequency</th>
                                    <th>Link</th>
                                    <th>Reminder</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody> 
                        </table>
                    </div>


            </section> 
        </div>
    </div>
</div>

<!-- vaccination -->
<div id="vaccination" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Pet Vaccinations</h4>
            </div>
            <section class="content">
                <div class="row">

                    <div class=" col-xs-12 ">
                        <form role="form">
                            <div class="form-group">
                                <input class="form-control" id="searchinput1" type="text" placeholder="Search..." />
                            </div>
                            <div id="srchvaccination"  class="list-group" >
                                <?php for ($i = 0; $i < count($vaccinations); $i++) { ?>
                                    <div class="list-group-item" >
                                        <a href="javascript:getVaccination(<?php echo $vaccinations[$i]['vaccineid']; ?>,'<?php echo $vaccinations[$i]['name']; ?>')"><span id="<?php echo $vaccinations[$i]['vaccineid']; ?>"><?php echo $vaccinations[$i]['name']; ?></span></a>
                                    </div>
                                <?php } ?>

                            </div> 
                        </form>
                        <div class="form-group">

                            <label for="">Due Date</label>
                            <div class="row" style="margin-top: 10px;">	
                                <div class="col-sm-5">
                                    <input class="form-control datepicker" placeholder="Enter Due Date" name="duedate" id="duedate" data-date-format="MM dd, yyyy">
                                </div>
                            </div>

                            <input type="hidden" id="vaccine"/>				
                            <input type="hidden" id="vaccinename"/>			
                            <br/>
                            <div class="row">
                                <div class="col-sm-3" > 
                                    <button class="btn btn-warning" name='add'  onclick="javascript:addVaccination();">Add</button>
                                    <button class="btn btn-warning" name='done'  onclick="javascript:addVaccinationDone();">Done</button></div>

                                <div class="col-sm-12"> <label class="text-red" id="validerror2"></label></div>
                            </div>					

                        </div> 
                    </div>
                    <div class="box-body table-responsive" style="padding: 10px 15px;">
                        <table id="vaccinationtable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Vaccination</th>
                                    <th>Due Date</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody> 
                        </table>
                    </div>


            </section> 
        </div>
    </div>
</div>


<!--  Vet lookup popup -->

<div id="vetlookup" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Local veterinarians</h4>
            </div>

            <section class="content">

                <!-- <div class="box">
              <div class="row">
              
              <div class="col-lg-9 col-xs-12 "> -->

                <p>
                <div id="vetlist"></div>
                </p>
                <!-- </div>
                </div>
                
          </div> -->
            </section>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
            </div>
        </div>

    </div>
</div>     
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
<!-- timepicker js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js" type="text/javascript"></script>
<style>
    .list-group{
        overflow:scroll;  
        height:250px;
    }
    .clsDatePicker {
        z-index:1200 !important;
    }
</style>
<script>

                                        $(window).load(function () {
                                            $("#validation").hide();
                                        });

                                        function PreviewImage() {
                                            var oFReader = new FileReader();
                                            oFReader.readAsDataURL(document.getElementById("profileimage").files[0]);

                                            oFReader.onload = function (oFREvent) {
                                                document.getElementById("preview").src = oFREvent.target.result;
                                            };
                                        }
                                        ;

                                        var medcnt = 0;
                                        var vaccnt = 0;

                                        var medicines = [];
                                        var vaccinations = [];
                                        
                                        var selected_medicine = [];
                                        var selected_vaccination = [];

                                        var medicationadded = {
                                            'medication': []
                                        };

                                        var vaccinationadded = {
                                            'vaccination': []
                                        };

                                        $(document).ready(function () {


                                            function daydiff(first, second) {
                                                return Math.round((second - first) / (1000 * 60 * 60 * 24));
                                            }

                                            $('input:radio[name="petageunit"]').click(function () {
                                                var petdob = new Date($("#birthdate").val());
                                                if (($(this).is(':checked')) && ($(this).val() == 'years') && (petdob != "Invalid Date"))
                                                {
                                                    console.log(petdob);
                                                    var today = new Date();
                                                    var age = today.getFullYear() - petdob.getFullYear();
                                                    $("#petage").val(age);
                                                } else if (($(this).is(':checked')) && ($(this).val() == 'weeks') && (petdob != "Invalid Date"))
                                                {

                                                    var today = new Date();
                                                    var age = daydiff(petdob, today);
                                                    age = Math.round((age / 7).toFixed(0));
                                                    $("#petage").val(age);
                                                } else if (($(this).is(':checked')) && ($(this).val() == 'days') && (petdob != "Invalid Date"))
                                                {

                                                    var today = new Date();
                                                    var age = daydiff(petdob, today);
                                                    $("#petage").val(age);
                                                } else
                                                {
                                                    $("#petage").val("Pet Birthdate is required.");
                                                }

                                            });

                                            $('#sendInvite').click(function () {

                                                var email = $('#customeremail').val();

                                                if (email)
                                                {
                                                    autoInvite(email);
                                                } else {

                                                    alert('Kindly enter an Email address.');
                                                }

                                            });

                                            $("#loading").hide();
                                            var petmedicationadded = '<?php echo $petmedication; ?>';
                                            var petvaccinationadded = '<?php echo $petvaccination ?>';
                                            /// alert(petmedicationadded);

                                            $('#srchmedicine').btsListFilter('#searchinput', {itemChild: 'span'});
                                            $('#srchvaccination').btsListFilter('#searchinput1', {itemChild: 'span'});

                                            $('#birthdate').datepicker({
                                                endDate: '+0d'
                                            });

                                            $('#duedate').datepicker({
                                                startDate: new Date()
                                            });

                                            $('.datepicker').datepicker({
                                                startDate: 'January 01, 1950',
                                                format: "MM dd, yyyy"
                                            });
                                            
                                            $('.clsDatePicker').datepicker({
                                                startDate: new Date(),
                                                format: "MM dd, yyyy"
                                            });

                                            var selCountry1 = "<?php echo $custdetail['country']; ?>";
                                            if (selCountry1 != "")
                                            {
                                                populateStates(selCountry1);
                                            }

                                            $("#country").on('change', function () {
                                                $('#state option').remove();
                                                $("#state").append('<option value="">Select State</option>');
                                                var selCountry = this.value;
                                                populateStates(selCountry);


                                            });


                                            $('[data-toggle="tooltip"]').tooltip(

                                                    );


                                            // $("#medtime").timepicker({showMeridian: false });
                                            $("#tohoursofoperation").timepicker({showMeridian: false});
                                            $("#fromhoursofoperation").timepicker({showMeridian: false});

<?php if ($petdetail['birthdayunsure'] == 0) { ?>
                                                $('.adoptiondetail').hide();
<?php } else { ?>
                                                $('.adoptiondetail').show();
<?php } ?>

                                            $('#bdayunsure').change(function () {
                                                if ($(this).is(":checked")) {
                                                    $('.adoptiondetail').show();
                                                } else
                                                {
                                                    $('.adoptiondetail').hide();
                                                }

                                            });

<?php if ($petdetail['microchipped'] == 0) { ?>
                                                $('.chipid').hide();
<?php } else { ?>
                                                $('.chipid').show();
<?php } ?>

                                            $('#microchipped').change(function () {
                                                if ($(this).is(":checked")) {
                                                    $('.chipid').show();
                                                } else
                                                {
                                                    $('.chipid').hide();
                                                }

                                            });

                                            if (petmedicationadded != "")
                                            {

                                                var jsonData = JSON.parse(petmedicationadded);

                                                console.log(jsonData);

                                                for (var i = 0; i < jsonData.medication.length; i++) {


                                                    var medobj = jsonData.medication[i];
                                                    var medid = medobj.medicineid;
                                                    var medlink = medobj.url;
                                                    var freq = medobj.frequency;
                                                    var dosg = medobj.dosage;
                                                    var name = medobj.name;
                                                    //var medtime=medobj.medicationtime;
                                                    var medtime = medobj.medicationtimings;
                                                    var freqtimes = medobj.frequencytimes;
                                                    medicationadded.medication.push({'medicineid': medid, 'frequency': freq, 'dosage': dosg, 'medicationtime': medtime, 'medicationtimings': medtime, 'frequencytimes': freqtimes});


                                                    medcnt++;
                                                    if (medcnt == 1)
                                                    {
                                                        $("#medicinetable1").append('<tr><th>Medicine</th><th>Frequency</th><th>Link</th><th>Reminder</th><th>Delete</th></tr>');
                                                    }

                                                    var reminders = medtime.split(",");

                                                    var remindersstr = "";

                                                    for (var j = 0; j < reminders.length; j++)
                                                    {

                                                        reminders1 = reminders[j].split(" ");
                                                        var H = +reminders1[1].substr(0, 2);
                                                        var h = H % 12 || 12;
                                                        var ampm = H < 12 ? "am" : "pm";
                                                        reminders1[1] = h + reminders1[1].substr(2, 3) + ampm;
                                                        remindersstr = remindersstr + reminders1[0] + " " + reminders1[1] + "<br>";
                                                    }


                                                    var dispfreq = "";
                                                    if (freq == "Day" || freq == "Week" || freq == "Month")
                                                        dispfreq = dosg + " Tablets  " + freqtimes + " times " + freq;


                                                    $("#medicinetable").append('<tr id="' + medid + '"><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + medlink + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td> <td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');
                                                    $("#medicinetable1").append('<tr id="' + medid + '"><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + medlink + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td> <td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');

                                                }
                                                //$('#medicationaddedlabel').html("<lable><b>"+medcnt+" medications added"+"</b></label>");          
                                                $('#medicinesadded').val(JSON.stringify(medicationadded));
                                            }

                                            if (petvaccinationadded != "")
                                            {

                                                var jsonData = JSON.parse(petvaccinationadded);

                                                console.log(jsonData);

                                                for (var i = 0; i < jsonData.vaccination.length; i++) {


                                                    var medobj = jsonData.vaccination[i];
                                                    var vacid = medobj.vaccineid;
                                                    //  var info=medobj.info;
                                                    var name = medobj.name;
                                                    //var medtime=medobj.medicationtime;
                                                    var vacdate = medobj.duedate;
                                                    var curr = vacdate.split('-');
                                                    vacdate = curr[1] +'-'+ curr[2] +'-'+ curr[0];
                                                    vaccinationadded.vaccination.push({'vaccineid': vacid, 'duedate': vacdate});

                                                    vaccnt++;
                                                    if (vaccnt == 1)
                                                    {
                                                        $("#vaccinationtable1").append('<tr><th>Vaccination</th><th>Due Date</th><th>Delete</th></tr>');
                                                    }
                                                    $("#vaccinationtable").append('<tr id="' + vacid + '"><td>' + name + '</td><td>' + vacdate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');
                                                    $("#vaccinationtable1").append('<tr id="' + vacid + '"><td>' + name + '</td><td>' + vacdate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');

                                                }
                                                //$('#vaccinationaddedlabel').html("<lable><b>"+vaccnt+" vaccinations added"+"</b></label>");          
                                                $('#vaccinesadded').val(JSON.stringify(vaccinationadded));
                                            }

                                            $("#update").click(function () {
                                                var fname = $("#customerfname").val();
                                                var lname = $("#customerlname").val();
                                                var email = $("#customeremail").val();
                                                var petname = $("#petname").val();
                                                var vetname = $("#vetname").val();
                                                var zipcode = $("#zipcode").val();
                                                var country = $("#country").val();
                                                var phoneno = $('#phoneno').val();
                                                var address = $("#address").val();
                                                var city = $("#city").val();
                                                var licensetag = $("#licensetag").val();
                                                var licensetagissue = $("#licensetagissue").val();
                                                var validText = /(<([^>]+)>)/ig;

                                                var hasError = false;
                                                var errormsg = "";
                                                if (fname.trim().length == 0)
                                                {
                                                    errormsg = 'Customer First Name is required!';
                                                    $("#customerfname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(fname))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#customerfname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (lname.trim().length == 0)
                                                {
                                                    errormsg = 'Customer Last Name is required!';
                                                    $("#customerlname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(lname))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#customerlname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (email != '' && !validateEmail(email)) {
                                                    errormsg = 'Enter valid Email ID';
                                                    $("#customeremail").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (phoneno != '')
                                                {
                                                    var phone = $('#phoneno').val();
                                                    var phoneNum = phone.replace(/[^\d]/g, '');
                                                    if (phoneNum.length > 12 || phoneNum.length < 6)
                                                    {
                                                        errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                                                        $("#phoneno").css('border-color', '#dd4b39');
                                                        hasError = true;
                                                    }
                                                } else if (validText.test(address))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#address").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(city))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#city").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (zipcode != '' && !validateZip(zipcode, country)) {
                                                    errormsg = 'Enter valid Zipcode for customer';
                                                    $("#zipcode").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (petname.trim().length == 0)
                                                {
                                                    errormsg = 'Pet Name is required!';
                                                    $("#petname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(petname))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#petname").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(licensetag))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#licensetag").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                } else if (validText.test(licensetagissue))
                                                {
                                                    errormsg = 'No html tags are allowed!';
                                                    $("#licensetagissue").css('border-color', '#dd4b39');
                                                    hasError = true;
                                                }

                                                if (fname.trim().length != 0 && !(validText.test(fname)))
                                                {
                                                    $("#customerfname").css('border-color', '#d2d6de');
                                                }
                                                if (lname.trim().length != 0 && !(validText.test(lname)))
                                                {
                                                    $("#customerlname").css('border-color', '#d2d6de');
                                                }
                                                if (address.trim().length == 0 || !(validText.test(address)))
                                                {
                                                    $("#address").css('border-color', '#d2d6de');
                                                }
                                                if (city.trim().length == 0 || !(validText.test(city)))
                                                {
                                                    $("#city").css('border-color', '#d2d6de');
                                                }
                                                if (petname.trim().length != 0 && !(validText.test(petname)))
                                                {
                                                    $("#petname").css('border-color', '#d2d6de');
                                                }
                                                if (licensetag.trim().length == 0 || !(validText.test(licensetag)))
                                                {
                                                    $("#licensetag").css('border-color', '#d2d6de');
                                                }
                                                if (licensetagissue.trim().length == 0 || !(validText.test(licensetagissue)))
                                                {
                                                    $("#licensetagissue").css('border-color', '#d2d6de');
                                                }

                                                if (hasError) {
                                                    $('#validation').addClass("alert alert-danger");
                                                    $('#validation').show();
                                                }

                                                if (hasError == false) {
                                                    $("#validerrorform").html('');
                                                    $("#addcustomerform").submit();
                                                } else
                                                {
                                                    $("#validerrorform").html(errormsg);
                                                    $("html, body").animate({scrollTop: 0}, "slow");
                                                    return false;
                                                }
                                            });


                                            $(".allownumericwithoutdecimal").on("keyup blur", function (e) {
                                                $(this).val($(this).val().replace(/[^\d].+/, ""));
                                                //if ((event.which < 48 || event.which > 57)) {
                                                // if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                                    event.preventDefault();
                                                }
                                            });

                                            $(".allownumericwithdecimal").on("keyup blur", function (e) {
                                                var val = $(this).val();
                                                if (isNaN(val)) {
                                                    val = val.replace(/[^0-9\.]/g, '');
                                                    if (val.split('.').length > 2)
                                                        val = val.replace(/\.+$/, "");
                                                }
                                                $(this).val(val);
                                            });

                                            $(".allowcharacterwithouthtmltags").on("keyup blur", function (e) {
                                                $(this).val($(this).val().replace(/(<([^>]+)>)/ig, ""));
                                                /*if((e.which > 64 && e.which < 91 ) || (e.which > 96 && e.which < 123)){
                                                 event.preventDefault();
                                                 }*/
                                            });


                                            function populateStates(selCountry)
                                            {
                                                var selState1 = "<?php echo $custdetail['state']; ?>";
                                                var jqueryarray = "";
                                                if (selCountry == "Canada")
                                                    jqueryarray = <?php echo json_encode($Canadastates); ?>;
                                                else
                                                    jqueryarray = <?php echo json_encode($USstates); ?>;
                                                for (var i = 0; i < jqueryarray.length; i++) {
                                                    if (selState1.match(jqueryarray[i]))
                                                    {
                                                        $('#state').append('<option value="' + jqueryarray[i] + '" selected>' + jqueryarray[i] + '</option>');
                                                    } else
                                                    {
                                                        $('#state').append('<option value="' + jqueryarray[i] + '">' + jqueryarray[i] + '</option>');
                                                    }
                                                }
                                            }
                                        });




                                        function showLocalVets()
                                        {

                                            $("#loading").show();
                                            var url = "<?php echo base_url(); ?>vendor/customer/getRegionalVets";
                                            $.post(url, function (data)
                                            {
                                                $("#loading").hide();
                                                $("#vetlist").html('');

                                                $("#vetlist").html(data);
                                                $('#vetlookup').modal('show');
                                            });

                                        }
                                        function validateEmail(email) {
                                            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                            return emailReg.test(email);
                                        }

                                        function validateZip(zipcode, country)
                                        {
                                            if (country == "Canada")
                                            {
                                                var zipReg = /^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/;
                                                return zipReg.test(zipcode);
                                            } else
                                            {
                                                if (zipcode.length != 5)
                                                    return false;
                                                else
                                                {
                                                    var intRegex = /^\d+$/;
                                                    return intRegex.test(zipcode);
                                                }
                                            }

                                        }

                                        function addReminders()
                                        {
                                            var d = new Date();

                                            var currDate = d.getDate();
                                            var currMonth = d.getMonth();
                                            var currYear = d.getFullYear();

                                            var dateStr = /*currYear+ "-"+currMonth+ "-"+currDate;*/new Date().toJSON().slice(0, 10);

                                            var freqtimes = $("#frequencytimes").val();
                                            var freq = $("#frequency").val();

                                            if (freqtimes != "" && freq != "")
                                            {
                                                $("#remindertable").html('');

                                                if (freq == "Day")
                                                {
                                                    for (var i = 0; i < freqtimes; i++)
                                                    {

                                                        var timeid = 'remindertime_' + i;
                                                        $("#remindertable").append('<tr><td>Reminder</td><td><div class="input-group"><input id="' + timeid + '"type="text" class="form-control  time ui-timepicker-input"></div></td></tr>');
                                                        $("#" + timeid).timepicker({showMeridian: false});
                                                        $("#" + timeid).timepicker('setTime', new Date());
                                                        $("#" + timeid).keydown(function (e)
                                                        {
                                                            e.preventDefault();
                                                        });
                                                    }
                                                } else if (freq == "Week" || freq == "Month")
                                                {
                                                    for (var i = 0; i < freqtimes; i++)
                                                    {

                                                        var timeid = 'remindertime_' + i;
                                                        var dateid = 'reminderdate_' + i;
                                                        $("#remindertable").append('<tr><td>Reminder</td><td><input type="text"  class="form-control clsDatePicker "  id="' + dateid + '" data-date-format="MM dd, yyyy"/></td><td><div class="input-group"><input id="' + timeid + '"type="text" class="form-control  time ui-timepicker-input"></div></td></tr>');
                                                        $("#" + dateid).datepicker({
                                                            startDate: new Date(),
                                                            autoclose: true
                                                        });
                                                        $("#" + dateid).datepicker('setDate', new Date());
                                                        $("#" + dateid).keydown(function (e)
                                                        {
                                                            e.preventDefault();
                                                        });

                                                        $("#" + timeid).timepicker({showMeridian: false});
                                                        $("#" + timeid).timepicker('setTime', new Date());
                                                        $("#" + timeid).keydown(function (e)
                                                        {
                                                            e.preventDefault();
                                                        });
                                                        //$("#"+dateid).val(dateStr);
                                                        // $(".clsDatePicker").datepicker("refresh");
                                                    }

                                                }

                                            }
                                        }

                                        function getMedicine(medicineid, medicinename, medicinelink)
                                        {

                                            $("#medicine").val(medicineid);
                                            $("#medicinename").val(medicinename);
                                            $("#medicinelink").val(medicinelink);
                                            $('#searchinput').val(medicinename);
                                        }

                                        function getVaccination(vaccineid, vaccinename)
                                        {

                                            $("#vaccine").val(vaccineid);
                                            $("#vaccinename").val(vaccinename);
                                            $('#searchinput1').val(vaccinename);
                                        }
                                        function addMedication()
                                        {
                                            
                                            var medicineid = $("#medicine").val();
                                            var medicinelink = $("#medicinelink").val();
                                            var frequency = $("#frequency").val();
                                            var dosage = $("#dosage").val();
                                            var frequencytimes = $("#frequencytimes").val();
                                            //var meditime = $("#medtime").val();

                                            var meditime = "";
                                            var meditime1 = new Date().toJSON().slice(0, 10);

                                            // var medicinename=$("#medicine option:selected").text();
                                            var medicinename = $("#medicinename").val();
                                            if ($('#searchinput').val() == '')
                                            {

                                                $("#validerror").html('Pleas select medication name.');
                                            } else if (frequency == '')
                                            {
                                                $("#validerror").html('Please select regularity value.');
                                            } else if (dosage == '')
                                            {
                                                $("#validerror").html('Please specify dosage value.');
                                            } else if (frequencytimes == '')
                                            {
                                                $("#validerror").html('Please select number of times value.');
                                            } else
                                            {
                                                $("#validerror").html('');
                                                $("#medicine").val('');
                                                $("#medicinelink").val('');
                                                $("#frequency").val('');
                                                $("#frequencytimes").val('');
                                                $("#dosage").val('');
                                                $('#searchinput').val('');

                                                var dispfreq = "";
                                                if (frequency == "Day" || frequency == "Week" || frequency == "Month")
                                                    //dispfreq=dosage+" Tablets at "+meditime+" "+frequency;
                                                    dispfreq = dosage + " dose(s)  " + frequencytimes + " times per " + frequency;
                                                // else
                                                //dispfreq=dosage+" Tablets at "+meditime+" every "+frequency;

                                                if (frequency == "Day")
                                                {

                                                    for (var i = 0; i < frequencytimes; i++)
                                                    {

                                                        var timeid = 'remindertime_' + i;
                                                        var time = getDateIn24($("#" + timeid).val());
                                                        if (meditime == "")
                                                            meditime = meditime1 + " " + time;
                                                        else
                                                            meditime = meditime + "," + meditime1 + " " + time;

                                                    }
                                                } else if (frequency == "Week" || frequency == "Month")
                                                {
                                                    for (var i = 0; i < frequencytimes; i++)
                                                    {
                                                        var timeid = 'remindertime_' + i;
                                                        var time = getDateIn24($("#" + timeid).val());
                                                        var dateid = 'reminderdate_' + i;
                                                        if (meditime == "")
                                                            meditime = $("#" + dateid).val() + " " + time;
                                                        else
                                                            meditime = meditime + "," + $("#" + dateid).val() + " " + time;
                                                    }

                                                }


                                                var reminders = meditime.split(",");
                                                var remindersstr = "";
                                                for (var i = 0; i < reminders.length; i++)
                                                {
                                                    reminders1 = reminders[i].split(" ");
                                                    var H = +reminders1[1].substr(0, 2);
                                                    var h = H % 12 || 12;
                                                    var ampm = H < 12 ? "am" : "pm";
                                                    reminders1[1] = h + reminders1[1].substr(2, 3) + ampm;
                                                    remindersstr = remindersstr + reminders1[0] + " " + reminders1[1] + "<br>";
                                                }
                                                //my edit
                                                if (medcnt == 0)
                                                {    
                                                    $("#medicinetable1").append('<tr id="autoHeader"><th>Medicine</th><th>Frequency</th><th>Link</th><th>Reminder</th><th>Delete</th></tr>');
                                                }

                                                if ($.inArray(medicineid, selected_medicine) ===  -1) {
                                                    $("#medicinetable").append('<tr id="' + medicineid + '"><td>' + medicinename + '</td><td>' + dispfreq + '</td><td> <a href="' + medicinelink + '" target="_blank">Link</a></td><td> <a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medicineid + ');">Remove </a> </td></tr>');

                                                    $("#medicinetable1").append('<tr id="' + medicineid + '"><td>' + medicinename + '</td><td>' + dispfreq + '</td><td> <a href="' + medicinelink + '" target="_blank">Link</a></td><td> <a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medicineid + ');">Remove </a> </td></tr>');
                                                    medicationadded.medication.push({'medicineid': medicineid, 'frequency': frequency, 'dosage': dosage, 'medicationtime': meditime, 'medicationtimings': meditime, 'frequencytimes': frequencytimes});
                                                    medcnt++;
                                                    selected_medicine.push(medicineid);
                                                } else {
                                                    $("#validerror").html('Warning, This medication has already been added for this pet.');
                                                }
                                                $("#remindertable").html('');
                                            }
                                        }


                                        function addVaccination()
                                        {

                                            var vaccineid = $("#vaccine").val();
                                            var info = "-";

                                            // var medicinename=$("#medicine option:selected").text();
                                            var vaccinename = $("#vaccinename").val();
                                            var duedate = $("#duedate").val();
                                            if ($('#searchinput1').val() == '')
                                            {

                                                $("#validerror2").html('Pleas select Vaccine name.');
                                            } else if (duedate == '')
                                            {
                                                $("#validerror2").html('Pleas Select Due Date.');
                                            } else
                                            {
                                                $("#validerror2").html('');
                                                $("#vaccine").val('');
                                                $('#searchinput1').val('');
                                                if (vaccnt == 0)
                                                {
                                                    $("#vaccinationtable1").append('<tr><th>Vaccination</th><th>Due Date</th><th>Delete</th></tr>');
                                                }
                                                if ($.inArray(vaccineid, selected_vaccination) ===  -1) {
                                                $("#vaccinationtable").append('<tr id="' + vaccineid + '"><td>' + vaccinename + '</td><td>' + duedate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vaccineid + ');">Remove </a> </td></tr>');
                                                $("#vaccinationtable1").append('<tr id="' + vaccineid + '"><td>' + vaccinename + '</td><td>' + duedate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vaccineid + ');">Remove </a> </td></tr>');

                                                vaccinationadded.vaccination.push({'vaccineid': vaccineid, 'duedate': duedate});
                                                vaccnt++;
                                                selected_vaccination.push(vaccineid);
                                                } else{
                                                    $("#validerror2").html("Warning, This vaccination has already been added for this pet.");
                                                }
                                            }
                                        }
                                        function getDateIn24(time)
                                        {
                                            var hours = Number(time.match(/^(\d+)/)[1]);
                                            var minutes = Number(time.match(/:(\d+)/)[1]);
                                            var AMPM = (time.match(/:(.*)$/)[1]).substr(2, 4);
                                            if (AMPM == "pm" && hours < 12)
                                                hours = hours + 12;
                                            if (AMPM == "am" && hours == 12)
                                                hours = hours - 12;
                                            var sHours = hours.toString();
                                            var sMinutes = minutes.toString();
                                            if (hours < 10)
                                                sHours = "0" + sHours;
                                            if (minutes < 10)
                                                sMinutes = "0" + sMinutes;
                                            return (sHours + ":" + sMinutes);
                                        }

                                        function removeMedication(removemedid)
                                        {

                                            var confrm = confirm('Are you sure you want to remove this medication?');
                                            if (confrm == true)
                                            {
                                                for (var i = 0, l = medicationadded.medication.length; i < l; i++) {
                                                    var obj = medicationadded.medication[i];

                                                    //alert(obj['medicineid']);
                                                    if (obj['medicineid'] == removemedid)
                                                    {
                                                        medicationadded.medication.splice(i, 1);
                                                        break;
                                                    }
                                                }
                                                // alert(JSON.stringify( medicationadded));
                                                //alert(medicationadded.medication.length);
//                                                var rowCount = $('#medicinetable1').length;
                                              
                                                $('#medicinetable tr#' + removemedid).remove();
                                                $('#medicinetable1 tr#' + removemedid).remove();
                                                var rowCount = $('#medicinetable1 tr').length;
                                                if(rowCount == '1')
                                                {
                                                    $("#autoHeader").remove();
                                                    medcnt = 0;
                                                    selected_medicine = [];                                                    
                                                }
                                                return true;
                                            } else
                                            {
                                                event.preventDefault();
                                                return false;
                                            }

                                        }

                                        function removeVaccination(removemedid)
                                        {

                                            var confrm = confirm('Are you sure you want to remove this vaccination?');
                                            if (confrm == true)
                                            {
                                                for (var i = 0, l = vaccinationadded.vaccination.length; i < l; i++) {
                                                    var obj = vaccinationadded.vaccination[i];

                                                    //alert(obj['medicineid']);
                                                    if (obj['vaccineid'] == removemedid)
                                                    {
                                                        vaccinationadded.vaccination.splice(i, 1);
                                                        break;
                                                    }
                                                }
                                                // alert(JSON.stringify( medicationadded));
                                                //alert(medicationadded.medication.length);
                                                $('#vaccinationtable tr#' + removemedid).remove();
                                                $('#vaccinationtable1 tr#' + removemedid).remove();
                                                return true;
                                            } else
                                            {
                                                event.preventDefault();
                                                return false;
                                            }

                                        }

                                        function addMedicationDone()
                                        {
                                            var totalmedadded = medcnt;

                                            $('#medicinesadded').val(JSON.stringify(medicationadded));
                                            //$('input[name="medicinesadded[]"]').val(medicines);
                                            $('#searchinput').val('');
                                            //$('#medicationaddedlabel').html("<lable><b>"+ medicationadded.medication.length+" medications added"+"</b></label>");
                                            $('#medication').modal('hide');
                                        }

                                        function addVaccinationDone()
                                        {
                                            var totalvacadded = vaccnt;

                                            $('#vaccinesadded').val(JSON.stringify(vaccinationadded));
                                            //$('input[name="medicinesadded[]"]').val(medicines);
                                            $('#searchinput').val('');
                                            //$('#vaccinationaddedlabel').html("<lable><b>"+ vaccinationadded.vaccination.length+" vaccinations added"+"</b></label>");
                                            $('#vaccination').modal('hide');
                                        }
                                        function autoInvite(email)
                                        {
                                            if (validateEmail(email))
                                            {
                                                var url = "<?php echo base_url(); ?>vendor/customer/checkInvitedUser";

                                                $.post(url, {email: email}, function (data)
                                                {
                                                    if (data == 'existinguser')
                                                    {
                                                        profileImportInvite(email, false);

                                                    } else if (data == 'newuser')
                                                    {
                                                        inviteUser(email, false);
                                                    }

                                                });
                                            } else {
                                                alert('Kindly enter a valid Email address.');
                                            }
                                        }


                                        function inviteUser(email, auto)
                                        {



                                            if (email == '')
                                            {

                                                $("#validerror").html('Enter Email ID');

                                            } else if (!validateEmail(email)) {
                                                $("#validerror").html('Enter valid Email ID');

                                            } else
                                            {
                                                $("#validerror").html('');
                                                var url = "<?php echo base_url(); ?>vendor/customer/inviteUser";

                                                $.post(url, {email: email}, function (data)
                                                {
                                                    if (!auto)
                                                    {
                                                        if (data == '1')
                                                        {
                                                            $("#inviteEmail").val('');
                                                            alert("Invitation mail sent successfully to " + email);
                                                        } else
                                                        {
                                                            alert("Some problem occured while sending mail!");
                                                        }
                                                    }

                                                });
                                            }
                                        }

                                        function profileImportInvite(email, auto)
                                        {



                                            if (email == '')
                                            {

                                                $("#validerror1").html('Enter Email ID');

                                            } else if (!validateEmail(email)) {
                                                $("#validerror1").html('Enter valid Email ID');

                                            } else
                                            {
                                                $("#validerror1").html('');
                                                var url = "<?php echo base_url(); ?>vendor/customer/inviteUserForProfileImport";

                                                $.post(url, {email: email}, function (data)
                                                {
                                                    if (!auto)
                                                    {
                                                        if (data == '1')
                                                        {
                                                            $("#inviteEmailProfImport").val('');
                                                            alert("Invitation mail sent successfully to " + email);
                                                        } else if (data == '2')
                                                        {
                                                            alert("BabelBark User with this email id doen't exist!");
                                                        } else if (data == '3')
                                                        {
                                                            alert("You have already sent the invitation to this user!");
                                                        } else
                                                        {
                                                            alert("Some problem occured while sending mail!");
                                                        }
                                                    }

                                                });
                                            }

                                        }


</script>
<script>
    //form validation step by step   

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('bg-orange').addClass('btn-default');
            $item.addClass('bg-orange');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

        // Start of Appointment add/update form validation //
        var hasError = false;
        var errormsg = "";
        var step = $(this).attr('value');
        var fname = $("#customerfname").val();
        var lname = $("#customerlname").val();
        var email = $("#customeremail").val();
        var petname = $("#petname").val();
        var vetname = $("#vetname").val();
        var zipcode = $("#zipcode").val();
        var country = $("#country").val();
        var phoneno = $('#phoneno').val();
        var vetphone = $('#vetphone').val();
        var address = $("#address").val();
        var city = $("#city").val();
        var licensetag = $("#licensetag").val();
        var licensetagissue = $("#licensetagissue").val();
        var validText = /(<([^>]+)>)/ig;

        switch (step) {
            case "step-1":
                //alert("step-1");
                if (fname.trim().length == 0)
                {
                    errormsg = 'The Customer First Name field is required!';
                    $("#customerfname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(fname))
                {
                    errormsg = 'No HTML Tags are allowed!';
                    $("#customerfname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (lname.trim().length == 0)
                {
                    errormsg = 'The Customer Last Name field is required!';
                    $("#customerlname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(lname))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#customerlname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (email != '' && !validateEmail(email)) {
                    errormsg = 'The Email field must contain a valid email address';
                    $("#customeremail").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (phoneno != '')
                {
                    var phone = $('#phoneno').val();
                    var phoneNum = phone.replace(/[^\d]/g, '');
                    if (phoneNum.length > 12 || phoneNum.length < 6)
                    {
                        errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                        $("#phoneno").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                } else if (validText.test(address))
                {
                    errormsg = 'No HTML Tags are allowed!';
                    $("#address").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(city))
                {
                    errormsg = 'No HTML Tags are allowed!';
                    $("#city").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (zipcode != '' && !validateZip(zipcode, country)) {
                    errormsg = 'validation for correct Zipcode format should be taken from Sign Up form';
                    $("#zipcode").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (fname.trim().length != 0 && !(validText.test(fname)))
                {
                    $("#customerfname").css('border-color', '#d2d6de');
                }
                if (lname.trim().length != 0 && !(validText.test(lname)))
                {
                    $("#customerlname").css('border-color', '#d2d6de');
                }
                if (email != '' && validateEmail(email)) {

                    $("#customeremail").css('border-color', '#d2d6de');
                }
                if (address.trim().length == 0 || !(validText.test(address)))
                {
                    $("#address").css('border-color', '#d2d6de');
                }
                if (city.trim().length == 0 || !(validText.test(city)))
                {
                    $("#city").css('border-color', '#d2d6de');
                }
                break;
            case "step-2":
                //alert("step-2");
                if (petname.trim().length == 0)
                {
                    errormsg = 'The Pet Name field is required!';
                    $("#petname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(petname))
                {
                    errormsg = 'No HTML Tags are allowed!';
                    $("#petname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(licensetag))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#licensetag").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(licensetagissue))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#licensetagissue").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (petname.trim().length != 0 && !(validText.test(petname)))
                {
                    $("#petname").css('border-color', '#d2d6de');
                }
                if (licensetag.trim().length == 0 || !(validText.test(licensetag)))
                {
                    $("#licensetag").css('border-color', '#d2d6de');
                }
                if (licensetagissue.trim().length == 0 || !(validText.test(licensetagissue)))
                {
                    $("#licensetagissue").css('border-color', '#d2d6de');
                }
                break;
            case "step-3":
                alert("sadasdd");
                if (vetphone != '')
                {
                    var phone = $('#phoneno').val();
                    var phoneNum = phone.replace(/[^\d]/g, '');
                    if (phoneNum.length > 12 || phoneNum.length < 6)
                    {
                        errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                        $("#vetphone").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                }
                break;

        }

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
            $("#validerrorform").html(errormsg);
        } else {
            $('#validation').hide();
            $("#validerrorform").html('');
        }

        // End of Appointment add/update form validation //

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alert(curInputs[i].val());
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.bg-orange').trigger('click');
    var rowCount=1 ;
    var petFile  ;
    function addMoreRowsToAttachEdit(frm){
        petFile = petFile + rowCount;
        var medicalfile = $('#medicalfile').val();
        var recRow = '<tr id="rowCount'+rowCount+'">' +
            "<td><input type='file' multiple='' name='petFile[]' id='"+petFile+"'/>"+"</td>" +
            "<td><label><?php echo date('d-m-Y');?></label></td>" +
            '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td>'+
            '</tr>';
        jQuery('#addedRowsToAttachEdit').append(recRow);
        rowCount ++;
    }

    function removeRow(removeNum) {
        jQuery('#rowCount'+removeNum).remove();
    }

    var vendorFile  ;
    function addMoreRowsToAttach(frm){
        vendorFile = vendorFile + rowCount;
        var recRow = '<tr id="rowCount'+rowCount+'">' +
            "<td><input type='file' multiple='' name='vendorFile[]' id='"+vendorFile+"'/>"+"</td>" +
            "<td><label><?php echo date('d-m-Y');?></label></td>" +
            '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td>'+
            '</tr>';
        jQuery('#addedRowsToAttach').append(recRow);
        rowCount ++;
    }

</script>