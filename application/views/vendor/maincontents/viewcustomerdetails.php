<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
if (ALLOW_VET == 1) {
    ?>
    <style>
        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            background-color: #23527c !important;
            color: #fff;
        }
    </style>
<?php } else { ?>
    <style>
        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            background-color: #ff851b !important;
            color: #fff;
        }
    </style>
<?php } ?>

<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
<!-- timepicker js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js"></script>

<style>
    .list-group{
        overflow:scroll;
        height:250px;
    }
    .clsDatePicker {
        z-index:1200 !important;
    }
    #updateInfo_s1, #updateInfo_s2, .close_s1 {
        margin-right: 2%;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($custdetail['customerid'] == 0)
                echo "Add Generic Customer";
            else
                echo "Customer Detail";
            ?>
        </h1>
        <?php if ($custdetail['customerid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customers</a></li>
                <li class="active">Add Customers</a></li>
            </ol>
        <?php } ?>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <form name="addcustomerform" id="addcustomerform"  action="<?php echo base_url(); ?>vendor/customer/updateCustomer" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="vendorid" id="vendorid" value="<?php echo $admin['adminDetails']['vendorid']; ?>"/>
            <input type="hidden" name="customerid" id="customerid" value="<?php echo $custdetail['customerid']; ?>"/>
            <input type="hidden" name="petid" id="petid"/>
            <input type="hidden" name="vetid" id="vetid" value="<?php echo $vetdetail['veterinarianid']; ?>"/>
            <input type="hidden" name="medicinesadded" id="medicinesadded"/>
            <input type="hidden" name="vaccinesadded" id="vaccinesadded"/>
            <div class="box">
                <center><h1>Customer Details</h1></center>
                <div class="box-tools ">
                    <div class="row">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-3 pull-right">
                            <div class="form-group">
                                <button class="btn bg-orange" type="button" id="sendInvite" >Invite User to Babelbark</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="stepwizard col-md-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn bg-orange btn-circle">1</a>
                                    <p>Customer Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle">2</a>
                                    <p>Pet Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle">3</a>
                                    <p>Veterinarian Details</p>
                                </div>
                            </div>
                        </div>



                        <!--############################# STEP 1 start ##########################################-->
                        <div class="row setup-content" id="step-1">
                            <div class="col-lg-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Customer First Name<font color="red">*</font></label>
                                                    <input id="customerfname" name="customerfname" class="form-control" type="text" placeholder="Customer First  Name" value="<?php echo set_value('customerfname', $custdetail['firstname']); ?>" maxlength="200">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Customer Last Name<font color="red">*</font></label>
                                                    <input id="customerlname" name="customerlname" class="form-control" type="text" placeholder="Customer Last  Name" value="<?php echo set_value('customerlname', $custdetail['lastname']); ?>" maxlength="200">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="targetpopulation">Email Address</label>
                                                    <input id="customeremail" class="form-control" type="email" placeholder="Email" name="customeremail" maxlength="200" value="<?php echo set_value('customeremail', $custdetail['email']); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Phone Number</label>
                                                    <?php
                                                    if ($custdetail['phoneno'] == 0) {
                                                        $custdetail['phoneno'] = "";
                                                    }
                                                    ?>
                                                    <input class="form-control " placeholder="Phone Number" id="phoneno" name="phoneno" type="text" value="<?php echo set_value('phoneno', $custdetail['phoneno']); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Address</label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="country" id="country">
                                                                <option value="">Select Country</option>
                                                                <option value="Canada" <?php echo set_select('country', 'Canada', ( $custdetail['country'] == 'Canada' ? TRUE : FALSE)); ?>>Canada</option>
                                                                <option value="United States" <?php echo set_select('country', 'United States', ( $custdetail['country'] == 'United States' || $custdetail['country'] == 'USA' ? TRUE : FALSE)); ?>>United States</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input id="city" class="form-control" type="text" placeholder="City" name="city" maxlength="45" value="<?php echo set_value('city', $custdetail['city']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-6">
                                                            <input id="address" class="form-control" type="text" placeholder="Address" name="address" maxlength="45" value="<?php echo set_value('address', $custdetail['address']); ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input id="zipcode" class="form-control" type="text" placeholder="Zipcode" name="zipcode" maxlength="15" value="<?php echo set_value('zipcode', $custdetail['zipcode']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="state" id="state"></select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-xs-12 ">
                                                <div class="form-group">
                                                    <label>Notes</label>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <textarea placeholder="Notes" class="form-control" id="notes" name="notes"><?php echo $custdetail['notes']; ?></textarea>
                                                        </div>
                                                        <div class="col-xs-6"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-lg-12 col-xs-12">
                                    <div class="box-body">
                                        <p style="border: 1px solid #C2C2C2"></p>
                                        <div class="col-lg-5 col-xs-5">
                                            <table class="table" id="fileUploadTable">
                                                <thead style="background-color:#EEEEEE;">
                                                <th><label>Select file<font color="red">*</font></label></th>
                                                <th><label>Upload Date</label></th>
                                                <th><td><a onclick="addMoreRowsToAttach(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                                </thead>
                                                <tbody id="addedRowsToAttach">

                                                </tbody>
                                            </table>
                                            <div class="alert alert-success">
                                                <strong>Uploaded files are only saved upon hitting "Save"</strong>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-xs-7">
                                            <?php
                                            if (count($vendorFileInfo)>0) {
                                                ?>
                                                <table class="table" id="fileVendorTable">
                                                    <thead style="background-color:#EEEEEE;">
                                                    <th>File Name</th>
                                                    <th>Upload Date</th>
                                                    <th>View File</th>
                                                    <th>Delete File</th>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($vendorFileInfo as $vendorRows){  ?>
                                                        <tr id="vendorRow<?php echo $vendorRows['id']; ?>">
                                                            <td><?php echo $vendorRows['filename']; ?></td>
                                                            <td><?php echo $vendorRows['updatedon']; ?></td>
                                                            <td><a href="<?php echo base_url().'view_vendor_file/'.$vendorRows['id']; ?>" target="_blank">View</td>
                                                            <td>
                                                                <a style="cursor: pointer" onclick="javascript:deleteVendorFile('<?php echo $vendorRows['id']; ?>','<?php echo $vendorRows['vendorid']; ?>','<?php echo $vendorRows['petid']; ?>')">
                                                                    <span class="label label-danger">Delete</span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                </table>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="col-lg-12 col-xs-12">
                                            <div class="col-lg-6 col-xs-6"></div>
                                            <div class="col-lg-6 col-xs-6">
                                                <div class="box-footer">
                                                    <div class="form-group">
                                                        <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                                        <button class="btn btn-warning pull-right" id="updateInfo_s1">Save</button>
                                                        <?php echo anchor('vendor/customer/listofgenericcustomer','Close','class="btn  btn-danger pull-right close_s1"');?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <!--############################# STEP 1 end ##########################################-->
                        <!--############################# STEP 2 start ##########################################-->
                        <div class="row setup-content" id="step-2">
                            <div class="col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Choose Pet </label>
                                                    <ul class="nav nav-pills" name="selectpet" id="selectpet">
                                                        <?php for ($i = 0; $i < count($pets); $i++) { ?>
                                                            <li value="<?php echo $pets[$i]['petid']; ?>">
                                                                <input type="hidden" class="pet_value pet_value<?php echo $i; ?>" value="<?php echo $pets[$i]['petid']; ?>">
                                                                <a href="#"><?php echo $pets[$i]['name']; ?></a>
                                                            </li>
                                                        <?php } ?>
                                                        <button class="btn bg-orange" type="button" id="addmorepets" style="float:relative; margin-left:8px;">Add More Pets</button>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Pet Name<font color="red">*</font></label>
                                                    <input id="petname" name="petname" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="Pet name" maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Photo (optional)</label>
                                                    <input class="form-control" type="file" id="profileimage" name="profileimage" onchange="PreviewImage();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-3" id="currentphoto">
                                                    <label for="">Current Photo</label>
                                                    <a href="#" class="thumbnail" style="width: 160px; height: 160px;">
                                                        <img src=" " id="currentimage" name="currentimage" style="width:150px; height: 150px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 15px;" id="previewimage">
                                                <div class="col-sm-3">
                                                    <label for="preview">Preview Photo</label>
                                                    <a href="#" class="thumbnail" style="width: 142px; height: 145px;">
                                                        <img src="<?php echo empty_image('', ''); ?>" id="preview" style="width: 140px; height: 135px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Is Pet Microchipped</label>
                                            <input type="checkbox" id="microchipped" name="microchipped" value="0" />
                                        </div>
                                        <div class="chipid">
                                            <div class="form-group">
                                                <label>Pet Microchip ID</label>
                                                <input type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Pet Microchip ID" id="microchipid" name="microchipid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Number</label>
                                            <input id="licensetag" type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Pet Govt License Tag Number" name="govtlicensetagno">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Current Weight (<?php
                                                if ($unittype == 'Metric') {
                                                    echo 'kgs';
                                                } else {
                                                    echo 'lbs';
                                                }
                                                ?>)</label>
                                            <input type="text" class="form-control allownumericwithdecimal" placeholder="Pet Current Weight" name="petcurrentweight" id="petcurrentweight">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Target Weight (<?php
                                                if ($unittype == 'Metric') {
                                                    echo 'kgs';
                                                } else {
                                                    echo 'lbs';
                                                }
                                                ?>)</label>
                                            <input type="text" class="form-control allownumericwithdecimal" placeholder="Pet Target Weight" name="pettargetweight" id="pettargetweight">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Issue</label>
                                            <input  id="licensetagissue" type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Pet Govt License Tag Issue" name="govtlicensetagissuer">
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Is Your Pet Adopted</label>
                                            <input type="checkbox" name="isadopted" id="isadopted" value="0"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="">Pet BirthDay</label> (Enter a date you celebrate your dog's birthday or adoption day)
                                            <input class="form-control datepicker" placeholder="Pet Birth / Adoption Date" data-date-format="MM dd, yyyy" name="birthdate" id="birthdate" >
                                        </div>
                                        Unsure of Pet's Birthdate
                                        <input type="checkbox" name="bdayunsure" id="bdayunsure" value="1"/>
                                        <div class="form-group">
                                            <div class="adoptiondetail">
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-xs-3">
                                                        Approximate Age
                                                    </div>
                                                    <div class="col-xs-1"> Years</div>
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control allownumericwithoutdecimal" name="approxageyrs" id="approxageyrs">
                                                    </div>
                                                    <div class="col-xs-1"> Months&nbsp;</div>
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control allownumericwithoutdecimal" name="approxagemonths" id="approxagemonths">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <label for="">Calculate Pet Age</label> <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
                                            <input type="text" class="form-control allownumericwithoutdecimal" name="petage" id="petage" value="" readonly>
                                        </div>
                                        <form id="ageunit" name="ageunit">
                                            Years  <input type="radio" name="petageunit"  value="years"/>
                                            &nbsp;&nbsp;Weeks <input type="radio"  name="petageunit"  value="weeks"/>
                                            &nbsp;&nbsp;Days  <input type="radio"  name="petageunit"  value="days" />
                                        </form>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <label for="">Gender </label>
                                            M <input type="radio" name="genderchk" id="genderchk0" value="M" />
                                            F <input type="radio" name="genderchk" id="genderchk1"value="F" />
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-9 col-xs-12 ">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label >Pet Medication : <a href="#"  data-toggle="modal" data-target="#medication"><u>Add Pet Medication</u></a></label>
                                                            <span id="medicationaddedlabel"></span>
                                                            <div class="box-body table-responsive" style="padding: 10px 15px;">
                                                                <table id="medicinetable1" class="table table-bordered table-striped ">
                                                                    <thead>
                                                                    <tr>
                                                                        <!--  <th>Medicine</th>
                                                                          <th>Frequency</th>
                                                                          <th>Link</th>
                                                                          <th>Reminder</th>
                                                                          <th>Delete</th> -->
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Pet Medication History</label>
                                            <textarea id="medicalhistory" class="form-control"  placeholder="Pet Medication History" name="medicalhistory" ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label >Pet Vaccination : <a href="#"  data-toggle="modal" data-target="#vaccination"><u>Add Pet Vaccination</u></a></label>
                                            <span id="vaccinationaddedlabel">   </span>
                                            <div class="box-body table-responsive">
                                                <table id="vaccinationtable1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <!-- <tr>
                                                                <th>Vaccination</th>
                                                                <th>Due Date</th>
                                                                <th>Delete</th>
                                                    </tr> -->
                                                    </thead></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 ">
                                        <div class="form-group">
                                            <label for="targetpopulation">Pet Breed</label>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <select name="primarybreed" id="primarybreed"class="form-control">
                                                        <option value="" >Select Primary Breed</option>
                                                        <?php for ($i = 0; $i < count($breeds); $i++) { ?>
                                                            <option value="<?php echo $breeds[$i]['breedid']; ?>"><?php echo $breeds[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="secondarybreed" id="secondarybreed" class="form-control">
                                                        <option value="" >Select Secondary Breed</option>
                                                        <?php for ($i = 0; $i < count($breeds); $i++) { ?>
                                                            <option value="<?php echo $breeds[$i]['breedid']; ?>"><?php echo $breeds[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 ">
                                        <div class="form-group">
                                            <label for="targetpopulation">Pet Dietary Notes</label>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <select name="primaryfood" id="primaryfood" class="form-control">
                                                        <option value="" >Select Primary Food</option>
                                                        <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                            <option value="<?php echo $feedbrands[$i]['feedbrandid']; ?>"><?php echo $feedbrands[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6" id="feedbrandlink1">

                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="secondaryfood" id="secondaryfood" class="form-control">
                                                        <option value="" >Select Secondary Food</option>
                                                        <?php for ($i = 0; $i < count($feedbrands); $i++) { ?>
                                                            <option value="<?php echo $feedbrands[$i]['feedbrandid']; ?>"><?php echo $feedbrands[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6" id="feedbrandlink2">

                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-6"> <div class="input-group"><input id="feedamount" class="form-control allownumericwithoutdecimal" type="number" placeholder="Amount of feed" name="feedamount"><div class="input-group-addon">(Kilos)</div></div></div>
                                                <div class="col-sm-6"> <div class="input-group"><input id="feedfrequency" class="form-control allownumericwithoutdecimal" type="number" placeholder="Frequency of feed" name="feedfrequency"><div class="input-group-addon">(per day)</div></div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12">
                                    <div class="box-body">
                                        <p style="border: 1px solid #C2C2C2"></p>
                                        <div class="col-lg-5 col-xs-5">
                                            <table class="table" id="fileUploadTable">
                                                <thead style="background-color:#EEEEEE;">
                                                <th><label>Select file<font color="red">*</font></label></th>
                                                <th><label>Upload Date</label></th>
                                                <th><td><a onclick="addMoreRowsToAttachEdit(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                                </thead>
                                                <tbody id="addedRowsToAttachEdit">

                                                </tbody>
                                            </table>
                                            <div class="alert alert-success">
                                                <strong>Uploaded files are only saved upon hitting "Save"</strong>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-xs-7">
                                            <?php
                                            if (count($petFileInfo)<=0) {
                                                $display = 'none';
                                            }else{
                                                $display = '';
                                            }
                                            ?>
                                            <table class="table" id="fileTable" style="display: <?php echo $display?>">
                                                <thead style="background-color:#EEEEEE;">
                                                <th>File Name</th>
                                                <th>Upload Date</th>
                                                <th>View File</th>
                                                <th>Delete File</th>
                                                </thead>
                                                <tbody id="fileTableBody">
                                                <?php
                                                foreach ($petFileInfo as $petRows){  ?>
                                                    <tr id="petRow<?php echo $petRows['id']; ?>">
                                                        <td><?php echo $petRows['filename']; ?></td>
                                                        <td><?php echo $petRows['updatedon']; ?></td>
                                                        <td><a href="<?php echo base_url().'view_vendor_file/'.$petRows['id']; ?>" target="_blank">View</td>
                                                        <td>
                                                            <a style="cursor: pointer" onclick="javascript:deletePetFile('<?php echo $petRows['id']; ?>','<?php echo $petRows['vendorid']; ?>','<?php echo $petRows['petid']; ?>')">
                                                                <span class="label label-danger">Delete</span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn pull-right" type="button" value="step-2">Next</button>
                                        <button class="btn btn-warning pull-right" id="updateInfo_s2">Save</button>
                                        <?php echo anchor('vendor/customer/listofgenericcustomer','Close','class="btn  btn-danger pull-right close_s1"');?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 2 end ##########################################-->
                        <!--############################# STEP 3 start ##########################################-->
                        <div class="row setup-content" id="step-3">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Veterinarian Details</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-5">
                                                                <a href="javascript:showLocalVets();" ><u>Search for local veterinarians</u></a>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <h5 id='loading' >loading, Please wait..</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Veterinarian Name</label>
                                                        <input id="vetname" class="form-control allowcharacterwithouthtmltags"  placeholder="Veterinarian Name" name="vetname" maxlength="200"  value="<?php echo set_value('vetname', $vetdetail['name']); ?>">
                                                        <label class="control-label" for="inputError"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Phone Number</label>
                                                        <input id="vetphone" class="form-control" type="text" placeholder="Phone Number" name="vetphone" value="<?php echo set_value('vetphone', $vetdetail['phoneno']); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Emergency Number</label>
                                                        <input id="vetemergphone" class="form-control allownumericwithoutdecimal" type="text" placeholder="Emergency Phone Number" name="vetemergphone" value="<?php echo set_value('vetemergphone', $vetdetail['emergencyno']); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Point of Contact</label>
                                                        <input id="vetcontactPerson" class="form-control" type="text" placeholder="Name of person to contact" name="vetcontactPerson" maxlength="200" value="<?php echo set_value('vetcontactPerson', $vetdetail['contactperson']); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Addresss</label>
                                                        <input id="vetaddress" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="Address" name="vetaddress" maxlength="45" value="<?php echo set_value('vetaddress', $vetdetail['address']); ?>">
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-6"> <input id="vetcity" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="City" name="vetcity" maxlength="45" value="<?php echo set_value('vetcity', $vetdetail['city']); ?>"></div>
                                                            <div class="col-sm-6"> <input id="vetstate" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="State" name="vetstate" maxlength="45" value="<?php echo set_value('vetstate', $vetdetail['state']); ?>"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-6"> <input id="vetzipcode" class="form-control allowcharacterwithouthtmltags" type="text" placeholder="Zipcode" name="vetzipcode" maxlength="15" value="<?php echo set_value('vetzipcode', $vetdetail['zipcode']); ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="">Hours of operation</label>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-xs-1"> From </div>
                                                            <div class="col-xs-5">
                                                                <input id="fromhoursofoperation" type="text allowcharacterwithouthtmltags" name="fromhoursofoperation" value="<?php echo set_value('fromhoursofoperation', $vetdetail['fromhoursofoperation']); ?>" class="form-control input-small">
                                                            </div>
                                                            <div class="col-xs-1"> To </div>
                                                            <div class="col-xs-5">
                                                                <input id="tohoursofoperation" type="text allowcharacterwithouthtmltags" name="tohoursofoperation" value="<?php echo set_value('tohoursofoperation', $vetdetail['tohoursofoperation']); ?>" class="form-control input-small">
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-xs-12">
                                                                <?php
                                                                $checkeddays_arr = array();
                                                                if ($vetdetail['availabilitydays'] != "") {
                                                                    $checkeddays_arr = explode(",", $vetdetail['availabilitydays']);
                                                                }
                                                                for ($i = 0; $i < count($weekdays); $i++) {
                                                                    ?>
                                                                    <?php echo $weekdays[$i]; ?> &nbsp;<input type="checkbox" name="availabilitydays[]" value="<?php echo $weekdays[$i]; ?>"  <?php echo (in_array($weekdays[$i], $checkeddays_arr) ? 'checked' : null); ?>/>
                                                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <?php if ($custdetail['customerid'] == 0) { ?>
                                        <input type="submit" class="btn btn-warning" name='submit' value="Save">
                                    <?php } else { ?>
                                    <div>
                                        <button class="btn btn-warning pull-right" id="updateInfo">Update</button>
                                        <?php echo anchor('vendor/customer/listofgenericcustomer','Close','class="btn  btn-danger pull-right close_s1"');?>
                                    </div>
                                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                    <?php
                                    #echo anchor('vendor/customer/listofgenericcustomer','Back','class="btn  btn-primary"');
                                    ?>
                                    <div style="float:right;">
                                        <?php
                                        if (intval($precustomer) > 0 && $preusertype != "N/A")
                                            #echo anchor('vendor/customer/viewDetails/'.$precustomer.'/'.rawurlencode($preusertype),'Previous','class="btn btn-primary"');

                                            ?>
                                            <?php
                                        #if(intval($nextcustomer)>0 && $nextusertype!="N/A")
                                        #echo anchor('vendor/customer/viewDetails/'.$nextcustomer.'/'.rawurlencode($nextusertype),'Next','class="btn btn-primary"');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 3 end ##########################################-->
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
<div id="medication" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Medication Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Pet Medication</h4>
            </div>
            <section class="content">
                <div class="row">
                    <div class=" col-xs-12 ">
                        <form role="form">
                            <div class="form-group">
                                <input class="form-control" id="searchinput" type="text" placeholder="Search..." />
                            </div>
                            <div id="srchmedicine"  class="list-group" >
                                <?php for ($i = 0; $i < count($medicines); $i++) { ?>
                                    <div class="list-group-item" >
                                        <a href="javascript:getMedicine(<?php echo $medicines[$i]['medicineid']; ?>,'<?php echo $medicines[$i]['name']; ?>','<?php echo $medicines[$i]['url']; ?>')"><span id="<?php echo $medicines[$i]['medicineid']; ?>"><?php echo $medicines[$i]['name']; ?></span></a>
                                        <a href="<?php echo $medicines[$i]['url']; ?>" target="_blank" style="float:right">  <img src="<?php echo base_url(); ?>assets/images/webicon.png" width="25"></a>
                                    </div>
                                <?php } ?>

                            </div>
                        </form>
                        <div class="form-group">

                            <input type="hidden" id="medicine"/>
                            <input type="hidden" id="medicinename"/>
                            <input type="hidden" id="medicinelink"/>
                            <div class="row">
                                <div class="col-sm-3">
                                    <input  name="dosage" id="dosage" class="form-control allownumericwithoutdecimal" type="text" placeholder="Tablet" maxlength="3" ></div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="frequencytimes" id="frequencytimes"  onchange="javscript:addReminders();">
                                        <option value=""> Times</option>
                                        <?php for ($i = 1; $i < 11; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>

                                    </select>

                                </div>
                                <div class="col-sm-1">
                                    Per
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="frequency" id="frequency" onchange="javscript:addReminders();">
                                        <option value=""> Regularity</option>
                                        <?php for ($i = 0; $i < count($regularity); $i++) { ?>
                                            <option value="<?php echo $regularity[$i]; ?>"><?php echo $regularity[$i]; ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <br/>



                            <div id="reminderbox" class="box-body table-responsive" style="padding: 10px 15px;">
                                <table id="remindertable" class="table table-bordered table-striped">


                                </table>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-sm-3" >
                                    <button class="btn btn-warning" name='add'  onclick="javascript:addMedication();">Add</button>
                                    <button class="btn btn-warning" name='done'  onclick="javascript:addMedicationDone();">Done</button></div>

                                <div class="col-sm-12"> <label class="text-red" id="validerror"></label></div>
                            </div>

                        </div>
                    </div>

                    <div class="box-body table-responsive" style="padding: 10px 15px;">
                        <table id="medicinetable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Medication</th>
                                <th>Frequency</th>
                                <th>Link</th>
                                <th>Reminder</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>


            </section>
        </div>
    </div>
</div>

<!-- vaccination -->
<div id="vaccination" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Pet Vaccinations</h4>
            </div>
            <section class="content">
                <div class="row">

                    <div class=" col-xs-12 ">
                        <form role="form">
                            <div class="form-group">
                                <input class="form-control" id="searchinput1" type="text" placeholder="Search..." />
                            </div>
                            <div id="srchvaccination"  class="list-group" >
                                <?php for ($i = 0; $i < count($vaccinations); $i++) { ?>
                                    <div class="list-group-item" >
                                        <a href="javascript:getVaccination(<?php echo $vaccinations[$i]['vaccineid']; ?>,'<?php echo $vaccinations[$i]['name']; ?>')"><span id="<?php echo $vaccinations[$i]['vaccineid']; ?>"><?php echo $vaccinations[$i]['name']; ?></span></a>
                                    </div>
                                <?php } ?>

                            </div>
                        </form>
                        <div class="form-group">

                            <label for="">Due Date</label>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-5">
                                    <input class="form-control datepicker" placeholder="Enter Due Date" name="duedate" id="duedate" data-date-format="MM dd, yyyy">
                                </div>
                            </div>

                            <input type="hidden" id="vaccine"/>
                            <input type="hidden" id="vaccinename"/>
                            <br/>
                            <div class="row">
                                <div class="col-sm-3" >
                                    <button class="btn btn-warning" name='add'  onclick="javascript:addVaccination();">Add</button>
                                    <button class="btn btn-warning" name='done'  onclick="javascript:addVaccinationDone();">Done</button></div>

                                <div class="col-sm-12"> <label class="text-red" id="validerror2"></label></div>
                            </div>

                        </div>
                    </div>
                    <div class="box-body table-responsive" style="padding: 10px 15px;">
                        <table id="vaccinationtable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Vaccination</th>
                                <th>Due Date</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>



<!--  Vet lookup popup -->

<div id="vetlookup" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="z-index: 1050">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Local veterinarians</h4>
            </div>

            <section class="content">
                <p>
                <div id="vetlist"></div>
                </p>
            </section>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>



<script>

    var fname_init = $("#customerfname").val();
    var lname_init = $("#customerlname").val();
    var email_init = $("#customeremail").val();
    var petname_init = $("#petname").val();
    var vetname_init = $("#vetname").val();
    var zipcode_init = $("#zipcode").val();
    var country_init = $("#country").val();
    var phoneno_init = $('#phoneno').val();
    var address_init = $("#address").val();
    var city_init = $("#city").val();
    var notes_init = $("#notes").val();

    $(window).load(function () {
        $("#validation").hide();
        $('#selectpet li:eq(0)').addClass('active');
//        var petid=$('#selectpet li:eq(0)').attr('value');
        var unitType = '<?php echo $unittype; ?>';
        var petid = $('#selectpet li:first-child input.pet_value').val();
        $('#petid').val(petid);
        var matchcount = 0;
        var jqueryarray = <?php echo json_encode($pets); ?>;
        var jqueryarray2 = <?php echo json_encode($feedbrands); ?>;
        var jqueryarray3 = <?php echo json_encode($breeds); ?>;
        var petmedicationadded = <?php echo json_encode($petmedication); ?>;
        var petvaccinationadded = <?php echo json_encode($petvaccination); ?>;
        var cweight = '';
        var tweight = '';
        for (var i = 0; i < jqueryarray.length; i++)
        {
            if (jqueryarray[i]['petid'] == petid)
            {
                matchcount = i;
                $('#petname').val(jqueryarray[matchcount]['name']);
                $('#microchipid').val(jqueryarray[matchcount]['microchipid']);
                $('#licensetag').val(jqueryarray[matchcount]['govtlicensetagno']);
                $('#licensetagissue').val(jqueryarray[matchcount]['govtlicensetagissuer']);
                if(unitType == 'British'){
                    cweight =  parseFloat((jqueryarray[matchcount]['currentweight'] * 2.20462));
                    tweight = parseFloat((jqueryarray[matchcount]['targetweight'] * 2.20462));
                    $('#petcurrentweight').val(cweight);
                    $('#pettargetweight').val(tweight);
                }else{
                    $('#petcurrentweight').val(jqueryarray[matchcount]['currentweight']);
                    $('#pettargetweight').val(jqueryarray[matchcount]['targetweight']);
                }             
                $('#isadopted').val(jqueryarray[matchcount]['isadopted']);
                var curr = jqueryarray[matchcount]['birthdate'];
                if(curr != null){
                    var birthdate = $.datepicker.formatDate( "MM dd, yy", new Date(curr) );
                    $('#birthdate').val(birthdate);
                }else {
                    $('#birthdate').val('');
                }                
                $('#feedamount').val(jqueryarray[matchcount]['amountoffeed']);
                $('#feedfrequency').val(jqueryarray[matchcount]['frequencyoffeed']);
                $('#medicalhistory').val(jqueryarray[matchcount]['medicalhistory']);

                if (jqueryarray[matchcount]['proilefpicture'] != null)
                {
                    $('#currentphoto').show();
                    $('#currentimage').attr('src', '<?php echo awsBucketPath; ?>' + jqueryarray[matchcount]['proilefpicture']);
//						$('#currentimage').attr('src','<?php //echo base_url();                    ?>//'+jqueryarray[matchcount]['proilefpicture']);
                    $('#currentimage').show();
                    $('#previewimage').hide();
                }
                else
                {
                    $('#previewimage').show();
                    $('#currentimage').hide();
                    $('#currentphoto').hide();
                }

                if (jqueryarray[matchcount]['birthdayunsure'] == 1)
                {
                    $('#bdayunsure').prop('checked', true);
                    $(".adoptiondetail").show();
                    var age = jqueryarray[matchcount]['approximateage'];
                    age = age.split('years');
                    $('#approxageyrs').val(age[0]);
                    age = age[1].split('months');
                    age[0] = age[0].split(',');
                    $('#approxagemonths').val(age[0]);
                }
                else
                {
                    $('#bdayunsure').prop('checked', false);
                }

                if (jqueryarray[matchcount]['isadopted'] == 1)
                {
                    $('#isadopted').prop('checked', true);
                }
                else
                {
                    $('#isadopted').prop('checked', false);
                }

                if (jqueryarray[matchcount]['microchipped'] == 1)
                {
                    $('#microchipped').prop('checked', true);
                    $('.chipid').show();
                }
                else
                {
                    $('#microchipped').prop('checked', false);
                    $('.chipid').hide();
                }

                if (jqueryarray[matchcount]['gender'] == 'M')
                {
                    $('#genderchk0').prop('checked', true);
                    $('#genderchk1').prop('checked', false);
                }
                else if (jqueryarray[matchcount]['gender'] == 'F')
                {
                    $('#genderchk0').prop('checked', false);
                    $('#genderchk1').prop('checked', true);
                }

                for (var j = 0; j < jqueryarray2.length; j++)
                {
                    if (jqueryarray[matchcount]['primaryfood'] == jqueryarray2[j]['feedbrandid'])
                    {
                        $('select[name=primaryfood] option:eq(' + jqueryarray2[j]['feedbrandid'] + ')').attr('selected', 'selected');
                        $('#feedbrandlink1').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[j]['link'] + '" target="_blank">Click on link to see pet primary food advisor.</a></u>');
                    }
                }
                for (var k = 0; k < jqueryarray2.length; k++)
                {
                    if (jqueryarray[matchcount]['secondaryfood'] == jqueryarray2[k]['feedbrandid'])
                    {
                        $('select[name=secondaryfood] option:eq(' + jqueryarray2[k]['feedbrandid'] + ')').attr('selected', 'selected');
                        $('#feedbrandlink2').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[k]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                    }
                }
                for (var l = 0; l < jqueryarray3.length; l++)
                {
                    if (jqueryarray[matchcount]['primarybreed'] == jqueryarray3[l]['breedid'])
                    {
                        $('select[name=primarybreed] option:eq(' + jqueryarray3[l]['breedid'] + ')').attr('selected', 'selected');
                    }
                }
                for (var m = 0; m < jqueryarray3.length; m++)
                {
                    if (jqueryarray[matchcount]['secondarybreed'] == jqueryarray3[m]['breedid'])
                    {
                        $('select[name=secondarybreed] option:eq(' + jqueryarray3[m]['breedid'] + ')').attr('selected', 'selected');
                    }
                }

                if (petmedicationadded[matchcount] != "")
                {
                    var jsonData = petmedicationadded[matchcount];
                    var medicines = [];
                    var medicationadded = {
                        'medication': []
                    };
                    $("#medicinetable1").append('<tr><th>Medicine</th><th>Frequency</th><th>Link</th><th>Reminder</th><th>Delete</th></tr>');
                    var cn = 0;
                    for (var c = 0; c < jsonData.length; c++)
                    {
                        var medobj = jsonData[c];
                        var medid = medobj.medicineid;
                        var link = medobj.url;
                        var freq = medobj.frequency;
                        var dosg = medobj.dosage;
                        var name = medobj.name;

                        var medtime = medobj.medicationtimings;
                        var freqtimes = medobj.frequencytimes;
                        medicationadded.medication.push({'medicineid': medid, 'frequency': freq, 'dosage': dosg});
                        cn++;

                        var reminders = medtime.split(",");
                        var remindersstr = "";
                        for (var j = 0; j < reminders.length; j++)
                        {
                            reminders1 = reminders[j].split(" ");
                            var H = +reminders1[1].substr(0, 2);
                            var h = H % 12 || 12;
                            var ampm = H < 12 ? "am" : "pm";
                            reminders1[1] = h + reminders1[1].substr(2, 3) + ampm;
                            remindersstr = remindersstr + reminders1[0] + " " + reminders1[1] + "<br>";
                        }

                        var dispfreq = "";
                        if (freq == "Day" || freq == "Week" || freq == "Month")
                            dispfreq = dosg + " Tablets  " + freqtimes + " times " + freq;
                        $("#medicinetable").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');
                        $("#medicinetable1").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');

                    }

                    $('#medicationaddedlabel').html("<lable>" + cn + " medications added" + "</label>");
                    if (cn == 0)
                    {
                        $("#medicinetable").hide();
                    }
                }

                if (petvaccinationadded[matchcount] != "")
                {
                    var jsonData = petvaccinationadded[matchcount];
                    var vaccinations = [];
                    var vaccinationadded = {
                        'vaccination': []
                    };
                    $("#vaccinationtable1").append('<tr><th>Vaccination</th><th>Due Date</th><th>Delete</th></tr>');
                    var cn = 0;
                    for (var c = 0; c < jsonData.length; c++)
                    {
                        var vacobj = jsonData[c];
                        var vacid = vacobj.vaccineid;
                        var d = new Date(vacobj.duedate);
                        var date = d.toDateString();
                        cn++;
                        $("#vaccinationtable").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');
                        $("#vaccinationtable1").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');

                    }
                    $('#vaccinationaddedlabel').html("<lable>" + cn + " vaccinations added" + "</label>");
                    if (cn == 0)
                    {
                        $("#vaccinationtable").hide();
                    }
                }
            }
        }
    });
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("profileimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("preview").src = oFREvent.target.result;
        };
    }

    var rowCount=1 ;
    var petFile  ;
    function addMoreRowsToAttachEdit(frm){
        petFile = petFile + rowCount;
        var medicalfile = $('#medicalfile').val();
        var recRow = '<tr id="rowCount'+rowCount+'">' +
            "<td><input type='file' multiple='' name='petFile[]' id='"+petFile+"'/>"+"</td>" +
            "<td><label><?php echo date('d-m-Y');?></label></td>" +
            '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td>'+
            '</tr>';
        jQuery('#addedRowsToAttachEdit').append(recRow);
        rowCount ++;
    }

    function removeRow(removeNum) {
        jQuery('#rowCount'+removeNum).remove();
    }
    function deletePetFile(petFileId,vendorId,petId){
        url_ = baseurl + 'vendor/customer/deletePetFile';
        data_ = "petFileId=" + petFileId+"&vendorId="+vendorId+"&petId="+petId;
        $.ajax({
            type: "post",
            url: url_,
            cache: false,
            data: data_,
            success: function (data) {
                var jsonObj = $.parseJSON(data);
                if (jsonObj.msg == 'File deleted successfully') {
                    $('#petRow'+petFileId).hide();
                    if(jsonObj.noOfFile==0){
                        $('#fileTable').hide();
                    }
                }
            }
        });
    }

    var vendorFile  ;
    function addMoreRowsToAttach(frm){
        vendorFile = vendorFile + rowCount;
        var recRow = '<tr id="rowCount'+rowCount+'">' +
            "<td><input type='file' multiple='' name='vendorFile[]' id='"+vendorFile+"'/>"+"</td>" +
            "<td><label><?php echo date('d-m-Y');?></label></td>" +
            '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td>'+
            '</tr>';
        jQuery('#addedRowsToAttach').append(recRow);
        rowCount ++;
    }
    function deleteVendorFile(vendorFileId,vendorId,petId){
        url_ = baseurl + 'vendor/customer/deleteVendorFile';
        data_ = "vendorFileId=" + vendorFileId+"&vendorId="+vendorId+"&petId="+petId;
        $.ajax({
            type: "post",
            url: url_,
            cache: false,
            data: data_,
            success: function (data) {
                var jsonObj = $.parseJSON(data);
                if (jsonObj.msg == 'File deleted successfully') {
                    $('#vendorRow'+vendorFileId).hide();
                    if(jsonObj.noOfFile==0){
                        $('#fileVendorTable').hide();
                    }
                }
            }
        });
    }

    var medcnt = 0;
    var vaccnt = 0;

    var medicines = [];
    var vaccinations = [];
    var selected_medicine = [];
    var selected_vaccination = [];
    var medicationadded = {
        'medication': []
    };

    var vaccinationadded = {
        'vaccination': []
    };

    $(document).ready(function () {
        $('#birthdate').datepicker({
            endDate: '+0d'
        });

        $('#duedate').datepicker({
            startDate: new Date()
        });
//        event.preventDefault();
        $('#isadopted').change(function () {
            if ($(this).is(":checked")) {
                $("#isadopted").val('1');
            }
        });
        $('#bdayunsure').change(function () {
            if ($(this).is(":checked")) {
                $('.adoptiondetail').show();
            }
            else
            {
                $('.adoptiondetail').hide();
            }

        });

        $('#addmorepets').click(function () {
            $('#selectpet li').removeClass("active");
            $('#feedbrandlink1').html('');
            $('#feedbrandlink2').html('');
            $('#petid').val("");
            $('#medicinetable tbody tr').remove();
            $('#medicinetable1 tbody tr').remove();
            $('#medicationaddedlabel').html("<lable>" + 0 + " medications added" + "</label>");
            $('#medicinefilestable tbody tr').remove();
            $('#vaccinationtable tbody tr').remove();
            $('#vaccinationtable1 tbody tr').remove();
            $('#vaccinationaddedlabel').html("<lable>" + 0 + " vaccinations added" + "</label>");
            $('input:radio[name="petageunit"]').prop("checked", false);
            $('#microchipped').prop('checked', false);
            $('#isadopted').prop('checked', false);
            $('#genderchk0').prop('checked', false);
            $('#genderchk1').prop('checked', false);
            $('#petname').val("");
            $('#microchipid').val("");
            $('#licensetag').val("");
            $('#licensetagissue').val("");
            $('#petcurrentweight').val("");
            $('#pettargetweight').val("");
            $('#birthdate').val("");
            $('#feedamount').val("");
            $('#feedfrequency').val("");
            $('#medicalhistory').val("");
            $('#currentphoto').hide();
            $('.chipid').hide();
            $('#currentimage').hide();
            $('#previewimage').show();
            $('#primaryfood option:selected').removeAttr('selected');
            $('#secondaryfood option:selected').removeAttr('selected');
            $('#primarybreed option:selected').removeAttr('selected');
            $('#secondarybreed option:selected').removeAttr('selected');
        });

        $(".adoptiondetail").hide();
        $("#loading").hide();
        var selCountry1 = "<?php echo $custdetail['country']; ?>";
        if (selCountry1 != "")
        {
            populateStates(selCountry1);
        }

        $("#country").on('change', function () {
            $('#state option').remove();
            $("#state").append('<option value="">Select State</option>');
            var selCountry = this.value;
            populateStates(selCountry);


        });
        function populateStates(selCountry)
        {
            var selState1 = "<?php echo $custdetail['state']; ?>";
            var jqueryarray = "";
            if (selCountry == "Canada")
                jqueryarray = <?php echo json_encode($Canadastates); ?>;
            else
                jqueryarray = <?php echo json_encode($USstates); ?>;
            for (var i = 0; i < jqueryarray.length; i++) {
                if (selState1.match(jqueryarray[i]))
                {
                    $('#state').append('<option value="' + jqueryarray[i] + '" selected>' + jqueryarray[i] + '</option>');
                }
                else
                {
                    $('#state').append('<option value="' + jqueryarray[i] + '">' + jqueryarray[i] + '</option>');
                }
            }
        }

        $('#currentimage').hide();
        $('#currentphoto').hide();

        $("#selectpet li").on('click', function (event) {
            event.preventDefault();
            $('#selectpet li').removeClass("active");
            $('#medicinetable tbody tr').remove();
            $('#medicinetable1 tbody tr').remove();
            $('#medicationaddedlabel').html("<lable>" + 0 + " medications added" + "</label>");
            $('#medicinefilestable tbody tr').remove();
            $('#vaccinationtable tbody tr').remove();
            $('#vaccinationtable1 tbody tr').remove();
            $('#vaccinationaddedlabel').html("<lable>" + 0 + " vaccinations added" + "</label>");
            $('input:radio[name="petageunit"]').prop("checked", false);
            $('#microchipped').prop('checked', false);
            $('#genderchk0').prop('checked', false);
            $('#genderchk1').prop('checked', false);
            $('#bdayunsure').prop('checked', false);
            $('#primaryfood option:selected').removeAttr('selected');
            $('#secondaryfood option:selected').removeAttr('selected');
            $('#primarybreed option:selected').removeAttr('selected');
            $('#secondarybreed option:selected').removeAttr('selected');
            $('#feedbrandlink1').html('');
            $('#feedbrandlink2').html('');
            $(".adoptiondetail").hide();
            $('#approxageyrs').val("");
            $('#approxagemonths').val("");
            $("#petage").val("");
            var index = $(this).index();
            $("#selectpet li:eq(" + index + ")").addClass('active');
            console.log(index);
            var petid = $(this).attr('value');
            var vendorid = $('#vendorid').val();
            // viewing vendorfile //
            url_ = baseurl + 'vendor/customer/getPetFile';
            data_ = "vendorId="+vendorid+"&petId="+petid;
            $.ajax({
                type: "post",
                url: url_,
                cache: false,
                data: data_,
                success: function (data) {
                    if(data==0){
                        $('#fileTable').hide();
                        $('#fileTableBody').html('')
                    }else{
                        $('#fileTable').show();
                        $('#fileTableBody').val(data);
                        $('#fileTableBody').html(data);
                    }

                }
            });
            console.log(petid);
            $('#petid').val(petid);
            var matchcount = 0;
            var jqueryarray = <?php echo json_encode($pets); ?>;
            var jqueryarray2 = <?php echo json_encode($feedbrands); ?>;
            var jqueryarray3 = <?php echo json_encode($breeds); ?>;
            var petmedicationadded = <?php echo json_encode($petmedication); ?>;
            var petvaccinationadded = <?php echo json_encode($petvaccination); ?>;

            for (var i = 0; i < jqueryarray.length; i++)
            {
                if (jqueryarray[i]['petid'] == petid)
                {
                    matchcount = i;
                    $('#petname').val(jqueryarray[matchcount]['name']);
                    $('#microchipped').val(jqueryarray[matchcount]['microchipped']);
                    $('#microchipid').val(jqueryarray[matchcount]['microchipid']);
                    $('#licensetag').val(jqueryarray[matchcount]['govtlicensetagno']);
                    $('#licensetagissue').val(jqueryarray[matchcount]['govtlicensetagissuer']);
                    $('#petcurrentweight').val(jqueryarray[matchcount]['currentweight']);
                    $('#pettargetweight').val(jqueryarray[matchcount]['targetweight']);
                    $('#isadopted').val(jqueryarray[matchcount]['isadopted']);
                    //$('#birthdate').val(jqueryarray[matchcount]['birthdate']);
                    var curr = jqueryarray[matchcount]['birthdate'];
                    if(curr != null){
                        var birthdate = $.datepicker.formatDate( "MM dd, yy", new Date(curr) );
                        $('#birthdate').val(birthdate);
                    }else {
                        $('#birthdate').val('');
                    } 
                    $('#feedamount').val(jqueryarray[matchcount]['amountoffeed']);
                    $('#feedfrequency').val(jqueryarray[matchcount]['frequencyoffeed']);
                    $('#medicalhistory').val(jqueryarray[matchcount]['medicalhistory']);

                    if (jqueryarray[matchcount]['proilefpicture'] != null)
                    {
                        $('#currentphoto').show();
                        $('#currentimage').attr('src', '<?php echo awsBucketPath ?>' + jqueryarray[matchcount]['proilefpicture']);
                        $('#currentimage').show();
                        $('#previewimage').hide();
                    }
                    else
                    {
                        $('#previewimage').show();
                        $('#currentimage').hide();
                        $('#currentphoto').hide();
                    }

                    if (jqueryarray[matchcount]['birthdayunsure'] == 1)
                    {
                        $('#bdayunsure').prop('checked', true);
                        $(".adoptiondetail").show();
                        var age = jqueryarray[matchcount]['approximateage'];
                        age = age.split('years');
                        $('#approxageyrs').val(age[0]);
                        age = age[1].split('months');
                        age[0] = age[0].split(',');
                        $('#approxagemonths').val(age[0]);
                    }
                    else
                    {
                        $('#bdayunsure').prop('checked', false);
                    }

                    if (jqueryarray[matchcount]['isadopted'] == 1)
                    {
                        $('#isadopted').prop('checked', true);
                    }
                    else
                    {
                        $('#isadopted').prop('checked', false);
                    }

                    if (jqueryarray[matchcount]['microchipped'] == 1)
                    {
                        $('#microchipped').prop('checked', true);
                        $('.chipid').show();
                    }
                    else
                    {
                        $('#microchipped').prop('checked', false);
                        $('.chipid').hide();
                    }

                    if (jqueryarray[matchcount]['gender'] == 'M')
                    {
                        $('#genderchk0').prop('checked', true);
                        $('#genderchk1').prop('checked', false);
                    }
                    else if (jqueryarray[matchcount]['gender'] == 'F')
                    {
                        $('#genderchk0').prop('checked', false);
                        $('#genderchk1').prop('checked', true);
                    }

                    for (var j = 0; j < jqueryarray2.length; j++)
                    {
                        if (jqueryarray[matchcount]['primaryfood'] == jqueryarray2[j]['feedbrandid'])
                        {
                            $('select[name=primaryfood] option:eq(' + jqueryarray2[j]['feedbrandid'] + ')').attr('selected', 'selected');
                            $('#feedbrandlink1').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[j]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                        }
                    }
                    for (var k = 0; k < jqueryarray2.length; k++)
                    {
                        if (jqueryarray[matchcount]['secondaryfood'] == jqueryarray2[k]['feedbrandid'])
                        {
                            $('select[name=secondaryfood] option:eq(' + jqueryarray2[k]['feedbrandid'] + ')').attr('selected', 'selected');
                            $('#feedbrandlink2').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[k]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                        }
                    }
                    for (var l = 0; l < jqueryarray3.length; l++)
                    {
                        if (jqueryarray[matchcount]['primarybreed'] == jqueryarray3[l]['breedid'])
                        {
                            $('select[name=primarybreed] option:eq(' + jqueryarray3[l]['breedid'] + ')').attr('selected', 'selected');
                        }
                    }
                    for (var m = 0; m < jqueryarray3.length; m++)
                    {
                        if (jqueryarray[matchcount]['secondarybreed'] == jqueryarray3[m]['breedid'])
                        {
                            $('select[name=secondarybreed] option:eq(' + jqueryarray3[m]['breedid'] + ')').attr('selected', 'selected');
                        }
                    }

                    if (petmedicationadded[matchcount] != "")
                    {
                        var jsonData = petmedicationadded[matchcount];
                        var medicines = [];
                        var medicationadded = {
                            'medication': []
                        };
                        $("#medicinetable1").append('<tr><th>Medicine</th><th>Frequency</th><th>Link</th><th>Reminder</th><th>Delete</th></tr>');
                        var cn = 0;
                        for (var c = 0; c < jsonData.length; c++)
                        {
                            var medobj = jsonData[c];
                            var medid = medobj.medicineid;
                            var link = medobj.url;
                            var freq = medobj.frequency;
                            var dosg = medobj.dosage;
                            var name = medobj.name;

                            var medtime = medobj.medicationtimings;
                            var freqtimes = medobj.frequencytimes;
                            medicationadded.medication.push({'medicineid': medid, 'frequency': freq, 'dosage': dosg});
                            cn++;

                            var reminders = medtime.split(",");
                            var remindersstr = "";
                            for (var j = 0; j < reminders.length; j++)
                            {
                                reminders1 = reminders[j].split(" ");
                                var H = +reminders1[1].substr(0, 2);
                                var h = H % 12 || 12;
                                var ampm = H < 12 ? "am" : "pm";
                                reminders1[1] = h + reminders1[1].substr(2, 3) + ampm;
                                remindersstr = remindersstr + reminders1[0] + " " + reminders1[1] + "<br>";
                            }

                            var dispfreq = "";
                            if (freq == "Day" || freq == "Week" || freq == "Month")
                                dispfreq = dosg + " Tablets  " + freqtimes + " times " + freq;
                            $("#medicinetable").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');
                            $("#medicinetable1").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td><td><a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medid + ');">Remove </a> </td></tr>');

                        }

                        $('#medicationaddedlabel').html("<lable>" + cn + " medications added" + "</label>");
                        if (cn == 0)
                        {
                            $("#medicinetable").hide();
                        }
                    }

                    if (petvaccinationadded[matchcount] != "")
                    {
                        var jsonData = petvaccinationadded[matchcount];
                        var vaccinations = [];
                        var vaccinationadded = {
                            'vaccination': []
                        };
                        $("#vaccinationtable1").append('<tr><th>Vaccination</th><th>Due Date</th><th>Delete</th></tr>');
                        var cn = 0;
                        for (var c = 0; c < jsonData.length; c++)
                        {
                            var vacobj = jsonData[c];
                            var vacid = vacobj.vaccineid;
                            var d = new Date(vacobj.duedate);
                            var date = d.toDateString();
                            cn++;
                            $("#vaccinationtable").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');
                            $("#vaccinationtable1").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vacid + ');">Remove </a> </td></tr>');

                        }
                        $('#vaccinationaddedlabel').html("<lable>" + cn + " vaccinations added" + "</label>");
                        if (cn == 0)
                        {
                            $("#vaccinationtable").hide();
                        }
                    }

                    break;
                }
            }
        });

        $("#update").click(function () {

            var fname = $("#customerfname").val();
            var lname = $("#customerlname").val();
            var email = $("#customeremail").val();
            var petname = $("#petname").val();
            var vetname = $("#vetname").val();
            var zipcode = $("#zipcode").val();
            var country = $("#country").val();
            var phoneno = $('#phoneno').val();
            var hasError = false;
            var errormsg = "";

            if (fname.trim().length == 0)
            {
                errormsg = 'Customer First Name is required!';
                $("#customerfname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (lname.trim().length == 0)
            {
                errormsg = 'Customer Last Name is required!';
                $("#customerlname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (email != '' && !validateEmail(email)) {
                errormsg = 'Enter valid Email ID';
                $("#customeremail").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (zipcode != '' && !validateZip(zipcode, country)) {
                errormsg = 'Enter valid Zipcode for customer';
                $("#zipcode").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (petname.trim().length == 0)
            {
                errormsg = 'Pet Name is required!';
                $("#petname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (phoneno != '')
            {
                var phone = $('#phoneno').val();
                var phoneNum = phone.replace(/[^\d]/g, '');
                if (phoneNum.length > 12 || phoneNum.length < 6)
                {
                    errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                    $("#phoneno").css('border-color', '#dd4b39');
                    hasError = true;
                }
            }
            if (fname.trim().length != 0)
            {
                $("#customerfname").css('border-color', '#d2d6de');
            }
            if (lname.trim().length != 0)
            {
                $("#customerlname").css('border-color', '#d2d6de');
            }
            if (petname.trim().length != 0)
            {
                $("#petname").css('border-color', '#d2d6de');
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }
            if (hasError == false) {

                $("#validerrorform").html('');
                //alert($("#addcustomerform").attr("action"));
                $("#addcustomerform").submit();
            }
            else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }
        });

        $(".allownumericwithoutdecimal").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            //if ((event.which < 48 || event.which > 57)) {
            // if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });
        
        $(".allownumericwithdecimal").on("keyup blur", function (e) {
            var val = $(this).val();
            if (isNaN(val)) {
                val = val.replace(/[^0-9\.]/g, '');
                if (val.split('.').length > 2)
                    val = val.replace(/\.+$/, "");
            }
            $(this).val(val);
        });

        $(".allowcharacterwithouthtmltags").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/(<([^>]+)>)/ig, ""));
            /*if((e.which > 64 && e.which < 91 ) || (e.which > 96 && e.which < 123)){
             event.preventDefault();
             }*/
        });

        function daydiff(first, second) {
            return Math.round((second - first) / (1000 * 60 * 60 * 24));
        }

        $('input:radio[name="petageunit"]').click(function () {
            var petdob = new Date($("#birthdate").val());
            if (($(this).is(':checked')) && ($(this).val() == 'years') && (petdob != "Invalid Date"))
            {
                console.log(petdob);
                var today = new Date();
                var age = today.getFullYear() - petdob.getFullYear();
                $("#petage").val(age);
            }
            else if (($(this).is(':checked')) && ($(this).val() == 'weeks') && (petdob != "Invalid Date"))
            {

                var today = new Date();
                var age = daydiff(petdob, today);
                age = Math.round((age / 7).toFixed(0));
                $("#petage").val(age);
            }
            else if (($(this).is(':checked')) && ($(this).val() == 'days') && (petdob != "Invalid Date"))
            {

                var today = new Date();
                var age = daydiff(petdob, today);
                $("#petage").val(age);
            }
            else
            {
                $("#petage").val("Pet Birthdate is required.");
            }

        });

        $('#sendInvite').click(function () {

            var email = $('#customeremail').val();

            if (email)
            {
                autoInvite(email);
            }
            else {

                alert('Kindly enter an Email address.');
            }

        });

        $('#srchmedicine').btsListFilter('#searchinput', {itemChild: 'span'});
        $('#srchvaccination').btsListFilter('#searchinput1', {itemChild: 'span'});

        $('.datepicker').datepicker({
            startDate: 'January 01, 1950',
            format: "MM dd, yyyy"
        });
        
        $('.clsDatePicker').datepicker({
            startDate: new Date(),
            format: "MM dd, yyyy"
        });

        $('[data-toggle="tooltip"]').tooltip();

        $("#tohoursofoperation").timepicker({showMeridian: false});
        $("#fromhoursofoperation").timepicker({showMeridian: false});

        $('.chipid').hide();
        $('#microchipped').change(function () {
            if ($(this).is(":checked")) {
                $('#microchipped').val('1');
                $('.chipid').show();
            }
            else
            {
                $('.chipid').hide();
            }

        });

        $('#deletePetFile').click(function (event) {
            var petFileId = $('#petFileId').val();
            url_ = baseurl + 'vendor/customer/deletePetFile';
            data_ = "petFileId=" + petFileId;
            $.ajax({
                type: "post",
                url: url_,
                cache: false,
                data: data_,
                success: function (data) {
                    if (data == 'File deleted successfully') {
                        $('#fileUploadDiv').show();
                        $('#fileTable').hide();
                    } else {
                        $('#fileUploadDiv').hide();
                    }
                }
            });
            event.preventDefault();
        });


        $('#updateInfo, #updateInfo_s1, #updateInfo_s2').click(function (event) {
            var fname = $("#customerfname").val();
            var lname = $("#customerlname").val();
            var email = $("#customeremail").val();
            var petname = $("#petname").val();
            var vetname = $("#vetname").val();
            var zipcode = $("#zipcode").val();
            var country = $("#country").val();
            var phoneno = $('#phoneno').val();
            var hasError = false;
            var errormsg = "";

            if (fname.trim().length == 0)
            {
                errormsg = 'The Customer First Name field is required!';
                $("#customerfname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (lname.trim().length == 0)
            {
                errormsg = 'The Customer Last Name field is required!';
                $("#customerlname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (email != '' && !validateEmail(email)) {
                errormsg = 'The Email field must contain a valid email address';
                $("#customeremail").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (zipcode != '' && !validateZip(zipcode, country)) {
                errormsg = 'Enter valid Zipcode for customer';
                $("#zipcode").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (petname.trim().length == 0)
            {
                errormsg = 'The Pet Name field is required!';
                $("#petname").css('border-color', '#dd4b39');
                hasError = true;
            }
            else if (phoneno != '')
            {
                var phone = $('#phoneno').val();
                var phoneNum = phone.replace(/[^\d]/g, '');
                if (phoneNum.length > 12 || phoneNum.length < 6)
                {
                    errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                    $("#phoneno").css('border-color', '#dd4b39');
                    hasError = true;
                }
            }
            if (fname.trim().length != 0)
            {
                $("#customerfname").css('border-color', '#d2d6de');
            }
            if (lname.trim().length != 0)
            {
                $("#customerlname").css('border-color', '#d2d6de');
            }
            if (petname.trim().length != 0)
            {
                $("#petname").css('border-color', '#d2d6de');
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }
            if (hasError == false) {
                $("#validerrorform").html('');
                //alert($("#addcustomerform").attr("action"));
                $("#addcustomerform").submit();
            }
            else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }

        });

        //form validation step by step

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('bg-orange').addClass('btn-default');
                $item.addClass('bg-orange');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            // Start of Appointment add/update form validation //
            var hasError = false;
            var errormsg = "";
            var step = $(this).attr('value');
            var customerid = $('#customerid').val();
            var fname = $("#customerfname").val();
            var lname = $("#customerlname").val();
            var email = $("#customeremail").val();
            var petname = $("#petname").val();
            var vetname = $("#vetname").val();
            var zipcode = $("#zipcode").val();
            var country = $("#country").val();
            var phoneno = $('#phoneno').val();
            var address = $("#address").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var notes = $("#notes").val();
            var licensetag = $("#licensetag").val();
            var licensetagissue = $("#licensetagissue").val();
            var validText = /(<([^>]+)>)/ig;

            switch (step) {
                case "step-1":
                    //alert("step-1");
                    if (fname.trim().length == 0)
                    {
                        errormsg = 'The Customer First Name field is required!';
                        $("#customerfname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (validText.test(fname))
                    {
                        errormsg = 'No html tags are allowed!';
                        $("#customerfname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (lname.trim().length == 0)
                    {
                        errormsg = 'The Customer Last Name field is required!';
                        $("#customerlname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (validText.test(lname))
                    {
                        errormsg = 'No html tags are allowed!';
                        $("#customerlname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (email != '' && !validateEmail(email)) {
                        errormsg = 'The Email field must contain a valid email address';
                        $("#customeremail").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (phoneno != '')
                    {
                        var phone = $('#phoneno').val();
                        var phoneNum = phone.replace(/[^\d]/g, '');
                        if (phoneNum.length > 12 || phoneNum.length < 6)
                        {
                            errormsg = 'Kindly enter correct phone number between 6 to 10 digits!';
                            $("#phoneno").css('border-color', '#dd4b39');
                            hasError = true;
                            isValid = false;
                        }
                    }
                    else if (validText.test(address))
                    {
                        errormsg = 'No html tags are allowed!';
                        $("#address").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (validText.test(city))
                    {
                        errormsg = 'No html tags are allowed!';
                        $("#city").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (zipcode != '' && !validateZip(zipcode, country)) {
                        errormsg = 'Enter valid Zipcode for customer';
                        $("#zipcode").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    if (fname.trim().length != 0 && !(validText.test(fname)))
                    {
                        $("#customerfname").css('border-color', '#d2d6de');
                    }
                    if (lname.trim().length != 0 && !(validText.test(lname)))
                    {
                        $("#customerlname").css('border-color', '#d2d6de');
                    }
                    if (email != '' && validateEmail(email)) {

                        $("#customeremail").css('border-color', '#d2d6de');
                    }
                    if (address.trim().length == 0 || !(validText.test(address)))
                    {
                        $("#address").css('border-color', '#d2d6de');
                    }
                    if (city.trim().length == 0 || !(validText.test(city)))
                    {
                        $("#city").css('border-color', '#d2d6de');
                    }

                    if (isValid) {
                        if ((fname_init != fname || lname_init != lname || email_init != email || city_init != city || notes_init != notes
                            || zipcode_init != zipcode || country_init != country || phoneno_init != phoneno || address_init != address)) {
                            $.ajax({
                                url: baseurl + 'vendor/customer/ajax_update_customer/1',
                                type: "post",
                                data: {customerid: customerid, fname: fname, lname: lname, email: email, city: city, notes: notes,
                                    zipcode: zipcode, country: country, phoneno: phoneno, address: address, state: state}
                            }).done(function (response) {
                                if (response == 'success') {
                                    fname_init = fname, lname_init = lname, email_init = email, city_init = city, notes_init = notes,
                                        zipcode_init = zipcode, country_init = country, phoneno_init = phoneno, address_init = address;
                                }
                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // Log the error to the console
                                console.error(
                                    "The following error occurred: " +
                                    textStatus, errorThrown
                                );
                            });
                        }
                    }
                    break;
                case "step-2":
                    //alert("step-2");
                    if (petname.trim().length == 0)
                    {
                        errormsg = 'The Pet Name field is required!';
                        $("#petname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }
                    else if (validText.test(petname))
                    {
                        errormsg = 'No html tags are allowed!';
                        $("#petname").css('border-color', '#dd4b39');
                        hasError = true;
                        isValid = false;
                    }

                    if (petname.trim().length != 0 && !(validText.test(petname)))
                    {
                        $("#petname").css('border-color', '#d2d6de');
                    }

                    break;

            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
                $("#validerrorform").html(errormsg);
            } else {
                $('#validation').hide();
                $("#validerrorform").html('');
            }

            // End of Appointment add/update form validation //

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                    alert(curInputs[i].val());
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.bg-orange').trigger('click');

    });


    function showLocalVets()
    {

        $("#loading").show();
        var url = "<?php echo base_url(); ?>vendor/customer/getRegionalVets";
        $.post(url, function (data)
        {
            $("#loading").hide();
            $("#vetlist").html('');

            $("#vetlist").html(data);
            $('#vetlookup').modal('show');
        });

    }

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }

    function validateZip(zipcode, country)
    {
        if (country == "Canada")
        {
            var zipReg = /^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/;
            return zipReg.test(zipcode);
        }
        else
        {
            if (zipcode.length != 5)
                return false;
            else
            {
                var intRegex = /^\d+$/;
                return intRegex.test(zipcode);
            }
        }

    }

    function addReminders()
    {
        var d = new Date();

        var currDate = d.getDate();
        var currMonth = d.getMonth();
        var currYear = d.getFullYear();

        var dateStr = /*currYear+ "-"+currMonth+ "-"+currDate;*/new Date().toJSON().slice(0, 10);

        var freqtimes = $("#frequencytimes").val();
        var freq = $("#frequency").val();

        if (freqtimes != "" && freq != "")
        {
            $("#remindertable").html('');

            if (freq == "Day")
            {
                for (var i = 0; i < freqtimes; i++)
                {

                    var timeid = 'remindertime_' + i;
                    $("#remindertable").append('<tr><td>Reminder</td><td><div class="input-group"><input id="' + timeid + '"type="text" class="form-control  time ui-timepicker-input"></div></td></tr>');
                    $("#" + timeid).timepicker({showMeridian: false});
                    $("#" + timeid).timepicker('setTime', new Date());
                    $("#" + timeid).keydown(function (e)
                    {
                        e.preventDefault();
                    });
                }
            }
            else if (freq == "Week" || freq == "Month")
            {
                for (var i = 0; i < freqtimes; i++)
                {

                    var timeid = 'remindertime_' + i;
                    var dateid = 'reminderdate_' + i;
                    $("#remindertable").append('<tr><td>Reminder</td><td><input type="text"  class="form-control clsDatePicker "  id="' + dateid + '" data-date-format="MM dd, yyyy" /></td><td><div class="input-group"><input id="' + timeid + '"type="text" class="form-control  time ui-timepicker-input"></div></td></tr>');
                    $("#" + dateid).datepicker({
                        startDate: new Date(),
                        autoclose: true
                    });
                    $("#" + dateid).datepicker('setDate', new Date());
                    $("#" + dateid).keydown(function (e)
                    {
                        e.preventDefault();
                    });

                    $("#" + timeid).timepicker({showMeridian: false});
                    $("#" + timeid).timepicker('setTime', new Date());
                    $("#" + timeid).keydown(function (e)
                    {
                        e.preventDefault();
                    });
                    //$("#"+dateid).val(dateStr);
                    // $(".clsDatePicker").datepicker("refresh");
                }
            }
        }
    }

    function getMedicine(medicineid, medicinename, medicinelink)
    {
        $("#medicine").val(medicineid);
        $("#medicinename").val(medicinename);
        $("#medicinelink").val(medicinelink);
        $('#searchinput').val(medicinename);
    }

    function getVaccination(vaccineid, vaccinename)
    {

        $("#vaccine").val(vaccineid);
        $("#vaccinename").val(vaccinename);
        $('#searchinput1').val(vaccinename);
    }

    function addMedication()
    {

        var medicineid = $("#medicine").val();
        var medicinelink = $("#medicinelink").val();
        var frequency = $("#frequency").val();
        var dosage = $("#dosage").val();
        var frequencytimes = $("#frequencytimes").val();
        //var meditime = $("#medtime").val();

        var meditime = "";
        var meditime1 = new Date().toJSON().slice(0, 10);

        // var medicinename=$("#medicine option:selected").text();
        var medicinename = $("#medicinename").val();
        if ($('#searchinput').val() == '')
        {
            $("#validerror").html('Pleas select medication name.');
        } else if (frequency == '')
        {
            $("#validerror").html('Please select regularity value.');
        } else if (dosage == '')
        {
            $("#validerror").html('Please specify dosage value.');
        } else if (frequencytimes == '')
        {
            $("#validerror").html('Please select number of times value.');
        } else
        {
            $("#validerror").html('');
            $("#medicine").val('');
            $("#medicinelink").val('');
            $("#frequency").val('');
            $("#frequencytimes").val('');
            $("#dosage").val('');
            $('#searchinput').val('');

            var dispfreq = "";
            if (frequency == "Day" || frequency == "Week" || frequency == "Month")
            //dispfreq=dosage+" Tablets at "+meditime+" "+frequency;
                dispfreq = dosage + " dose(s)  " + frequencytimes + " times per " + frequency;
            // else
            //dispfreq=dosage+" Tablets at "+meditime+" every "+frequency;

            if (frequency == "Day")
            {

                for (var i = 0; i < frequencytimes; i++)
                {

                    var timeid = 'remindertime_' + i;
                    var time = getDateIn24($("#" + timeid).val());
                    if (meditime == "")
                        meditime = meditime1 + " " + time;
                    else
                        meditime = meditime + "," + meditime1 + " " + time;

                }
            }
            else if (frequency == "Week" || frequency == "Month")
            {
                for (var i = 0; i < frequencytimes; i++)
                {
                    var timeid = 'remindertime_' + i;
                    var time = getDateIn24($("#" + timeid).val());
                    var dateid = 'reminderdate_' + i;
                    if (meditime == "")
                        meditime = $("#" + dateid).val() + " " + time;
                    else
                        meditime = meditime + "," + $("#" + dateid).val() + " " + time;
                }

            }
            var reminders = $.trim(meditime).split(",");
            var remindersstr = "";
            if(reminders.length > 1){
                for (var i = 0; i < reminders.length; i++)
                {
                    reminders1 = reminders[i].split(" ");
                    var H = +reminders1[1].substr(0, 2);
                    var h = H % 12 || 12;
                    var ampm = H < 12 ? "am" : "pm";
                    reminders1[1] = h + reminders1[1].substr(2, 3) + ampm;
                    remindersstr = remindersstr + reminders1[0] + " " + reminders1[1] + "<br>";
                }
            }
            if (medcnt == 0)
            {
                $("#medicinetable11").append('<tr><th>Medicine</th><th>Frequency</th><th>Link</th><th>Reminder</th><th>Delete</th></tr>');
            }
            if ($.inArray(medicineid, selected_medicine) ===  -1) {
                $("#medicinetable").append('<tr id="' + medicineid + '"><td>' + medicinename + '</td><td>' + dispfreq + '</td><td> <a href="' + medicinelink + '" target="_blank">Link</a></td><td> <a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medicineid + ');">Remove </a> </td></tr>');
                $("#medicinetable1").append('<tr id="' + medicineid + '"><td>' + medicinename + '</td><td>' + dispfreq + '</td><td> <a href="' + medicinelink + '" target="_blank">Link</a></td><td> <a href="#" data-toggle="tooltip" data-html="true" title="' + remindersstr + '">Reminders</a></td><td><a href="#" onclick="javascript:removeMedication(' + medicineid + ');">Remove </a> </td></tr>');
                medicationadded.medication.push({'medicineid': medicineid, 'frequency': frequency, 'dosage': dosage, 'medicationtime': meditime, 'medicationtimings': meditime, 'frequencytimes': frequencytimes});
                medcnt++;
                selected_medicine.push(medicineid);
            } else {
                $("#validerror").html('Warning, This medication has already been added for this pet.');
            }
            $("#remindertable").html('');
        }
    }

    function addVaccination()
    {

        var vaccineid = $("#vaccine").val();
        var info = "-";

        // var medicinename=$("#medicine option:selected").text();
        var vaccinename = $("#vaccinename").val();
        var duedate = $("#duedate").val();
        if ($('#searchinput1').val() == '')
        {

            $("#validerror2").html('Pleas select Vaccine name');
        }
        else if (duedate == '')
        {
            $("#validerror2").html('Pleas Select Due Date');
        }
        else
        {
            $("#validerror2").html('');
            $("#vaccine").val('');
            $('#searchinput1').val('');
            if (vaccnt == 0)
            {
                $("#vaccinationtable1").append('<tr><th>Vaccination</th><th>Due Date</th><th>Delete</th></tr>');
            }
            if ($.inArray(vaccineid, selected_vaccination) ===  -1) {
            $("#vaccinationtable").append('<tr id="' + vaccineid + '"><td>' + vaccinename + '</td><td>' + duedate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vaccineid + ');">Remove </a> </td></tr>');
            $("#vaccinationtable1").append('<tr id="' + vaccineid + '"><td>' + vaccinename + '</td><td>' + duedate + '</td><td><a href="#" onclick="javascript:removeVaccination(' + vaccineid + ');">Remove </a> </td></tr>');

            vaccinationadded.vaccination.push({'vaccineid': vaccineid, 'duedate': duedate});
            vaccnt++;
            selected_vaccination.push(vaccineid);
            } else {
                $("#validerror2").html('Warning, This vaccination has already been added for this pet.');
            }
        }
    }

    function getDateIn24(time)
    {
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = (time.match(/:(.*)$/)[1]).substr(2, 4);
        if (AMPM == "pm" && hours < 12)
            hours = hours + 12;
        if (AMPM == "am" && hours == 12)
            hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10)
            sHours = "0" + sHours;
        if (minutes < 10)
            sMinutes = "0" + sMinutes;
        return (sHours + ":" + sMinutes);
    }

    function removeMedication(removemedid)
    {

        var confrm = confirm('Are you sure you want to remove this medication?');
        if (confrm == true)
        {
            for (var i = 0, l = medicationadded.medication.length; i < l; i++) {
                var obj = medicationadded.medication[i];

                //alert(obj['medicineid']);
                if (obj['medicineid'] == removemedid)
                {
                    medicationadded.medication.splice(i, 1);
                    break;
                }
            }
            // alert(JSON.stringify( medicationadded));
            //alert(medicationadded.medication.length);

            $('#medicinetable tr#' + removemedid).remove();
            $('#medicinetable1 tr#' + removemedid).remove();
            return true;
        } else
        {
            event.preventDefault();
            return false;
        }

    }

    function removeVaccination(removemedid)
    {

        var confrm = confirm('Are you sure you want to remove this vaccination?');
        if (confrm == true)
        {
            for (var i = 0, l = vaccinationadded.vaccination.length; i < l; i++) {
                var obj = vaccinationadded.vaccination[i];

                //alert(obj['medicineid']);
                if (obj['vaccineid'] == removemedid)
                {
                    vaccinationadded.vaccination.splice(i, 1);
                    break;
                }
            }
            // alert(JSON.stringify( medicationadded));
            //alert(medicationadded.medication.length);
            $('#vaccinationtable tr#' + removemedid).remove();
            $('#vaccinationtable1 tr#' + removemedid).remove();
            return true;
        } else
        {
            event.preventDefault();
            return false;
        }

    }

    function addMedicationDone()
    {
        var totalmedadded = medcnt;

        $('#medicinesadded').val(JSON.stringify(medicationadded));
        //$('input[name="medicinesadded[]"]').val(medicines);
        $('#searchinput').val('');
        //$('#medicationaddedlabel').html("<lable><b>"+ medicationadded.medication.length+" medications added"+"</b></label>");
        $('#medication').modal('hide');
    }

    function addVaccinationDone()
    {
        var totalvacadded = vaccnt;

        $('#vaccinesadded').val(JSON.stringify(vaccinationadded));
        //$('input[name="medicinesadded[]"]').val(medicines);
        $('#searchinput').val('');
        //$('#vaccinationaddedlabel').html("<lable><b>"+ vaccinationadded.vaccination.length+" vaccinations added"+"</b></label>");
        $('#vaccination').modal('hide');
    }

    function autoInvite(email)
    {
        if (validateEmail(email))
        {
            var url = "<?php echo base_url(); ?>vendor/customer/checkInvitedUser";

            $.post(url, {email: email}, function (data)
            {
                if (data == 'existinguser')
                {
                    profileImportInvite(email, false);

                }
                else if (data == 'newuser')
                {
                    inviteUser(email, false);
                }

            });
        }
        else {
            alert('Kindly enter a valid Email address.');
        }
    }


    function inviteUser(email, auto)
    {

        if (email == '')
        {

            $("#validerror").html('Enter Email ID');

        }
        else if (!validateEmail(email)) {
            $("#validerror").html('Enter valid Email ID');

        }
        else
        {
            $("#validerror").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/inviteUser";

            $.post(url, {email: email}, function (data)
            {
                if (!auto)
                {
                    if (data == '1')
                    {
                        $("#inviteEmail").val('');
                        alert("Invitation mail sent successfully to " + email);
                    }
                    else
                    {
                        alert("Some problem occured while sending mail!");
                    }
                }

            });
        }
    }

    function profileImportInvite(email, auto)
    {

        if (email == '')
        {

            $("#validerror1").html('Enter Email ID');

        }
        else if (!validateEmail(email)) {
            $("#validerror1").html('Enter valid Email ID');

        }
        else
        {
            $("#validerror1").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/inviteUserForProfileImport";

            $.post(url, {email: email}, function (data)
            {
                if (!auto)
                {
                    if (data == '1')
                    {
                        $("#inviteEmailProfImport").val('');
                        alert("Invitation mail sent successfully to " + email);
                    }
                    else if (data == '2')
                    {
                        alert("BabelBark User with this email id doen't exist!");
                    }
                    else if (data == '3')
                    {
                        alert("You have already sent the invitation to this user!");
                    }
                    else
                    {
                        alert("Some problem occured while sending mail!");
                    }
                }

            });
        }

    }


</script>