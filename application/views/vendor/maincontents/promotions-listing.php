<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Promotions

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Promotions</a></li>
            <li class="active">Promotions Listing</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content" style="min-height: 80px;">
        <div class="row">

            <br><br>
            <div class=" col-xs-12 ">
                <div class="form-group">
<!--                    <div class="row">
                        <div class=" col-xs-12 ">

                            <a href="<?php echo base_url(); ?>vendor/promotions/redeemrequests"> <h4><u>Promotion Redemption</u></h4></a></div>
                    </div>-->
                    <!--  	<div class="col-sm-4">	
                    
                               <select class="form-control" name="promocode" id="promocode">
       <option value="">Select Promocode</option>
                    <?php for ($i = 0; $i < count($promocodes); $i++) {
                        ?>
                                                   <option value="<?php echo $promocodes[$i]['promotionid']; ?>" ><?php echo $promocodes[$i]['promocode']; ?></option>
                    <?php } ?>

 </select>
                            </div>
                            
             <div class="row">
                             <div class="col-sm-4">
   
     <select class="form-control" name="selectuser" id="selectuser">
       <option value="">Select BabelBark User</option>
                    <?php
                    for ($i = 0; $i < count($appusers); $i++) {
                        $username = $appusers[$i]['firstname'] . " " . $appusers[$i]['lastname'];
                        ?>
                                                   <option value="<?php echo $appusers[$i]['appuserid']; ?>" ><?php echo $username; ?></option>
                    <?php } ?>

 </select>
 </div>
                            <div class="col-sm-2"> <button class="btn btn-warning" name='submit'  onclick="javascript:showPromoInfo();">Redeem</button></div></div>
                            <div class="col-sm-2"> <label class="text-red" id="validerror"></label></div> -->
                </div>			
            </div>
        </div>
    </section>
    <br><br> 
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title" style="margin-right:300px; margin-top:10px;">Promotions</h3></center>
                        <?php
                        if ($userrole == "admin") {
                            ?>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="<?php echo base_url(); ?>vendor/promotions/addPromotions">Add Promotion</a>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div><!-- /.box-header -->
                    <?php if ($this->session->flashdata('error_message')) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            <?php echo $this->session->flashdata('error_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success_message')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>	<i class="icon fa fa-check"></i> Success</h4>
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if (count($promotions) > 0) { ?>
                        <div class="box-body table-responsive"  style="padding: 10px 15px;">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">PromoCode</th>

                                        <th>Promotion name</th>
                                        <th>Target Population</th>
<!--                                        <th>Advert Locations</th>-->
                                        <th>Promotion Details</th>
                                        <?php
                                        if ($userrole == "admin") {
                                            ?>
                                            <th>Status</th>
                                            <th>Delete</th>
                                        <?php } else { echo '<th></th><th></th>'; }  ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    #var_dump($promotions);
                                    ?>
                                    <?php foreach ($promotions as $row) { ?>
                                        <tr id="<?php echo $row['promotionid']; ?>">
                                            <td><?php echo $row['promocode']; ?></td>
                                            <td><?php echo $row['name']; ?> </td>
                                            <td><?php echo $row['agerangefrom'] . "-" . $row['agerangeto']; ?></td>
<!--                                            <td><?php echo $row['advertlocations']; ?></td>-->

                                            <td><center><a href="<?php echo base_url() . 'vendor/promotions/viewDetails/' . $row['promotionid']; ?>"><i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i> </a></center>  </td>
                                    <?php
                                    if ($userrole == "admin") {
                                        ?>
                                        <td>
                                            <?php
                                            if ($row['isactive'] == 1) {
                                                echo anchor('vendor/promotions/changeStatus/' . $row['promotionid'] . '/0', '<span class="label label-success button disabled">Active</span>', 'title="Click to deactivate"');
                                            } else {
                                                echo anchor('vendor/promotions/changeStatus/' . $row['promotionid'] . '/1', '<span class="label label-danger">Inactive</span>', 'title="Click to activate"');
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo anchor('vendor/promotions/deletePromotion/' . $row['promotionid'], '<i class="fa fa-trash fa-lg"></i>', 'title="Click to delete" class="cnf-del-custom"'); ?>
                                        </td>
                                    <?php } else { echo '<td></td><td></td>'; } ?>                                    
                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <!-- <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            foreach ($links as $link) {
                                echo "<li>" . $link . "</li>";
                            }
                            ?>
                            </ul> -->
                        <?php } else { ?>
                            <div align="center"><h1>No data available !</h1></div><br /><br />
                        <?php } ?>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->

        </div><!-- /.row -->
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="generic" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Promotion Redemption</h4>
                <div id="modalcontentdiv">

                </div>
            </div>

        </div>
    </div>
</div>




<script type="text/javascript">



    $(document).ready(function () {
        $('#example3').DataTable({bFilter: false,
            bInfo: false,
            order: [],
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0, 2, 3, 4, 5]
                }]
        });
        $(".cnf-del-custom").on("click", function (e) {
            var confrm = confirm('Are you sure ? \n Deleting the promotion will also delete all data related to the promotion.');
            if (confrm == true)
            {
                return true;
            } else
            {
                e.preventDefault();
                return false;
            }
        });




    });

    function showPromoInfo()
    {

        var promotionid = $("#promocode").val();
        var appuserid = $("#selectuser").val();

        if (promotionid == '')
        {

            $("#validerror").html('Select Promocode');
        } else if (appuserid == '')
        {
            $("#validerror").html('Select User');
        } else
        {
            $("#validerror").html('');
            var url = "<?php echo base_url(); ?>vendor/promotions/fetchPromotionInfo";

            $.post(url, {promotionid: promotionid, appuserid: appuserid}, function (data)
            {
                if (data == "promotionRedeemedAlready")
                {
                    alert('Promocode has been already redeemed for this user');
                } else if (data == "promotionCapacityExceeded")
                {
                    alert('Promocode has exceeded its capcity');
                } else
                {
                    $("#modalcontentdiv").html(data);
                    $('#generic').modal('show');
                }
            });
        }
    }

    function confirmPromocode(promotionid, promocode, appuserid)
    {

        var url = "<?php echo base_url(); ?>vendor/promotions/confirmPromocode";

        $.post(url, {promotionid: promotionid, promocode: promocode, appuserid: appuserid}, function (data)
        {


            $("#promocode").val('');
            $("#selectuser").val('');
            $('#generic').modal('hide');

            if (data == '1')
            {
                alert('Promocode redempted successfully!');
            } else
            {
                alert("Some problem occured!");
            }
        });

    }





</script>
