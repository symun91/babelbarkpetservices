<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<style>
    .box{
        border-top: 0 !important;
    }
    #updateInfo_s1, #updateInfo_s2, .close_s1 {
        margin-right: 2%;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($appuserdetail['appuserid'] == 0)
                echo "Add Babelbark User";
            else
                echo "Babelbark User Detail";
            ?>
        </h1>
        <?php if ($appuserdetail['appuserid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customers</a></li>
                <li class="active">Add Customers</a></li>
            </ol>
        <?php } ?>
    </section>
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <label class="text-red"><?php echo validation_errors(); ?></label>
        </div>
        <!--        <form role="form" action="<?php echo base_url(); ?>vendor/customer/uploadPetFile" method="POST" name="settingsform" enctype="multipart/form-data">-->
        <form name="addcustomerform" id="addcustomerform"  action="<?php echo base_url(); ?>vendor/customer/updateappCustomer" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="vendorid" id="vendorid" value="<?php echo $admin['adminDetails']['vendorid']; ?>"/>
            <input type="hidden" name="customerid" id="customerid" value="<?php echo $appuserdetail['appuserid']; ?>"/>
            <input type="hidden" name="petid" id="petid"/>
            <div class="box">
                <center><h1>Customer Details</h1></center>
                <div class="row">
                    <div class="col-md-12">
                        <div class="stepwizard col-md-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                    <p>Customer Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle">2</a>
                                    <p>Pet Details</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle">3</a>
                                    <p>Veterinarian Details</p>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 start ##########################################-->
                        <div class="row setup-content" id="step-1">
                            <div class="col-lg-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body" id="box-body">
                                    <div class="col-lg-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label for="">Customer First Name</label>
                                            <input id="customerfname" name="customerfname" class="form-control" type="text" placeholder="Enter first  name" value="<?php echo set_value('customerfname', $appuserdetail['firstname']); ?>" maxlength="200">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Customer Last Name</label>
                                            <input id="customerlname" name="customerlname" class="form-control" type="text" placeholder="Enter last  name" value="<?php echo set_value('customerlname', $appuserdetail['lastname']); ?>" maxlength="200">
                                        </div>
                                        <div class="form-group">
                                            <label for="targetpopulation">Email Address</label>
                                            <input id="customeremail" class="form-control" type="email" placeholder="Email Address" name="customeremail" maxlength="200" value="<?php echo set_value('customeremail', $appuserdetail['email']); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Phone Number</label>
                                            <input class="form-control" placeholder="Enter Phone Number" id="phoneno" name="phoneno" type="number" value="<?php echo set_value('phoneno', $appuserdetail['phonenumber']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label for="">Addresss</label>
                                            <input id="address" class="form-control" type="text" placeholder="Enter address" name="address" maxlength="200" value="<?php echo set_value('address', $appuserdetail['address']); ?>">
                                            <div class="row" style="margin-top: 15px;">
                                                <div class="col-sm-6"> <input id="city" class="form-control" type="text" placeholder="Enter city" name="city" maxlength="45" value="<?php echo set_value('city', $appuserdetail['city']); ?>"></div>
                                                <div class="col-sm-6"> <input id="state" class="form-control" type="text" placeholder="Enter state" name="state" maxlength="45" value="<?php echo set_value('state', $appuserdetail['state']); ?>"></div>
                                            </div>
                                            <div class="row" style="margin-top: 15px;">
                                                <div class="col-sm-6"> <input id="zipcode" class="form-control" type="text" placeholder="Enter zipcode" name="zipcode" maxlength="15" value="<?php echo set_value('zipcode', $appuserdetail['zip']); ?>"></div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="customer-notes">
                                            <label>Notes</label>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control countable" placeholder="Notes" name="user_notes" data-length="1500"><?php echo $appuserdetail['notes']; ?></textarea>
                                                </div>
                                                <div class="col-xs-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <div class="box-body" style="padding-left: 0px;">
                                    <p style="border: 1px solid #C2C2C2"></p>
                                    <div class="col-lg-5 col-xs-5">
                                        <table class="table" id="fileUploadTable">
                                            <thead style="background-color:#EEEEEE;">
                                            <th><label>Select file<font color="red">*</font></label></th>
                                            <th><label>Upload Date</label></th>
                                            <th><td><a onclick="addMoreRowsToAttach(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                            </thead>
                                            <tbody id="addedRowsToAttach">

                                            </tbody>
                                        </table>
                                        <div class="alert alert-success">
                                            <strong>Uploaded files are only saved upon hitting "Save"</strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-xs-7" style="padding-left: 25px">
                                        <?php
                                        if (count($vendorFileInfo) > 0) {
                                            ?>
                                            <table class="table" id="fileVendorTable">
                                                <thead style="background-color:#EEEEEE;">
                                                <th>File Name</th>
                                                <th>Upload Date</th>
                                                <th>View File</th>
                                                <th>Delete File</th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($vendorFileInfo as $vendorRows) { ?>
                                                        <tr id="vendorRow<?php echo $vendorRows['id']; ?>">
                                                            <td><?php echo $vendorRows['filename']; ?></td>
                                                            <td><?php echo $vendorRows['updatedon']; ?></td>
                                                            <td><a href="<?php echo base_url() . 'view_vendor_file/' . $vendorRows['id']; ?>" target="_blank">View</td>
                                                            <td>
                                                                <a style="cursor: pointer" onclick="javascript:deleteVendorFile('<?php echo $vendorRows['id']; ?>', '<?php echo $vendorRows['vendorid']; ?>', '<?php echo $vendorRows['petid']; ?>')">
                                                                    <span class="label label-danger">Delete</span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="col-lg-6 col-xs-6"></div>
                                        <div class="col-lg-6 col-xs-6">
                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                                    <?php if ($appuserdetail['appuserid'] != 0) { ?>
                                                        <button class="btn btn-warning pull-right" id="updateInfo_s1">Save</button>
                                                        <?php echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"'); ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--############################# STEP 1 end ##########################################-->
                        <!--############################# STEP 2 start ##########################################-->
                        <div class="row setup-content" id="step-2">
                            <div class="col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="">Choose Pet</label>
                                            <ul class="nav nav-pills" name="selectpet" id="selectpet">
                                                <?php for ($i = 0; $i < count($pets); $i++) { ?>
                                                    <li value="<?php echo $pets[$i]['petid']; ?>"><a href="#"><?php echo $pets[$i]['name']; ?></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body" id="box-body1">
                                    <div class="col-lg-12" style="border-bottom: 1px solid #C2C2C2">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="">Pet Name</label>
                                                    <input id="petname" name="petname" class="form-control" type="text" placeholder="Enter pet name" maxlength="100">
                                                </div>
                                            </div>
                                            <div class="row"  id="pet-notes">
                                                <div class="form-group">
                                                    <label>Notes</label>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control countable" placeholder="Notes" name="pet_notes" id="petnotes" data-length="1500"></textarea>
                                                        </div>
                                                        <div class="col-xs-6"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="">Photo (optional)</label>
                                                    <input class="form-control" type="file"  >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-5"><img src=" " id="profileimage" name="profileimage" width="150" height="150"/></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-md-12" style="margin-top: 5px;">
                                        <div class="form-group">
                                            <label for="">Is Pet Microchipped</label>
                                            <input type="checkbox" id="microchipped" name="microchipped" value="1" />
                                        </div>
                                        <div class="chipid">
                                            <div class="form-group">
                                                <label>Pet Microchip ID</label>
                                                <input type="text" class="form-control" placeholder="Enter Pet Microchip ID" id="microchipid" name="microchipid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Number</label>
                                            <input id="licensetag" type="text" class="form-control" placeholder="Enter Pet License Tag Number" name="govtlicensetagno">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Current Weight(lbs)</label>
                                            <input type="text" class="form-control allownumericwithoutdecimal" name="petcurrentweight" id="petcurrentweight">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="">Pet Target Weight(lbs)</label>
                                            <input type="text" class="form-control allownumericwithoutdecimal" name="pettargetweight" id="pettargetweight">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Pet Govt License Tag Issue</label>
                                            <input  id="licensetagissue" type="text" class="form-control" placeholder="Enter Pet License Tag Issue" name="govtlicensetagissuer">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="border-top: 1px solid #C2C2C2">
                                        <div class="form-group">
                                            <label for="">Is Adopted</label>
                                            <input type="checkbox" name="isadopted" id="isadopted" value="1"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Pet Birth / Adoption Date</label>
                                            <input class="form-control" type="text" placeholder="Pet Birth / Adoption Date" name="birthdate" id="birthdate" >
                                        </div>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <label for="">Calculate Pet Age</label> <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
                                            <input type="text" class="form-control allownumericwithoutdecimal" name="petage" id="petage" value="" readonly>
                                        </div>
                                        <form id="ageunit" name="ageunit">
                                            Years  <input type="radio" name="petageunit"  value="years"/>
                                            &nbsp;&nbsp;Weeks <input type="radio"  name="petageunit"  value="weeks"/>
                                            &nbsp;&nbsp;Days  <input type="radio"  name="petageunit"  value="days" />
                                        </form>
                                        <div id="box-body2">
                                            <div class="form-group">
                                                <label for="">Gender</label>
                                                M <input type="radio" name="genderchk0" id="genderchk0" value="M" />
                                                F <input type="radio" name="genderchk1" id="genderchk1"value="F" />
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="row" style="margin-top: 10px;">
                                                        <label >Pet Medication</label>
                                                        <span id="medicationaddedlabel">   </span>
                                                        <!--   <p><a href="#"  data-toggle="modal" data-target="#medication"><u>View Medications</u></a></p> -->
                                                        <div class="box-body table-responsive" id="box-body3" style="padding: 10px 15px;">
                                                            <table id="medicinetable" class="table table-bordered table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Medicine</th>
                                                                        <th>Frequency</th>
                                                                        <th>Link</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <label >Pet Medical Files</label>
                                                        <span id="medicationfileslabel"> Files uploaded from vendors. </span>
                                                        <!--   <p><a href="#"  data-toggle="modal" data-target="#medication"><u>View Medications</u></a></p> -->
                                                        <div class="box-body table-responsive" id="box-body4" style="padding: 10px 15px;">
                                                            <table id="medicinefilestable" class="table table-bordered table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>File Name</th>
                                                                        <th>File Url</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label >Pet Medical History</label>
                                            <textarea id="medicalhistory" class="form-control"  placeholder="Pet Mediacal History / Medications" name="medicalhistory" ></textarea>
                                        </div>
                                    </div>

                                    <div class="col-xs-12" style="border-top: 1px solid #C2C2C2">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="petvaccination">Pet Vaccination</label>
                                                        <span id="vaccinationfileslabel"> Files uploaded from vendors. </span>
                                                        <!--   <p><a href="#"  data-toggle="modal" data-target="#medication"><u>View Medications</u></a></p> -->
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="box-body table-responsive" id="box-body5" style="padding: 10px 15px;">
                                                                <table id="vaccinationtable" class="table table-bordered table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Due date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="targetpopulation">Pet Breed</label>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-6">
                                                                <select name="primarybreed" id="primarybreed"class="form-control">
                                                                    <option value="" >Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <select name="secondarybreed" id="secondarybreed" class="form-control">
                                                                    <option value="" >Select</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="targetpopulation">Pet Dietary Notes</label>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-6">
                                                                <select name="primaryfood" id="primaryfood" class="form-control">
                                                                    <option value="" >Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6" id="feedbrandlink1"></div>
                                                            <div class="col-sm-6">
                                                                <select name="secondaryfood" id="secondaryfood" class="form-control">
                                                                    <option value="" >Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6" id="feedbrandlink2">
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px;">
                                                            <div class="col-sm-6"> <input id="feedamount" class="form-control" type="number" placeholder="Enter amount of feed" name="feedamount"></div>
                                                            <div class="col-sm-6"> <input id="feedfrequency" class="form-control" type="number" placeholder="Enter frequency of feed" name="feedfrequency"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p style="border: 1px solid #C2C2C2"></p>
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <div class="box-body" style="padding-left: 0px;">
                                    <div class="col-lg-5 col-xs-5">
                                        <table class="table" id="fileUploadTable">
                                            <thead style="background-color:#EEEEEE;">
                                            <th><label>Select file<font color="red">*</font></label></th>
                                            <th><label>Upload Date</label></th>
                                            <th><td><a onclick="addMoreRowsToAttachEdit(this.form);" class="glyphicon-class label label-info">Add</span></a></td></th>
                                            </thead>
                                            <tbody id="addedRowsToAttachEdit">

                                            </tbody>
                                        </table>
                                        <div class="alert alert-success">
                                            <strong>Uploaded files are only saved upon hitting "Save"</strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-xs-7" style="padding-left: 25px;">
                                        <?php
                                        if (count($petFileInfo) <= 0) {
                                            $display = 'none';
                                        } else {
                                            $display = '';
                                        }
                                        ?>
                                        <table class="table" id="fileTable" style="display: <?php echo $display ?>">
                                            <thead style="background-color:#EEEEEE;">
                                            <th>File Name</th>
                                            <th>Upload Date</th>
                                            <th>View File</th>
                                            <th>Delete File</th>
                                            </thead>
                                            <tbody id="fileTableBody">
                                                <?php foreach ($petFileInfo as $petRows) { ?>
                                                    <tr id="petRow<?php echo $petRows['id']; ?>">
                                                        <td><?php echo $petRows['filename']; ?></td>
                                                        <td><?php echo $petRows['updatedon']; ?></td>
                                                        <td><a href="<?php echo base_url() . 'view_vendor_file/' . $petRows['id']; ?>" target="_blank">View</td>
                                                        <td>
                                                            <a style="cursor: pointer" onclick="javascript:deletePetFile('<?php echo $petRows['id']; ?>', '<?php echo $petRows['vendorid']; ?>', '<?php echo $petRows['petid']; ?>')">
                                                                <span class="label label-danger">Delete</span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn pull-right" type="button" value="step-2">Next</button>
                                        <?php if ($appuserdetail['appuserid'] != 0) { ?>
                                            <button class="btn btn-warning pull-right" id="updateInfo_s1">Save</button>
                                            <?php echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"'); ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--############################# STEP 2 end ##########################################-->
                        <!--############################# STEP 3 start ##########################################-->
                        <div class="row setup-content" id="step-3">
                            <div class="col-md-12">
                                <div class="box-body" id="box-body6">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="">Veterinarian Name </label>
                                                <input id="vetname" class="form-control"  placeholder="Enter Vet Name" name="vetname" maxlength="200">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="">Point of Contact</label>
                                                <input id="vetcontactPerson" class="form-control" type="text" placeholder="Enter Name of person to contact" name="vetcontactPerson" maxlength="200">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input id="vetphone" class="form-control" type="number" placeholder="Enter phone Number" name="vetphone">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label>Emergency Number</label>
                                                <input id="vetemergphone" class="form-control" type="number" placeholder="Enter emergency phone Number" name="vetemergphone">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="">Addresss</label>
                                                <input id="vetaddress" class="form-control" type="text" placeholder="Enter address" name="vetaddress" maxlength="45">
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-sm-1">City</div>
                                                    <div class="col-sm-5"> <input id="vetcity" class="form-control" type="text" placeholder="Enter city" name="vetcity" maxlength="45"></div>
                                                </div>
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-sm-1">State</div>
                                                    <div class="col-sm-5"> <input id="vetstate" class="form-control" type="text" placeholder="Enter state" name="vetstate" maxlength="45"></div>
                                                </div>
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-sm-1">Zipcode &nbsp;</div>
                                                    <div class="col-sm-5"> <input id="vetzipcode" class="form-control" type="text" placeholder="Enter zipcode" name="vetzipcode" maxlength="15"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Hours of operation</label>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-xs-1"> From </div>
                                                <div class="col-xs-3">
                                                    <input type="text" class="form-control" name="fromhoursofoperation" id="fromhoursofoperation">
                                                </div>
                                                <div class="col-xs-1"> To </div>
                                                <div class="col-xs-3">
                                                    <input type="text" class="form-control" name="tohoursofoperation" id="tohoursofoperation">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <?php for ($i = 0; $i < count($weekdays); $i++) { ?>
                                                    <?php echo $weekdays[$i]; ?> &nbsp; <input type="checkbox" name="availabilitydays[]" id="availabilitydays<?php echo $i; ?>" value="<?php echo $weekdays[$i]; ?>"/> &nbsp;
                                                    &nbsp;
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <?php if ($appuserdetail['appuserid'] == 0) { ?>
                                        <input type="submit" class="btn btn-warning" name='submit' value="Save">
                                        <?php
                                    } else {
                                        echo anchor('vendor/customer/listofgenericcustomer', 'Back', 'class="btn  btn-primary pull-left"');
                                        ?>
                                        <div>
                                            <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Update">
                                        </div>
                                        <div>
                                            <?php
                                            if (intval($precustomer) > 0 && $preusertype != "N/A")
                                                echo anchor('vendor/customer/viewDetails/' . $precustomer . '/' . rawurlencode($preusertype), 'Previous', 'class="btn btn-primary pull-right close_s1"');
                                            ?>
                                            <?php
                                            if (intval($nextcustomer) > 0 && $nextusertype != "N/A")
                                                echo anchor('vendor/customer/viewDetails/' . $nextcustomer . '/' . rawurlencode($nextusertype), 'Next', 'class="btn btn-primary pull-right close_s1"');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end box -->
            </div>
        </form>
    </section><!-- /.content -->
</div>

<div id="medication" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">View Medicatinon</h4>
            </div>
            <section class="content">

                <div class="box-body table-responsive" id="box-body6" style="padding: 10px 15px;">
                    <table id="" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Medicine</th>
                                <th>Frequency</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(window).load(function () {

        $('#deletePetFile').click(function (event) {
            var petFileId = $('#petFileId').val();
            url_ = baseurl + 'vendor/customer/deletePetFile';
            data_ = "petFileId=" + petFileId;
            $.ajax({
                type: "post",
                url: url_,
                cache: false,
                data: data_,
                success: function (data) {
                    if (data == 'File deleted successfully') {
                        $('#fileUploadDiv').show();
                        $('#petFileTable').hide();
                    } else {
                        $('#fileUploadDiv').hide();
                    }
                }
            });
            event.preventDefault();
        });
        $('#uploadPetFile').on('click', function (e) {
            url = baseurl + 'vendor/customer/uploadPetFile';
            $.ajax({
                url: url,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                }
            });
            e.preventDefault();
        });

        $('#selectpet li:eq(0)').addClass('active');
        var petid = $('#selectpet li:eq(0)').attr('value');
        var matchcount = 0;
        var jqueryarray = <?php echo json_encode($pets); ?>;
        var jqueryarray1 = <?php echo json_encode($vets); ?>;
        var jqueryarray2 = <?php echo json_encode($feedbrands); ?>;
        var jqueryarray3 = <?php echo json_encode($breeds); ?>;
        var petmedicationadded = <?php echo json_encode($petmedication); ?>;
        var petvaccinationadded = <?php echo json_encode($petvaccination); ?>;
        var petmedicalfiles = <?php echo json_encode($medicalfiles); ?>;
        console.log(petmedicalfiles);
        for (var i = 0; i < jqueryarray.length; i++)
        {
            if (jqueryarray[i]['petid'] == petid)
            {
                matchcount = i;
                $('#petid').val(jqueryarray[matchcount]['petid']);
                $('#petname').val(jqueryarray[matchcount]['name']);
                $('#petnotes').val(jqueryarray[matchcount]['notes']);
                $('#microchipped').val(jqueryarray[matchcount]['microchipped']);
                $('#microchipid').val(jqueryarray[matchcount]['microchipid']);
                $('#licensetag').val(jqueryarray[matchcount]['govtlicensetagno']);
                $('#licensetagissue').val(jqueryarray[matchcount]['govtlicensetagissuer']);
                $('#petcurrentweight').val(jqueryarray[matchcount]['currentweight']);
                $('#pettargetweight').val(jqueryarray[matchcount]['targetweight']);
                $('#isadopted').val(jqueryarray[matchcount]['isadopted']);
                //$('#birthdate').val(jqueryarray[matchcount]['birthdate']);
                var curr = jqueryarray[matchcount]['birthdate'];
                if (curr != null) {
                    var birthdate = $.datepicker.formatDate("MM dd, yy", new Date(curr));
                    $('#birthdate').val(birthdate);
                } else {
                    $('#birthdate').val('');
                }
                $('#feedamount').val(jqueryarray[matchcount]['amountoffeed']);
                $('#feedfrequency').val(jqueryarray[matchcount]['frequencyoffeed']);
                $('#medicalhistory').val(jqueryarray[matchcount]['medicalhistory']);

                if (jqueryarray[matchcount]['proilefpicture'] != null)
                {
                    $('#profileimage').attr('src', '<?php echo awsBucketPath; ?>' + jqueryarray[matchcount]['proilefpicture']);
                    $('#profileimage').show();
                } else
                {
                    $('#profileimage').hide();
                }

                if (jqueryarray[matchcount]['isadopted'] == 1)
                {
                    $('#isadopted').prop('checked', true);
                } else
                {
                    $('#isadopted').prop('checked', false);
                }

                if (jqueryarray[matchcount]['microchipped'] == 1)
                {
                    $('#microchipped').prop('checked', true);
                } else
                {
                    $('#microchipped').prop('checked', false);
                }

                if (jqueryarray[matchcount]['gender'] == 'M')
                {
                    $('#genderchk0').prop('checked', true);
                    $('#genderchk1').prop('checked', false);
                } else if (jqueryarray[matchcount]['gender'] == 'F')
                {
                    $('#genderchk0').prop('checked', false);
                    $('#genderchk1').prop('checked', true);
                }

                for (var j = 0; j < jqueryarray2.length; j++)
                {
                    if (jqueryarray[matchcount]['primaryfood'] == jqueryarray2[j]['feedbrandid'])
                    {
                        $('#primaryfood').append($('<option>', {
                            value: jqueryarray2[j]['feedbrandid'],
                            text: jqueryarray2[j]['title'],
                            selected: "selected"
                        }));
                        $('#feedbrandlink1').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[j]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                    } else
                    {
                        $('#primaryfood').append($('<option>', {
                            value: jqueryarray2[j]['feedbrandid'],
                            text: jqueryarray2[j]['title']
                        }));
                    }
                }
                for (var k = 0; k < jqueryarray2.length; k++)
                {
                    if (jqueryarray[matchcount]['secondaryfood'] == jqueryarray2[k]['feedbrandid'])
                    {
                        $('#secondaryfood').append($('<option>', {
                            value: jqueryarray2[k]['feedbrandid'],
                            text: jqueryarray2[k]['title'],
                            selected: "selected"
                        }));
                        $('#feedbrandlink2').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[k]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                    } else
                    {
                        $('#secondaryfood').append($('<option>', {
                            value: jqueryarray2[k]['feedbrandid'],
                            text: jqueryarray2[k]['title']
                        }));
                    }
                }
                for (var l = 0; l < jqueryarray3.length; l++)
                {
                    if (jqueryarray[matchcount]['primarybreed'] == jqueryarray3[l]['breedid'])
                    {
                        $('#primarybreed').append($('<option>', {
                            value: jqueryarray3[l]['breedid'],
                            text: jqueryarray3[l]['title'],
                            selected: "selected"
                        }));
                    } else
                    {
                        $('#primarybreed').append($('<option>', {
                            value: jqueryarray3[l]['breedid'],
                            text: jqueryarray3[l]['title']
                        }));
                    }
                }
                for (var m = 0; m < jqueryarray3.length; m++)
                {
                    if (jqueryarray[matchcount]['secondarybreed'] == jqueryarray3[m]['breedid'])
                    {
                        $('#secondarybreed').append($('<option>', {
                            value: jqueryarray3[m]['breedid'],
                            text: jqueryarray3[m]['title'],
                            selected: "selected"
                        }));
                    } else
                    {
                        $('#secondarybreed').append($('<option>', {
                            value: jqueryarray3[m]['breedid'],
                            text: jqueryarray3[m]['title']
                        }));
                    }
                }

                for (var n = 0; n < jqueryarray1.length; n++)
                {
                    if (jqueryarray[matchcount]['primaryvet'] == jqueryarray1[n]['vendorid'])
                    {
                        $('#vetname').val(jqueryarray1[n]['comapnyname']);
                        $('#vetphone').val(jqueryarray1[n]['contact']);
                        $('#vetemergphone').val(jqueryarray1[n]['contact']);
                        $('#vetaddress').val(jqueryarray1[n]['address']);
                        $('#vetcity').val(jqueryarray1[n]['city']);
                        $('#vetzipcode').val(jqueryarray1[n]['zipcode']);
                        $('#vetcountry').val(jqueryarray1[n]['country']);
                        $('#vetstate').val(jqueryarray1[n]['state']);
                        $('#vetcontactPerson').val(jqueryarray1[n]['firstname'] + " " + jqueryarray1[n]['lastname']);
                        var fromtime = jqueryarray1[n]['availabilityfromhour'];
                        fromtime = fromtime.split(',');
                        fromtime[0] = fromtime[0] + ' ' + "am";
                        $('#fromhoursofoperation').val(fromtime[0]);

                        var totime = jqueryarray1[n]['availabilitytohour'];
                        totime = totime.split(',');
                        totime[0] = totime[0] + ' ' + "pm";
                        $('#tohoursofoperation').val(totime[0]);

                        var operatingdays = jqueryarray1[n]['operatingdays'];
                        operatingdays = operatingdays.split(',');
                        for (var op = 0; op < operatingdays.length; op++)
                        {
                            if (operatingdays[op] == "M")
                            {
                                $('#availabilitydays0').prop('checked', true);
                            } else if (operatingdays[op] == "T")
                            {
                                $('#availabilitydays1').prop('checked', true);
                            } else if (operatingdays[op] == "W")
                            {
                                $('#availabilitydays2').prop('checked', true);
                            } else if (operatingdays[op] == "Th")
                            {
                                $('#availabilitydays3').prop('checked', true);
                            } else if (operatingdays[op] == "F")
                            {
                                $('#availabilitydays4').prop('checked', true);
                            } else if (operatingdays[op] == "Sa")
                            {
                                $('#availabilitydays5').prop('checked', true);
                            } else if (operatingdays[op] == "Su")
                            {
                                $('#availabilitydays6').prop('checked', true);
                            }
                        }
                        break;
                    } else {
                        $('#vetname').val("");
                        $('#vetphone').val("");
                        $('#vetemergphone').val("");
                        $('#vetaddress').val("");
                        $('#vetcity').val("");
                        $('#vetzipcode').val("");
                        $('#vetcountry').val("");
                        $('#vetstate').val("");
                        $('#vetcontactPerson').val("");
                        $('#fromhoursofoperation').val("");
                        $('#tohoursofoperation').val("");
                    }
                }
                if (petmedicationadded[matchcount] != "")
                {
                    var jsonData = petmedicationadded[matchcount];
                    var medicines = [];
                    var medicationadded = {
                        'medication': []
                    };
                    var cn = 0;
                    for (var c = 0; c < jsonData.length; c++)
                    {
                        var medobj = jsonData[c];
                        var medid = medobj.medicineid;
                        var link = medobj.url;
                        var freq = medobj.frequency;
                        var dosg = medobj.dosage;
                        var name = medobj.name;

                        var freqtimes = medobj.frequencytimes;
                        medicationadded.medication.push({'medicineid': medid, 'frequency': freq, 'dosage': dosg});
                        cn++;
                        var dispfreq = "";
                        if (freq == "Daily" || freq == "Weekly" || freq == "Monthly")
                            dispfreq = dosg + " Tablets  " + freqtimes + " times " + freq;
                        $("#medicinetable").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td></tr>');

                    }

                    $('#medicationaddedlabel').html("<lable>" + cn + " medications added" + "</label>");
                    if (cn == 0)
                    {
                        $("#medicinetable").hide();
                    }
                }

                if (petvaccinationadded[matchcount] != "")
                {
                    var jsonData = petvaccinationadded[matchcount];
                    var vaccinations = [];
                    var vaccinationadded = {
                        'vaccination': []
                    };
                    var cn = 0;
                    for (var c = 0; c < jsonData.length; c++)
                    {
                        var vacobj = jsonData[c];
                        var d = new Date(vacobj.duedate);
                        var date = d.toDateString();
                        cn++;
                        $("#vaccinationtable").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td></tr>');

                    }
                    $('#vaccinationfileslabel').html("<lable>" + cn + " vaccinations added" + "</label>");
                    if (cn == 0)
                    {
                        $("#vaccinationtable").hide();
                    }
                }

                if (petmedicalfiles[matchcount] != "")
                {
                    var cnt = 0;
                    var jsonData = petmedicalfiles[matchcount];
                    var medicalfile = [];
                    var medicalfilesadded = {
                        'medfiles': []
                    };
                    for (var i = 0; i < jsonData.length; i++) {
                        cnt++;
                        var url = jsonData[i].fileurl.toString();
                        console.log(url);
                        /*url = url.replaceAll(' ','%20');
                         console.log(url);*/
                        $("#medicinefilestable").append('<tr><td>' + jsonData[i].filename + '</td><td> <a target="_blank" href=' + url + '>Link</a></td></tr>');

                    }

                    if (cnt == 0)
                    {
                        $("#medicinefilestable").hide();
                    }

                }
                break;
            }
        }

    });

    $(window).click(function () {
        //event.preventDefault();
        if ($('#selectpet li').hasClass("active")) {
            // event.preventDefault();
        }
    });
    $(document).ready(function () {

        $("#box-body :input").attr("disabled", "disabled");
        $("#box-body1 :input").attr("disabled", "disabled");
        $("#box-body2 :input").attr("disabled", "disabled");
        $("#box-body3 :input").attr("disabled", "disabled");
        $("#box-body4 :input").attr("disabled", "disabled");
        $("#box-body5 :input").attr("disabled", "disabled");
        $("#box-body6 :input").attr("disabled", "disabled");
        $("#customer-notes :input").attr("disabled", false);
        $("#pet-notes :input").attr("disabled", false);
        $('#profileimage').hide();
        $("#selectpet li").on('click', function (event) {
            event.preventDefault();
            $('#selectpet li').removeClass("active");
            $('#medicinetable tbody tr').remove();
            $('#medicationaddedlabel').html("<lable>" + 0 + " medications added" + "</label>");
            $('#medicinefilestable tbody tr').remove();
            $('#vaccinationtable tbody tr').remove();
            $('#vaccinationfileslabel').html("<lable>" + 0 + " vaccinations added" + "</label>");
            $('input:radio[name="petageunit"]').prop("checked", false);
            $('#microchipped').prop('checked', false);
            $('#genderchk0').prop('checked', false);
            $('#genderchk1').prop('checked', false);
            $('#availabilitydays0').prop('checked', false);
            $('#availabilitydays1').prop('checked', false);
            $('#availabilitydays2').prop('checked', false);
            $('#availabilitydays3').prop('checked', false);
            $('#availabilitydays4').prop('checked', false);
            $('#availabilitydays5').prop('checked', false);
            $('#availabilitydays6').prop('checked', false);
            $('#feedbrandlink1').html('');
            $('#feedbrandlink2').html('');
            $('#primaryfood option:selected').removeAttr('selected');
            $('#secondaryfood option:selected').removeAttr('selected');
            $('#primarybreed option:selected').removeAttr('selected');
            $('#secondarybreed option:selected').removeAttr('selected');
            $("#petage").val("");
            var index = $(this).index();
            $("#selectpet li:eq(" + index + ")").addClass('active');
            var petid = $(this).attr('value');
            var vendorid = $('#vendorid').val();
            // viewing vendorfile //
            url_ = baseurl + 'vendor/customer/getPetFile';
            data_ = "vendorId=" + vendorid + "&petId=" + petid;
            $.ajax({
                type: "post",
                url: url_,
                cache: false,
                data: data_,
                success: function (data) {
                    if (data == 0) {
                        $('#fileTable').hide();
                        $('#fileTableBody').html('')
                    } else {
                        $('#fileTable').show();
                        $('#fileTableBody').val(data);
                        $('#fileTableBody').html(data);
                    }

                }
            });


            var matchcount = 0;
            var jqueryarray = <?php echo json_encode($pets); ?>;
            var jqueryarray1 = <?php echo json_encode($vets); ?>;
            var jqueryarray2 = <?php echo json_encode($feedbrands); ?>;
            var jqueryarray3 = <?php echo json_encode($breeds); ?>;
            var petmedicationadded = <?php echo json_encode($petmedication); ?>;
            var petvaccinationadded = <?php echo json_encode($petvaccination); ?>;
            var petmedicalfiles = <?php echo json_encode($medicalfiles); ?>;
            console.log(petmedicalfiles);
            for (var i = 0; i < jqueryarray.length; i++)
            {
                if (jqueryarray[i]['petid'] == petid)
                {
                    matchcount = i;
                    $('#petid').val(jqueryarray[matchcount]['petid']);
                    $('#petname').val(jqueryarray[matchcount]['name']);
                    $('#microchipped').val(jqueryarray[matchcount]['microchipped']);
                    $('#microchipid').val(jqueryarray[matchcount]['microchipid']);
                    $('#licensetag').val(jqueryarray[matchcount]['govtlicensetagno']);
                    $('#licensetagissue').val(jqueryarray[matchcount]['govtlicensetagissuer']);
                    $('#petcurrentweight').val(jqueryarray[matchcount]['currentweight']);
                    $('#pettargetweight').val(jqueryarray[matchcount]['targetweight']);
                    $('#isadopted').val(jqueryarray[matchcount]['isadopted']);
                    //$('#birthdate').val(jqueryarray[matchcount]['birthdate']);
                    var curr = jqueryarray[matchcount]['birthdate'];
                    if (curr != null) {
                        var birthdate = $.datepicker.formatDate("MM dd, yy", new Date(curr));
                        $('#birthdate').val(birthdate);
                    } else {
                        $('#birthdate').val('');
                    }
                    $('#feedamount').val(jqueryarray[matchcount]['amountoffeed']);
                    $('#feedfrequency').val(jqueryarray[matchcount]['frequencyoffeed']);
                    $('#medicalhistory').val(jqueryarray[matchcount]['medicalhistory']);

                    if (jqueryarray[matchcount]['proilefpicture'] != null)
                    {
                        $('#profileimage').attr('src', '<?php echo awsBucketPath ?>' + jqueryarray[matchcount]['proilefpicture']);
                        $('#profileimage').show();
                    } else
                    {
                        $('#profileimage').hide();
                    }

                    if (jqueryarray[matchcount]['isadopted'] == 1)
                    {
                        $('#isadopted').prop('checked', true);
                    } else
                    {
                        $('#isadopted').prop('checked', false);
                    }

                    if (jqueryarray[matchcount]['microchipped'] == 1)
                    {
                        $('#microchipped').prop('checked', true);
                    } else
                    {
                        $('#microchipped').prop('checked', false);
                    }

                    if (jqueryarray[matchcount]['gender'] == 'M')
                    {
                        $('#genderchk0').prop('checked', true);
                        $('#genderchk1').prop('checked', false);
                    } else if (jqueryarray[matchcount]['gender'] == 'F')
                    {
                        $('#genderchk0').prop('checked', false);
                        $('#genderchk1').prop('checked', true);
                    }

                    for (var j = 0; j < jqueryarray2.length; j++)
                    {
                        if (jqueryarray[matchcount]['primaryfood'] == jqueryarray2[j]['feedbrandid'])
                        {
                            $('#primaryfood').append($('<option>', {
                                value: jqueryarray2[j]['feedbrandid'],
                                text: jqueryarray2[j]['title'],
                                selected: "selected"
                            }));
                            $('#feedbrandlink1').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[j]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                        } else
                        {
                            $('#primaryfood').append($('<option>', {
                                value: jqueryarray2[j]['feedbrandid'],
                                text: jqueryarray2[j]['title']
                            }));
                        }
                    }
                    for (var k = 0; k < jqueryarray2.length; k++)
                    {
                        if (jqueryarray[matchcount]['secondaryfood'] == jqueryarray2[k]['feedbrandid'])
                        {
                            $('#secondaryfood').append($('<option>', {
                                value: jqueryarray2[k]['feedbrandid'],
                                text: jqueryarray2[k]['title'],
                                selected: "selected"
                            }));
                            $('#feedbrandlink2').html('&nbsp; &nbsp; &nbsp; &nbsp; <u><a href="' + jqueryarray2[k]['link'] + '" target="_blank">Click on link to see pet secondary food advisor.</a></u>');
                        } else
                        {
                            $('#secondaryfood').append($('<option>', {
                                value: jqueryarray2[k]['feedbrandid'],
                                text: jqueryarray2[k]['title']
                            }));
                        }
                    }
                    for (var l = 0; l < jqueryarray3.length; l++)
                    {
                        if (jqueryarray[matchcount]['primarybreed'] == jqueryarray3[l]['breedid'])
                        {
                            $('#primarybreed').append($('<option>', {
                                value: jqueryarray3[l]['breedid'],
                                text: jqueryarray3[l]['title'],
                                selected: "selected"
                            }));
                        } else
                        {
                            $('#primarybreed').append($('<option>', {
                                value: jqueryarray3[l]['breedid'],
                                text: jqueryarray3[l]['title']
                            }));
                        }
                    }
                    for (var m = 0; m < jqueryarray3.length; m++)
                    {
                        if (jqueryarray[matchcount]['secondarybreed'] == jqueryarray3[m]['breedid'])
                        {
                            $('#secondarybreed').append($('<option>', {
                                value: jqueryarray3[m]['breedid'],
                                text: jqueryarray3[m]['title'],
                                selected: "selected"
                            }));
                        } else
                        {
                            $('#secondarybreed').append($('<option>', {
                                value: jqueryarray3[m]['breedid'],
                                text: jqueryarray3[m]['title']
                            }));
                        }
                    }

                    for (var n = 0; n < jqueryarray1.length; n++)
                    {
                        if (jqueryarray[matchcount]['primaryvet'] == jqueryarray1[n]['vendorid'])
                        {
                            $('#vetname').val(jqueryarray1[n]['comapnyname']);
                            $('#vetphone').val(jqueryarray1[n]['contact']);
                            $('#vetemergphone').val(jqueryarray1[n]['contact']);
                            $('#vetaddress').val(jqueryarray1[n]['address']);
                            $('#vetcity').val(jqueryarray1[n]['city']);
                            $('#vetzipcode').val(jqueryarray1[n]['zipcode']);
                            $('#vetcountry').val(jqueryarray1[n]['country']);
                            $('#vetstate').val(jqueryarray1[n]['state']);
                            $('#vetcontactPerson').val(jqueryarray1[n]['firstname'] + " " + jqueryarray1[n]['lastname']);
                            var fromtime = jqueryarray1[n]['availabilityfromhour'];
                            fromtime = fromtime.split(',');
                            fromtime[0] = fromtime[0] + ' ' + "am";
                            $('#fromhoursofoperation').val(fromtime[0]);

                            var totime = jqueryarray1[n]['availabilitytohour'];
                            totime = totime.split(',');
                            totime[0] = totime[0] + ' ' + "pm";
                            $('#tohoursofoperation').val(totime[0]);

                            var operatingdays = jqueryarray1[n]['operatingdays'];
                            operatingdays = operatingdays.split(',');
                            for (var op = 0; op < operatingdays.length; op++)
                            {
                                if (operatingdays[op] == "M")
                                {
                                    $('#availabilitydays0').prop('checked', true);
                                } else if (operatingdays[op] == "T")
                                {
                                    $('#availabilitydays1').prop('checked', true);
                                } else if (operatingdays[op] == "W")
                                {
                                    $('#availabilitydays2').prop('checked', true);
                                } else if (operatingdays[op] == "Th")
                                {
                                    $('#availabilitydays3').prop('checked', true);
                                } else if (operatingdays[op] == "F")
                                {
                                    $('#availabilitydays4').prop('checked', true);
                                } else if (operatingdays[op] == "Sa")
                                {
                                    $('#availabilitydays5').prop('checked', true);
                                } else if (operatingdays[op] == "Su")
                                {
                                    $('#availabilitydays6').prop('checked', true);
                                }
                            }
                            break;
                        } else {
                            $('#vetname').val("");
                            $('#vetphone').val("");
                            $('#vetemergphone').val("");
                            $('#vetaddress').val("");
                            $('#vetcity').val("");
                            $('#vetzipcode').val("");
                            $('#vetcountry').val("");
                            $('#vetstate').val("");
                            $('#vetcontactPerson').val("");
                            $('#fromhoursofoperation').val("");
                            $('#tohoursofoperation').val("");
                        }
                    }
                    if (petmedicationadded[matchcount] != "")
                    {
                        var jsonData = petmedicationadded[matchcount];
                        var medicines = [];
                        var medicationadded = {
                            'medication': []
                        };
                        var cn = 0;
                        for (var c = 0; c < jsonData.length; c++)
                        {
                            var medobj = jsonData[c];
                            var medid = medobj.medicineid;
                            var link = medobj.url;
                            var freq = medobj.frequency;
                            var dosg = medobj.dosage;
                            var name = medobj.name;

                            var freqtimes = medobj.frequencytimes;
                            medicationadded.medication.push({'medicineid': medid, 'frequency': freq, 'dosage': dosg});
                            cn++;
                            var dispfreq = "";
                            if (freq == "Daily" || freq == "Weekly" || freq == "Monthly")
                                dispfreq = dosg + " Tablets  " + freqtimes + " times " + freq;
                            $("#medicinetable").append('<tr><td>' + name + '</td><td>' + dispfreq + '</td><td><a href="' + link + '" target="_blank">Link</a></td></tr>');

                        }

                        $('#medicationaddedlabel').html("<lable>" + cn + " medications added" + "</label>");
                        if (cn == 0)
                        {
                            $("#medicinetable").hide();
                        }
                    }

                    if (petvaccinationadded[matchcount] != "")
                    {
                        var jsonData = petvaccinationadded[matchcount];
                        var vaccinations = [];
                        var vaccinationadded = {
                            'vaccination': []
                        };
                        var cn = 0;
                        for (var c = 0; c < jsonData.length; c++)
                        {
                            var vacobj = jsonData[c];
                            var d = new Date(vacobj.duedate);
                            var date = d.toDateString();
                            cn++;
                            $("#vaccinationtable").append('<tr><td>' + vacobj.name + '</td><td>' + date + '</td></tr>');

                        }
                        $('#vaccinationfileslabel').html("<lable>" + cn + " vaccinations added" + "</label>");
                        if (cn == 0)
                        {
                            $("#vaccinationtable").hide();
                        }
                    }

                    if (petmedicalfiles[matchcount] != "")
                    {
                        var cnt = 0;
                        var jsonData = petmedicalfiles[matchcount];
                        var medicalfile = [];
                        var medicalfilesadded = {
                            'medfiles': []
                        };
                        for (var i = 0; i < jsonData.length; i++) {
                            cnt++;
                            var url = jsonData[i].fileurl.toString();
                            console.log(url);
                            /*url = url.replaceAll(' ','%20');
                             console.log(url);*/
                            $("#medicinefilestable").append('<tr><td>' + jsonData[i].filename + '</td><td> <a target="_blank" href=' + url + '>Link</a></td></tr>');

                        }

                        if (cnt == 0)
                        {
                            $("#medicinefilestable").hide();
                        }

                    }
                    break;
                }
            }
        });

        function daydiff(first, second) {
            return Math.round((second - first) / (1000 * 60 * 60 * 24));
        }

        $('input:radio[name="petageunit"]').click(function () {
            var petdob = new Date($("#birthdate").val());
            if (($(this).is(':checked')) && ($(this).val() == 'years') && (petdob != "Invalid Date"))
            {
                console.log(petdob);
                var today = new Date();
                var age = today.getFullYear() - petdob.getFullYear();
                $("#petage").val(age);
            } else if (($(this).is(':checked')) && ($(this).val() == 'weeks') && (petdob != "Invalid Date"))
            {

                var today = new Date();
                var age = daydiff(petdob, today);
                age = Math.round((age / 7).toFixed(0));
                $("#petage").val(age);
            } else if (($(this).is(':checked')) && ($(this).val() == 'days') && (petdob != "Invalid Date"))
            {

                var today = new Date();
                var age = daydiff(petdob, today);
                $("#petage").val(age);
            } else
            {
                $("#petage").val("Pet Birthdate is required.");
            }

        });

    });

    $("#update, #updateInfo_s1").click(function () {

        var hasError = false;
        var errormsg = "";

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
        }

        if (hasError == false) {
            $("#validerrorform").html('');
            $("#addcustomerform").submit();
        } else
        {
            $("#validerrorform").html(errormsg);
            $("html, body").animate({scrollTop: 0}, "slow");
            return false;
        }
    });

</script>
<script>
    //form validation step by step

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

        // Start of Appointment add/update form validation //
        var hasError = false;
        var errormsg = "";
        var step = $(this).attr('value');
        var fname = $("#customerfname").val();
        var lname = $("#customerlname").val();
        var email = $("#customeremail").val();
        var petname = $("#petname").val();
        var vetname = $("#vetname").val();
        var zipcode = $("#zipcode").val();
        var country = $("#country").val();
        var phoneno = $('#phoneno').val();
        var address = $("#address").val();
        var city = $("#city").val();
        var licensetag = $("#licensetag").val();
        var licensetagissue = $("#licensetagissue").val();
        var validText = /(<([^>]+)>)/ig;

        switch (step) {
            case "step-1":

                break;
            case "step-2":
                //alert("step-2");
                if (petname.trim().length == 0)
                {
                    errormsg = 'Pet Name is required!';
                    $("#petname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(petname))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#petname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(licensetag))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#licensetag").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(licensetagissue))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#licensetagissue").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (petname.trim().length != 0 && !(validText.test(petname)))
                {
                    $("#petname").css('border-color', '#d2d6de');
                }
                if (licensetag.trim().length == 0 || !(validText.test(licensetag)))
                {
                    $("#licensetag").css('border-color', '#d2d6de');
                }
                if (licensetagissue.trim().length == 0 || !(validText.test(licensetagissue)))
                {
                    $("#licensetagissue").css('border-color', '#d2d6de');
                }
                break;

        }

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
            $("#validerrorform").html(errormsg);
        } else {
            $('#validation').hide();
            $("#validerrorform").html('');
        }

        // End of Appointment add/update form validation //

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alert(curInputs[i].val());
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    var rowCount = 1;
    var petFile;
    function addMoreRowsToAttachEdit(frm) {
        petFile = petFile + rowCount;
        var medicalfile = $('#medicalfile').val();
        var recRow = '<tr id="rowCount' + rowCount + '">' +
                "<td><input type='file' multiple='' name='petFile[]' id='" + petFile + "'/>" + "</td>" +
                "<td><label><?php echo date('d-m-Y'); ?></label></td>" +
                '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow(' + rowCount + ');">Delete</a></td>' +
                '</tr>';
        jQuery('#addedRowsToAttachEdit').append(recRow);
        rowCount++;
    }

    function removeRow(removeNum) {
        jQuery('#rowCount' + removeNum).remove();
    }
    function deletePetFile(petFileId, vendorId, petId) {
        url_ = baseurl + 'vendor/customer/deletePetFile';
        data_ = "petFileId=" + petFileId + "&vendorId=" + vendorId + "&petId=" + petId;
        $.ajax({
            type: "post",
            url: url_,
            cache: false,
            data: data_,
            success: function (data) {
                var jsonObj = $.parseJSON(data);
                if (jsonObj.msg == 'File deleted successfully') {
                    $('#petRow' + petFileId).hide();
                    if (jsonObj.noOfFile == 0) {
                        $('#fileTable').hide();
                    }
                }
            }
        });
    }
    // var rowCount=1 ;
    var vendorFile;
    function addMoreRowsToAttach(frm) {
        vendorFile = vendorFile + rowCount;
        var recRow = '<tr id="rowCount' + rowCount + '">' +
                "<td><input type='file' multiple='' name='vendorFile[]' id='" + vendorFile + "'/>" + "</td>" +
                "<td><label><?php echo date('d-m-Y'); ?></label></td>" +
                '<td><a class="label label-danger" href="javascript:void(0);" onclick="removeRow(' + rowCount + ');">Delete</a></td>' +
                '</tr>';
        jQuery('#addedRowsToAttach').append(recRow);
        rowCount++;
    }
    function deleteVendorFile(vendorFileId, vendorId, petId) {
        url_ = baseurl + 'vendor/customer/deleteVendorFile';
        data_ = "vendorFileId=" + vendorFileId + "&vendorId=" + vendorId + "&petId=" + petId;
        $.ajax({
            type: "post",
            url: url_,
            cache: false,
            data: data_,
            success: function (data) {
                var jsonObj = $.parseJSON(data);
                if (jsonObj.msg == 'File deleted successfully') {
                    $('#vendorRow' + vendorFileId).hide();
                    if (jsonObj.noOfFile == 0) {
                        $('#fileVendorTable').hide();
                    }
                }
            }
        });
    }
</script>