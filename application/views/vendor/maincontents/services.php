<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Services
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Services</a></li>
            <li class="active">Services</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
        <div class="box-body">
        <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
        </div>
        <?php //echo '<pre>'; print_r($row); echo '</pre>';?>
        
        
        <div class="row">
            <div class="col-md-12">
              
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Account Settings</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Plan Details</a></li>
                  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Service Providers</a></li>
                  <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Users</a></li>
                  
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                  	
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapse in">
                            Basic Information
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      	<form role="form">
                        <div class="box-body">
                          
			                    <!-- text input -->
			                    <div class="form-group">
			                      <label>Name</label>
			                      <input type="text" class="form-control" placeholder="Enter Name">
			                    </div>
			                    
			                    <div class="form-group">
			                      <label>Email Address</label>
			                      <input type="email" class="form-control" placeholder="Enter Email Address">
			                    </div>
			
			                    <div class="form-group">
			                      <label>Company Name</label>
			                      <input type="text" class="form-control" placeholder="Enter your company name">
			                    </div>
			                    
			                    <div class="form-group">
			                      <label>Contact No.</label>
			                      <input type="tel" class="form-control" placeholder="Enter your Contact Number">
			                    </div>
			                    
			                    <div class="form-group">
			                      <label>Address</label>
			                      <textarea class="form-control" placeholder="Enter your Address" rows="4"></textarea>
			                    </div>
                        </div>
                        
                        <div class="box-footer">
	                    		<button type="submit" class="btn btn-warning">Submit</button>
	                    </div>
	                    
                       </form>
                      </div>
                    </div>
                    
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" class="collapsed">
                            Run Information
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        
                        <form role="form">
                        
                        <div class="box-body">
                          
			                    <div class="form-group">
			                      <label>How do you charge for bookings?</label>
			                      <select class="form-control">
			                        <option>I charge by the night</option>
			                        <option>I charge by the day</option>
			                      </select>
			                    </div>
			                    
			                    <div class="callout callout-warning">
				                    <p style="color: #784C06;">A Monday to Friday booking could count as a 4 night booking, or a 5 day booking, depending on your rate setup.</p>
				                  </div>
				                  
				                <div class="form-group">
			                      <label>How many runs do you have?</label>
			                      <select class="form-control">
			                        <option>0</option>
			                        <option>1</option>
			                        <option>2</option>
			                        <option>3</option>
			                        <option>4</option>
			                        <option>5</option>
			                        <option>6</option>
			                        <option>7</option>
			                        <option>8</option>
			                        <option>9</option>
			                        <option>10</option>
			                        <option>11</option>
			                        <option>12</option>
			                        <option>13</option>
			                        <option>14</option>
			                        <option>15</option>
			                        <option>16</option>
			                        <option>17</option>
			                        <option>18</option>
			                        <option>19</option>
			                        <option>20</option>
			                        <option>21</option>
			                        <option>22</option>
			                        <option>23</option>
			                        <option>24</option>
			                        <option>25</option>
			                        <option>26</option>
			                        <option>27</option>
			                        <option>28</option>
			                        <option>29</option>
			                        <option>30</option>
			                        <option>31</option>
			                        <option>32</option>
			                        <option>33</option>
			                        <option>34</option>
			                        <option>35</option>
			                      </select>
			                    </div>  
                  			
                        </div>
                        
                        <div class="box-footer">
	                    		<button type="submit" class="btn btn-warning">Save</button>
	                    </div>
	                    
	                    </form>
                      </div>
                    </div>
                    
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" class="collapsed">
                            Special Rates
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                      </div>
                    </div>
                    
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" class="collapsed">
                            Key Details
                          </a>
                        </h4>
                      </div>
                      <div id="collapsefour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                      </div>
                    </div>
                    
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" class="collapsed">
                            Payment Types
                          </a>
                        </h4>
                      </div>
                      <div id="collapsefive" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                      </div>
                    </div>
                    
                    <div class="box box-warning box-solid">
                      <div class="box-header ">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" class="collapsed">
                            Blackout Dates
                          </a>
                        </h4>
                      </div>
                      <div id="collapsesix" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <form role="form">
                        
                        <div class="box-body">
                          
			     
			                    <div class="callout callout-warning">
				                    <p style="color: #784C06;">Set dates when you will be unavailable. Useful for blacking out holidays and vacations. You can set future dates to be blacked out, and these dates will show as unavailable for booking requests, and will be marked in your calendar.</p>
				                  </div>
				                  
				                <div class="row">
				                    <div class="col-xs-5">
				                      <label>From</label>
				                      <input type="date" class="form-control" placeholder=".col-xs-3">
				                    </div>
				                    <div class="col-xs-5">
				                      <label>To</label>
				                      <input type="date" class="form-control" placeholder=".col-xs-4">
				                    </div>
				                    <div class="col-xs-2">
				                    	<label style="color: transparent;">Sumbit</label>	<br>
				                      <a type="submit" class="btn btn-warning">Save</a>
				                    </div>
				                  </div>
                  			
                        </div>
                        
	                    
	                    </form>
                      </div>
                    </div>
                  </div><!-- /.tab-pane -->
                  
                  <div class="tab-pane" id="tab_2">
                    The European languages are members of the same family. Their separate existence is a myth.
                    For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                    in their grammar, their pronunciation and their most common words. Everyone realizes why a
                    new common language would be desirable: one could refuse to pay expensive translators. To
                    achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                    words. If several languages coalesce, the grammar of the resulting language is more simple
                    and regular than that of the individual languages.
                  </div><!-- /.tab-pane -->
                  
                  <div class="tab-pane" id="tab_3">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div><!-- /.tab-pane -->
                  
                  <div class="tab-pane" id="tab_4">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div><!-- /.tab-pane -->
                  
                </div><!-- /.tab-content -->
              </div>
           
            </div><!-- /.col -->
            
          </div> 
			
        </section><!-- /.content -->
        
         </div><!-- /.content-wrapper -->