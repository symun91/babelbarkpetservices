<?php $admin=$this->session->userdata('admin');
					$userrole=$admin['role'];
?>
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Appointments
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Appointments</a></li>
            <li class="active">View Appointments</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
        
       
           
          <br><br> <br><br><br><br> 
            <div class="col-md-12">
            
            
            
              <div class="box">
                <div class="box-header with-border">
                <div class=" pull-left">
                  	<div class="row">
                  	
                  		<div class="col-sm-3">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/';?>">Back</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                  <center><h3 class="box-title">Appointments of <?php echo $vendorname."(#".$vendorid.")";?></h3></center>
                   <div class="box-tools pull-right">
                  	<div class="row">
                  	
                  		<div class="col-sm-6">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/downloadAppointments/'.$vendorid;?>">Download</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                </div><!-- /.box-header -->
                <br>
              
            <?php if(count($appointments) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="example3" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <!--th>ID</th-->
                      <th>Time of Appointments</th>
                      <th>Service Provider</th>
                      <th>Customer Name</th>
                      <th>Pet Name</th>
                      <th>Pet Breed</th>
                    </tr>
                  </thead>

                     <?php foreach($appointments as $row){
                      $schedultime=date('D,M j,Y',strtotime($row['boardingfromdate']));
                      if($row['boardingfromdate']!="");
                        $schedultime=$schedultime." ".date('G:i',strtotime($row['boardingfromtime']));
                      if(($row['boardingtodate']!="")&&($row['boardingtodate']!='0000-00-00'))
                        $schedultime=$schedultime." to ".date('G:i',strtotime($row['boardingtodate']));
                      else
                        $schedultime=$schedultime." to ".$row['boardingtodate'];
                      if($row['boardingtotime']!="")
                        $schedultime=$schedultime." ".date('G:i',strtotime($row['boardingtotime']));
                      ?>
                      <tbody>
                      <tr>
                      <!--td><?php echo $row['appointmentid'] ?></td-->
                      <td><?php echo $schedultime;?></td>
                      <td><?php echo $row['servicename'];?></td>
                      <td><?php echo $row['firstname']." ".$row['lastname'];?></td>
                      <td><?php echo $row['petname'];?></td>
                      <td><?php echo $row['breedname'];?></td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
              
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links();?>
                  </ul>
                 <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      

      
      
<!-- ----------------------------------------------- -->
        <script type="text/javascript">
      $(document).ready(function(){
    	  $('[data-toggle="tooltip"]').tooltip(); 

        $('#example3').DataTable({
              "paging": true,
              "lengthChange": false,
              "searching": true,
              "info": false,
              "autoWidth": true,
              "iDisplayLength": 20,
              "aoColumnDefs" : [
                            {
                              'bSortable' : false,
                              'aTargets' : [4]
                            }]
            });
      });
      </script>
      
      
    