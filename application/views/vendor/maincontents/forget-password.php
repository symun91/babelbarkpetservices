<!DOCTYPE html>
<html>
<?php
if(ALLOW_VET == 1){
    $logo = 'logo_vet.png';
}else{
    $logo = 'logo_biz.png';
}
?>
  <head> <?php echo $head;?> </head>
  <body class="layout-boxed">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url();?>vendor/home/forgetPassword"><?php echo img(array('src'=>'assets/images/'.$logo,'width'=>'250'));?> <br/> Forget Password ?</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
      <?php if($this->session->flashdata('error_msg')){?><p class="login-box-msg alert alert-danger form-error-msg"><?php echo $this->session->flashdata('error_msg');?></p><?php }?>
      <?php if($this->session->flashdata('success_msg')){?><p class="login-box-msg alert alert-success form-error-msg"><?php echo $this->session->flashdata('success_msg');?></p><?php }?>
         
        <p>If you have forgotten your password - reset it here.</p>
        <p>Enter your login email id below</p>
         
        <?php echo form_open(base_url().'vendor/administrator/forgetPassword',array('method'=>'post','name'=>'loginForm','id'=>'LoginForm'));?>
          <div class="form-group has-feedback">
            <?php echo form_input(array('type'=>'email','name'=>'email','class'=>'form-control','placeholder'=>'Enter Email','id'=>'email'));?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php echo form_error('email');?>
          </div>
          
          <div class="row">
            <div class="col-xs-4">
              <?php echo form_button(array('class'=>"btn btn-primary btn-flat",'id' => 'reset-submit', 'name' => 'reset'), 'Reset Password');?>
            </div><!-- /.col -->
            <div class="col-xs-4"></div>
            <div class="col-xs-4 ">
              <?php //echo anchor(array('class'=>"btn btn-primary btn-flat",'value'=>'Reset Password','name'=>'submit'));?>
              <?php echo anchor('vendor/login','Back to login','class="btn btn-primary btn-flat pull-right"');?>
            </div><!-- /.col -->
          </div>
        <?php echo form_close();?>

       <!--   <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div>--><!-- /.social-auth-links -->

       <!-- <a href="#">I forgot my password</a><br>
         <a href="register.html" class="text-center">Register a new membership</a> -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });

        $(document).on('click', '#reset-submit', function (e) {
          e.preventDefault();

          $('.form-error-msg').remove();

          $(this).addClass('disabled');
          $(this).prop('disabled', true);

          var input = $('#email');

          var error = '<div class="alert alert-danger form-error-msg">';
              error += 'An email address is required.';
              error += '</div>';

          if ($('#email').val().length == 0) {
            $('#email').after(error);
            $(this).removeClass('disabled');
            $(this).prop('disabled', false);
          } else {
            $('#LoginForm').submit();
          }
        });
      });
    </script>
  </body>
</html>
