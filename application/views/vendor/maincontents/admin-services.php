<?php $admin=$this->session->userdata('admin');
					$userrole=$admin['role'];
?>
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Services
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Services</a></li>
            <li class="active">View Services</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
        
       
           
          <br><br> <br><br><br><br> 
            <div class="col-md-12">
            
            
            
              <div class="box">
                <div class="box-header with-border">
                <div class=" pull-left">
                  	<div class="row">
                  	
                  		<div class="col-sm-3">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/';?>">Back</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                  <center><h3 class="box-title">Services of <?php echo $vendorname."(#".$vendorid.")";?></h3></center>
                   <div class="box-tools pull-right">
                  	<div class="row">
                  	
                  		<div class="col-sm-6">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/downloadServices/'.$vendorid;?>">Download</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                </div><!-- /.box-header -->
                <br>
               
            <?php if(count($services) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="example3" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Type</th>
                       <th>Price</th>
                      <th><a href="#" data-toggle="tooltip" title="The estimated time this service requires">Duration</a></th>
                      <th><a href="#" data-toggle="tooltip" title="What time is this service offered?">Availability</a></th>
                      <th><a href="#" data-toggle="tooltip" title=" Are reservations required for this service?">Reservation</a></th>
                      <th>Restrictions</th>
                      <th>Notes</th>
                      <th>UpdatedOn</th>
                    </tr>
                  </thead>
                     <?php  foreach($services as $row) { 
                     
                      ?>
                      <tbody>
                     <tr id="<?php echo $row['serviceid'];;?>">
                     <td><?php echo $row['serviceid'];;?></td>
                     <td><?php echo $row['name'];?></td>
                     <td><?php echo $row['type'];?></td>
                     <td><?php echo $row['price'].'$'.' ('.$row['priceby'].')';?></td>
                     <td><?php echo $row['duration']." ".$row['durationunit'];?></td>
                    	<td><?php 
                    	$availabilitydays= $row['availabilitydays'];
                    	$avafromhour=$row['availabilityfromhour'];
     					$avatohour=$row['availabilitytohour'];
                     	$checkeddays_arr=array();
                     	$avafromhour_arr=array();
                     	$avatohour_arr=array();
                     	$availdays_dispstr="";
				         if($availabilitydays!="" && $availabilitydays!="0")
				         {
				         	$checkeddays_arr=explode(",", $availabilitydays);
				         	$avafromhour_arr=explode(",", $avafromhour);
				         	$avatohour_arr=explode(",", $avatohour);
				         
				         	for($i=0;$i<count($checkeddays_arr);$i++)
				         	{
				         		$availdays_dispstr=$availdays_dispstr."<br>".$checkeddays_arr[$i];
				         		if(count($avafromhour_arr)>$i)
				         			$availdays_dispstr=$availdays_dispstr.": ".$avafromhour_arr[$i]."-".$avatohour_arr[$i];
				         	}
				         }
				         echo $availdays_dispstr;
                    	?></td>
                    	<?php if($row['reservation']==1) 
                    	 		$dispres='Yes';
                    	 	else 
                    	 		$dispres='No';
                    	?>
                    	<td><?php echo $dispres;?></td>
                        
                     
                    	<?php if($row['restriction']==1){
                    		if($row['restrictfromage'] != "" && $row['restricttoage'] != "")
                    			$agerestrinctions=$row['restrictfromage']."-".$row['restricttoage'];
                    		else 
                    			$agerestrinctions='None';
                    		if($row['restrictfromwt']!="" && $row['restricttowt']!="")
                    			$wtrestrinctions=$row['restrictfromwt']."-".$row['restricttowt'];
                    		else 
                    			$wtrestrinctions='None';
                    			
                    	}
                      else
                      {
                        $agerestrinctions='None';
                        $wtrestrinctions='None';
                      }
                    	if ($row['medicalcondnotallowed'] != "")
                      {
                        $medicalcondnotallowed=$row['medicalcondnotallowed']; 
                      }
                      else
                      {
                        $medicalcondnotallowed="None";
                      }
                      	?>

                      <td>
                    	Age Restrictions:&nbsp;<?php echo $agerestrinctions;?><br/>
                    	Weight Restrictions:&nbsp;<?php echo $wtrestrinctions;?><br/>
                    	Medical Conditions Allowed:&nbsp;<?php echo $medicalcondnotallowed;?><br/>
                    	 </td>
                    	 <td><?php echo $row['notes'];?></td>
                    	 <td><?php echo $row['updatedon'];?></td>
                    	 
                    </tr>
                  </tbody>
                    <?php }?>
                 
                 
                    
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
              
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links();?>
                  </ul>
                 <?php } else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      

      
      
<!-- ----------------------------------------------- -->
        <script type="text/javascript">
      $(document).ready(function(){
    	  $('[data-toggle="tooltip"]').tooltip(); 

        $('#example3').DataTable({
              "paging": false,
              "lengthChange": false,
              "searching": true,
              "info": false,
              "autoWidth": true,
              "iDisplayLength": 0,
              "aoColumnDefs" : [
                            {
                              'bSortable' : false,
                              'aTargets' : [5,6,7,8,9]
                            }]
            });
      });
      </script>
      
      
    