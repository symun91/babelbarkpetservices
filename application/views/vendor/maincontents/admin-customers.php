 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Customers
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customers</a></li>
            <li class="active">Customer Listing</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
        
         
        
            <div class="col-md-12">
            
            
            
              <div class="box">
                <div class="box-header with-border">
                
                
                 <div class=" pull-left">
                  	<div class="row">
                  	
                  		<div class="col-sm-3">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/';?>">Back</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                  
                  <center><h3 class="box-title">Customers of <?php echo $vendorname."(#".$vendorid.")";?></h3></center>
                  
                 
                  
                  <div class="box-tools pull-right">
                  	<div class="row">
                  	
                  		<div class="col-sm-6">
                  			<div class="form-group">
						 		<a class="btn bg-orange" href="<?php echo base_url().'admin/home/downloadCustomers/'.$vendorid;?>">Download</a>
						 	</div>
                  		</div>
                  	</div>
                  </div>
                </div><!-- /.box-header -->
                <br>
              
            <?php if(count($customers) > 0) {?>
                <div class="box-body table-responsive" style="padding: 10px 15px;">
                  <table id="example3" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Customer name</th>
                      <th>BabelBark User</th>
                      <th>Customer Contact Information</th>
                      <th>Pet Name</th>
                      <th>Notes</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    	$count = 1;
                    	foreach($customers as $row) { 
                    		if($row['usertype'] == 'Generic Customer'){
                    ?>
                    
                     <tr id="<?php echo $row['customerid'];?>">
                    	<td><?php echo $count;?></td>
                    <td><?php echo $row['firstname'].' '.$row['lastname'];?></td>
                    	<td>&nbsp;</td>
                      <?php 
                      if($row['phoneno'] == 0)
                        $row['phoneno'] = "";
                      ?>
                    	<td>Email Address: <?php echo $row['email'];?><br>Phone No: <?php echo $row['phoneno'];?><br>Address: <?php echo $row['address'];?><br> <?php echo $row['city'];?>,<?php echo $row['state'];?></td>
                    	<td><?php echo $row['petname'];?></td>
                    	<td><?php echo $row['notes'];?></td>
                    	 </tr>
                    <?php } else { ?>
                    <tr id="<?php echo $row['appuserid'];?>">
                    	<td><?php echo $count;?></td>
                    	<td><?php echo $row['firstname'].' '.$row['lastname'];?></td>
                    	<td> <center><img src="<?php echo base_url();?>assets/images/app_logo.png" style="width: 35px; height: 35px;" /></center>
        </td>
                    <?php 
                      if($row['phonenumber'] == 0)
                        $row['phonenumber'] = "";
                      ?>
                    	<td>Email Address: <?php echo $row['email'];?><br>Phone No: <?php echo $row['phonenumber'];?><br>Address: <?php echo $row['address'];?><br> <?php echo $row['city'];?>,<?php echo $row['state'];?></td>
                    	<td><?php echo $row['petname'];?></td>
                    	<td>&nbsp;</td>
                    	  </tr>
                    
                    <?php }
                    $count++;
                    }?>
                  
                  </tbody>  
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
              
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <?php //echo $this->pagination->create_links();?>
                  </ul>
                  <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
          
   
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
     
     
     
     
       <div id="generic" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;"> Invitation Email Preview</h4>
      </div>
     <section class="content">
         
    	    <div class="box">
    	  <div class="row">
    	  
    	  <div class="col-lg-9 col-xs-12 ">
    	
    	  <p>
    	  <?php $admin=$this->session->userdata('admin');
		$vendorid=$admin['adminDetails']['vendorid'];
		$vendorfname=$admin['adminDetails']['firstname'];
		$vendorlname=$admin['adminDetails']['lastname'];
		$sendmessage  = file_get_contents('resources/invitemail_template.html'); 
 		$sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage); 
		$sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage); 
		echo $sendmessage;
		?>
		</p>
		</div>
		</div>
		
    	  </div>
    	</section> 
    </div>
    </div>
  </div>
 
  
  
  <div id="generic1" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;"> Invitation Email Preview</h4>
      </div>
     <section class="content">
         
    	    <div class="box">
    	  <div class="row">
    	  
    	  <div class="col-lg-9 col-xs-12 ">
    	
    	  <p>
    	  <div id="previewdiv"></div>
		</p>
		</div>
		</div>
		
    	  </div>
    	</section> 
    </div>
    </div>
  </div>
      
   <!-- ----------------------------------------------- -->   
      
      
<!-- ----------------------------------------------- -->
      
      
      
      <script type="text/javascript">
     
	
      
      $(document).ready(function(){
    	  $('#example3').DataTable({
              "paging": true,
              "lengthChange": false,
              "searching": true,
              "info": false,
              "autoWidth": true,
              "iDisplayLength": 20,
              "aoColumnDefs" : [
                            {
                              'bSortable' : false,
                              'aTargets' : [3,5]
                            }]
            });


    	

      });
      function validateEmail(email) {
    	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    	  return emailReg.test(email );
    	}
      function inviteUser()
		{
  		
    	  var email = $("#inviteEmail").val();
    
  		
    	  if (email == '')
    	  {
    		 
    		 $("#validerror").html('Enter Email ID');

    	  }
    	  else if( !validateEmail(email)) {
    		 $("#validerror").html('Enter valid Email ID');
    		 
    	  }
    	  else
    	  {
    		  $("#validerror").html('');
    		  var url = "<?php echo base_url();?>vendor/customer/inviteUser";
        	  
    		  $.post(url, {email : email} , function(data)
  					{
  						if(data == '1')
  						{
  							$("#inviteEmail").val('');
  							alert("Invitation mail sent successfully to "+email);
  						}
  						else
  						{
  							alert("Some problem occured while sending mail!");
  						}
  					});
    	  }
		}

		function profileImportInvite()
		{
			  var email = $("#inviteEmailProfImport").val();
	    	  
	    	
	    	  if (email == '')
	    	  {
	    		 
	    		 $("#validerror1").html('Enter Email ID');

	    	  }
	    	  else if( !validateEmail(email)) {
	    		 $("#validerror1").html('Enter valid Email ID');
	    		 
	    	  }
	    	  else
	    	  {
	    		  $("#validerror1").html('');
	    		  var url = "<?php echo base_url();?>vendor/customer/inviteUserForProfileImport";
	        	  
	    		  $.post(url, {email : email} , function(data)
	  					{
	  				 
	  						if(data == '1')
	  						{
	  							$("#inviteEmailProfImport").val('');
	  							alert("Invitation mail sent successfully to "+email);
	  						}
	  						else if(data == '2')
	  						{
	  							alert("BabelBark User with this email id doen't exist!");
	  						}
	  						else if(data == '3')
	  						{
	  							alert("You have already sent the invitation to this user!");
	  						}
	  						else
	  						{
	  							alert("Some problem occured while sending mail!");
	  						}
	  					});
	    	  }

		}

		function showEmailPreview()
		{
			  var email = $("#inviteEmailProfImport").val();
			  if (email == '')
	    	  {
	    		 
	    		 $("#validerror1").html('Enter Email ID');

	    	  }
	    	  else if( !validateEmail(email)) {
	    		 $("#validerror1").html('Enter valid Email ID');
	    		 
	    	  }
	    	  else
	    	  {
	    		  $("#validerror1").html('');
			var url = "<?php echo base_url();?>vendor/customer/profileImportPriviewEmail";
      	  
  		  $.post(url, {email:email} , function(data)
					{

  			if(data == '2')
				{
  				
					alert("BabelBark User with this email id doen't exist!");
				}
				else 
				{
					
					$("#previewdiv").html(data);
					$('#generic1').modal('show');
				}
				      
					});
	    	  }
		}

		
		function filterUser(object){
			console.log(object);
			if(object != ''){
				var basePsth = "<?php echo base_url();?>vendor/customer/listofgenericcustomer/"+object;
				window.location.href = basePsth;
			}
		}
      </script>
