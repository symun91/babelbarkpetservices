<style>
    .box{
        border-top: 0 !important;
    }
    #updateInfo_s1, .close_s1 {
        margin-right: 2%;
    }
</style>
<div id="spinner"
     style="position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url(); ?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($appointmentdetail['appointmentid'] == 0)
                echo "Add Appointment";
            else
                echo "Appointment Detail";
            ?>
        </h1>
        <?php if ($appointmentdetail['appointmentid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Appointment</a></li>
                <li class="active">Add Appointment</a></li>
            </ol>
        <?php } ?>
    </section>
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <form name="addappointmentform" id="addappointmentform"  action="<?php echo base_url(); ?>vendor/appointments/updateAppointment" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="appointmentid" id="appointmentid" value="<?php echo $appointmentdetail['appointmentid']; ?>"/>
            <input type="hidden" name="customertype" id="customertype"   value="<?php echo $appointmentdetail['customertype']; ?>"/>
            <div class="box">
                <div class="row">
                    <div class="col-md-12">
                        <form role="form" action="" method="post">
                            <div class="stepwizard col-md-offset-3">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                        <p>Book Appointment</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle" <?php echo $appointmentdetail['appointmentid'] == 0 ? 'disabled="disabled"' : ''; ?>>2</a>
                                        <p>Customer</p>
                                    </div>
                                </div>
                            </div>
                            <!--############################# STEP 1 start ##########################################-->
                            <div class="row setup-content" id="step-1">
                                <div class="col-xs-12 col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"></h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="form-group">
                                                    <label>Service Name<font color="red">*</font></label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <select class="form-control" name="service" id="service" onchange="checkassign_emp_status()">
                                                                <option selected="" value="0">Select Service Name</option>
                                                                <?php for ($i = 0; $i < count($services); $i++) { ?>
                                                                    <option value="<?php echo $services[$i]['serviceid']; ?>" <?php echo set_select('service', $services[$i]['serviceid'], ( $appointmentdetail['serviceid'] == $services[$i]['serviceid'] ? TRUE : FALSE)); ?>><?php echo $services[$i]['name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $uid = $appointmentdetail['userid'];
                                        ?>
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-9" <?php
                                                        if (isset($uid) AND ( $uid > 0)) {
                                                            echo 'id="showemp"';
                                                        } else {
                                                            echo 'id="employeeinfo"';
                                                        }
                                                        ?>>
                                                            <label>Select Employee</label>
                                                            <select class="form-control" name="userid" id="userid">
                                                                <option selected="" value="0">Select</option>
                                                                <?php for ($i = 0; $i < count($vendorEmployees); $i++) { ?>
                                                                    <option value="<?php echo $vendorEmployees[$i]['userid']; ?>" <?php echo set_select('userid', $vendorEmployees[$i]['userid'], ( $appointmentdetail['userid'] == $vendorEmployees[$i]['userid'] ? TRUE : FALSE)); ?>><?php echo $vendorEmployees[$i]['firstname']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $estimateddur = $appointmentdetail['estimatedduration'];
                                        $durationhr = "";
                                        $durationmin = "";
                                        if ($estimateddur != "") {

                                            $temparr1 = explode("Hr", $estimateddur);
                                            if (count($temparr1) > 0) {
                                                if ($temparr1[0] != "")
                                                    $durationhr = $temparr1[0];
                                                else
                                                    $durationhr = "";
                                            } else
                                                $durationhr = "";

                                            if (count($temparr1) > 1) {

                                                if ($temparr1[1] != "") {
                                                    $temparr2 = explode("Min", $temparr1[1]);
                                                    if (count($temparr2) > 0 && $temparr2[0] != "")
                                                        $durationmin = $temparr2[0];
                                                } else
                                                    $durationmin = "";
                                            }
                                            else {
                                                $durationmin = "";
                                            }
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label>Estimated Duration</label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <input type="text" class="form-control allownumericwithoutdecimal" name="estimatedduration" id="estimatedduration" value="<?php echo set_value('estimatedduration', $serviceInfo['duration']); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <form id="durunit">
                                                    Days  <input type="radio" class="durationunits" name="durationunit"  value="days" <?php echo set_value('durationunit', $appointmentdetail['durationunit']) == "days" ? "checked" : ""; ?> />
                                                    &nbsp;&nbsp;Hours <input type="radio" class="durationunits" name="durationunit"  value="hours"  <?php echo set_value('durationunit', $appointmentdetail['durationunit']) == "hours" ? "checked" : ""; ?>/>
                                                    &nbsp;&nbsp;Minutes  <input type="radio" class="durationunits" name="durationunit"  value="minutes" <?php echo set_value('durationunit', $appointmentdetail['durationunit']) == "minutes" ? "checked" : ""; ?> />
                                                </form>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label>Start Date<font color="red">*</font></label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <?php
                                                            $sdate = $appointmentdetail['boardingfromdate'];
                                                            if ($sdate == '') {
                                                                $startdate = "";
                                                            } else {
                                                                $startdate = date('F d , Y', strtotime($sdate));
                                                            }
                                                            ?>
                                                            <input class="form-control datepicker"  name="startdate" id="startdate" data-date-format="MM dd, yyyy" value="<?php echo set_value('startdate', $startdate); ?>">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <?php
                                                            $edate = $appointmentdetail['boardingtodate'];
                                                            if ($edate == '') {
                                                                $enddate = "";
                                                            } else {
                                                                $enddate = date('F d , Y', strtotime($edate));
                                                            }
                                                            ?>
                                                            <input class="form-control datepicker" data-provide="datepicker"  name="enddate" id="enddate" data-date-format="MM dd, yyyy" value="<?php echo set_value('enddate', $enddate); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="col-xs-5">
                                                    <div class="form-group">
                                                        <label>Start Time<font color="red">*</font></label>
                                                        <?php
                                                        if ($appointmentdetail['boardingfromtime'] != '') {
                                                            $boardingfromtime = explode(":", $appointmentdetail['boardingfromtime']);
                                                            if ($boardingfromtime[0] > 12) {
                                                                $boardingfromtime[0] = $boardingfromtime[0] - 12;
                                                                $appointmentdetail['boardingfromtime'] = $boardingfromtime[0] . ':' . $boardingfromtime[1] . ' ' . "PM";
                                                            } else if ($boardingfromtime[0] != 12) {
                                                                $appointmentdetail['boardingfromtime'] = $boardingfromtime[0] . ':' . $boardingfromtime[1] . ' ' . "AM";
                                                            } else {
                                                                $appointmentdetail['boardingfromtime'] = $boardingfromtime[0] . ':' . $boardingfromtime[1] . ' ' . "PM";
                                                            }
                                                        }
                                                        ?>
                                                        <div class="input-group bootstrap-timepicker timepicker">
                                                            <input id="starttime" name="starttime" type="text" class="form-control input-small" value="<?php echo set_value('starttime', $appointmentdetail['boardingfromtime']); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-5">
                                                    <div class="form-group">
                                                        <label>End Time<font color="red">*</font></label>
                                                        <?php
                                                        if ($appointmentdetail['boardingtotime'] != '') {
                                                            $boardingtotime = explode(":", $appointmentdetail['boardingtotime']);
                                                            if ($boardingtotime[0] > 12) {
                                                                $boardingtotime[0] = $boardingtotime[0] - 12;
                                                                $appointmentdetail['boardingtotime'] = $boardingtotime[0] . ':' . $boardingtotime[1] . ' ' . "PM";
                                                            } else if ($boardingtotime[0] != 12) {
                                                                $appointmentdetail['boardingtotime'] = $boardingtotime[0] . ':' . $boardingtotime[1] . ' ' . "AM";
                                                            } else if ($boardingfromtime[0] > 12) {
                                                                $appointmentdetail['boardingtotime'] = $boardingtotime[0] . ':' . $boardingtotime[1] . ' ' . "AM";
                                                            } else {
                                                                $appointmentdetail['boardingtotime'] = $boardingtotime[0] . ':' . $boardingtotime[1] . ' ' . "PM";
                                                            }
                                                        }
                                                        ?>
                                                        <div class="input-group bootstrap-timepicker timepicker">
                                                            <input id="endtime" name="endtime" type="text" class="form-control input-small" value="<?php echo set_value('endtime', $appointmentdetail['boardingtotime']); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                                <?php if ($appointmentdetail['status'] == 0) { ?>
                    <!--                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="updateInfo_s1" value="Save add">-->
                                                <?php } elseif ($appointmentdetail['status'] == 1) { ?>
                                                    <input type="hidden" name="status" id="status" value="<?php echo $appointmentdetail['status']; ?>">
                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="updateInfo_s1" value="Schedule">
                                                    <?php echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"'); ?>
                                                <?php } elseif ($appointmentdetail['status'] == 4) { ?>
                                                    <input type="hidden" name="status" id="status" value="<?php echo $appointmentdetail['status']; ?>">
                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="updateInfo_s1" value="Schedule">
                                                    <?php echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"'); ?>
                                                <?php } else { ?>
                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="updateInfo_s1" value="Save">
                                                    <?php
                                                    echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--############################# STEP 1 end ##########################################-->
                            <!--############################# STEP 2 start ##########################################-->
                            <div class="row setup-content" id="step-2">
                                <div class="col-xs-6 col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"></h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <label>Customer Name<font color="red">*</font></label>
                                                            <input type="text" id="selectcustomer"  name="selectcustomer" class="form-control allowcharacterwithouthtmltags" placeholder="Start typing Customer Name to see list of available customers for selection"/>

                                                            <?php if ($appointmentdetail['customertype'] == 'customer') { ?>

                                                                <input type="hidden" id="selectcustomerid"  name="selectcustomerid" value="<?php echo $appointmentdetail['customerid']; ?>"/>

                                                            <?php } else { ?>
                                                                <input type="hidden" id="selectcustomerid"  name="selectcustomerid" value="<?php echo $appointmentdetail['appuserid']; ?>"/>
                                                            <?php } ?>
                                        <!-- <select class="form-control" name="selectcustomer" id="selectcustomer">
<option value="">Select</option>
                                                            <?php
                                                            for ($i = 0; $i < count($customers); $i++) {
                                                                $customername = $customers[$i]['firstname'] . " " . $customers[$i]['lastname'];
                                                                if ($customers[$i]['usertype'] == 'customer') {
                                                                    ?>
                                                                                        <option value="<?php echo $customers[$i]['customerid']; ?>" <?php echo set_select('selectcustomer', $customers[$i]['customerid'], ( $appointmentdetail['customerid'] == $customers[$i]['customerid'] ? TRUE : FALSE)); ?>><?php echo $customername; ?></option>
                                                                                                            
                                                                <?php } else { ?>
                                                                                        <option value="<?php echo $customers[$i]['appuserid']; ?>" <?php echo set_select('selectcustomer', $customers[$i]['appuserid'], ( $appointmentdetail['appuserid'] == $customers[$i]['appuserid'] ? TRUE : FALSE)); ?>><?php echo $customername; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>

</select> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label>Pet Name<font color="red">*</font></label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <select class="form-control" id="selectpet" name="selectpet">
                                                                <option value="">Select Pet Name</option>
                                                            </select>
                                                            <input type="hidden" name="petid" id="petid" value="<?php echo $appointmentdetail['petid']; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label> Notes</label>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <textarea rows="5" class="form-control countable" name="bookingnote" placeholder="Notes" data-length="1500"><?php echo $appointmentdetail['bookingnote']; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-9 col-xs-12 ">
                                                <div class="form-group">
                                                    <label for="">Pickup/Dropoff </label>&nbsp;&nbsp;
                                                    <input type="checkbox" name="pickdrop" value="1" <?php echo ($appointmentdetail['pickdrop'] == '1' ? 'checked' : null); ?>/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Customer Reminder</label>
                                                    <input type="checkbox" name="reminderneeded" value="1" <?php echo ($appointmentdetail['reminderrequired'] == '1' ? 'checked' : null); ?>/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                            <?php if ($appointmentdetail['status'] == 0) { ?>
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Save">
                                            <?php } elseif ($appointmentdetail['status'] == 1) { ?>
                                                <input type="hidden" name="status" id="status" value="<?php echo $appointmentdetail['status']; ?>">
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Schedule">
                                            <?php } elseif ($appointmentdetail['status'] == 4) { ?>
                                                <input type="hidden" name="status" id="status" value="<?php echo $appointmentdetail['status']; ?>">
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Schedule">
                                            <?php } else { ?>
                                                <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Update">
                                                <?php echo anchor('vendor/appointments/listofappointments', 'Close', 'class="btn  btn-danger pull-right close_s1"'); ?>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--############################# STEP 2 end ##########################################-->
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>

<script type="text/javascript">

    $(function () {
        var customerslist = <?php echo json_encode($customers); ?>;
        var customers = new Array();
        var cust = [{label: "Choice1", value: "value1"}, {label: "Choiasd1", value: "value1"}, {label: "Choi", value: "value1"}];
        for (var i = 0; i < customerslist.length; i++)
        {


            if (customerslist[i]['usertype'] === 'customer')
            {
                customers.push({value: customerslist[i]['firstname'].concat(' ', customerslist[i]['lastname']), label: customerslist[i]['firstname'].concat(' ', customerslist[i]['lastname']), id: customerslist[i]['customerid']});
            } else {
                customers.push({value: customerslist[i]['firstname'].concat(' ', customerslist[i]['lastname']), label: customerslist[i]['firstname'].concat(' ', customerslist[i]['lastname']), id: customerslist[i]['appuserid']});
            }

            //customers.push(customer);
        }

//        $("#selectcustomer").autocomplete({  // BUG FOUND
//        });

        $("#selectcustomer").autocomplete({
            source: customers,
            select: function (a, b) {
                $(this).val(b.item.id);
                var customerid = b.item.id;
                var count = 0;
                for (var j = 0; j < customerslist.length; j++)
                {
                    if (customerslist[j]['usertype'] == 'customer')
                    {
                        if (customerid == customerslist[j]['customerid'])
                        {
                            count++;
                        }
                    } else
                    {
                        if (customerid == customerslist[j]['appuserid'])
                        {
                            count++;
                        }
                    }
                }
                if (count != 0)
                {
                    // console.log(b.item.value);
                    $('#selectcustomerid').val(customerid);
                    var selectedservice = $('#service').find(":selected").val();
                    if (selectedservice == 0)
                    {
                        alert('Please select Service Provided first.');
                        return;
                    }

                    var matchcount = 0;
                    var jqueryarray = <?php echo json_encode($customers); ?>;
                    var jqueryarray1 = <?php echo json_encode($pets); ?>;
                    var jqueryServices = <?php echo json_encode($services); ?>;
                    var selectedservicedetails;
                    for (var i = 0; i < jqueryServices.length; i++) {
                        if (selectedservice == jqueryServices[i]['serviceid'])
                        {
                            selectedservicedetails = jqueryServices[i];
                            break;
                        }
                    }
                    console.log(selectedservicedetails);
                    if (selectedservicedetails)
                    {
                        var validPets = 0;
                        for (var i = 0; i < jqueryarray.length; i++) {

                            if (jqueryarray[i]['customerid'] == customerid || jqueryarray[i]['appuserid'] == customerid)
                            {

                                matchcount = i;
                                $('#customertype').val(jqueryarray[matchcount]['usertype']);
                                // console.log(jqueryarray1[matchcount]);
                                var petarray = jqueryarray1[matchcount];
                                $("#selectpet").empty();
                                $("#selectpet").append($('<option>', {
                                    value: "",
                                    text: "Select"
                                }));

                                if (Object.prototype.toString.call(petarray) === '[object Array]')  //// If this customer have multiple pets.
                                {
                                    var name = "";
                                    var val = "";
                                    var agelimitvalid = true, wtlimitvalid = true;
                                    for (var j = 0; j < petarray.length; j++)
                                    {

                                        if (customerid != petarray[j]['customer_id']) {
                                            continue;
                                        }
                                        var birthdate = new Date(petarray[j]['birthdate']);
                                        var petage = _calculateAge(birthdate);
                                        agelimitvalid = true;
                                        wtlimitvalid = true;
                                        var petcurrentweight = petarray[j]['currentweight'];

                                        ///////////// Pet Age Check
                                        switch (selectedservicedetails.restrictagelimit)
                                        {
                                            case "inbetween":
                                                if (selectedservicedetails.restrictfromage && selectedservicedetails.restricttoage)
                                                {
                                                    var restrictfromage = selectedservicedetails.restrictfromage;
                                                    var year = restrictfromage.substr(0, restrictfromage.indexOf('years'));
                                                    var month = restrictfromage.substring(restrictfromage.lastIndexOf("years") + 5, restrictfromage.lastIndexOf("months"));

                                                    var serviceagelimitfrom = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                    serviceagelimitfrom = parseFloat(serviceagelimitfrom);
                                                    var restricttoage = selectedservicedetails.restricttoage;
                                                    var year = restricttoage.substr(0, restricttoage.indexOf('years'));
                                                    var month = restricttoage.substring(restricttoage.lastIndexOf("years") + 5, restricttoage.lastIndexOf("months"));
                                                    var serviceagelimitto = ((isNumber(year) ? parseFloat(year) : 0) + ((isNumber(month) ? parseFloat(month) : 0) / 10));
                                                    serviceagelimitto = parseFloat(serviceagelimitto);
                                                    if (serviceagelimitto > serviceagelimitfrom) {
                                                        if (!(petage >= serviceagelimitfrom && petage <= serviceagelimitto)) {
                                                            agelimitvalid = false;
                                                        }
                                                    } else {
                                                        if (!(petage >= serviceagelimitfrom)) {
                                                            agelimitvalid = false;
                                                        }
                                                    }


                                                }

                                                break;
                                            case "atleast":
                                                if (selectedservicedetails.restrictfromage)
                                                {
                                                    var restrictfromage = selectedservicedetails.restrictfromage;
                                                    var year = restrictfromage.substr(0, restrictfromage.indexOf('years'));
                                                    var month = restrictfromage.substring(restrictfromage.lastIndexOf("years") + 5, restrictfromage.lastIndexOf("months"));
                                                    var birthdate = new Date(petarray[j]['birthdate']);
                                                    var petage = _calculateAge(birthdate);
                                                    var serviceagelimitfrom = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                    serviceagelimitfrom = parseFloat(serviceagelimitfrom);
                                                    if (!(petage >= serviceagelimitfrom)) {
                                                        agelimitvalid = false;
                                                    }


                                                }

                                                break;
                                            case "atmost":
                                                if (selectedservicedetails.restricttoage)
                                                {
                                                    var birthdate = new Date(petarray[j]['birthdate']);
                                                    var petage = _calculateAge(birthdate);
                                                    var restricttoage = selectedservicedetails.restricttoage;
                                                    var year = restrictfromage.substr(0, restricttoage.indexOf('years'));
                                                    var month = restrictfromage.substring(restricttoage.lastIndexOf("years") + 5, restricttoage.lastIndexOf("months"));
                                                    var serviceagelimitto = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                    serviceagelimitto = parseFloat(serviceagelimitto);
                                                    if (!(petage <= serviceagelimitto)) {
                                                        agelimitvalid = false;
                                                    }

                                                }

                                                break;
                                        }

                                        ///////////// Pet Weight Check
                                        switch (selectedservicedetails.restrictweightlimit)
                                        {
                                            case "inbetween":
                                                if (selectedservicedetails.restrictfromwt && selectedservicedetails.restricttowt)
                                                {
                                                    var fromwt = parseFloat(selectedservicedetails.restrictfromwt);
                                                    var towt = parseFloat(selectedservicedetails.restricttowt);
                                                    if (isNumber(petcurrentweight))
                                                    {
                                                        if (!(parseFloat(petcurrentweight) >= fromwt && parseFloat(petcurrentweight) <= towt))
                                                        {
                                                            wtlimitvalid = false;
                                                        }
                                                    }
                                                }

                                                break;
                                            case "atleast":

                                                if (selectedservicedetails.restrictfromwt)
                                                {
                                                    var fromwt = parseFloat(selectedservicedetails.restrictfromwt);

                                                    if (isNumber(petcurrentweight))
                                                    {
                                                        if (!(parseFloat(petcurrentweight) >= fromwt))
                                                        {
                                                            wtlimitvalid = false;
                                                        }
                                                    }
                                                }

                                                break;
                                            case "atmost":

                                                if (selectedservicedetails.restricttowt)
                                                {
                                                    var fromwt = parseFloat(selectedservicedetails.restrictfromwt);
                                                    var towt = parseFloat(selectedservicedetails.restricttowt);
                                                    if (isNumber(petcurrentweight))
                                                    {
                                                        if (!(parseFloat(petcurrentweight) <= towt))
                                                        {
                                                            wtlimitvalid = false;
                                                        }
                                                    }

                                                }

                                                break;
                                        }


                                        if (agelimitvalid && wtlimitvalid)
                                        {
                                            name = petarray[j]['name'];
                                            val = petarray[j]['petid'];

                                            $('#selectpet').append($('<option>', {
                                                value: val,
                                                text: name
                                            }));
                                            validPets++;
                                        }

                                    }
                                } else
                                {
                                    if (customerid != petarray.customer_id) {
                                        continue;
                                    }

                                    var birthdate = new Date(petarray.birthdate);
                                    var petage = _calculateAge(birthdate);
                                    agelimitvalid = true;
                                    wtlimitvalid = true;
                                    var petcurrentweight = petarray.currentweight;

                                    ///////////// Pet Age Check
                                    switch (selectedservicedetails.restrictagelimit)
                                    {
                                        case "inbetween":
                                            if (selectedservicedetails.restrictfromage && selectedservicedetails.restricttoage)
                                            {
                                                var restrictfromage = selectedservicedetails.restrictfromage;
                                                var year = restrictfromage.substr(0, restrictfromage.indexOf('years'));
                                                var month = restrictfromage.substring(restrictfromage.lastIndexOf("years") + 5, restrictfromage.lastIndexOf("months"));

                                                var serviceagelimitfrom = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                serviceagelimitfrom = parseFloat(serviceagelimitfrom);
                                                var restricttoage = selectedservicedetails.restricttoage;
                                                var year = restricttoage.substr(0, restricttoage.indexOf('years'));
                                                var month = restricttoage.substring(restricttoage.lastIndexOf("years") + 5, restricttoage.lastIndexOf("months"));
                                                var serviceagelimitto = ((isNumber(year) ? parseFloat(year) : 0) + ((isNumber(month) ? parseFloat(month) : 0) / 10));
                                                serviceagelimitto = parseFloat(serviceagelimitto);
                                                if (serviceagelimitto > serviceagelimitfrom) {
                                                    if (!(petage >= serviceagelimitfrom && petage <= serviceagelimitto)) {
                                                        agelimitvalid = false;
                                                    }
                                                } else {
                                                    if (!(petage >= serviceagelimitfrom)) {
                                                        agelimitvalid = false;
                                                    }
                                                }


                                            }

                                            break;
                                        case "atleast":
                                            if (selectedservicedetails.restrictfromage)
                                            {
                                                var restrictfromage = selectedservicedetails.restrictfromage;
                                                var year = restrictfromage.substr(0, restrictfromage.indexOf('years'));
                                                var month = restrictfromage.substring(restrictfromage.lastIndexOf("years") + 5, restrictfromage.lastIndexOf("months"));
                                                var birthdate = new Date(petarray[j]['birthdate']);
                                                var petage = _calculateAge(birthdate);
                                                var serviceagelimitfrom = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                serviceagelimitfrom = parseFloat(serviceagelimitfrom);
                                                if (!(petage >= serviceagelimitfrom)) {
                                                    agelimitvalid = false;
                                                }


                                            }

                                            break;
                                        case "atmost":
                                            if (selectedservicedetails.restricttoage)
                                            {
                                                var birthdate = new Date(petarray[j]['birthdate']);
                                                var petage = _calculateAge(birthdate);
                                                var restricttoage = selectedservicedetails.restricttoage;
                                                var year = restrictfromage.substr(0, restricttoage.indexOf('years'));
                                                var month = restrictfromage.substring(restricttoage.lastIndexOf("years") + 5, restricttoage.lastIndexOf("months"));
                                                var serviceagelimitto = isNumber(year) ? parseFloat(year) : 0 + ((isNumber(month) ? parseFloat(month) : 0) / 10);
                                                serviceagelimitto = parseFloat(serviceagelimitto);
                                                if (!(petage <= serviceagelimitto)) {
                                                    agelimitvalid = false;
                                                }

                                            }

                                            break;
                                    }

                                    ///////////// Pet Weight Check
                                    switch (selectedservicedetails.restrictweightlimit)
                                    {
                                        case "inbetween":
                                            if (selectedservicedetails.restrictfromwt && selectedservicedetails.restricttowt)
                                            {
                                                var fromwt = parseFloat(selectedservicedetails.restrictfromwt);
                                                var towt = parseFloat(selectedservicedetails.restricttowt);
                                                if (isNumber(petcurrentweight))
                                                {
                                                    if (!(parseFloat(petcurrentweight) >= fromwt && parseFloat(petcurrentweight) <= towt))
                                                    {
                                                        wtlimitvalid = false;
                                                    }
                                                }
                                            }

                                            break;
                                        case "atleast":

                                            if (selectedservicedetails.restrictfromwt)
                                            {
                                                var fromwt = parseFloat(selectedservicedetails.restrictfromwt);

                                                if (isNumber(petcurrentweight))
                                                {
                                                    if (!(parseFloat(petcurrentweight) >= fromwt))
                                                    {
                                                        wtlimitvalid = false;
                                                    }
                                                }
                                            }

                                            break;
                                        case "atmost":

                                            if (selectedservicedetails.restricttowt)
                                            {
                                                var fromwt = parseFloat(selectedservicedetails.restrictfromwt);
                                                var towt = parseFloat(selectedservicedetails.restricttowt);
                                                if (isNumber(petcurrentweight))
                                                {
                                                    if (!(parseFloat(petcurrentweight) <= towt))
                                                    {
                                                        wtlimitvalid = false;
                                                    }
                                                }

                                            }
                                            break;
                                    }

                                    if (agelimitvalid && wtlimitvalid)
                                    {
                                        $('#selectpet').append($('<option>', {
                                            value: petarray.petid,
                                            text: petarray.name
                                        }));

                                        validPets++;
                                    }

                                }
                                if (validPets == 0)
                                {
                                    alert('No pets found matching the service requirements of selected customer.');
                                }
                                break;
                            }
                        }
                    } else
                    {
                        alert('Please select Service Provided first.');
                        return;
                    }
                } else
                {
                    $("#validerrorform").html('Customer Name is not exist!');
                }

            }
        });
    });

    $(window).load(function () {
        $("#validation").hide();
        var customerid = $('#selectcustomerid').val();
        var jqueryarray = <?php echo json_encode($customers); ?>;
        var jqueryarray1 = <?php echo json_encode($pets); ?>;
        var matchcount = 0;
        for (var i = 0; i < jqueryarray.length; i++)
        {
            if (jqueryarray[i]['customerid'] == customerid || jqueryarray[i]['appuserid'] == customerid)
            {
                matchcount = i;
                var name = jqueryarray[matchcount]['firstname'].concat(' ', jqueryarray[matchcount]['lastname']);
                $('#selectcustomer').val(name);
                var petarray = jqueryarray1[matchcount];
                if (typeof petarray !== 'undefined') {
                    if (petarray.petid == '<?php echo $appointmentdetail['petid'] ?>') {
                        $('#selectpet').append($('<option>', {
                            value: petarray.petid,
                            text: petarray.name,
                            selected: "selected"
                        }));
                    }
                }
                for (var j = 0; j < petarray.length; j++) {

                    if (petarray[j]['petid'] == '<?php echo $appointmentdetail['petid'] ?>')
                    {
                        $('#selectpet').append($('<option>', {
                            value: petarray[j]['petid'],
                            text: petarray[j]['name'],
                            selected: "selected"
                        }));
                    } else
                    {
                        if (customerid != petarray[j]['customer_id']) {
                            continue;
                        }
                        $('#selectpet').append($('<option>', {
                            value: petarray[j]['petid'],
                            text: petarray[j]['name']
                        }));
                    }
                }
                break;
            }

        }
        $("#spinner").fadeOut("slow");
        $(function () {
            if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
                var datepicker = $.fn.datepicker.noConflict();
                $.fn.bootstrapDP = datepicker;
            }
        });
    });



    $(document).ready(function () {


        $('.datepicker').datepicker({
            startDate: new Date(),
            format: "MM dd, yyyy"
        });

        $('#selectpet').change(function () {
            $(this).find("option:selected").each(function () {
                $('#petid').val(this.value);
            });
        });

        /*  $("#dialog").dialog({
         autoOpen: false,
         modal: true
         });*/
        $('#service').change(function () {
            /*$(this).find("option:selected").each(function(){
             if( $(this).attr("value") == '2')
             {
             $("#estimatedduration").val("1");
             //$("#estimatedduration").attr("disabled","disabled");
             //$(".durationunits").attr("disabled","disabled");
             }
             else
             {
             $("#estimatedduration").val("");
             //$("#estimatedduration").removeAttr("disabled","disabled");
             //$(".durationunits").removeAttr("disabled","disabled");
             }
             });*/
            var selserviceid = this.value;

<?php for ($i = 0; $i < count($services); $i++) { ?>
                var serviceiddb =<?php echo $services[$i]['serviceid'] ?>;
                if (selserviceid == serviceiddb)
                {
                    serviceduration = "<?php echo $services[$i]['duration'] ?>";
                    servicedurationunit = "<?php echo $services[$i]['durationunit'] ?>";

                }
<?php } ?>
            $("#estimatedduration").val(serviceduration);
            $("input[value='" + servicedurationunit + "']").prop('checked', true);



            if ($("#service").val() == "" || $("#service").val() == "Other")
            {
                $("#estimatedduration").val("");
                $("input[value=hours]").prop('checked', true);
            }


        });

        var appointmentid = "<?php echo $appointmentdetail['appointmentid']; ?>";
        if(appointmentid > '0'){ 
            onStartTimeChange();
        }
        $("#starttime").timepicker({showMeridian: true});
        $("#endtime").timepicker();
        //$("#endtime").timepicker({defaultTime: false});

        $("#service").on('change', function () {

            $('#startdate').bind('changeDate', onDateChange);
            $('#startdate').bind('input', onDateChange);


            $('#starttime').bind('changeTime.timepicker', onStartTimeChange);

            $('#starttime').bind('change', onStartTimeChange);
            //onStartTimeChange();
        });

        var serviceduration = "";
        var servicedurationunit = "";

        var petid = "<?php echo $appointmentdetail['petid']; ?>";
        var jqueryarray1 = <?php echo json_encode($pets); ?>;
        for (var i = 0; i < jqueryarray1.length; i++) {
            if (jqueryarray1[i]['petid'] == petid)
            {
                $('#petname').val(jqueryarray1[i]['name']);
                break;
            }
        }
        $('#petid').val(petid);

        var customerid = $('#selectcustomerid').val();

        /*$("#starttime").focusout(function() {
         var starttime = $("#starttime").val();
         var estdurationhr=$("#estimdurationhr").val();
         var estdurationmin=$("#estimdurationmin").val();
         //alert(starttime);
         if(starttime!="")
         {
         if(estdurationhr!="")
         {
         var textArr = starttime.split(":");
         var origDate = new Date(1, 1, 1, textArr[0], textArr[1], 0, 0);
         
         
         origDate.setHours(origDate.getHours()+parseInt(estdurationhr));
         alert(origDate.getHours());
         if(estdurationmin!="")
         origDate.setMinutes(origDate.getMinutes()+parseInt(estdurationmin));
         else
         origDate.setMinutes(origDate.getMinutes());
         
         
         var endTime = pad(origDate.getHours(),2)+":"+pad(origDate.getMinutes(),2);
         
         $("#endtime").val(endTime);
         }
         }
         // alert( "focusout fired: "  );
         });*/



        $("#selectcustomer").on('change', function () {

            var customerid = this.value;
            // console.log(customerid);

            var matchcount = 0;
            var count = 0;
            var jqueryarray = <?php echo json_encode($customers); ?>;
            var jqueryarray1 = <?php echo json_encode($pets); ?>;
            for (var i = 0; i < jqueryarray.length; i++) {
                // alert(customerid +":"+jqueryarray[i]['customerid']);
                if (jqueryarray[i]['firstname'] + ' ' + jqueryarray[i]['lastname'] == customerid || jqueryarray[i]['firstname'] + ' ' + jqueryarray[i]['lastname'] == customerid)
                {

                    matchcount = i;
                    count++;
                    $('#customertype').val(jqueryarray[matchcount]['usertype']);

                    $('#petname').val(jqueryarray1[matchcount]['name']);
                    $('#petid').val(jqueryarray1[matchcount]['petid']);
                    break;
                }
            }
            if (count == 0)
            {
                $("#selectpet").empty();
                $("#selectpet").append($('<option>', {
                    value: "",
                    text: "Select"
                }));
                $('#validation').addClass("alert alert-danger");
                $("#validation").show();
                $("#validerrorform").html('Customer does not exist!');
                $("html, body").animate({scrollTop: 0}, "slow");
                $("#selectpet").css('border-color', '#d2d6de');
                $("#selectcustomer").css('border-color', '#dd4b39');
            }

            if ($('#service').find(":selected").val() == 0)
            {
                $('#selectcustomer').val('');
                return;
            }

        });


        $("#service").on('change', function () {
            var selserviceid = this.value;
            var servicetype = "";

<?php for ($i = 0; $i < count($services); $i++) { ?>
                var serviceiddb =<?php echo $services[$i]['serviceid'] ?>;
                if (selserviceid == serviceiddb)
                {
                    servicetype = "<?php echo $services[$i]['type'] ?>";
                    serviceduration = "<?php echo $services[$i]['duration'] ?>";
                    servicedurationunit = "<?php echo $services[$i]['durationunit'] ?>";
                }
<?php } ?>

            if (servicetype == 'walker' || servicetype == 'grooming')
                $("#enddate").attr("disabled", "disabled");


        });

        /* $("#startdate").focusout(function() {
         alert('in blur');
         });*/

        $(".allownumericwithoutdecimal").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });

        $(".allowcharacterwithouthtmltags").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/(<([^>]+)>)/ig, ""));
            /*if((e.which > 64 && e.which < 91 ) || (e.which > 96 && e.which < 123)){
             event.preventDefault();
             }*/
        });

        $("#starttime").on('click', function () {
            $("#validerrorform").html('');
        });

        $("#endtime").on('change', function () {

            var estduration = $("#estimatedduration").val();
            var durationunit = $('input[name=durationunit]:checked').val();

            if (durationunit == "hours")
            {
                var hours = parseInt($("#endtime").val().split(':')[0], 10) - parseInt($("#starttime").val().split(':')[0], 10);
                if (hours < 0)
                {
                    hours = 24 + hours;
                }
                if (hours != estduration)
                {
                    $("#validerrorform").html('Your start & end time duration does not match with the estimated service duration !');
                } else
                {
                    $("#validerrorform").html('');
                }
            } else if (durationunit == "minutes")
            {
                var tim1 = $("#starttime").val(), tim2 = $("#endtime").val();
                var ary1 = tim1.split(':'), ary2 = tim2.split(':');
                var minsdiff = parseInt(ary2[0], 10) * 60 + parseInt(ary2[1], 10) - parseInt(ary1[0], 10) * 60 - parseInt(ary1[1], 10);
                var resultInMinutes = String(100 + minsdiff % 60).substr(1);
                resultInMinutes *= 1;

                if (resultInMinutes != estduration)
                {
                    $("#validerrorform").html('Your start & end time duration does not match with the estimated service duration !');
                } else
                {
                    $("#validerrorform").html('');
                }
            }


        });
        $("#update, #updateInfo_s1").click(function () {
            var service = $("#service").val();
            var startdate = $("#startdate").val();
            var enddate = $("#enddate").val();
            var starttime = $("#starttime").val();
            var endtime = $("#endtime").val();
            var customer = $("#selectcustomer").val();
            var pet = $('#selectpet').val();
            var errormsg = "";
            var hasError = false;
            var btnValue = $(this).val();
            var appointmentid = $('#appointmentid').val();

            if (service <= 0)
            {
                errormsg = 'The Service Name field is required!';
                $("#service").css('border-color', '#dd4b39');
                hasError = true;
            } else if (startdate.trim().length == 0)
            {
                errormsg = 'The Start Date field is required!';
                $("#startdate").css('border-color', '#dd4b39');
                hasError = true;
            } else if (starttime.trim().length == 0)
            {
                errormsg = 'The Start Time field is required!';
                $("#starttime").css('border-color', '#dd4b39');
                hasError = true;
            } else if (endtime.trim().length == 0)
            {
                errormsg = 'The End Time field is required!';
                $("#endtime").css('border-color', '#dd4b39');
                hasError = true;
            } else if ((enddate.trim().length != 0) && (startdate > enddate))
            {
                errormsg = 'End date never be comes before start date!';
                $("#enddate").css('border-color', '#dd4b39');
                hasError = true;
            } else if (customer.trim().length == 0)
            {
                errormsg = 'The Customer Name field is required!';
                $("#selectcustomer").css('border-color', '#dd4b39');
                hasError = true;
            } else if (pet.trim().length == 0)
            {
                errormsg = 'The Pet Name field  is Required!';
                $("#selectpet").css('border-color', '#dd4b39');
                hasError = true;
            }

            if (service > 0)
            {
                $("#service").css('border-color', '#d2d6de');
            }
            if (startdate.trim().length != 0)
            {
                $("#startdate").css('border-color', '#d2d6de');
            }
            if (starttime.trim().length != 0)
            {
                $("#starttime").css('border-color', '#d2d6de');
            }
            if (endtime.trim().length != 0)
            {
                $("#endtime").css('border-color', '#d2d6de');
            }
            if (customer.trim().length != 0)
            {
                $("#selectcustomer").css('border-color', '#d2d6de');
            }
            if (pet.trim().length != 0)
            {
                $("#selectpet").css('border-color', '#d2d6de');
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }

            if (hasError == false) {
                $("#validerrorform").html('');
                $("#validation").hide();
                //check appointment rule validation
                var url = "<?php echo base_url(); ?>vendor/appointments/validateAppointmentRule";

                $.post(url, {service: service, startdate: startdate, starttime: starttime, endtime: endtime, btnValue: btnValue, appointmentid: appointmentid}, function (data)
                {

                    if (data == 'valid')
                    {
                        $("#addappointmentform").submit();
                    } else
                    {

                        var msg = data + "<br>" + "Do you want to still submit the form?";
                        BootstrapDialog.show({
                            title: 'Confirmation Required',
                            message: msg,
                            buttons: [{
                                    label: 'Submit',
                                    action: function (dialog) {
                                        $("#addappointmentform").submit();
                                    }
                                }, {
                                    label: 'Cancel',
                                    action: function (dialog) {
                                        dialog.close();
                                    }
                                }]
                        });
                    }
                });
            } else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }
        });


        function onDateChange()
        {
            if (serviceduration != "" || serviceduration != "0")
            {
                if (servicedurationunit == "days")
                {
                    
                    var date = new Date($("#startdate").val()),
                            days = parseInt(serviceduration, 10);
                    date.setDate(date.getDate() + days);
                    var curdate = date.toInputFormat();
                    var currenddate = $.datepicker.formatDate( "MM dd, yy", new Date(curdate));
                    $("#enddate").val(currenddate);
                    $("#endtime").timepicker({showMeridian: true});                    
                    var weekday = ["Su","Mo","Tu","We","Th","Fr","Sa"];

                    var a = new Date($("#startdate").val());
                    var startdayname = weekday[a.getDay()];
                    var serviceid = $("#service").val();
                    var datastring = 'serviceid=' + serviceid + '&startdayname=' + startdayname;
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url(); ?>vendor/appointments/getServiceTime',
                        data: datastring,
                        success: function (data) {
                            var jsonObj = $.parseJSON(data);
                            $("#starttime").val(jsonObj.dayStartTime);
                            $("#endtime").val(jsonObj.dayEndTime);
                        }
                    });
                } else if (servicedurationunit == "hours")
                {
                    var date = new Date($("#startdate").val());
                    var starttime = $("#starttime").val();
                    var time = starttime;
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    var totaltime = parseInt(sHours) + parseInt(serviceduration);
                    if (totaltime >= 24) {
                        date.setDate(date.getDate() + 1);
                        var curdate = date.toInputFormat();
                        var currenddate = $.datepicker.formatDate( "MM dd, yy", new Date(curdate));
                        $("#enddate").val(currenddate);
                    } else {
                        var curdate = date.toInputFormat();
                        var currenddate = $.datepicker.formatDate( "MM dd, yy", new Date(curdate));
                        $("#enddate").val(currenddate);
                    }
                    onStartTimeChange();
                } else if (servicedurationunit == "minutes")
                {
                    var date = new Date($("#startdate").val());
                    var starttime = $("#starttime").val();
                    var time = starttime;
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    var totaltime = parseFloat(sHours) + (sMinutes/60) + (serviceduration/60);
                    if (totaltime >= 24) {
                        date.setDate(date.getDate() + 1);
                        var curdate = date.toInputFormat();
                        var currenddate = $.datepicker.formatDate( "MM dd, yy", new Date(curdate));
                        $("#enddate").val(currenddate);
                    } else {
                        var curdate = date.toInputFormat();
                        var currenddate = $.datepicker.formatDate( "MM dd, yy", new Date(curdate));
                        $("#enddate").val(currenddate);
                    }
                    onStartTimeChange();
                }
            }

        }

        function onStartTimeChange()
        {

            var starttime = $("#starttime").val();
            var estduration = $("#estimatedduration").val();
            var durationunit = $('input[name=durationunit]:checked').val();
            // var durationunit=$("#durationunit").val();
            //console.log(durationunit);
            if (starttime != "")
            {
                // Duration is in Hours
                if ((estduration != "") && (durationunit == "hours"))
                {
                    var textArr = starttime.split(":");
                    var textArr1 = textArr[1].split("");
                    var origDate = new Date(1, 1, 1, textArr[0], textArr1[0] + "" + textArr1[1], 0, 0);

                    var timeformate = textArr1[2] + "" + textArr1[3] + "" + textArr1[4];
                    if (origDate.getHours() < 12)
                    {
                        origDate.setHours(origDate.getHours() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {
                            if (timeformate == " PM")
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                                    $("#endtime").val(endtime);
                                } else
                                {
                                    if (origDate.getHours() == 12)
                                    {
                                        var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                                        $("#endtime").val(endtime);
                                    } else
                                    {
                                        var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                                        $("#endtime").val(endtime);
                                    }
                                }
                            } else
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                                    $("#endtime").val(endtime);
                                } else
                                {
                                    if (origDate.getHours() == 12)
                                    {
                                        var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                                        $("#endtime").val(endtime);
                                    } else
                                    {
                                        var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                                        $("#endtime").val(endtime);
                                    }
                                }
                            }
                        } else
                        {
                            if (origDate.getHours() == 0)
                            {
                                var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#endtime").val(endtime);
                            } else
                            {
                                var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#endtime").val(endtime);
                            }
                        }
                    } else
                    {
                        origDate.setHours(origDate.getHours() + parseInt(estduration));
                        if (origDate.getHours() == 0)
                        {
                            var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#endtime").val(endtime);
                        } else
                        {
                            if (origDate.getHours() < 12)
                            {
                                var endtime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                $("#endtime").val(endtime);
                            } else
                            {
                                if (origDate.getHours() == 0)
                                {
                                    var endtime = pad(origDate.getHours() + 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                    $("#endtime").val(endtime);
                                } else
                                {
                                    var endtime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                                    $("#endtime").val(endtime);
                                }
                            }
                        }
                    }
                }
                // Duration is in Minutes
                else if ((estduration != "") && (durationunit == "minutes"))
                {
                    var textArr = starttime.split(":");
                    var textArr1 = textArr[1].split("");
                    var origDate = new Date(1, 1, 1, textArr[0], textArr1[0] + "" + textArr1[1], 0, 0);

                    var timeformate = textArr1[2] + "" + textArr1[3] + "" + textArr1[4];
                    // 11:59 AM to 12:00 PM
                    if (origDate.getHours() == 11 && timeformate == " AM")
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {
                            var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                            var hours = endTime.split(":");

                            if (hours[0] == 00)
                            {
                                endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                            }
                            $("#endtime").val(endTime);
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#endtime").val(endTime);
                        }

                    }
                    // 11:59 PM to 12:00 AM
                    else if (origDate.getHours() == 11 && timeformate == " PM")
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 12)
                        {

                            var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                            var hours = endTime.split(":");

                            if (hours[0] == 00)
                            {
                                endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                            }
                            $("#endtime").val(endTime);
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#endtime").val(endTime);
                        }
                    } else
                    {
                        origDate.setMinutes(origDate.getMinutes() + parseInt(estduration));
                        if (origDate.getHours() >= 13)
                        {
                            if (timeformate == " AM")
                            {
                                var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " PM";
                                $("#endtime").val(endTime);
                            } else
                            {
                                var endTime = pad(origDate.getHours() - 12, 2) + ":" + pad(origDate.getMinutes(), 2) + " AM";
                                $("#endtime").val(endTime);
                            }
                        } else
                        {
                            var endTime = pad(origDate.getHours(), 2) + ":" + pad(origDate.getMinutes(), 2) + timeformate;
                            $("#endtime").val(endTime);
                        }
                    }
                }
            }
        }
        Date.prototype.toInputFormat = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();
            return  yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // new - padding
//            return (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]) + "-" + yyyy; // old - padding
        };


    });



    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function isNumber(n) {
        return !isNaN(parseInt(n)) && isFinite(n);
    }

    function _calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        var inyears = Math.abs(ageDate.getUTCFullYear() - 1970);
        return inyears + (ageDate.getUTCMonth() / 10);
    }

    $("#employeeinfo").hide();
    $("#showemp").show();
    function checkassign_emp_status() {
        var serviceid = $("#service").val();
        var datastring = 'serviceid=' + serviceid;
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>vendor/appointments/check_status',
            data: datastring,
            success: function (data) {
                if (data == 1) {
                    $("#employeeinfo").show();
                } else {
                    $("#employeeinfo").hide();
                }
            }
        });
    }



</script>
<script>
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

        // Start of Appointment add/update form validation //
        var errormsg = "";
        var hasError = false;
        var step = $(this).attr('value');
        var service = $('#service').val();
        var estimatedduration = $('#estimatedduration').val();
        var startdate = $("#startdate").val();
        var enddate = $("#enddate").val();
        var starttime = $("#starttime").val();
        var endtime = $("#endtime").val();
        //var customer = $("#selectcustomer").val();
        //var pet = $('#selectpet').val();
        var customer = $("#newCustomer").val();
        var pet = $('#newPet').val();

        switch (step) {
            case "step-1":
                if (service <= 0)
                {
                    errormsg = 'The Service Name field is required!';
                    $("#service").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (startdate.trim().length == 0)
                {
                    errormsg = 'The Start Date field is required!';
                    $("#startdate").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (starttime.trim().length == 0)
                {
                    errormsg = 'The Start Time field is required!';
                    $("#starttime").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (endtime.trim().length == 0)
                {
                    errormsg = 'The End Time field is required!';
                    $("#endtime").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if ((enddate.trim().length != 0) && (startdate > enddate))
                {
                    errormsg = 'End date never be comes before start date!';
                    $("#enddate").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (service > 0)
                {
                    $("#service").css('border-color', '#d2d6de');
                }
                if (estimatedduration != '') {
                    $("#estimatedduration").css('border-color', '#d2d6de');
                }
                if (startdate.trim().length != 0)
                {
                    $("#startdate").css('border-color', '#d2d6de');
                }
                if (starttime.trim().length != 0)
                {
                    $("#starttime").css('border-color', '#d2d6de');
                }
                if (endtime.trim().length != 0)
                {
                    $("#endtime").css('border-color', '#d2d6de');
                }
                break;
            case "step-2":
                if (customer.trim().length == 0)
                {
                    errormsg = 'The Customer Name field is required!';
                    $("#newCustomer").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (pet.trim().length == 0)
                {
                    errormsg = 'Pet Name field is Required!';
                    $("#newPet").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (customer.trim().length != 0)
                {
                    $("#newCustomer").css('border-color', '#d2d6de');
                }
                if (pet.trim().length != 0)
                {
                    $("#newPet").css('border-color', '#d2d6de');
                }
                break;
            case "step-3":
                break;
        }

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
            $("#validerrorform").html(errormsg);
        } else {
            $('#validation').hide();
            $("#validerrorform").html('');
        }

        // End of Appointment add/update form validation //

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alert(curInputs[i].val());
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    // End of section wise form //


</script>                                                 

