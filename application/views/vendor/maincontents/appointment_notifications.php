
            <?php if(count($newappointments) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="example3" class="table table-bordered table-striped">
                    <tr>
                    
                       <th>User</th>
                       <th>Pet Name</th>
                       <th>Service</th>
                      <th>Start Date-Time</th>
                     <th>End Date-Time</th>
                      <th>Notes</th>
                      <th>Email</th>
                      <th>Phone</th>
                    </tr>
                     <?php  foreach($newappointments as $row) { 
                     $scheduletime=date('D, M j, Y',strtotime($row['boardingfromdate']));
                      if($row['boardingfromtime']!="");
                         $scheduletime=$scheduletime." ".date('G:i',strtotime($row['boardingfromtime']));
                      if($row['boardingtodate']!="" && $row['boardingtodate'] !="0000-00-00")
                        $scheduletime1=date('D, M j, Y',strtotime($row['boardingtodate']));
                      else 
                        $scheduletime1=$row['boardingtodate'];
                      if($row['boardingtotime']!="")
                         $scheduletime1=$scheduletime1." ".date('G:i',strtotime($row['boardingtotime'])); 
                      ?>
                     <tr>
                    <td><?php echo $row['firstname']." ".$row['lastname'];?></td>
                     <td><?php echo $row['petname'];?></td>
                     <td><?php echo $row['servicename'];?></td>
                     <td><?php echo $scheduletime;?></td>
                   	 <td><?php echo $scheduletime1;?></td>
                     <?php if ( $row['bookingnote'] == "Notes") {?>
                        <td>N/A</td>
                     <?php } else { ?>
                        <td><?php echo $row['bookingnote'];?></td>
                     <?php } ?>
                     <td><?php echo $row['email'];?></td>
                     <td><?php echo $row['phonenumber'];?></td>
                   </tr>
                    <?php }?>
                 
                 
                    
                  </table>
                </div><!-- /.box-body -->
             
             <?php }?>

      
      
    