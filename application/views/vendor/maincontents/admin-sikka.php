<?php $admin=$this->session->userdata('admin');
	
?>
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sikka
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'admin/home/navigateToSikka';?>"><i class="fa fa-align-right fa-rotate-90"></i>Sikka</a></li>
            <li class="active">Sikka</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content sikkaoffices">
          <div class="row">
          <br><br> 
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
               <div class="row">
                <div class="box-tools">
                <div class ="col-sm-3">  
                    <center><a href="" style="text-decoration:none; display:inline-block; color:black; font-size:24px;">Sikka Offices</a></center>
                </div>
               </div>

                <div class="box-tools pull-right" > 
                <div class="col-sm-4">
                      <div class="form-group">
                        
                      </div>
                </div>
              </div>
                    </div>
                  </div>
                    
                  </div>
                   
                 
                </div><!-- /.box-header -->
                <br>
                <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
           

            <?php if(count($offices) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="vendorstable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <!--th style="width: 10px">#</th-->
                       <th><center>office id</center></th>
                      <th><center>Office Name</center></th>
                      <th><center>Address</center></th>
                      <th><center>City</center></th>
                      <th><center>State</center></th>
                      <th><center>Zip</center></th>
                    
                    </tr>
                    </thead>
                      <tbody>
                     <?php  foreach($offices as $row) { 
                     
                      ?>
                    
                    <tr class="sikkaOfficedetails" id="<?php echo $row['sikkaofficeid'];?>"> 
                     <!--td><?php echo $row['vendorid'];?></td-->
                    <td><center><?php echo $row['sikkaofficeid']?></center></td>
                    <td><center><?php echo $row['sikkaofficename'];?></center></td>
                  
                    <td><center><?php echo $row['address'];?></center></td>
                    <td><center><?php echo $row['city'];?></center></td>
                    <td><center><?php echo $row['state'];?></center></td>
                    <td><center><?php echo $row['zip'];?></center></td>
                    </tr>
                    <?php }?>
                 
                 
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
              
                 
                 <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                </div>
              </div><!-- /.box -->

              
          
    
        </section><!-- /.content -->

      <section class="content sikka_practices" style="display: none;">
          <div class="row">
          <br><br> 
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
               <div class="row">
                <div class="box-tools">
                <div class ="col-sm-6">  
                    <a href="" id="sikka_practice_name" style="text-decoration:none; display:inline-block; color:black; font-size:24px;"></a>
                </div>
               </div>

                <div class="box-tools pull-right" > 
                <div class="col-sm-4">
                      <div class="form-group">
                        
                      </div>
                </div>
              </div>
                    </div>
                  </div>
                    
                  </div>
                   
                 
                </div><!-- /.box-header -->
                <br>
           

            
                <div id="data-practices" class="box-body table-responsive">
                  <table id="vendorstable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th><center>Practice Name</center></th>
                      <th><center>Email</center></th>
                      <th><center>Action</center></th>
                      <th><center>Prescriptions</center></th>
                      <th><center>Vaccinations</center></th>
                    </tr>
                    </thead>
                      <tbody id="practices">

                      </tbody>
                  </table>
                </div><!-- /.box-body -->
                <div id="no-data-practices" class="box-footer clearfix">
                    <div align="center"><h1>No data available !</h1></div><br /><br />
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

       <div class="modal fade" id="confirm-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Email Not Available
            </div>
            <div class="modal-body">
            <p>Email address of this practice is not available. Please enter a valid email address and then click <b>Add to Bizbark</b>.</p>
               <div class="form-group">
                 
           <input type="email" class="form-control" id="email_practice" placeholder="Enter email">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning btn-ok" onclick="javascript:enterEmail();">Submit</a>
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="confirm-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Sikka Response
            </div>
            <div class="modal-body">
               <p class="sikka-response"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Sikka Migration Progress
            </div>
            <div class="modal-body">
                  <div class="progress">
                    <div id="progress-percent" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                      0%
                    </div>
                  </div>
                  <p class="text-center" id="progress-message"></p>
            </div>
        </div>
    </div>
</div>


      
      
    <script type="text/javascript">
        $(document).ready(function(){
          $('#refreshSikkaOffices').click(function(){
                $('#spinner').show();
                   $.ajax({
                      type: "post",
                      url: '<?php echo base_url();?>admin/home/navigateToSikka',
                      cache: false,
                      success: function(json){            
                      try{
                        $('#spinner').hide();
                        var obj = jQuery.parseJSON(json);
                          console.log(obj); 
                      }catch(e) {   
                        $('#spinner').hide();
                        console.log(e);
                        alert('Exception while request..');
                      }   
                      },
                      error: function(){  
                      $('#spinner').hide();         
                        alert('Error while request..');
                      }

                  });
          });
        });
    </script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/sikkaadmin.js"></script>