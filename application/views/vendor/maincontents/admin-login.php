<!DOCTYPE html>
<html>
  <head> <?php echo $head;?> </head>
  <body class="layout-boxed">
    <div class="login-box">
      <div class="login-logo">
          <?php if(ALLOW_VET==1){?>
              <img src="<?php echo base_url();?>assets/images/logo_vet.png" width="250"/>
          <?php }else{?>
              <img src="<?php echo base_url();?>assets/images/logo_biz.png" width="250"/>
          <?php } ?>
        <br /><b>  Admin Login</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        
         <?php if($this->session->flashdata('error_msg')){?><p class="login-box-msg alert alert-danger form-error-msg"><?php echo $this->session->flashdata('error_msg');?></p><?php }?>
         <?php if($this->session->flashdata('success_message')){?><p class="login-box-msg alert alert-success form-error-msg"><?php echo $this->session->flashdata('success_message');?></p><?php }?>
      <p class="login-box-msg alert alert-info form-error-msg">Sign in to start your session</p>
         <?php echo form_open(base_url().'admin/login',array('method'=>'post','name'=>'loginForm','id'=>'LoginForm'));?>
          <div class="form-group has-feedback">
            <?php echo form_input(array('type'=>'textt','name'=>'username','class'=>'form-control','placeholder'=>'UserName','id'=>'username'));?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php echo form_error('username');?>
          </div>
          <div class="form-group has-feedback">
            <?php echo form_input(array('type'=>'password','name'=>'password','class'=>'form-control','placeholder'=>'Password'));?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo form_error('password');?>
          </div>
          <div class="row">
           
            <div class="col-xs-4">
              <?php echo form_submit(array('class'=>"btn btn-primary btn-block btn-flat",'value'=>'Sign In','name'=>'submit'));?>
            </div><!-- /.col -->
          </div>
        <?php echo form_close();?>

       <!--   <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div>--><!-- /.social-auth-links -->

       <!-- <a href="#">I forgot my password</a><br>
         <a href="register.html" class="text-center">Register a new membership</a> -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
