<?php $admin=$this->session->userdata('admin');
	
?>
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Vendors
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'admin/home/index';?>"><i class="fa fa-align-right fa-rotate-90"></i>Vendors</a></li>
            <li class="active">View Vendors</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
        
       
           
          <br><br> 
            <div class="col-md-12">
            
            
            
              <div class="box">
                <div class="box-header with-border">
               <div class="row">

                 <form role="form" action="<?php echo base_url().'admin/home/search'; ?>" method="post">
                   <div class="box-tools" >
                      <div class="col-sm-3">
                        <div class="form-group ">
                            <input type="text" class="form-control" name="search" id="search" placeholder="Search Vendor"/>
                        </div>
                      </div>

                      <div class ="col-sm-2">
                        <div class="form-group">
                            <input type="submit" class="btn bg-orange" name="submit" value="Search"/>
                        </div>
                      </div>
                </div>
              </form>

                <div class="box-tools">
                <div class ="col-sm-3">  
                    <center><a href="" style="text-decoration:none; display:inline-block; color:black; font-size:24px;">Vendors</a></center>
                </div>
               </div>

                <div class="box-tools pull-right" > 
                <div class="col-sm-4">
                      <div class="form-group">
             <a class="btn bg-orange" href="<?php echo base_url();?>admin/home/downloadVendors">Download</a>
            
                  </div>
                </div>
              </div>
                    </div>
                  </div>
                    
                  </div>
                   
                 
                </div><!-- /.box-header -->
                <br>
                <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
           

            <?php if(count($vendors) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="vendorstable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <!--th style="width: 10px">#</th-->
                       <th><center>Name</center></th>
                       <th><center>Company</center></th>
                       <th><center>Logo</center></th>
                      <!--th>Contact Information</th-->
                      <th><center>Email</center></th>
                      <th><center>Phone Number</center></th>
                      <th><center>Address</center></th>
                      <th><center>Website</center></th>
                      <th><center>Category</center></th>
                      <!--th>Terms and conditions</th-->
                      <th><center>Description</center></th>
                     <th><center>Date Created</center></th>
                      <th style="width: 10px"><center>Total Appointments</center></th>
                      <th>Appointments</th>
                      <th><center>Customers</center></th>
                      <th><center>Services</center></th>
                      <th><center>Delete</center></th>
                    
                    </tr>
                    </thead>
                      <tbody>
                     <?php  foreach($vendors as $row) { 
                     
                      ?>
                    
                    <tr id="<?php echo $row['vendorid'];;?>"> 
                     <!--td><?php echo $row['vendorid'];?></td-->
                    <td><?php echo $row['firstname']." ".$row['lastname'];?></td>
                    <td><?php echo $row['comapnyname'];?></td>
                    <td>
                     <?php if($row['logoimage']!=""){?>
                     <img src="<?php echo awsBucketPath.$row['logoimage'];?>" width="140" height="80"/>
                     <?php }?>
                    </td>
                    <td><?php echo $row['email'];?></td>
                    <td><?php echo $row['contact'];?></td>
                    <td><?php echo $row['address'];?><br><?php echo $row['city'];?>,<?php echo $row['state'];?><br><?php echo $row['country'];?></td>
                  	<td><center><a href="<?php echo $row['website'];?>" target="_blank"><?php echo $row['website'];?></a></center></td>
                  	<td><?php echo $row['category'];?></td>
                  	<!--td><?php echo $row['termsandconditions'];?></td-->
                  	<td><?php echo $row['description'];?></td>
                  	<td><?php echo $row['createdon'];?></td>
                  	<td><?php echo $row['noofappointments'];?></td>
                     <td><center><a href="<?php echo base_url().'admin/home/appointments/'.$row['vendorid'];?>"><i class="fa fa-calendar" style=" font-size: 1.5em;"></i></a></center></td>
                  	 <td><center><a href="<?php echo base_url().'admin/home/customers/'.$row['vendorid'];?>"><i class="fa fa-users" style=" font-size: 1.5em;"></i> </a></center>  </td>
                  	 <td><center><a href="<?php echo base_url().'admin/home/services/'.$row['vendorid'];?>"><i class="fa fa-wrench" style=" font-size: 1.5em;"></i> </a></center>  </td>
                     <td><center><a href="<?php echo base_url().'admin/home/delete/'.$row['vendorid'];?>" ><i class="fa fa-trash" style=" font-size: 1.5em;"></i> </a></center>  </td>
                    </tr>
                    <?php }?>
                 
                 
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
              
                 
                 <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      

      
      
<!-- ----------------------------------------------- -->
        <script type="text/javascript">
      $(document).ready(function(){
    	 
       $(".fa-trash").on("click",function(e){
        var confrm=confirm('Are you sure ? \n Deleting the Vendor will also delete all data related to the Vendor.');
        if(confrm==true)
        {
          return true;
        }else
        {
          e.preventDefault();
          return false;
        }
      });

        $('#vendorstable').DataTable({
				
    		  bFilter: false, 
    		  bInfo: false,
    		  "aoColumnDefs" : [
    		                    {
    		                      'bSortable' : false,
    		                      'aTargets' : [2,5,6,8,11,12,13,14]
    		                    }]
        	  });
    	  
      });
      </script>
      
      
    