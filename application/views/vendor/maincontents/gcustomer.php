<script type="text/javascript">
    $('.selectall').click(function () {
        if ($(this).is(':checked')) {
            $('div input').prop('checked', true);
        } else {
            $('div input').prop('checked', false);
        }
    });

    $(document).ready(function () {
        $('#example3').DataTable({

            bFilter: false,
            bInfo: false,
            "paging": true,
            "lengthchange": false,
            "searching": true,
            "info": false,
            "autoWidth": true,
            "iDisplayLenght": 20,
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [2, 4, 5]
                }]

        });

        $(".cnf-del-custom").on("click", function (e) {
            var confrm = confirm('Are you sure ? \n Deleting the customer will also delete all data related to the customer.');
            if (confrm == true)
            {
                return true;
            } else
            {
                e.preventDefault();
                return false;
            }
        });

        $(".cnf-del-custom1").on("click", function (e) {
            var confrm = confirm('Are you sure ? \n Deleting the BabelBark customer will also delete all data related to the BabelBark customer.');
            if (confrm == true)
            {
                return true;
            } else
            {
                e.preventDefault();
                return false;
            }

        });
        $('#inviteConfirmed').click(function () {
            var emails = [];
            $('#inviteusersmodal').modal('toggle');
            $('.inviteme').each(function (i, obj) {
                if (obj.checked)
                {
                    emails.push($(this).val());
                }
            });

            $.each(emails, function (index, email) {
                if (email)
                {
                    autoInvite(email);
                    var successmsg = "Invitation emails successfully sent";
                    $('#validation').addClass("alert alert-success");
                    $('#validation').show();
                    $("#inviteConfirmedMsg").html(successmsg);
                }
            });
        });
    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }

    function autoInvite(email) {
        if (validateEmail(email))
        {
            var url = "<?php echo base_url(); ?>vendor/customer/checkInvitedUser";

            $.post(url, {email: email}, function (data)
            {
                if (data == 'existinguser')
                {
                    profileImportInvite(email, true);

                } else if (data == 'newuser')
                {
                    inviteUser(email, true);
                }

            });
        }
    }

    function checkUser() {
        var email = $("#inviteEmail").val();
        if (email == '') {
            $("#validerror").html('The Customer email address field is required');
        } else if (!validateEmail(email)) {
            $("#validerror").html('The Customer email address field must contain a valid email address');
        } else {
            $("#validerror").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/checkInvitedUser";

            $.post(url, {email: email}, function (data)
            {              
                if (data == 'existinguser')
                {
                    profileImportInvite(email, false);

                } else if (data == 'newuser')
                {
                    inviteUser(email, false);
                }

            });
        }
    }

    function inviteUser(email, auto) {



        if (email == '')
        {

            $("#validerror").html('The Customer email address field is required');

        } else if (!validateEmail(email)) {
            $("#validerror").html('The Customer email address field must contain a valid email address');

        } else {
            $("#validerror").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/inviteUser";

            $.post(url, {email: email}, function (data)
            {
                if (!auto)
                {
                    if (data == '1')
                    {
                        $("#inviteEmail").val('');
                        alert("Invitation mail sent successfully to " + email);
                    } else
                    {
                        alert("Some problem occured while sending mail!");
                    }
                }

            });
        }
    }

    function profileImportInvite(email, auto) {
        if (email == '') {
            $("#validerror1").html('The Customer email address field is required');
        } else if (!validateEmail(email)) {
            $("#validerror1").html('The Customer email address field must contain a valid email address');
        } else {
            $("#validerror1").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/inviteUserForProfileImport";

            $.post(url, {email: email}, function (data)
            {
                if (!auto)
                {
                    if (data == '1')
                    {
                        $("#inviteEmailProfImport").val('');
                        alert("Invitation mail sent successfully to " + email);
                    } else if (data == '2')
                    {
                        alert("BabelBark User with this email id doen't exist!");
                    } else
                    {
                        alert("Some problem occured while sending mail!");
                    }
                }

            });
        }

    }

    function showEmailPreview()
    {

        var email = $("#inviteEmail").val();
        if (email == '')
        {

            $("#validerror1").html('The Customer email address field is required');

        } else if (!validateEmail(email)) {
            $("#validerror1").html('The Customer email address field must contain a valid email address');

        } else
        {
            $("#validerror1").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/checkInvitedUser";

            $.post(url, {email: email}, function (data)
            {
                if (data == 'existinguser')
                {
                    showProfileImportEmailPreview(email);
                } else if (data == 'newuser')
                {
                    showInviteUserEmailPreview(email);
                }

            });


        }
    }


    function showInviteUserEmailPreview(email)
    {
        var url = "<?php echo base_url(); ?>vendor/customer/inviteUserPriviewEmail";

        $.post(url, {email: email}, function (data)
        {



            $("#previewdiv").html(data);
            $('#generic1').modal('show');


        });

    }

    function showProfileImportEmailPreview(email)
    {

        var url = "<?php echo base_url(); ?>vendor/customer/profileImportPriviewEmail";

        $.post(url, {email: email}, function (data)
        {

            if (data == '2')
            {

                alert("BabelBark User with this email id doen't exist!");
            } else
            {

                $("#previewdiv").html(data);
                $('#generic1').modal('show');
            }

        });

    }

    function filterUser(object) {
        console.log(object);
        if (object != '') {
            var basePsth = "<?php echo base_url(); ?>vendor/customer/listofgenericcustomer/" + object;
            window.location.href = basePsth;
        }
    }
</script>