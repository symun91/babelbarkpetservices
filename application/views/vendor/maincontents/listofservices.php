<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Services
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Services</a></li>
            <li class="active">View Services</a></li>
        </ol>
    </section>
    <br><br>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <br><br> <br><br><br><br> 
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title">Services</h3></center>
                        <?php
                        if ($userrole == "admin") {
                            ?>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="<?php echo base_url(); ?>vendor/services/addservice">Add Service</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div><!-- /.box-header -->
                    <br>
                    <?php if ($this->session->flashdata('error_message')) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            <?php echo $this->session->flashdata('error_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success_message')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>	<i class="icon fa fa-check"></i> Success</h4>
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if (count($services) > 0) { ?>
                        <div class="box-body table-responsive" style="padding: 10px 15px;">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <!--th style="width: 10px">#</th-->
                                        <th>Name</th>
                                        <th>Service Type</th>
                                        <th>Price</th>
                                        <th><a href="#" data-toggle="tooltip" title="The estimated time this service requires">Duration</a></th>
                                        <th><a href="#" data-toggle="tooltip" title="What time is this service offered?">Availability</a></th>
                                        <th><a href="#" data-toggle="tooltip" title=" Are reservations required for this service?">Reservation</a></th>
                                        <th>Details</th>
                                        <th><?php echo $userrole == "employee" ? '' : 'Delete'; ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($services as $row) {
                                        ?>                    
                                        <tr id="<?php echo $row['serviceid']; ?>">
                                        <!--td><?php echo $row['serviceid']; ?></td-->
                                            <td><?php echo $row['name']; ?></td>
                                            <td><?php echo $row['type']; ?></td>                                            
                                            <td>
                                                <?php
                                                if ($row['price'] != '') {
                                                    if ($row['priceby'] == 'days') {
                                                        echo $row['price'] . '$' . " " . 'by day';
                                                    } elseif ($row['priceby'] == 'byhour') {
                                                        echo $row['price'] . '$' . " " . 'by hour';
                                                    } else {
                                                        echo $row['price'] . '$' . " " . $row['priceby'];
                                                    }
                                                } else {
                                                    echo 'Not Specified';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                            <?php
                                            if ($row['duration'] == 0) {
                                                echo 'Custom';
                                            } else {
                                                echo $row['duration'] . " " . $row['durationunit'];
                                            }
                                            ?>
                                            </td>
                                            <td>
                                            <?php
                                            if ($row['availabilitydays'] == '0'){
                                                echo "Not Specified";
                                            } else {
                                                echo $row['availabilitydays'];
                                            }
                                            ?>
                                            </td>
                                            <?php
                                            if ($row['reservation'] == 1)
                                                $dispres = 'Yes';
                                            else
                                                $dispres = 'No';
                                            ?>
                                            <td><?php echo $dispres; ?></td>

                                            <td><center><a href="<?php echo base_url() . 'vendor/services/viewDetails/' . $row['serviceid']; ?>"><i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i> </a></center>  </td>

                                    <td><center><?php echo $userrole == "employee" ? '' : anchor('vendor/services/deleteServices/' . $row['serviceid'], '<i class="fa fa-trash fa-lg"></i>', 'title="Click to delete" class="cnf-del-custom"'); ?></center></td>
                                    </tr>
                                <?php } ?>
                                </tbody>


                            </table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">

                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                //foreach ($links as $link) {
                                //   echo "<li>".$link."</li>";
                                // }
                                ?>
                            </ul>
                        <?php }else { ?>
                            <div align="center"><h1>No data available !</h1></div><br /><br />
                        <?php } ?>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->

        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->




<!-- ----------------------------------------------- -->
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#example3').DataTable({bFilter: false,
            bInfo: false,
            order: [],
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [1, 4, 6, 7]
                }]
        });
    });
</script>


