<style type="text/css">
    textarea { resize: vertical; }
    .not-active {
        pointer-events: none;
        cursor: default;
    }
    #profile, #service{
        margin-top: 15px;
    }
</style>
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
<!-- timepicker js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imgareaselect-default.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imgareaselect.pack.js"></script>
<div id="spinner"
     style="display:none;position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url(); ?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Edit Profile</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Profile</a></li>
            <li class="active">Profile</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Profile</h3>
                <div class="box-tools pull-right"></div>
            </div>
            <div class="box-body">
                <?php if ($this->session->flashdata('error_message')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success_message')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4>	<i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php } ?>
                <div id="validation">
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <label id="validerrorform"></label>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                                <li><a href="#service" data-toggle="tab">Service Details</a></li>
                            </ul>
                            <form name="changeProfileForm" id="changeProfileForm"  action="<?php echo base_url(); ?>vendor/home/profile" method="POST" enctype="multipart/form-data">
                                <?php echo form_hidden('adminid', $details['vendorid']); ?>
                                <input type="hidden" name="formsubmit" id="formsubmit" value=""/>
                                <div class="tab-content">
                                    <div class="active tab-pane" id="profile">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="firstName">First Name<font color="red">*</font></label>
                                                    <input type="text" required title="Please Enter First Name" oninvalid="this.setCustomValidity('Enter User Name Here')" class="form-control" id="firstname" placeholder="First Name" name="firstname" maxlength="25" value="<?php echo $details['firstname']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname">Last Name<font color="red">*</font></label>
                                                    <input type="text" required title="Please Enter Last Name" class="form-control" id="lastname" placeholder="Last Name" name="lastname" maxlength="25" value="<?php echo $details['lastname']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="companyname">Company Name<font color="red">*</font></label>
                                                    <input type="text" required title="Please Company Name" class="form-control" id="comapnyname" placeholder="Company Name" name="comapnyname" maxlength="100" value="<?php echo $details['comapnyname']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email<font color="red">*</font></label>
                                                    <input type="text" required title="Please Enter Email" class="form-control" id="email" placeholder="Email" name='email' value="<?php echo $details['email']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Phone Number</label>
                                                    <input class="form-control allownumericwithoutdecimal" placeholder="Phone Number" id="phoneno" name="phoneno" type="text" value="<?php echo set_value('phoneno', $details['contact']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="website">Website</label>
                                                    <input type="text" class="form-control" id="website" placeholder="Website" name="website" value="<?php echo $details['website']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Address</label>
                                                    <div class="row">
                                                        <div class="col-sm-2">Country<font color="red">*</font></div>
                                                        <div class="col-sm-9">
                                                            <select class="form-control" name="country" id="country" required title="Please Select Country">
                                                                <option value="">Select Country</option>
                                                                <option value="Canada" <?php echo set_select('country', 'Canada', ( $details['country'] == 'Canada' ? TRUE : FALSE)); ?>>Canada</option>
                                                                <option value="United States" <?php echo set_select('country', 'United States', ( $details['country'] == 'United States' ? TRUE : FALSE)); ?>>United States</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-2">Address<font color="red">*</font></div>
                                                        <div class="col-sm-9"> <input id="address" required title="Please Enter Street Address" class="form-control" type="text" placeholder="Address" name="address" maxlength="45" value="<?php echo set_value('address', $details['address']); ?>"></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-2">City<font color="red">*</font></div>
                                                        <div class="col-sm-9">
                                                            <input id="city" class="form-control" type="text" required title="Please Enter City" placeholder="City" name="city" maxlength="45" value="<?php echo set_value('city', $details['city']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-2">State<font color="red">*</font></div>
                                                        <div class="col-sm-9">
                                                            <select class="form-control" name="state" id="state" required title="Please Select State">

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-sm-2">Zipcode<font color="red">*</font></div>
                                                        <div class="col-sm-9"> <input id="zipcode" required title="Please Enter Postal Code" class="form-control" type="text" placeholder="Zipcode" name="zipcode" maxlength="15" value="<?php echo set_value('zipcode', $details['zipcode']); ?>"></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px">
                                                        <div class="col-sm-10">
                                                            <div id="map_canvas" style="border:0px solid #fff452;width:500px;height:320px;">
                                                                <!-- map loaded here -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if ($details['addresslatlng'] != "") {
                                                        $latlng = explode(",", $details['addresslatlng']);
                                                    } else {
                                                        $details['addresslatlng'] = "37.09024,-95.71289100000001";
                                                        $latlng = explode(",", $details['addresslatlng']);
                                                    }
                                                    ?>
                                                    <input type="hidden" id="lat" name="lat" value="<?php echo $latlng[0]; ?>">
                                                    <input type="hidden" id="lng" name="lng" value="<?php echo $latlng[1]; ?>">


                                                    <input type="hidden" name="logoimage1" id="logoimage1"/>
                                                    <input type="hidden" name="logoimagetemp" id="logoimagetemp"/>
                                                    <input type="hidden" value="0" id="remove_logo" name="remove_logo">

                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <form  id="upload">
                                                    <div class="form-group">
                                                        <label for="image" style="margin-top: 10px;">Logo Image</label>
                                                        <div class="row"  style="margin-top: 10px;">
                                                            <div class="col-sm-5">
                                                            <input class="form-control" type="file" id="logoimage" name='logoimage' onchange="return uploadimage()"></div>
                                                            <input type="button" class="btn btn-warning" name="upload" id="upload" value="Upload"></button>
                                                            <button type="button" class="btn btn-danger" id="discard_logo">Remove</button>
                                                            <?php echo form_error('image'); ?>
                                                        </div>
                                                    </div>
                                                </form>
                                                <form>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <?php if ($details['logoimage'] != "") { ?>
                                                                <div class="col-sm-4">
                                                                    <div class="input-group">
                                                                        <label for="preview">Preview Image</label>
                                                                        <a href="#" class="thumbnail not-active">
                                                                            <img src="<?php echo empty_image('', ''); ?>" id="logoimagepreview"/>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <label for="currentImage">Current Image</label>
                                                                        <a href="#" class="thumbnail not-active" style="width: 200px; height: 170px;">
                                                                            <img src="<?php echo awsBucketPath . $details['logoimage']; ?>?<?= Date('U') ?>" id="currentImage" style="width: 190px; height: 160px;"/>
                                                                            <input type="hidden" id="logo_exist" value="<?= !empty($details['logoimage']) ? 1 : 0; ?>">							            	
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="col-sm-6">
                                                                    <div class="input-group">
                                                                        <label for="preview">Preview Image</label>
                                                                        <a href="#" class="thumbnail not-active">
                                                                            <img src="<?php echo empty_image('', ''); ?>" id="logoimagepreview"/>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="input-group">
                                                                        <label for="currentImage">Cropped Image</label>
                                                                        <a href="#" class=" not-active">
                                                                            <img src="<?php echo empty_image('', ''); ?>" id="currentImage"/>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="box-footer">
                                                    <div class="form-group">
                                                        <input type="button" class="btn btn-warning " name="update" id="update" value="Update">
                                                    </div>
                                                </div>                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="service">
                                        <div class="form-group">
                                            <label>Category<font color="red">*</font></label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <select name="selecttype[]" required title="Please Select Category"  id="selecttype" class="form-control" multiple="multiple">
                                                        <?php for ($i = 0; $i < count($servicetypes); $i++) { ?>
                                                            <option value="<?php echo $servicetypes[$i]['categoryid']; ?>" <?php echo set_select('selecttype') ?> ><?php echo $servicetypes[$i]['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Operating Hours</label>
                                            <div class="row">
                                                <div class="col-xs-2">Days</div>
                                                <div class="col-xs-10">
                                                    <?php
                                                    $checkeddays_arr = array();
                                                    if ($details['operatingdays'] != "") {
                                                        $checkeddays_arr = explode(",", $details['operatingdays']);
                                                    }
                                                    for ($i = 0; $i < count($weekdays); $i++) {
                                                        ?>
                                                        <?php echo $weekdays[$i]; ?> &nbsp;<input type="checkbox"  name="availabilitydays[]" value="<?php echo $weekdays[$i]; ?>"  <?php echo (in_array($weekdays[$i], $checkeddays_arr) ? 'checked' : null); ?> onchange="dayselected(<?php echo $i; ?>);"/>

                                                        &nbsp;&nbsp; &nbsp;&nbsp;
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-2">Hours</div>
                                                <!--  <div class="col-xs-4">
                                                <label>From </label>
                                                    <input type="time" class="form-control" name="fromhour" value="<?php echo set_value('fromhour', $details['availabilityfromhour']); ?>">
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>To</label>
                                                    <input type="time" class="form-control" name="tohour" value="<?php echo set_value('tohour', $details['availabilitytohour']); ?>">
                                                </div>-->
                                                <br/>
                                                <input type="hidden" id="availabityfromhours" name="availabityfromhours"/>
                                                <input type="hidden" id="availabitytohours" name="availabitytohours"/>
                                                <div class="availabilitydetail"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <textarea rows="4" class="form-control countable" name="description" data-length="1500"><?php echo $details['description']; ?></textarea>
                                                </div>
                                                <div class="col-xs-6"></div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <input type="button" class="btn btn-warning " name="update" id="updateInfo_s1" value="Update">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<script type="text/javascript">
    //Upload Image Validation
    function uploadimage() {
        var fileUpload = document.getElementById("logoimage");
        //Get reference of FileUpload.
        //var fileUpload = document.getElementById("fileUpload");
        var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
        if (size > 5000) {
            alert("The Logo Image can be at most 5 Mb in size.");
            $("#logoimage").val('');
            return false;
        }
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
        if (regex.test(fileUpload.value.toLowerCase())) {

            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    //Validate the File Height and Width.
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                        if (height > 480 || width > 320) {
                            alert("The Logo Image can be at most 480x320 pixels in size.");
                            $("#logoimage").val('');
                            return false;
                        }
                    };
                }
            } else {
                alert("This browser does not support HTML5.");
                return false;
            }
        } else {
            alert("The Logo Image can be of *.png, *.jpg, *.jpeg types only.");
            return false;
        }
    }

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("logoimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("logoimagepreview").src = oFREvent.target.result;
        };
    }
    ;

    $(window).load(function () {
        $("#validation").hide();
    });

    var daycount = 0;
    var startindex = 0;
    var applyall = 0;
    var defaultfromhoursstr = "";
    var defaulttohoursstr = "";

    function getSizes(im, obj)
    {
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = uploadedBannnerImageWidth;
        var thumb_height = uploadedBannnerImageHeight;
        var img = $("#logoimagetemp").val();
        /* img=img.split('/bizbark/');
         img=img[1];*/
        /*alert(img);*/
        if (thumb_width > 0)
        {
            var data_ = {
                x_axis: x_axis,
                y_axis: y_axis,
                x2_axis: x2_axis,
                y2_axis: y2_axis,
                thumb_width_: uploadedBannnerImageWidth,
                thumb_height_: uploadedBannnerImageHeight,
                img_: img
            };

            $.ajax({
                type: "post",
                data: data_,
                cache: false,
                url: '<?php echo base_url(); ?>vendor/home/upload_thumbnail',
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (data) {
                    /*console.log(data);*/
                    var d = new Date();
                    var newdata = data + '?' + d.getTime();

                    $("#currentImage").attr('src', '<?php echo base_url(); ?>' + newdata);
                    $("#logoimage1").attr('value', data);
                    $("#spinner").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(ajaxOptions);
                    console.log(thrownError);

                }
            });
        }
    }

    function getMeta(url) {
        var img = new Image();
        img.addEventListener("load", function () {
            uploadedBannnerImageWidth = this.naturalWidth;
            uploadedBannnerImageHeight = this.naturalHeight;
        });
        img.src = url;
    }

    $(document).ready(function () {
        var uploadedBannnerImageWidth = 0;
        var uploadedBannnerImageHeight = 0;

        $("#upload").on('click', (function (e) {
            e.preventDefault();
            var file_data = $("#logoimage").prop("files")[0];       // Getting the properties of file from file field
            var form_data = new FormData();                         // Creating object of FormData class
            form_data.append("logoimage", file_data)                // Appending parameter named file with properties of file_field to form_data
            $.ajax({
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                url: '<?php echo base_url(); ?>vendor/home/storeImageUrl',
                processData: false,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (data) {
                    getMeta('<?php echo awsBucketPath ?>' + data);
                    $("#logoimagepreview").attr('src', '<?php echo awsBucketPath; ?>' + data);
                    $("#logoimage1").attr('value', data);
                    $("#logoimagetemp").attr('value', data);
                    $("#spinner").hide();
                    $('#remove_logo').val(0);
                    $('#logo_exist').val(1);
                    $('#discard_logo').prop('disabled', false);

                },
                error: function () {
                }
            });
        }));

        $('#logoimagepreview').load(function () {
            var self = this;
            $('#logoimagepreview').imgAreaSelect({
                aspectRatio: '1:1',
                imageHeight: self.naturalHeight,
                imageWidth: self.naturalWidth,
                onSelectEnd: getSizes,
                resizable: false
            });
            var height = (this.naturalWidth / 1) * 1;
            if (height <= this.naturalHeight) {
                var diff = (this.naturalHeight - height) / 2;
                var coords = {x1: 0, y1: diff, x2: this.naturalWidth, y2: height + diff};
            } else { // if new height out of bounds, scale width instead
                var width = (this.naturalHeight / 1) * 1;
                var diff = (this.naturalWidth - width) / 2;
                var coords = {x1: diff, y1: 0, x2: width + diff, y2: this.naturalHeight};
            }
            $(this).imgAreaSelect(coords);
        });

        setTimePicker = function (index) {

            $("#from_" + index).timepicker({showMeridian: false});
            $("#to_" + index).timepicker({showMeridian: false});

            /* $("#from_"+index).timepicker({ 'timeFormat': 'H:i' });
             $("#to_"+index).timepicker({ 'timeFormat': 'H:i' });*/

            $("#from_" + index).timepicker('setTime', new Date());
            $("#to_" + index).timepicker('setTime', new Date());

            if (applyall == 1)
            {
                $("#from_" + index).timepicker('setTime', defaultfromhoursstr);
                $("#to_" + index).timepicker('setTime', defaulttohoursstr);
            }
        }

        var weekdayarr = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
        var avadays = "<?php echo $details['operatingdays']; ?>";
        var avafromhour = "<?php echo $details['availabilityfromhour']; ?>";
        var avatohour = "<?php echo $details['availabilitytohour']; ?>";
        if (avadays != "" && avadays != 0)
        {
            var avadaysarr = avadays.split(',');
            var avafromhourarr = avafromhour.split(',');
            var avatohourarr = avatohour.split(',');

            for (var i = 0; i < avadaysarr.length; i++)
            {

                var index = weekdayarr.indexOf(avadaysarr[i]);
                dayselected(index);
                $("#from_" + index).val(avafromhourarr[i]);
                $("#to_" + index).val(avatohourarr[i]);

            }
        }

        var selCountry1 = "<?php echo $details['country']; ?>";
        if (selCountry1 != "")
        {
            populateStates(selCountry1);
        }

        var categstr = "<?php echo $details['category']; ?>";
        var categarr = categstr.split(",");
        $("#selecttype").val(categarr);



        $("#country").on('change', function () {
            $('#state option').remove();
            $("#state").append('<option value="">Select State</option>');
            var selCountry = this.value;
            populateStates(selCountry);


        });

<?php if (ALLOW_VET == 1) { ?>//
            //        $('#selecttype option[value="1"]').prop('disabled', true);
            $("#selecttype option").on('click', function () {
                $('#selecttype option[value="1"]').prop('selected', true);
            });
<?php } ?>

        $('#selecttype option').mousedown(function (e) {
            e.preventDefault();
            $(this).prop('selected', $(this).prop('selected') ? false : true);
            return false;
        });

        function populateStates(selCountry)
        {
            var selState1 = "<?php echo $details['state']; ?>";
            var jqueryarray = "";
            if (selCountry == "Canada")
                jqueryarray = <?php echo json_encode($Canadastates); ?>;
            else
                jqueryarray = <?php echo json_encode($USstates); ?>;

            for (var i = 0; i < jqueryarray.length; i++)
            {

                if (selState1.match(jqueryarray[i]))
                {
                    $('#state').append('<option value="' + jqueryarray[i] + '" selected>' + jqueryarray[i] + '</option>');
                } else
                {
                    $('#state').append('<option value="' + jqueryarray[i] + '">' + jqueryarray[i] + '</option>');
                }
            }

            /*if(selState1!="")
             {
             alert(selState1);
             $("#state").val(selState1);
             }*/
        }


        $(".allownumericwithoutdecimal").on("keypress keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });

        $("#update, #updateInfo_s1").click(function () {
            var fname = $("#firstname").val();
            var lname = $("#lastname").val();
            var compname = $("#comapnyname").val();
            var email = $("#email").val();
            var servicetype = $("#selecttype").val();
            var address = $('#address').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var country = $('#country').val();
            var zipcode = $('#zipcode').val();
            var lat = $('#lat').val();
            var lng = $('#lng').val();
            var fromhoursstr = "";
            var tohoursstr = "";
            var website = $("#website").val();
            var validText = /(<([^>]+)>)/ig;

            $.each($("input[name='availabilitydays[]']:checked"), function () {
                //values.push($(this).val());
                var index = weekdayarr.indexOf($(this).val());
                if (fromhoursstr == "")
                    fromhoursstr = $("#from_" + index).val();
                else
                    fromhoursstr = fromhoursstr + "," + $("#from_" + index).val();

                if (tohoursstr == "")
                    tohoursstr = $("#to_" + index).val();
                else
                    tohoursstr = tohoursstr + "," + $("#to_" + index).val();
            });
            $("#availabityfromhours").val(fromhoursstr);
            $("#availabitytohours").val(tohoursstr);

            var optionssel = $('#selecttype > option:selected');
            var hasError = false;
            var errormsg = "";
            if (fname.trim().length == 0) {
                errormsg = 'The First Name field is required!';
                $("#firstname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (fname.trim().length > 25) {
                errormsg = 'The First Name field can be at most 25 characters in length!';
                $("#firstname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (validText.test(fname))
            {
                errormsg = 'No html tags are allowed!';
                $("#firstname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (lname.trim().length == 0) {
                errormsg = 'The Last Name field is required!';
                $("#lastname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (lname.trim().length > 25) {
                errormsg = 'The Last Name field can be at most 25 characters in length!';
                $("#lastname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (validText.test(lname)) {
                errormsg = 'No html tags are allowed!';
                $("#lastname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (compname.trim().length == 0) {
                errormsg = 'The Company Name field is required!';
                $("#comapnyname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (compname.trim().length > 100) {
                errormsg = 'The Company Name field can be at most 100 characters in length!';
                $("#comapnyname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (validText.test(compname)) {
                errormsg = 'No html tags are allowed!';
                $("#comapnyname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (email.trim().length == 0) {
                errormsg = 'The Email field is required!';
                $("#email").css('border-color', '#dd4b39');
                hasError = true;
            } else if (!validateEmail(email)) {
                errormsg = 'The Email field must contain a valid email address';
                $("#email").css('border-color', '#dd4b39');
                hasError = true;
            } else if (website != '' && !ValidURL(website))
            {
                errormsg = 'Provide valid and full URL with http/https for Website';
                $("#website").css('border-color', '#dd4b39');
                hasError = true;
            } else if (country.trim().length == 0) {
                $("#country").css('border-color', '#dd4b39');
                errormsg = "The Country field is required!";
                hasError = true;
            } else if (address.trim().length == 0) {
                $("#address").css('border-color', '#dd4b39');
                errormsg = "The Address field is required!";
                hasError = true;
            } else if (validText.test(address)) {
                $("#address").css('border-color', '#dd4b39');
                errormsg = "No html tags are allowed!";
                hasError = true;
            } else if (state.trim().length == 0) {
                $("#state").css('border-color', '#dd4b39');
                errormsg = "The State field is required!";
                hasError = true;
            } else if (city.trim().length == 0) {
                $("#city").css('border-color', '#dd4b39');
                errormsg = "The City field is required!";
                hasError = true;
            } else if (validText.test(city)) {
                $("#city").css('border-color', '#dd4b39');
                errormsg = "No html tags are allowed!";
                hasError = true;
            } else if (zipcode.trim().length == 0) {
                $("#zipcode").css('border-color', '#dd4b39');
                errormsg = "The Zipcode field is required!";
                hasError = true;
            } else if (validText.test(zipcode)) {
                $("#zipcode").css('border-color', '#dd4b39');
                errormsg = "No html tags are allowed!";
                hasError = true;
            } else if (optionssel.length == 0) {
                errormsg = 'The Category field is required!';
                $("#selecttype").css('border-color', '#dd4b39');
                hasError = true;
            }

            if (fname.trim().length != 0 && fname.trim().length <= 25 && !(validText.test(fname))) {
                $("#firstname").css('border-color', '#d2d6de');
            }
            if (lname.trim().length != 0 && lname.trim().length <= 25 && !(validText.test(lname))) {
                $("#lastname").css('border-color', '#d2d6de');
            }

            if (compname.trim().length != 0 && compname.trim().length <= 100 && !(validText.test(compname))) {
                $("#comapnyname").css('border-color', '#d2d6de');
            }
            if (email.trim().length != 0) {
                $("#email").css('border-color', '#d2d6de');
            }
            if (ValidURL(website) || website.trim().length == 0)
            {
                $("#website").css('border-color', '#d2d6de');
            }

            if (address.trim().length != 0 && !(validText.test(address))) {
                $("#address").css('border-color', '#d2d6de');
            }
            if (city.trim().length != 0 && !(validText.test(city))) {
                $("#city").css('border-color', '#d2d6de');
            }
            if (state.trim().length != 0) {
                $("#state").css('border-color', '#d2d6de');
            }
            if (zipcode.trim().length != 0 && !(validText.test(zipcode))) {
                $("#zipcode").css('border-color', '#d2d6de');
            }
            if (country.trim().length != 0) {
                $("#country").css('border-color', '#d2d6de');
            }
            if (optionssel.length != 0) {
                $("#selecttype").css('border-color', '#d2d6de');
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }

            if (hasError == false) {
                $("#validerrorform").html('');
                $("#formsubmit").val('yes');
                $("#changeProfileForm").submit();
            } else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }
        });

        $(document).on('click', '#applyall', function () {

            if ($(this).is(":checked")) {
                applyall = 1;
                defaultfromhoursstr = $("#from_" + startindex).val();
                defaulttohoursstr = $("#to_" + startindex).val();

                $.each($("input[name='availabilitydays[]']:checked"), function () {
                    //values.push($(this).val());
                    var index = weekdayarr.indexOf($(this).val());
                    $("#from_" + index).val(defaultfromhoursstr);
                    $("#to_" + index).val(defaulttohoursstr);
                });
            }
        });

        $('#discard_logo').click(function () {
            var logo_exist = parseInt($('#logo_exist').val());
            if (logo_exist) {
                if (confirm('Current logo image will be deleted after submitting the update button. Do you wish to continue?')) {
                    $('#remove_logo').val(1);
                    $('#currentImage').attr('src', '');
                    $(this).prop('disabled', true);
                }
            }
        });

    });


    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }

    function ValidURL(str) {
        var pattern = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;// fragment locater
        return pattern.test(str);
    }

    /*
     |--------------------------------------------------------------------------
     | NEW DAYSELECTED AND ADDAPPLYALL METHODS
     |--------------------------------------------------------------------------
     |
     | Rewrote the dayselected method and added addApplyAll() method so that
     | the applyall checkbox could be toggled according to the days
     | selected by the user.
     |
     | VK 7-6-2017
     |
     */

    function dayselected(index) {
        var weekarr = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        var day = weekarr[index];

        if ($("#" + day).length) {
            $("#" + day).remove();
            daycount--;

            addApplyAll();

        } else {
            var html = '<div class="row" id="' + day + '">';
            html += '<div class="col-xs-2">&nbsp;</div>';
            html += '<div class="col-xs-2">' + day + '</div>';
            html += '<div class="col-xs-2"> <div class="input-group "><input id="from_' + index + '" name="from_' + index + '" type="text" class="form-control  time"  /></div></div>';
            html += '<div class="col-xs-2"> <div class="input-group"><input id="to_' + index + '" name="to_' + index + '" type="text" class="form-control  time" /></div></div>';

            html += '</div>';

            $(".availabilitydetail").append(html);

            addApplyAll();

            if (daycount == 0) {
                startindex = index;
            }

            daycount++;

            setTimePicker(index);
        }
    }

    function addApplyAll() {
        var day;
        var week = ['M', 'T', 'W', 'Th', 'F', 'Sa', 'Su'];
        var days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

        var checked = $("input[name='availabilitydays[]']:checked");
        var first = checked[0];

        $.each(week, function (index, value) {
            if (value == $(first).val()) {
                day = days[index];
            }
        });

        html = '<div class="col-xs-2"><input type="checkbox" id="applyall" name="applyall" value="1"> Apply to all</div>';

        if ($('#applyall').length == 0 && checked.length > 1) {
            $('#' + day).append(html);
        } else if ($('#applyall').length > 0 && checked.length < 2) {
            $('#applyall').parent().remove();
        }
    }

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/map-api.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGygmI0eyoD-mZowFiEfb-xGNCrasI8ZI&callback=initMap" type="text/javascript"></script>