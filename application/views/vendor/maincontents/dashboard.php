<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];

$customers = array();
foreach ($all_customers as $val) {
    if ($val['usertype'] == 'appuser') {
        $customers[$val['appuserid']] = $val['firstname'] . ' ' . $val['lastname'];
    } else {
        $customers[$val['customerid']] = $val['firstname'] . ' ' . $val['lastname'];
    }
}
$customers = json_encode($customers);
?>
<link href="<?php echo base_url(); ?>assets/css/fullcalendar_002.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/custom-style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/fullcalendar.css" rel="stylesheet" media="print">
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
<script>

    $(document).ready(function () {
        var all_customer = <?php echo $customers; ?>;

//        console.log(all_customer);

        $('#search_customer').autocomplete({
            minLength: 1,
            source: function (request, response) {
                response($.map(all_customer, function (value, key) {
                    var text = $('#search_customer').val();
                    var text_camel = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                        return letter.toUpperCase();
                    });
                    if (~value.indexOf(text)) {
                        return {
                            label: value,
                            value: key
                        }
                    } else if (~value.indexOf(text.toUpperCase())) {
                        return {
                            label: value,
                            value: key
                        }
                    } else if (~value.indexOf(text.toLowerCase())) {
                        return {
                            label: value,
                            value: key
                        }
                    } else if (~value.indexOf(text_camel)) {
                        return {
                            label: value,
                            value: key
                        }
                    }

                }).slice(0, 20));

            },
            // Once a value in the drop down list is selected, do the following:
            select: function (event, ui) {

                if ($("#all_customer option[value='" + ui.item.value + "']").length <= 0) {
                    $('#all_customer option.dummy').remove();
                    $('#all_customer').append("<option value=\"" + ui.item.value + "\" selected=\"selected\">" + ui.item.label + "</option>");
                }

                return false;
            }
        });
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: new Date(),
            editable: false,
            eventRender: function(event, element, view) {
                element.find('.fc-content')
                    .append("<div class='weekDescription' style='display: none'>" + event.weekdescription + "</div>" +
                        "<div class='dayDescription' style='display: none'>" + event.daydescription + "</div>");
            },

            events:<?php echo $events;?>,
        });
        $('.fc-agendaWeek-button').click(function () {
            $('.weekDescription').show();
        });
        $('.fc-agendaDay-button').click(function () {
            $('.dayDescription').show();
        });


        //check for new appointments
        var url = "<?php echo base_url(); ?>vendor/appointments/newAppointments";


        $.post(url, function (data)
        {
            if (data != "nodata")
            {
                $("#newapptnotification").fadeIn("slow").append('A user has scheduled an appointment with you through the BabelBark App! The appointment has been added to your calendar.');
                $("#newapptnotification").append("<br>" + data);
            }

        });

        $(".dismiss").click(function () {

            //update status as read
            var url1 = "<?php echo base_url(); ?>vendor/appointments/updateAppointmentStatusToRead";

            $.post(url1, function (data)
            {
                if (data == 1)
                {

                }
                $("#newapptnotification").fadeOut("slow");
            });
        });

        $('.label-customer').css({'display': 'block'});
        $('#clear_customers').click(function () {
            $('#all_customer option').remove();
            $('#all_customer').append('<option value="0" class="dummy">No Customers Selected</option>');
        });
        $('#clear_employee').click(function () {
           $("#all_employee option:selected").prop("selected", false)
        });
        $('#clear_service').click(function () {
            $("#all_service option:selected").prop("selected", false)
        });
        $("select").mousedown(function(e){
            e.preventDefault();

            var select = this;
            var scroll = select.scrollTop;

            e.target.selected = !e.target.selected;

            setTimeout(function(){select.scrollTop = scroll;}, 0);

            $(select).focus();
        }).mousemove(function(e){e.preventDefault()});

    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }
    function inviteUser()
    {

        var email = $("#inviteEmail").val();

        if (email == '')
        {

            $("#validerror").html('Enter Email ID');
        }
        else if (!validateEmail(email)) {
            $("#validerror").html('Enter valid Email ID');
        }
        else
        {
            $("#validerror").html('');
            var url = "<?php echo base_url(); ?>vendor/customer/inviteUser";

            $.post(url, {email: email}, function (data)
            {
                if (data == '1')
                {
                    $("#inviteEmail").val('');
                    $('#generic').modal('hide');
                    alert("Invitation mail sent successfully to " + email);
                }
                else
                {

                }
            });
        }
    }

</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Home</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="newapptnotification" style="display: none;">
            <span class="dismiss"><a title="dismiss this notification" class="close">&times;</a></span>
        </div>
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Appointments</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form action="" id="calender_filter" method="post">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="all_service" class="label-customer">Service <span class="btn btn-danger btn-xs pull-right" id="clear_service">Clear</span></label>
                                    <select id="all_service" class="form-control multiselect" multiple="multiple" name="vendor_services[]">
                                        <?php
                                        foreach ($all_services as $val) {
                                            echo '<option value="' . $val->serviceid . '"' . (in_array($val->serviceid, $selected_service) ? ' selected="selected"' : '') . '>' . $val->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="employee" class="label-customer">Employee <span class="btn btn-danger btn-xs pull-right" id="clear_employee">Clear</span></label>
                                    <select id="all_employee" class="form-control multiselect" name="vendor_employee[]" multiple="multiple">
                                        <?php
                                        echo '<option value="' . $vendor_data['value'] . '"' . (in_array($vendor_data['value'], $selected_employee) ? ' selected="selected"' : '') . '>' . $vendor_data['name'] . '</option>';
                                        foreach ($all_employee as $val) {
                                            echo '<option value="' . $val->userid . '"' . (in_array($val->userid, $selected_employee) ? ' selected="selected"' : '') . '>' . $val->firstname . '&nbsp;' . $val->lastname . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Search Customer:</label>
                                        <input type="text" class="form-control" id="search_customer" placeholder="Begin Typing to Search">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label for="customer" class="label-customer">Customers <span class="btn btn-danger btn-xs pull-right" id="clear_customers">Clear</span></label>
                                    <select id="all_customer" class="form-control multiselect" name="vendor_customer[]" multiple="multiple">
                                        <?php
                                        if (!empty($selected_customer)) {
                                            foreach ($all_customers as $val) {
                                                if (in_array($val['appuserid'], $selected_customer)) {
                                                    echo '<option value="' . $val['appuserid'] . '" selected="selected">' . $val['firstname'] . '&nbsp;' . $val['lastname'] . '</option>';
                                                } else if (in_array($val['customerid'], $selected_customer)) {
                                                    echo '<option value="' . $val['customerid'] . '" selected="selected">' . $val['firstname'] . '&nbsp;' . $val['lastname'] . '</option>';
                                                } else {
                                                    continue;
                                                }
                                            }
                                        } else {
                                            echo '<option value="0" class="dummy" disabled="disabled">No Customers Selected</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <input type="submit" class="btn btn-primary btn-calender-filter" name="submit_filter" value="Filter Calender">
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-sm-12" id="calendar"></div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- end box -->
            </div><!-- end col-sm-12 -->
        </div><!-- end row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="generic" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Invite User to BabelBark</h4>
            </div>
            <section class="content">
                <div class="row">

                    <div class="col-lg-9 col-xs-12 ">
                        <div class="form-group">

                            <div class="col-sm-12">	<center><input id="inviteEmail" name="inviteEmail" class="form-control" type="text" placeholder="Enter user's email address" value="<?php echo set_value('customername'); ?>" maxlength="200"></center></div>
                            <div class="col-sm-12" style="margin-top: 20px;">
                                <button class="btn btn-warning" name='submit'  onclick="javascript:inviteUser();">Invite</button></div>
                            <div class="col-sm-12"> <label class="text-red" id="validerror"></label></div>

                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="row">

                        <div class="col-lg-9 col-xs-12 ">

                            <label> Email Preview : </label><br>
                            <p>
                                <?php
                                $admin = $this->session->userdata('admin');
                                $vendorid = $admin['adminDetails']['vendorid'];
                                $vendorfname = $admin['adminDetails']['firstname'];
                                $vendorlname = $admin['adminDetails']['lastname'];
                                $sendmessage = file_get_contents('resources/invitemail_template.html');
                                $sendmessage = str_replace('%vendorfname%', $vendorfname, $sendmessage);
                                $sendmessage = str_replace('%vendorlname%', $vendorlname, $sendmessage);
                                echo $sendmessage;
                                ?>
                            </p>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

