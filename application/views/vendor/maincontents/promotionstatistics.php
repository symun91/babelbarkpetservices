    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Promotions Statistics for PromoCode: <?php echo $promocode;?>
          
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Promotions</a></li>
            <li class="active">Promotions Statistics</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
   <div style="width: 100%">
       Redeemed Count
			<canvas id="canvas" height="250" width="600"></canvas>
		
        </div>
        </section>
        
         <div class="col-lg-12 col-xs-12">
								<div class="box-footer">
								<?php 
				                	 echo anchor('vendor/promotions/listing','Back','class="btn  btn-primary"');
				                	?>
								</div>
		</div>
        </div>
       
         <script type="text/javascript">


      var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	 	var barChartData = {
	 		labels : <?php print_r( $daysarray);?>,
	 		datasets : [
	 			{
	 				fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,0.8)",
					highlightFill : "rgba(151,187,205,0.75)",
					highlightStroke : "rgba(151,187,205,1)",
	 				data :<?php print_r( $redeemcntdata);?>
	 			}
	 		]
	 	}

	 	var ctx = $("#canvas").get(0).getContext("2d");
	 	var myNewChart  = new Chart(ctx).Bar(barChartData, {
			responsive : true,
			scaleOverride: true,
			
		    scaleSteps: <?php echo $capacity?>,
		    scaleStepWidth: 1,
		    scaleStartValue: 0
		});
		</script>
<?php 


?>