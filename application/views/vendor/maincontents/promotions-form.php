<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>
<style>
    .box{
        border-top: 0 !important;
    }
    #updateInfo_s1, #updateInfo_s2, .close_s1, .close_s2 {
        margin-right: 2%;
    }
</style>
<div id="spinner" 
     style="display:none;position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url(); ?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            if ($detail['promotionid'] == 0)
                echo "Add Promotion";
            else
                echo "Promotion Details";
            ?>
        </h1>
        <?php if ($detail['promotionid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Promotions</a></li>
                <li class="active">Add Promotion </a></li>
            </ol>
        <?php } ?>
    </section>
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <form name="addpromotionform" id="addpromotionform"  action="<?php echo base_url(); ?>vendor/promotions/updatePromotions" method="POST" enctype="multipart/form-data"  > 
            <input type="hidden" name="promotionid" id="promotionid" value="<?php echo $detail['promotionid']; ?>"/>
            <div class="row">
                <div class="col-md-12">
                    <div class="stepwizard col-md-offset-3">
                        <div class="stepwizard-row setup-panel">
                            <div class="stepwizard-step">
                                <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                <p>Promotion Details</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-2" type="button" class="btn btn-default btn-circle" <?php echo $detail['promotionid'] == 0 ? 'disabled="disabled"' : '';?>>2</a>
                                <p>Pet Information</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-3" type="button" class="btn btn-default btn-circle" <?php echo $detail['promotionid'] == 0 ? 'disabled="disabled"' : '';?>>3</a>
                                <p>Promotional Details</p>
                            </div>
                        </div>
                    </div>
                    <!--############################# STEP 1 start ##########################################-->
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-12 col-md-12">
                            <div class="box box-warning " >
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-8 col-xs-12 ">
                                            <div class="form-group">
                                                <label for="promotionname">Promotion Name<font color="red">*</font></label>
                                                <input id="promotionname" class="form-control" required title="Promotion Name" type="text" placeholder="Promotion name" name="promotionname" value="<?php echo set_value('promotionname', $detail['name']); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 col-xs-12 ">
                                            <div class="form-group">
                                                <label for="promotiondesc">Promotion Description</label>
                                                <textarea class="form-control countable" placeholder="Promotion Description" rows="3" id="promotiondesc" name="promotiondesc" data-length="1500"><?php echo set_value('promotiondesc', $detail['description']); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 col-xs-12 ">
                                            <div class="form-group">
                                                <?php
                                                $selcategarr = array();
                                                if ($detail['category'] != "") {
                                                    $selcategarr = explode(",", $detail['category']);
                                                }
                                                ?>
                                                <label>Category<font color="red">*</font></label>
                                                <select name="selecttype[]" id="selecttype" required title="Please Select Category" class="form-control" multiple="multiple">
                                                    <?php if (count($servicetypes) == 1) { ?>
                                                        <option value="<?php echo $servicetypes[0]['categoryid']; ?>" selected><?php echo $servicetypes[0]['title']; ?></option>
                                                    <?php } else { ?>      
                                                        <?php for ($i = 0; $i < count($servicetypes); $i++) { ?>
                                                            <?php $selected = in_array($servicetypes[$i]['categoryid'], $selcategarr) ? " selected " : null; ?>
                                                            <option value="<?php echo $servicetypes[$i]['categoryid']; ?>" <?php echo $selected; ?>><?php echo $servicetypes[$i]['title']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 col-xs-12 ">
                                            <div class="form-group">
                                                <label for="duration">Duration <font color="red">*</font></label>
                                                <div class="row">
                                                    <div class="col-sm-6">  
                                                        <?php
                                                        $sdate = $detail['beginingdate'];
                                                        if($sdate == ''){
                                                            $startdate = "";
                                                        }else{
                                                            $startdate = date('F d , Y', strtotime($sdate));
                                                        }
                                                        ?>
                                                        <input type="text" required title="Please Enter Beginnig Date" class="form-control datepicker" placeholder="From" name="startdate" id="startdate" data-date-format="MM dd, yyyy" value="<?php echo set_value('startdate', $startdate); ?>"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <?php
                                                        $expdate = $detail['expirationdate'];
                                                        if($expdate == ''){
                                                            $expirydate = "";
                                                        }else{
                                                            $expirydate = date('F d , Y', strtotime($expdate));
                                                        }
                                                        ?>
                                                        <input type="text" required title="Please Enter Expiration Date" class="form-control datepicker" placeholder="To" name="expirydate" id="expirydate" data-date-format="MM dd, yyyy" value="<?php echo set_value('expirydate', $expirydate); ?>"/>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <button class="btn btn-primary nextBtn  pull-right" type="button" value="step-1">Next</button>
                                            <?php if ($detail['promotionid'] != 0){ ?>                                            
                                            <button class="btn btn-warning pull-right" id="updateInfo_s1">Save</button>                                            
                                            <?php echo anchor('vendor/promotions/listing','Close','class="btn  btn-danger pull-right close_s1"');?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--############################# STEP 1 end ##########################################-->
                    <!--############################# STEP 2 start ##########################################-->
                    <div class="row setup-content" id="step-2">
                        <div class="col-xs-12 col-md-12">
                            <div class="box box-warning" >
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">                                    
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label for="">Pet Adopted</label>
                                                <input type="checkbox" name="isadopted" value="1" <?php echo ($detail['isadopted'] == '1' ? 'checked' : null); ?>/>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="targetpopulation">Target pet population</label>
                                                        <br>
                                                        <label for="targetpopulation">Age range (in years)</label>
                                                        <div class="row">                                                  
                                                            <div class="col-sm-6"><input id="agerangefrom" class="form-control" type="number" min="0" placeholder="From" name="agerangefrom" value="<?php echo set_value('agerangefrom', $detail['agerangefrom']); ?>"></div>
                                                            <div class="col-sm-6"><input id="agerangeto" class="form-control" type="number" min="0" placeholder="To" name="agerangeto" value="<?php echo set_value('agerangeto', $detail['agerangeto']); ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-xs-12 ">
                                                    <div class="form-group">
                                                        <label for="targetpopulation">Weight Range (<?php
                                                            if ($unittype == 'Metric') {
                                                                echo 'kgs';
                                                                $targetWtFrom = $detail['targetweightsfrom'];
                                                                $targetWtTo = $detail['targetweightsto'];
                                                            } else {
                                                                echo 'lbs';
                                                                $targetWtFrom = ($detail['targetweightsfrom'] * 2.20462);
                                                                $targetWtTo = ($detail['targetweightsto'] * 2.20462);
                                                            }
                                                            ?>)</label>
                                                        <div class="row" style="margin-top: 10px;">                                                    
                                                            <div class="col-sm-6"><input id="weightfrom" class="form-control" type="text" min="0" placeholder="From" name="weightfrom" value="<?php echo set_value('weightfrom', $targetWtFrom); ?>"></div>
                                                            <div class="col-sm-6"><input id="weightto" class="form-control" type="text" min="0" placeholder="To" name="weightto" value="<?php echo set_value('weightto', $targetWtTo); ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">	
                                                <div class="col-sm-10">
                                                    <label for="">Breeds</label>
                                                    <div class="form-group">                                                
                                                        <?php
                                                        $selbreedarr = array();
                                                        if ($detail['targetbreeds'] != "") {
                                                            $selbreedarr = explode(",", $detail['targetbreeds']);
                                                        }
                                                        ?>
                                                        <select name="breeds[]" class="form-control" multiple="multiple">
                                                            <option value="" >Select</option>
                                                            <?php for ($i = 0; $i < count($breeds); $i++) { ?>
                                                                <?php $selected = in_array($breeds[$i]['breedid'], $selbreedarr) ? " selected " : null; ?>
                                                                <option value="<?php echo $breeds[$i]['breedid']; ?>"  <?php echo $selected; ?>><?php echo $breeds[$i]['title']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <!-- This field is now comment issue-181   
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <div class="form-group">
                                                <label for="">Target subscribed customers</label>
                                                <input type="checkbox" name="targetsubscribedcustomers" value="1" <?php echo ($detail['targetsubscribedcustomers'] == '1' ? 'checked' : null); ?>/>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                            <button class="btn btn-primary nextBtn pull-right" type="button" value="step-2">Next</button>
                                            <?php if ($detail['promotionid'] != 0){ ?>
                                            <button class="btn btn-warning pull-right" id="updateInfo_s2">Save</button>
                                            <?php echo anchor('vendor/promotions/listing','Close','class="btn  btn-danger pull-right close_s2"');?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--############################# STEP 2 end ##########################################-->
                    <!--############################# STEP 3 start ##########################################-->
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-9 col-md-12">
                            <div class="box box-warning " >
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <label>Promotional URL </label>
                                                Enter URL  <input type="checkbox" name="urlchk" value="external" <?php echo set_value('urlchk', $detail['prmourltype']) == "external" ? "checked" : ""; ?> />&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="col-lg-8 col-xs-12 ">
                                                <input id="promotionalurl" class="form-control" type="text" placeholder="Promotional URL" name="promotionalurl" value="<?php echo set_value('promotionalurl', $detail['promotionalurl']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="bannerimage1" id="bannerimage1"  style="display:none"/>
                                    <input type="hidden" name="bannerimagetemp" id="bannerimagetemp"  style="display:none"/>
                                    </form>                                    
                                    <div class="row">
                                        <div class="col-lg-9 col-xs-12 ">
                                            <form id="upload">
                                                <div class="form-group">
                                                    <label for=""  style="margin-top: 10px;">Promotional App Banner </label>
                                                    <div class="row"  style="margin-top: 10px;">
                                                        <div class="col-sm-5">
                                                            <input class="form-control" type="file" id="bannerimage" name="bannerimage"></div>
                                                        <input type="submit" class="btn btn-warning" name="upload" id="upload" value="Upload">
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="form-group">
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-lg-9 col-xs-12 ">
                                                        <?php if ($detail['bannerimage'] != "") { ?>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="input-group">
                                                                        Preview Image
                                                                        <a href="#" class="thumbnail" >
                                                                            <img src="<?php echo empty_image('', ''); ?>" id="bannerpreview" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-8">
                                                                    Current Image
                                                                    <a href="#" class="thumbnail" style="width: 600px; height: 100px;">
            <!--							            	<img src="--><?php //echo base_url().$detail['bannerimage'];                           ?><!--" id="bannercurrentImage" style="width:  600px; height: 90px;"/>-->
                                                                        <img src="<?php echo awsBucketPath . $detail['bannerimage']; ?>" id="bannercurrentImage" style="width:  600px; height: 90px;"/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="input-group">
                                                                        Preview Image
                                                                        <a href="#" class="thumbnail" >
                                                                            <img src="<?php echo empty_image('', ''); ?>" id="bannerpreview" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    Banner to be shown in BabelBark App
                                                                    <a href="#" class="thumbnail" style="width: 500px; height: 100px;">
                                                                        <img src="<?php echo empty_image('', ''); ?>" id="bannercurrentImage" style="width:  600px; height: 90px;"/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                                            <?php
                                            if ($userrole == "admin") {
                                                if ($detail['promotionid'] == 0) {
                                                    ?>
                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Save">
                                                <?php } else {
                                                    ?>
                                                    <input type="button" class="btn btn-warning pull-right" name="update" id="update" value="Update">
                                                    <?php echo anchor('vendor/promotions/listing','Close','class="btn  btn-danger pull-right close_s2"');?>
                                                    <?php
                                                }
                                            }
                                            # echo anchor('vendor/promotions/listing', 'Back', 'class="btn  btn-primary"');
                                            ?>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--############################# STEP 3 end ##########################################-->
                </div>
            </div>
        </form>
    </section>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imgareaselect-default.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        var start = new Date();
        var end = new Date(new Date().setYear(start.getFullYear()+1));
        $('#startdate').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){
            $('#expirydate').datepicker('setStartDate', new Date($(this).val()));
        }); 

        $('#expirydate').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){
            $('#startdate').datepicker(new Date($(this).val()));
        });
    });
    
    function getSizes(im, obj)
    {
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = uploadedBannnerImageWidth;
        var thumb_height = uploadedBannnerImageHeight;
        var img = $("#bannerimagetemp").val();
        /* img=img.split('/bizbark/');
         img=img[1];*/
        /*alert(img);*/
        if (thumb_width > 0)
        {
            var data_ = {
                x_axis: x_axis,
                y_axis: y_axis,
                x2_axis: x2_axis,
                y2_axis: y2_axis,
                thumb_width_: uploadedBannnerImageWidth,
                thumb_height_: uploadedBannnerImageHeight,
                img_: img
            };
            $.ajax({
                type: "post",
                data: data_,
                cache: false,
                url: '<?php echo base_url(); ?>vendor/promotions/upload_thumbnail',
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (data) {
                    /*console.log(data);*/

                    var d = new Date();
                    var newdata = data + '?' + d.getTime();

                    $("#bannercurrentImage").attr('src', '<?php echo base_url(); ?>' + newdata);
                    $("#bannerimage1").attr('value', data);
                    $("#spinner").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(ajaxOptions);
                    console.log(thrownError);

                }
            });
        }
    }
    function getMeta(url) {
        var img = new Image();
        img.addEventListener("load", function () {
            uploadedBannnerImageWidth = this.naturalWidth;
            uploadedBannnerImageHeight = this.naturalHeight;
        });
        img.src = url;
    }

    $(window).load(function () {
        $("#validation").hide();
    });

    $(document).ready(function () {
        var uploadedBannnerImageWidth = 0;
        var uploadedBannnerImageHeight = 0;

        $("#upload").on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                url: '<?php echo base_url(); ?>vendor/promotions/storeImageUrl',
                processData: false,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (data) {
                    getMeta('<?php echo awsBucketPath; ?>' + data);
                    $("#bannerpreview").attr('src', '<?php echo awsBucketPath; ?>' + data);
//   				 getMeta('<?php //echo base_url();   ?>//'+data);
//   				 $("#bannerpreview").attr('src','<?php //echo base_url();   ?>//'+data);
                    $("#bannerimage1").attr('value', data);
                    $("#bannerimagetemp").attr('value', data);
                    $("#spinner").hide();

                },
                error: function () {}
            });
        }));

        $('#bannerpreview').load(function () {
            var self = this;
            $('#bannerpreview').imgAreaSelect({
                aspectRatio: '6:1',
                imageHeight: self.naturalHeight,
                imageWidth: self.naturalWidth,
                onSelectEnd: getSizes,
                resizable: false
            });
            var height = (this.naturalWidth / 6) * 1;
            if (height <= this.naturalHeight) {
                var diff = (this.naturalHeight - height) / 2;
                var coords = {x1: 0, y1: diff, x2: this.naturalWidth, y2: height + diff};
            } else { // if new height out of bounds, scale width instead
                var width = (this.naturalHeight / 1) * 6;
                var diff = (this.naturalWidth - width) / 2;
                var coords = {x1: diff, y1: 0, x2: width + diff, y2: this.naturalHeight};
            }
            $(this).imgAreaSelect(coords);


        });
        /*$('#bannerpreview').load(function(){
         
         var height = ( this.width / 6 ) * 1;
         if( height <= this.height ){     
         var diff = ( this.height - height ) / 2;
         var coords = { x1 : 0, y1 : diff, x2 : this.width, y2 : height + diff };
         }   
         else{ // if new height out of bounds, scale width instead
         var width = ( this.height / 1 ) * 6; 
         var diff = ( this.width - width ) / 2;
         var coords = { x1 : diff, y1 : 0, x2 : width + diff, y2: this.height };
         }   
         $( this ).imgAreaSelect(coords);
         });*/
        $('#selecttype option').mousedown(function (e) {
            e.preventDefault();
            $(this).prop('selected', $(this).prop('selected') ? false : true);
            return false;
        });
        $('.datepicker').datepicker({
            startDate: new Date(),
            format: "MM dd, yyyy"
        });
        $('[name="breeds[]"]').DualListBox({
            json: false,
            moveAllBtn: false,
            title: "Breeds"
        });
        var userrole = "<?php echo $userrole; ?>";
        if (userrole == "employee")
            $(".box-body :input").attr("disabled", "disabled");


        $("#promotionalurl").hide();
        $("#webpage").hide();
        $("#graphicUrl").hide();

        var detailurltype = "<?php echo $detail['prmourltype']; ?>";
        if (detailurltype != "")
        {
            if (detailurltype == "external")
            {
                $("#promotionalurl").show();
                $("#webpage").hide();
                $("#graphicUrl").hide();
            } else if (detailurltype == "graphicUrl")
            {
                $("#promotionalurl").hide();
                $("#webpage").hide();
                $("#graphicUrl").show();
            } else if (detailurltype == "internal")
            {
                $("#promotionalurl").hide();
                $("#webpage").show();
                $("#graphicUrl").hide();
            } else
            {
                $("#promotionalurl").hide();
                $("#webpage").hide();
                $("#graphicUrl").hide();
            }
        }

        $('input:checkbox[name="urlchk"]').click(function () {

            var urltype = $('input[name="urlchk"]:checked').val();
            if (urltype == "external")
            {
                $("#promotionalurl").show();
                $("#webpage").hide();
                $("#graphicUrl").hide();
            } else if (urltype == "graphicUrl")
            {
                $("#promotionalurl").hide();
                $("#webpage").hide();
                $("#graphicUrl").show();
            } else
            {
                $("#promotionalurl").hide();
                $("#webpage").show();
                $("#graphicUrl").hide();
            }

        });


        $(".allownumericwithoutdecimal").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });

        $("#update, #updateInfo_s1, #updateInfo_s2").click(function () {
            var promotionname = $("#promotionname").val();
            var startdate = $("#startdate").val();
            var expirydate = $("#expirydate").val();
            var capacity = $("#capacity").val();
            var category = $('[name="selecttype[]"]').val();
            var agefromrange = $("#agerangefrom").val();
            var agetorange = $("#agerangeto").val();
            //var weightfrom = $("#weightfrom").val();
            //var weightto = $("#weightto").val();
            var weightfrom = parseFloat($("#weightfrom").val()) || 0;
            var weightto = parseFloat($("#weightto").val()) || 0;
            var actscoresmin = $("#actscoresmin").val();
            var actscoresmax = $("#actscoresmax").val();
            var hasError = false;
            var errormsg = "";
            var validText = /(<([^>]+)>)/ig;

            if (promotionname.trim().length == 0)
            {
                errormsg = 'The Promotion Name field is required!';
                $("#promotionname").css('border-color', '#dd4b39');
                hasError = true;

            } else if (validText.test(promotionname))
            {
                errormsg = 'No html tags are allowed!';
                $("#promotionname").css('border-color', '#dd4b39');
                hasError = true;

            } else if ($("#selecttype :selected").length == 0)
            {
                errormsg = 'The Category field is required!';
                hasError = true;
            } else if (startdate.trim().length == 0)
            {
                errormsg = 'The Start Date field is required!';
                $("#startdate").css('border-color', '#dd4b39');
                hasError = true;
            } else if (expirydate.trim().length == 0)
            {
                errormsg = 'The End Date field is required!';
                $("#expirydate").css('border-color', '#dd4b39');
                hasError = true;
            } else if (Date.parse(startdate) > Date.parse(expirydate))
            {
                errormsg = 'End Date cannot be before than Start Date!';
                $("#expirydate").css('border-color', '#dd4b39');
                hasError = true;
            } else if (agefromrange < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#agerangefrom").css('border-color', '#dd4b39');
                hasError = true;
            } else if (agetorange < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#agerangeto").css('border-color', '#dd4b39');
                hasError = true;
            } else if (agefromrange > agetorange) {
                errormsg = 'From Age cannot be larger than To Age!';
                $("#agerangefrom").css('border-color', '#dd4b39');
                $("#agerangeto").css('border-color', '#dd4b39');
                hasError = true;
                isValid = false;
            }else if (weightfrom < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#weightfrom").css('border-color', '#dd4b39');
                hasError = true;
            } else if (weightto < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#weightto").css('border-color', '#dd4b39');
                hasError = true;
            } else if (weightfrom > weightto)
            {
                errormsg = 'From Weight cannot be larger than To Weight!';
                 $("#weightfrom").css('border-color', '#dd4b39');
                $("#weightto").css('border-color', '#dd4b39');
                hasError = true;
            }else if (actscoresmin < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#actscoresmin").css('border-color', '#dd4b39');
                hasError = true;
            } else if (actscoresmax < 0)
            {
                errormsg = 'Negative values are not allowed!';
                $("#actscoresmax").css('border-color', '#dd4b39');
                hasError = true;
            }

            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }

            if (promotionname.trim().length != 0 && !(validText.test(promotionname)))
            {
                $("#promotionname").css('border-color', '#d2d6de');
            }

            if (startdate.trim().length != 0)
            {
                $("#startdate").css('border-color', '#d2d6de');
            }

            if (expirydate.trim().length != 0)
            {
                $("#expirydate").css('border-color', '#d2d6de');
            }
            if (agefromrange < agetorange || agefromrange == "")
            {
                $("#agerangefrom").css('border-color', '#d2d6de');
            }
            if (agetorange > agefromrange || agetorange == "")
            {
                $("#agerangeto").css('border-color', '#d2d6de');
            }
            if (weightfrom < weightto || weightfrom == "")
            {
                $("#weightfrom").css('border-color', '#d2d6de');
            }
            if (weightto > weightfrom || weightto == "")
            {
                $("#weightto").css('border-color', '#d2d6de');
            }
            if (actscoresmin >= 0 || actscoresmin == "")
            {
                $("#actscoresmin").css('border-color', '#d2d6de');
            }
            if (actscoresmax >= 0 || actscoresmax == "")
            {
                $("#actscoresmax").css('border-color', '#d2d6de');
            }
            if (capacity > 0)
            {
                $("#capacity").css('border-color', '#d2d6de');
            }

            if (hasError == false) {
                $("#validerrorform").html('');
                $("#addpromotionform").submit();
            } else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                ;
                return false;
            }
        });

    });
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("bannerimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("bannerpreview").src = oFREvent.target.result;
        };
    }
    ;

    function graphicPreviewImage()
    {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("graphicimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("graphicpreview").src = oFREvent.target.result;
        };
    }
    ;

    function webPreviewImage()
    {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("webpageimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("webpageimgpreview").src = oFREvent.target.result;
        };
    }
    ;



</script>


<script>
    //form validation step by step   

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

        // Start of Appointment add/update form validation //
        var errormsg = "";
        var hasError = false;
        var step = $(this).attr('value');
        var promotionname = $('#promotionname').val();
        var selecttype = $('[name="selecttype[]"]').val();
        var startdate = $("#startdate").val();
        var expirydate = $("#expirydate").val();

        var capacity = $("#capacity").val();

        var agefromrange = parseFloat($("#agerangefrom").val()) || 0;
        var agetorange = parseFloat($("#agerangeto").val()) || 0;
        var weightfrom = parseFloat($("#weightfrom").val()) || 0;
        var weightto = parseFloat($("#weightto").val()) || 0;
        var actscoresmin = $("#actscoresmin").val();
        var actscoresmax = $("#actscoresmax").val();
        var validText = /(<([^>]+)>)/ig;

        switch (step) {
            case "step-1":
                if (promotionname.trim().length == 0)
                {
                    errormsg = 'The Promotion Name field is required!';
                    $("#promotionname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (validText.test(promotionname))
                {
                    errormsg = 'No html tags are allowed!';
                    $("#promotionname").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;

                } else if (selecttype <= 0)
                {
                    errormsg = 'The Category field is required!';
                    $("#selecttype").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (startdate.trim().length == 0)
                {
                    errormsg = 'The Start Date field is required!';
                    $("#startdate").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (expirydate.trim().length == 0)
                {
                    errormsg = 'The End Date field is required!';
                    $("#expirydate").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (Date.parse(startdate) > Date.parse(expirydate))
                {
                    errormsg = 'End Date cannot be before than Start Date!';
                    $("#expirydate").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }
                if (promotionname.trim().length != 0 && !(validText.test(promotionname))) {
                    $("#promotionname").css('border-color', '#d2d6de');
                }
                if (selecttype > 0)
                {
                    $("#selecttype").css('border-color', '#d2d6de');
                }
                if (startdate.trim().length != 0)
                {
                    $("#startdate").css('border-color', '#d2d6de');
                }
                if (expirydate.trim().length != 0)
                {
                    $("#expirydate").css('border-color', '#d2d6de');
                }
                break;

            case "step-2":
                if (actscoresmin < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#actscoresmin").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (actscoresmax < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#actscoresmax").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (actscoresmin > actscoresmax)
                {
                    errormsg = 'Activity Score Min value must be less than Activity Score Max value!';
                    $("#actscoresmin").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (agefromrange < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#agerangefrom").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (agetorange < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#agerangeto").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (agefromrange > agetorange) {
                    errormsg = 'From Age cannot be larger than To Age!';
                    $("#agerangefrom").css('border-color', '#dd4b39');
                    $("#agerangeto").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (weightfrom < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#weightfrom").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (weightto < 0)
                {
                    errormsg = 'Negative values are not allowed!';
                    $("#weightto").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                } else if (weightfrom > weightto)
                {
                    errormsg = 'From Weight cannot be larger than To Weight!';
                    $("#weightfrom").css('border-color', '#dd4b39');
                    $("#weightto").css('border-color', '#dd4b39');
                    hasError = true;
                    isValid = false;
                }

                if (actscoresmin >= 0 || actscoresmin == "")
                {
                    $("#actscoresmin").css('border-color', '#d2d6de');
                }
                if (actscoresmax >= 0 || actscoresmax == "")
                {
                    $("#actscoresmax").css('border-color', '#d2d6de');
                }
                if (agefromrange < agetorange || agefromrange == "")
                {
                    $("#agerangefrom").css('border-color', '#d2d6de');
                }
                if (agetorange > agefromrange || agetorange == "")
                {
                    $("#agerangeto").css('border-color', '#d2d6de');
                }
                if (weightfrom < weightto || weightfrom == "")
                {
                    $("#weightfrom").css('border-color', '#d2d6de');
                }
                if (weightto > weightfrom || weightto == "")
                {
                    $("#weightto").css('border-color', '#d2d6de');
                }
                break;
            case "step-3":
                break;
        }

        if (hasError) {
            $('#validation').addClass("alert alert-danger");
            $('#validation').show();
            $("#validerrorform").html(errormsg);
        } else {
            $('#validation').hide();
            $("#validerrorform").html('');
        }

        // End of Appointment add/update form validation //

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alert(curInputs[i].val());
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    // End of section wise form //

</script>