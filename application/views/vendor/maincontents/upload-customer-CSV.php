<?php $admin=$this->session->userdata('admin');
					$userrole=$admin['role'];
?>
  <div id="spinner" 
  style="display:none;position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url();?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
</div>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
 <!-- Content Header (Page header) -->
       
         <section class="content-header">
          <h1>
          
           <?php  echo "Import CSV";  ?>
          </h1>
        
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customer</a></li>
            <li class="active">Import CSV</a></li>
          </ol>
      
        </section>

        <!-- Main content -->
        <section class="content">
        
        <div class="box-body">
        <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
              <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
        </div>
        <?php //echo '<pre>'; print_r($row); echo '</pre>';?>
        
        <style>
        	.collapsed-box {
        		border:2px solid #F9CC85;
        	}
        </style>
          <form name="csvform" id="csvform" action="<?php echo base_url();?>vendor/customer/importCSV" method="POST"  enctype="multipart/form-data">
       
        <div class="row">
            <div class="col-md-12">
              
              <div class="box box-warning" >
            
              	<div class="box-header with-border">
                  <h3 class="box-title">Upload CSV</h3>
                
                </div><!-- /.box-header -->
             
                <div class="box-body">
               
				    <div class="form-group">
						 <label>Select File<font color="red">*</font></label>
						  <div class="row">
				          <div class="col-xs-6">
					     <input type="file" class="form-control" id="csvfile" name="csvfile" />
				      </div>
					</div> 
					</div>
					
					
					
				        
				     
				   
                </div>
				 
                </div><!-- /.box-body -->
                
                
              </div>
              
              
              
            
              
            <div class="col-lg-12 col-xs-12">
								<div class="box-footer">
								
				                <input type="submit" class="btn btn-warning" onclick="showloader()" name="update" id="update" value="Upload">
				                
				                	<?php 
				                
				                	 echo anchor('vendor/customer/listofgenericcustomer','Back','class="btn  btn-primary"');
				                	?>
				            	</div>
							</div>	
              </div> 
             
             
            <div class = "box-body">
                <ol>
                 <li><h5> <a href="<?php echo base_url();?>resources/downloads/CustomerPetCSV_sample.csv" download>Download Sample CSV File</a></h5></li>
                 <li><h5> Open CSV file in Excel or any other text editing software</h5></li>
                 <li><h5> Add your customers' details to the CSV file</h5></li>
                      <h5><font color="red">*</font>(Customer first name, last name, pet name, country & state are mandatory)</h5>
                 <li><h5> Country must be entered as United States or Canada </h5></li>
                 <li><h5> State full name must be entered</h5></li>
                 <li><h5> Save the file in .csv format (for Mac Excel users, please save in Windows Comma Separated format)</h5></li>
                 <li><h5> Upload the file to BizBark</h5></li>
                 <!-- <h5><font color="red">*</font>Note that the state field will only display in customer profile if country information is also populated</h5> -->
                </ol>

            </div>
        </div>
        <!-- /.col -->
            

          
          
			</form>
        </section><!-- /.content -->
        
         </div><!-- /.content-wrapper -->
         
           <script type="text/javascript">

           function showloader(){
            $('#spinner').show();

           }
      $(document).ready(function(){

    	$('form#csvform').submit(function(){
        $(this).find('input[type=submit]').prop('disabled', true);
      });
      
    	  /*$("#update").click(function() {
    		  var firstname = $("#firstname").val();
    		  var email = $("#email").val();
    		  var password = $("#password").val();
    		  var confirmPassword = $("#confirmPassword").val();
    		 
    		  var hasError = false;
    		  if (firstname == '')
         	  {
         		 
         		  $("#validerrorform").html('First Name is required!');
         		 hasError = true;
         	  }
    		  else if (email == '')
         	  {
         		 
         		  $("#validerrorform").html('Email is required!');
         		 hasError = true;
         	  }
    		  else if( !validateEmail(email)) {
        		  $("#validerrorform").html('Enter valid Email ID');
        		  hasError = true;
        	  }
    		  else if (password == '')
         	  {
         		 
         		  $("#validerrorform").html('Password is required!');
         		 hasError = true;
         	  }
    		  else if (password != confirmPassword)
         	  {
         		 
         		  $("#validerrorform").html('Password and Confirm Password are not matching!');
         		 hasError = true;
         	  }
    		 
         	  

    		  if(hasError == false) { 
    	       	  
 	       		 $("#validerrorform").html('');
 	       		$("#employeeform").submit();
 	       	  }
 	       	else
 	       	{
 	       		return false; 
 	       	}
    	  });*/
      });
      </script>