<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Settings
             </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Settings</a></li>
            <li class="active">Settings</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> Measurement Units Settings</h3>
              <div class="box-tools pull-right">
                <!--  <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
            <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
              <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
              </div>
            </div>
              <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
          <div class="col-lg-9 col-xs-12 ">
	        				   <!-- general form elements -->
              
                <!-- form start -->
               <form name="settingsForm" id="settingsForm"  action="<?php echo base_url();?>vendor/settings" method="POST" enctype="multipart/form-data">
       
                <input type="hidden" name="formsubmit" id="formsubmit" value=""/>
        	
        	<?php 
        	$metricunitsstr="";
        	foreach ($metricunits as $key=>$value){
        		if($metricunitsstr=="")
        			$metricunitsstr=$value;
        		else
        			$metricunitsstr=$metricunitsstr.",".$value;
        	}
        	
        	$britishunitsstr="";
        	foreach ($britishunits as $key=>$value){
        		if($britishunitsstr=="")
        			$britishunitsstr=$value;
        		else
        			$britishunitsstr=$britishunitsstr.",".$value;
        	}
        	?>
        	
                  <div class="box-body">
                    <div class="form-group">
                    
                      <label for="">Units</label>
                       <div class="row">
				          <div class="col-xs-6">
								Metric (<?php echo $metricunitsstr;?> ) <input type="radio" name="unitchk" value="Metric" <?php echo set_value('genderchk', $settingdetails['unittype']) == "Metric" ? "checked" : ""; ?> />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								British (<?php echo $britishunitsstr;?> ) <input type="radio" name="unitchk" value="British" <?php echo set_value('genderchk', $settingdetails['unittype']) == "British" ? "checked" : ""; ?> />
                     </div>
                     </div>
                     </div>
                  </div>
                  
                
                  
                  <div class="box-body">
                    <div class="form-group">
                      <label for="feed">Feeding Amount<font color="red">*</font></label>
                       <div class="row">
				          <div class="col-xs-6">
                      <select name="selectfeedunit"  id="selectfeedunit" class="form-control">
                       <option value="">Select</option>
                    <?php 
				         for($i=0;$i<count($feedunits);$i++){ ?>
				               <option value="<?php echo $feedunits[$i];?>" <?php echo set_select('selectfeedunit', $feedunits[$i],( $settingdetails['feedingunit'] ==  $feedunits[$i] ? TRUE : FALSE ))?> ><?php echo $feedunits[$i];?></option>
			                <?php }?> 
			                </select>
			                </div>
                    </div>
                     </div>
                  </div>
                  
				  
                    
                 <div class="form-group">
                 <input type="button" class="btn btn-warning " name="update" id="update" value="Update">
				                	
                  </div>
             </form>
              </div><!-- /.box -->
  			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
       
            </div><!-- /.box-body -->
            <div class=""></div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
 
<script type="text/javascript">

    $(window).load(function(){
      $("#validation").hide();
    });  


    $(document).ready(function() {

    	  $("#update").click(function() {
    		  var distunit = $("#selectdistanceunit").val();
        	  var wtunit = $("#selectwtunit").val();
        	  var feedunit = $("#selectfeedunit").val();
        	  if ( distunit == '')
        	  {
        		  $('#validation').addClass("alert alert-danger");
              $('#validation').show();
        		  $("#validerrorform").html('Select Distance Unit');
        	  }
        	  else if(wtunit == '')
        	  { 
              $('#validation').addClass("alert alert-danger");
              $('#validation').show();
        		  $("#validerrorform").html('Select Weight Unit');
        	  }
        	  else if(feedunit == '')
        	  {
              $('#validation').addClass("alert alert-danger");
              $('#validation').show();
        		  $("#validerrorform").html('Select Feeding amount Unit');
        	  }
        	  else
        	  {
        		  $("#validerrorform").html('');
       			 $("#formsubmit").val('yes');
       	
       			$("#settingsForm").submit();
        	  }
       	
    	 	 
         });
    });

   
  

</script>