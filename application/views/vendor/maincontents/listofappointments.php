<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Appointments

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Appointments</a></li>
            <li class="active">View Appointments</a></li>
        </ol>
    </section>
    <br><br>
    <!-- Main content -->


    <section class="content">
        <?php if($this->session->flashdata('error_message')){?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <?php echo $this->session->flashdata('error_message');?>
            </div>
        <?php }?>
        <?php if($this->session->flashdata('success_message')){?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>	<i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('success_message');?>
            </div>
        <?php }?>
        <div id="newapptnotification" style="display: none;">
            <span class="dismiss"><a title="dismiss this notification" class="close">&times;</a></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title">Unscheduled Appointments</h3></center>
                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <br>
                    <?php if(count($unscheduledappointments) > 0) {?>
                        <div class="box-body table-responsive" style="padding: 10px 15px;">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Customer name</th>
                                    <th>Pet name</th>
                                    <th>Service</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  foreach($unscheduledappointments as $rowOfUns) {
                                    if($rowOfUns['appuserid'] != ''){
                                        $firstName = $rowOfUns['appuserFirstName'];
                                        $lastName  = $rowOfUns['appuserLastName'];
                                        $email = $rowOfUns['appUserEmail'];
                                        $phone = $rowOfUns['appUserPhone'];
                                    }else{
                                        $firstName = 'NA';
                                        $lastName  = 'NA';
                                        $email = 'NA';
                                        $phone = 'NA';
                                    }
                                    ?>
                                    <tr id="<?php echo $rowOfUns['appointmentid'];?>">
                                        <!--td><?php echo $rowOfUns['appointmentid'];?></td-->
                                        <td><?php echo $firstName." ".$lastName;?></td>
                                        <td><?php echo $rowOfUns['petname'];?></td>
                                        <td><?php echo $rowOfUns['serviceName'];?></td>
                                        <td><?php echo date('D, M j, Y',strtotime($rowOfUns['boardingfromdate'])).' '.$rowOfUns['boardingfromtime'];?> </td>
                                        <td><?php echo date('D, M j, Y',strtotime($rowOfUns['boardingtodate'])).' '.$rowOfUns['boardingtotime'];?> </td>
                                        <td><?php echo $email;?></td>
                                        <td><?php echo $phone;?></td>
                                        <td><center><a class="btn bg-orange" href="<?php echo base_url().'vendor/appointments/declineAppointment/'.$rowOfUns['appointmentid'];?>"> Decline</a></center></td>
                                        <td><center><a class="btn bg-orange" href="<?php echo base_url().'vendor/appointments/viewDetails/'.$rowOfUns['appointmentid'];?>"> Schedule</a></center></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    <?php }else{?>
                        <div align="center"><h1>No data available !</h1></div><br /><br />
                    <?php }?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <center><h3 class="box-title">Appointments</h3></center>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="<?php echo base_url();?>vendor/appointments/addappointment">Add Appointment</a>

                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <br>
                        <?php if(count($appointments) > 0) {?>
                        <div class="box-body table-responsive"  style="padding: 10px 15px;">
                            <table id="appointmenttable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <!--th style="width: 10px">#</th-->
                                    <th>Start Time of appointment</th>
                                    <th>End Time of appointment</th>
                                    <th>Service provided</th>
                                    <th>Customer Name</th>
                                    <th>Pet Name</th>
                                    <th>Pet Breed</th>
                                    <th>Details</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php  foreach($appointments as $row) {
                                    $scheduletime=date('D, M j, Y',strtotime($row['boardingfromdate']));
                                    if($row['boardingfromtime']!=""){
                                        $boardingfromtime=explode(':',$row['boardingfromtime']);
                                        if($boardingfromtime[0]>12){
                                            $boardingfromtime[0]=$boardingfromtime[0]-12;
                                            $row['boardingfromtime']=$boardingfromtime[0].':'.$boardingfromtime[1].' '."PM";
                                        }
                                        else if($boardingfromtime[0] != 12)
                                        {
                                            $row['boardingfromtime']=$boardingfromtime[0].':'.$boardingfromtime[1].' '."AM";
                                        }
                                        else
                                        {
                                            $row['boardingfromtime']=$boardingfromtime[0].':'.$boardingfromtime[1].' '."PM";
                                        }
                                        $scheduletime=$scheduletime." ".$row['boardingfromtime'];
                                    }
                                    if($row['boardingtodate']!="" && $row['boardingtodate'] !="0000-00-00"){
                                        $scheduletime=$scheduletime."  to  ".date('D, M j, Y',strtotime($row['boardingtodate']));
                                    }
                                    else
                                    {
                                        $scheduletime=$scheduletime." to ".$row['boardingtodate'];
                                    }
                                    if($row['boardingtotime']!=""){
                                        $boardingtotime=explode(':', $row['boardingtotime']);
                                        if($boardingtotime[0]>12){
                                            $boardingtotime[0]=$boardingtotime[0]-12;
                                            $row['boardingtotime']=$boardingtotime[0].':'.$boardingtotime[1].' '."PM";
                                        }
                                        else if($boardingtotime[0] != 12)
                                        {
                                            $row['boardingtotime']=$boardingtotime[0].':'.$boardingtotime[1].' '."AM";
                                        }
                                        else if($boardingfromtime[0] >12)
                                        {
                                            $row['boardingtotime']=$boardingtotime[0].':'.$boardingtotime[1].' '."AM";
                                        }
                                        else
                                        {
                                            $row['boardingtotime']=$boardingtotime[0].':'.$boardingtotime[1].' '."PM";
                                        }

                                        $scheduletime=$scheduletime." ".$row['boardingtotime'];
                                    }
                                    ?>

                                    <tr id="<?php echo $row['appointmentid'];?>">
                                        <!--td><?php echo $row['appointmentid'];?></td-->
                                        <td><?php echo date('D, M j, Y',strtotime($row['boardingfromdate'])).' '.$row['boardingfromtime'];?> </td>
                                        <td><?php 
                                        if($row['boardingtodate'] == 0):
                                        echo 'Not Specified';
                                        else:
                                            echo date('D, M j, Y',strtotime($row['boardingtodate'])).' '.$row['boardingtotime'];
                                        endif;                                        
                                        ?> </td>
                                        <td><?php echo $row['servicename'];?></td>
                                        <td><?php echo $row['firstname']." ".$row['lastname'];?></td>
                                        <td><?php echo $row['petname'];?></td>
                                        <td><?php echo $row['breedname'];?></td>
                                        <td><center><a href="<?php echo base_url().'vendor/appointments/viewDetails/'.$row['appointmentid'];?>"><i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i> </a></center>  </td>
                                        <td><center><?php echo anchor('vendor/appointments/deleteAppointment/'. $row['appointmentid'],'<i class="fa fa-trash fa-lg"></i>','title="Click to delete" class="cnf-del-custom"');?>
                                            </center>
                                        </td>
                                    </tr>
                                <?php }?>

                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <!--  <ul class="pagination pagination-sm no-margin pull-right">
                    <?php //echo $this->pagination->create_links();?>
                  </ul> -->
                            <?php }else {?>
                                <div align="center"><h1>No data available !</h1></div><br /><br />
                            <?php } ?>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->




<!-- ----------------------------------------------- -->



<script type="text/javascript">

    $(document).ready(function(){
        $('#appointmenttable').DataTable({ bFilter: false,
            bInfo: false,
            "aoColumnDefs" : [{"type" : "date", 'target' : 0, 'bSortable': false, 'aTargets': [6,7] }]
        });

        //check for new appointments
        var url = "<?php echo base_url();?>vendor/appointments/newAppointments";

        $.post(url, function(data)
        {
            if(data!="nodata")
            {
                $("#newapptnotification").fadeIn("slow").append('A user has scheduled an appointment with you through the BabelBark App! The appointment has been added to your calendar.');
                $("#newapptnotification").append("<br>"+data);
            }

        });

        $(".dismiss").click(function(){

            //update status as read
            var url1 = "<?php echo base_url();?>vendor/appointments/updateAppointmentStatusToRead";

            $.post(url1, function(data)
            {
                if(data==1)
                {

                }
                $("#newapptnotification").fadeOut("slow");
            });
        });


    });
    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email );
    }
    function inviteUser()
    {

        var email = $("#inviteEmail").val();

        if (email == '')
        {

            $("#validerror").html('Enter Email ID');
        }
        else if( !validateEmail(email)) {
            $("#validerror").html('Enter valid Email ID');
        }
        else
        {
            $("#validerror").html('');
            var url = "<?php echo base_url();?>vendor/customer/inviteUser";

            $.post(url, {email : email} , function(data)
            {
                if(data == '1')
                {
                    $("#inviteEmail").val('');
                    alert("Invitation mail sent successfully to "+email);
                }
                else
                {

                }
            });
        }
    }
</script>
