  <?php $admin=$this->session->userdata('admin');
 $userrole=$admin['role'];
 ?>

     <div id="spinner_single" 
      style="position:fixed;top:0px;right:0px;width:100%;height:100%; background-color:rgba(189, 206, 204, 0.498039);background-image:url('<?php echo base_url();?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center;z-index:10000000; ">
    </div>
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Generic Customers
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Generic Customers</a></li>
            <li class="active">Generic Customer Listing</a></li>
          </ol>
        </section>
<br><br>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12"> 
              <div class="box">
                <div class="box-header with-border">
                  <center><h3 class="box-title" style=" margin-top:10px;">Generic Customer List</h3></center>
                  <div class="box-tools" style="margin-top:3px;" > 
                  <div class="col-sm-4">
                        <a class="btn bg-orange" href="<?php echo base_url();?>admin/home/downloadGenericcustomers">Download</a>
                  </div>
                </div>
                </div><!-- /.box-header -->
                <br>
                <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
            <?php if(count($customers) > 0) {?>
                <div class="box-body table-responsive" style="padding: 10px 15px;">
                  <table id="example3" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <!--th style="width: 10px">#</th-->
                      <th><center>Customer name</center></th>
                      <th><center>Customer Contact Information</center></th>
                      <th><center>Pet Name</center></th>
                      <th><center>Pet Birthday / Adoption Date</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    	//$count = 1;
                    	foreach($customers as $row) { ?>
                    <tr id="<?php echo $row['customerid'];?>">
                    
                    	<td><?php echo $row['firstname'].' '.$row['lastname'];?></td>
                      <?php 
                      if($row['phoneno'] == 0)
                        $row['phoneno'] = "";
                      ?>
                    	<td>Email Address: <?php echo $row['email'];?><br>Phone No: <?php echo $row['phoneno'];?><br>Address: <?php echo $row['address'];?><br> <?php echo $row['city'];?>,<?php echo $row['state'];?></td>
                    	<td><?php echo $row['petname'];?></td>
                      <td><?php echo $row['petDB'];?></td>
                    </tr>
                    
                    <?php 
                    //$count++;
                    }?>
                  
                  </tbody>  
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <?php foreach ($links as $link) {
                           echo "<li>".$link."</li>";
                    }?>
                  </ul>
                  <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                  <?php } ?>
                </div>
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            
          </div><!-- /.row -->
          
   
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
   <!-- ----------------------------------------------- -->   
      
      
<!-- ----------------------------------------------- -->
      
      
      
      <script type="text/javascript">
      $(window).load(function() {
      $("#spinner_single").fadeOut("slow");
})
      
      $(document).ready(function(){
        $('#example3').DataTable({

          bFilter: false, 
          bInfo: false,
          "paging": false,
          "lengthchange": false,
          "searching": true, 
          "info":false,
          "autoWidth": true,
          "iDisplayLenght": 0,
          "aoColumnDefs" : [
                            {
                              'bSortable' : false,
                              'aTargets' : [1,3]
                            }]

            });
        });
      </script>
