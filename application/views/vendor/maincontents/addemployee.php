<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];

if ($userrole == "employee" && $admin['adminDetails']['userid'] == $employeedetail['userid']) {
    $employee_update = 1;
} else {
    $employee_update = 0;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php
            if ($employeedetail['userid'] == 0)
                echo "Add Employee";
            else
                echo "Employee Details";
            ?>
        </h1>
        <?php if ($employeedetail['userid'] == 0) { ?>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Employee</a></li>
                <li class="active">Add Employee</a></li>
            </ol>
        <?php } ?>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box-body">
            <?php if ($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <div id="validation">
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <label id="validerrorform"></label>
            </div>
        </div>
        <?php //echo '<pre>'; print_r($row); echo '</pre>';   ?>
        <style>
            .collapsed-box {
                border:2px solid #F9CC85;
            }
        </style>
        <form name="employeeform" id="employeeform" action="<?php echo base_url(); ?>vendor/employee/updateEmployee" method="POST"  enctype="multipart/form-data">
            <input type="hidden" name="userid" id="userid" value="<?php echo $employeedetail['userid']; ?>"/>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning" >
                        <div class="box-body">
                            <div class="form-group">
                                <label>First Name<font color="red">*</font></label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control allowcharacterwithouthtmltags" placeholder="First Name" id="firstname" name="firstname" value="<?php echo set_value('firstname', $employeedetail['firstname']); ?>" maxlength="45"/>
                                    </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control allowcharacterwithouthtmltags" placeholder="Last Name" id="lastname" name="lastname" value="<?php echo set_value('lastname', $employeedetail['lastname']); ?>" maxlength="45"/>
                                    </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="email">Email<font color="red">*</font></label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="email" class="form-control" id="email" placeholder="Email" name='email' onkeyup="checkUniqueEmail()" value="<?php echo $employeedetail['email']; ?>">
                                        <span id="unirueemailreq"></span>
                                    </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="email">Phone</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control allownumericwithoutdecimal" id="phone" placeholder="Phone Number" name='phone' value="<?php echo $employeedetail['phone']; ?>">
                                    </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="newPassword"> Password<font color="red">*</font></label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="password" class="form-control" id="password" placeholder="New Password" name='password' value="<?php echo $employeedetail['password']; ?>" maxlength="10">
                                    </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="confirmPassword">Confirm Password<font color="red">*</font></label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" name='confirmPassword' value="<?php echo $employeedetail['password']; ?>"  maxlength="10">
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div>

                <div class="col-lg-12 col-xs-12">
                    <div class="box-footer">
                        <?php
                        
//                                    print_r($_SESSION); exit;
                        if ($userrole == "admin" || $employee_update) {
                            if ($employeedetail['userid'] == 0) {
                                ?>
                                <input type="button" class="btn btn-warning" name="update" id="update" value="Save">
                            <?php } else { ?>
                                <input type="button" class="btn btn-warning" name="update" id="update" value="Update">

                                <?php
                            }
                        }
                        echo anchor('vendor/employee/listofemployees', 'Back', 'class="btn  btn-primary"');
                        ?>
                    </div>
                </div>	
            </div><!-- /.col -->
        </form>
    </section>
</div> 

<script type="text/javascript">

    $(window).load(function () {
        $("#validation").hide();
    });

    $(document).ready(function () {
        var userrole = "<?php echo $userrole; ?>";
        var employee_update = <?php echo $employee_update; ?>;
        if (userrole == "employee" && !employee_update)
            $(".box-body :input").attr("disabled", "disabled");

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(email);
        }

        $(".allowcharacterwithouthtmltags").on("keyup blur", function (e) {
            $(this).val($(this).val().replace(/(<([^>]+)>)/ig, ""));
            /*if((e.which > 64 && e.which < 91 ) || (e.which > 96 && e.which < 123)){
             event.preventDefault();
             }*/
        });

        $(".allownumericwithoutdecimal").on("keypress keyup blur", function (e) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                event.preventDefault();
            }
        });

        $("#update").click(function () {
            var firstname = $("#firstname").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var confirmPassword = $("#confirmPassword").val();
            var errormsg = "";
            var hasError = false;
            if (firstname.trim().length == 0)
            {
                errormsg = 'The First Name field is required!';
                $("#firstname").css('border-color', '#dd4b39');
                hasError = true;
            } else if (email.trim().length == 0)
            {
                errormsg = 'The Email field is required!';
                $("#email").css('border-color', '#dd4b39');
                hasError = true;
            } else if (!validateEmail(email)) {
                errormsg = 'The Email field must contain a valid email address';
                $("#email").css('border-color', '#dd4b39');
                hasError = true;
            } else if (password.trim().length == 0)
            {
                errormsg = 'The Password field is required!';
                $("#password").css('border-color', '#dd4b39');
                hasError = true;
            } else if (password.trim().length < 6)
            {
                errormsg = 'The Password field must be at least 6 characters in length!';
                $("#password").css('border-color', '#dd4b39');
                hasError = true;
            } else if (confirmPassword.trim().length == 0)
            {
                errormsg = 'The Confirm Password field is required!';
                $("#confirmPassword").css('border-color', '#dd4b39');
                hasError = true;
            } else if (password != confirmPassword)
            {
                errormsg = 'The Confirm Password field does not match the Password field!';
                $("#password").css('border-color', '#dd4b39');
                $("#confirmPassword").css('border-color', '#dd4b39');
                hasError = true;
            }

            if (firstname.trim().length != 0)
            {
                $("#firstname").css('border-color', '#d2d6de');
            }
            if (email.trim().length != 0)
            {
                $("#email").css('border-color', '#d2d6de');
            }
            if (password.trim().length != 0)
            {
                $("#password").css('border-color', '#d2d6de');
            }
            if (hasError) {
                $('#validation').addClass("alert alert-danger");
                $('#validation').show();
            }

            if (hasError == false) {

                $("#validerrorform").html('');
                $("#employeeform").submit();
            } else
            {
                $("#validerrorform").html(errormsg);
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            }
        });
    });
    
    function checkUniqueEmail() {
        var email = $("#email").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>vendor/employee/checkUniqueEmail",
            data: 'email=' + email,
            success: function (data) {
                if (data == 0) {
                    $("#unirueemailreq").text("");
                    $("#email").css('border-color', '#d2d6de');
                    document.getElementById('employeeform').onsubmit = function () {
                        return true;
                    }
                }
                if (data == 1) {
                    $("#unirueemailreq").text("");
                    $("#unirueemailreq").text("This email is already registered with us, please try again.");
                    $("#unirueemailreq").css('color', '#dd4b39');
                    $("#email").css('border-color', '#dd4b39');
                    document.getElementById('employeeform').onsubmit = function () {
                        return false;
                    }
                }
            }
        });
    }
</script>