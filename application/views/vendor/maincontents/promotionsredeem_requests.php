<?php $admin=$this->session->userdata('admin');
					$userrole=$admin['role'];
?>
 
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Redemption Listing 
          
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Promotions</a></li>
            <li class="active">Redemption Listing</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
          <div class="box-body">
        <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
            <label class="text-red" id="validerrorform"><?php echo validation_errors(); ?></label>
        </div>
          <div class="row">
             <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Redeem Requests</h3>
               </div>
               
                <?php if(count($redeemrequests) > 0) {?>
                <div class="box-body table-responsive">
                  <table id="example3" class="table table-bordered table-striped">
                    <tr>
                      <th style="width: 10px">#</th>
                    
                      <th>Promotion name</th>
                      <th>User</th>
                      <th>&nbsp;</th>
                   </tr>
                    <?php  foreach($redeemrequests as $row) { ?>
                    <tr>
                    <td><?php echo $row['promotionid'];?></td>
                    <td><?php echo $row['promoname'];?></td>
                     <td><?php echo $row['username'];?></td>
                       <td><a href="<?php echo base_url().'vendor/promotions/redeemPromotion/'.$row['id'].'/'. $row['promotionid'];?>">Redeem </a></center></td>
                    </tr>
                    <?php }?>
                   </table>
                   </div>  
                    <?php }else {?>
                          	<div align="center"><h1>No data available !</h1></div><br /><br />
                          <?php } ?>
                  </div>
                  </div>
                 
          
          </div>
          
          </section>
          </div>
         
