<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Schedule Appointment
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Schedule Appointment</a></li>
            <li class="active">Schedule Appointment</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
        <div class="box-body">
        <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
        </div>
        <?php //echo '<pre>'; print_r($row); echo '</pre>';?>
        
        <style>
        	.collapsed-box {
        		border:2px solid #F9CC85;
        	}
        </style>
        <div class="row">
            <div class="col-md-12">
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">  
              	<div class="box-header with-border">
                  <h3 class="box-title">Overnight Boarding</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" ><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
               </a> 
                <div class="box-body">
                  <div class="row">
				     <div class="col-xs-6">
				         <label>From</label>
				         <input type="date" class="form-control">
				     </div>
				     <div class="col-xs-6">
				         <label>To</label>
				         <input type="date" class="form-control" >
				     </div>
				   </div>
				   
				   <div class="row">
				     <div class="col-xs-6">
				         <label>Checkin Time</label>
				         <input type="time" class="form-control">
				     </div>
				     <div class="col-xs-6">
				         <label>CheckOut Time</label>
				         <input type="time" class="form-control" >
				     </div>
				   </div>
				   
                </div><!-- /.box-body -->
                
                <div class="box-footer">
	                <button type="submit" class="btn btn-warning">Skip</button>
	                <button type="submit" class="btn btn-warning">Next</button>
	            </div>
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Service By Appointment</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  <div class="row">
				     <div class="col-xs-6">
				         <label>Date</label>
				         <input type="date" class="form-control" placeholder=".col-xs-3">
				     </div>
				     <div class="col-xs-6">
				         <label>Select Sevice</label>
				         <select class="form-control">
			               <option>0</option>
			               <option>1</option>
			             </select>      
				     </div>
				     
				   </div>
				   
				   <div class="row">
				     <div class="col-xs-6">
				         <label>From Time</label>
				         <input type="time" class="form-control" placeholder="">
				     </div>
				     <div class="col-xs-6">
				         <label>To Time</label>
				         <input type="time" class="form-control" placeholder="">
				     </div>
				     
				   </div>
				   
                </div><!-- /.box-body -->
                
                <div class="box-footer">
	                <button type="submit" class="btn btn-warning">Skip</button>
	                <button type="submit" class="btn btn-warning">Next</button>
	            </div>
	            
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Customer</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  <div class="row">
				     <div class="col-xs-6">
				         <label>Select Customer</label>
				         <select class="form-control">
			               <option>0</option>
			               <option>1</option>
			             </select>
				     </div>
				     <div class="col-xs-6"></div>
				  </div>
                </div><!-- /.box-body -->
                
                <div class="box-footer">
	                <button type="submit" class="btn btn-warning">Add New</button>
	                <button type="submit" class="btn btn-warning">Next</button>
	            </div>
	            
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Pets</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  <div class="row">
				     <div class="col-xs-6">
				         <label>Select Pet</label>
				         <select class="form-control">
			               <option>0</option>
			               <option>1</option>
			             </select>
				     </div>
				     <div class="col-xs-6"></div>
				  </div>
				  <br>
				  <div class="callout callout-warning">
					  <div class="row">
					     <div class="col-xs-5">
					         <label style="color: #dd8500;">Select a Run</label>
					           <select class="form-control">
				                 <option>0</option>
				                 <option>1</option>
				               </select>
					     </div>
					     <div class="col-xs-5">
					         <label style="color: #dd8500;">Select an appointment</label>
					         <select class="form-control">
				                 <option>0</option>
				                 <option>1</option>
				               </select>
					     </div>
					     <div class="col-xs-2">
					     	  <label style="color: transparent;">Delete</label><br>
					     	  <a><i class="fa fa-trash fa-2x" style="color: #dd8500;"></i></a>
					     	</div>
					   </div>  
				   </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
	                <button type="submit" class="btn btn-warning">Add New</button>
	                <button type="submit" class="btn btn-warning">Next</button>
	            </div>
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Vets</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  <div class="row">
				     <div class="col-xs-6">
				         <label>Select Vet</label>
				         <select class="form-control">
			               <option>0</option>
			               <option>1</option>
			             </select>
				     </div>
				     <div class="col-xs-6"></div>
				  </div>
                </div><!-- /.box-body -->
                
                <div class="box-footer">
	                <button type="submit" class="btn btn-warning">Add New</button>
	                <button type="submit" class="btn btn-warning">Next</button>
	            </div>
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Cost</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  
					<div class="btn-group-vertical">
                     <button type="button" class="btn btn-warning">Nights</button>
                     <button type="button" class="btn btn-warning">Pets</button>
                     <button type="button" class="btn btn-warning">Other charges</button>
                     <button type="button" class="btn btn-warning">Tax</button>
                     <button type="button" class="btn btn-warning">Deposit</button>
                     <button type="button" class="btn btn-warning">Invoice ID</button>
                   </div>
                   
                </div><!-- /.box-body -->
              </div>
              
              <div class="box box-warning collapsed-box" >
              <a data-widget="collapse" href="">
                <div class="box-header with-border">
                  <h3 class="box-title">Confirm</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                </a>
                <div class="box-body">
                  <button class="btn bg-orange btn-flat margin">Save Booking</button>
                </div><!-- /.box-body -->
              </div>
              
            </div><!-- /.col -->
            
          </div> 
			
        </section><!-- /.content -->
        
         </div><!-- /.content-wrapper -->