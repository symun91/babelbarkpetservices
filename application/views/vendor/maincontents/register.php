<!DOCTYPE html>
<html>
<?php
if(ALLOW_VET == 1){
    $logo = 'logo_vet.png';
}else{
    $logo = 'logo_biz.png';
}
?>
<head> <?php echo $head;?></head>
<body class="layout-boxed">
<div class="register-box">
    <div class="register-box-body">
        <div class="register-logo">
            <!--        <a href="--><?php //echo base_url();?><!--admin/home">-->
            <!--            <img src="--><?php //echo base_url().'assets/images/'.$logo ;?><!--" width="250"/>-->
            <!--            <br />Create new account-->
            <!--        </a>-->

            <div>
                <div class="row">
                    <div class="col-lg-6 col-xs-6"  style="text-align: center">
                        <a href="<?php echo BABELVET_ADD;?>/vendor/login">
                            <img src="<?php echo base_url();?>assets/images/babelvet_login.png" width="100"/>
                            <br />
                        </a>
                    </div>
                    <div class="col-lg-6 col-xs-6" style="text-align: center">
                        <a href="<?php echo BIZBARK_ADD;?>/vendor/login">
                            <img src="<?php echo base_url();?>assets/images/BizBark_Login.png" width="92"/>
                            <br />
                        </a>
                    </div>
                    <br />Create new account
                </div>
            </div>
        </div>
        <form action="<?php echo base_url();?>vendor/administrator/register" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" maxlength="25" onkeyup="charcount()" value="<?php echo set_value('firstname')?>">
                <div id="firstnamereq"></div>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <?php echo form_error('firstname');?>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" maxlength="25" onkeyup="charcount()" value="<?php echo set_value('lastname')?>">
                <div id="lastnamereq"></div>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <?php echo form_error('lastname');?>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="comapnyname" id="comapnyname" class="form-control" placeholder="Company Name" maxlength="100" onkeyup="charcount()" value="<?php echo set_value('comapnyname')?>">
                <div id="comapnynamereq"></div>
                <span class="glyphicon glyphicon-globe form-control-feedback"></span>
                <?php echo form_error('comapnyname');?>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo set_value('email')?>">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <?php echo form_error('email');?>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <?php echo form_error('password');?>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="confirm_password" class="form-control" placeholder="Confirm password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                <?php echo form_error('confirm_password');?>
            </div>
            <div class="form-group has-feedback">
                <label> Category</label>
                <select name="selecttype[]" id="selecttype" class="form-control" multiple="multiple">
                    <?php
                    for($i=0;$i<count($servicetypes);$i++){
                        ?>
                        <option value="<?php echo $servicetypes[$i]['categoryid'];?>"<?php echo set_select('selecttype',$servicetypes[$i]['categoryid'])?>><?php echo $servicetypes[$i]['title'];?></option>
                    <?php }?>

                </select>
                <?php echo form_error('selecttype');?>
            </div>

            <div class="form-group">
                <label for="">Address</label>

                <select class="form-control" name="country" id="country">
                    <option value="">Select Country</option>
                    <option value="Canada" <?php echo set_select('country','Canada'); ?>>Canada</option>
                    <option value="United States" <?php echo set_select('country','United States'); ?>>United States</option>

                </select>
                <?php echo form_error('country');?>
            </div>

            <div class="form-group">

                <input id="address" class="form-control" type="text" placeholder="Address" name="address" maxlength="45" value="<?php echo set_value('address'); ?>">
            </div>
            <?php echo form_error('address');?>

            <div class="form-group">
                <input id="city" class="form-control" type="text" placeholder="City" name="city" maxlength="45" value="<?php echo set_value('city'); ?>">
            </div>
            <?php echo form_error('city');?>

            <div class="form-group">
                <select class="form-control" name="state" id="state">
                    <option value="">Select State</option>
                </select>
            </div>
            <?php echo form_error('state');?>
            <div class="form-group">
                <input id="zipcode" class="form-control" type="text" placeholder="Zipcode" name="zipcode" maxlength="15" value="<?php echo set_value('zipcode'); ?>">

            </div>
            <?php echo form_error('zipcode');?>
            <div class="form-group">
                <div id="map_canvas" style="border:0px solid #fff452;width:320px;height:454px;">
                    <!-- map loaded here -->
                </div>
            </div>

            <div class="form-group">
                <label>How did you hear about us?</label>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="hearaboutuscheckbox" id="hearaboutuscheckbox">

                            <input type="checkbox"  class="parentcheckbox" name="parentcheckbox[]" value="Internet Search">
                            <label>Internet Search</label><br/>

                            <input type="checkbox"  class="parentcheckbox" value="1">
                            <label>Social</label>
                            <div class="hearaboutuschildbox1" id="hearaboutuschildbox1" style=" float: right;">
                                <input type="checkbox" name="parentcheckbox[]" value="Social:Facebook" ><label>Facebook</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Social:Twiter" ><label>Twiter</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Social:Google" ><label>Google</label></br>
                            </div>
                            </br>


                            <input type="checkbox" class="parentcheckbox" value="2">
                            <label>Friend / Colleague</label>
                            <div class="hearaboutuschildbox2" id="hearaboutuschildbox2" style="float: right;">
                                <label>Name of Friend/Colleague: </label><input type="text" name="parentcheckbox[]" value="">
                            </div>
                            </br>


                            <input type="checkbox"  class="parentcheckbox" value="3">
                            <label>Event</label>
                            <div class="hearaboutuschildbox3" id="hearaboutuschildbox3" style="float: right;">
                                <input type="checkbox" name="parentcheckbox[]" value="Event:Super Zoo 2016" ><label>Super Zoo 2016</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Event:Groom Zoo" ><label>Groom Zoo</label></br>
                            </div>
                            </br>

                            <input type="checkbox"  class="parentcheckbox" value="4">
                            <label>Email</label>
                            <div class="hearaboutuschildbox4" id="hearaboutuschildbox4" style="float: right;">
                                <input type="checkbox" name="parentcheckbox[]" value="Email:Groom Expo Email" ><label>Groom Expo Email</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Email:Pet Product News Email" ><label>Pet Product News Email</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Email:Pet Age Email" ><label>Pet Age Email</label></br>
                            </div>
                            </br>

                            <input type="checkbox"  class="parentcheckbox" value="5">
                            <label>Magazine</label>
                            <div class="hearaboutuschildbox5" id="hearaboutuschildbox5" style="float: right;">
                                <input type="checkbox" name="parentcheckbox[]" value="Magazine:Pet Age Magazine" ><label>Pet Age Magazine</label></br>
                                <input type="checkbox" name="parentcheckbox[]" value="Magazine:Groomer to Groomer Magazine" ><label>Groomer to Groomer Magazine</label></br>
                            </div>
                            </br>

                            <input type="checkbox"  class="parentcheckbox" value="6">
                            <label>Mail</label>
                            <div class="hearaboutuschildbox6" id="hearaboutuschildbox6" style="float: right;">
                                <input type="checkbox" name="parentcheckbox[]" value="Mail:Postcard"><label>Postcard</label></br>
                            </div>
                            </br>

                            <input type="checkbox"  class="parentcheckbox" value="7">
                            <label>Other</label>
                            <div class="hearaboutuschildbox7" id="hearaboutuschildbox7" style="float: right;">
                                <label>Write:</label><input type="text" name="parentcheckbox[]" value="">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox iCheck">

                                    <input type="checkbox" name="agreedterms" value="1" <?php echo set_checkbox('agreedterms', '1'); ?>> &nbsp;&nbsp;I agree to the
                                    <a href="<?php echo base_url();?>resources/BabelBarkUsersTermsofService_2.html" target="blank">Terms and Conditions</a>
                                    <?php echo form_error('agreedterms');?>

                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox iCheck">

                                    <input type="checkbox" name="agreement" value="1" <?php echo set_checkbox('agreement', '1'); ?>>&nbsp;&nbsp;I agree to the
                                    <!-- <a href="javascript:;" id="agreementlink">Vendor Affiliate Agreement</a> -->
                                    <a href="<?php echo base_url();?>resources/AffiliateAgreement2.html" target="blank">Vendor Affiliate Agreement</a>


                                    <?php echo form_error('agreement');?>
                                </div>
                            </div>

                        </div>

                        <input type="hidden" id="lat" name="lat">
                        <input type="hidden" id="lng" name="lng">


                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <?php echo form_submit(array('class'=>"btn btn-primary btn-flat",'value'=>'Register','name'=>'submit'));?>

                            </div>

                            <div class="col-xs-8" style="margin-top: 10px;">
                                <a href="<?php echo base_url();?>vendor/administrator/login" class="text-center">I already have a membership</a>

                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
        </form>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->




<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script type="text/javascript">
        function charcount(){
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var comapnyname = $("#comapnyname").val();
        if (firstname.trim().length >= 25){
            $("#firstnamereq").text('The First Name field can be at most 25 characters in length!');
            $("#firstnamereq").css('color', '#dd4b39');
            $("#firstname").css('border-color', '#dd4b39');
        } else if (lastname.trim().length >= 25){
            $("#lastnamereq").text('The Last Name field can be at most 25 characters in length!');
            $("#lastnamereq").css('color','#dd4b39');
            $("#lastname").css('border-color', '#dd4b39');
        } else if (comapnyname.trim().length >= 100){
            $("#comapnynamereq").text('The Company Name field can be at most 100 characters in length!');
            $("#comapnynamereq").css('color', '#dd4b39');
            $("#comapnyname").css('border-color', '#dd4b39');
        }
        if (firstname.trim().length < 25){
            $("#firstnamereq").text('');
            $("#firstname").css('border-color', '#d2d6de');
        }        
        if (lastname.trim().length < 25){
            $("#lastnamereq").text('');
            $("#lastname").css('border-color', '#d2d6de');
        }
        if (comapnyname.trim().length < 100){
            $("#comapnynamereq").text('');
            $("#comapnyname").css('border-color', '#d2d6de');
        }
    }
</script>
<script>

    $(document).ready(function() {


        $(".hearaboutuschildbox1").hide();
        $(".hearaboutuschildbox2").hide();
        $(".hearaboutuschildbox3").hide();
        $(".hearaboutuschildbox4").hide();
        $(".hearaboutuschildbox5").hide();
        $(".hearaboutuschildbox6").hide();
        $(".hearaboutuschildbox7").hide();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '5%' // optional
        });

        $('.parentcheckbox').on('ifChanged', function(event){
            //$(".icheckbox_square-blue").not(this).removeClass('checked');

            $(".hearaboutuschildbox1").hide();
            $(".hearaboutuschildbox2").hide();
            $(".hearaboutuschildbox3").hide();
            $(".hearaboutuschildbox4").hide();
            $(".hearaboutuschildbox5").hide();
            $(".hearaboutuschildbox6").hide();
            $(".hearaboutuschildbox7").hide();

            if(this.value == 1 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").show();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 2 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").show();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 3 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").show();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 4 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").show();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 5 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").show();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 6 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").show();
                $(".hearaboutuschildbox7").hide();
            }
            else if(this.value == 7 && $(this).prop("checked") == true)
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").show();
            }
            else
            {
                $(".hearaboutuschildbox1").hide();
                $(".hearaboutuschildbox2").hide();
                $(".hearaboutuschildbox3").hide();
                $(".hearaboutuschildbox4").hide();
                $(".hearaboutuschildbox5").hide();
                $(".hearaboutuschildbox6").hide();
                $(".hearaboutuschildbox7").hide();
            }
        });
<!--        --><?php //if(ALLOW_VET==1){?>
//        $('#selecttype option[value="1"]').prop('disabled', true);
//
//        $("#selecttype option").on('click',function() {
//            $('#selecttype option[value="1"]').prop('selected',true);
//        });
//        <?php //}?>

        $('#selecttype option').mousedown(function(e) {
            e.preventDefault();
            $(this).prop('selected', $(this).prop('selected') ? false : true);

            return false;
        });
        //populating state after form validation error
        var selState1before="";
        var validationerror="<?php echo $formvalidationerror;?>";
        if(validationerror=="yes")
        {
            var selcountrybefore="<?php echo $country;?>";
            selState1before="<?php echo $state;?>";
            if(selcountrybefore!="")
                populateStates(selcountrybefore);
        }

        $("#country").on('change', function() {
            $('#state option').remove();
            $("#state").append('<option value="">Select State</option>');
            var selCountry=this.value ;
            populateStates(selCountry);


        });


        function populateStates(selCountry)
        {


            var jqueryarray = "";
            if(selCountry=="Canada")
                jqueryarray = <?php echo json_encode($Canadastates); ?>;
            else
                jqueryarray = <?php echo json_encode($USstates); ?>;
            for (var i = 0; i < jqueryarray.length; i++) {
                $("#state").append('<option value="' + jqueryarray[i] + ' <?php echo set_select("state",'+jqueryarray[i] + '); ?>">' + jqueryarray[i] + '</option>');
            }

            if(selState1before!="")
                $("#state").val(selState1before);
        }


        $("a#agreementlink").click(nWin);
    });

    function nWin() {
        var w = window.open();
        $.get( "<?php echo base_url();?>/resources/AffiliateAgreement2.html", function( data ) {
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

            var updateddata = data.replace("%dateofagree%", currentDate);
            $(w.document.body).html(updateddata);

        });


    }
</script>



<div id="terms" class="modal fade" role="dialog" >
    <div class="modal-dialog termsdialoge">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <section class="content">
                <?php
                echo "<pre>";
                echo (file_get_contents("resources/BabelBarkUsersTermsofService_2.html"));

                ?>

            </section>
        </div>
    </div>
</div>



<div id="agreement" class="modal fade" role="dialog" >
    <div class="modal-dialog agreementdialoge">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <section class="content">
                <?php
                echo "<pre>";
                echo file_get_contents("resources/AffiliateAgreement2.html");


                ?>
            </section>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/map-api.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGygmI0eyoD-mZowFiEfb-xGNCrasI8ZI&callback=initMap" type="text/javascript"></script>


</body>
</html>
