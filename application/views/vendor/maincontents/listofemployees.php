<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
if ($userrole == "employee") {
    $employeeid = $admin['adminDetails']['userid'];
} else {
    $employeeid = $admin['adminDetails']['vendorid'];
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employees
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Employees</a></li>
            <li class="active">View Employees</a></li>
        </ol>
    </section>
    <br><br>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <br><br> <br><br><br><br> 
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title">Employees</h3></center>
                        <?php
                        if ($userrole == "admin") {
                            ?>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="<?php echo base_url(); ?>vendor/employee/addemployee">Add Employee</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div><!-- /.box-header -->
                    <br>
                    <?php if ($this->session->flashdata('error_message')) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            <?php echo $this->session->flashdata('error_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success_message')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>	<i class="icon fa fa-check"></i> Success</h4>
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if (count($employees) > 0) { ?>
                        <div class="box-body table-responsive" style="padding: 10px 15px;">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <!--th style="width: 10px">#</th-->
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th><center>Details</center></th>
                                <?php
                                if ($userrole == "admin") {
                                    ?>
                                    <th><center>Delete</center></th>
                                <?php } else { echo '<th></th>'; } ?>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($employees as $row) {
                                        if ($employeeid != $row['userid']) {
                                            ?>
                                            <tr id="<?php echo $row['userid']; ?>">
                                            <!--td><?php echo $row['userid']; ?></td-->
                                                <td><?php echo $row['firstname'] . " " . $row['lastname']; ?></td>
                                                <td><?php echo $row['email']; ?></td>

                                                <td><center><a href="<?php echo base_url() . 'vendor/employee/viewDetails/' . $row['userid']; ?>"><i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i> </a></center>  </td>

                                        <td><center><?php echo $userrole == "employee" ? '' : anchor('vendor/employee/deleteEmployee/' . $row['userid'], '<i class="fa fa-trash fa-lg"></i>', 'title="Click to delete" class="cnf-del-custom"'); ?></center> </td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr id="<?php echo $row['userid']; ?>">
                                       <!--td><?php echo $row['userid']; ?></td-->
                                            <td><?php echo $row['firstname'] . " " . $row['lastname']; ?></td>
                                            <td><?php echo $row['email']; ?></td>

                                            <td><center><a href="<?php echo base_url() . 'vendor/employee/viewDetails/' . $row['userid']; ?>"><i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i> </a></center>  </td>
                                        <?php
                                        if ($userrole == "admin") {
                                            ?>
                                            <td><center><?php echo anchor('#', '<i class="fa fa-trash fa-lg"></i>', 'title="You can not delete it.Because its your profile and you logged in." class="cnf-del-custom"'); ?></center></td>
                                        <?php } else { echo '<td></td>'; } ?>            
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>


                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">

                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                //foreach ($links as $link) {
                                //echo "<li>" . $link . "</li>";
                                //}
                                ?>
                            </ul>
                        <?php } else { ?>
                            <div align="center"><h1>No data available !</h1></div><br /><br />
                        <?php } ?>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->

        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#example3').DataTable({bFilter: false,
            bInfo: false,
            order: [],
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [2, 3]
                }]
        });
    });
</script>



<!-- ----------------------------------------------- -->



