<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin related action are performed here.</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Change Password</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Change Password</h3>
              <div class="box-tools pull-right">
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
            <?php if($this->session->flashdata('error_message')){?>
             <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error_message');?>
                  </div>
            <?php }?>
             <?php if($this->session->flashdata('success_message')){?>
              <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>	<i class="icon fa fa-check"></i> Success</h4>
                    <?php echo $this->session->flashdata('success_message');?>
                  </div>
            <?php }?>
              <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
               
                <!-- form start -->
                <form role="form" name='changePasswordForm' action="<?php echo base_url();?>admin/home/adminChangePassword" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="oldPassword">Old Password</label>
                      <input type="password" class="form-control" id="oldPassword" placeholder="Old Password" name='old_password'>
                      <?php echo form_error('old_password');?>
                    </div>
                    <div class="form-group">
                      <label for="newPassword">New Password</label>
                      <input type="password" class="form-control" id="newPassword" placeholder="New Password" name='new_password'>
                      <?php echo form_error('new_password');?>
                    </div>
                   <div class="form-group">
                      <label for="confirmPassword">Confirm Password</label>
                      <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" name='confirm_password'>
                      <?php echo form_error('confirm_password');?>
                    </div>

                  <div class="box-footer">
                    <input type="submit" class="btn btn-primary" name='submit' value="Submit">
                  </div>
                </form>
              </div><!-- /.box -->

          </div>   <!-- /.row -->
        </section><!-- /.content -->
        
            </div><!-- /.box-body -->
            <div class="box-footer"></div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->