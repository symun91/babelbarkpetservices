<?php
$GLOBALS = array(
    'vet_logo' => $vet_logo,
    'company' => $company
);

$pdf = new MyPdf('L','mm','A4');
$pdf->AliasNbPages();
//$pdf->AddPage();
//Disable automatic page break
$pdf->SetAutoPageBreak(false);
//Add first page
$pdf->AddPage();
$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);
$fontFamily = 'Arial';
$fontStyleBold = 'B';
$fontStyleNormal = '';
$fontSize = 8;
/************* VETERINARY INFORMATION ******************/
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(160,10,'VETERINARY INFORMATION',0,0,'R');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(110,10,'Report Generated : '.$generateDate,0,0,'R');
$pdf->Ln();

$w = array(55, 80, 55, 80);
$height = 7;
$fill = true;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Name", 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $vendorInfo['firstname'].' '.$vendorInfo['firstname'], 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Phone", 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $vendorInfo['contact'], 'LRT', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Address", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $vendorInfo['address'], 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Email", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $vendorInfo['email'], 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "City", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $vendorInfo['city'], 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "website", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $vendorInfo['website'], 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "State/Province", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $vendorInfo['state'], 'LR', 0, 'L', $fill);
$pdf->Cell($w[2], $height, "", 'LR', 0, 'L', $fill);
$pdf->Cell($w[3], $height, "", 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "ZIP/Postal Address", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $vendorInfo['zipcode'], 'LR', 0, 'L', $fill);
$pdf->Cell($w[2], $height, "", 'LR', 0, 'L', $fill);
$pdf->Cell($w[3], $height, "", 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

/************* CLIENT INFORMATION ******************/
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(270,10,'CLIENT INFORMATION',0,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',9);
$w = array(55, 80, 55, 80);
$height = 7;
$fill = true;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0],$height,"Name",'LRT',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1],$height,$clientInfo['firstname'].' '.$clientInfo['lastname'],'LRT',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2],$height,"Phone",'LRT',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3],$height,$clientInfo['phonenumber'],'LRT',0,'L',$fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0],$height,"Address",'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1],$height,$clientInfo['address'],'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2],$height,"Email",'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3],$height,$clientInfo['email'],'LR',0,'L',$fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0],$height,"City",'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1],$height,$clientInfo['city'],'LR',0,'L',$fill);
$pdf->Cell($w[2],$height,"",'LR',0,'L',$fill);
$pdf->Cell($w[3],$height,"",'LR',0,'L',$fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0],$height,"State/Province",'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1],$height,$clientInfo['state'],'LR',0,'L',$fill);
$pdf->Cell($w[2],$height,"",'LR',0,'L',$fill);
$pdf->Cell($w[3],$height,"",'LR',0,'L',$fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0],$height,"ZIP/Postal Address",'LR',0,'L',$fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1],$height,$clientInfo['zip'],'LR',0,'L',$fill);
$pdf->Cell($w[2],$height,"",'LR',0,'L',$fill);
$pdf->Cell($w[3],$height,"",'LR',0,'L',$fill);
$pdf->Ln();
$fill = !$fill;
// Closing line
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

/************* PATIENT INFORMATION ******************/
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(270,10,'PATIENT INFORMATION',0,0,'C');
$pdf->Ln();
// Color and font restoration
$pdf->SetFont('Arial','',9);
$w = array(55, 80, 55, 80);
$height = 7;
$fill = true;

if($patientInfo->gender =='M'){
    $gender = 'Male';
}elseif(($patientInfo->gender =='F')){
    $gender = 'Female';
}else{
    $gender = 'N/A';
}

$dateString=$patientInfo->birthdate;
$years = round((time()-strtotime($dateString))/(3600*24*365.25));
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Name", 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $patientInfo->name, 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Primary Breed", 'LRT', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $patientInfo->primarybreed, 'LRT', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Sex", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $gender, 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Secondary Breed", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $patientInfo->secondarybreed, 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Birthday", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $patientInfo->birthdate, 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Age", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $years, 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[0], $height, "Microchip ID", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[1], $height, $patientInfo->microchipid, 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$pdf->Cell($w[2], $height, "Weight", 'LR', 0, 'L', $fill);
$pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
$pdf->Cell($w[3], $height, $patientInfo->currentweight, 'LR', 0, 'L', $fill);
$pdf->Ln();
$fill = !$fill;

// Closing line
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

/************* Reminders for: Doog ******************/
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(270,10,"REMINDERS FOR: $patientInfo->name",0,0,'C');
$pdf->Ln();
// Color and font restoration
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$w = array(100,170);
$header = array('Due Date','Name');
$alignment = array(0,1);
for($i=0;$i<count($header);$i++)
    $pdf->Cell($w[$i],7,$header[$i],1,0,'C',$fill);
$pdf->Ln();
$fill = !$fill;
$page_height = 185; // mm (portrait letter)
$bottom_margin = 0; // mm
foreach ($vaccinationInfo as $vacRows) {
    $space_left=$page_height-($pdf->GetY()+$bottom_margin);
    if($height>$space_left){
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->AddPage();
        $pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
        for($i=0;$i<count($header);$i++)
            $pdf->Cell($w[$i],$height,$header[$i],1,$alignment[$i],'C',true);
    }
    $pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
    $pdf->Cell($w[0], $height, $vacRows['duedate'], 'LRT', 0, 'L', $fill);
    $pdf->Cell($w[1], $height, $vacRows['name'], 'LRT', 0, 'L', $fill);
    $pdf->Ln();
    $fill = !$fill;
}
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

$space_left=$page_height-($pdf->GetY()+$bottom_margin);
if($height>$space_left){
    $pdf->AddPage();
}
/************* Medications for: Doog ******************/
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(270,10,"MEDICATIONS FOR : $patientInfo->name",0,0,'C');
$pdf->Ln();
// Color and font restoration
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$w = array(100,170);
$header = array('Medication','Direction');
for($i=0;$i<count($header);$i++)
    $pdf->Cell($w[$i],$height,$header[$i],1,0,'C',$fill);
$pdf->Ln();
$fill = !$fill;
foreach ($medicationInfo as $medRows) {
    $space_left=$page_height-($pdf->GetY()+$bottom_margin);
    if($height>$space_left){
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->AddPage();
        $pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
        for($i=0;$i<count($header);$i++)
            $pdf->Cell($w[$i],$height,$header[$i],1,$alignment[$i],'C',true);
    }
    $pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
    $direction = $medRows['dosage'].' '.$medRows['dosageunit'].' '.$medRows['frequencytimes'].' times '.$medRows['frequency'];
    $pdf->Cell($w[0], $height, $medRows['name'], 'LRT', 0, 'L', $fill);
    $pdf->Cell($w[1], $height, $direction, 'LRT', 0, 'L', $fill);
    $pdf->Ln();
    $fill = !$fill;
}
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

$space_left=$page_height-($pdf->GetY()+$bottom_margin);
if($height>$space_left){
    $pdf->AddPage();
}
/************* Prescriptions for: Doog ******************/

$pdf->SetFont('Arial','BU',10);
$pdf->Cell(270,10,"PRESCRIPTIONS FOR : $patientInfo->name",0,0,'C');
$pdf->Ln();
// Color and font restoration
$pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
$w = array(100,170);
$header = array('Medication','Direction');
for($i=0;$i<count($header);$i++)
    $pdf->Cell($w[$i],$height,$header[$i],1,0,'C',$fill);
$pdf->Ln();

$fill = !$fill;
foreach ($sikkaMedicationInfo as $sikkaMedRows) {
    $space_left=$page_height-($pdf->GetY()+$bottom_margin);
    if($height>$space_left){
        $pdf->Cell(array_sum($w),0,'','T');
        $pdf->AddPage();
        $pdf->SetFont($fontFamily,$fontStyleBold,$fontSize);
        for($i=0;$i<count($header);$i++)
            $pdf->Cell($w[$i],$height,$header[$i],1,$alignment[$i],'C',true);
    }
    $pdf->SetFont($fontFamily,$fontStyleNormal,$fontSize);
    $pdf->Cell($w[0], $height, $sikkaMedRows['description'], 'LRT', 0, 'L', $fill);
    $pdf->Cell($w[1], $height, $sikkaMedRows['direction'], 'LRT', 0, 'L', $fill);
    $pdf->Ln();
    $fill = !$fill;
}
$pdf->Cell(array_sum($w),0,'','T');
$pdf->Ln();

$pdf->Output('F',$fileTmpPath);
