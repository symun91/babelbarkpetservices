
<?php
$admin = $this->session->userdata('admin');
$userrole = $admin['role'];
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-align-right fa-rotate-90"></i>Customers</a></li>
            <li class="active"><a>Customer Listing</a></li>
        </ol>
    </section>
    <br><br>
    <!-- Main content -->
    <section class="content">
        <div class="box-body">
            <div id="validation">

                <label id="inviteConfirmedMsg"></label>
            </div>
        </div>
        <div class="row">




            <div class=" col-xs-12 ">
                <div class="form-group">
                    <div class="col-sm-3">	<center><label for=""><h4>Invite Customer To BabelBark</h4></label></center></div>
                    <div class="col-sm-5">	<center><input id="inviteEmail" name="inviteEmail" class="form-control" type="text" placeholder="Customer's email address" value="" maxlength="200"></center>
                        <span id="validerror1" class="text-red"></span>
                    </div>
                    <div class="col-sm-1"> <button class="btn btn-warning" name='submit1' onclick="javascript:checkUser();">Invite</button></div>

                    <div class="col-sm-2"> <a href="javascript:showEmailPreview();"  ><u>Email Preview</u></a></div>
                </div>
            </div>

            <br><br> <br><br><br><br>
            <div class="col-md-12">



                <div class="box">
                    <div class="box-header with-border">

                        <div class=" pull-left">
                            <div class="row">

                                <div class="col-sm-1">
                                    <div class="form-group">

                                        <a class="btn bg-orange" href="<?php echo base_url() . 'vendor/customer/downloadCustomers/'; ?><?php echo ucfirst($admin['adminDetails']['vendorid']); ?>">Export CSV</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <a class="btn bg-orange" href="<?php echo base_url() . 'vendor/customer/uploadCSV/'; ?>">Import CSV</a>
                            </div>
                        </div>

                        <center><h3 class="box-title" style="margin-right:300px; margin-top:10px;">Customers</h3></center>

                        <div class="box-tools pull-right" style="margin-top:5px;">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="#"  data-toggle="modal" data-target="#inviteusersmodal">Invite Customers to BabelBark</a>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <a class="btn bg-orange" href="<?php echo base_url(); ?>vendor/customer/addgenericcustomer">Add Generic Customer</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <br>
                    <?php if ($this->session->flashdata('error_message')) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            <?php echo $this->session->flashdata('error_message'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success_message')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>	<i class="icon fa fa-check"></i> Success</h4>
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php } ?>

                    <div id="section_customers">
                        <div id="spinner"
                             style="position:fixed;top:100px;left:60px;width:100%;height:100%;background-image:url('<?php echo base_url(); ?>/assets/img/cube.gif'); background-repeat:no-repeat;background-position:center; ">
                        </div>
                    </div>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->

        </div><!-- /.row -->


    </section><!-- /.content -->
</div><!-- /.content-wrapper -->









<div id="generic1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;"> Invitation Email Preview</h4>
            </div>
            <section class="content">

                <div class="box">
                    <div class="row">

                        <div class="col-lg-9 col-xs-12 ">

                            <p>
                            <div id="previewdiv"></div>
                            </p>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<div id="inviteusersmodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Invite Users to BabelBark</h4>
            </div>
            <section class="content">
                <input type="checkbox" name="Dept" value="checkall" checked="" class="selectall" /> Select all

                <div id='checkboxdynamic'>

                </div>

            </section>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="inviteConfirmed" >Invite</button>
            </div>
        </div>
    </div>
</div>


<!-- ----------------------------------------------- -->
<?php
include_once 'gcustomer.php';
?>

<script>
        $.ajax({
            url : "<?php echo base_url(); ?>vendor/customer/listofgenericcustomerajax",
            type : 'GET',
            success: function (data) {
                var customers = JSON.parse(data).customers;
                if(customers.length > 0){
                    var html_text = '<div class="box-body table-responsive" style="padding: 10px 15px;">' +
                        '<table id="example3" class="table table-bordered table-striped">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Customer Name</th>' +
                        '<th>BabelBark User</th>' +
                        '<th>Customer Contact Information</th>' +
                        '<th>Pet Name</th>' +
                        '<th>Customer & Pet Details</th>' +
                        '<th>Delete</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    var tr = '';
                    $.each(customers, function(index, value) {
                        if(value.usertype == 'Generic Customer'){
                            tr+='<tr id="'+value.customerid+'">' +
                                '<td>'+value.firstname+' '+value.lastname+'</td>' +
                                '<td>&nbsp;</td>' +
                                '<td>Email Address: '+value.email+'<br>'
                            if(value.phoneno){
                                tr+=' Phone No: '+value.phoneno+'<br>Address: '+value.address+'';
                            }else{
                                tr+='<br>Address: '+value.address+'';
                            }
                            if(value.city && value.state && value.zipcode){
                                tr+='<br/>'+value.city +', '+ value.state +', ' + value.zipcode;
                            } else if (value.city && value.state){
                                tr+='<br/>'+value.city +', '+ value.state;
                            } else if(value.city && value.zipcode){
                                tr+='<br/>'+value.city +', '+ value.zipcode;
                            } else if(value.state && value.zipcode){
                                tr+='<br/>'+ value.state +', ' + value.zipcode;
                            } else if(value.city){
                                tr+='<br/>'+value.city;
                            } else if(value.state){
                                tr+='<br/>'+value.state;
                            } else if(value.zipcode){
                                tr+='<br/>'+value.zipcode;
                            } else{
                                tr+='';
                            }                            
                            if(value.country){
                                tr+='<br/>'+value.country;
                            }else{
                                tr+='';
                            }
                            tr+='</td>';

                            tr+='<td>'+value.petname+'</td>' +
                                '<td>' +
                                '<center>' +
                                '<a href="<?php echo base_url() . 'vendor/customer/viewDetails/'?>'+value.customerid+'/'+rawurlencode (value.usertype)+'">' +
                                '<i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i>' +
                                ' </a>' +
                                '</center>  ' +
                                '</td>' +
                                '<td>' +
                                '<center>' +
                                '<a href="<?php echo base_url() . 'vendor/customer/deleteCustomer/';?>'+value.customerid+'" title="Click to delete" class="cnf-del-custom">' +
                                '<i class="fa fa-trash fa-lg"></i>' +
                                '</a>' +
                                '</center>' +
                                '</td>';
                            tr+='</tr>';
                        }else{
                            tr+='<tr id="'+value.appuserid+'">' +
                                '<td>'+value.firstname+' '+value.lastname+'</td>' +
                                '<td><center><img src="<?php echo base_url(); ?>assets/images/app_logo.png" style="width: 35px; height: 35px;" /></center></td>' +
                                '<td>Email Address: '+value.email+'<br>'
                            if(value.phonenumber){
                                tr+=' Phone No: '+value.phonenumber+'<br>Address: '+value.address+'';
                            }else{
                                tr+='<br>Address: '+value.address+'';
                            }
                            if(value.city){
                                tr+=value.city+',';
                            }else{
                                tr+='';
                            }
                            if(value.state){
                                tr+='<br/>'+value.state+',';
                            }else{
                                tr+='<br/>';
                            }
                            tr+='</td>';

                            tr+='<td>'+value.petname+'</td>' +
                                '<td>' +
                                '<center>' +
                                '<a href="<?php echo base_url() . 'vendor/customer/viewDetails/'?>'+value.appuserid+'/'+rawurlencode (value.usertype)+'">' +
                                '<i class="fa fa-arrow-circle-right" style=" font-size: 1.5em;"></i>' +
                                ' </a>' +
                                '</center>  ' +
                                '</td>' +
                                '<td>' +
                                '<center>' +
                                '<a href="<?php echo base_url() . 'vendor/customer/deleteCustomer/';?>'+value.appuserid+'" title="Click to delete" class="cnf-del-custom1">' +
                                '<i class="fa fa-trash fa-lg"></i>' +
                                '</a>' +
                                '</center>' +
                                '</td>';
                            tr+='</tr>';
                        }

                    });
                    html_text = html_text + tr + '</tbody></table></div><div class="box-footer clearfix"></div>';
                    $('#section_customers').html(html_text)

                }else{
                    $('#section_customers').html('<div align="center"><h1>No data available !</h1></div><br /><br />');
                }


                var generic_ustomers = JSON.parse(data).genericCustomers;
                var cnt = 0;
                if (!generic_ustomers) {
                    var jsonData = generic_ustomers;
                    for (var i = 0; i < jsonData.length; i++) {
                        cnt++;
                        $("#checkboxdynamic").append('<div class="col-lg-3"><input type="checkbox" class="inviteme" value="' + jsonData[i].email + '" checked> ' + jsonData[i].firstname + " " + jsonData[i].lastname + '</div>');
                    }
                }
                $('#example3').dataTable({searching: true, paging: true});
                $('#spinner').hide('slow');
            }
        });


    function rawurlencode (str) {
        str = (str+'').toString();
        return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A');
    }
</script>
