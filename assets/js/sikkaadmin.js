var isValidEmail;
var btn_id;
var currentEmail;

$(document).ready(function(){

    $.ajaxSetup({cache: false});

    $('.sikkaOfficedetails').click(function(event){
        $('#spinner').show();
        var data_ = {sikkaofficeid:this.id};

        var url_ =  baseurl+'index.php/admin/sikka/fetchStoreSikkaOffices';

        $.ajax({
            type: "post",
            url:url_,
            cache: false,
            data:data_,
            success: function(json){
                try{
                    $('#spinner').hide();
                    var obj = jQuery.parseJSON(json);
                    $('.sikkaoffices').hide();
                    if( Object.prototype.toString.call( obj ) === '[object Array]' ) {
                        obj.forEach(function(item,index)
                        {
                            $('#sikka_practice_name').text(item.name);
                        });
                    }else if(Object.prototype.toString.call( obj ) === '[object String]')
                    {
                        $('#data-practices').hide();
                        $('#no-data-practices').show();
                        alert(obj);
                    }
                    else{

                        $('#sikka_practice_name').text(obj.name);
                        var title,classname,Vaccination;
                        if(obj.shouldUpdate==0)
                        {
                            title = "Add to Bizbark";
                            classname = "addtobizbark";
                            Prescriptions = "disabled";
                            Vaccination = "disabled";
                        }
                        else
                        {
                            title = "Update";
                            classname = "updatebizbark";
                            Vaccination = "";
                            if(obj.prescription_added==0)
                            {
                                Prescriptions = "";
                            }
                            else
                            {
                                Prescriptions = "disabled";
                            }

                            if(obj.vaccinationadded == 0)
                            {
                                Vaccination = "";

                            }
                            else{
                                Vaccination = "disabled";
                            }

                        }

                        isValidEmail =  validateEmail(obj.email);

                        currentEmail = obj.email;


                        $('#practices').append('<tr id="'+obj.practice_id+'">\
                    <td><center>'+obj.name+'</center></td>\
                    <td><center>'+obj.email+'</center></td>\
                    <td><center><button id="'+obj.office_id+'_'+obj.practice_id+'" class="btn bg-orange '+classname+'">'+title+'</button></center></td>\
                     <td><center><button id="'+obj.office_id+'_'+obj.practice_id+'_p" class="btn bg-orange addprescription '+Prescriptions+'">Add Prescriptions</button></center></td>\
                      <td><center><button id="'+obj.office_id+'_'+obj.practice_id+'_v" class="btn bg-orange addvaccination '+Vaccination+'">Add Vaccination</button></center></td>\
                    </tr>');
                        $('#data-practices').show();
                        $('#no-data-practices').hide();
                    }

                    $('.addtobizbark').click(function(event){

                        btn_id = this.id;
                        if(validateEmail(currentEmail))
                        {
                            migration();
                        }
                        else{
                            $('#confirm-update').modal('show');
                        }

                    });


                    $('.addprescription').click(function(event){
                        $('#spinner').show();
                        url_ =  baseurl+'index.php/admin/sikka/addPrescription';
                        data_ = {practice_id:this.id};

                        btn_id = this.id;

                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.addEventListener("progress", function(evt) {
                                    console.log(evt)
                                }, false);

                                return xhr;
                            },
                            type: "post",
                            url:url_,
                            cache: false,
                            data:data_,
                            success: function(json){
                                try{
                                    $('#spinner').hide();
                                    var obj = jQuery.parseJSON(json);
                                    if(obj=="success")
                                    {
                                        $('#'+btn_id).addClass('disabled');
                                    }
                                    else if(obj == "error"){
                                        alert('Kindly first add Practice to Bizbark, by clicking "Add to Bizbark"');
                                    }
                                    else{
                                        alert(obj);
                                    }
                                    console.log(obj);

                                }catch(e) {
                                    $('#spinner').hide();
                                    console.log(e);
                                    alert('Something went wrong while fetching..');
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                if(xhr.status !=504)
                                {
                                    $('#spinner').hide();
                                    alert('Error while request..');
                                }

                            }
                        });
                    });

                    $('.addvaccination').click(function(event){
                        $('#confirm-progress').modal({backdrop: 'static', keyboard: false});
                        $('#confirm-progress').modal('show');
                        url_ =  baseurl+'index.php/admin/sikka/addVaccination';
                        data_ = {practice_id:this.id};

                        btn_id = this.id;

                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();


                                xhr.addEventListener("progress", function(evt) {
                                    console.log(evt)
                                }, false);

                                return xhr;
                            },
                            type: "post",
                            url:url_,
                            cache: false,
                            data:data_,
                            success: function(json){
                                try{
                                    $('#confirm-progress').modal('hide');
                                    var obj = jQuery.parseJSON(json);
                                    if(obj=="success")
                                    {
                                        $('#'+btn_id).addClass('disabled');
                                    }
                                    else if(obj == "error"){
                                        alert('Kindly first add Practice to Bizbark, by clicking "Add to Bizbark"');
                                    }
                                    else{
                                        alert(obj);
                                    }
                                    console.log(obj);

                                }catch(e) {
                                    $('#confirm-progress').modal('hide');
                                    console.log(e);
                                    alert('Something went wrong while fetching..');
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                if(xhr.status != 504)
                                {
                                    $('#confirm-progress').modal('hide');
                                    alert('Error while request..');
                                }
                            }
                        });

                        getProgress(true);
                    });

                    $('.updatebizbark').click(function(event){

                        $('#confirm-progress').modal({backdrop: 'static', keyboard: false});
                        $('#confirm-progress').modal('show');
                        url_ =  baseurl+'index.php/admin/sikka/updatepracticetoBizbark';
                        data_ = {practice_id:this.id};
                        $.ajax({
                            type: "post",
                            url:url_,
                            cache: false,
                            data:data_,
                            success: function(json){
                                try{
                                    $('#confirm-progress').modal('hide');
                                    var obj = jQuery.parseJSON(json);

                                    console.log(obj);

                                    if(obj=="success")
                                    {

                                        obj = "Data Updated Successfully";
                                        $('.sikka-response').text(obj);
                                        $('#confirm-details').modal('show');
                                    }
                                    else{
                                        $('.sikka-response').text(obj);
                                        $('#confirm-details').modal('show');
                                    }

                                }catch(e) {
                                    $('#confirm-progress').modal('hide');

                                    var response = "Something went wrong kindly try again.";
                                    $('.sikka-response').text(response);
                                    $('#confirm-details').modal('show');
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError){

                                if(xhr.status != 504)
                                {
                                    $('#confirm-progress').modal('hide');
                                    var response = "Sikka response error";
                                    $('.sikka-response').text(response);
                                    $('#confirm-details').modal('show');
                                    console.log(xhr);
                                }

                            }
                        });

                        getProgress(true);
                    });


                    $('.sikka_practices').show();

                }catch(e) {
                    $('#spinner').hide();
                    console.log(e);
                    alert('Something went wrong while fetching..');
                }
            },
            error: function(){
                $('#spinner').hide();
                alert('Error while request..');
            }

        });
    });

});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function enterEmail()
{
    currentEmail  = $('#email_practice').val();
    $('#confirm-update').modal('hide');
}

function migration()
{
    $('#confirm-progress').modal({backdrop: 'static', keyboard: false});
    $('#confirm-progress').modal('show');

    url_ =  baseurl+'index.php/admin/sikka/addtoBizBark';
    data_ = {practice_id:btn_id,email:currentEmail};
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.addEventListener("progress", function(evt) {
                console.log(evt);
            }, true);

            return xhr;
        },
        type: "post",
        url:url_,
        data:data_,
        success: function(json){
          try{
                $('#spinner').hide();

                $('#confirm-progress').modal('hide');

                breakit = true;
                var obj = jQuery.parseJSON(json);

                if(obj>0)
                {
                    $('#'+btn_id).text('Update');
                    $('#'+btn_id+'_p').removeClass('disabled');
                    var customercount = obj;
                    obj = customercount+" Customers Added Successfully";
                    $('.sikka-response').text(obj);
                    $('#confirm-details').modal('show');
                }else{
                    $('#'+btn_id).text('Update');
                    $('#'+btn_id+'_p').removeClass('disabled');
                    obj = "No Customers found for this practice.";
                    $('.sikka-response').text(obj);
                    $('#confirm-details').modal('show');
                }

            }
            catch(e) {
                $('#spinner').hide();
                $('#confirm-progress').modal('hide');
                var response = "Something went wrong kindly try again.";
                $('.sikka-response').text(response);
                $('#confirm-details').modal('show');
            }
        },
        error: function(xhr, ajaxOptions, thrownError){

            if(xhr.status != 504)
            {
                breakit = true;
                $('#spinner').hide();
                $('#confirm-progress').modal('hide');
                var response = "Sikka response Error";
                $('.sikka-response').text(response);
                $('#confirm-details').modal('show');
                console.log(xhr);
            }
        }
    });

    getProgress(true);

}

var last_progress = 0;

var error;
var breakit =  false;

function getProgress(isfirsttime){

    var  url_ =  baseurl+'index.php/admin/sikka/getProgress';
    $.ajax({
        type: "post",
        url:url_,
        data:{isfirst:isfirsttime},
        success: function(json){
            try{

                error = json;

                var obj = jQuery.parseJSON(json);

                if(obj.percent != null && obj.percent>last_progress)
                {
                    last_progress = obj.percent;
                    console.log(obj);

                    $('#progress-percent').html(obj.percent+'%');
                    $('#progress-percent').attr('aria-valuenow', obj.percent).css('width',obj.percent+'%');
                    $('#progress-message').text(obj.message);

                    if(obj.percent==100)
                    {
                        breakit = true;
                        $('#confirm-progress').modal('hide');
                    }


                }
                if(breakit == false)
                {
                    getProgress(false);
                }


            }
            catch(e) {


                var response = "Something went wrong kindly try again.";
                console.log(error);
                // $('.sikka-response').text(response);
                //  $('#confirm-details').modal('show');
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            console.log("Get Progress Error");
            console.log(xhr);
            getProgress(false);
        }
    });


}