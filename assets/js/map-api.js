var map;
var myLatlng;
var mapOptions;
var marker;
var contentString;
var infowindow;

 function initMap()
 {
				
					//console.log(addFullAddress);
					
					
			
					 myLatlng = new google.maps.LatLng(37.09024,-95.71289100000001);
				
					 mapOptions = {
						center: myLatlng,
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}
				
					map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);
				
					
				
					 contentString="<div>Click on the map to relocate marker.</div>";
				
					 infowindow=new google.maps.InfoWindow({
						content: contentString
					});
					 UserLatLng();
 }


$(document).ready(function(){
    $("#country,#address,#state,#city,#zipcode").change(function(){
    	calculateLatLng();
    });

});

$(document).ready(function(){
    $("#lat,#lng").change(function(){
    	UserLatLng();
    });

});

function UserLatLng()
{
			var lat = $("#lat").val();
			var lng = $("#lng").val();
			var addFullAddress=lat+" "+lng;

			var geocoder = new google.maps.Geocoder();

			console.log(addFullAddress);

			geocoder.geocode( {'address': addFullAddress}, function(results, status) {
						
						if (status == google.maps.GeocoderStatus.OK) {
							map.setCenter(results[0].geometry.location);
							
							if (marker) {
								//if marker already was created change positon
								marker.setPosition(results[0].geometry.location);
							} else {
								//create a marker
								marker = new google.maps.Marker({          
									position: results[0].geometry.location,
									animation: google.maps.Animation.DROP,
									map: map
								});
							}
								addressLat=marker.position.lat();
								addressLng=marker.position.lng();
								

								$('#lat').val(addressLat);
								$('#lng').val(addressLng);

								//console.log('in addFullAddress Lat' + addressLat);
								//console.log('in addFullAddress Long' + addressLng);
								
								infowindow.open(map,marker);
							
						} 
						else
						{
							//alert('Could not find address on the map. Manually locate store and click on the map to drop a marker');
							map.setCenter(new google.maps.LatLng(37.09024,-95.71289100000001));
							map.setZoom(13);
						}
						
							
					});	
				
				
				google.maps.event.addListener(map, 'click', function(event) {
					
					var clickedLatLng=event.latLng;
					addressLat=clickedLatLng.lat();
					addressLng=clickedLatLng.lng();

					$('#lat').val(clickedLatLng.lat());
					$('#lng').val(clickedLatLng.lng());
					
					var latitudeLong=new google.maps.LatLng(addressLat,addressLng)
					if (marker)
					{
						//if marker already was created change positon
						marker.setPosition(latitudeLong);
					} 
					else
					{
						//create a marker
						marker = new google.maps.Marker({          
							position: latitudeLong,
							animation: google.maps.Animation.DROP,
							map: map
						});
					}
				});		

}



function calculateLatLng()
{
			var addAddress = $("#address").val();
			var zipcode = $('#zipcode').val();
			var addCity = $("#city").val();
			var addCountry = $("#country").val();
			var addState = $("#state").val();
			var addFullAddress=zipcode+" "+addAddress + " " + addCity + " " +addState+" "+ addCountry;

			var geocoder = new google.maps.Geocoder();

			console.log(addFullAddress);

			geocoder.geocode( {'address': addFullAddress}, function(results, status) {
						
						if (status == google.maps.GeocoderStatus.OK) {
							map.setCenter(results[0].geometry.location);
							
							if (marker) {
								//if marker already was created change positon
								marker.setPosition(results[0].geometry.location);
							} else {
								//create a marker
								marker = new google.maps.Marker({          
									position: results[0].geometry.location,
									animation: google.maps.Animation.DROP,
									map: map
								});
							}
								addressLat=marker.position.lat();
								addressLng=marker.position.lng();

              
								$('#lat').val(addressLat);
								$('#lng').val(addressLng);
								



								//console.log('in addFullAddress Lat' + addressLat);
								//console.log('in addFullAddress Long' + addressLng);
								
								infowindow.open(map,marker);
							
						} 
						else
						{
							//alert('Could not find address on the map. Manually locate store and click on the map to drop a marker');
							map.setCenter(new google.maps.LatLng(37.09024,-95.71289100000001));
							map.setZoom(13);
						}
						
							
					});	
				
				
				google.maps.event.addListener(map, 'click', function(event) {
					
					var clickedLatLng=event.latLng;
					addressLat=clickedLatLng.lat();
					addressLng=clickedLatLng.lng();

					$('#lat').val(clickedLatLng.lat());
					$('#lng').val(clickedLatLng.lng());
					
					var latitudeLong=new google.maps.LatLng(addressLat,addressLng)
					if (marker)
					{
						//if marker already was created change positon
						marker.setPosition(latitudeLong);
					} 
					else
					{
						//create a marker
						marker = new google.maps.Marker({          
							position: latitudeLong,
							animation: google.maps.Animation.DROP,
							map: map
						});
					}
				});		

}

